#include <iostream>
#include <utils/exit.hh>

#include <core/data/sequence/Sequence.hh>
#include <core/calc/statistics/Hexbins.hh>
#include <core/data/io/fasta_io.hh>
#include <core/alignment/PairwiseSequenceAlignment.hh>
#include <core/alignment/SemiglobalAligner.hh>
#include <core/alignment/on_alignment_computations.hh>
#include <core/alignment/scoring/QuickSequenceIdentity.hh>
#include <core/alignment/scoring/NcbiSimilarityMatrixFactory.hh>

using namespace core::data::io;
using namespace core::alignment::scoring;
using namespace core::data::sequence;

std::string program_info = R"(

Estimates pairwise sequence similarity for a set of sequences given in a FASTA format.

The output table has 5 columns: i, j (indexing the pair of aligned sequences), 
seq_id-8, seq_id-16 and alignment seq_id, where the first two seq_id values are estimations
calculated for 8-aa and 16-aa reduced alphabet and the last value is the true sequence identity
evaluated over a sequence alignment.

If the number of sequences in the provided input file does not exceed 1000, true sequence identity
is evaluated for all pairs; when the input data set is larger, exact sequence alignment is computed
only when seq_id-16 score is greater than 0.2

USAGE:
    ap_QuickSequenceIdentity sequences.fasta

)";


/** @brief Estimates pairwise sequence similarity for a set of sequences given in a FASTA format
 *
 * CATEGORIES: core/alignment/scoring/QuickSequenceIdentity; core::calc::statistics::Hexbin
 * KEYWORDS:   sequence identity estimation; sequence alignment; Needleman-Wunsch; Hexbin plot; FASTA input; alignment score
 */
int main(const int argc, const char *argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  // --- read input database fasta file
  std::vector<Sequence_SP> all_fasta;  // --- stores all database sequences
  core::data::io::read_fasta_file(argv[1], all_fasta);

  all_fasta.erase(std::remove_if(all_fasta.begin(), all_fasta.end(), [](const Sequence_SP s) { return (s->length() < 40); }),all_fasta.end());
  all_fasta.erase(std::remove_if(all_fasta.begin(), all_fasta.end(),
    [](const Sequence_SP s) { return (std::find(s->sequence.begin(), s->sequence.end(), 'X')!=s->sequence.end()); }), all_fasta.end());
  all_fasta.erase(std::remove_if(all_fasta.begin(), all_fasta.end(),
    [](const Sequence_SP s) { return (core::data::sequence::count_aa_types(*s)<5); }), all_fasta.end());

  // --- prepare objects that quickly estimate protein sequence identity
  QuickSequenceIdentity16 quick_score_LZMJ16(all_fasta,"lz-mj.16");
  QuickSequenceIdentity8 quick_score_LZMJ8(all_fasta,"lz-mj.8");

  // --- prepare sequence aligner (dynamic programming)
  unsigned max_len = 0;
  std::for_each(all_fasta.begin(), all_fasta.end(), [&max_len](const Sequence_SP s) {max_len = std::max(max_len,unsigned(s->length()));});
  NcbiSimilarityMatrix_SP sim_m = NcbiSimilarityMatrixFactory::get().get_matrix("BLOSUM62");
  core::alignment::NWAligner<short, SimilarityMatrixScore<short>> aligner(max_len);

  core::calc::statistics::Hexbins<core::real, core::index4> hist(0.01);

  std::cout <<"# i   j hsdm.16 lz-mj.16   seq_id n_id min_len n_ali\n";
  for (size_t i = 1; i < all_fasta.size(); ++i) {
    for (size_t j = 0; j < i; ++j) {
      SimilarityMatrixScore<short> score(all_fasta[i]->sequence, all_fasta[j]->sequence, *sim_m);
      aligner.align(-10, -1, score);
      core::alignment::PairwiseAlignment_SP ali = aligner.backtrace();
      core::alignment::PairwiseSequenceAlignment seq_ali(ali, all_fasta[i], all_fasta[j]);
      double n_id = sum_identical(seq_ali);
      double min_len = std::min(all_fasta[i]->sequence.length(), all_fasta[j]->sequence.length());
      double n_aligned = sum_aligned(*ali);
      double LZMJ8_score = quick_score_LZMJ8(i, j);
      double LZMJ16_score = quick_score_LZMJ16(i, j);
      double seq_id =  n_id / min_len;
      if(all_fasta.size()>1000) {

        if((LZMJ16_score>0.2)||(seq_id>0.5))
          std::cout << utils::string_format("%3d %3d  %6.3f   %6.3f   %6.3f %4d    %4d  %4d   %s %s\n",
            i, j, LZMJ8_score, LZMJ16_score,seq_id, int(n_id), int(min_len),
            int(n_aligned), all_fasta[i]->header().c_str(), all_fasta[j]->header().c_str());

        hist.insert(LZMJ16_score,seq_id);
      } else
        std::cout << utils::string_format("%3d %3d  %6.3f   %6.3f   %6.3f %4d    %4d  %4d   %s %s\n",
          i, j, LZMJ8_score, LZMJ16_score,seq_id, int(n_id), int(min_len),
          int(n_aligned), all_fasta[i]->header().c_str(), all_fasta[j]->header().c_str());

      if (all_fasta.size() < 10) {
        std::cout << core::data::io::create_fasta_string(*ali, *all_fasta[i], *all_fasta[j]);
      }
    }
  }

  if(all_fasta.size()>100) {
    std::ofstream out("hex_histogram.dat");
    for (auto it = hist.cbegin(); it != hist.cend(); ++it) {
      auto bin = (*it).first;
      out << utils::string_format("%4d %4d %4d\n", bin.first, bin.second, (*it).second);
    }
    out.close();
  }
}

