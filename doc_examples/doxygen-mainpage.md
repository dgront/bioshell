# BioShell - a toolkit for bioinformatics, focused on macromolecular structures

BioShell is a set of command line utilities that solve most common bioinformatical problems. BioShell is also a C++ software library
for those who woyld like to build custom solutions for their problem. Since the version 3.0, BioShell offers bindings to Python scripting language,
i.e. BioShell classes can be easily used as any other Python modules

## Getting Started

These documentation provides primarily API of BioShell library. All the other topics, including the most basics ones are covered on our [ReadTheDocs website](https://bioshell.readthedocs.io/):

* [BioShell repository](https://bitbucket.org/dgront/bioshell/) - the source code, released under Apache 2.0 license, is hosted on bitbucked
* [BioShell install](https://bioshell.readthedocs.io/en/latest/doc/installation.html) - explains how to checkout the project and compile it
* [PyBioShell install](https://bioshell.readthedocs.io/en/latest/doc/pybioshell-installation.html) - PyBioShell distribution is just a single file; here you will learn how to download and use it


