#!/bin/bash

INPUT="2gb1A"

mkdir -p outputs_from_test

~/src.git/bioshell/bin/surpass_annealing -verbose=FINEST \
	-model:random \
	-in:ss2=test_inputs/$INPUT.ss2 \
	-sample:t_start=2.2 \
	-sample:t_end=0.9 \
	-sample:t_steps=15 \
	-sample:mc_outer_cycles=10 \
	-sample:mc_inner_cycles=10 \
	-sample:mc_cycle_factor=10 \
	-sample:perturb:range=0.7 \
	-out:pdb:min_en=tra-min.pdb \
	-out:pdb:min_en::fraction=0.05 \
	-sample:seed=12345
	
#	-in:pdb:native=native-surpass.pdb
#	-model:pdb=test_inputs/$INPUT.pdb \

mv trajectory.pdb energy.dat movers.dat observers.dat topology.dat outputs_from_test/