#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../data"

mkdir -p outputs_from_test

echo -e "Running: ./strc -in::pdb=./test_inputs/5edw.pdb -select::aa -out::pdb > outputs_from_test/aa.pdb"
./strc -in::pdb=./test_inputs/5edw.pdb -select::aa -out::pdb > outputs_from_test/aa.pdb
echo -e "Running: ./strc -in::pdb=./test_inputs/5edw.pdb -select::na -out::pdb > outputs_from_test/na.pdb"
./strc -in::pdb=./test_inputs/5edw.pdb -select::nt -out::pdb > outputs_from_test/na.pdb
echo -e "Running: ./strc -in::pdb=./test_inputs/5edw.pdb -select::ca -out::pdb > outputs_from_test/ca.pdb"
./strc -in::pdb=./test_inputs/5edw.pdb -select::ca -out::pdb > outputs_from_test/ca.pdb
echo -e "Running: ./strc -in::pdb=./test_inputs/5edw.pdb -select::bb -out::pdb > outputs_from_test/bb.pdb"
./strc -in::pdb=./test_inputs/5edw.pdb -select::bb -out::pdb > outputs_from_test/bb.pdb
echo -e "Running: ./strc -in::pdb=./test_inputs/5edw.pdb -select::bb_cb -out::pdb > outputs_from_test/bb_cb.pdb"
./strc -in::pdb=./test_inputs/5edw.pdb -select::bb_cb -out::pdb > outputs_from_test/bb_cb.pdb
echo -e "Running: ./strc -in::pdb=./test_inputs/5edw.pdb -select::atoms::by_name="_OG_" -out::pdb > outputs_from_test/frag1.pdb"
./strc -in::pdb=./test_inputs/5edw.pdb -select::atoms::by_name="_OG_" -out::pdb > outputs_from_test/frag1.pdb
echo -e "Running: ./strc -in::pdb=./test_inputs/5edw.pdb -select::chains=TP -out::pdb > outputs_from_test/tp.pdb"
./strc -in::pdb=./test_inputs/5edw.pdb -select::chains=TP -out::pdb > outputs_from_test/tp.pdb

