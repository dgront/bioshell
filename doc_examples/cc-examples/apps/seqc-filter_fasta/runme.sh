#!/bin/bash
echo -e "Running: ./seqc -if=5edw.fasta -select::sequence::long_at_least=10 -select::sequence::nucleic -out::fasta"
./seqc -if=test_inputs/5edw.fasta -select::sequence::long_at_least=10 -select::sequence::nucleic -out::fasta

echo -e "Running: ./seqc -if=5edw.fasta -select::sequence::long_at_least=100 -select::sequence::nucleic -out::fasta"
./seqc -if=test_inputs/5edw.fasta -select::sequence::long_at_least=100 -select::sequence::nucleic -out::fasta

echo -e "Running: ./seqc -if=5edw.fasta -select::sequence::long_at_least=10 -select::sequence::protein -out::fasta"
./seqc -if=test_inputs/5edw.fasta -select::sequence::long_at_least=10 -select::sequence::protein -out::fasta

echo -e "Running: ./seqc -if=5edw.fasta -select::sequence::long_at_least=500 -select::sequence::protein -out::fasta"
./seqc -if=test_inputs/5edw.fasta -select::sequence::long_at_least=500 -select::sequence::protein -out::fasta
