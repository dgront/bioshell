#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../data"

# the most simplistic test on four atoms:
echo -e "./str_calc -calc::crmsd -in:pdb=test_inputs/four_atoms-1.pdb -in:pdb:native=test_inputs/four_atoms-2.pdb -out::pdb"
./str_calc -calc::crmsd -in:pdb=test_inputs/four_atoms-1.pdb -in:pdb:native=test_inputs/four_atoms-2.pdb -out::pdb

# simple crmsd calculations
echo -e "#./str_calc -ipn=test_inputs/2gb1.pdb -ip=test_inputs/2gb1-model1.pdb -calc::crmsd"
./str_calc -ipn=test_inputs/2gb1.pdb -ip=test_inputs/2gb1-model1.pdb -calc:crmsd

# Cut the helix of 2gb1
echo -e "./strc -ip=test_inputs/2gb1.pdb -select:substructure=A:24-36 -select::bb -out::pdb=Hnative.pdb"
./strc -ip=test_inputs/2gb1.pdb -select:substructure=A:24-36 -select::bb -out::pdb=Hnative.pdb

# Cut the helix of 2gb1 - model 1
echo -e "./strc -ip=test_inputs/2gb1-model1.pdb -select:substructure=A:24-36 -select::bb -out::pdb=Hmodel.pdb"
./strc -ip=test_inputs/2gb1-model1.pdb -select:substructure=A:24-36 -select::bb -out::pdb=Hmodel.pdb

# Superimpose the two helices
echo -e "./str_calc -in:pdb:native=Hnative.pdb -in:pdb=Hmodel.pdb -calc::crmsd -op=o.pdb"
./str_calc -in:pdb:native=Hnative.pdb -in:pdb=Hmodel.pdb -calc::crmsd -op=o1.pdb

# Cut bb of  2gb1
echo -e "./strc -ip=test_inputs/2gb1.pdb -select::bb -out::pdb=bbn.pdb"
./strc -ip=test_inputs/2gb1.pdb -select::bb -out::pdb=bbn.pdb

# Cut bb of 2gb1 - model 1
echo -e "./strc -ip=test_inputs/2gb1-model1.pdb -select::bb -out::pdb=bbm.pdb"
./strc -ip=test_inputs/2gb1-model1.pdb -select::bb -out::pdb=bbm.pdb

#superimpose the two
echo -e "str_calc -ipn=bbn.pdb -ip=bbm.pdb -calc::crmsd -calc::crmsd::matching_atoms=A:24-36"
./str_calc -ipn=bbn.pdb -ip=bbm.pdb -calc::crmsd -calc::crmsd::matching_atoms=A:24-36

rm *.pdb
