#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../data"

mkdir -p outputs_from_test

# Using strc tool to extract chain A from each protein to be aligned
echo "Using strc tool to extract chain A from each protein to be aligned" 
echo "" > pdb_list # Clean pdb_list file just in case	
for i in ./test_inputs/*.pdb 
do 
  PDB_CODE=`basename $i .pdb`
  ./strc -in::pdb=$i -select::substructure=A -out::pdb=$PDB_CODE"A.pdb"
  echo $PDB_CODE"A.pdb" >> pdb_list
done

echo -e "Running: str_align -in:pdb:listfile=pdb_list -n_threads=4 -str_align:out:tabular >./outputs_from_test/str_align.out 2>str_align.log"
./str_align -in:pdb:listfile=pdb_list -n_threads=4 -str_align:out:tabular > ./outputs_from_test/str_align.out

# Convert tm-scores to distances
grep -v "#" ./outputs_from_test/str_align.out | awk '{print $2,$7,1.0 - (($15>$18) ? $15 : $18),$15,$18}' > ./outputs_from_test/distances.txt

./clust -n=35 -i=./outputs_from_test/distances.txt -clustering:out:tree -complete -clustering:out:distance=0.1 > ./outputs_from_test/clustering_tree_complete.txt

# move cluster files to the output directory
mv c*_* ./outputs_from_test/
# Cleanup after test (remove unnecessary output files)
rm pdb_list ????A.pdb 



