#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../data"

# Extract chain A from each deposit (to remove water, ligands, other chains, etc)
./strc -select:chains=A -in:pdb=test_inputs/1clf.pdb -select:aa -op=1clfA.pdb
./strc -select:chains=A -in:pdb=test_inputs/1pc5.pdb -select:aa -op=1pc5A.pdb

# Run the actual alignment; write output both in PDB and FASTA formats
echo -e "Running: ./str_align -in::pdb=1clfA.pdb -in::pdb::native=1pc5A.pdb -out::fasta=1clfA-1pc5A.fasta -out::pdb"
./str_align -in:pdb=1clfA.pdb -in:pdb:native=1pc5A.pdb -out:fasta=1clfA-1pc5A.fasta -out:pdb

mkdir -p outputs_from_test

# Copy results
mv 1clfA-1pc5A.fasta 1clfA-on-1pc5A.pdb 1pc5A-on-1clfA.pdb outputs_from_test/

# Clean up
rm 1clfA.pdb 1pc5A.pdb

