#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../data"

mkdir -p outputs_from_test

./str_calc -select:bb -in::pdb:path=./test_inputs \
    -in:pdb:listfile=./test_inputs/input_pdb_list \
    -calc:phi_psi -in:pdb:header -select::aa > str_calc.results
grep -v "#" str_calc.results | awk '{print $5,$6}' > phi_psi.results

./ap_Hexbins phi_psi.results 18 > hexahist.dat
awk '{print $1,$2,log($3)}' hexahist.dat > hexahist-log.dat

# Uncomment the line below to make a plot of the Ramachandran map
#python ./make_plots.py hexahist-log.dat -10 10 -10 10 1

mv *.results *.dat outputs_from_test/
