#!/bin/bash

# This script makes nice plots for Ramachandran map variants, as desired.
# Let's say the output file from str_calc is named RAMA_RAW

grep -v "#" RAMA_RAM | awk '{print $3,$5,$6}' | cut -c 3,6- Rama-2 > Rama-3
for i in A B C D E F G H I K L M N P Q R S T V W 
do
  grep $i Rama-3 | cut -c 2- > Rama-3-$i
  ap_Hexbins Rama-3-$i 3.6 > hist-$i
  awk '{print $1,$2,log($3)}' hist-$i > hist-$i-log
done 

cut -c 2- Rama-3 > Rama-3-all
ap_Hexbins Rama-3-all 1.8 > hist-all
awk '{print $1,$2,log($3)}' hist-all > hist-all-log

python make_plots.py hist-all-log -100 100 -100 100 1
