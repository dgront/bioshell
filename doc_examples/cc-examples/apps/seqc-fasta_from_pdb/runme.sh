#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../data"

echo -e "Running: ./seqc -in::pdb=./test_inputs/1rrx.pdb -out::fasta"
./seqc -in::pdb=./test_inputs/1rrx.pdb -out::fasta

echo -e "Running: ./seqc -in::pdb=./test_inputs/1ofz.pdb -out::fasta"
./seqc -in::pdb=./test_inputs/1ofz.pdb -out::fasta

echo -e "Running: ./seqc -in::pdb=./test_inputs/2kwi.pdb -out::fasta -select::chains=B"
./seqc -in::pdb=./test_inputs/1ofz.pdb -out::fasta

echo -e "Running: ./seqc -in:pdb:chainid::list=./test_inputs/chains_list.txt -in:pdb:path=./test_inputs/ -out::fasta"
./seqc -in:pdb:chainid::list=./test_inputs/chains_list.txt -in:pdb:path=./test_inputs/ -out::fasta
