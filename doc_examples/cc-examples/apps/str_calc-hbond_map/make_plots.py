#!/usr/bin/env python

import sys, subprocess, re
import matplotlib.pyplot as plt

INFILE = sys.argv[1] if (len(sys.argv) == 2) else "outputs_from_test/stdout.out"

x    = []
y    = []
nres = 0
for line in open(INFILE) :
  tokens = re.split("\s+",line.strip())
  dssp_energy = float(tokens[12])
  xi = int(tokens[0])
  yi = int(tokens[4])
  if max(xi,yi) > nres : nres = max(xi,yi)
  if dssp_energy < -0.4 : 
    x.append(xi)
    y.append(yi)
    y.append(xi)
    x.append(yi)
    
fig, ax = plt.subplots()
ax.scatter(x, y, marker="s", c="black")
ax.set_xlim((0,nres+1))
ax.set_ylim((0,nres+1))
ax.set_aspect(1.0)
fig.savefig('hbond_map.png', dpi=600)
plt.close(fig)
