#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../data"

echo -e "Running: ./str_calc -in::pdb=./test_inputs/2gb1.pdb -calc:hbonds:bb"
./str_calc -in::pdb=./test_inputs/2gb1.pdb -calc:hbonds:bb
