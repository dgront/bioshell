#!/usr/bin/env python

import sys, subprocess, re
import matplotlib.pyplot as plt

INFILE = sys.argv[1]
OUTFILE = 'contact_map.png'
if len(sys.argv) > 2 : OUTFILE = sys.argv[1]

x    = []
y    = []
cnts = []
nres = 0
for line in open(INFILE) :
  if line.strip()[0] == '#' : continue
  tokens = re.split("\s+",line.strip())
  if len(tokens) < 7 : continue
  distance = float(tokens[6])
  xi = int(tokens[2])
  yi = int(tokens[5])
  if max(xi,yi) > nres : nres = max(xi,yi)
  if distance < 4.8 and abs(xi-yi) > 2 : 
    x.append(xi)
    y.append(yi)
    y.append(xi)
    x.append(yi)
    if len(tokens) > 7 : 
      cnts.append(float(tokens[7]))
      cnts.append(float(tokens[7]))
    else : 
      cnts.append(1.0)
      cnts.append(1.0)
        
print len(x)
print len(cnts)
fig, ax = plt.subplots()
ax.scatter(x, y, c=cnts, lw=0.1, marker='s', cmap='jet')
ax.set_xlim((0,nres+1))
ax.set_ylim((0,nres+1))
ax.set_aspect(1.0)
fig.savefig(OUTFILE, dpi=600)
plt.close(fig)
