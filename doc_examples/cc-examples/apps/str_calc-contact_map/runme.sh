#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../data"

# calculate a map of minimal distances between residues
echo -e "Running: ./str_calc -in::pdb=./test_inputs/2gb1.pdb -calc::distmap::min -calc::distmap::describe"
./str_calc -in::pdb=./test_inputs/2gb1.pdb -calc::distmap::min -calc::distmap::describe

# calculate all-atom map of distances
./str_calc -in::pdb=./test_inputs/2gb1.pdb -calc::distmap::allatom -calc::distmap::describe > all_distances

mkdir -p outputs_from_test

# create a contact map from distances
awk '{if(($11<4.5)) print $1,$2,$3,$6,$7,$8}' < all_distances | uniq -c | awk '{print $2,$3,$4,$5,$6,$7,1.0,$1}' > outputs_from_test/aa_contacts_cnts
rm all_distances



