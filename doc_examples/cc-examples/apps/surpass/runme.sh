#!/bin/bash

INPUT="2gb1A"

./surpass -verbose=FINER \
	-in:pdb=test_inputs/$INPUT.pdb \
	-in:ss2=test_inputs/$INPUT.ss2 \
	-sample:replicas:observation_mode=1 \
	-sample:t_start=1.2 \
	-sample:mc_outer_cycles=1000 \
	-sample:mc_inner_cycles=100 \
	-sample:mc_cycle_factor=1 \
	-sample:perturb:range=0.7 \
	-out:pdb:min_en=tra-min.pdb \
	-out:pdb:min_en::fraction=0.05 \
	-sample:seed=12345
	
#	-in:pdb:native=native-surpass.pdb
