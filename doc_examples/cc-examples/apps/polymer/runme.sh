#!/bin/bash

echo "# Running ./polymer -polymer::n_beads=20 -sample:t_start=1.0 -sample:t_steps=1-sample:mc_inner_cycles=500 -sample:mc_outer_cycles=10 -sample::perturb::range=0.3 -sample:seed=82345 -v=INFO"
./polymer -polymer::n_beads=20 \
	-sample:t_start=1.0 \
	-sample:t_steps=1 \
	-sample:mc_inner_cycles=500 \
	-sample:mc_outer_cycles=10 \
	-sample::perturb::range=0.3 \
	-out::pdb=tra.pdb \
	-sample:seed=82345 -v=INFO
mkdir -p outputs_from_test
mv *.pdb *.dat outputs_from_test/

