#include <iostream>
#include <core/data/basic/Vec3Cubic.hh>
#include <simulations/systems/BuildPolymerChain.hh>
#include <simulations/systems/SingleAtomType.hh>
#include <simulations/systems/CartesianChains.hh>
#include <simulations/observers/cartesian/PdbObserver.hh>
#include <simulations/observers/cartesian/ExplicitPdbFormatter.hh>


#include <utils/exit.hh>

using namespace simulations::systems;
using namespace core::data::structural;
using core::data::basic::Vec3Cubic;


std::string program_info = R"(

Creates a mixture of simple polymer chains in a periodic box

USAGE:
    ./ex_BuildPolymerChain box_width n_chains n_atoms_in_chain

)";

/* @brief Creates a mixture of simple polymer chains in a periodic box
 * 
 * CATEGORIES: simulations::systems::BuildPolymerChain
 * KEYWORDS:   Chain; ResidueChain; Polymer; Vec3Cubic
 */
int main(const int argc, const char *argv[]) {

  if (argc < 4) utils::exit_OK_with_message(program_info);

  double box = atof(argv[1]);
  core::index2 n_chains = atoi(argv[2]);
  core::index2 n_res_each = atoi(argv[3]);

  // --- here we create a Structure object of n_chains polyalanine chains
  Structure_SP starting_structure = std::make_shared<Structure>("");
  std::string sequence(n_res_each,'A'); // --- We create a polyalanine chain, the sequence is made by many 'A's
  for (core::index2 i = 0; i < n_chains; ++i)
    starting_structure->push_back( Chain::create_ca_chain(sequence, std::string{utils::letters[i]} )); // --- A + 1 makes the chain code

  // --- we have to renumber atoms so the indexes are consistent in the whole structure
  core::index4 i_atom = 0;
  for(Chain_SP m : *starting_structure)
    std::for_each(m->first_atom(), m->last_atom(), [&](PdbAtom_SP e) {(e)->id(++i_atom);});
 // Vec3Cubic::set_box_len(box); // --- set periodic box width
  std::shared_ptr<AtomTypingInterface> atom_typing = std::make_shared<SingleAtomType>(); // --- simplest atom typing possible
  CartesianChains chains(atom_typing,*starting_structure);
  BuildPolymerChain chain_builder(chains);
  chain_builder.generate(3.8,5.5);
  core::index4 ai = 0;
    std::shared_ptr<simulations::observers::cartesian::AbstractPdbFormatter> fmt = std::make_shared<simulations::observers::cartesian::ExplicitPdbFormatter>(*starting_structure);
    simulations::observers::cartesian::PdbObserver start(chains, fmt, "");
    start.observe();
}

