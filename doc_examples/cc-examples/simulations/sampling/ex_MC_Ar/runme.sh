#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

mkdir -p outputs_from_test
echo -e "# Running ./ex_MC_Ar 256 0.5 97 10 100 1.0"
./ex_MC_Ar 256 0.5 97 10 1000 1.0 
mv final.pdb start.pdb
echo -e "# Running ./ex_MC_Ar start.pdb 0.5 97 10 100 3.0"
./ex_MC_Ar start.pdb  0.5 97 10 100 3.0
rm -rf ar_tra.pdb start.pdb
mkdir -p outputs_from_test
mv *.pdb *.dat outputs_from_test/
