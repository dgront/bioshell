#include <iostream>

#include <simulations/evaluators/CallEvaluator.hh>
#include <simulations/forcefields/CalculateEnergyBase.hh>

#include <simulations/movers/ising/SingleFlip2D.hh>
#include <simulations/movers/ising/WolffMove2D.hh>

#include <simulations/observers/ObserveEvaluators.hh>
#include <simulations/observers/ObserveMoversAcceptance.hh>
#include <simulations/observers/TriggerEveryN.hh>
#include <simulations/observers/ObserveReplicaFlow.hh>

#include <simulations/sampling/IsothermalMC.hh>
#include <simulations/sampling/ReplicaExchangeMC.hh>
#include <simulations/systems/ising/Ising2D.hh>

using namespace core::data::basic;

utils::Logger logs("ex_REMC_Ising");

std::string program_info = R"(

The program runs a Replica Exchange Monte Carlo simulation of a simple 2D Ising system (a spin glass).

The simulation performs N_INNER x N_OUTER MC cycles and then a replica exchange is attempted.


USAGE:
    ex_REMC_Ising [system_size inner_cycles outer_cycles n_exchanges]

EXAMPLE:
    ex_REMC_Ising 32 50 100 100
)";


/** @brief The program runs a Replica Exchange Monte Carlo simulation of a simple 2D Ising system (spin glass).
 *
 * This example shows how to set up a REMC simulation
 *
 * CATEGORIES: simulations/sampling/ReplicaExchangeMC; simulations/systems/ising/Ising2D
 * KEYWORDS:   REMC; Ising2D; observer; simulation
 * IMG: Energy_plot.png
 * IMG_ALT: Energy over time in Ising model
 */
int main(const int argc,const char* argv[]) {

  using namespace simulations::systems::ising;
  using namespace simulations::movers::ising;

  core::index4 n_outer_cycles = 10;
  core::index4 n_inner_cycles = 10;
  core::index4 n_exchanges = 10;
  std::vector<double> temperatures = {100.0, 7.5, 5, 4, 3, 2.5, 2.25, 2, 1.75, 1.5, 1};

  core::index2 system_size = 32;
  if (argc < 2) std::cerr << program_info;
  else {
    system_size = atoi(argv[1]);
    n_inner_cycles = atoi(argv[2]);
    n_outer_cycles = atoi(argv[3]);
    n_exchanges = atoi(argv[4]);
  }

  core::calc::statistics::Random::get().seed(12345);  // --- seed the generator for repeatable results

  std::vector<std::shared_ptr<Ising2D<core::index1,core::index2>>> systems;
  std::vector<simulations::sampling::IsothermalMC_SP> replica_samplers;
  std::vector<simulations::forcefields::TotalEnergy_SP> energies;

  for (core::index2 irepl = 0; irepl < temperatures.size(); ++irepl) {

    // ---------- Create the systems to be sampled ----------
    std::shared_ptr<Ising2D<core::index1,core::index2>> system
        = std::make_shared<Ising2D<core::index1,core::index2>>(system_size, system_size);
    system->initialize();    // Populate system with random spins
    systems.push_back(system);
    energies.push_back(system);

    // ---------- Movers definition ----------
    simulations::movers::MoversSet_SP movers = std::make_shared<simulations::movers::MoversSetSweep>();
    movers->add_mover( std::make_shared<SingleFlip2D<core::index1,core::index2>>(*system),system->count_spins());
    movers->add_mover( std::make_shared<WolffMove2D<core::index1,core::index2>>(*system),system->count_spins()*0.2);

    // ---------- Create the sampler ----------
    auto sampler = std::make_shared<simulations::sampling::IsothermalMC>(movers, temperatures[irepl]);
    replica_samplers.push_back(sampler);
    sampler->cycles(n_inner_cycles,n_outer_cycles);

    simulations::observers::ObserveEvaluators_SP observations
      = std::make_shared<simulations::observers::ObserveEvaluators>(utils::string_format("energy-%.3f.dat",temperatures[irepl]));
    observations->add_evaluator(system);
    sampler->outer_cycle_observer(observations);
    simulations::observers::ObserveMoversAcceptance_SP obs_ms
      = std::make_shared<simulations::observers::ObserveMoversAcceptance>(*movers, utils::string_format("movers-%.3f.dat",temperatures[irepl]));
    obs_ms->observe_header();
    sampler->outer_cycle_observer(obs_ms);
  }

  bool replica_isothermal_observation_mode = true;
  auto remc = std::make_shared<simulations::sampling::ReplicaExchangeMC>(replica_samplers, energies, replica_isothermal_observation_mode);
  auto remc_flow = std::make_shared<simulations::observers::ObserveReplicaFlow>(*remc,"replica_flow.dat");
  remc->exchange_observer(remc_flow);
  remc->replica_exchanges(n_exchanges);
  remc->run();
}

