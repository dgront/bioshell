#include <iostream>
#include <string>
#include <stdexcept>
#include <stdlib.h>
#include <fstream>
#include <vector>

#include <simulations/evaluators/CallEvaluator.hh>
#include <simulations/forcefields/CalculateEnergyBase.hh>
#include <simulations/evaluators/Evaluator.hh>
#include <simulations/forcefields/TotalEnergy_OBSOLETE.hh>

#include <simulations/observers/ObserveEvaluators.hh>
#include <simulations/observers/ObserveMoversAcceptance.hh>
#include <simulations/observers/TriggerEveryN.hh>
#include <simulations/observers/ObserveReplicaFlow.hh>
#include <simulations/observers/ObserveWLSampling.hh>

#include <simulations/sampling/IsothermalMC.hh>
#include <simulations/sampling/ReplicaExchangeMC.hh>
#include <simulations/systems/ising/RubikCube.hh>

#include <utils/options/sampling_options.hh>
#include <utils/options/sampling_from_cmdline.hh>
#include <simulations/sampling/WangLandauSampler.hh>


using namespace simulations;
using namespace simulations::systems::ising;

utils::Logger logs("ap_Rubik_simulation");

std::string program_info = R"(

The program runs a Replica Exchange Monte Carlo simulation of a Rubik's cube system

)";

/** @brief Turns energy of a system into an energy bin index (integer)
 * @param energy - system's energy
 * @return integer assigned to a bin; may be negative
 */
inline int bfe(double energy) { return (int) energy; }

std::shared_ptr<simulations::sampling::WangLandauSampler> prepare_wl_simulation(const simulations::SimulationSettings &settings) {

  using namespace utils::options;
  int system_size = settings.get<int>("cube_size");
  Rubik_SP system = std::make_shared<Rubik>(system_size);
  logs << "Minimum energy: " << system->calculate()<<"\n";
  system->scramble();    // Set the cube to a random conformation
  logs << "starting energy: " << system->calculate()<<"\n";

  // ---------- Movers definition ----------
  simulations::movers::MoversSet_SP movers = std::make_shared<simulations::movers::MoversSetSweep>();
  movers->add_mover(std::static_pointer_cast<simulations::movers::Mover>(system), system_size * system_size);

  // ---------- Create the sampler ----------
  std::shared_ptr<simulations::sampling::WangLandauSampler> sampler = std::make_shared<simulations::sampling::WangLandauSampler>(
      movers, system->calculate(), bfe, system_size * system_size * 6);
  sampler->reset(settings);

  simulations::forcefields::CalculateEnergyBase_SP energies;

  simulations::observers::ObserveEvaluators_SP observations
      = std::make_shared<simulations::observers::ObserveEvaluators>(utils::string_format("energy-wl.dat"));
  observations->add_evaluator(system);
  sampler->outer_cycle_observer(observations);
  sampler->outer_cycle_observer(std::make_shared<simulations::observers::ObserveWLSampling>(*sampler, "wl.dat"));

  simulations::observers::ObserveMoversAcceptance_SP obs_ms
      = std::make_shared<simulations::observers::ObserveMoversAcceptance>(*movers,
                                                                          utils::string_format("movers-wl.dat"));
  obs_ms->observe_header();
  sampler->outer_cycle_observer(obs_ms);

  return sampler;
}

std::shared_ptr<simulations::sampling::ReplicaExchangeMC> prepare_replica_simulation(const simulations::SimulationSettings& settings) {

  using namespace utils::options;
  std::vector<Rubik_SP> systems;
  std::vector<simulations::sampling::IsothermalMC_SP> replica_samplers;
  std::vector<simulations::forcefields::CalculateEnergyBase_SP> energies;
  std::vector<double> temperatures;
  utils::split(settings.get<std::string>(replicas),temperatures, ',');
  core::index4 n_outer_cycles = settings.get<core::index4>(mc_outer_cycles);
  core::index4 n_inner_cycles = settings.get<core::index4>(mc_inner_cycles);
  core::index4 n_exchanges = settings.get<core::index4>(replica_exchanges);

  int system_size =  settings.get<int>("cube_size");
  for (core::index2 irepl = 0; irepl < temperatures.size(); ++irepl) {

    // ---------- Create the systems to be sampled ----------
    Rubik_SP system = std::make_shared<Rubik>(system_size);
    system->scramble();    // Set the cube to a random conformation
    systems.push_back(system);
    energies.push_back(system);

    // ---------- Movers definition ----------
    simulations::movers::MoversSet_SP movers = std::make_shared<simulations::movers::MoversSetSweep>();
    movers->add_mover(std::static_pointer_cast<simulations::movers::Mover>(system), system_size * system_size);

    // ---------- Create the sampler ----------
    auto sampler = std::make_shared<simulations::sampling::IsothermalMC>(movers, temperatures[irepl]);
    replica_samplers.push_back(sampler);
    sampler->cycles(n_inner_cycles, n_outer_cycles);

    simulations::observers::ObserveEvaluators_SP observations
        = std::make_shared<simulations::observers::ObserveEvaluators>(utils::string_format("energy-%.3f.dat", temperatures[irepl]));
    observations->add_evaluator(system);
    sampler->outer_cycle_observer(observations);
    simulations::observers::ObserveMoversAcceptance_SP obs_ms
        = std::make_shared<simulations::observers::ObserveMoversAcceptance>(*movers,
                                                                            utils::string_format("movers-%.3f.dat", temperatures[irepl]));
    obs_ms->observe_header();
    sampler->outer_cycle_observer(obs_ms);
  }
  auto remc = std::make_shared<simulations::sampling::ReplicaExchangeMC>(replica_samplers, energies, true);
  auto remc_flow = std::make_shared<simulations::observers::ObserveReplicaFlow>(*remc, "replica_flow.dat");
  remc->exchange_observer(remc_flow);
  remc->replica_exchanges(n_exchanges);

  return remc;
}

/** @brief The program runs a Replica Exchange Monte Carlo simulation of a Rubik's cube system.
 *
 * This example shows how to simulate a system using BioShell library
 *
 * CATEGORIES: simulations/sampling/ReplicaExchangeMC;
 * KEYWORDS:   Monte Carlo; sampling;  observer; simulation
 * IMG_ALT: Example results from a Rubik's cube simulations
 */
int main(const int argc, const char *argv[]) {

  using namespace utils::options; // --- All the options are in this namespace
  static Option cube_size("-c", "-cube_size", "size of the Rubik's cube");
  static Option sampler("-s", "-sampler", "MC sampler: 'remc' or 'wl'");

  utils::options::OptionParser &cmd = utils::options::OptionParser::get();
  cmd.register_option(utils::options::help, verbose, rnd_seed, cube_size(3), sampler);
  cmd.register_option(mc_outer_cycles(10000), mc_inner_cycles(10), mc_cycle_factor(1), replica_exchanges(10));
  cmd.register_option(begin_temperature(2.0), end_temperature(0.5), temp_steps(0.1),
                      replicas("2,1.75,1.5,1.25,1.0,0.8,0.7,0.6,0.5"));

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  if (rnd_seed.was_used()) {
    auto rnd = option_value<core::calc::statistics::Random::result_type>(rnd_seed);
    core::calc::statistics::Random::seed(rnd);
    logs << utils::LogLevel::SEVERE << "Pseudorandom start: " << rnd << "\n";
  } else {
    core::calc::statistics::Random::get().seed(12345);  // --- seed the generator for repeatable results
    logs << utils::LogLevel::SEVERE << "Pseudorandom start with seed: 12345\n";
//    core::calc::statistics::Random::seed(time(0));
//    logs << utils::LogLevel::SEVERE << "Pseudorandom start with time(0) seed: \n";
  }

  simulations::SimulationSettings settings;
  settings.insert_or_assign(cmd, true);

  if ((option_value<std::string>(sampler) == "wl") || (option_value<std::string>(sampler) == "WL")) {
    auto wl = prepare_wl_simulation(settings);
    wl->run();
  } else {
    auto remc = prepare_replica_simulation(settings);
    remc->run();
  }

}
