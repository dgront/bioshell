#include <memory>
#include <iostream>
#include <random>

#include <core/calc/statistics/Random.hh>

#include <simulations/movers/Mover.hh>
#include <simulations/movers/MoversSetSweep.hh>
#include <simulations/evaluators/EchoEvaluator.hh>
#include <simulations/evaluators/CallEvaluator.hh>
#include <simulations/sampling/SimulatedAnnealing.hh>

#include <simulations/observers/ObserveEvaluators.hh>

/** @brief  Models a point particle in 1D space with harmonic energy.
 * This simple class implements the Mover interface. One has to implement just two
 * essential methods: <code>move()</code> and <code>undo()</code>. For simplicity, in this example energy calculation
 * is implemented within the <code>move()</code> method.
 */
class HarmonicSystemMover : public simulations::movers::Mover {
public:
  const double x0;        ///< Initial position of the particle, where energy is 0.0
  double x;               ///< Actual position of the particle at the end of the spring
  double recent_energy;   ///< Actual energy of the spring

  HarmonicSystemMover() : simulations::movers::Mover("HarmonicSystemMover"), x0(0.0) {}

  /** @brief  Moves the particle randomly in either direction.
   * This method is declared abstract in Mover class and must be implemented here
   */
  virtual bool move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme)  {

    double old_en = (x - x0) * (x - x0);
    double delta_x = 0.1 - 0.2 * rand_coordinate(generator);
    x += delta_x;
    double new_en = (x - x0) * (x - x0);
    inc_move_counter();

    if (!mc_scheme.test(old_en, new_en)) {
      undo();
      recent_energy = old_en;
      return false;
    } else {
      recent_energy = new_en;
      return true;
    }
  }

  /** @brief Back up the most recent move.
   * This method is declared abstract in Mover class and must be implemented here
   */
  inline void undo() {
    dec_move_counter();
    x -= delta_x;
  }

  /// Yet another method inherited from the base class
  virtual const std::string &name() const { return name_; }

  /// Does nothing, but must be implemented since it's been declared as virtual in the base class
  void max_move_range(const double max_range) {}

  /// Reads the maximum range for a move
  virtual double max_move_range() const { return 0.1; }

private:
  double delta_x;
  std::uniform_real_distribution<double> rand_coordinate;
  core::calc::statistics::Random &generator = core::calc::statistics::Random::get();
  static std::string name_;
};

std::string HarmonicSystemMover::name_ = "HarmonicSystemMover";

/** @brief A simple example shows how to use Monte Carlo simulated annealing.
 *
 * This demo also shows how to implement a simple mover and how to hook it up to sampling protocol.
 * CATEGORIES: simulations/generic/movers/Mover; simulations/generic/sampling/SimulatedAnnealing; simulations/generic/evaluators/EchoEvaluator;
 * CATEGORIES: simulations/generic/evaluators/EchoEvaluator; simulations/generic/evaluators/CallEvaluator
 * KEYWORDS:   Monte Carlo; Mover; sampling; simulated annealing; evaluators
 */
int main(const int argc, const char *argv[]) {

  using namespace simulations::evaluators;

  // ---------- Here we create a system to me modelled
  std::shared_ptr<HarmonicSystemMover> harmonic_ptr = std::make_shared<HarmonicSystemMover>();
  // --- You need a mover set to use SimulatedAnnealing protocol, even if the set contains just one mover
  simulations::movers::MoversSet_SP moves = std::make_shared<simulations::movers::MoversSetSweep>();
  moves->add_mover(harmonic_ptr,1);

  simulations::sampling::SimulatedAnnealing sa(moves,{2.0,1.5,1.0});

  // ---------- Create an observer which calls evaluators and writes the observations on the screen
  std::shared_ptr<simulations::observers::ObserveEvaluators> obs = std::make_shared<simulations::observers::ObserveEvaluators>("");
  // --- Create an evaluator which just return the X position of the particle
  // --- Add the evaluator to the observer. Obviously there might be may evaluators added to this observer;
  // --- then a nice table will be printed with a column corresponding to an evaluator.
  obs->add_evaluator(std::make_shared<EchoEvaluator<double>>(harmonic_ptr->x,"position X"));
  obs->add_evaluator(std::make_shared<EchoEvaluator<double>>(harmonic_ptr->recent_energy,"energy"));

  std::function<double(void)> get_temperature = [&sa]() { return sa.temperature(); };
  obs->add_evaluator(
    std::make_shared<CallEvaluator<std::function<double(void)>>>(get_temperature, "temperature"));

  sa.outer_cycle_observer(obs);
  sa.cycles(100,100);
  sa.run();
}

