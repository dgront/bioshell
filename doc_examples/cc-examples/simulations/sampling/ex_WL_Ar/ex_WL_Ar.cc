#include <iostream>
#include <thread>

#include <core/data/basic/Vec3Cubic.hh>

#include <utils/string_utils.hh>
#include <utils/options/OptionParser.hh>

#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/systems/BuildFluidSystem.hh>
#include <simulations/systems/SingleAtomType.hh>

#include <simulations/movers/TranslateAtom.hh>

#include <simulations/forcefields/cartesian/LJEnergySWHomogenic.hh>

#include <simulations/sampling/WangLandauSampler.hh>

#include <simulations/observers/ObserveEvaluators.hh>
#include <simulations/observers/cartesian/PdbObserver.hh>
#include <simulations/observers/ObserveWLSampling.hh>
#include <simulations/observers/AdjustMoversAcceptance.hh>
#include <simulations/observers/TriggerEveryN.hh>
#include <simulations/observers/cartesian/SimplePdbFormatter.hh>

#include <simulations/evaluators/CallEvaluator.hh>

using namespace core::data::basic;

utils::Logger logs("ex_WL_Ar");

std::string program_info = R"(

The program runs a Wang-Landau MC simulation of argon gas. By default it stars from a regular lattice conformation
unless an input file (PDB) with initial conformation is provided
USAGE:
    ex_WL_Ar n_atoms density temperature small_cycles big_cycles [max_jump]
    ex_WL_Ar starting.pdb density temperature small_cycles big_cycles [max_jump]

)";

const double EPSILON = 1.654E-21;	// [J] per molecule
const double EPSILON_BY_K = EPSILON / 1.381E-23; 	// = 119.6 in Kelvins
const double SIGMA = 3.4;		// in Angstroms

inline int bin_from_energy(double E)
{
    return (int)(E / 100);
}

/** @brief Isothermal Monte Carlo simulation of argon gas.
 *
 */
int main(const int argc,const char* argv[]) {

    using core::data::basic::Vec3Cubic;
    using namespace simulations::systems;
    using namespace simulations::movers; // for MoversSet
    using namespace simulations::observers::cartesian; // for all observers

    core::index4 n_outer_cycles = 1000;
    core::index4 n_inner_cycles = 1000;
    double density = 0.5;     // density of the system controls how many atoms will be contained in the box
    double temperature = 97;  // in Kelvins
    core::index4 n_atoms = 256;
    double max_jump = 0.5;		// Random move range (in Angstroms)

    core::data::structural::Structure_SP argon_structure = nullptr;
    core::data::structural::PdbAtom_SP ar_atom = std::make_shared<core::data::structural::PdbAtom>(1," AR");
    if (argc < 6) std::cerr << program_info;
    else {
        if (utils::is_integer(argv[1])) n_atoms = atoi(argv[1]);
        else { // --- read an input file if given
            core::data::io::Pdb reader(argv[1]);
            argon_structure = reader.create_structure(0);
            n_atoms = argon_structure->count_atoms();
        }
        density = atof(argv[2]);
        temperature = atof(argv[3]);
        n_inner_cycles = atoi(argv[4]);
        n_outer_cycles = atoi(argv[5]);
        if (argc == 7) max_jump = atof(argv[6]);
    }
    double ar_volume = 4.0 / 3.0 * M_PI * SIGMA * SIGMA * SIGMA * n_atoms;
    double box_len = pow(ar_volume / density, 0.33333333333333);

    // --- Initialize periodic boundary conditions
    core::data::basic::Vec3Cubic::set_box_len(box_len);
    logs << utils::LogLevel::INFO << "box width for " << int(n_atoms) << " atoms : " << box_len << "\n";

    // --- Create the system and distribute atoms in the box
    AtomTypingInterface_SP ar_type = std::make_shared<SingleAtomType>(" AR");
    CartesianAtoms ar(ar_type, n_atoms);
    core::calc::statistics::Random::seed(1234);

  if(argon_structure != nullptr) {        // --- read coordinates from a PDB file if provided
    set_conformation(argon_structure->first_const_atom(), argon_structure->last_const_atom(), ar);
  } else {                                // --- otherwise generate coordinates
    const auto grid = std::make_shared<SimpleCubicGrid>(box_len, n_atoms);
    BuildFluidSystem::generate(ar, *ar_atom, grid);
  }
  CartesianAtoms ar_backup(ar);           // --- make a backup system


  // --- Create energy function - just LJ potential
  simulations::forcefields::cartesian::LJEnergySWHomogenic lj_energy(ar, SIGMA, EPSILON_BY_K);

  // --- Create a mover, which is a random perturbation of an atom in this case, and place it in a movers' set
  std::shared_ptr<TranslateAtom> translate = std::make_shared<TranslateAtom>(ar, ar_backup, lj_energy);
  translate->max_move_range_allowed(1.5);
  MoversSet_SP movers = std::make_shared<simulations::movers::MoversSetSweep>();
  movers->add_mover(translate, n_atoms);
  translate->max_move_range(max_jump); // --- set the maximum distance a single atom can be moved by a single MC perturbation

    // --- create a Wang-Landau Monte Carlo sampler
    double initial_energy = lj_energy.energy(ar);
  logs << utils::LogLevel::INFO << "Initial energy of the system (used to limit WL sampling) : " << initial_energy << "\n";

  simulations::sampling::WangLandauSampler sampler(movers, initial_energy, bin_from_energy, initial_energy + 1);

    // ---------- Create an observer which calls energy calculation and prints it on the screen
    std::shared_ptr<simulations::observers::ObserveEvaluators> obs = std::make_shared<simulations::observers::ObserveEvaluators>("");
    std::function<double(void)> recent_energy = [&lj_energy,&ar]() { return lj_energy.energy(ar); };
  obs->add_evaluator(
      std::make_shared<simulations::evaluators::CallEvaluator<std::function<double(void)>>>(recent_energy, "energy", 8));

  std::shared_ptr<AbstractPdbFormatter> fmt = std::make_shared<SimplePdbFormatter>(" AR ", "AR ", "AR");
  auto observe_trajectory = std::make_shared<simulations::observers::cartesian::PdbObserver>(ar, fmt, "ar_tra.pdb");

    observe_trajectory->trigger(std::make_shared<simulations::observers::TriggerEveryN>(1));
    sampler.outer_cycle_observer(observe_trajectory); // --- commented out to save disk space

    std::shared_ptr<simulations::observers::AdjustMoversAcceptance> observe_moves
        = std::make_shared<simulations::observers::AdjustMoversAcceptance>(*movers,"movers.dat", 0.4);

    sampler.outer_cycle_observer(observe_moves);
    sampler.outer_cycle_observer(obs);
    sampler.cycle_size(1000);
    sampler.inner_cycles(n_inner_cycles);
    sampler.outer_cycles(n_outer_cycles);
    sampler.outer_cycle_observer(std::make_shared<simulations::observers::ObserveWLSampling>(sampler, "wl.dat"));

    sampler.run();

  simulations::observers::cartesian::PdbObserver final(ar, fmt, "final.pdb");
  final.observe();
  logs << utils::LogLevel::INFO << "Final energy " << lj_energy.energy(ar) << "\n";

}

