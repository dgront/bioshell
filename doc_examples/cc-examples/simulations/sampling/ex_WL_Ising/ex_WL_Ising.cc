#include <iostream>

#include <simulations/observers/ObserveWLSampling.hh>

#include <simulations/movers/ising/SingleFlip2D.hh>
#include <simulations/movers/ising/WolffMove2D.hh>

#include <simulations/observers/ObserveMoversAcceptance.hh>
#include <simulations/observers/TriggerEveryN.hh>

#include <simulations/sampling/WangLandauSampler.hh>
#include <simulations/systems/ising/Ising2D.hh>

using namespace core::data::basic;

utils::Logger logs("ex_WL_Ising");

std::string program_info = R"(

The program runs a Wang-Landau Monte Carlo simulation of a simple 2D Ising system (spin glass)

)";


/** @brief Turns energy of a system into an energy bin index (integer)
 * @param energy - system's energy
 * @return integer assigned to a bin; may be negative
 */
inline int bfe(double energy) { return (int) energy; }

/** @brief The program runs a Wang-Landau Monte Carlo simulation of a simple 2D Ising system (spin glass).
 *
 * This example shows how to set up a WL simulation
 *
 * CATEGORIES: simulations/sampling/WangLandauSampler; simulations/systems/ising/Ising2D
 * KEYWORDS:   observer; simulation
 */
int main(const int argc, const char *argv[]) {

  using namespace simulations::systems::ising;
  using namespace simulations::movers::ising;
  using namespace simulations::observers;

  core::index4 n_outer_cycles = 1;
  core::index4 n_inner_cycles = 10000;

  core::index2 system_size = 10;
  if (argc < 2) std::cerr << program_info;
  else {
    system_size = atoi(argv[1]);
    if (argc == 4) {
      n_inner_cycles = atoi(argv[2]);
      n_outer_cycles = atoi(argv[3]);
    }
  }

  core::calc::statistics::Random::get().seed(12345);  // --- seed the generator for repeatable results

  // ---------- Create the system to be sampled ----------
  std::shared_ptr<Ising2D<core::index1, core::index2>> system
      = std::make_shared<Ising2D<core::index1, core::index2>>(system_size, system_size);
  system->initialize();    // Populate system with random spins

  // ---------- Movers definition ----------
  simulations::movers::MoversSet_SP movers = std::make_shared<simulations::movers::MoversSetSweep>();
  movers->add_mover(std::make_shared<SingleFlip2D<core::index1, core::index2>>(*system), system->count_spins());
  movers->add_mover(std::make_shared<WolffMove2D<core::index1, core::index2>>(*system), system->count_spins() * 0.2);

  // ---------- Create the sampler ----------
  const double initial_energy = system->calculate();
  simulations::sampling::WangLandauSampler sampler(movers, initial_energy, bfe, 13);
  sampler.inner_cycles(n_inner_cycles);
  sampler.outer_cycles(n_outer_cycles);
  sampler.inner_cycle_observer(std::make_shared<ObserveWLSampling>(sampler, "wl.dat"));

  sampler.run();
}

