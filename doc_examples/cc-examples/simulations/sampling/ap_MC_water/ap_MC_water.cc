#include <iostream>
#include <vector>
#include <string>

#include <core/data/basic/Vec3I.hh>
#include <core/BioShellVersion.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/sampling_options.hh>

#include <simulations/systems/CartesianChains.hh>
#include <simulations/systems/BuildFluidSystem.hh>
#include <simulations/movers/RotateRigidMolecule.hh>
#include <simulations/movers/TranslateMolecule.hh>
#include <simulations/movers/MoversSetSweep.hh>
#include <simulations/forcefields/mm/Water3PointEnergy.hh>
#include <simulations/forcefields/mm/WaterModelParameters.hh>

#include <simulations/sampling/IsothermalMC.hh>
#include <simulations/observers/ObserveEvaluators.hh>
#include <simulations/observers/cartesian/PdbObserver.hh>
#include <simulations/observers/ObserveMoversAcceptance.hh>
#include <simulations/observers/TriggerEveryN.hh>
#include <simulations/observers/AdjustMoversAcceptance.hh>
#include <simulations/observers/cartesian/ExplicitPdbFormatter.hh>
#include <simulations/evaluators/CallEvaluator.hh>
#include <simulations/systems/SimpleAtomTyping.hh>


using namespace core::data::basic;

utils::Logger logs("ap_MC_water");

std::string program_info = R"(

The program runs an isothermal MC simulation of water. By default it starts from a regular lattice conformation
unless an input file (PDB) with initial conformation is provided
USAGE:
    ap_MC_water n_molecules temperature small_cycles big_cycles
    ap_MC_water starting.pdb temperature small_cycles big_cycles

)";

/** @brief Isothermal Monte Carlo simulation of water.
 *
 */
int main(const int argc,const char* argv[]) {

  using core::data::basic::Vec3Cubic;
  using namespace simulations::systems;
  using namespace simulations::movers; // for MoversSet
  using namespace simulations::observers::cartesian; // for all observers
  using simulations::forcefields::WaterModelParameters;

  logs << utils::LogLevel::INFO << "BioShell version:\n" << core::BioShellVersion().to_string() << "\n";

  core::index4 n_outer_cycles = 1000;
  core::index4 n_inner_cycles = 10;
  double temperature = 298;  // in Kelvins
  core::index4 n_molecules = 216; // 216
  core::calc::statistics::Random::seed(1234);

  double water_density = 0.99823;
  double water_mass = 18.01528;
  core::data::structural::Structure_SP water_structure = nullptr;

  if (argc < 5) std::cerr << program_info;
  else {
    if (utils::is_integer(argv[1])) n_molecules = atoi(argv[1]);
    else { // --- read an input file if given
      core::data::io::Pdb reader(argv[1]);
      water_structure = reader.create_structure(0);
      n_molecules = water_structure->count_residues();
    }
    temperature = atof(argv[2]);
    n_inner_cycles = atoi(argv[3]);
    n_outer_cycles = atoi(argv[4]);
  }
  double water_volume =  n_molecules * 10 * water_mass/6.02214;
  double box_len = pow(water_volume / water_density, 0.33333333333333);

  // --- Initialize periodic boundary conditions
  core::data::basic::Vec3I::set_box_len(box_len);
  logs << utils::LogLevel::INFO << "box width for " << int(n_molecules) << " molecules : " << box_len << "\n";

  WaterModelParameters::load_models();
  WaterModelParameters tip3p = WaterModelParameters::get_model("TIP3P");

  // --- Create water structure if not loaded from PDB
  if (water_structure == nullptr) {
    core::data::structural::Residue_SP hoh = tip3p.create_residue();
    core::data::structural::Residue_SP water_molecule = std::make_shared<core::data::structural::Residue>(1,"HOH");
    PointGridGenerator_SP grid = std::make_shared<SimpleCubicGrid>(box_len, n_molecules);
    water_structure = BuildFluidSystem::build_structure(*hoh, grid);
  }

//  SimpleAtomTyping tip3p_typing({"HOH"}, {"O", "H"}, {" O  ", " H  "});
  // --- Create the system to be sampled
  std::vector<std::string> res_types {"HOH"};
  std::vector<std::string> atom_types {"O", "H"};
  std::vector<std::string> pdb_types {" O  ", " H  "};
  AtomTypingInterface_SP tip3p_typing = std::make_shared<SimpleAtomTyping>(res_types, atom_types, pdb_types);
  CartesianChains system(tip3p_typing, *water_structure);
  CartesianChains backup(system);

  // --- Create energy function - TIP3P potential
  simulations::forcefields::mm::Water3PointEnergy en(tip3p);

  // --- Create a mover, which is a random perturbation of an atom in this case, and place it in a movers' set
  std::shared_ptr<RotateRigidMolecule> rot = std::make_shared<RotateRigidMolecule>(system, backup, en, 0);
  std::shared_ptr<TranslateMolecule> trs = std::make_shared<TranslateMolecule>(system, backup, en);
  MoversSet_SP movers = std::make_shared<simulations::movers::MoversSetSweep>();
  movers->add_mover(rot, n_molecules);
  movers->add_mover(trs, n_molecules);

  // --- create an isothermal Monte Carlo sampler
  simulations::sampling::IsothermalMC mc(movers,temperature);

  // ---------- Create an observer which calls energy calculation and prints it on the screen
  std::shared_ptr<simulations::observers::ObserveEvaluators> obs = std::make_shared<simulations::observers::ObserveEvaluators>("");
  // ---------- Create an observer which calls energy calculation and prints it to a file
//  std::shared_ptr<simulations::observers::ObserveEvaluators> obs = std::make_shared<simulations::observers::ObserveEvaluators>("energy.dat");
  std::function<double(void)> recent_energy = [&en, &system]() { return en.energy(system) / (system.count_residues() * 1000); };
  obs->add_evaluator(
    std::make_shared<simulations::evaluators::CallEvaluator<std::function<double(void)>>>(recent_energy, "energy", 8));

  std::shared_ptr<simulations::observers::AdjustMoversAcceptance> observe_moves
    = std::make_shared<simulations::observers::AdjustMoversAcceptance>(*movers,"movers.dat", 0.4);
  observe_moves->observe_header();

  std::shared_ptr<AbstractPdbFormatter> fmt = std::make_shared<ExplicitPdbFormatter>(*water_structure);
  auto observe_trajectory = std::make_shared<simulations::observers::cartesian::PdbObserver>(system, fmt, "water_tra.pdb");
//  observe_trajectory->trigger(std::make_shared<simulations::observers::TriggerEveryN>(10));
  mc.outer_cycle_observer(observe_trajectory); // --- commented out to save disk space
  mc.outer_cycle_observer(observe_moves);
  mc.outer_cycle_observer(obs);
  mc.cycles(n_inner_cycles,n_outer_cycles,1);

  mc.run();

  simulations::observers::cartesian::PdbObserver final(system, fmt, "final.pdb");
  final.observe();
//  logs << utils::LogLevel::INFO << "Final energy " << lj_energy.calculate() << "\n";
}

