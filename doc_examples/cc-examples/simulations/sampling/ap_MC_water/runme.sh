#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

mkdir -p outputs_from_test
echo -e "# Running ./ap_MC_water 256 298 10 100"
./ap_MC_water 64 298 10 10
mv final.pdb start.pdb

echo -e "# Running ./ap_MC_water start.pdb 298 10 100"
./ap_MC_water start.pdb 298 10 10
rm -rf water.pdb start.pdb
mkdir -p outputs_from_test
mv *.pdb *.dat outputs_from_test/
