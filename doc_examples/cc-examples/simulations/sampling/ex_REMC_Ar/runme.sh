#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

mkdir -p outputs_from_test/
echo -e "# Running ./ex_REMC_Ar 512 0.5 10 1 100 100 105 110"
./ex_REMC_Ar 512 0.5 10 1 100 100 105 110

# Comment the line below to keep also trajectories from each replica
rm ar_tra-?.pdb
mv *.pdb *.dat outputs_from_test/
