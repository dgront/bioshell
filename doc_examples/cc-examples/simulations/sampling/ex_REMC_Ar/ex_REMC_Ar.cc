#include <cstdio>
#include <ctime>
#include <iostream>

#include <core/data/basic/Vec3Cubic.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/sampling_options.hh>

#include <simulations/systems/CartesianAtomsSimple.hh>
#include <simulations/systems/BuildFluidSystem.hh>
#include <simulations/systems/SingleAtomType.hh>
#include <simulations/movers/TranslateAtom.hh>
#include <simulations/forcefields/cartesian/LJEnergySWHomogenic.hh>
#include <simulations/sampling/IsothermalMC.hh>
#include <simulations/observers/ObserveMoversAcceptance.hh>
#include <simulations/observers/TriggerEveryN.hh>
#include <simulations/observers/ObserveReplicaFlow.hh>
#include <simulations/observers/ObserveEnergyComponents.hh>
#include <simulations/observers/UpdateSystemTags.hh>
#include <simulations/observers/AdjustMoversAcceptance.hh>
#include <simulations/observers/cartesian/SimplePdbFormatter.hh>

#include <utils/exit.hh>

using namespace core::data::basic;

utils::Logger logs("ex_REMC_Ar");

std::string program_info = R"(

The program runs an Replica Exchange MC simulation of argon gas. By default it stars from a regular lattice conformation
unless an input file (PDB) with initial conformation is provided
USAGE:
    ex_REMC_Ar n_atoms density small_cycles big_cycles n_exchange temperature_1 temperature_2 [temperature_3 ...]
    ex_REMC_Ar starting.pdb density small_cycles big_cycles n_exchange temperature_1 temperature_2 [temperature_3 ...]

)";

const double EPSILON = 1.654E-21;	// [J] per molecule
const double EPSILON_BY_K = EPSILON / 1.381E-23; 	// = 119.6 in Kelvins
const double SIGMA = 3.4;		// in Angstroms

/** @brief A helper function that creates a dummy structure of 830 argon atoms.
 *
 * The structure is necessary to be able to save the system state in the PDB format. It will not be used
 * in the simulation - just for output formatting. The atoms may be stored in multiple chains, because PDB file
 * format allows a single chain to have at most 9999 atoms.
 * 
 * CATEGORIES: simulations::sampling::ReplicaExchangeMC
 * KEYWORDS:   Mover;Replica Exchange; Monte Carlo; sampling
 */
core::data::structural::Structure_SP create_argon_structure(const core::index4 n_ar) {

  using namespace core::data::structural;
  Structure_SP s = std::make_shared<Structure>("");
  core::index2 n_chains = n_ar / 9999;
  core::index4 n_atoms = 0;
  for (core::index2 ic = 0; ic < n_chains; ++ic) {
    Chain_SP chain = std::make_shared<Chain>(std::string{utils::letters[ic]});
    for (core::index4 i = 0; i < 9999; ++i) {
      Residue_SP res = std::make_shared<Residue>(i + 1, " AR");
      res->push_back(std::make_shared<PdbAtom>(i + 1, " AR ", core::chemical::AtomicElement::ARGON.z));
      chain->push_back(res);
      ++n_atoms;
    }
    s->push_back(chain);
  }
  if (n_atoms < n_ar) {
    Chain_SP chain = std::make_shared<Chain>(std::string{utils::letters[n_chains]});
    for (core::index4 i = n_atoms; i < n_ar; ++i) {
      Residue_SP res = std::make_shared<Residue>(i + 1 - n_atoms, " AR");
      res->push_back(std::make_shared<PdbAtom>(i + 1 - n_atoms, " AR ", core::chemical::AtomicElement::ARGON.z));
      chain->push_back(res);
    }
    s->push_back(chain);
  }

  return s;
}

/** @brief Isothermal Monte Carlo simulation of argon gas.
 *
 */
int main(const int argc,const char* argv[]) {

  using core::data::basic::Vec3Cubic;
  using namespace simulations::systems;
  using namespace simulations::observers::cartesian;
  using namespace simulations::forcefields; // for CalculateEnergyBase, NeighborList
  using namespace simulations::movers; // for MoversSet

  // ---------- Define some new types so the program is easier to read and lines get shorter
  typedef typename cartesian::LJEnergySWHomogenic LjEnergy;
  typedef typename simulations::observers::ObserveEnergyComponents<TotalEnergy> EnergyObserverType;
  typedef std::shared_ptr<EnergyObserverType> EnergyObserverType_SP;

  core::index4 n_atoms;

  if (argc < 8) utils::exit_OK_with_message(program_info);

  std::vector<core::data::structural::Structure_SP> argon_structures;
  bool input_from_file = false;
  if (utils::is_integer(argv[1])) {
    n_atoms = atoi(argv[1]);
    argon_structures.push_back(create_argon_structure(n_atoms));
  }
  else { // --- read an input file if given
    core::data::io::Pdb reader(argv[1]);
    for(core::index2 i_str=0;i_str<reader.count_models();++i_str)
      argon_structures.push_back(reader.create_structure(i_str));
    n_atoms = argon_structures[0]->count_atoms();
    input_from_file = true;
  }

  double density = atof(argv[2]);
  core::index4 n_inner_cycles = atoi(argv[3]);
  core::index4 n_outer_cycles = atoi(argv[4]);
  core::index4 n_exchanges = atoi(argv[5]);
  double ar_volume = 4.0 / 3.0 * M_PI * SIGMA * SIGMA * SIGMA * n_atoms;
  double box_len = pow(ar_volume / density, 0.33333333333333);
  core::calc::statistics::Random::seed(1234);

  // --- Initialize periodic boundary conditions for the box length
  core::data::basic::Vec3Cubic::set_box_len(box_len);
  logs << utils::LogLevel::INFO << "box width for "<<int(n_atoms)<<" atoms : " << box_len << "\n";

  std::vector<double> temperatures;
  for (int i = 6; i < argc; ++i) temperatures.push_back(atof(argv[i]));

  AtomTypingInterface_SP ar_type = std::make_shared<SingleAtomType>(" AR");
  std::vector<std::shared_ptr<CartesianAtomsSimple<Vec3Cubic>>> systems;
  core::data::structural::PdbAtom_SP ar_atom = std::make_shared<core::data::structural::PdbAtom>(1," AR");

  std::shared_ptr<AbstractPdbFormatter> fmt = std::make_shared<SimplePdbFormatter>(" AR ", "AR ", "AR");

  std::vector<simulations::sampling::IsothermalMC_SP> replica_samplers;
  std::vector<CalculateEnergyBase_SP> energies;
  std::vector<simulations::observers::ObserverInterface_SP> mover_adjusters;
  for (core::index2 irepl = 0; irepl < temperatures.size(); ++irepl) {

    // ---------- Create the systems to be sampled ----------
    CartesianAtoms ar(ar_type, n_atoms);
    systems.push_back(ar);

    // ---------- Distribute atoms in the box or use coordinates from provided PDB file
    if(input_from_file) {
      const auto strctr = argon_structures[irepl % argon_structures.size()];
      core::index2 ia=0;
      for(auto a_it = strctr->first_const_atom();a_it !=strctr->last_const_atom();++a_it) {
        (*ar)[ia].set(**a_it);
        ++ia;
      }
    }
    else
      BuildFluidSystem<Vec3Cubic>::generate(*ar, *ar_atom, n_atoms);

    // ---------- Create energy function - just LJ potential
    std::shared_ptr<NeighborList_OBSOLETE<Vec3Cubic>> nbl = std::make_shared<NeighborList_OBSOLETE<Vec3Cubic>>(*ar, 9.0, 3.0);
    std::shared_ptr<LjEnergy> lj_energy_term = std::make_shared<LjEnergy>(*ar, *nbl, SIGMA, EPSILON_BY_K);
    std::shared_ptr<TotalEnergy_OBSOLETE<ByAtomEnergy_OBSOLETE>> lj_energy = std::make_shared<TotalEnergy_OBSOLETE<ByAtomEnergy_OBSOLETE>>();
    lj_energy->add_component(lj_energy_term,1.0);
    energies.push_back(std::static_pointer_cast<class CalculateEnergyBase>(lj_energy));

    // --- Create a mover, which is a random perturbation of an atom in this case, and place it in a movers' set
    std::shared_ptr<TranslateAtom<Vec3Cubic>> translate = std::make_shared<TranslateAtom<Vec3Cubic>>(*ar, *lj_energy_term);
    MoversSet_SP movers = std::make_shared<simulations::movers::MoversSetSweep>();
    movers->add_mover(translate, n_atoms);
    translate->max_move_range(0.5); // --- set the maximum distance a single atom can be moved by a single MC perturbation

    // --- Create an observer to record and adjust movers acceptance rate
    simulations::observers::AdjustMoversAcceptance_SP adj = std::make_shared<simulations::observers::AdjustMoversAcceptance>(
      *movers, utils::string_format("movers-%d.dat", irepl), 0.4);
    mover_adjusters.push_back(adj);
    adj->observe_header();

    // ---------- create an isothermal Monte Carlo sampler
    auto  mc = std::make_shared<simulations::sampling::IsothermalMC>(movers,temperatures[irepl]);

    // ---------- Create an observer for energy components
    EnergyObserverType_SP obs_en
      = std::make_shared<EnergyObserverType>(*lj_energy,utils::string_format("energy-%d.dat",irepl));
    obs_en->observe_header();

    // ---------- Create an observer for trajectory in PDB format
    auto observe_trajectory = std::make_shared<simulations::observers::cartesian::PdbObserver_OBSOLETE<Vec3Cubic>>(
      *ar, fmt, utils::string_format("ar_tra-%d.pdb", irepl));

    observe_trajectory->trigger(std::make_shared<simulations::observers::TriggerEveryN>(10));
    mc->outer_cycle_observer(observe_trajectory);
    mc->outer_cycle_observer(obs_en);
    mc->cycles(n_inner_cycles,n_outer_cycles,1);
    replica_samplers.push_back(mc);
  }

  bool replica_isothermal_observation_mode = true;
  simulations::sampling::ReplicaExchangeMC remc(replica_samplers, energies, replica_isothermal_observation_mode);
  auto remc_flow = std::make_shared<simulations::observers::ObserveReplicaFlow>(remc,"replica_flow.dat");
  remc.exchange_observer(remc_flow);
  auto tag_updater = std::make_shared<simulations::observers::UpdateSystemTags<CartesianAtomsSimple<Vec3Cubic>>>(systems,remc);
  tag_updater->observe();
  remc.exchange_observer(tag_updater);

  remc.replica_exchanges(n_exchanges);
  for (auto o : mover_adjusters) remc.exchange_observer(o);
  remc.run();

  simulations::observers::cartesian::PdbObserver_OBSOLETE<Vec3Cubic> final(*systems[0], fmt, "final.pdb");
  final.observe();
  for (core::index2 i_system = 1; i_system < systems.size(); ++i_system) {
    for (core::index4 i_atom = 0; i_atom < systems[0]->n_atoms; ++i_atom)
      systems[0]->operator[](i_atom) = systems[i_system]->operator[](i_atom);
    final.observe();
  }
  final.finalize();
}

