#!/bin/bash 

export BIOSHELL_DATA_DIR="../../../../../../data" #../../../../../data"

# Here we tabulate R13 potential for ALA(helical)-HIS(helical) in CABS model
echo -e 'Running ./ex_MeanFieldDistributions "forcefield/cabs/R13_cabs.dat"  0.02 2.0 15.0 AD.HH'
./ex_MeanFieldDistributions "forcefield/cabs/R13_cabs.dat"  0.02 2.0 15.0 AD.HH
