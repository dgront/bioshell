#include <string>

#include <simulations/forcefields/mf/MeanFieldDistributions.hh>

#include <utils/exit.hh>

std::string program_info = R"(

Prints values for a given Mean Field potential so it can be plotted nicely. The parameters of the program are:
MF potental file name, pseudocounts, min_x, max_x, potential name [other potential names ..]
USAGE:
    ex_MeanFieldDistributions  "forcefield/cabs/R13_cabs.dat"  0.01 2.0 15.0 AD.HH
(note that apostrophes may be mandatory, otherwise bash will not pass the arguments correctly)
)";

/** @brief Prints values for a given Mean Field potential so it can be plotted nicely.
 *
 * The program works for any potential stored in the Bioshell row-wise format; both CABS and SURPASS potentials may be
 * plotted with this utility. Program usage:
 *
 * ex_MeanFieldDistributions  "forcefield/cabs/R13_cabs.dat"  0.01 2.0 15.0 AD.HH
 *
 * where the parameters of the program are:
 *   MF potental file name, pseudocounts, min_x, max_x, potential name [other potential names ..]
 *
 * CATEGORIES: simulations/forcefields/mf/MeanFieldDistributions
 * KEYWORDS:   force field
 */
int main(const int argc, const char *argv[]) {

  using namespace simulations::forcefields::mf;

  if(argc < 5) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  double pseudocounts = utils::from_string<double>(argv[2]);
  double min_x = utils::from_string<double>(argv[3]);
  double max_x = utils::from_string<double>(argv[4]);

  std::shared_ptr<MeanFieldDistributions> mf = load_1D_distributions(argv[1], pseudocounts);
  std::vector<EnergyComponent_SP> terms;
  if (argc > 5)
    for (int i = 5; i < argc; ++i) {
      std::string label(argv[i]);
      if(mf->contains_distribution(label)) terms.push_back(mf->at(label));
      else std::cerr <<"Key "<<label<<" not found!\n";
    }
  else
    for (const std::string label:mf->known_distributions())
        terms.push_back(mf->at(label));

  for (double d = min_x; d <= max_x; d += 0.0125) {
    std::cout << utils::string_format("%7.3f",d);
    for (const auto e : terms) std::cout << utils::string_format(" %8.3f", (*e)(d));
    std::cout << "\n";
  }
}
