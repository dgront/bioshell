#include <iostream>
#include <vector>

#include <core/data/io/Pdb.hh>
#include <core/BioShellEnvironment.hh>
#include <simulations/forcefields/mm/MMForceField.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Reads a PDB file and assigns MM atom typing for every atom of the given protein according to the given force field
parametrisation file. If no .par file was given, AMBER03 force field will be used.

USAGE:
    ./ex_MMAtomTyping [param-file] input.pdb
EXAMPLE:
    ./ex_MMAtomTyping 2gb1.pdb
    ./ex_MMAtomTyping amber03_atoms.par 2gb1.pdb

)";

/** @brief Assigns MM atom typing for every atom of the given protein according to the given force field parametrisation file
 *
 * CATEGORIES: simulations/forcefields/mm/MMAtomTyping
 * KEYWORDS:   PDB input; atom typing; force field
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace simulations::forcefields::mm;
  MMForceField mmff("AMBER03");
  const MMAtomTyping & atom_typing = *(mmff.mm_atom_typing());
  core::data::io::Pdb pdb((argc == 2) ? argv[1] : argv[2]);
  core::data::structural::Structure_SP s = pdb.create_structure(0);
  for (auto chain: *s) {
    for (core::index2 ires = 0; ires < chain->size(); ++ires) {
      for (auto atom : *(*chain)[ires]) {
        core::index2 t = atom_typing.atom_type(*atom);
        std::cout << *(*atom).owner() << " " << (*atom).atom_name() << " : " << t << " " << atom_typing.atom_internal_name(t) << "\n";
      }
    }
  }
}
