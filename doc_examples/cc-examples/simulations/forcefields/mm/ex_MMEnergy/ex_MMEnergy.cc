#include <string>
#include <chrono>

#include <core/data/basic/Vec3.hh>

#include <utils/string_utils.hh>
#include <utils/LogManager.hh>

#include <simulations/forcefields/mm/MMNonBonded.hh>
#include <simulations/forcefields/mm/MMBondType.hh>
#include <simulations/forcefields/mm/MMBondEnergy.hh>
#include <simulations/forcefields/mm/MMPlanarEnergy.hh>
#include <simulations/forcefields/mm/MMDihedralEnergy.hh>
#include <simulations/forcefields/mm/MMBondedParameters.hh>
#include <simulations/forcefields/mm/MMForceField.hh>

int main(const int argc, const char *argv[]) {

  using namespace core::data::structural; // for Structure, Residue, PdbAtom
  using namespace core::data::basic; // for Vec3
  using namespace simulations::forcefields::mm; // for all MM - related force field classes
  using namespace simulations::systems; // for ResidueChain

  utils::LogManager::get().set_level("INFO");

  if (argc < 2) {
    std::cerr << "USAGE:\n\t./ex_MMEnergy atoms.par ff_bonded.par 2gb1.pdb\n\n";
    exit(0);
  }

  // --- Read atom typing and bond parameters
    MMForceField mmff("AMBER03");
    simulations::forcefields::mm::MMBondedParameters ff(argv[2],mmff.mm_atom_typing());

  // --- Read structure for which calculate bond energy
  core::data::io::Pdb reader(argv[3]); // file name (PDB format, may be gzip-ped)
  Structure_SP strctr = reader.create_structure(0);
  auto rc = CartesianChains(mmff.mm_atom_typing(), *strctr);

  size_t i = 0;
//  for (auto atom_it = strctr->first_const_atom(); atom_it != strctr->last_const_atom(); ++atom_it) {
//    auto at = atom_typing->atom_type(**atom_it);
//    (*rc)[i].register_ = (1.0 + at.charge()) * 10000.0;
//    ++i;
//  }

  // --- Create molecular mechanic bond energy object
  simulations::forcefields::mm::MMBondEnergy bond_energy(rc, ff);
  // --- Create molecular mechanic planar energy objects
  simulations::forcefields::mm::MMPlanarEnergy planar_energy(rc, ff, bond_energy);
  // --- Create molecular mechanic dihedral energy objects
  simulations::forcefields::mm::MMDihedralEnergy dihedral_energy(rc, ff, bond_energy);

  // --- Create non-bonded energy; pass the reference to bond energy object so non-bonded energy can exclude bonds
  MMNonBonded nb_energy(rc, bond_energy);
  //  nb_energy.get_excluded_pairs().print(std::cout);

  auto start = std::chrono::high_resolution_clock::now();
   std::cout<<"#bond_energy planar_energy dihedral_energy  nb_energy\n";
  std::cout << utils::string_format("  %10.3f    %10.3f      %10.3f %10.3f\n",
    bond_energy.energy(rc), planar_energy.energy(rc), dihedral_energy.energy(rc), nb_energy.energy(rc));
  auto end = std::chrono::high_resolution_clock::now();
  std::cerr << "# computed in " << std::chrono::duration<double>(end - start).count() << " [s]\n";
}
