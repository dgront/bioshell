#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../../data"

echo "Running: ./ex_MMNonBonded test_inputs/amber03_atoms.par test_inputs/amber03_ffbonded.itp test_inputs/2gb1.pdb"
./ex_MMNonBonded test_inputs/amber03_atoms.par test_inputs/amber03_ffbonded.itp test_inputs/2gb1.pdb
