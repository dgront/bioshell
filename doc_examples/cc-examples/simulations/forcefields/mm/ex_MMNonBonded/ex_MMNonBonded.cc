#include <iostream>
#include <string>
#include <vector>
#include <tuple>

#include <utils/string_utils.hh>
#include <utils/LogManager.hh>
#include <core/data/structural/Structure.hh>
#include <simulations/forcefields/mm/MMBondType.hh>
#include <simulations/forcefields/mm/MMBondEnergy.hh>
#include <simulations/forcefields/mm/MMNonBondedSW.hh>
#include <simulations/forcefields/mm/MMNonBonded.hh>
#include <simulations/forcefields/mm/MMForceField.hh>

/** \todo_code Fix this demo one MM topology files parser is ready
 *
 */
int main(const int argc, const char *argv[]) {

  using namespace simulations::forcefields::mm; // for all MM - related force field classes
  using namespace core::data::structural; // for Structure, Residue, PdbAtom
  using namespace core::data::basic; // for Vec3
  using namespace simulations::systems; // for ResidueChain

  utils::LogManager::get().set_level("FINE");

  if (argc < 2) {
    std::cerr << "USAGE:\n\t./ex_MMNonBondEnergy data_atom_typing data_bond_typing input.pdb\n\n";
    exit(0);
  }

  // --- Read atom typing and bond parameters
    MMForceField mmff("AMBER03");
  MMBondedParameters bonded_manager(argv[2],mmff.mm_atom_typing());


  // --- Read structure for which calculate bond energy
  core::data::io::Pdb reader(argv[3]); // file name (PDB format, may be gzip-ped)
  Structure_SP strctr = reader.create_structure(0);
  auto rc = CartesianChains(mmff.mm_atom_typing(), *strctr);

  // --- Create molecular mechanic bond energy object
  MMBondEnergy bond_energy(rc, bonded_manager);

  // --- Create non-bonded energy; pass the reference to bond energy object so non-bonded energy can exclude bonds
  MMNonBonded nb_energy(rc,bond_energy);
  std::cout<<"# list of atoms pair excluded from pairwise calculation information\n";
 // nb_energy.get_neighbor_list().print(std::cout);

  // --- Calculate total non-bonded energy of the structure
  double energy_by_str = nb_energy.energy(rc);
  std::cout << utils::string_format("%10.3f\n", energy_by_str);
}
