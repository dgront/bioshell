#include <iostream>

#include <core/data/basic/Vec3.hh>
#include <core/data/structural/Structure.hh>
#include <utils/LogManager.hh>

#include <simulations/forcefields/mm/MMBondEnergy.hh>
#include <simulations/forcefields/mm/MMBondedParameters.hh>
#include <simulations/forcefields/mm/MMForceField.hh>

int main(const int argc, const char *argv[]) {

  using namespace simulations::forcefields::mm; // for all MM - related force field classes
  using namespace core::data::structural; // for Structure, Residue, PdbAtom
  using namespace core::data::basic; // for Vec3

  utils::LogManager::get().set_level("FINE");

  if (argc < 2) {
    std::cerr << "USAGE:\n\t./ex_MMBondEnergy atom_typing ff_bonded.par input.pdb\n\n";
    exit(0);
  }

  // --- Create molecular mechanic bond energy object
    MMForceField mmff("AMBER03");

    MMBondedParameters bond_manager(argv[2],mmff.mm_atom_typing());

  // --- Read structure for which calculate bond energy
  core::data::io::Pdb reader(argv[3]); // file name (PDB format, may be gzip-ped)
  core::data::structural::Structure_SP strctr = reader.create_structure(0);
  auto rc = CartesianChains(mmff.mm_atom_typing(), *strctr);

  // --- Create molecular mechanic bond energy object
  simulations::forcefields::mm::MMBondEnergy bond_energy(rc,bond_manager);
  const auto & bonds = bond_energy.get_bonds();
    std::cout << "#   i     j      E(d0)      d0    d\n";

  // --- Calculate energy for each bond in the structure
  double total = 0.0;
  for (auto it = bonds.cbegin(); it != bonds.cend(); ++it) {
    double en = bond_energy.calculate(*it);
    total += en;
    double dreal = (rc)[it->i].distance_to((rc)[it->j]);
    std::cout << utils::string_format("%5d %5d %10.3f    %4.2f %4.2f\n", (*it).i, (*it).j, en,(*it).d0,dreal);
  }
  // --- Calculate total bond energy by whole structure
  double total_en = bond_energy.energy(rc);

  std::cout << utils::string_format("%10.3f %10.3f\n", total, total_en);
}

