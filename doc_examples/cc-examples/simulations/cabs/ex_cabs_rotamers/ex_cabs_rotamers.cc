#include <iostream>

#include <core/chemical/ChiAnglesDefinition.hh>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>
#include <core/data/structural/selectors/SelectClashingResidues.hh>
#include <core/data/structural/selectors/SelectContiguousResidues.hh>

#include <core/calc/structural/angles.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/transformations/CartesianToSpherical.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>

#include <utils/io_utils.hh>
#include <utils/LogManager.hh>

utils::Logger logger("ex_cabs_rotamers");

int main(const int argc, const char* argv[]) {

  using namespace core::chemical;
  using namespace core::data::structural;
  using namespace core::calc::structural;
  using namespace core::calc::structural::transformations;

  utils::LogManager::get().set_level("FINE");

  if(argc==1) {
    return 0;
  }
  core::data::io::Pdb reader(argv[1],
      core::data::io::all_true(core::data::io::is_not_alternative,core::data::io::is_not_hydrogen),
      core::data::io::keep_all, false);  // Create a PDB reader for a given file
  core::data::structural::Structure_SP str = reader.create_structure(0); // Create a structure object from the first model

  // ---------- Selector we use to be sure that the residue is correct
  selectors::IsAA is_aa;
  selectors::IsBBCB atom_is_bb_cb;
  selectors::ResidueHasBBCB has_bb_cb;
  selectors::ResidueHasAllHeavyAtoms all_atoms;
  selectors::SelectChainBreaks breaks;
  selectors::SelectClashingResidues clash_test(str,2.3);
  selectors::SelectContiguousResidues check_resids;

  std::cout << "#   r     alpha   theta   alpha   theta      x      y      z  ss res_id aa chain prot rotamer chi angles\n";
  CartesianToSpherical acs;
  std::vector<std::string> lcs_atom_names = {" N  ", " CA ", " C  "};
  auto ires = str->first_residue(); ++ires;
  auto next_res = str->first_residue(); ++(++next_res);
  for (; next_res != str->last_residue(); ++ires,++next_res) { // iterate over all residues staring from the second one

    if (!is_aa(**ires)) continue;
    if (((**ires).residue_type() == Monomer::GLY) || ((**ires).residue_type() == Monomer::ALA)) continue;

    if(!has_bb_cb(**ires)) {
      logger << utils::LogLevel::WARNING << "Residue "<<(**ires) << " has incomplete backbone or lacks its CB atom\n";
      continue;
    }

    if(!all_atoms(**ires)) {
      logger << utils::LogLevel::WARNING << "Residue side chain "<<(**ires) << " is incomplete\n";
      continue;
    };

    if(!check_resids(**ires)) {
      logger << utils::LogLevel::WARNING << "Residue "<<(**ires) << " is broken\n";
      continue;
    };

    if(breaks(**ires)) {
      logger << utils::LogLevel::WARNING << "Residue "<<(**ires) << " is at chain break\n";
      continue;
    };

    if(clash_test(**ires)) {
      logger << utils::LogLevel::WARNING << "Residue "<<(**ires) << " is in a steric clash\n";
      continue;
    };

    PdbAtom_SP CA = (*ires)->find_atom_safe(" CA ");
    PdbAtom_SP N = (*ires)->find_atom_safe(" N  ");
    PdbAtom_SP C = (*ires)->find_atom_safe(" C  ");
    PdbAtom_SP CB = (*ires)->find_atom_safe(" CB ");
    double t = core::calc::structural::evaluate_dihedral_angle(*N, *C, *CB, *CA) * 180.0 / 3.14159;
    if ((t < -50) || (t > -20)) {
      logger << utils::LogLevel::WARNING << "Residue " << (**ires) << " has incorrect geometry at CA. Dihedral angle: " << t << "\n";
      continue;
    };

    core::data::basic::Vec3 cm;
    double n = 0;
    std::for_each((*ires)->cbegin(),(*ires)->cend(),[&](const PdbAtom_SP a){ if(!atom_is_bb_cb(*a)) { cm+=(*a); ++n;} });

    if (n == 0) continue;

    cm /= n;
    Rototranslation_SP lcs = local_coordinates_three_atoms(**ires, lcs_atom_names);
    Vec3 sph;
    lcs->apply(cm);
    acs.apply(cm, sph);
    std::cout << utils::string_format("%7.4f %7.2f %7.2f %7.4f %7.4f   %6.3f %6.3f %6.3f ",
      sph.x, to_degrees(sph.y), to_degrees(sph.z), sph.y, sph.z, cm.x, cm.y, cm.z);

    std::cout << utils::string_format("%c %6d %3s %s %s %4s",
      (*ires)->ss(), (*ires)->id(), (*ires)->residue_type().code3.c_str(), (*ires)->owner()->id().c_str(),
      utils::basename(str->code()).c_str(),
      core::calc::structural::define_rotamer(**ires).c_str());
    for (unsigned short i = 1; i <= core::chemical::ChiAnglesDefinition::count_chi_angles((*ires)->residue_type()); ++i)
      std::cout << utils::string_format(" %6.1f", core::calc::structural::evaluate_chi(**ires, i) * 180.0 / 3.1415);
    std::cout << "\n";
  }
}
