#include <iostream>

#include <core/data/io/Pdb.hh>
#include <utils/exit.hh>

#include <simulations/representations/cabs/cabs_utils.hh>

using namespace core::data::structural;
using namespace core::data::io;

/** @brief Reads an all-atom structure from a PDB file and produces a structure in CABS-BB representation.
 */
std::string program_info = R"(

Converts all-atom protein structure to CABS-bb representation
USAGE:
    ex_cabsbb_representation input.pdb
USAGE:
    ex_cabsbb_representation 2gb1.pdb

)";

/** @brief Converts all-atom protein structure to CABS-bb representation
 *
 *
 * CATEGORIES: simulations/representations/cabs/cabs_utils;
 * KEYWORDS:   PDB input; CABS-bb
 */int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  // --- Read the input PDB and create a structure object
  core::data::io::Pdb reader(argv[1], all_true(is_not_hydrogen,is_not_water,is_not_alternative), keep_all, true);
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

  // --- Check whether loaded structure is in the CABSBB representation
  if (simulations::representations::is_cabsbb_model(*strctr))
    std::cerr << "Loaded structure of " << argv[1] << " has CABSBB representation! Load fullatom model.\n";
  else if (simulations::representations::is_cabs_model(*strctr))
    std::cerr << "Loaded structure of " << argv[1] << " has CABS representation! Load fullatom model.\n";

  else {
    // --- Convert the Structure into CABSBB representation and write the result in the PDB format
    core::data::structural::Structure_SP structure_sp = simulations::representations::cabsbb_representation(*strctr);
    for (auto atom_sp = structure_sp->first_atom(); atom_sp != structure_sp->last_atom(); ++atom_sp)
      std::cout << (*atom_sp)->to_pdb_line() << "\n";

    // --- Here we generate CONNECT lines so the PDB file displays nicely in PyMOL
    for (auto it = structure_sp->first_const_residue(); it != structure_sp->last_const_residue(); ++it) {
      if ((*it)->count_atoms() == 6) { // --- if this CABSBB residue has 6 atoms, it must have the SC atom
        auto cb = *((*it)->cbegin() + 4); // --- CB is always the fifth one
        auto sc = *((*it)->cbegin() + 5); // --- SC is always the sixth one
        core::data::io::Conect cn(cb->id(),sc->id());
        std::cout << cn.to_pdb_line();
      }
    }
  }
}
