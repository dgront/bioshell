#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

mkdir -p outputs_from_test

for i in 2gb1 1rrx 5l3z 5lpc
do
  echo -e "# Running ./ex_cabsbb_representation test_inputs/"$i".pdb"
  ./ex_cabsbb_representation test_inputs/$i.pdb > outputs_from_test/$i-cabsbb.pdb
done


