#include <iostream>
#include <iomanip>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <simulations/representations/cabs/cabs_utils.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which reads an all-atom structure from a PDB file and produces a structure in CABS representation.

USAGE:
    ./ex_cabs_representation input.pdb

EXAMPLE:
    ./ex_cabs_representation 2gb1.pdb

REFERENCE:
Kolinski, Andrzej. "Protein modeling and structure prediction with a reduced representation."
Acta Biochimica Polonica 51 (2004).

Kmiecik, Sebastian, et al. "Coarse-grained protein models and their applications."
Chemical reviews 116.14 (2016): 7898-7936. doi: 10.1021/acs.chemrev.6b00163

)";

using namespace core::data::structural;
using namespace core::data::io;

/** @brief Reads an all-atom structure from a PDB file and produces a structure in CABS representation.
 *
 * CATEGORIES: simulations::representations::cabs::cabs_utils
 * KEYWORDS:   PDB input; CABS; representation
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  // --- Read the input PDB and create a structure object
  core::data::io::Pdb reader(argv[1], is_not_alternative, only_ss_from_header, true);
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

  // --- Check whether loaded structure is in the CABS representation
  if (simulations::representations::is_cabs_model(*strctr))
    std::cerr<<"Loaded structure of "<<argv[1]<<" has CABS representation! Load all-atom model.\n";
  else if (simulations::representations::is_cabsbb_model(*strctr)) {
    std::cerr<<"Loaded structure of "<<argv[1]<<" has CABSBB representation! Load fullatom model.\n";
  }
  else {
  // --- Convert the Structure into CABS representation and write the result in the PDB format
    Structure_SP structure_sp = simulations::representations::cabs_representation(*strctr);
    for (auto atom_sp = structure_sp->first_atom(); atom_sp != structure_sp->last_atom(); ++atom_sp)
      std::cout << (*atom_sp)->to_pdb_line() << "\n";

    // --- Here we generate CONNECT lines so the PDB file displays nicely in PyMOL
    auto prev_ca_sp = *structure_sp->first_atom(); // --- the very first CA in the structure
    for (auto res_sp_it = (++(structure_sp->first_residue())); res_sp_it != structure_sp->last_residue(); ++res_sp_it) {
      auto ca = *((*res_sp_it)->cbegin()); // --- iterator to the CA of this residue
      core::data::io::Conect cn(prev_ca_sp->id(), ca->id());
      std::cout << cn.to_pdb_line();
      if ((*res_sp_it)->count_atoms() < 2) { // --- it has also CB
        auto cb = *((*res_sp_it)->cbegin() + 2); // --- CB is always the third one
        core::data::io::Conect cn(ca->id(), cb->id());
        std::cout << cn.to_pdb_line();
        if ((*res_sp_it)->count_atoms() < 2) { // --- it has also CB
          auto sc = *((*res_sp_it)->cbegin() + 3); // --- SC is always the fourth one
          core::data::io::Conect cn(cb->id(), sc->id());
          std::cout << cn.to_pdb_line();
        }
      }
      prev_ca_sp = ca;
    }
  }
}
