#include <iostream>
#include <fstream>
#include <simulations/systems/ising/Ising2D.hh>
#include <simulations/movers/ising/SingleFlip2D.hh>
#include <simulations/movers/ising/WolffMove2D.hh>

#include <simulations/observers/ObserveEvaluators.hh>
#include <simulations/movers/MoversSetSweep.hh>
#include <simulations/sampling/SimulatedAnnealing.hh>


/* @brief Simple but fully functional Ising simulating program.
 *
 * Runs simulated annealing simulations for 100x100
 * 
 * CATEGORIES: simulations::systems::ising::Ising2D
 * KEYWORDS:   Mover;Simulated Annealing;Ising2D
 */
using namespace simulations::systems::ising;
using namespace simulations::movers::ising;

int main(const int argc, const char *argv[]) {
  /* Simulation controlling variables */
  int n_cols = 10, n_rows = 10;                    // size of system

  /* Other settings necessary for the simulation */
  int seed = 12345;                          // seed for rng
  core::calc::statistics::Random::seed(seed);

  /* Initializing the system */
  std::shared_ptr<Ising2D<core::index1,core::index2>> system = std::make_shared<Ising2D<core::index1,core::index2>>(n_rows, n_cols);
  system->initialize();    // Populate system with random spins

  simulations::movers::MoversSet_SP movers = std::make_shared<simulations::movers::MoversSetSweep>();
  movers->add_mover( std::make_shared<SingleFlip2D<core::index1,core::index2>>(*system),system->count_spins());
  movers->add_mover( std::make_shared<WolffMove2D<core::index1,core::index2>>(*system),system->count_spins()*0.2);

  std::vector<float> temperatures = {5,4,3,2.5,2.25,2,1.75,1.5,1};
  simulations::sampling::SimulatedAnnealing sa(movers,temperatures);
  sa.cycles(100,100);

  simulations::observers::ObserveEvaluators_SP observations = std::make_shared<simulations::observers::ObserveEvaluators>("");
  observations->add_evaluator(system);
  sa.outer_cycle_observer(observations);

  sa.run();
  observations->finalize();
}





