#include <iostream>

#include <core/data/basic/Vec3.hh>

#include <simulations/movers/movers_utils.hh>

using namespace core::data::basic;
using namespace simulations;

/** @brief Simple test shows that random_vector_on_sphere() really produces a unifirm distribution
 *
 * CATEGORIES: simulations/movers/random_vector_on_sphere;
 */
int main(int argc, char *argv[]) {

  core::data::basic::Vec3 v;
  core::data::basic::Vec3 s;
  core::index4 N = 100000;
  for (size_t i = 0; i < N; ++i) {
    simulations::movers::random_vector_on_sphere(v);
    s += v;
    std::cout << v << "\n";
  }
  s /= N;
  std::cout << "# sum: " << s << "\n";
}

