#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

mkdir -p outputs_from_test

echo -e "# Running ./ap_LigandTossingMover test_inputs/2kwi.pdb GNP 5"
./ap_LigandTossingMover test_inputs/2kwi.pdb GNP 5

mv out.pdb outputs_from_test/
