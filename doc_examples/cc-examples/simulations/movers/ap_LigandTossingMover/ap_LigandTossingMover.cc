#include <core/data/basic/Vec3.hh>
#include <core/data/io/Pdb.hh>
#include <simulations/movers/LigandTossingMover.hh>
#include <simulations/forcefields/ConstEnergy.hh>
#include <simulations/systems/PdbAtomTyping.hh>
#include <simulations/observers/cartesian/PdbObserver_OBSOLETE.hh>
#include <simulations/observers/cartesian/ExplicitPdbFormatter_OBSOLETE.hh>
#include <simulations/sampling/AlwaysAccept.hh>

using namespace core::data::basic;
using namespace simulations;

#include <utils/exit.hh>

std::string program_info = R"(

The program creates a multimodel PDB with random orientations of a ligand in respect to the protein.

USAGE:
    ap_LigandTossingMover 2kwi.pdb GNP [30]

where 2kwi.pdb is the name of the input PDB file, GNP is the code of the ligand (must be in the same PDB file)
and 30 is the number of random conformations generated (optional argument)

)";

/** @brief To test LigandTossingMover mover, tosses a ligand on a proteins surface
 *
 * The program creates a multimodel PDB with random orientations of a ligand in respect to the protein
 *
 * CATEGORIES: simulations::movers::LigandTossingMover
 * KEYWORDS:   docking; Mover
 * IMG: ap_LigandTossingMover.png
 * IMG_ALT: 25 conformations of the same ligand randomly placed on the surface of a protein
 */
int main(int argc, char *argv[]) {

  using namespace simulations::systems; // for AtomTypingInterface and ResidueChain
  using namespace simulations::observers::cartesian;
  using namespace core::data::io; // for Pdb reader
  using core::data::basic::Vec3;

  if(argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  // --- Read the input PDB and create a structure object
//  Pdb reader(argv[1], all_true(is_not_hydrogen, is_not_water, is_not_alternative), true);
  Pdb reader(argv[1]);
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

  // --- Prepare a modeled system from a given PDB file
  ResidueChain_OBSOLETE<Vec3> system(*strctr);

  std::string ligand_code(argv[2]);
  int from = -1, to = -1, i = 0;
  for (auto atom_it = strctr->first_atom(); atom_it != strctr->last_atom(); ++atom_it) {
    if (ligand_code.size()==3) { // truly it's a ligand code
      if (((*atom_it)->owner()->residue_type().code3 == ligand_code) && (from == -1)) from = i;
      if (((*atom_it)->owner()->residue_type().code3 != ligand_code) && (to == -1) && (from != -1)) to = i - 1;
    } else { // it should be a chain code then
        if (((*atom_it)->owner()->owner()->id() == ligand_code) && (from == -1)) from = i;
        if (((*atom_it)->owner()->owner()->id() != ligand_code) && (to == -1) && (from != -1)) to = i - 1;
    }
    ++i;
  }
  if (to == -1) to = i - 1; // --- assign the last atom if nothing has been assigned yet
  AtomRange moving(from,to);
  std::cout << "Moving atoms: " << moving << "\n";

  core::index4 n_moves = (argc==4) ? atoi(argv[3]) : 10;
  simulations::movers::LigandTossingMover<Vec3> mover(system, moving, 5.0);

  simulations::sampling::AlwaysAccept alwaysMove;
  std::shared_ptr<AbstractPdbFormatter_OBSOLETE<Vec3>> fmt = std::make_shared<ExplicitPdbFormatter_OBSOLETE<Vec3>>(*strctr);

  simulations::observers::cartesian::PdbObserver_OBSOLETE<Vec3> trajectory(system, fmt, "out.pdb");
  for (size_t i = 0; i < n_moves; i++) {
    mover.move(alwaysMove);
    trajectory.observe();
  }
}

