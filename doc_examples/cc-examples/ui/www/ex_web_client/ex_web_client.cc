#include <iostream>
#include <ui/www/web_client.hh>
#include <utils/exit.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Simple test for web_client methods  downloads 2GB1 protein from rcsb.org website
USAGE:
    ex_web_client

)";

/** @brief Simple test for web_client methods downloads 2GB1 protein from rcsb.org website
 *
 * CATEGORIES: ui/www/web_client
 * KEYWORDS:   WWW
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace ui::www;

  http_t *request = http_get("http://files.rcsb.org/view/2GB1.pdb", NULL);
  if (!request) {
    std::cerr << "Invalid request.\n";
    return 1;
  }

  http_status_t status = HTTP_STATUS_PENDING;
  int prev_size = -1;
  while (status == HTTP_STATUS_PENDING) {
    status = http_process(request);
    if (prev_size != (int) request->response_size) {
      std::cout << utils::string_format("%d byte(s) received.\n", (int) request->response_size);
      prev_size = (int) request->response_size;
    }
  }

  if (status == HTTP_STATUS_FAILED) {
    std::cerr << utils::string_format("HTTP request failed (%d): %s.\n", request->status_code, request->reason_phrase);
    http_release(request);
    return 1;
  }

  std::cout << "\nContent type: " << request->content_type << "\n\n" << (char const *) request->response_data << "\n";
  http_release(request);

  return 0;
}
