#include <iostream>

#include <utils/exit.hh>
#include <utils/io_utils.hh>

std::string program_info = R"(

Simple app downloads a requested pdb file from RCSB website; it expects a four-letter PDB code of the deposit
USAGE:
    ap_download_pdb PDB_code
EXAMPLE:
    ap_download_pdb 2gb1

)";

/** @brief Simple app downloads a pdb file from RCSB website
 *
 * CATEGORIES: utils/read_properties_file
 * KEYWORDS:   PDB file; download
 * GROUP:      File processing
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  std::ofstream out(std::string(argv[1])+".pdb");
  out << utils::download_pdb(argv[1]);
  out.close();
}
