#include <iostream>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/LogManager.hh>

using namespace utils::options;

/** @brief Shows how to use BioShell command line parser in your own program
 *
 * To test this program, run:
 * ./ex_OptionParser -n=4 -nn=1,2,3,4
 *
 * CATEGORIES: utils::options::OptionParser
 * KEYWORDS:   option parsing
 */
int main(const int cnt, const char *argv[]) {

  // --- Limit the stdout on stderr (logging)
  utils::LogManager::WARNING();

  // --- First get th parser instance (it's a singleton)
  utils::options::OptionParser &cmd = OptionParser::get("ex_OptionParser");

  // --- This is how to register an option that has already been declared in BioShell library
  cmd.register_option(utils::options::verbose, help);

  // --- User can also declare  non-standard options
  Option number("-n", "-number", "returns an integer");
  Option numbers("-nn", "-numbers", "returns a vector of integers");
  Option value("-x", "-value_x", "returns a real value of X");
  // --- Options that have beed declared, must also be registered
  // --- (Declaration doesn't mean automatic registration)
  cmd.register_option(number, numbers, value);

  // --- after all the relevant options were registered, we parse a program command line
  cmd.parse_cmdline(cnt, argv);

  // ---- User should not check for -help and -verbose flags : this is automatically done by OptionParser

  // --- Once command line has been parsed, we may check for a program parameter and retrieve its value:
  if (numbers.was_used()) std::cout << "A number given: " << option_value<int>(number) << "\n";

  // --- Options may be also accessed by their long name (but not by the abbreviated name, so cmd.was_used("-x") won't work
  if (cmd.was_used("-value_x")) std::cout << "A number given: " << option_value<double>("-value_x") << "\n";

  // --- This shows how to read a vector of values
  if(cmd.was_used(numbers)) {
    std::vector<int> v = option_value<std::vector<int>, int>(numbers);
    std::cout << "Given set of values: ";
    for (auto vi : v) std::cout << vi << " ";
    std::cout << "\n";
    // --- This is how the raw string given at the command line may be accessed:
    std::cout << "The raw string associated with -value_x option was: " << cmd.value_string(numbers) << "\n";
  }
}

