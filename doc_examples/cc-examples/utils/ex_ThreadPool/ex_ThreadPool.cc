#include <iostream>

#include <utils/ThreadPool.hh>
#include <utils/string_utils.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to use a ThreadPool class.

USAGE:
./ex_ThreadPool

)";

/// Operator called by each thread (pretends to run very time consuming calculations)
struct Op { std::string operator()(int i) { return "Hello " + utils::to_string(i); } };

/** @brief Simple test for a ThreadPool class
 *
 * CATEGORIES: utils/ThreadPool
 * KEYWORDS:  concurrency; multi-threading
 */
int main(const int cnt, const char *argv[]) {

  if ((cnt > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  utils::ThreadPool pool(4); // --- Create a pool of four threads = four jobs can be executed at a time

  typedef typename std::result_of<Op(int)>::type return_type;
  Op o;
  std::vector<std::future<return_type>> futures;

  // --- Here we start 10 jobs which will be executed by four workers (threads)
  for (int i = 0; i < 10; ++i) futures.push_back(pool.enqueue(o, i));

  for (int i = 0; i < 10; ++i)
    std::cout << std::string("Hello " + utils::to_string(i)) << " " << futures[i].get() << "\n";
}
