#include <iostream>

#include <utils/io_utils.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Simple test for ex_read_properties_file function reads a file given from command line.
The program expects a file in JAVA's .properties file format

USAGE:
    ex_read_properties_file input_file.properties

REFERENCE:
https://en.wikipedia.org/wiki/.properties

)";

/** @brief Simple test reads .properties file and prints these settings on the screen
 *
 * CATEGORIES: utils/read_properties_file
 * KEYWORDS:   file utils;properties file
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  for (int i = 1; i < argc; ++i) {
    // In the single line below we read the properties file
    auto mapa = utils::read_properties_file(argv[i]);

    // Here we print the content of the map in the same format (i.e. .properties)
    for (auto it = mapa.cbegin(); it != mapa.cend(); ++it) {
      std::cout << it->first << " : ";
      for (auto it2 = it->second.cbegin(); it2 != it->second.cend(); ++it2)
        std::cout << (*it2) << " ";
      std::cout << "\n";
    }
  }
}
