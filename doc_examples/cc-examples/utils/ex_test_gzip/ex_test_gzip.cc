#include <sstream>

#include <utils/io_utils.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which gzips and un-gzips a string data.

USAGE:
./ex_test_gzip

)";

// --- The input data to be compressed
std::string ala_cif_data =
    R"(data_ALA
# 
_chem_comp.id                                    ALA 
_chem_comp.name                                  ALANINE 
_chem_comp.type                                  "L-PEPTIDE LINKING" 
_chem_comp.pdbx_type                             ATOMP 
_chem_comp.formula                               "C3 H7 N O2" 
_chem_comp.mon_nstd_parent_comp_id               ? 
_chem_comp.pdbx_synonyms                         ? 
_chem_comp.pdbx_formal_charge                    0 
_chem_comp.pdbx_initial_date                     1999-07-08 
_chem_comp.pdbx_modified_date                    2011-06-04 
_chem_comp.pdbx_ambiguous_flag                   N 
_chem_comp.pdbx_release_status                   REL 
_chem_comp.pdbx_replaced_by                      ? 
_chem_comp.pdbx_replaces                         ? 
_chem_comp.formula_weight                        89.093 
_chem_comp.one_letter_code                       A 
_chem_comp.three_letter_code                     ALA 
_chem_comp.pdbx_model_coordinates_details        ? 
_chem_comp.pdbx_model_coordinates_missing_flag   N 
_chem_comp.pdbx_ideal_coordinates_details        ? 
_chem_comp.pdbx_ideal_coordinates_missing_flag   N 
_chem_comp.pdbx_model_coordinates_db_code        ? 
_chem_comp.pdbx_subcomponent_list                ? 
_chem_comp.pdbx_processing_site                  RCSB 
 )";

/** @brief Simple test to gzip and un-gzip  a string data
 *
 * CATEGORIES: utils/io_utils
 * KEYWORDS:   GZIP
 */
int main(int cnt, char* argv[]) {

  if ((cnt > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  std::string zipped,result;
  // --- here we compress ala_cif_data string with ZIP and store the result in another string
  utils::zip_string(ala_cif_data,zipped);
  // --- here we un-zip it back and store the ouput in the string "result"
  utils::unzip_string(zipped,result);
  if (result == ala_cif_data) {
    std::cout << "GZIP OK :-)\n";
    std::cout << "compressed from " << ala_cif_data.size() << " to " << zipped.size() << " bytes\n";
  } else std::cout << "GZIP ERROR !!!\n";

  // --- Here we un-zip directly to a stream
  std::stringstream ss;
  utils::unzip_string(zipped,ss);

  if (ss.str() == ala_cif_data)
    std::cout << "GZIP OK :-)\n";
  else std::cout << "GZIP ERROR !!!\n";
}
