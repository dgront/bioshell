#include <iostream>

#include <core/calc/numeric/Quaternion.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/io/Pdb.hh>
#include <core/calc/statistics/Random.hh>

/** @brief ex_Quaternion illustrates how to use Quaternion class
 *
 * CATEGORIES: core::calc::numeric::Quaternion
 * KEYWORDS: algebra
 */
int main(const int argc, const char* argv[]) {

  using namespace core::calc::numeric;
  using namespace core::data::basic; // --- for Vec3

  Quaternion p, rot;
  core::calc::statistics::Random::seed(0);
  rot.random();       // --- Random rotation axis
  //rot = Quaternion::create_from_axis_angle(1,0,0,M_PI/3.0);
  
  // ---------- Read a structure to be rotated
  core::data::io::Pdb reader(argv[1],core::data::io::is_not_alternative, core::data::io::only_ss_from_header, true);
  const auto strctr_sp = reader.create_structure(0);
  
  // ---------- Iterate over atoms and rotate them one by one
  for(auto a_it = strctr_sp->first_atom(); a_it != strctr_sp->last_atom(); ++a_it) {
    // --- use quaternion rotation method
    p.i = (**a_it).x;
    p.j = (**a_it).y;
    p.k = (**a_it).z;
    p.rotate_by(rot);
    std::cout << p.i<<" "<<p.j<<" "<<p.k <<"\n";

    // --- and now the same, but with apply() method - output should be numerically the same
    rot.apply(**a_it);
    std::cout << (**a_it).to_pdb_line() << "\n";
  }

  return 0;
}


