#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

mkdir -p outputs_from_test

echo -e "#Running ./ex_Quaternion  test_inputs/2gb1.pdb"
./ex_Quaternion test_inputs/2gb1.pdb

./strc -in:pdb=test_inputs/2kwi.pdb -select:chains=B -out:pdb=peptide.pdb
echo "" > peptide_rot.pdb
for i in `seq 1 1 10`
do
  echo "MODEL      "$i >> peptide_rot.pdb
  ./ex_Quaternion peptide.pdb >> peptide_rot.pdb
  echo "ENDMDL" >> peptide_rot.pdb
done

mv peptide.pdb peptide_rot.pdb outputs_from_test/
