#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/calc/structural/ProteinArchitecture.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Detects secondary structure using BioShell's implementation of the DSSP algorithm.
USAGE:
    ap_dssp input.pdb

EXAMPLE:
    ap_dssp 5edw.pdb

REFERENCE:
Kabsch, Wolfgang, and Christian Sander. "Dictionary of protein secondary structure: pattern recognition
of hydrogen‐bonded and geometrical features." Biopolymers 22 (1983): 2577-2637. doi:10.1002/bip.360221211

)";
/** @brief DSSP implementation
 *
 * CATEGORIES: core::calc::structural::ProteinArchitecture;
 * KEYWORDS:   PDB input; Hydrogen bonds; secondary structure; DSSP; Protein structure features
 * GROUP: Structure calculations;
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  core::data::io::Pdb reader(argv[1]); // file name (PDB format, may be gzip-ped)
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

  core::calc::structural::ProteinArchitecture pa(*strctr);
  std::cout << pa.hec_string() << "\n";
}
