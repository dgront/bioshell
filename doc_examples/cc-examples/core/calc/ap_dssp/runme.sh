#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./dssp test_inputs/2gb1.pdb"
./ap_dssp test_inputs/2gb1.pdb
echo -e "Running ./dssp test_inputs/5edw.pdb"
./ap_dssp test_inputs/5edw.pdb
echo -e "Running ./dssp test_inputs/3dcg.pdb"
./ap_dssp test_inputs/3dcg.pdb
