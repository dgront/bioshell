#include <iostream>

#include <core/index.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/basic/Vec3I.hh>
#include <core/calc/statistics/Histogram.hh>

#include <utils/exit.hh>

std::string program_info = R"(

ap_calc_rdf calculates Radial Distribution Function (RDF) over a trajectory

If a multi-model PDB file was given, the program combines the data from all models

USAGE:
    ap_calc_rdf  trajectory.pdb HOH O box_side

where trajectory.pdb is the input file multimodel-PDB file, HOH and O defines the atom in a  molecules for which
the RDF will be evaluated

)";

/** @brief Calculates  Radial Distribution Function
 *
 * CATEGORIES: core::data::basic::Vec3
 * KEYWORDS: PDB input; simulation
 * GROUP: Structure calculations;
 */
int main(const int argc, const char *argv[]) {

  if (argc < 4) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  double L = utils::from_string<double>(argv[4]); // The third parameter is the box width (in Angstroms)
  core::data::basic::Vec3I::set_box_len(L);
  core::data::io::Pdb reader(argv[1]); // --- file name (PDB format, may be gzip-ped)

  core::index4 n_atoms = reader.count_atoms(0);
  core::index4 n_frmes = reader.count_models();

  std::vector<core::data::basic::Vec3I> frame_i(n_atoms);

  core::calc::statistics::Histogram<double, core::index4> h(0.01, 0, L/4.0);
  // ---------- Load coordinates to memory and accumulate RDF ----------
  for (int i_start = 0; i_start < n_frmes; ++i_start) {
    reader.fill_structure(i_start, frame_i);
    for(core::index4 i=1;i<n_atoms;++i) {
      for(core::index4 j=0;j<i;++j) {
        double d = frame_i[i].distance_to(frame_i[j]);
        h.insert(d);
      }
    }
  }

  double norm = 4 * M_PI * n_atoms / pow(L, 3.0) * n_frmes;
  for(core::index4 i=0;i<h.count_bins();++i) {
    std::cout << h.bin_middle_val(i) << " " << h.get_bin(i) / (norm * h.bin_middle_val(i) * h.bin_middle_val(i)) << " "
              << h.get_bin(i) << "\n";
  }
  // ---------- Calculate displacement ----------
}