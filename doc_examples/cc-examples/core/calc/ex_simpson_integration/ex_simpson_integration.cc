#include <math.h>

#include <iostream>

#include <core/calc/numeric/numerical_integration.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test for Simpson numerical integration routine.

USAGE:
    ex_simpson_integration

REFERENCE:
W. H. Press, S. A. Teukolsky, W. T. Vetterling, and B. P. Flannery (1992)
Numerical Recipes in C: The Art of Scientific Computing, Cambridge Univ. Press,
)";

/// First of the two functions integrated in this example
struct Sin {

  double operator()(double x) { return sin(x); }
} sin_func;

/// Second of the two functions integrated in this example
struct X2 {

  double operator()(double x) { return x*x; }
} x_square_func;

/** @brief Example for numerical integration with Simpson method
 *
 * CATEGORIES: core/calc/numeric/simpson_integration
 * KEYWORDS: numerical methods
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  std::cout << core::calc::numeric::simpson_integration(sin_func, 0, M_PI,1000) << "\n";
  std::cout << core::calc::numeric::simpson_integration(x_square_func, 0.0, 1.0,1000) << "\n";
}


