#include <iostream>

#include <iostream>
#include <core/data/io/Pdb.hh>
#include <core/calc/structural/local_backbone_geometry.hh>
#include <utils/exit.hh>
#include <core/data/structural/ResidueSegmentProvider.hh>
#include <core/data/structural/selectors/ResidueSegmentSelector.hh>
#include <core/data/structural/selectors/SelectPlanarCAGeometry.hh>
#include <core/protocols/selection_protocols.hh>

std::string program_info = R"(

Calculates Phi,Psi angles (Ramachandran map) for every model found in the input protein structure
USAGE:
    ex_evaluate_phi_psi input.pdb [chain-id]

EXAMPLE:
    ex_evaluate_phi_psi 2kwi.pdb B

)";

/** @brief Calculates Phi,Psi angles (Ramachandran map) for the input protein structure
 *
 * CATEGORIES: core::calc::structural::LocalBackboneProperties
 * KEYWORDS:   PDB input; structural properties; structure validation
 * IMG: phi_psi_scatterplot.png
 * IMG_ALT: Phi,Psi values plotted for every residue of 2KWI PDB deposit; radius of a circle denotes standard deviation calculated from the NMR ensemble
 */
int main(const int argc, const char *argv[]) {

  using namespace core::calc::structural;
  using namespace core::data::structural;
  using namespace core::data::io;

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  core::data::io::Pdb reader(argv[1], keep_all, keep_all, true);  // Create a PDB reader for a given file
  selectors::HasProperlyConnectedCA is_connected;
  core::data::structural::selectors::ResidueHasAllHeavyAtoms check_atoms;
  core::data::structural::selectors::SelectPlanarCAGeometry if_flat;
  Phi phi(1);
  Psi psi(1);
  for(int i=0;i<reader.count_models();++i) {
    core::data::structural::Structure_SP str = reader.create_structure(i);
    if (argc==3) {
      core::data::structural::selectors::ChainSelector pick_chain(argv[2]);
      core::protocols::keep_selected_chains(pick_chain, *str);
    }
    ResidueSegmentProvider rsp(str, 3);
    while (rsp.has_next()) {
      const ResidueSegment_SP seg = rsp.next();
      if (is_connected(*seg)) {
        for(int i=0;i<3;++i) {
          if (if_flat((*seg)[i])) break;
          if (!check_atoms((*seg)[i])) break;
        }
        const auto &res = *(*seg)[1];
        std::cout << utils::string_format("%4s %s %4d %3s  ", str->code().c_str(), res.owner()->id().c_str(), res.id(), res.residue_type().code3.c_str());
        std::cout << seg->sequence()->sequence << " " << seg->sequence()->str()<< " ";
        std::cout << utils::string_format(phi.format(), phi(*seg)) << " ";
        std::cout << utils::string_format(psi.format(), psi(*seg)) << "\n";
      }
    }
  }
}
