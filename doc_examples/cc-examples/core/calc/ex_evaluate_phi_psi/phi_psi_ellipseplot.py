#!/usr/bin/env python3

# A script that plots Chi-1 vs CHi2 scatterplot

from sys import argv, stderr
from math import sqrt
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcl
from matplotlib.patches import Ellipse

COLUMN_RES = 0 # Residue number
COLUMN_CHN = 2 # Chain id
COLUMN_PHI = 3 # Phi angle
COLUMN_PSI = 4 # Psi angle

phi = {}
psi = {}
phi2 = {}
psi2 = {}
cnt = {}
for line in open(argv[1]) :
  if len(line) > 1 and line[0] != '#' : 
    tokens = line.strip().split()
    if len(tokens) > COLUMN_PSI :
      try :
        c1 = float(tokens[COLUMN_PHI])
        c2 = float(tokens[COLUMN_PSI])
        cn = tokens[COLUMN_CHN]
        rn = tokens[COLUMN_RES]
        key = cn+rn
        if key not in phi :
          phi[key] = 0.0
          psi[key] = 0.0
          phi2[key] = 0.0
          psi2[key] = 0.0
          cnt[key] = 1
        phi[key] += c1
        psi[key] += c2
        phi2[key] += c1*c1
        psi2[key] += c2*c2
        cnt[key] += 1
      except :
        print("Can't parse line: "+line,file=stderr)
      
plt.xlim(-180,180)
plt.ylim(-180,180)
plt.xticks(np.arange(-180,200,45))
plt.yticks(np.arange(-180,200,45))
plt.xlabel(r"$\Phi$ angle")
plt.ylabel(r"$\Psi$ angle")
ax = plt.gca()
ax.set_aspect('equal', adjustable='box')  # square
plt.grid()

ellipse_data = []
for key in cnt :
  ellipse_x = phi[key] / cnt[key] # average phi
  ellipse_y = psi[key] / cnt[key] # average psi
  ellipse_vx = phi2[key] / cnt[key] - ellipse_x*ellipse_x # variance of Phi
  ellipse_vy = psi2[key] / cnt[key] - ellipse_y*ellipse_y # variance of Psi
  ellipse_data.append( (ellipse_x, ellipse_y,sqrt(ellipse_vx), sqrt(ellipse_vy),sqrt(ellipse_vx)*sqrt(ellipse_vy)) )
  
ellipse_data.sort(key=lambda x: x[4],reverse=True)
  
for e in ellipse_data:
  ellipse = Ellipse(xy=(e[0],e[1]), width=e[2], height=e[3], edgecolor='darkslateblue', fc='slateblue', alpha=0.3, lw=0.5)
  ax.add_patch(ellipse)
plt.title(r'Fluctuations of $\Phi,\Psi$ angles from %d structures' % (cnt[key]), fontsize=10)
plt.savefig('phi_psi_scatterplot.png', dpi=400)
