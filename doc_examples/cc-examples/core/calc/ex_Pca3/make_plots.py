#!/usr/bin/env python

import sys, re
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

INFILE = sys.argv[1] if (len(sys.argv) == 2) else "expected_outputs/stdout.out"

x_before = []
y_before = []
z_before = []
x_after  = []
y_after  = []
z_after  = []

for line in open(INFILE) :
  tokens = re.split("\s+",line.strip())
  if len(tokens)==6 : 
    x_before.append(float(tokens[0]) )
    y_before.append(float(tokens[1]) )
    z_before.append(float(tokens[2]) )
    x_after.append( float(tokens[3]) )
    y_after.append( float(tokens[4]) )
    z_after.append( float(tokens[5]) )
    
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x_before, y_before, z_before, marker="s", c="black")
ax.scatter(x_after, y_after, z_after, marker="o", c="r")
plt.xticks([-2.0, -1.0, 0, 1.0, 2.0])
plt.yticks([-1.0, 0,1.0, 2.0, 3.0])
ax.set_zticks([-0.4, 0.0, 0.4, 0.8, 1.2])
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
plt.tight_layout()
fig.savefig('scatter_3d.png', dpi=600)
plt.close(fig)
