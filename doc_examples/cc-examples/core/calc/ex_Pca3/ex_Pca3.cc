#include <iostream>
#include <random>

#include <core/index.hh>
#include <core/calc/numeric/basic_algebra.hh>
#include <core/calc/numeric/Pca3.hh>
#include <utils/exit.hh>
#include <utils/options/OptionParser.hh>

std::string program_info = R"(

Unit test orients 3D points along the axes using the PCA algorithm.

USAGE:
./ex_Pca3

REFERENCE:
Pearson, Karl. "On lines and planes of closest fit to systems of points in space."
Philosophical Magazine 2 (1901): 559-572. doi:10.1080/14786440109462720.

)";

/** @brief Orients 3D points along the axes using PCA algorithm
 *
 * CATEGORIES: core/calc/numeric/basic_algebra.hh
 * KEYWORDS: PCA; transformations
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::data::basic; // --- for Vec3 and Array2D

  std::mt19937 gen;
  std::uniform_real_distribution<double> r(0, 1);
  std::vector<Vec3> points3d;
  for (core::index2 i = 0; i < 100; ++i) {
    double x = r(gen), y = r(gen), z = r(gen);
    points3d.emplace_back(x + 0.3 * y + 0.6 * z, 0.4 * x + 1.9 * y + 0.7 * z, 0.3 * x + 0.1 * y + 0.7 * z);
  }
  core::calc::numeric::Pca3 pca3(points3d);
  auto rt = pca3.create_transformation();
  for (auto &p:points3d) {
    std::cout << p << " ";
    rt.apply(p);
    std::cout << p << "\n";
  }
}

