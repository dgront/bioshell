#include <iostream>
#include <map>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <utils/exit.hh>

std::string program_info = R"(

ex_protein_peptide_interface finds  atomic contacts between a receptor and a peptide found in an input PDB file.
The peptide is defined as a protein chain shorter than 35 residues, while the receptor must consist
of at least 40 amino acids.

Output provides: protein residue name and ID, protein chain ID, peptide protein name and ID,
peptide chain ID, minimum distance between the residues, e.g.:

ILE   36 A  ARG  104 X 5.92977
LEU   44 A  ARG  104 X 5.92685
LEU   44 A  LEU  108 X 5.57779
GLU   45 A  THR  102 X 6.81994

USAGE:
    ex_protein_peptide_interface file.pdb  cutoff-distance

EXAMPLE:
    ex_protein_peptide_interface 1dt7.pdb  7.0

where 1dt7.pdb id an input file and 7.0 - contact distance in Angstroms.

)";

unsigned int MAX_PEPTIDE_LENGTH = 35;
unsigned int MIN_PROTEIN_LENGTH = 40;

/** @brief Finds contacts atomic contacts between a receptor and a peptide.
 *  *
 * CATEGORIES: core::data::structural::Structure
 * KEYWORDS: PDB input; contact map; peptide; STL
 */
int main(const int argc, const char* argv[]) {

  if (argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io;
  Pdb reader(argv[1], all_true(is_not_water,is_not_alternative,is_not_hydrogen)); // --- file name (PDB format, may be gzip-ped)

  core::data::structural::Structure_SP strctr = reader.create_structure(0);
  core::data::structural::selectors::IsAA is_aa_tester;
  core::data::structural::Structure_SP sub_strctr = strctr;// = strctr->clone(is_aa_tester);

  double cutoff = utils::from_string<double>(argv[2]); // The second parameter is the contact distance (in Angstroms)

  for (auto protein_chain_sp: *strctr) { // --- protein_chain_sp is a shared pointer to a chain
    if (protein_chain_sp->size() < MIN_PROTEIN_LENGTH) continue;
    for (auto i_residue_sp: *protein_chain_sp) { // --- i_residue_sp is a shared pointer to a residue
      for (auto peptide_chain_sp: *strctr) {
        if (peptide_chain_sp->size() > MAX_PEPTIDE_LENGTH) continue;
        for (auto j_residue_sp: *peptide_chain_sp) {
          double d = (i_residue_sp)->min_distance(j_residue_sp);
          if (d < cutoff)
            std::cout << (*i_residue_sp) << " " << (*i_residue_sp).owner()->id() << " " << (*j_residue_sp) << " "
                      << (*j_residue_sp).owner()->id() << " " << d << "\n";
        }
      }
    }
  }
}
