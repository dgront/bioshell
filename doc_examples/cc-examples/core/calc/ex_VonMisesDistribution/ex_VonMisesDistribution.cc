#include <iostream>
#include <random>

#include <core/calc/statistics/VonMisesDistribution.hh>
#include <core/calc/statistics/Random.hh>

std::string program_info = R"(

ex_VonMisesDistribution withdraws N random values (by default N = 1000) from a Normal distribution
and fits Von Mises distribution to the data.

If exactly two arguments are provided (mu and kappa, respectively) the program tabulates Von Mises distribution
for that parameters.
USAGE:
    ex_VonMisesDistribution N
    ex_VonMisesDistribution mu kappa

EXAMPLES:
    ex_VonMisesDistribution 10000
    ex_VonMisesDistribution 1.5708 100.0

)";

/** @brief Example which estimates parameters of von Mises distribution and tabulates its values
 * CATEGORIES: core/calc/statistics/VonMisesDistribution
 * KEYWORDS: statistics
 * IMG: von_mises.png
 * IMG_ALT: Example von Mises distribution: histogram of a sample, pdf(x) and cdf(x)
 */
int main(const int argc, const char *argv[]) {

  using namespace core::calc::statistics;

  core::calc::statistics::VonMisesDistribution f(std::vector<double>{0.0, 1.0}); // --- initial distribution
  if (argc == 3) {
    double mu = atof(argv[1]);
    double kappa = atof(argv[2]);
    f.copy_parameters_from(std::vector<double>{mu, kappa});
    std::cout << "# tabulating VonMisesDistribution: " << f << "\n";
    for (double x = -M_PI; x <= M_PI; x += M_PI / 25.0)
      std::cout
          << utils::string_format("%6.3f %9f %5.3f\n", x, f.evaluate(x),
                                  VonMisesDistribution::cdf(x, f.mu(), f.kappa()));
    return 0;
  }

  core::index4 N = 1000;
  if (argc < 2) std::cerr << program_info;
  else N = atoi(argv[1]);
  std::vector<std::vector<double>> input_data;
  Random r = Random::get();
  r.seed(9876543);
  std::normal_distribution<double> dist(M_PI / 2.0, 0.1);

  // ---------- prepare data that will be use to estimate the distribution
  for (core::index4 i = 0; i < N; ++i) {
    std::vector<double> v({dist(r)});
    input_data.push_back(v);
  }
  f.estimate(input_data); // --- run the estimation
  std::cout << f << "\n";
  // ---------- now prepare weighted data: just copy some points ten times and insert them with weight 0.1
  input_data.clear();
  std::vector<double> weights;
  for (core::index4 i = 0; i < N; ++i) {
    double x = dist(r);
    std::vector<double> v({x});
    if (x < 2) {
      input_data.push_back(v);
      weights.push_back(1.0);
    } else {
      for (int j = 0; j < 10; ++j) {
        input_data.push_back(v);
        weights.push_back(0.1);
      }
    }
  }
  f.estimate(input_data, weights); // --- run the estimation based on weighted observations
  std::cout << f << "\n";
}
