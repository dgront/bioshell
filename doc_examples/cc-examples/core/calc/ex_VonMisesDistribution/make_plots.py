import sys
import matplotlib.pyplot as plt

angle = []
pdf = []
cdf = []
random_values = []
for line in open(sys.argv[1]) :
  if line[0] == '#' : continue
  values = line.strip().split()
  if len(values) == 1 : random_values.append( float(values[0]) )
  else :
    angle.append( float(values[0]) )
    pdf.append( float(values[1]) )
    cdf.append( float(values[2]) )
  
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.hist(random_values, bins=30, facecolor='#124653', alpha=0.5, edgecolor=None, normed=1, label = "sample")
    
ax1.plot(angle, pdf, label = "pdf(x)")
ax1.plot(angle, cdf, label = "cdf(x)")
ax1.legend()
ax1.set_xticks([2.5,3.0,3.5])
ax1.set_yticks([0,1,2,3,4])
ax1.set_xlim(2.5,3.75)
ax1.set_title('Von Mises distribution')
ax1.set_xlabel("angle")
ax1.set_ylabel("")
fig1.tight_layout()
fig1.savefig("von_mises.png", dpi=500)

