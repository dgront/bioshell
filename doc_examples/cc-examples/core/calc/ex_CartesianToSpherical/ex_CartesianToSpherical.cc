#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>
#include <core/calc/structural/transformations/CartesianToSpherical.hh>
#include <utils/options/OptionParser.hh>

std::string program_info = R"(

Unit test that calculates spherical coordinates from a few points in the Cartesian space using BioShell

USAGE:
./ex_CartesianToSpherical

)";

std::string input_pdb = R"(ATOM    201  N   SER A  12      25.081  -7.330 -14.416  1.00  0.00           N
ATOM    202  CA  SER A  12      25.875  -6.648 -15.435  1.00  0.00           C
ATOM    203  C   SER A  12      25.030  -6.429 -16.700  1.00  0.00           C
ATOM    204  O   SER A  12      25.187  -5.429 -17.400  1.00  0.00           O
ATOM    205  CB  SER A  12      27.126  -7.492 -15.717  1.00  0.00           C
ATOM    206  OG  SER A  12      27.645  -8.029 -14.500  1.00  0.00           O
ATOM    207  H   SER A  12      25.486  -8.177 -14.049  1.00  0.00           H)";

/** @brief Calculates spherical coordinates using BioShell and 'by hand' to check of it works
 *
 * CATEGORIES: core/calc/structural/CartesianToSpherical;
 * KEYWORDS:   internal coordinates
 */
int main(const int argc, const char *argv[]) {

  using namespace core::data::structural;
  using namespace core::calc::structural::transformations;

  std::stringstream in_stream(input_pdb); // --- Create an input stream from the text
  core::data::io::Pdb reader(in_stream, core::data::io::keep_all); // --- read from this stream
  core::data::structural::Structure_SP strctr = reader.create_structure(0); // --- create a structure object

  auto residue = *(strctr->first_residue()); // --- get the residue ...
  PdbAtom_SP n = residue->find_atom(" N  "); // --- and extract three atoms
  PdbAtom_SP ca = residue->find_atom(" CA ");// ---  to form a local coordinate system (LCS)
  PdbAtom_SP c = residue->find_atom(" C  ");
  PdbAtom_SP cb = residue->find_atom(" CB "); // --- CB will be transformed

  Rototranslation_SP rt = local_coordinates_three_atoms(*n, *ca, *c);
  CartesianToSpherical to_spherical;

  Vec3 cb_local, cb_spherical;
  rt->apply(*cb, cb_local);
  to_spherical.apply(cb_local, cb_spherical);

  double r = cb_local.length();
  double theta = acos(cb_local.z / r);
  double phi = atan2(cb_local.y, cb_local.x);

  std::cout << "local computed by BioShell: " << cb_local << "\n";
  std::cout << "spherical by BioShell:      " << cb_spherical << "\n";
  std::cout << "spherical computed here:    " << utils::string_format("%8.3f %8.3f %8.3f\n", r, theta, phi);
  double x = r * sin(theta) * cos(phi);
  double y = r * sin(theta) * sin(phi);
  double z = r * cos(theta);
  std::cout << "local from inversion:       " << utils::string_format("%8.3f %8.3f %8.3f\n", x, y, z);
  to_spherical.apply_inverse(cb_spherical);
  std::cout << "local by BioShell   :      " << cb_spherical << "\n";
}
