#!/usr/bin/env python3

# A script that plots Chi-1 vs CHi2 scatterplot

from sys import argv, stderr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcl

COLUMN_CHI1 = 4
COLUMN_CHI2 = 5

chi1 = []
chi2 = []
for line in open(argv[1]) :
  tokens = line.strip().split()
  if len(tokens) > COLUMN_CHI2 :
    try :
      c1 = float(tokens[COLUMN_CHI1])
      c2 = float(tokens[COLUMN_CHI2])
      chi1.append(c1)
      chi2.append(c2)
    except :
      print("Can't parse line: "+line,file=stderr)
      
plt.xlim(0,360)
plt.ylim(0,360)
plt.xticks(np.arange(0,400,45))
plt.yticks(np.arange(0,400,45))
plt.xlabel(r"$\chi_1$ angle")
plt.ylabel(r"$\chi_2$ angle")
# plt.arrow(-180, 0, 360, 0)
# plt.arrow(0, -180, 0, 360)
# plt.colorbar(ticks=range(0, 120, 30)).set_label('Difference in corresponding Chi angles')
plt.gca().set_aspect('equal', adjustable='box')  # square
plt.grid()

# cmap = plt.cm.rainbow
# norm = mcl.BoundaryNorm(np.arange(0, 120, 30), cmap.N)
# plt.scatter(chi1,chi2, c=z, cmap=cmap, norm=norm, s=100, edgecolor='none')
plt.scatter(chi1,chi2, color='peachpuff',edgecolor='indianred',alpha=0.5)

plt.savefig('chi1_chi2.png', dpi=400)
