#include <iostream>

#include <core/data/io/Pdb.hh>

#include <core/chemical/ChiAnglesDefinition.hh>
#include <core/calc/structural/protein_angles.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Calculates all side chain Chi dihedral angles for the input protein structure
USAGE:
    ex_evaluate_chi input.pdb
EXAMPLE:
    ex_evaluate_chi 2kwi.pdb

)";

/** @brief Calculates all side chain Chi dihedral angles for the input protein structure
 *
 * CATEGORIES: core::chemical::ChiAnglesDefinition; core::calc::structural::evaluate_chi()
 * KEYWORDS:   PDB input; structural properties; structure validation
 * IMG: ex_evaluate_chi.png
 * IMG_ALT: Chi1-Chi2 statistics for ILE residue
 */
int main(const int argc, const char *argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  core::data::io::Pdb reader(argv[1]);  // Create a PDB reader for a given file
  core::data::structural::Structure_SP str = reader.create_structure(
    0); // Create a structure object from the first model

  for (auto ires = str->first_residue(); ires != str->last_residue(); ++ires) { // iterate over all residues
    std::string line = utils::string_format("%4d %3s %4s", (*ires)->id(), (*ires)->residue_type().code3.c_str(), (*ires)->owner()->id().c_str());
    
    try {
      for (unsigned short i = 1; i <= core::chemical::ChiAnglesDefinition::count_chi_angles((*ires)->residue_type()); ++i)
        line += utils::string_format(" %6.1f", core::calc::structural::evaluate_chi(**ires, i) * 180.0 / 3.1415);
      std::cout << line << "\n";
    } catch(const std::exception& e) {
	    std::cerr << "Skipping incomplete residue: "<<line<<"\n";
    }
  }
}
