#include <iostream>
#include <cmath>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <utils/exit.hh>

using namespace core::data::structural;
using namespace core::data::io;
using namespace core::data::basic;

std::string program_info = R"(

Sets a particular values for Phi, Psi and Omega angles at a certain residue in a protein.

USAGE:
    ex_set_dihedral res-id file.pdb phi psi omega
EXAMPLE:
    ex_set_dihedral 2gb1.pdb 18 -80.4 90.4 180.0

where 2gb1.pdb is the protein structure to be modified, 18 is the residue ID and the three following
real values are Phi, Psi and omega dihedrals (in the range [-180.0,180.0]). The results is printed in PDB format
)";

/** @brief Sets a particular values for Phi, Psi and Omega angles at a certain residue in a protein.
 * *
 * CATEGORIES: core/calc/structural/transformations/Rototranslation
 * KEYWORDS:   PDB input; rototranslation; structural properties
 */
int main(const int argc, const char *argv[]) {

  if(argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  // --- The first parameter of the program is the PDB file name
  core::data::io::Pdb reader(argv[1], is_not_alternative, keep_all, false); // --- Read in a PDB file
  core::data::structural::Structure_SP strctr = reader.create_structure(0);
  // --- create a Structure object from the first model and extract the first chain (indexed as 'A') from it
  core::data::structural::Chain & chain = *(strctr->get_chain('A'));
  core::index2 res_idx = utils::from_string<core::index2>(argv[2]);
  core::calc::structural::transformations::Rototranslation rt;

  // --- Phi rotation; the new value of the Phi angle (in degrees) is the fourth parameter of this program
  if (argc > 3 && strlen(argv[3]) > 1) {
    double phi = core::calc::structural::evaluate_phi(*chain[res_idx-1],*chain[res_idx]);
    std::cerr << "Phi angle before change: " << phi * 180.0 / M_PI << " degrees\n";
    phi = utils::from_string<double>(argv[3]) * M_PI / 180.0 - phi;
    PdbAtom & N = *chain[res_idx]->find_atom_safe(" N  ");
    PdbAtom & CA = *chain[res_idx]->find_atom_safe(" CA ");
    core::calc::structural::transformations::Rototranslation::around_axis(N, CA, phi, CA, rt);
    for (core::index2 ires = 0 ; ires < res_idx; ++ires) {
      for (PdbAtom_SP ai : *chain[ires]) rt.apply(*ai);
    }
    if(chain[res_idx]->find_atom(" H  ")!=nullptr)
      rt.apply(*chain[res_idx]->find_atom(" H  "));
  }

  // --- Psi rotation; the new value of the Psi angle (in degrees) is the fifth parameter of this program
  if (argc > 4 && strlen(argv[4]) > 1) {
    double psi = core::calc::structural::evaluate_psi(*chain[res_idx],*chain[res_idx+1]);
    std::cerr << "Psi angle before change: " << psi * 180.0 / M_PI << " degrees\n";
    psi = utils::from_string<double>(argv[4]) * M_PI / 180.0 - psi;
    PdbAtom & C = *chain[res_idx]->find_atom_safe(" C  ");
    PdbAtom & CA = *chain[res_idx]->find_atom_safe(" CA ");
    core::calc::structural::transformations::Rototranslation::around_axis(CA, C, psi, C, rt);
    rt.apply(*chain[res_idx]->find_atom_safe(" O  "));
    for (core::index2 ires = res_idx + 1; ires < chain.count_residues(); ++ires) {
      for (PdbAtom_SP ai : *chain[ires]) rt.apply(*ai);
    }
  }

  // --- Omega rotation; the new value of the Omega angle (in degrees) is the fifth parameter of this program
  if (argc > 5 && strlen(argv[5]) > 1) {
    double omega = core::calc::structural::evaluate_omega(*chain[res_idx],*chain[res_idx+1]);
    std::cerr << "Omega angle before change: " << omega * 180.0 / M_PI << " degrees\n";
    omega = utils::from_string<double>(argv[5]) * M_PI / 180.0 - omega;
    PdbAtom & C = *chain[res_idx]->find_atom_safe(" C  ");
    PdbAtom & N = *chain[res_idx+1]->find_atom_safe(" N  ");
    core::calc::structural::transformations::Rototranslation::around_axis(C, N, omega, N, rt);
    for (core::index2 ires = res_idx + 1; ires < chain.count_residues(); ++ires) {
      for (PdbAtom_SP ai : *chain[ires]) rt.apply(*ai);
    }
  }

  std::for_each(chain.first_atom(),chain.last_atom(),[](PdbAtom_SP ai){std::cout << ai->to_pdb_line()<<"\n";});
}
