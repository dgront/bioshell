#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running:  ./ex_set_dihedral ./test_inputs/2gb1.pdb 18 -80.4 90.4 180.0"
./ex_set_dihedral ./test_inputs/2gb1.pdb 18 -80.4 90.4 180.0
