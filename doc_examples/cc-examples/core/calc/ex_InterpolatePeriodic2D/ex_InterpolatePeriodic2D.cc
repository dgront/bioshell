#include <cmath>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/InterpolatePeriodic2D.hh>

#include <core/calc/structural/angles.hh>
#include <utils/options/OptionParser.hh>

std::string program_info = R"(

Simple example creates an interpolating polynomial for sin(x) * cos(y) 2D function and computes
maximal interpolation error (i.e. the maximum of the difference between the true function and its interpolator)

USAGE:
./ex_InterpolatePeriodic2D

)";

using namespace core::calc::numeric;

/** @brief Simple test for interpolation of a periodic 2D function
 *
 * CATEGORIES: core::calc::numeric::InterpolatePeriodic2D;
 * KEYWORDS:   interpolation
 */
int main(int cnt, char* argv[]) {

  if ((cnt > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  double inter_step = core::calc::structural::to_radians(5.0); // --- interpolation step : 5 degrees
  const double EPS = 0.0001;
  std::vector<double> vx; // --- vector of function arguments: X axis
  std::vector<double> vy; // --- vector of function arguments: Y axis
  for (double x = -M_PI; x < M_PI-EPS; x += inter_step) {
    vx.push_back(x);
    vy.push_back(x); // --- we use the same values both for X and Y but in general they may differ
  }
  core::index2 nx = vx.size(); // the number of grid points

  // --- Prepare data to be interpolated
  std::shared_ptr<core::data::basic::Array2D<double>> data_periodic = std::make_shared<core::data::basic::Array2D<double>>(nx,nx);
  for (size_t ix = 0; ix < vx.size(); ++ix)
    for (size_t iy = 0; iy < vy.size(); ++iy) data_periodic->set(ix, iy, sin(vx[ix]) * cos(vy[iy]));

  // --- Create the actual interpolator object
  CatmullRomInterpolator<double> cri;
  InterpolatePeriodic2D<double, CatmullRomInterpolator<double> > ip(-M_PI,inter_step,nx,-M_PI,inter_step,nx, data_periodic, cri);
  double max_error = 0.0;
  for (double x = -5; x <= 4.0; x += inter_step / 3.0) {
    for (double y = -5.0; y <= 4.0; y += inter_step / 3.0) {
      double v = sin(x) * cos(y); // --- calculate the true value of the interpolated function ...
      double vi = ip(x, y);     // --- also the interpolated value
      double err = fabs(v - vi);
      max_error = std::max(err, max_error);
    }
  }

  std::cout << "Maximum interpolation error: " << max_error << "\n";
}
