#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ap_BackboneHBondMap test_inputs/2gb1.pdb"
./ap_BackboneHBondMap test_inputs/2gb1.pdb
echo -e "Running ./ap_BackboneHBondMap test_inputs/5edw.pdb"
./ap_BackboneHBondMap test_inputs/5edw.pdb
echo -e "Running ./ap_BackboneHBondMap test_inputs/3dcg.pdb"
./ap_BackboneHBondMap test_inputs/3dcg.pdb
