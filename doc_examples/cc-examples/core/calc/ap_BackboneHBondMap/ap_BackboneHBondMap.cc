#include <iostream>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/protocols/selection_protocols.hh>
#include <core/calc/structural/interactions/BackboneHBondMap.hh>
#include <utils/exit.hh>

using namespace core::data::structural;
using namespace core::data::io;
using namespace core::data::basic;

std::string program_info = R"(

Reads a PDB file and calculates a map of backbone hydrogen bonds, providing also the geometry of each bond.
The resulting table, printed on the screen provides:
- H donor residue name and id (columns 1 and 2)
- H acceptor residue name and id (columns 4 and 5)
- two distances: r(O..H) and r(N..O) (columns 7 and 8)
- planar (C-O..H) and dihedral (C-O..H-N) (columns 9 and 10)
- DSSP energy for this bond (column 11)
- X,Y, Z coordinates of H atom in the local coordinates system (columns 12, 13 and 14)
- theta, phi spherical coordinates of H atom (columns 15 and 16)


EXAMPLE OUTPUT:
# 42 hydrogen bonds found in backbone:
 TYR    3 ->  THR   18 : 2.620 3.346 165.58   94.25  -1.170    -0.702   0.211   2.515     16.25  163.29
 LYS    4 ->  LYS   50 : 2.259 3.156 120.71  159.88  -1.310     1.445   1.249   1.205     57.75   40.83
 LEU    5 ->  THR   16 : 1.838 2.802 143.84 -115.27  -2.834     0.102  -1.075   1.488     35.98  -84.57

USAGE:
./ap_BackboneHBondMap input.pdb

EXAMPLE:
./ap_BackboneHBondMap 5edw.pdb

)";

/** @brief Calculates a map of backbone hydrogen bonds.
 *
 * BackboneHBondMap is derived from PairwiseResidueMap class
 *
 * CATEGORIES: core/calc/structural/BackboneHBondMap;
 * KEYWORDS:   PDB input; Hydrogen bonds; data_structures; Protein structure features
 * GROUP: Structure calculations;
 * IMG: ap_BackboneHBondMap-2gb1.png
 * IMG_ALT: Map of backbone hydrogen bonds for 2GB1 protein
 */
int main(const int argc, const char* argv[]) {
  
  if(argc < 2) utils::exit_OK_with_message(program_info);                                  // --- complain about missing program parameter
  core::data::io::Pdb reader(argv[1], is_not_alternative, only_ss_from_header, true); // --- Read in a PDB file
  core::data::structural::Structure_SP strctr = reader.create_structure(0);     // --- create a Structure object from the first model

  // ---------- Remove everything but amino acids from an input structure; BackboneHBondMap currently can process only AA
  core::protocols::keep_selected_atoms(core::data::structural::selectors::IsAA{}, *strctr);

  // --- The line below creates a map of backbone hydrogen bonds; -0.2 is the energy cutoff (in kcal/mol) to recognize the bond
  core::calc::structural::interactions::BackboneHBondMap hb_map(*strctr, -0.2);
  std::cout << "# " << hb_map.count_bonds() << " hydrogen bonds found in backbone:\n";
  for (auto h_it = hb_map.cbegin(); h_it != hb_map.cend(); ++h_it) // --- iterate over the bonds and print each of them
    std::cout << *((*h_it).second->donor_residue()) << " -> " << *((*h_it).second->acceptor_residue()) << " : "<< *(*h_it).second << "\n";

  // --- Here we test some other BackboneHBondMap methods
  const auto hbond = (*hb_map.cbegin()).second; // --- here we make a local copy of the first h-bond to be used in tests
  std::cerr << "# Is residue 0 h-bonded to residue 3? " << ((hb_map.are_H_bonded(0, 3)) ? "yes\n" : "no\n");
  std::cerr << "# Is residue 0 h-bonded to residue 5? " << ((hb_map.at(0, 5) != nullptr) ? "yes\n" : "no\n");
  std::cerr << "# Are " << *hbond->donor_residue() << " and " << *hbond->acceptor_residue() << " really bonded? " <<
    ((hb_map.at(*hbond->donor_residue(), *hbond->acceptor_residue()) != nullptr) ? "yes\n" : "no\n");
}

