#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ap_orient_pdb test_inputs/2kwi.pdb B 391 418 > helix_rotated.pdb"
./ap_orient_pdb test_inputs/2kwi.pdb B 391 418 > helix_rotated.pdb

mkdir -p outputs_from_test
mv *.pdb outputs_from_test/
