#include <iostream>
#include <random>

#include <core/index.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/calc/numeric/Pca3.hh>
#include <utils/exit.hh>
#include <core/calc/structural/angles.hh>

std::string program_info = R"(

ap_orient_pdb reads a PDB file and orients the atoms along the axes so the longest protein dimension is along X
and the second longest along Y.

This example also creates a second transformation, that repeatedly rotate a structure fragment around Z axis by 45 degrees
The first (mandatory) argument is a PDB file name. User can also specify a structural fragment by providing a respective
chain-ID and a residue range.

USAGE:
    ap_orient_pdb input.pdb [chain-id first-resid last-resid]
EXAMPLE:
    ap_orient_pdb input.pdb B 419 446

where 2kwi.pdb is the name of an input file and 419 446 are the first and last
of the reoriented residues of chain B, respectively

)";

/** @brief Shows how to rotate a piece of a protein structure
 *
 * CATEGORIES: core/calc/numeric/PCA.hh
 * KEYWORDS: PDB input; structural fragment; structure selectors; PCA; transformations
 * GROUP: Structure calculations;
 * IMG: helices.png
 * IMG_ALT: Alpha helix rotated a few times by a fixed angle
 */
int main(const int argc, const char *argv[]) {

  using namespace core::data::basic; // --- for Vec3 and Array2D

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  core::data::io::Pdb reader(argv[1]); // --- read the input PDB file
  core::data::structural::Structure_SP structure_sp = reader.create_structure(0); // --- create a structure corresponding to the first model

  std::vector<core::data::structural::PdbAtom_SP> points3d;

  // --- if a chain-ID and residue range were also given, create a selector to extract the relevant part of the input
  if (argc > 4) {
    std::string selection_string = utils::string_format("%c:%d-%d", argv[2][0], atoi(argv[3]), atoi(argv[4]));
    core::data::structural::selectors::SelectChainResidues selector(selection_string);
    // --- if selector selects (returns true), copy the atoms
    for (auto atom_it = structure_sp->first_atom(); atom_it != structure_sp->last_atom(); ++atom_it)
      if (selector((*atom_it)->owner())) points3d.push_back(*atom_it);
  } else { // --- If there is no selection, copy all the atoms from the given structure
    for (auto atom_it = structure_sp->first_atom(); atom_it != structure_sp->last_atom(); ++atom_it)
      points3d.push_back(*atom_it);
  }
  core::calc::numeric::Pca3 pca3(points3d);
  auto rt = pca3.create_transformation();
  std::cout << "MODEL        1\n";
  for (auto atom : points3d) {
    rt.apply(*atom);
    std::cout << (atom)->to_pdb_line() << "\n";
  }
  std::cout << "ENDMDL\n";
  auto rt2 = core::calc::structural::transformations::Rototranslation::around_axis(
    Vec3(0,0,1),core::calc::structural::to_radians(45.0),Vec3(0,0,0));
  for(int i=2;i<5;i++) {
    std::cout << "MODEL        " << i << "\n";
    for (auto atom : points3d) {
      rt2.apply(*atom);
      std::cout << atom->to_pdb_line() << "\n";
    }
    std::cout << "ENDMDL\n";
  }

}

