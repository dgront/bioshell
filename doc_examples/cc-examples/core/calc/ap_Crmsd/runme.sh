#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

for pdb in test_inputs/*model*pdb
do
  echo -e "#Running ./ap_Crmsd  test_inputs/2gb1.pdb" $pdb
  ./ap_Crmsd test_inputs/2gb1.pdb $pdb
done

echo -e "#Running ./ap_Crmsd  test_inputs/1cey.pdb"
./ap_Crmsd test_inputs/1cey.pdb

