#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/calc/structural/transformations/Crmsd.hh>

#include <utils/exit.hh>

std::string program_info = R"(

Calculates cRMSD (coordinate Root-Mean-Square Deviation) value on C-alpha coordinates and prints it.
If only one input PDB file is given, cRMSD is computed for every pair of models found in the input
file (each-vs-each). If exactly two structures are provided, the program calculates cRMSD between
the first model of structure A and the first model of structure B. Finally, when more than two input
files are specified, each-vs-each calculations are performed for every pair of given structures.

Note, that all the structures must contain the same number of C-alpha atoms.

USAGE:
./ap_Crmsd file1.pdb [file2.pdb ..]

EXAMPLEs:
./ap_Crmsd 1cey.pdb
./ap_Crmsd 2gb1.pdb 2gb1-model1.pdb
./ap_Crmsd 2gb1-model1.pdb 2gb1-model2.pdb 2gb1-model3.pdb 2gb1-model4.pdb

REFERENCE:
Kabsch, W. "A Solution for the Best Rotation to Relate Two Sets of Vectors."
Acta Cryst (1976) 32 922-923

)";

/** @brief Calculates crmsd value on C-alpha coordinates. The program prints just the crmsd value.
 *
 * CATEGORIES: core/calc/structural/transformations/Crmsd
 * KEYWORDS:   PDB input; crmsd
 * GROUP: Structure calculations;
 * IMG: ap_Crmsd_deepteal_brown_1.png
 * IMG_ALT: 2GB1 model structure superimposed on the native, crmsd = 4.93952
 */
int main(const int argc, const char *argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using core::data::basic::Vec3;
  using namespace core::calc::structural::transformations;

  Crmsd<std::vector<Vec3>,std::vector<Vec3>> rms;

  if(argc==2) { // --- The case of each-vs-each calculations between models of a single PDB file
    core::data::io::Pdb q_reader(argv[1],core::data::io::is_ca, core::data::io::keep_all, false);
    core::data::structural::Structure_SP q_strctr = q_reader.create_structure(0); // --- create a structure object

    std::vector<std::vector<core::data::basic::Vec3>> models(q_reader.count_models());
    for(int i=0;i<q_reader.count_models();++i) {
      models[i].resize(q_strctr->count_atoms());
      q_reader.fill_structure(i,models[i]);
      for (int j = 0; j < i; ++j)
        std::cout << i<<" "<<j << " "<<rms.crmsd(models[i], models[j],models[j].size()) << "\n";
    }
  } else { // --- The case when two PDB files are given
    core::data::io::Pdb q_reader(argv[1], core::data::io::is_ca, core::data::io::keep_all, false);
    core::data::structural::Structure_SP q_strctr = q_reader.create_structure(0); // --- create a structure object

    core::data::io::Pdb t_reader(argv[2], core::data::io::is_ca, core::data::io::keep_all, false);
    core::data::structural::Structure_SP t_strctr = t_reader.create_structure(0); // --- create a structure object

    if (q_strctr->count_atoms() != t_strctr->count_atoms())
      utils::exit_OK_with_message("The two structures have different number of CA atoms!\n");

    std::vector<Vec3> q, t;
    for (auto atom_it = q_strctr->first_atom(); atom_it != q_strctr->last_atom(); ++atom_it) q.push_back(**atom_it);
    for (auto atom_it = t_strctr->first_atom(); atom_it != t_strctr->last_atom(); ++atom_it) t.push_back(**atom_it);
    std::cout << "crmsd: " << rms.crmsd(q, t, q_strctr->count_atoms()) << "\n";
  }
}
