#include <iostream>

#include <core/index.hh>
#include <core/calc/statistics/Random.hh>
#include <core/calc/statistics/OnlineStatistics.hh>
#include <core/calc/statistics/simple_statistics.hh>
#include <utils/exit.hh>

std::string program_info = R"(

ap_bootstrap_quantile reads a file with real values and calculates statistics for a given quantile.

The statistics: expected quantile value and its standard deviation are computed by 100-folt bootstrap
procedure.

If no input file is provided, the program calculates the statistics of a random sample withdrawn
from a normal distribution (mean=0.0, variance = 1.0)
USAGE:
    ap_bootstrap_quantile quantile_value infile
    ap_bootstrap_quantile quantile_value


)";

/** @brief Reads a file with real values and calculates statistics for a given quantile
 *
 * If no input file is provided, the program calculates the statistics from a random sample
 *
 * CATEGORIES: core::calc::statistics::simple_statistics.hh; core::calc::statistics::Random
 * KEYWORDS:   random numbers; statistics
 * GROUP: Statistics;
 */
int main(const int argc, const char *argv[]) {

  if(argc ==1)  utils::exit_OK_with_message(program_info);

  double quantile_level = atof(argv[1]);

  core::calc::statistics::OnlineStatistics stats;
  if(argc < 3) {
    // --- complain about missing program parameter
    //std::cerr << program_info;
    // ---------- Use the random engine if no data is provided - for testing purposes
    size_t n_data = 10000;
    std::vector<double> data(n_data);
    core::calc::statistics::Random r = core::calc::statistics::Random::get();
    r.seed(12345);  // --- seed the generator for repeatable results
    core::calc::statistics::NormalRandomDistribution<double> normal_random;
    for (core::index4 n = 0; n < n_data; ++n) data[n] = normal_random(r);
    const auto out = core::calc::statistics::bootstrap_quantile(data, quantile_level, 100);
    std::cout << "q-level value stdev\n" << quantile_level << " " << out.first << " " << out.second << "\n";
  } else {
    std::vector<double> data;
    for(size_t i_file=2;i_file<argc;++i_file) {
      data.clear();
      std::ifstream in(argv[i_file]);
      double r;
      while(in) {
        in >> r;
        data.push_back(r);
      }
      in.close();
      const auto out = core::calc::statistics::bootstrap_quantile(data, quantile_level, 100);
      std::cout << "fname q-level value stdev\n" << argv[i_file] << " " << quantile_level << " " << out.first << " "
                << out.second << "\n";
    }
  }

}
