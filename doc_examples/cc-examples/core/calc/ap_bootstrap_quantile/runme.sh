#!/bin/bash

echo -e "# Running ./ap_bootstrap_quantile 0.25"
./ap_bootstrap_quantile 0.25

echo -e "# Running ./ap_bootstrap_quantile 0.2 test_inputs/random_normal.txt"
./ap_bootstrap_quantile 0.2 test_inputs/random_normal.txt
