#include <iostream>
#include <numeric>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/io/Pdb.hh>
#include <core/calc/structural/interactions/StackingInteraction.hh>
#include <core/calc/structural/interactions/StackingInteractionCollector.hh>
#include <core/calc/structural/interactions/VdWInteractionCollector.hh>
#include <core/calc/structural/interactions/VdWInteraction.hh>
#include <core/calc/structural/interactions/HydrogenBondInteraction.hh>
#include <core/calc/structural/interactions/HydrogenBondCollector.hh>
#include <utils/LogManager.hh>

#include <utils/exit.hh>

std::string program_info = R"(

Finds ligand - protein interactions in a given PDB file. Ligand code must also be provided

The program prints interactions between protein and ligand including stacking, hydrogen bonds and van der Waals interactions.

USAGE:
    ap_ligand_interactions input.pdb ligand_code

EXAMPLE:
    ap_ligand_interactions 5ldk.pdb ATP

)";

using namespace core::data::io; // --- Pdb reader and PdbLineFilter lives there
using namespace core::calc::structural::interactions;


/** @brief Finds stacking, hbonds and van der Waals interactions for ligand in a given PDB file.
 *
 * CATEGORIES: core::data::io::Pdb; core::calc::structural::interactions
 * KEYWORDS:   PDB input; PDB line filter; interactions
 * GROUP: Structure calculations;
 */
int main(const int argc, const char *argv[]) {

    if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
    utils::LogManager::FINER(); // --- INFO is the default logging level; set it to FINE to see more

    // --- Read a PDB file given as an argument to this program
    Pdb reader(argv[1], // --- input PDB file
               all_true(is_not_water, is_not_alternative, is_not_hydrogen,
                        invert_filter(is_bb)), // --- Inverted backbone selector  reads only side chains
                keep_all, true); // --- yes, read header

    core::data::structural::Structure_SP s = reader.create_structure(0);
    std::string code_3 = argv[2];

    VdWInteractionCollector vdw_collector = VdWInteractionCollector();
    HydrogenBondCollector hb_collector = HydrogenBondCollector();
    StackingInteractionCollector stack_collector = StackingInteractionCollector();

    std::vector<ResiduePair_SP> v_sink;
    std::vector<ResiduePair_SP> hb_sink;
    std::vector<ResiduePair_SP> s_sink;

    hb_collector.collect(*s, hb_sink);
    vdw_collector.collect(*s, v_sink);
    stack_collector.collect(*s, s_sink);

    std::cout << VdWInteraction::output_header() << "\n";
    for (const ResiduePair_SP ri:v_sink) {
        VdWInteraction_SP bi = std::dynamic_pointer_cast<VdWInteraction>(ri);
        if (bi && bi->first_residue()->residue_type().code3.c_str() == code_3) std::cout << *bi << "\n";
    }

    std::cout << HydrogenBondInteraction::output_header() << "\n";
    for (const ResiduePair_SP ri:hb_sink) {
        HydrogenBondInteraction_SP bi = std::dynamic_pointer_cast<HydrogenBondInteraction>(ri);
        if (bi && bi->first_residue()->residue_type().code3.c_str() == code_3) std::cout << *bi << "\n";
    }

    std::cout << StackingInteraction::output_header() << "\n";
    for (const ResiduePair_SP ri:s_sink) {
        StackingInteraction_SP bi = std::dynamic_pointer_cast<StackingInteraction>(ri);
        if (bi && bi->first_residue()->residue_type().code3.c_str() == code_3) std::cout << *bi << "\n";
    }
}
