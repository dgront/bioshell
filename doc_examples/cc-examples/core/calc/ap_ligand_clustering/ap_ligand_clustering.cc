#include <iostream>
#include <map>

#include <core/data/io/Pdb.hh>
#include <utils/exit.hh>
#include <utils/io_utils.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/protocols/PairwiseLigandCrmsd.hh>
#include <core/calc/clustering/DistanceByValues1B.hh>
#include <core/calc/clustering/HierarchicalClustering1B.hh>

std::string program_info = R"(

ap_ligand_clustering performs clustering analysis on small molecule docking poses.
The default settings for this program are: clustering_cutoff: 5.0 Angstroms and
min_cluster_size: 5

Every line of the output contains a single cluster:  the first is number that cluster size,
followed by PDB file names that belong to that cluster

SEE:
    pdb_from_clustering.py example script is a tool to create PDB files based on output from
    ap_ligand_clustering and input PDB files

USAGE:
    ap_ligand_clustering code list_of_files.txt [ min_cluster_size clustering_cutoff1 ..  ]

SEE ALSO:
  ap_docking_crmsd - for a flexible docking crmsd calculations
  ap_stiff_docking_crmsd - for a rigid docking crmsd calculations
  ap_LigandsOnGridProtocol - simple clustering by projecting ligands on a 3D grid; crude but fast; can handle very large poolsof models

EXAMPLE:
    ap_ligand_clustering CLO  list.txt 10 2.0 5.0

)";

/** @brief Performs clustering analysis on small molecule docking poses
 *
 * CATEGORIES: core::calc::clustering::HierarchicalClustering1B
 * KEYWORDS: PDB input; clustering;
 * GROUP: Structure calculations;  Docking;
 * IMG:
 * IMG_ALT:
 */
int main(const int argc, const char* argv[]) {

  if (argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  utils::Logger l("ap_ligand_clustering");

  using namespace core::data::structural::selectors;
  AtomSelector_SP select_ligand =
      std::static_pointer_cast<AtomSelector>(std::make_shared<SelectResidueByName>(argv[1]));
  AtomSelector_SP select_ca =
      std::static_pointer_cast<AtomSelector>(std::make_shared<IsCA>());

  core::protocols::PairwiseLigandCrmsd crmsd_calculator(select_ligand, select_ca);

  std::vector<std::string> fnames = utils::read_listfile(argv[2]);
  for(const std::string & f:fnames) {
    core::data::io::Pdb reader(f, "is_not_hydrogen", false, false);
    crmsd_calculator.add_input_structure(reader.create_structure(0), f);
  }

  core::index2 min_cluster_size = (argc < 4) ? 5 : atof(argv[3]);
  std::vector<double> clustering_cutoffs;
  double max_clustering_cutoff = 0.0;
  if (argc < 4) {
    max_clustering_cutoff = 15.0;
    clustering_cutoffs.push_back(15.0);
  } else {
    for (int i = 4; i < argc; ++i) {
      clustering_cutoffs.push_back(atof(argv[i]));
      max_clustering_cutoff = std::max(max_clustering_cutoff, clustering_cutoffs.back());
    }
  }
  double evaluate_cutoff = max_clustering_cutoff * 1.5;
  double conversion_factor = 255 / evaluate_cutoff;

  crmsd_calculator.crmsd_cutoff(evaluate_cutoff);
  crmsd_calculator.set_out_matrix();
  crmsd_calculator.calculate();
  auto out = crmsd_calculator.out_matrix();

  core::calc::clustering::DistanceByValues1B distances(crmsd_calculator.tags());
  for (core::index4 i = 1; i < fnames.size(); ++i) {
    for (core::index4 j = 0; j < i; ++j) {
      if (out->has_element(i, j)) {
        double v = out->at(i, j) * conversion_factor;
        distances.set(i, j, core::index1(v));
        distances.set(j, i, core::index1(v));
        // --- uncomment the line below to see the actual distance values together with their converted counterparts
        // std::cerr << i << " " << j << " " << out->at(i, j) << " " << int(v) << "\n";
      }
    }
  }

  core::calc::clustering::HierarchicalClustering1B hac(distances.labels(), "");
  hac.run_clustering(distances, "COMPLETE_LINK");
  for (double clustering_cutoff:clustering_cutoffs) {
    std::ofstream out(utils::string_format("clusters-%.2f.txt", clustering_cutoff));
    auto clusters = hac.get_clusters(clustering_cutoff * conversion_factor, min_cluster_size);
    l << utils::LogLevel::INFO << clusters.size() << " clusters created for cutoff " << clustering_cutoff << "\n";

    for (const auto &c : clusters) {
      std::vector<std::string> el = c->cluster_items(c);
      out << el.size() << " ";
      for (const std::string &s:el) out << s << " ";
      out << "\n";
    }
    out.close();
  }
}
