#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "# Running ./ap_ligand_clustering Clo test_inputs/file_list.txt 5 6.0 10.0 "
./ap_ligand_clustering Clo test_inputs/file_list.txt 5 6.0 10.0 

mkdir -p outputs_from_test
mv clusters-10.00.txt clusters-6.00.txt outputs_from_test
