#!/bin/bash

echo -e "Running ./ex_chi2_independence_test"
./ex_chi2_independence_test

echo -e "Running ./ex_chi2_independence_test 4 1.51"
./ex_chi2_independence_test 4 1.51

echo -e "Running ./ex_chi2_independence_test test_inputs/chi2_test_data.txt"
./ex_chi2_independence_test test_inputs/chi2_test_data.txt
