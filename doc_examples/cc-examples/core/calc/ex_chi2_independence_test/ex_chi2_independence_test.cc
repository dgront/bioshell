#include <iostream>

#include <core/data/basic/Array2D.hh>
#include <core/calc/statistics/simple_statistics.hh>

std::string program_info = R"(

Performs chi-square test: calculates p-value for a given number of DOFs. Alternatively,
it can read a contingency matrix from a file and calculate test for independence of its two first rows
When no input data is provided, the example performs Chi-square independence test on a test data
USAGE:
    ex_chi2_independence_test [n_dofs chi2_value]
    ex_chi2_independence_test [input_contingency_matrix_file]

EXAMPLE:
    ex_chi2_independence_test 4 1.52
    ex_chi2_independence_test matrix.dat

REFERENCE:
Pearson, Karl. "X. On the criterion that a given system of deviations from the probable in the case
of a correlated system of variables is such that it can be reasonably supposed to have arisen from random sampling."
The London, Edinburgh, and Dublin Philosophical Magazine and Journal of Science 50.302 (1900): 157-175.

)";

/** @brief Chi-square test for independence.
 *
 * CATEGORIES: core::calc::statistics::chi2_independence_test;
 * KEYWORDS:   statistics; data table
 */
int main(const int argc, const char* argv[]) {

  using core::data::basic::Array2D;

  if(argc==3) { // --- calculate chi-square test for given chi-square statistics value and the number of DOFs
    int k = utils::from_string<int>(argv[1]);
    double crit = utils::from_string<double>(argv[2]);
    std::cout << "# DOFs:       " << k << "\n";
    std::cout << "# chi2 value: " << crit << "\n";
    std::cout << "# p-value:    " << core::calc::numeric::chi_square_pvalue(k,crit) << "\n";
    return 0;
  }

  if(argc==2) { // --- calculate chi-square test for given data (test the independence of the two first rows)
    Array2D<core::index4> m = Array2D<core::index4>::from_file(argv[1]);
    int k = (m.count_rows() - 1) * (m.count_columns() - 1);
    double crit = core::calc::statistics::chi2_independence_test(m);
    std::cout << "# DOFs:       " << k << "\n";
    std::cout << "# chi2 value: " << crit << "\n";
    std::cout << "# p-value:    " << core::calc::numeric::chi_square_pvalue(k, crit) << "\n";

    return 0;
  }

  std::cerr << program_info;

  std::vector<core::index4> data = {71,154,398,4992,2808,2737};
  Array2D<core::index4> m(2,3, data);

  int k = 2; // (2-1)*(3-1) = 2
  double crit = core::calc::statistics::chi2_independence_test(m);

  std::cout << "# DOFs:       " << k << "\n";
  std::cout << "# chi2 value: " << crit << "\n";
  std::cout << "# p-value:    " << core::calc::numeric::chi_square_pvalue(k,crit) << "\n";
}
