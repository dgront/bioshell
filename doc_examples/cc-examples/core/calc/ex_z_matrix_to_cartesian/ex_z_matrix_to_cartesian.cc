#include <iostream>
#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/angles.hh>
#include <core/data/structural/PdbAtom.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Test for z_matrix_to_cartesian() function recovers cartesian coordinates of a fluoroethylene
from Z-matrix (internal coordinates)

USAGE:
./ex_z_matrix_to_cartesian

)";

/** @brief Test for z_matrix_to_cartesian() function
 *
 * This test recovers fluoroethylene from Z-matrix (internal coordinates)
 * CATEGORIES: core::calc::structural::z_matrix_to_cartesian
 * KEYWORDS:   internal coordinates
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using core::data::structural::PdbAtom;
  using namespace core::calc::structural;

  PdbAtom F(1, " F  ", -1.0606, 0.1723, 0.0001, core::chemical::AtomicElement::FLUORINE.z);
  PdbAtom C1(2, " C1 ", 0.1319, -0.4627, -0.0005, core::chemical::AtomicElement::CARBON.z);
  PdbAtom C2(3, " C2 ", 1.2458, 0.2325, 0.0001, core::chemical::AtomicElement::CARBON.z);
  PdbAtom H11(2, " H11", 0.1690, -1.5420, 0.0030, core::chemical::AtomicElement::HYDROGEN.z);
  PdbAtom H21(3, " H21", 2.1991, -0.2751, -0.0004, core::chemical::AtomicElement::HYDROGEN.z);
  PdbAtom H22(3, " H22", 1.2087, 1.3119, 0.0010, core::chemical::AtomicElement::HYDROGEN.z);


  PdbAtom H11_(2, " H11", 0,0,0, core::chemical::AtomicElement::HYDROGEN.z);
  PdbAtom H21_(2, " H21", 0,0,0, core::chemical::AtomicElement::HYDROGEN.z);
  PdbAtom H22_(2, " H22", 0,0,0, core::chemical::AtomicElement::HYDROGEN.z);
  core::calc::structural::z_matrix_to_cartesian(F, C2, C1, 1.0, to_radians(120), to_radians(180), H11_);
  core::calc::structural::z_matrix_to_cartesian(F, C1, C2, 1.0, to_radians(120), to_radians(180), H21_);
  core::calc::structural::z_matrix_to_cartesian(H21_, C1, C2, 1.0, to_radians(120), to_radians(180), H22_);
  std::cout << C2.to_pdb_line() << "\n";
  std::cout << C1.to_pdb_line() << "\n";
  std::cout << F.to_pdb_line() << "\n";
  std::cout << H11_.to_pdb_line() << "\n";
  std::cout << H21_.to_pdb_line() << "\n";
  std::cout << H22_.to_pdb_line() << "\n";
  double error = H11_.distance_to(H11);
  error += H21_.distance_to(H21);
  error += H22_.distance_to(H22);
  std::cout << "# Average error on the three hydrogen atoms: "<<error/3.0<<"\n";
}
