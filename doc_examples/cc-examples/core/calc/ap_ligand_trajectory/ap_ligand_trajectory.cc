#include <iostream>
#include <map>

#include <core/data/io/Pdb.hh>
#include <utils/string_utils.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Finds contacts between a ligand molecule and a protein.

Reads a multi-model PDB file and detects contacts in every model (e.g. a frame of an MD simulation).
The output provides the interacting residues (name and residue ID) along with the number of observations
for this contact. Requires PDB input file, three-letter ligand code and contact distance in Angstroms.

USAGE:
    ./ap_ligand_trajectory input.pdb ligand-code contact-distance

EXAMPLE:
    ./ap_ligand_trajectory test_inputs/2kwi.pdb GNP 3.5

)";

/** @brief Finds contacts between a ligand molecule and a multimodel-protein.
 *
 * CATEGORIES: core::data::io::Pdb
 * KEYWORDS: PDB input; contact map; ligand
 * GROUP: Structure calculations;
 */
int main(const int argc, const char* argv[]) {

  if (argc < 4) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  core::data::io::Pdb reader(argv[1]); // --- file name (PDB format, may be gzip-ped)

  const std::string code(argv[2]);    // --- The ligand code is the second parameter of the program
  double cutoff = utils::from_string<double>(argv[3]); // The third parameter is the contact distance (in Angstroms)

  std::map<std::string, int> results; // --- Map used to store results
  for (size_t i = 0; i < reader.count_models(); ++i) { // --- Iterate over all models in the input file
    core::data::structural::Structure_SP strctr = reader.create_structure(i);

    // --- Here we use a standard <code>find_if</code> algorithm to find the ligand residue by its 3-letter code
    auto ligand = std::find_if(strctr->first_residue(), strctr->last_residue(), [&code](core::data::structural::Residue_SP res) {return (res->residue_type().code3==code);});
    if(ligand== strctr->last_residue()) { // --- If no ligand - print a message and take next structure
      std::cerr << "Model " << i << " has no " << argv[2] << " residue\n";
      continue;
    }
    for (auto it_resid = strctr->first_residue(); it_resid != strctr->last_residue(); ++it_resid) {
      double d = (*it_resid)->min_distance(*ligand);
      if (d < cutoff) { // --- if this is close enough,
        std::string key = utils::string_format("%3s  %4s  %4d",(*it_resid)->residue_type().code3.c_str(),
          (*it_resid)->owner()->id().c_str(), (*it_resid)->id());
        if (results.find(key) == results.end()) results[key] = 1;
        else results[key]++;
      }
    }
  }

  // --- print results
  std::cout <<"#resn chain resid counts\n";
  for(const auto & p:results)
    std::cout << p.first<<" "<<utils::string_format("%5d",p.second)<<"\n";
}
