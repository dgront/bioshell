#include <iostream>
#include <random>
#include <cstdlib>

#include <core/calc/statistics/VonMisesDistribution.hh>
#include <core/calc/statistics/Random.hh>

std::string program_info = R"(

ex_plot_VonMises_mixture evaluates a mixture of Von Mises distribution so it can be plotted nicely

USAGE:
    ex_plot_VonMises_mixture scaling mu kappa [scaling2 mu2 kappa2 ...]

EXAMPLE:
    ex_plot_VonMises_mixture 0.487862 -3.00582 17.4059 0.0794212 -1.02886 112.164

where the six numbers are scaling, mean and spread of two VonMises distributions

REFERENCE:
    http://mathworld.wolfram.com/vonMisesDistribution.html
    https://en.wikipedia.org/wiki/Von_Mises_distribution
)";

/** @brief Example which evaluates a mixture of Von Mises distribution so it can be plotted nicely
 * CATEGORIES: core/calc/statistics/VonMisesDistribution
 * KEYWORDS: statistics
 * IMG: ex_plot_VonMises_mixture.png
 * IMG_ALT: Mixture of von Mises functions plotted
 */
int main(const int argc, const char *argv[]) {

  using namespace core::calc::statistics;

  std::vector<double> factors;
  std::vector<VonMisesDistribution> components;
  for (size_t i = 1; i < argc; i += 3) {
    factors.push_back(atof(argv[i]));
    components.push_back(VonMisesDistribution{atof(argv[i + 1]), atof(argv[i + 2])});
  }

  for (double x = -M_PI; x <= M_PI; x += M_PI / 62.8) {
    double val = 0;
    for (size_t i = 0; i < components.size(); ++i) val += factors[i] * components[i].evaluate(x);
    std::cout << utils::string_format("%6.3f %9f\n", x, val);
  }
}
