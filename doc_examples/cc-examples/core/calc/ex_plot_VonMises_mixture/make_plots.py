import math
import matplotlib.pyplot as plt

input_data_file = "expected_outputs/stdout.out" # Output from the test

X = []
Y = []
for line in open(input_data_file) :
  if line[0] =='#' : continue
  tokens = line.strip().split()
  X.append(float(tokens[0]))
  Y.append(float(tokens[1]))

fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.plot(X, Y, color='#124653')
ax1.legend()
plt.title('Sum of VonMises distributions')
ax1.set_xlabel("x")
ax1.set_ylabel("f(x)")
fig1.tight_layout()
fig1.savefig("ex_plot_VonMises_mixture.png", dpi=500)

