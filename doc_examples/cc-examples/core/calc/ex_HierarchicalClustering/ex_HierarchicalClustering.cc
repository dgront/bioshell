#include <iostream>
#include <sstream>
#include <vector>
#include <numeric> // for std::accumulate

#include <core/algorithms/trees/algorithms.hh>
#include <core/calc/clustering/DistanceByValues.hh>
#include <core/calc/clustering/HierarchicalCluster.hh>
#include <core/calc/clustering/HierarchicalClustering.hh>
#include <utils/options/OptionParser.hh>

std::string program_info = R"(

Example showing how to use hierarchical clustering method. The program uses Single Link method to cluster letters.
Once clustering is done, it prints the clustering tree.

USAGE:
./ex_HierarchicalClustering

)";

using namespace core::calc::clustering;

static utils::Logger l("ex_HierarchicalClustering");

/// Data points to be clustered
std::vector<std::string> points = {"A", "B", "C", "E", "G", "L", "M", "Q", "R", "T", "X", "Y", "Z"};

/// Distance function defined for the data above; here the alphabetic distance is used
DistanceByValues<float> calc_distance_matrix(std::vector<std::string> points) {

  DistanceByValues<float> d(points, 99.0, 99.0);
  for (size_t i = 1; i < d.n_data(); i++)
    for (size_t j = 0; j < i; j++) {
      float v = std::sqrt((points[j][0] - points[i][0]) * (points[j][0] - points[i][0]));
      d.set(i, j, v);
      d.set(j, i, v);
    }

  return d;
}

/** @brief Example showing how to use hierarchical clustering method.
 *
 * CATEGORIES: core::calc::clustering::HierarchicalClustering
 * KEYWORDS: clustering; hierarchical clustering
 */
int main(int cnt, char *argv[]) {

  if ((cnt > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  DistanceByValues<float> d = calc_distance_matrix(points);

  HierarchicalClustering<float, std::string> hac(d.labels(), "");
  hac.run_clustering(d, "");
  std::vector<std::string> elements;
  for (size_t i = 0; i < hac.count_steps(); i++) {
    elements.clear();
    std::shared_ptr<BinaryTreeNode<std::string> > c_node(std::static_pointer_cast<BinaryTreeNode<std::string>>(hac.clustering_step(i)));
    core::algorithms::trees::collect_leaf_elements(c_node, elements);
    std::string a = std::accumulate(elements.begin(), elements.end(), std::string(""));
    a.erase(std::remove(a.begin(), a.end(), ' '), a.end());
    std::sort(a.begin(), a.end());
    std::cout <<  "Clustering step: "<<i<<" : ";
    std::cout << a << "\n";
  }

  // --- write the clustering steps to a stream
  std::ostringstream sso;
  hac.write_merging_steps(sso);
  std::cout <<sso.str();

  // --- get medoid element for of the clusters created at distance d = 1.0
  auto clusters = hac.get_clusters(1.0, 2);
  for (core::index2 i = 0; i < 4; i++)
    std::cout << medoid_by_average_distance<float, std::string, DistanceByValues<float> >(clusters[i], d).medoid << "\n";
}
