#include <iostream>
#include <algorithm>
#include <set>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/sasa.hh>

#include <utils/exit.hh>

std::string program_info = R"(

Calculates Solvent Accessible Surface Area (SASA) for every atom in the input structure for the probe sphere
with the given radius (in Angstroms) and number of dots (n_dots) used to approximate surface area.
Resulting values will be stored as B-factor values in PDB file.

Default probe radius is 1.6 Angstroms, the program uses 960 dots by default

USAGE:
    ./ap_sasa input.pdb probe-radius n-dots

EXAMPLE:
    ./ap_sasa 2gb1.pdb 1.6

REFERENCE:
Lee, Byungkook, Frederic M. Richards. "The interpretation of protein structures: estimation of static accessibility."
JMB 55 (1971): 379-IN4.  doi:10.1016/0022-2836(71)90324-X

Shrake, A., J. A. Rupley. "Environment and exposure to solvent of protein atoms. Lysozyme and insulin."
JMB 79(1973): 351-371. doi:10.1016/0022-2836(73)90011-9.

)";


/** @brief Calculates Solvent Accessible Surface Area for every atom in the input structure
 *  
 *
 * CATEGORIES: core::calc::structural::shrake_rupley_sasa
 * KEYWORDS: PDB input; structural properties
 * GROUP: Structure calculations;
 */
int main(const int argc, const char* argv[]) {

  if (argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io;
  Pdb reader(argv[1], all_true(is_not_water,is_not_alternative)); // --- file name (PDB format, may be gzip-ped)
  double probe_radius = (argc > 2) ? atof(argv[2]) : 1.6;
  int n_dots = (argc > 3) ? atoi(argv[3]) : 960;
  using namespace core::data::structural;
  Structure_SP strctr = reader.create_structure(0);
  std::vector<double> sasa;
  core::calc::structural::shrake_rupley_sasa(*strctr, sasa, probe_radius, n_dots);
  int i = -1;
  for (auto it_atom_i = strctr->first_const_atom(); it_atom_i != strctr->last_const_atom(); ++it_atom_i) {
    (**it_atom_i).b_factor(sasa[++i]);
    std::cout << (**it_atom_i).to_pdb_line() << "\n";
  }
}
