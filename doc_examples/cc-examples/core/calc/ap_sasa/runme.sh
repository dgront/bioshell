#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ap_sasa test_inputs/2gb1.pdb"
./ap_sasa test_inputs/2gb1.pdb 1.6 > 2gb1_sasa.pdb
echo -e "Running ./ap_sasa test_inputs/5edw.pdb"
./ap_sasa test_inputs/5edw.pdb 2.2 > 5edw_sasa.pdb

mkdir -p outputs_from_test
mv 2gb1_sasa.pdb 5edw_sasa.pdb  outputs_from_test
