#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ex_peptide_hydrogen test_inputs/2gb1.pdb"
./ex_peptide_hydrogen test_inputs/2gb1.pdb
echo -e "Running ./ex_peptide_hydrogen test_inputs/5edw.pdb"
./ex_peptide_hydrogen test_inputs/5edw.pdb
echo -e "Running ./ex_peptide_hydrogen test_inputs/3dcg.pdb"
./ex_peptide_hydrogen test_inputs/3dcg.pdb
