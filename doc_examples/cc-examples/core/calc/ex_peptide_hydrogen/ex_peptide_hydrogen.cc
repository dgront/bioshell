#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/interactions/BackboneHBondCollector.hh>
#include <utils/exit.hh>

using namespace core::data::structural;
using namespace core::data::io;
using namespace core::data::basic;

std::string program_info = R"(

ex_peptide_hydrogen reconstructs peptide hydrogen atoms using BioShell algorithm, 
where amide H is placed in reference to its N atom. Resulting coordinates are printed
on the screen. The program also computes the amide-H positions using DSSP approach
and calculates the average error (in Angstroms) between the two methods.

USAGE:
    ex_peptide_hydrogen input.pdb

EXAMPLE:
    ex_peptide_hydrogen 5edw.pdb

REFERENCE:
Kabsch, Wolfgang, and Christian Sander. "Dictionary of protein secondary structure: pattern recognition
of hydrogen‐bonded and geometrical features." Biopolymers 22 (1983): 2577-2637. doi:10.1002/bip.360221211

)";

/** @brief Reconstructs peptide hydrogen atoms using two methods and compares the error between them.
 * CATEGORIES: core::calc::structural::peptide_hydrogen()
 * KEYWORDS:   PDB input; hydrogen reconstruction
 */
int main(const int argc, const char* argv[]) {
  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  core::data::io::Pdb reader(argv[1], all_true(is_not_alternative, is_not_water)); // --- Read in a PDB file
  core::data::structural::Structure_SP strctr = reader.create_structure(0); // --- create a Structure object from the first model

  double err = 0, n = 0;
  auto res_it = ++(strctr->first_residue()); // --- The residue being reconstructed
  auto prev_res_it = strctr->first_residue(); // --- preceding residue
  for (; res_it != strctr->last_residue(); ++res_it) {
    std::cerr << "# reconstructing:" << (**prev_res_it) << " and " << (**res_it) << "\n";
    if (((*prev_res_it)->residue_type().parent_id > 20) || ((*res_it)->residue_type().parent_id > 20)) {  // --- its not an amino acid
      ++prev_res_it;
      continue;
    }
    try {
      if((*prev_res_it)->owner() != (*res_it)->owner()) {
        std::cerr << "# Chain break between residues:" << (**prev_res_it) << " and " << (**res_it) << "\n";
        ++prev_res_it;
        continue;
      }
      // --- Rebuild the peptide hydrogen in a residue pointed by res_it iterator.
      // --- This method actually adds the newly created H atom to the residue
      core::calc::structural::interactions::peptide_hydrogen(*prev_res_it, *res_it);
      auto new_H = (*res_it)->find_atom_safe(" H  ");
      // --- Here we reconstruct amide H knowing the relevant atoms, but now according to the DSSP approach. Resulting H is not inserted
      auto prev_O = (*prev_res_it)->find_atom_safe(" O  ");
      auto prev_C = (*prev_res_it)->find_atom_safe(" C  ");
      auto this_N = (*res_it)->find_atom_safe(" N  ");
      PdbAtom other_H;
      core::calc::structural::interactions::peptide_hydrogen_dssp(*prev_C, *prev_O, *this_N, other_H);
      err += other_H.distance_to(*new_H);
      n++;
      for (const auto &atom : **res_it)
        std::cout << atom->to_pdb_line() << "\n"; //-- print all atoms in the current residue (in PDB format)
      ++prev_res_it; // --- advance one of the iterators by one residue; the other iterator is advanced by the loop
    } catch (utils::exceptions::AtomNotFound e) {
      std::cerr << e.what() << "\n";
      ++prev_res_it;
    }
  }
  std::cout << "# difference between two methods: " << err / n << "\n";
}

