#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ex_local_coordinates_three_atoms test_inputs/2gb1.pdb"
./ex_local_coordinates_three_atoms test_inputs/2gb1.pdb
echo -e "Running ./ex_local_coordinates_three_atoms test_inputs/5edw.pdb"
./ex_local_coordinates_three_atoms test_inputs/5edw.pdb
