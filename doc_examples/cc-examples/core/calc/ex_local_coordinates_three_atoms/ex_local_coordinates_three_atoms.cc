#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which reads a PDB file and prints local coordinates of every atom.

For every residue, a local coordinate system (LCS) is constructed based on its N, C-alpha and C atoms. Then the program prints coordinates of all the atoms of that residue defined in the respective LCS.

USAGE:
    ./ex_local_coordinates_three_atoms input.pdb
EXAMPLE:
    ./ex_local_coordinates_three_atoms 5edw.pdb

)";

/** @brief Reads a PDB file and prints local coordinates for sidechain atoms
 *
 * CATEGORIES: core::calc::structural::transformations::local_coordinates_three_atoms
 * KEYWORDS:   PDB input; local coordinates; rototranslation
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  using namespace core::data::structural;

  core::data::io::Pdb reader(argv[1]);
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

  for (auto it_resid = strctr->first_residue(); it_resid != strctr->last_residue(); ++it_resid) {

    PdbAtom_SP n = (*it_resid)->find_atom(" N  ");
    PdbAtom_SP ca = (*it_resid)->find_atom(" CA ");
    PdbAtom_SP c = (*it_resid)->find_atom(" C  ");

    if ((n == nullptr)||(ca == nullptr)||(c == nullptr)) {
        std::cout << "Missing backbone atom\n";
        continue;
    }

    core::calc::structural::transformations::Rototranslation_SP rt =
      core::calc::structural::transformations::local_coordinates_three_atoms(*n,*ca,*c);
    Vec3 tmp_atom;
    for (auto i_atom : **it_resid) {
      tmp_atom = *i_atom;
      rt->apply(*i_atom);
      std::cout << i_atom->to_pdb_line() << "\n";
      // --- Here we test if the inverse transformation really moves an atom to its original location
      rt->apply_inverse(*i_atom);
      if(tmp_atom.distance_to(*i_atom)>0.001)
        throw std::runtime_error("Incorrect position after transformation!");
    }
  }
}
