#include <math.h>

#include <iostream>
#include <random>

#include <core/data/io/DataTable.hh>
#include <core/calc/statistics/Random.hh>
#include <core/calc/statistics/BivariateNormal.hh>
#include <core/calc/statistics/RobustDistributionDecorator.hh>

std::string program_info = R"(

Estimates parameters of a two-dimensional Gaussian distribution

The program expects a file with columns of real values; based on them parameters of the distributions are estimated.
Otherwise the example withdraws 10000 random numbers from a normal distribution and later it estimates
a normal distribution from the sample.

USAGE:
    ex_BivariateNormal infile [x_column y_column]

EXAMPLE:
 ./ex_BivariateNormal bivariate_normal.dat 0 1
    
where x_column y_column are optional parameters that indicate which columns should be used for estimation;
by default columns 0 and 1 are used.

)";

/** @brief Estimates parameters of a two-dimensional Gaussian distribution
 *
 * CATEGORIES: core::calc::statistics::BivariateNormal; core::calc::statistics::Random
 * KEYWORDS:   statistics; random numbers; estimation
 */
int main(const int argc, const char* argv[]) {

  using namespace core::calc::statistics;

  std::vector<std::vector<double> > data_2D;
  std::vector<double> row(2);
  if (argc == 1) { // --- No input file? Generate random data for the test

    std::cerr << program_info;

    Random rd = core::calc::statistics::Random::get();
    rd.seed(12345); // --- seed the generator for repeatable results
    unsigned N = 100000; //--- the number of random points to use in tests
    core::calc::statistics::NormalRandomDistribution<double> nX(1.0, 2.5);
    core::calc::statistics::NormalRandomDistribution<double> nY(2.0, 0.7);

    for (unsigned i = 0; i < N; ++i) { // --- get a random sample in 2D
      double x = nX(rd);
      double y = nY(rd);
      row[0] = x - y; // --- make X variable correlated with Y
      row[1] = x + y;
      data_2D.push_back(row);
    }
  } else {
    core::data::io::DataTable in_data(argv[1]);
    int column_x_id = 0, column_y_id = 1;
    if (argc > 3) {
      column_x_id = utils::from_string<int>(argv[2]);
      column_y_id = utils::from_string<int>(argv[3]);
    }
    for (const auto &data_row : in_data) {
      row[0] = data_row.get<double>(column_x_id);
      row[1] = data_row.get<double>(column_y_id);
      data_2D.push_back(row);
    }
  }

  std::vector<double> initial_parameters{0.0, 0.0, 1.0, 1.0, 1.0};
  // --- Here we declare a 2D normal distribution ...
  core::calc::statistics::BivariateNormal n(initial_parameters);
  // ... and estimate its parameters
  const std::vector<double> & params = n.estimate(data_2D);
  // show the estimated parameters of the distribution
  std::cout << "          estimated parameters: " << params[0] << " " << params[1] << " " << params[2] << " " << params[3] << " " << params[4] << "\n";
  // ... and estimate its parameters
  core::calc::statistics::RobustDistributionDecorator<BivariateNormal> rn(initial_parameters, 0.05);
  const std::vector<double> & params_r = rn.estimate(data_2D);
  // show the estimated parameters of the distribution
  std::cout << " estimated parameters (robust): " << params_r[0] << " " << params_r[1] << " " << params_r[2] << " " << params_r[3] << " " << params_r[4] << "\n";
}








