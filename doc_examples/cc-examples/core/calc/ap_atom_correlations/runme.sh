#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "#Running ./ap_atom_correlations test_inputs/2kwi.pdb | sort -nk 9 | grep -v e | tail -200"
./ap_atom_correlations test_inputs/2kwi.pdb | sort -nk 9 | grep -v e | tail -200
