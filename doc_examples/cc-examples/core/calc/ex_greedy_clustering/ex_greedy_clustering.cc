#include <vector>
#include <iostream>
#include <random>

#include <core/calc/clustering/greedy_clustering.hh>

/// A distance operator calculates the distance between two points indexed by <code>i</code> and <code>j</code>
struct PointDistance {
  std::vector<double> & points;

  /// Constructor just copies the reference of a data vector
  PointDistance(std::vector<double> & pts) : points(pts) {}
  /// Call-operator computes the distance
  double operator()(const size_t i,const size_t j) const { return fabs(points[i]-points[j]); }
};

/** @brief Example showing how to use greedy clustering method.
 *
 * CATEGORIES: core::calc::clustering::greedy_clustering()
 * KEYWORDS: clustering
 */
int main(const int argc, const char* argv[]) {

  // --- Prepare random number generators
  std::mt19937 gen(1234567);
  std::normal_distribution<> d1(10.5, 2.0);
  std::normal_distribution<> d2(-0.5, 2.0);
  std::vector<double> data;

  // --- Generate 20 random values
  for (unsigned short i = 0; i < 10; ++i) {
    data.push_back(d1(gen));
    data.push_back(d2(gen));
  }

  std::vector<size_t> clusters; // --- Clusters will be stored here
  std::vector<size_t> cluster_members; // --- vector for members assigned to clusters
  PointDistance distance(data); // --- instance of the distance operator
  core::calc::clustering::greedy_clustering(data,distance,5.0,clusters,cluster_members);

  // --- Show results
  std::cout << "n_clusters: " << clusters.size() << "\n";
  std::cout << "cluster assignment: ";
  for (const unsigned short i : cluster_members) std::cout << i << " ";
  std::cout << "\n";
}
