#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

./strc -in::pdb=test_inputs/4rm4.pdb -select::chains=A -out::pdb=4rm4A.pdb
./strc -in::pdb=test_inputs/5ofq.pdb -select::chains=A -out::pdb=5ofqA.pdb

echo -e "# Running ap_superimpose_pdb_by_ligand 4rm4A.pdb HEM 5ofqA.pdb" 
./ap_superimpose_pdb_by_ligand 4rm4A.pdb HEM 5ofqA.pdb
rm  4rm4A.pdb 5ofqA.pdb

mkdir -p outputs_from_test
mv out.pdb outputs_from_test
