#include <memory>
#include <iostream>
#include <vector>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/protocols/PairwiseCrmsd.hh>
#include <utils/LogManager.hh>

using namespace core::data::structural;

std::string program_info = R"(

Superimposes protein structures by matching ligand molecules.

All the given protein structures must contain the same ligand molecule, every time in the same conformation.
The program calculates a transformation (rotation-translation) that superimposes that ligand from input structures
on the same ligand molecule found in the native PDB. The transformation is then used to rototranslate whole protein
structures. Results is written to "out.pdb" file

USAGE:
     ./ap_superimpose_pdb_by_ligand native_pdb ligand_name pdb_file_1 [pdb_file_2 ...]

EXAMPLE:
    ./ap_superipose_pdb_by_ligand 4rm4A.pdb HEM 5ofqA.pdb
)";

/** @brief Superimposes protein structures by matching ligand molecules.
 * *
 * CATEGORIES: core/calc/structural/transformations/PairwiseCrmsd
 * KEYWORDS:   PDB input; rototranslation; superimposition; crmsd; docking
 * GROUP: Structure calculations;
 */
int main(const int argc, const char *argv[]) {

  if(argc < 4) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  utils::LogManager::get().FINE();
  selectors::AtomSelector_SP selector = std::make_shared<selectors::SelectResidueByName>(argv[2]); // --- select a ligand residue by its 3-letter code

  core::data::io::Pdb read_native(argv[1], core::data::io::keep_all); // --- Read the native (reference) structure, keep all atoms
  Structure_SP native = read_native.create_structure(0);
  std::vector<Structure_SP> models; // --- Container for targets to be superimposed
  for (int i = 3; i < argc; ++i) {
    core::data::io::Pdb reader(argv[i], core::data::io::keep_all);
    for (int j = 0; j < reader.count_models(); ++j) // --- Read all models from each target PDB file
      models.push_back(reader.create_structure(j));
  }

  selectors::AtomSelector_SP select_all = std::make_shared<selectors::AtomSelector>();
  core::protocols::PairwiseCrmsd rms_calc(models, selector);
  std::shared_ptr<std::ostream> out = std::make_shared<std::ofstream>("out.pdb");
  rms_calc.calculate(native, out);
}
