#include <iostream>

#include <core/index.hh>
#include <core/calc/numeric/LBFGS.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to use Broyden–Fletcher–Goldfarb–Shanno (BFGS) function minimizer.

USAGE:
./ex_LBFGS

REFERENCE:
Fletcher, Roger. Practical methods of optimization. John Wiley & Sons, 2013.

)";

using namespace core::calc::numeric;

class TestFunction : public DerivableFunction<double> {
public:

  double a, b;

  TestFunction() : a(1.0), b(30.0) {}

  virtual double operator()(const std::vector<double> &x) {
    return (a - x[0]) * (a - x[0]) + b * (x[1] - x[0] * x[0]) * (x[1] - x[0] * x[0]);
  }

  virtual double operator()(const std::vector<double> &x, std::vector<double> &gradient) {

    float t1 = a - x[0];
    float t2 = b * (x[1] - x[0] * x[0]);
    gradient[1] = 2 * b * t2;
    gradient[0] = -2.0 * (x[0] * gradient[1] + t1);

    return t1 * t1 + t2 * t2;
  }

  core::index2 dim() const { return 2; }
};

/** @brief Example shows how to use BFGS function minimizer
 *
 *
 * CATEGORIES: core/calc/numeric/Bfgs
 * KEYWORDS:   numerical methods
 */
int main(const int argc, const char *argv[]) {

  TestFunction f;
  LBFGS<double> minimizer(2);

  std::vector<double> x{0.0,0.0};
  double val;
  core::index2 nit = minimizer.minimize(f, x, val);

  std::cout << "Minimum value " << val << " found at [";
  for (const double v:x) std::cout << v << ' ';
  std::cout << "] after " << nit << " iterations\n";
  return 0;
}








