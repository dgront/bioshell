#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo "# Running ./ap_contact_map_overlap SC ./test_inputs/2gb1.pdb ./test_inputs/2gb1-model1.pdb 4.5"
./ap_contact_map_overlap SC ./test_inputs/2gb1.pdb ./test_inputs/2gb1-model1.pdb 4.5

echo "# Running ./ap_contact_map_overlap SC ./test_inputs/2gb1.pdb ./test_inputs/2gb1-model2.pdb 4.5"
./ap_contact_map_overlap SC ./test_inputs/2gb1.pdb ./test_inputs/2gb1-model2.pdb  4.5

echo "# Running ./ap_contact_map_overlap SC ./test_inputs/2kwi.pdb ./test_inputs/2kwi.pdb 4.5"
./ap_contact_map_overlap SC ./test_inputs/2kwi.pdb ./test_inputs/2kwi.pdb 4.5

echo "# Running ./ap_contact_map_overlap SC ./test_inputs/2kwi.pdb 4.5"
./ap_contact_map_overlap SC ./test_inputs/2kwi.pdb 4.5
