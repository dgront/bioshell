#include <vector>
#include <algorithm>

#include <core/data/io/Pdb.hh>
#include <core/calc/structural/interactions/ContactMap.hh>

#include <utils/exit.hh>

std::string program_info = R"(

ap_contact_map_overlap calculates overlap between contact maps calculated for two (or more) structures.

The overlap, defined as Jaccard coefficient, is computed between the native structure
and every model found in models.pdb; map-type can take one of the following values:
CA CB and SC for C-alpha, C-beta and all atom side chain, respectively.
Contact is recorded when any selected atoms from two different residues are closer to each other than
the give cutoff.

If only one PDB file is given, the program computes calculates overlap between the first and any other
model found in that file

USAGE:
    ap_contact_map_overlap map-type native.pdb models.pdb cutoff

EXAMPLE:
    ap_contact_map_overlap SC 2gb1.pdb 2gb1-model1.pdb 4.5

REFERENCE:
    https://en.wikipedia.org/wiki/Jaccard_index
)";

/** @brief Calculates overlap between contact maps calculated for two (or more) structures
 *
 * CATEGORIES: core::calc::structural::ContactMap
 * KEYWORDS: PDB input; contact map
 * GROUP: Structure calculations;
 */
int main(const int argc, const char *argv[]) {

  if (argc < 4) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io; // PDB is from this namespace
  using namespace core::data::structural;
  using namespace core::data::structural::selectors;    // --- for all structural selectors
  using namespace core::calc::structural::interactions;

  AtomSelector_SP selector = std::make_shared<IsSC>();
  core::data::io::PdbLineFilter filter = core::data::io::is_not_water;
  if (std::strcmp(argv[1],"CA")==0 ) {
    selector = std::make_shared<IsNamedAtom>(" CA ");
    core::data::io::PdbLineFilter filter = core::data::io::is_ca;
  }
  if (std::strcmp(argv[1],"CB")==0) {
    core::data::io::PdbLineFilter filter = core::data::io::is_cb;
    selector = std::make_shared<IsNamedAtom>(" CB ");
  }
  double cutoff = utils::from_string<double>(argv[(argc==5) ? 4 : 3]); // The third/fourth parameter is the contact distance (in Angstroms)

  // --- This is the case when user gave a reference structure (e.g. the native one)
  if (argc == 5) {
    Pdb pdb_native = Pdb(argv[2], filter);
    core::data::structural::Structure_SP reference_structure = pdb_native.create_structure(0);
    ContactMap_SP reference_map = std::make_shared<ContactMap>(*reference_structure, cutoff, selector);

    Pdb models_pdb = Pdb(argv[3], filter);
    ContactMap cmap(*reference_structure, cutoff, selector);
    std::vector<std::pair<core::index2, core::index2>> contacts;
    for (core::index4 i = 0; i < models_pdb.count_models(); ++i) {
      models_pdb.fill_structure(i,*reference_structure);
      ContactMap cmap(*reference_structure, cutoff, selector);
      std::cout << i << " " << reference_map->jaccard_overlap_coefficient(cmap) << "\n";
    }
  } else {
    Pdb models_pdb = Pdb(argv[2], filter);
    core::data::structural::Structure_SP structure = models_pdb.create_structure(0);
    std::vector<std::vector<std::pair<core::index2, core::index2>>> models(models_pdb.count_models());
    for (int i = 0; i < models_pdb.count_models(); ++i) {
      models_pdb.fill_structure(i,*structure);
      ContactMap cmap(*models_pdb.create_structure(i), cutoff, selector);
      cmap.nonempty_indexes(models[i]);
    }

    for (core::index4 i = 1; i < models.size(); ++i) {
      for (core::index4 j = 0; j < i; ++j)
        std::cout << i << " " << j << " "
              << core::calc::structural::interactions::jaccard_overlap_coefficient(models[i], models[j]) << "\n";
    }
  }
}
