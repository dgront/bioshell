#include <math.h>

#include <iostream>
#include <random>
#include <vector>

#include <core/calc/statistics/NormalDistribution.hh>
#include <core/calc/statistics/RobustDistributionDecorator.hh>
#include <utils/options/OptionParser.hh>

std::string program_info = R"(

Example showing how to create and use a RobustDistributionDecorator, which facilitates distribution estimation
of any probability distribution function that is defined in BioShell.

This example estimates parameters of a normal distribution from a noised data using regular and robust methods.

USAGE:
./ex_RobustDistributionDecorator [data.txt]

REFERENCE:
Kim Seong-Ju "The Metrically Trimmed Mean as a Robust Estimator of Location",
The Annals of Statistics (1992) 20 1534-1547
)";

/** @brief Example showing how to create and use a RobustDistributionDecorator
 *
 * CATEGORIES: core::calc::statistics::NormalDistribution; core::calc::statistics::RobustDistributionDecorator
 * KEYWORDS: estimation
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::calc::statistics;

  unsigned int rd = 9876543;
  std::mt19937 gen(rd);
  core::index4 N = 10000; //--- the number of random points to use in tests
  core::index4 Nnoise = 10; //--- the number of random points from the noise distribution

  // ---------- The two distributions used in this test
  std::normal_distribution<> base(2.0, 0.5); // --- the "base" distribution
  std::normal_distribution<> noise(2.0, 50); // --- the "noise" distribution

  // ----------
  std::vector<std::vector<double> > random_points;

  if (argc==1) { // ---------- Generate random sample
    for (core::index4 i = 0; i < N; ++i)
      random_points.emplace_back( std::initializer_list<double>{base(gen)} );
    for (core::index4 i = 0; i < Nnoise; ++i)
      random_points.emplace_back( std::initializer_list<double>{noise(gen)} );
  } else {    // ---------- Read data from a file
    std::fstream infile(argv[1], std::ios_base::in);
    double a;
    while (infile >> a) {
      random_points.emplace_back(std::initializer_list<double>{a});
    }
  }

  std::vector<double> init_params{1.0,0.0}; // --- initial parameters of the estimated distribution
  NormalDistribution n(init_params);
  n.estimate(random_points);
  std::cout << "Estimated:          " << n << "\n";
  RobustDistributionDecorator<NormalDistribution> rn(init_params, 0.05);
  rn.estimate(random_points);
  std::cout << "Estimated (robust): "<< rn << "\n";
  if (argc==1)  std::cout << "True distribution:  2.0   0.5\n";
}

