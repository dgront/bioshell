#include <iostream>
#include <vector>
#include <map>

#include <core/calc/statistics/Cart.hh>
#include <core/algorithms/basic_algorithms.hh>

using namespace core::calc::statistics;
using namespace core::data::io;
using namespace core::data::basic;

utils::Logger logs("ex_Cart");

/** @brief Shows how to use CART classification model
 * 
 * CATEGORIES: core::calc::statistics::Cart
 * KEYWORDS:   CART; observer
 */
int main(const int argc, const char *argv[]) {

  DataTable dt;
  dt.load(argv[1]);

  std::vector<LabelledObservationVector_SP> observations;
  std::vector<std::string> class_names;
  std::vector<core::index2> class_ids;
  std::map<std::string, core::index2> class_to_id;

  // --- First find distinct labels
  core::index2 i_class = 0;
  for (const TableRow &tr : dt) {
    if (class_to_id.find(tr.back()) == class_to_id.end()) {
      class_ids.push_back(i_class);
      class_to_id[tr.back()] = i_class;
      ++i_class;
    }
  }

  for (const TableRow &tr : dt)
    observations.push_back(std::make_shared<LabelledObservationVector>(tr, class_to_id[tr.back()], 0, tr.size() - 2));

  // --- print some debug info : known classes etc.
  logs << utils::LogLevel::INFO << "classification into " << class_ids.size() << " classes\n";
  if (logs.is_logable(utils::LogLevel::INFO)) {
    logs << utils::LogLevel::INFO << "Known classes:\n";
    core::index1 icol = 0;
    for (auto c:class_to_id) {
      logs << c.first << " ";
      ++icol;
      if (icol % 10 == 0) logs << "\n";
    }
    logs << "\n";
  }

  // --- create the CART classifier and train it
  Cart cart(class_ids);
  cart.train(observations);
  std::cout << cart;

  // --- test the classifier for the training data set
  core::index4 n_ok = 0;
  for(const auto o : observations) {
    n_ok += (cart.classify(o) == o->label());
  }
  std::cout << utils::string_format("# classification test:\n# success rate: %d of %d (%6.2f%%)\n", n_ok, observations.size(),
    100.0*n_ok / float(observations.size()));
}

