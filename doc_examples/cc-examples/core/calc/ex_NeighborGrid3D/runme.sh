#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "# Running ./ex_NeighborGrid3D test_inputs/2gb1.pdb A:12"
./ex_NeighborGrid3D test_inputs/2gb1.pdb 2.0 A:12
