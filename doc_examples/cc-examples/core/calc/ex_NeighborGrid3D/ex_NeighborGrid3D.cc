#include <iostream>
#include <algorithm>
#include <set>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/NeighborGrid3D.hh>

#include <utils/exit.hh>
#include <utils/LogManager.hh>

std::string program_info = R"(

ex_NeighborGrid3D finds possible structural neighbors of a given residue using a 3D hashing grid


USAGE:
    ex_NeighborGrid3D 2gb1.pdb 4.0 A:12

where 2gb1.pdb is an input file, 4.0 - grid size,  A:12 - selector of a query residue

)";


/** @brief Finds possible structural neighbors of a given atom
 *  *
 * CATEGORIES: core::calc::structural::NeighborGrid3D
 * KEYWORDS: PDB input; structure selectors
 */
int main(const int argc, const char* argv[]) {

  if (argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  utils::LogManager::FINE();

  using namespace core::data::io;
  Pdb reader(argv[1], all_true(is_not_water,is_not_alternative)); // --- file name (PDB format, may be gzip-ped)

  utils::Logger logs("ex_NeighborGrid3D");
  using namespace core::data::structural;
  Structure_SP strctr = reader.create_structure(0);
  selectors::SelectChainResidues select_query(argv[3]);
  // --- Here we find the query residue, selected by a selector string
  Residue_SP query_resid = nullptr;
  for(auto it=strctr->first_residue();it!=strctr->last_residue();++it) {
    if (select_query(*it)) {
      query_resid = *it;
      logs << utils::LogLevel::INFO << "selecting spatial neighbors of " <<
           " " << query_resid->residue_type().code3 << query_resid->residue_id() << "\n";
      break;
    }
  }

  // --- Create a 3D grid object
  double grid_mesh = atof(argv[2]);
  core::calc::structural::NeighborGrid3D grid(*strctr,grid_mesh);

  // --- Print content of the grid
  std::cout << "# Atoms as they are located on the grid\n";
  std::cout << "# Grid center is: " << grid.cx() << " " << grid.cy() << " " << grid.cz() << "\n";
  core::index2 ix, iy, iz;
  for(const auto & hash : grid.filled_cells()) {
    grid.xyz_from_hash(hash, ix, iy, iz);
    for(const PdbAtom_SP & at : grid.get_cell(hash)) {
      std::cout << "# " << hash << " " << at->owner()->residue_type().code3 << at->owner()->id() << " " << *at
                << " ix: " << ix << " iy: " << iy << " iz: " << iz << "\n";
    }
  }

  // --- Print neighbor cells of the selected residue
  core::index4 hash_ca = grid.hash(*query_resid->find_atom_safe(" CA "));
  std::vector<core::index4> neighb_hash;
  grid.get_neighbor_cells(hash_ca, neighb_hash);
  std::cout << "# neighbors of a cell " << hash_ca << "\n# ";
  for (core::index4 n:neighb_hash) std::cout << " " << n;
  std::cout << "\n";

  // --- Mark the selection on a PDB file by setting B-factor to 10.0 (all other atoms to 0.0)
  float max_distance = 0;
  std::vector< PdbAtom_SP> result;
  for(auto it=strctr->first_atom();it!=strctr->last_atom();++it) (**it).b_factor(0.0);
  for(PdbAtom_SP a : *query_resid) {
    result.clear();
    grid.get_neighbors(*a,result);
    for(auto atom_sp:result) {
      atom_sp->b_factor(10.0);
      max_distance = std::max(max_distance,atom_sp->distance_to(*a));
    }
  }

  for(auto it=strctr->first_atom();it!=strctr->last_atom();++it)
    std::cout << (**it).to_pdb_line() << "\n";
  std::cout << "# Max distance: " <<max_distance << "\n";
}
