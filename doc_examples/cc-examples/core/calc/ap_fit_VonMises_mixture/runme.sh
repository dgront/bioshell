#!/bin/bash

echo -e "#Running ./ap_fit_VonMises_mixture test_inputs/THR_chi1.dat -1.05 30 0.0 30 1.05 30"
./ap_fit_VonMises_mixture test_inputs/THR_chi1.dat -1.05 30 -3.0 30 1.05 30

echo -e "#Running ./ap_fit_VonMises_mixture test_inputs/THR_chi1.dat 3"
./ap_fit_VonMises_mixture test_inputs/THR_chi1.dat 3
