#include <math.h>

#include <iostream>
#include <random>

#include <core/algorithms/basic_algorithms.hh>
#include <core/algorithms/Combinations.hh>
#include <core/calc/statistics/NormalDistribution.hh>
#include <core/calc/statistics/expectation_maximization.hh>
#include <core/calc/numeric/numerical_integration.hh>
#include <core/calc/statistics/VonMisesDistribution.hh>
#include <core/calc/statistics/RobustDistributionDecorator.hh>
#include <core/data/io/DataTable.hh>

#include <utils/exit.hh>

std::string program_info = R"(

ap_fit_VonMisses_mixture reads a text file with 1D arbitrary observations in degrees
and fits a mixture of VonMisses distributions to the data. The number of distributions to fit
is determined by the starting parameters: \f$\mu\f$ and \f$\kappa\f$ for each distribution. Alternatively,
the program can scan the parameter space automatically, when only the number of distributions is given
at the input.

USAGE:
    ap_fit_VonMises_mixture chi_angles.dat  mu kappa [mu2 kappa2 ...]
    ap_fit_VonMises_mixture chi_angles.dat n_dist

EXAMPLES:
    ap_fit_VonMises_mixture chi_angles.dat  -1.05 30 -3.0 30 1.05 30
    ap_fit_VonMises_mixture chi_angles.dat 3

)";

/** @brief Reads a file with 1D data and estimates a mixture of VonMisesDistribution based on these observations.
 *
 * CATEGORIES: core::calc::statistics::VonMissesDistribution
 * KEYWORDS:   statistics; estimation; expectation-maximization
 * GROUP: Statistics;
 * IMG: ap_fit_VonMises_mixture.png
 * IMG_ALT: Distribution of Chi1 angles of THR side chains approximated with a mixture of three von Mises distribution
 */
int main(const int argc, const char *argv[]) {

  if (argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  using namespace core::calc::statistics;

  double deg_to_rad = M_PI / 180.0;

  // ---------- Read in data points (your observations) from a file
  std::vector<std::vector<double>> data_points;
  core::data::io::DataTable in(argv[1]);
  double min = 180, max = -180; // This is to detects whether angle values are in radians or in degrees
  for (const auto &row : in) {
    std::vector<double> d;
    double v = row.get<double>(0);
    if (v < min) min = v;
    if (v > max) max = v;
    d.push_back(v);
    data_points.push_back(d);
  }
  if (((min < -M_PI) || (max > M_PI))) std::cerr << "Converting from degrees to radians!\n";
  else deg_to_rad = 1.0;
  for (std::vector<double> &d : data_points) d[0] *= deg_to_rad;

  std::vector<double> default_params{0.0, 100.0};

  // ---------- RobustDistributionDecorator object for each distribution
  core::index1 n_distributions = atoi(argv[2]);
  std::vector<RobustDistributionDecorator<VonMisesDistribution>> r_distributions_1D;
  std::vector<RobustDistributionDecorator<VonMisesDistribution>> r_best_distributions;

  std::vector<std::vector<std::vector<double>>> initial_parameters; // --- Initial parameters for fitting
  std::vector<core::index1> distribution_index; // --- Resulting assignment of every data point to a distribution
  std::vector<core::index1> best_assignment; // --- Resulting assignment of every data point to a distribution

  // --- This is the case when user provided initial parameters for fitting Von Mises distribution
  if (argc >= 4) {
    // ---------- Read parameters from cmdline and create distribution objects
    std::vector<std::vector<double>> params;
    for (int i = 2; i < argc; i += 2) {
      params.push_back(std::vector<double>{atof(argv[i]) * deg_to_rad, atof(argv[i + 1])});
      r_distributions_1D.emplace_back(RobustDistributionDecorator<VonMisesDistribution>(params.back()));
      r_best_distributions.emplace_back(RobustDistributionDecorator<VonMisesDistribution>(default_params));
      initial_parameters.push_back(params);
    }
    initial_parameters.push_back(params);
  } else {
    // --- This is the case when user provided the number of distributions to be fit automatically
    n_distributions = atoi(argv[2]);
    for (size_t i = 0; i < n_distributions; ++i) {
      r_distributions_1D.emplace_back(RobustDistributionDecorator<VonMisesDistribution>(default_params));
      r_best_distributions.emplace_back(RobustDistributionDecorator<VonMisesDistribution>(default_params));
    }

    std::vector<double> random_starts{-1.0, -0.8, -0.6, -0.4, -0.2, 0.0, 0.2, 0.4, 0.6, 0.8, 1.0}; // multiplicity of PI

    core::algorithms::Combinations<double> generator(n_distributions, random_starts);
    std::vector<double> combination(n_distributions);
    std::vector<std::vector<double>> params;
    while (generator.next(combination)) {
      params.clear();
      for (size_t i_distr = 0; i_distr < n_distributions; ++i_distr)
        params.push_back(std::vector<double>{combination[i_distr] * M_PI, 100.0});
      initial_parameters.push_back(params);
    }
  }

  double best_likelihood = -std::numeric_limits<double>::max();
  // ---------- Run Expectation-Maximization algorithm
  for (size_t i_start = 0; i_start < initial_parameters.size(); ++i_start) { // --- iterate over starting points
    for (size_t i_distr = 0; i_distr < r_distributions_1D.size(); ++i_distr)  // --- loop over distributions to set each starting point
      r_distributions_1D[i_distr].copy_parameters_from(initial_parameters[i_start][i_distr]);
    double score = expectation_maximization(data_points, r_distributions_1D, distribution_index, 0.1, 100);
    if (score > best_likelihood) {
      best_likelihood = score;
      for (size_t i = 0; i < n_distributions; ++i)
        r_best_distributions[i].copy_parameters_from(r_distributions_1D[i].parameters());
      best_assignment.swap(distribution_index);
      std::cerr << "# Best likelihood so far: " << best_likelihood << "\n";
    }
  }
  std::map<core::index1, core::index4> counts;
  core::algorithms::count_distinct(best_assignment, counts);
  const double total = std::accumulate(std::begin(counts), std::end(counts), 0,
                                       [](const size_t previous, decltype(*counts.begin()) p) {
                                           return previous + p.second;
                                       });

  std::cout << "# Best likelihood " << best_likelihood << "\n# Estimated distributions:\n";
  for (size_t i_distr = 0; i_distr < r_distributions_1D.size(); ++i_distr) {
    std::cout << counts[i_distr] / total << " " << r_best_distributions[i_distr] << "\n";
  }
}


