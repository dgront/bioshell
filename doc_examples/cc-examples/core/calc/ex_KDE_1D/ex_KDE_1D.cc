#include <iostream>
#include <iomanip>
#include <core/data/io/Pdb.hh>

#include <core/data/io/DataTable.hh>
#include <core/calc/statistics/KDE_1D.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Reads one column of observations and calculates Kernel Density Estimator (KDE) with given bandwidth value for the data.
 If optional parameters min and max are given, it defines the evaluation range. The last optional argument
is the word 'periodic' to treat the estimated distribution as periodic.

USAGE:
    ex_KDE_1D normal.txt 0.25 [min max periodic]

REFERENCE:
Davis, Richard A., Keh-Shin Lii,  Dimitris N. Politis. "Remarks on some nonparametric estimates of a density function."
Selected Works of Murray Rosenblatt. Springer, New York, NY, 2011. 95-100. doi:10.1214/aoms/1177728190.

Parzen, Emanuel. "On estimation of a probability density function and mode."
The annals of mathematical statistics 33.3 (1962): 1065-1076.

)";

/** @brief Reads one column of observations and calculates Kernel Density Estimator (KDE) for the data
 *
 * CATEGORIES: core/calc/statistics/KDE_1D
 * KEYWORDS:   statistics; estimation; data table
 */
int main(const int argc, const char* argv[]) {

  if(argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  core::data::io::DataTable input_data(argv[1]); // --- Here we read a text file with data values in columns
  std::vector<double> col1; // --- empty vector to hold the data from a file

  bool is_periodic = (argc > 2 && argv[argc-1][0] == 'p');
  // --- Here we read the input file and create a KDE estimator for the data in the first column
  core::calc::statistics::KDE_Kernel kernel_type =  core::calc::statistics::normal_kernel;
  core::calc::statistics::KDE_1D kde(input_data.column(0, col1), atof(argv[2]), kernel_type, is_periodic);

  // --- find max and min value in the file; tabulate the data in 100 steps
  double min = (argc < 5) ? *std::min_element(col1.begin(), col1.end()) : atof(argv[3]);
  double max = (argc < 5) ? *std::max_element(col1.begin(), col1.end()) : atof(argv[4]);
  double step = (max - min) / 300.0;
  for (double x = min; x <= max; x += step) std::cout << utils::string_format("%8.3f %8.3f\n", x, kde(x, is_periodic));
}
