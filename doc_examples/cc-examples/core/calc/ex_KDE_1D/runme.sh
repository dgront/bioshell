#!/bin/bash

echo -e "#Running ./ex_KDE_1D test_inputs/normal.txt 0.4"
./ex_KDE_1D test_inputs/normal.txt 0.4

echo -e "#Running ./ex_KDE_1D test_inputs/THR_chi1.dat 0.1 -3.14159 3.13149 periodic"
./ex_KDE_1D test_inputs/THR_chi1.dat 0.1 -3.14159 3.13149 periodic
