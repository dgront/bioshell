#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which reads a PDB file and prints local coordinates for side chain atoms. The example uses
BBQ local coordinate system definition, based on three subsequent alpha carbon atoms.

USAGE:
./ex_local_BBQ_coordinates input.pdb

EXAMPLE:
./ex_local_BBQ_coordinates 5edw.pdb

REFERENCE:
D. Gront, S. Kmiecik, A. Kolinski . "Backbone building from quadrilaterals: A fast and accurate algorithm
for protein backbone reconstruction from alpha carbon coordinates."
J Comput Chem (2007) 1593-1597. doi:10.1002/jcc.20624

)";

/** @brief Reads a PDB file and prints local coordinates for side chain atoms
 *
 * CATEGORIES: core::calc::structural::transformations::local_BBQ_coordinates
 * KEYWORDS:   PDB input; local coordinates
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  using namespace core::data::structural;
  using namespace core::calc::structural::transformations;

  core::data::io::Pdb reader(argv[1]);
  Structure_SP strctr = reader.create_structure(0);

  Rototranslation_SP rt = nullptr;
  for(auto a_chain : *strctr) {
    for (core::index2 i_residue = 1; i_residue < a_chain->count_residues() - 1; ++i_residue) {
      const Residue & the_residue = *(*a_chain)[i_residue];
      const Residue & prev_residue = *(*a_chain)[i_residue - 1];
      const Residue & next_residue = *(*a_chain)[i_residue + 1];
      try {
        rt = local_BBQ_coordinates(*prev_residue.find_atom_safe(" CA "),
          *the_residue.find_atom_safe(" CA "), *next_residue.find_atom_safe(" CA "));
      } catch (utils::exceptions::AtomNotFound ex) {
        i_residue+=2;
        continue;
      }
      Vec3 tmp_atom;
      for (auto i_atom : the_residue) {
        tmp_atom = *i_atom;
        rt->apply(*i_atom);
        std::cout << i_atom->to_pdb_line() << "\n";
        // --- Here we test if the inverse transformation really moves an atom to its original location
        rt->apply_inverse(*i_atom);
        if(tmp_atom.distance_to(*i_atom)>0.001)
          throw std::runtime_error("Incorrect position after transformation!");
      }
    }
  }
}

