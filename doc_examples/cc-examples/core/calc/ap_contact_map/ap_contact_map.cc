#include <iostream>

#include <core/index.hh>
#include <core/data/io/Pdb.hh>
#include <core/calc/structural/interactions/ContactMap.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <utils/exit.hh>

std::string program_info = R"(

ap_contact_map calculates a contact map for a given protein structure

If a multi-model PDB file was given, the program prints for every contact in how many models
the contact was observed. The program can calculate the contacts either on side chains,
on alpha carbon or on beta carbon atoms.

USAGE:
    ap_contact_map  atom-filter input.pdb cutoff

EXAMPLE:
    ap_contact_map  CA 2kwi.pdb 4.5

where 2kwi.pdb is the input file and 4.5 the contact distance in Angstroms. CA defines the contact map type;
allowed options are: CA CB and SC for C-alpha, C-beta and all atom side chain, respectively

)";

/** @brief Calculates a contact map for a given protein structure
 *
 * CATEGORIES: core::calc::structural::ContactMap
 * KEYWORDS: PDB input; contact map
 * GROUP: Structure calculations;
 * IMG: ap_contact_map.png
 * IMG_ALT: Contact map calculated for 2KWI protein structure solved by NMR. The 2KWI deposit holds 51 models, the color scale shows how popular is a given contact among the models
 */
int main(const int argc, const char* argv[]) {

  if (argc < 4) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  core::data::structural::selectors::AtomSelector_SP selector
      = std::make_shared<core::data::structural::selectors::IsSC>();
  core::data::io::PdbLineFilter filter = core::data::io::is_not_water;
  if (std::strcmp(argv[1],"CA")==0 ) {
    selector = std::make_shared<core::data::structural::selectors::IsNamedAtom>(" CA ");
    core::data::io::PdbLineFilter filter = core::data::io::is_ca;
  }
  if (std::strcmp(argv[1],"CB")==0) {
    core::data::io::PdbLineFilter filter = core::data::io::is_cb;
    selector = std::make_shared<core::data::structural::selectors::IsNamedAtom>(" CB ");
  }

  double cutoff = utils::from_string<double>(argv[3]); // The third parameter is the contact distance (in Angstroms)
  core::data::io::Pdb reader(argv[2],filter); // --- file name (PDB format, may be gzip-ped)

  core::data::structural::Structure_SP structure = reader.create_structure(0);
  core::calc::structural::interactions::ContactMap cmap(*structure, cutoff, selector);
  for (int i_model = 1; i_model < reader.count_models(); ++i_model) {
    reader.fill_structure(i_model, *structure);
    cmap.add(*structure);
  }

  std::vector<std::pair<core::index2, core::index2>> contacts;
  cmap.nonempty_indexes(contacts);

  for(const std::pair<core::index2,core::index2> ij : contacts) {
    core::index2 i_res = ij.first;
    core::index2 j_res = ij.second;
    std::cout << utils::string_format("%4d %4s %4d%c %4d %4s %4d%c %d\n", i_res,
      cmap.residue_index(i_res).chain_id.c_str(),
      cmap.residue_index(i_res).residue_id, cmap.residue_index(i_res).i_code,
      j_res, cmap.residue_index(j_res).chain_id.c_str(),
      cmap.residue_index(j_res).residue_id, cmap.residue_index(j_res).i_code,
      cmap.at(i_res, j_res, 0));
  }
}
