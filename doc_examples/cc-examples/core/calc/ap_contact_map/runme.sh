#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
mkdir -p outputs_from_test

echo -e "#Running CA ./ap_contact_map test_inputs/2kwi.pdb 8.0 > 2kwi.ca.cmap"
./ap_contact_map CA test_inputs/2kwi.pdb 8.0 > 2kwi.ca.cmap
echo -e "#Running CB ./ap_contact_map test_inputs/2kwi.pdb 6.0 > 2kwi.cb.cmap"
./ap_contact_map CB test_inputs/2kwi.pdb 6.0 > 2kwi.cb.cmap
echo -e "#Running SC ./ap_contact_map test_inputs/2kwi.pdb 4.5 > 2kwi.sc.cmap"
./ap_contact_map SC test_inputs/2kwi.pdb 4.5 > 2kwi.sc.cmap

echo -e "#Running -- ./ap_contact_map test_inputs/1cey.pdb 4.5 > 1cey.sc.cmap"
./ap_contact_map -- test_inputs/1cey.pdb 4.5 > 1cey.sc.cmap

mv *.cmap outputs_from_test

# --- uncomment to produce plots
#for i in 2kwi.ca 2kwi.cb 2kwi.sc 1cey.sc
#do
#  python3 make_plots.py outputs_from_test/$i.cmap
#  mv ap_contact_map.svg $i"-ap_contact_map.svg"
#done
