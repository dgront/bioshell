#!/usr/bin/env python3.6

import sys
from os.path import expanduser
sys.path.append(expanduser("~")+'/src.git/visualife/src')

from core.SvgViewport import SvgViewport
from core.Plot import Plot
from core.styles import ColorRGB, color_by_name

BOX_SIZE = 10 # size of every square used to represent a hydrogen bond on a scatter plot
x = []
y = []
c = []
# Open and read input file produced by ap_contact_map
# Every line looks as below:
#  234 B  443   190 B  399  51
for line in open(sys.argv[1]) :
  if line[0] == '#' : continue
  tokens = line.split()
  i_res = int(tokens[0])
  j_res = int(tokens[3])
  counts = float(tokens[6])
  x.append(i_res)
  y.append(j_res)
  c.append(counts)
  
min_xy = min(min(x),min(y))
max_xy = max(min(x),max(y))
#for i in range(len(x)) :
#  x[i] += 0.5
#  y[i] += 0.5
plot_size = (max_xy - min_xy)*10

drawing = SvgViewport("ap_contact_map.svg", 0, 0, plot_size + 100, plot_size + 100)
pl = Plot(drawing,50,plot_size + 50,50,plot_size + 50,min_xy,max_xy+1,min_xy,max_xy+1, axes_definition="UBLR")

stroke_color = color_by_name("SteelBlue").create_darker(0.3)
xax, yax = pl.axes["B"], pl.axes["L"]
xax.label, xax.label_font_size, xax.ticslabel_font_size = "residue i", 25, 12
yax.label, yax.label_font_size, yax.ticslabel_font_size = "residue j", 25, 12

for key,ax in pl.axes.items() :
  ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 2.0
  ax.tics_every_step(1000,10)
xax.add_tics_labels("%d")
yax.add_tics_labels("%d")
  
pl.draw_axes()
pl.draw_grid()
pl.plot_label = "contact fraction  map"
pl.draw_plot_label()
pl.scatter(x,y, markersize=8, markerstyle='s', title="hbmap", colors=c, cmap="pinks", stroke_width=0.5, stroke="Black")
drawing.close()

