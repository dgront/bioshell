#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "# Running ./ap_ligand_contacts test_inputs/5edw.pdb TTP 3.5"
./ap_ligand_contacts test_inputs/5edw.pdb TTP 3.5

echo -e "# Running ./ap_ligand_contacts  test_inputs/2kwi.pdb GNP 4.0"
./ap_ligand_contacts  test_inputs/2kwi.pdb GNP 4.0
