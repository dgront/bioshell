#include <iostream>
#include <map>

#include <core/data/io/Pdb.hh>
#include <utils/string_utils.hh>
#include <utils/exit.hh>

std::string program_info = R"(

ap_ligand_contacts finds contacts between a ligand molecule and a protein.

It reads a multi-model PDB file and for each of the models detects contacts between a particular ligand
and the rest of the complex. The ligand must be identified by its three-letter code.
The output provides the interacting residues (name and residueId) along - separately for each model

USAGE:
    ap_ligand_contacts input.pdb ligand-code cutoff-distance

EXAMPLE:
    ap_ligand_contacts 5edw.pdb TTP 7.0

where 5edw.pdb id an input file, TTP the ligand code and 7.0 - contact distance in Angstroms

OUTPUT (fragment):
 ---- ligand ---- | --------- partner -------- | distance
c  res  id atname |  c  res  id   type  atname | in Angstrom
A  TTP  404  C5'     A  ASP  105 protein  OD1    3.371
A  TTP  404  C5'     A  ASP  105 protein  OD2    3.149
A  TTP  404  O2G     A  LYS  159 protein  CE     2.958
A  TTP  404  O2G     A  LYS  159 protein  NZ     2.936
A  TTP  404  O3G     A  LYS  159 protein  NZ     3.455
A  TTP  404  O2A     A   CA  401 unknown CA      2.316
A  TTP  404  O2B     A   CA  401 unknown CA      2.375
A  TTP  404  O2G     A   CA  401 unknown CA      2.325
A  TTP  404  O2A     A   CA  402 unknown CA      2.356
A  TTP  404  O1A     A  HOH  510 unknown  O      3.150
A  TTP  404  O3A     A  HOH  510 unknown  O      2.782
A  TTP  404  O1G     A  HOH  510 unknown  O      3.373
A  TTP  404  O2      T   DG    6 nucleic  N1     3.048
A  TTP  404  O2      T   DG    6 nucleic  C2     3.467
A  TTP  404  O2      T   DG    6 nucleic  N2     2.931
A  TTP  404  N3      T   DG    6 nucleic  O6     3.129

)";

/** @brief Finds contacts between a ligand molecule and a protein.
 *
 * CATEGORIES: core::data::io::Pdb
 * KEYWORDS: PDB input; contact map; ligand
 * GROUP: Structure calculations;
 * IMG: ap_ligand_contacts.png
 * IMG_ALT: Contacts found between 5EDW protein and its ligand TTP
 */
int main(const int argc, const char* argv[]) {

  if (argc < 4) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  core::data::io::Pdb reader(argv[1]); // --- file name (PDB format, may be gzip-ped)

  const std::string code(argv[2]);    // --- The ligand code is the second parameter of the program
  double cutoff = utils::from_string<double>(argv[3]); // The third parameter is the contact distance (in Angstroms)

  std::cout << " ---- ligand ---- | --------- partner -------- | distance\n";
  std::cout << "c  res  id atname |  c  res  id   type  atname | in Angstrom\n";

  for (size_t i = 0; i < reader.count_models(); ++i) { // --- Iterate over all models in the input file
    core::data::structural::Structure_SP strctr = reader.create_structure(i);

    // --- Here we use a standard <code>find_if</code> algorithm to find the ligand residue by its 3-letter code
    auto ligand = std::find_if(strctr->first_residue(), strctr->last_residue(), [&code](core::data::structural::Residue_SP res) {return (res->residue_type().code3==code);});
    if(ligand== strctr->last_residue()) { // --- If no ligand - print a message and take next structure
      std::cerr << "Model " << i << " of " << argv[1] << " has no " << argv[2] << " residue\n";
      continue;
    }

    if (reader.count_models() > 1) std::cout << "# Model " << i + 1 << "\n";
    for (auto it_resid = strctr->first_residue(); it_resid != strctr->last_residue(); ++it_resid) {
      if(*it_resid == *ligand) continue;
      double d = (*it_resid)->min_distance(*ligand);
      if (d < cutoff) { // --- if this is close enough,
        for(auto const & ligand_atom : **ligand) {
          for(auto const & other_atom : **it_resid) {
            if(ligand_atom->distance_to(*other_atom) <= cutoff) {
              std::cout << utils::string_format("%4s  %3s %4d %4s     %4s  %3s %4d %6s %4s   %6.3f\n",
                                                (**ligand).owner()->id().c_str(),
                                                (**ligand).residue_type().code3.c_str(), (**ligand).id(),
                                                ligand_atom->atom_name().c_str(),
                                                (**it_resid).owner()->id().c_str(),
                                                (**it_resid).residue_type().code3.c_str(), (**it_resid).id(),
                                                core::chemical::monomer_type_name((**it_resid).residue_type()).c_str(),
                                                other_atom->atom_name().c_str(),
                                                ligand_atom->distance_to(*other_atom));
            }
          }
        }
      }
    }
  }
}
