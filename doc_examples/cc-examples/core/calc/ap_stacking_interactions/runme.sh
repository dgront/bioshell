#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ap_stacking_interactions test_inputs/2gb1.pdb"
./ap_stacking_interactions test_inputs/2gb1.pdb
echo -e "Running ./ap_stacking_interactions test_inputs/5edw.pdb"
./ap_stacking_interactions test_inputs/5edw.pdb
echo -e "Running ./ap_stacking_interactions test_inputs/3dcg.pdb"
./ap_stacking_interactions test_inputs/3dcg.pdb
