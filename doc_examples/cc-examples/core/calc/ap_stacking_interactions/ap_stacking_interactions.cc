#include <iostream>
#include <core/data/io/Pdb.hh>
#include <core/calc/structural/interactions/StackingInteraction.hh>
#include <core/calc/structural/interactions/StackingInteractionCollector.hh>
#include <utils/exit.hh>
#include <utils/LogManager.hh>
 
std::string program_info = R"(

Finds stacking interactions in a given PDB file.

The program reports all stacking interactions detected in a given PDB file.

A plausible stacking interaction is detected when two aromatic rings are found to be close in space.

Detection of aromatic rings in a given PDB deposit is based on the definition of respective monomers.
The most popular monomers including amino acids and nucleotides are provided with the BioShell distribution.
Others must be provided by a user, either in CIF or in PDB format.

USAGE:
    ap_stacking_interactions input.pdb [ligand1.cif [ligand2.pdb ...] ]

EXAMPLE:
    ap_stacking_interactions 5edw.pdb

)";

using namespace core::data::io; // --- Pdb reader and PdbLineFilter lives there
using namespace core::chemical;
using namespace core::calc::structural::interactions;

/** @brief Finds stacking interactions in a given PDB file.
 *
 * CATEGORIES: core::calc::structural::interactions::StackingInteraction
 * KEYWORDS:   PDB input; PDB line filter; stacking interactions
 * GROUP: Structure calculations;
 * IMG: ap_stacking_interactions_sq.png
 * IMG_ALT: Two tyrosine residues in stacking interaction
 */
int main(const int argc, const char *argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  utils::LogManager::INFO();                      // --- INFO is the default logging level; set it to FINE to see more

  // ---------- Read a PDB file given as an argument to this program
  Pdb reader(argv[1],                // --- input PDB file
      all_true(is_not_water, is_not_alternative, is_not_hydrogen,
      invert_filter(is_bb)),                      // --- Inverted backbone selector  reads only side chains
      core::data::io::only_ss_from_header, true);                         // --- yes, read header

  core::data::structural::Structure_SP s = reader.create_structure(0);

  // ---------- Register additional monomers, provided by a user from a command line, either .pdb or .cif
  for (int i = 2; i < argc; ++i)
    MonomerStructureFactory::get_instance().register_monomer(argv[i]);

  StackingInteractionCollector collector=StackingInteractionCollector();
  std::vector<ResiduePair_SP> sink;
  collector.collect(*s,sink);
  std::cout << StackingInteraction::output_header()<<"\n";

  for (const ResiduePair_SP ri:sink) {
    StackingInteraction_SP bi = std::dynamic_pointer_cast<StackingInteraction>(ri);
    if (bi) std::cout << *bi << "\n";
  }
}
