#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "#Running ./ap_crmsd_on_common_subset -in:pdb:native=6h60.pdb -select:chains=A -select:bb -in:pdb=6h60_A_rebuilt.pdb"
./ap_crmsd_on_common_subset -in:pdb:native=test_inputs/6h60.pdb -select:chains=A -select:bb -in:pdb=test_inputs/6h60_A_rebuilt.pdb

