#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/calc/structural/transformations/Crmsd.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <utils/options/select_options.hh>
#include <utils/options/selector_from_cmdline.hh>
#include <utils/exit.hh>
#include <core/algorithms/basic_algorithms.hh>
#include <utils/options/output_options.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>

std::string program_info = R"(

Calculates cRMSD (coordinate Root-Mean-Square Deviation) value on common subset of atoms

Program ensures the same number of atoms.

USAGE:
./ap_crmsd_on_common_subset -in:pdb:native=file.pdb -select:chains=A -select:bb -in:pdb=rebuilt.pdb

EXAMPLEs:
./ap_crmsd_on_common_subset -in:pdb:native=6h60.pdb -select:chains=A -select:bb -in:pdb=6h60_A_rebuilt.pdb

REFERENCE:
Kabsch, W. "A Solution for the Best Rotation to Relate Two Sets of Vectors."
Acta Cryst (1976) 32 922-923

)";

void extract_atom_by_name(const core::data::structural::Structure_SP s, std::vector<std::string> & atom_names,
    std::map<std::string, core::data::structural::PdbAtom_SP> & atoms_by_name,
    bool aa_only = true, bool skip_chainbreaks=true) {

    using namespace core::data::structural;

    // --- select only amino acids
    selectors::IsAA is_aa;
    // --- remove atoms at chain breaks
    selectors::ProperlyConnectedCA at_gap;
    for (auto c: *s) {
        for (auto r: *c) {
            if (r == nullptr) continue;
            if (aa_only && ! is_aa(*r)) continue;
            if (skip_chainbreaks && ! at_gap(*r)) continue;
            for(auto a: (*r)) {
                std::string code = c->id() + (*r).residue_type().code3 + (*r).residue_id() + a->atom_name();
                atom_names.push_back(code);
                atoms_by_name[code] = a;
            }
        }
    }
}

/** @brief Calculates crmsd value on C-alpha coordinates. The program prints just the crmsd value.
 *
 * CATEGORIES: core/calc/structural/transformations/Crmsd
 * KEYWORDS:   PDB input; crmsd
 * GROUP: Structure calculations;
 */
int main(const int argc, const char *argv[]) {

    if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

    using core::data::basic::Vec3;
    using namespace core::calc::structural::transformations;
    using namespace core::data::io;
    using namespace utils::options;
    using namespace core::data::basic;
    using namespace core::data::structural;
    using namespace core::data::structural::selectors;
    using namespace core::calc::structural;
    using namespace core::calc::structural::transformations;

    Crmsd<std::vector<Vec3>, std::vector<Vec3>> rms;

    utils::options::OptionParser &cmd = OptionParser::get("ap_crmsd_on_common_subset");
    // ---------- input PDB structures
    cmd.register_option(input_pdb, input_pdb_native, input_pdb_list, input_pdb_path, input_pdb_header);
    // ---------- selecting options
    cmd.register_option(select_ca, select_bb, select_bb_cb, select_cb, select_atoms_by_name, select_aa, select_chains, all_models);
    // ---------- output PDB structures
    cmd.register_option(output_pdb,output_name_prefix);
    cmd.register_option(verbose, db_path);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
    Structure_SP native = native_from_cmdline();
    std::vector<core::data::structural::Structure_SP> structures;
    std::vector<std::string> structure_ids;
    utils::options::structures_from_cmdline(structure_ids, structures);

    // ---------- Load atoms from the native structure
    std::vector<std::string> native_names;
    std::map<std::string, PdbAtom_SP> native_name_to_atom;
    extract_atom_by_name(native, native_names, native_name_to_atom, select_aa.was_used());
    std::sort(native_names.begin(), native_names.end());

    std::vector<Vec3> q, t;
    std::cout << "native nres, atoms " << native->count_residues() << " " << native->count_atoms() << "\n";
    std::cout << "model nres, atoms " << structures[0]->count_residues() << " " << structures[0]->count_atoms() << "\n";

  for (auto i = 0; i < structures.size(); ++i) {
    // ---------- Load atoms from a query structure
    std::vector<std::string> q_names;
    std::map<std::string, PdbAtom_SP> q_name_to_atom;
    extract_atom_by_name(structures[i], q_names, q_name_to_atom, select_aa.was_used());
    std::sort(q_names.begin(), q_names.end());

    // ---------- find the common subset
    std::vector<std::string> common_atom_names;
    core::algorithms::intersect_sorted(native_names.begin(), native_names.end(), q_names.begin(), q_names.end(),
        common_atom_names);

    // ---------- get the two subset of atoms
    q.clear();
    t.clear();
    std::shared_ptr<std::vector<double>> errors = std::make_shared<std::vector<double>>();
    for (const std::string &name: common_atom_names) {
      t.push_back(*q_name_to_atom[name]);
      q.push_back(*native_name_to_atom[name]);
    }

    double rms_val = rms.crmsd(q, t, q.size(), true);
    rms.calculate_crmsd_value(q, t, q.size(), errors);
    // ---------- This is the moment when we can dump the transformed structure into a PDB file
    if(output_pdb.was_used()) {
        int iatm = -1;
        std::string fname;
        if(input_pdb_list.was_used())
        fname = option_value<std::string>(output_name_prefix)+utils::split(structure_ids[i],{'/'}).back() + ".pdb";
        else  fname = option_value<std::string>(output_pdb);
        std::ofstream out(fname);
      for (const std::string &name: common_atom_names) {
          q_name_to_atom[name]->b_factor((*errors)[++iatm]);
          out << q_name_to_atom[name]->to_pdb_line() << "\n";
//        std::cout << name << " " << (*errors)[++iatm] << "\n";
      }

    }

    std::cout << native->code() << " " << i << " crmsd: " << rms_val << "\n";
  }
}

