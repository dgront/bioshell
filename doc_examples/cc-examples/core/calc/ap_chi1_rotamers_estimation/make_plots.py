import math
import matplotlib.pyplot as plt
from scipy.stats import vonmises

input_data_file = "test_inputs/THR_chi1.dat" # The same input file as was used to run ap_chi1_rotamers_estimation
n1, mu1, kappa1 = 16156, -1.04831, 75.4575
n2, mu2, kappa2 = 18211, 1.07518, 50.7715
n3, mu3, kappa3 = 2953, -3.02569, 19.5993

data = [ float(line.strip()) for line in open(input_data_file) ]
angle_bins = [ i*math.pi/180.0 for i in range(-180, 180,2) ]

fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.hist(data, bins=angle_bins, facecolor='#124653', alpha=0.5, edgecolor='#124653')
    
bin_factor = 2.0/180.0*math.pi
xp = [i*math.pi/180.0 for i in range(-180,180)]
yp = [ vonmises.pdf(ix,kappa1,mu1) * n1 * bin_factor for ix in xp ]
ax1.plot(xp, yp, label = "gauge-")
yp = [ vonmises.pdf(ix,kappa2,mu2) * n2 * bin_factor for ix in xp ]
ax1.plot(xp, yp, label = "trans")
yp = [ vonmises.pdf(ix,kappa3,mu3) * n3 * bin_factor for ix in xp ]
ax1.plot(xp, yp, label="gauge+")
ax1.legend()

plt.title('Chi1 angle distribution')
ax1.set_xlabel("angle")
ax1.set_ylabel("counts")
fig1.tight_layout()
fig1.savefig("histogram.png", dpi=500)

