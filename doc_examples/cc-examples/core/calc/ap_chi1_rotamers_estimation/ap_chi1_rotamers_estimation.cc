#include <math.h>

#include <iostream>
#include <random>

#include <core/calc/statistics/VonMisesDistribution.hh>
#include <core/calc/statistics/expectation_maximization.hh>
#include <core/data/io/DataTable.hh>
#include <core/calc/numeric/numerical_integration.hh>
#include <utils/exit.hh>

std::string program_info = R"(

ap_chi1_rotamers_estimation reads a text file with Chi_1 angles (single column of real values)
and fits a mixture of VonMisses distributions to the data. The program may be thus used for deriving
rotamer library for VAL, THR, SER and CYS

USAGE:
    ap_chi1_rotamers_estimation input-data

EXAMPLE:
    ap_chi1_rotamers_estimation THR_chi1.dat

REFERENCE:
Mardia, Kanti V., and Peter E. Jupp. Directional statistics. Vol. 494. John Wiley & Sons, 2009

)";

/** @brief Reads a file with 1D data and estimates a mixture of VonMissesDistribution based on these observations.
 *
 * This example may be used to approximate a $\Chi_1$ rotamer (such as VAL, THR or SER) with a mixture of
 * VonMisses distributions.
 *
 * CATEGORIES: core::calc::statistics::VonMissesDistribution
 * KEYWORDS:   von Misses distribution; estimation; expectation-maximization; statistics
 * GROUP: Statistics;
 * IMG: ap_chi1_rotamers_estimation.png
 * IMG_ALT: Distribution of Chi1 angles of THR side chains approximated with a mixture of three von Mises distribution
 */
int main(const int argc, const char *argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  const double deg_to_rad = M_PI/180.0;
  using namespace core::calc::statistics;

  std::vector<std::vector<double>> chi_angle;
  if (argc == 2) {
    core::data::io::DataTable in(argv[1]);
    for(const auto & row : in) {
      std::vector<double> chi;
      chi.push_back( row.get<double>(0) );
      chi_angle.push_back(chi);
    }
  }
  // ---------- Three distributions - one for each rotamer
  VonMisesDistribution m(-60 * deg_to_rad, 10.0), p(60 * deg_to_rad, 10.0),
    t(-180 * deg_to_rad, 10.0); // medium, gauge-plus and gauge-minus

  std::vector<VonMisesDistribution> distributions_1D({{-60 * deg_to_rad, 10.0}, // --- gauge minus
                                                       {60 * deg_to_rad, 10.0}, // --- gauge plus
                                                       {-180 * deg_to_rad, 10.0}}); // --- trans
  std::vector<core::index1> index_1D;
  double score = expectation_maximization(chi_angle, distributions_1D, index_1D, 0.000001);
  core::index4 cnt0 = std::count(index_1D.cbegin(), index_1D.cend(), 0);
  core::index4 cnt1 = std::count(index_1D.cbegin(), index_1D.cend(), 1);
  core::index4 cnt2 = std::count(index_1D.cbegin(), index_1D.cend(), 2);
  std::cout << "# log-likelihood: " << score << "\n";
  std::cout << "# " << cnt0 << " " << distributions_1D[0]
            << " " << cnt1 << " " << distributions_1D[1]
            << " " << cnt2 << " " << distributions_1D[2] << "\n";

  for (double x = -M_PI; x < M_PI; x += 0.01)
    std::cout << utils::string_format("%6.3f %8.5f %8.5f %8.5f\n", x,
                                      cnt0 * distributions_1D[0].evaluate(x) / chi_angle.size(),
                                      cnt1 * distributions_1D[1].evaluate(x) / chi_angle.size(),
                                      cnt2 * distributions_1D[2].evaluate(x) / chi_angle.size());
}


