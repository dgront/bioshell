#!/usr/bin/env python
"""
    Creates a PDF figure from hexbin histogram data calculated by ex_Hexbin app (BioShell example)
    USAGE:
	./make_plots.py data_hex [x_min x_max [y_min y_max]]
"""

#python make_plots.py outputs_from_test/LEU_rotamers.dat  -180 180 -180 180 3.6
import math, re, sys, matplotlib
import matplotlib.pyplot as plt
from matplotlib.collections import PolyCollection
import matplotlib.colors as colors
import matplotlib.cm as cmx
from mpl_toolkits.axes_grid1 import make_axes_locatable

BIN_X_COLUMN     = 0 # x-index of a hexbin (integer)
BIN_Y_COLUMN     = 1 # y-index of a hexbin (integer)
BIN_CNT_COLUMN   = 2 # Histogram counts for a bin
HEX_START_COLUMN = 4 # 12 columns with coordinates of the 6 hexagon vertices starts here
HEXABIN_WIDTH    = 3.6 # default width of a hexabin

def calculate_hexbin_vertices(side, i, j) :
    s32 = math.sqrt(3.0)/2.0
    px = i + 0.5 if (j % 2 == 0) else i;
    x0 = px * side * 2 * s32;
    y0 = j * side * 1.5;

    return [
      [0 + x0, side + y0],
      [side * s32 + x0, side / 2 + y0],
      [side * s32 + x0, -side / 2 + y0],
      [0 + x0, -side + y0],
      [-side * s32 + x0, -side / 2 + y0],
      [-side * s32 + x0, side / 2 + y0],
      [0 + x0, side + y0]
    ];

if len(sys.argv) < 2 : 
   print >>sys.stderr, __doc__
   sys.exit(0)

infile    = sys.argv[1]
min_x     = float(sys.argv[2]) if len(sys.argv) > 3 else 0
max_x     = float(sys.argv[3]) if len(sys.argv) > 3 else 10
min_y     = float(sys.argv[4]) if len(sys.argv) > 5 else min_x
max_y     = float(sys.argv[5]) if len(sys.argv) > 5 else max_x
width      = float(sys.argv[6]) if len(sys.argv) > 6 else HEXABIN_WIDTH
vertices  = []
values    = []
max_value = 0

# --- Load hexbins from file
for l in open(sys.argv[1]) :
  l = l.strip()
  if len(l) < 2 : continue
  data = re.split("\s+",l)
  if data[0][0] == '#' : continue # skip comments
  v = float(data[BIN_CNT_COLUMN])
# --- uncomment the line below to get hexbins in plot logscale
  v = math.log10(v)
  if v == 0 : continue
  values.append( v )
  if max_value < v : max_value = v
  vert = []
  if len(data) < HEX_START_COLUMN : # if vertex coordinates are missing, they will be recalculated from bin index
    vertices.append( calculate_hexbin_vertices(width,int(data[BIN_X_COLUMN]),int(data[BIN_Y_COLUMN])) )
  else :
    for i in range(HEX_START_COLUMN,HEX_START_COLUMN+12,2) : vert.append( (float(data[i]), float(data[i+1])) )
    vertices.append(vert)
  
jet = cm = plt.get_cmap('jet')
cNorm  = colors.Normalize(vmin=0, vmax=max_value)
#cNorm  = colors.LogNorm(vmin=1, vmax=max_value)
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)
value_colors = []
for v in values :  value_colors.append( scalarMap.to_rgba(v) )
 
poly = PolyCollection(vertices, facecolors = value_colors, linewidths=0.01, edgecolor="white")

pl,axes = plt.subplots()
axes.add_collection(poly)
axes.set_aspect(1.0)

divider = make_axes_locatable(axes)
cax = divider.append_axes('right', size='5%', pad=0.05)
cb = matplotlib.colorbar.ColorbarBase(cax, cmap=jet, norm=cNorm, spacing='proportional')

axes.set_xlim(min_x, max_x)
axes.set_ylim(min_y, max_y)
plt.savefig("hist.pdf")

