#!/bin/bash

mkdir -p outputs_from_test

echo -e "# Running ./ap_Hexbins " > outputs_from_test/normal_2d.dat
./ap_Hexbins >> outputs_from_test/normal_2d.dat
echo -e "# Running ./ap_Hexbins test_inputs/LEU_chi1_chi2_rad.dat 3.6" > outputs_from_test/LEU_rotamers.dat
./ap_Hexbins test_inputs/LEU_chi1_chi2_rad.dat 3.6 >> outputs_from_test/LEU_rotamers.dat

