#include <iostream>
#include <random>
#include <vector>

#include <core/index.hh>

#include <core/calc/statistics/Hexbins.hh>
#include <core/calc/statistics/Random.hh>
#include <core/data/io/DataTable.hh>

std::string program_info = R"(

Reads a file with 2D observations (two columns with real values) and makes hexbin histogram.
USAGE:
    ap_Hexbins input.dat [bin_side_width]

)";

/** @brief Reads a file with 2D observations (two columns) and makes hexbin histogram.
 *
 * CATEGORIES: core::calc::statistics::Hexbins
 * KEYWORDS:   histogram; statistics
 * GROUP: Statistics;
 * IMG: ramachandran_map_all.png
 * IMG_ALT: hexabin representation of Ramachandran map (histogram made from non-redundant subset of PDB)
 */
int main(const int argc, const char *argv[]) {

  using namespace core::calc::statistics;

  Hexbins<double, core::index4> hist(0.05);
  if (argc > 1) { // --- If an input file was given, make histogram using this data
    if (argc > 2) hist.bin_side(atof(argv[2]));
    float x,y;
    std::ifstream in(argv[1]);
    std::string line;
    // --- here we read the input file using pure C API since it's faster than C++ fancy streams
    while (std::getline(in, line)) {
      sscanf(line.c_str(),"%f %f",&x,&y);
      hist.insert(x,y);
    }
  } else { // --- otherwise generate some random data
    std::cerr << program_info <<"\n";
    Random r = Random::get();
    r.seed(9876543);
    NormalRandomDistribution<double> dist_x(1.0, 0.25);
    NormalRandomDistribution<double> dist_y(3.0, 0.5);
    for (size_t i = 0; i < 100000; ++i) {
      hist.insert(dist_x(r), dist_y(r));
    }
  }
  std::cout << "# Created histogram of " << hist.count_entries() << " observations, " << hist.count_outside()
            << " were outside\n";

  std::vector<std::pair<double,double>> coordinates; // --- a vector used to retrieve coordinates of each hexagon

  for (auto it = hist.cbegin(); it != hist.cend(); ++it) {
    coordinates.clear();
    auto bin = (*it).first;
    hist.bin_vertices(bin,coordinates);
    std::cout << utils::string_format("%4d %4d %4d ",bin.first, bin.second,(*it).second);
// --- uncomment the lines below to print coordinates of hexbin vertexes in every line
// --- Note: this is a lot of (redundant) output; make_plots.py script may generate these coordinates for you based on bin indexes
//    std::cout <<" : ";
//    for(const auto & xy : coordinates) std::cout << utils::string_format("%8.3f %8.3f ",xy.first,xy.second);
    std::cout <<"\n";
  }
}

