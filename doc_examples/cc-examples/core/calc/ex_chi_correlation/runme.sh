#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "#Running ex_chi_correlation test_inputs/2fd2.pdb test_inputs/5fd1.pdb"
./ex_chi_correlation test_inputs/2fd2.pdb test_inputs/5fd1.pdb
echo -e "#Running ex_chi_correlation test_inputs/1bgx_aligned.pdb test_inputs/1xo1_aligned.pdb"
./ex_chi_correlation test_inputs/1bgx_aligned.pdb test_inputs/1xo1_aligned.pdb

