#include <iostream>
#include <iomanip>
#include <core/data/io/Pdb.hh>

#include <core/calc/structural/protein_angles.hh>
#include <core/chemical/ChiAnglesDefinition.hh>
#include <utils/exit.hh>
#include <core/calc/structural/angles.hh>

std::string program_info = R"(

Unit test which calculates Chi dihedral angles for every pair of amino acid side chains measured in two different
homologous protein structures which are assumed to be aligned.

USAGE:
    ex_chi_correlation file-1.pdb file-2.pdb

EXAMPLE:
    ex_chi_correlation 1bgx_aligned.pdb 1xo1_aligned.pdb

)";

/** @brief Calculates correlation between Chi dihedral angles measured in two different protein structures.
 *
 * CATEGORIES: core/calc/structural/evaluate_chi;
 * KEYWORDS:   PDB input; structural properties; rotamers
 * IMG: ex_chi_correlation_plot.png
 * IMG_ALT: Example correlation of chi angles between two homologus structures
 */
int main(const int argc, const char* argv[]) {

  if(argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::structural;
  core::data::io::Pdb readerA(argv[1]); // file name (PDB format, may be gzip-ped)
  Structure_SP proteinA = readerA.create_structure(0);
  core::data::io::Pdb readerB(argv[2]);
  Structure_SP proteinB = readerB.create_structure(0);

  std::vector<std::vector<double>> chiA, chiB;
  std::vector<std::string> labels; // for nice output on a screen
  std::vector<std::string> mpt_annotationsA; // MPT string - one per residue only (for other lines of a given residue empty strings are inserted)
  std::vector<std::string> mpt_annotationsB;

  if (proteinA->count_residues() != proteinB->count_residues()) {
    std::cerr << "The two input PDB files should contain only the aligned parts of input proteins and be equal in length!\n";
    return 0;
  }
  auto a_res_it = proteinA->first_const_residue();
  auto b_res_it = proteinB->first_const_residue();
  while(a_res_it!=proteinA->last_const_residue()) {

    if ((*a_res_it)->residue_type() == (*b_res_it)->residue_type()) { // check chi angles

      std::vector<double> ca, cb;
      for (core::index2 k = 1; k <= core::chemical::ChiAnglesDefinition::count_chi_angles((*a_res_it)->residue_type()); ++k) {
        try {
          double aA = core::calc::structural::evaluate_chi((**a_res_it), k);
          double aB = core::calc::structural::evaluate_chi((**b_res_it), k);
          ca.push_back(aA);
          cb.push_back(aB);
          labels.push_back(utils::string_format("%4d%c %4d%c %c %1d", (*a_res_it)->id(), (*a_res_it)->icode(),
            (*b_res_it)->id(), (*b_res_it)->icode(), (*a_res_it)->residue_type().code1, k));
        } catch (utils::exceptions::AtomNotFound e) {
          std::cerr << e.what() << "\n";
          std::cerr << "Can't define chi angle for residue " << (**a_res_it) << "\n";
        }
      }
      if (ca.size() == cb.size() && ca.size() > 0) {
        chiA.push_back(ca);
        chiB.push_back(cb);
        mpt_annotationsA.push_back(core::calc::structural::define_rotamer(ca));
        mpt_annotationsB.push_back(core::calc::structural::define_rotamer(cb));
      }
    }
    ++a_res_it;
    ++b_res_it;
  } // end of while loop over residues

  std::cout << "#ires jres aa k  ichi_k   jchi_k delta(chi) irot jrot\n";
  double err = 0.0;
  core::index2 n_reoriented = 0;

  size_t ilabel=0;
  for(size_t ires=0;ires<chiA.size();++ires) {
    for (size_t i = 0; i < chiA[ires].size(); ++i) {
      double e = fabs(chiA[ires][i] - chiB[ires][i]);
      if (e > M_PI) e = 2.0 * M_PI - e;
      if (e > 0.523) ++n_reoriented; // --- i.e. 30 degrees of error
      std::cout << labels[ilabel] << utils::string_format("%8.2f %8.2f %8.2f",
        core::calc::structural::to_degrees(chiA[ires][i]), core::calc::structural::to_degrees(chiB[ires][i]),
        core::calc::structural::to_degrees(e));
      if (i == 0)
        std::cout << std::setw(5) << mpt_annotationsA[ires] << std::setw(5) << mpt_annotationsB[ires] << "\n";
      else std::cout << "\n";
      err += e;
      ++ilabel;
    }
  }
  std::cout <<"# avg_err, n_diff, n_res: " << err / double(chiA.size()) << " "<<n_reoriented<<" "<<chiA.size()<<"\n";
}
