#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

mkdir -p outputs_from_test
for input in ./test_inputs/*.pdb; do
    echo -e "#Running ./ap_ProteinArchitecture $input"
    ./ap_ProteinArchitecture $input
done
