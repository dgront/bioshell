#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/calc/structural/ProteinArchitecture.hh>
#include <utils/exit.hh>
#include <utils/LogManager.hh>

using namespace core::data::structural;
using namespace core::data::io;
using namespace core::data::basic;

std::string program_info = R"(

ap_ProteinArchitecture reads a PDB file and describes its architecture in terms of secondary structure elements (SSEs)
and their connectivity (i.e. how strands are connected in sheets). The SSEs themselves are defined based on data
from PDB file header. If DSSP flag has been given, the app will detect secondary structure elements
using BioShell's implementation of DSSP algorithm.

USAGE:
    ap_ProteinArchitecture input.pdb [DSSP]

EXAMPLE:
    ap_ProteinArchitecture 5edw.pdb [DSSP]

REFERENCE:
Kabsch, Wolfgang, and Christian Sander.
"Dictionary of protein secondary structure: pattern recognition of hydrogen‐bonded and geometrical features."
Biopolymers 22 (1983): 2577-2637. doi:10.1002/bip.360221211

)";

/** @brief Calculates a map of backbone hydrogen bonds.
 *
 * CATEGORIES: core/calc/structural/ProteinArchitecture;
 * KEYWORDS:   PDB input; Hydrogen bonds; Protein structure features
 * GROUP: Structure calculations;
 */
int main(const int argc, const char* argv[]) {

  using namespace core::calc::structural;

  utils::LogManager::INFO(); // --- Turn it to FINE to see a lot more of messages, e.g about missed h-bonds

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  core::data::io::Pdb reader(argv[1], core::data::io::all_true(is_not_alternative, is_not_water),
      core::data::io::keep_all, true); // --- Read in a PDB file
  Structure_SP strctr = reader.create_structure(0); // --- create a Structure object from the first model

  bool if_dssp = (argc > 2) && (strcmp(argv[2], "DSSP") == 0);
  core::calc::structural::ProteinArchitecture pa(*strctr, if_dssp);

  std::cout <<"# ---------- Secondary structure elements ----------\n";
  for (const auto sse : pa.sse_vector())
    std::cout << *sse << "\n";

  std::cout <<"# ---------- Beta strand connectivity ----------\n";
  auto sse_graph = pa.create_strand_graph();
  sse_graph->print_adjacency_matrix(std::cerr);
  for(auto e_it = sse_graph->cbegin_strand();e_it!=sse_graph->cend_strand();++e_it) {
    std::cout << (*e_it)->info()<<" paired with:\n";
    for(auto partner_it = sse_graph->cbegin_strand(*e_it); partner_it != sse_graph->cend_strand(*e_it); ++partner_it) {
      auto pairing_sp = sse_graph->get_strand_pairing(*e_it, *partner_it);
      std::cout << "\t" << (*partner_it)->name() << " " << Strand::strand_type_name(pairing_sp->pairing_type)
                << " by " << pairing_sp->hydrogen_bonds().size() << " hbonds\n";
    }
  }
}

