#include <iostream>

#include <core/data/basic/Array2D.hh>
#include <core/calc/statistics/simple_statistics.hh>
#include <utils/exit.hh>

std::string program_info = R"(

The program read a contingency matrix from a file and calculates Goodman and Kruskal's gamma parameters 
which is a measure of rank correlation.

USAGE:
    ex_goodman_kruskal_rank_correlation input_contingency_matrix_file

EXAMPLE:
    ex_goodman_kruskal_rank_correlation contingency_matrix.txt

REFERENCE:
Kruskal, William H., and Leo Goodman. "Measures of association for cross classifications."
Journal of the American Statistical Association 49 (1954): 732-764. doi:10.2307/2281536.

)";

/** @brief Calculates Goodman and Kruskal's gamma parameters 
 *
 * CATEGORIES: core::calc::statistics::goodman_kruskal_rank_correlation;
 * KEYWORDS:   statistics; data table
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using core::data::basic::Array2D;

  // --- Read an input file - data table format
  Array2D<core::index4> m = Array2D<core::index4>::from_file(argv[1]);
  std::cout << core::calc::statistics::goodman_kruskal_rank_correlation(m)<<"\n";
}
