#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "# Running ./ap_hbonds test_inputs/2gb1.pdb"
./ap_hbonds test_inputs/2gb1.pdb 

echo -e "# Running ./ap_hbonds  test_inputs/2kwi-1.pdb"
./ap_hbonds  test_inputs/2kwi-1.pdb
