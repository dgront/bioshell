#include <iostream>
#include <map>

#include <core/data/io/Pdb.hh>
#include <core/calc/structural/interactions/HydrogenBondInteraction.hh>
#include <core/calc/structural/interactions/BackboneHBondInteraction.hh>
#include <core/calc/structural/interactions/HydrogenBondCollector.hh>
#include <core/chemical/MonomerStructureFactory.hh>
#include <utils/string_utils.hh>
#include <utils/exit.hh>

std::string program_info = R"(

ap_hbonds finds all hydrogen bonds in a given protein structure, including side chain interactions.

For each bond the program lists residues involved and describes its geometry (bond length and respective angles).
Backbone hydrogen bonds are reported separately from those involving side chains.

Detection of hydrogen bond donors and acceptors in a given PDB deposit is based on the definition
of respective monomers. The most popular monomers including amino acids and nucleotides are provided
with the BioShell distribution. Others must be provided by a user, either in CIF or in PDB format.
The input protein must include hydrogen atoms. Crystal structures should be protonated before using this app


USAGE:
    ap_hbonds input.pdb [ligand_1.cif [ ligand_2.pdb ...] ]

EXAMPLE:
    ap_hbonds 2gb1.pdb


OUTPUT (fragment):


)";

/** @brief Finds all hydrogen bonds in a given protein structure
 *
 * CATEGORIES: core::calc::structural::interactions::HydrogenBondInteraction
 * KEYWORDS: PDB input; interactions
 * GROUP: Structure calculations;
 * IMG: ap_hbonds_sq.png
 * IMG_ALT: Hydrogen bonds
 */
int main(const int argc, const char *argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io; // --- Pdb reader and PdbLineFilter lives there
  using namespace core::chemical;
  using namespace core::calc::structural::interactions;

  // ---------- Register additional monomers, provided by a user from a command line, either .pdb or .cif
  for (int i = 2; i < argc; ++i)
    MonomerStructureFactory::get_instance().register_monomer(argv[i]);

  // ---------- Read a PDB file given as an argument to this program
  Pdb reader(argv[1]);
  HydrogenBondCollector collector;
  std::vector<ResiduePair_SP> sink;
  // ---------- Iterate over all models in the input file
  for (size_t i_protein = 0; i_protein < reader.count_models(); ++i_protein) {
    sink.clear();
    core::data::structural::Structure_SP s = reader.create_structure(0);
    collector.collect(*s, sink);
    // ---------- The first loop prints backbone hydrogen bonds
    std::cout << BackboneHBondInteraction::output_header() << "\n";
    for (const ResiduePair_SP ri:sink) {
      BackboneHBondInteraction_SP bi = std::dynamic_pointer_cast<BackboneHBondInteraction>(ri);
      if (bi) std::cout << *bi << "\n";
    }
    // ---------- The second loop prints hydrogen bonds involving side chain atoms
    std::cout << HydrogenBondInteraction::output_header() << "\n";
    for (const ResiduePair_SP ri:sink) {
      BackboneHBondInteraction_SP bi = std::dynamic_pointer_cast<BackboneHBondInteraction>(ri);
      if (!bi) std::cout << *std::dynamic_pointer_cast<HydrogenBondInteraction>(ri) << "\n";
    }
  }
}
