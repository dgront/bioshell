#include <iostream>

#include <core/data/basic/Vec3.hh>
#include <core/calc/numeric/basic_algebra.hh>
#include <utils/options/OptionParser.hh>

std::string program_info = R"(

Unit test that calculates eigenvalues and eigenvectors for a 3x3 matrix

USAGE:
./ex_basic_algebra

)";

/** @brief ex_basic_algebra illustrates how to calculate eigenvalues and eigenvectors for a 3x3 matrix
 *
 * CATEGORIES: core/calc/numeric/basic_algebra.hh
 * KEYWORDS: numerical methods
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::calc::numeric;
  using namespace core::data::basic; // --- for Array2D

  Array2D<double> m3x3(3,3,{2, -1, 0, -1, 2, -1, 0, -1, 2}); // --- input matrix to be solved

  std::cout << "\nOriginal matrix:\n";
  m3x3.print("%8.3f", std::cout);

  std::vector<double> eigenval;
  core::calc::numeric::eigenvalues3(m3x3, eigenval);
  std::cout << "\nEigenvalues: " << eigenval[0] << " " << eigenval[1] << " " << eigenval[2] << "\n\n";

  auto eigenv = eigenvectors3(m3x3, eigenval);
  std::cout << "\nEigenvectors:\n" << eigenv[0] << "\n" << eigenv[1] << "\n" << eigenv[2] << "\n\n";
}

