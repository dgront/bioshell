#include <iostream>
#include <sstream>
#include <vector>
#include <numeric> // for std::accumulate

#include <core/algorithms/trees/algorithms.hh>
#include <core/algorithms/UnionFind.hh>
#include <core/calc/clustering/DistanceByValues1B.hh>
#include <core/calc/clustering/HierarchicalCluster.hh>
#include <core/calc/clustering/HierarchicalClustering1B.hh>
#include <core/BioShellEnvironment.hh>
#include <utils/LogManager.hh>

using namespace core::calc::clustering;

static utils::Logger l("ex_HierarchicalClustering1B");

std::string program_info = R"(

Example showing how to use hierarchical clustering method - 1 byte version.

HierarchicalClustering1B is a specialized version of HierarchicalClustering which uses as least memory
as possible. Distance values must be an integer in the range 0-255 (both inclusive);  user is responsible
for an appropriate and relevant conversion.
The program uses Complete Link strategy. Once clustering is done, it prints medoids - elements located
in centers of their clusters, corresponding to the given distance cutoff. The default cutoff value is set to 195.
The clustering tree is printed on stderr.

USAGE:
    ex_HierarchicalClustering1B input.txt

EXAMPLE:
    ex_HierarchicalClustering1B fasta_distances

REFERENCE:
Dominik Gront, Andrzej Koliński. "HCPM–program for hierarchical clustering of protein models."
Bioinformatics, 21 (2005):3179–80  doi:10.1093/bioinformatics/bti450

)";

const int MAX = 10;             // --- longest sequence id string

DistanceByValues1B read_distance_matrix(const std::string &distance_file, std::set<std::string> & labels) {

  core::index1 val;
  std::string line;
  std::ifstream infile(distance_file);
  char name_i[MAX], name_j[MAX];
  while (std::getline(infile, line)) {
    if(scanf_row(line, name_i, name_j, val)==3) {
      labels.insert(name_i);
      labels.insert(name_j);
    }
  }
  infile.close();
  infile.open(distance_file);

  std::vector<std::string> v( labels.begin(), labels.end() );
  core::algorithms::UnionFindSI4 uf(labels.size());
  for (const std::string &s:v) uf.add_element(s);
  DistanceByValues1B d(v);
  while (std::getline(infile, line)) {
    if (scanf_row(line, name_i, name_j, val) == 3) {
      size_t i_index = d.at(name_i);
      size_t j_index = d.at(name_j);
      d.set(i_index, j_index, val);
      d.set(j_index, i_index, val);
      if(val==255)
        uf.union_set(i_index, j_index);
    }
  }

  std::cout << "# UnionFind groups larger than 2 (representative PDB-ID, group size):\n";
  const auto sets = uf.retrieve_sets();
  for (const auto &set: sets) {
    if (set.second.size() > 2)
      std::cout << uf.element(set.first) << " : " << set.second.size() << "\n";
  }
  std::cout << sets.size() << "\n";

  return d;
}

/** @brief Example showing how to use hierarchical clustering method - 1 byte version.
 *
 * CATEGORIES: core::calc::clustering::HierarchicalClustering
 * KEYWORDS: clustering; hierarchical clustering
 */
int main(int cnt, char *argv[]) {


  std::set<std::string> labels_set;
  DistanceByValues1B d = read_distance_matrix(argv[1], labels_set);
  std::cout << labels_set.size() << " items for clustering\n";

  utils::LogManager::INFO();

  HierarchicalClustering1B hac(d.labels(), "");
  CompleteLink1B merge;

  hac.run_clustering(d, merge);

  // --- write the clustering steps to a stream
  hac.write_merging_steps(std::cerr);
}
