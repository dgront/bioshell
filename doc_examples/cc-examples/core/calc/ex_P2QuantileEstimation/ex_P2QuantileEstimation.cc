#include <iostream>

#include <core/index.hh>
#include <core/calc/statistics/Random.hh>
#include <core/calc/statistics/P2QuantileEstimation.hh>

std::string program_info = R"(

ex_P2QuantileEstimation reads a file with real values and calculates a quantile using the P-square algorithm

If no input file is provided, the program calculates 0.25, 0.5 and 0.75 quantiles
of a random sample from normal distribution

USAGE:
    ex_P2QuantileEstimation [infile p_value]

EXAMPLE:
    ex_P2QuantileEstimation random_normal.txt 0.5

REFERENCE:
Jain, Raj, Imrich Chlamtac. "The P2 algorithm for dynamic calculation of quantiles
and histograms without storing observations." Communications of the ACM 28.10 (1985): 1076-1085. doi:10.1145/4372.4378

)";

/** @brief Reads a file with real values and calculates simple statistics: min, mean, stdev, max.
 *
 * If no input file is provided, the program calculates the statistics from a random sample
 *
 * CATEGORIES: core/calc/statistics/OnlineStatistics; core/calc/statistics/Random
 * KEYWORDS:   statistics
 */
int main(const int argc, const char *argv[]) {

  if(argc < 3) {
    // --- complain about missing program parameter
    std::cerr << program_info;
    // ---------- Use the random engine if no data is provided
    core::calc::statistics::Random r = core::calc::statistics::Random::get();
    r.seed(12345);  // --- seed the generator for repeatable results
    std::normal_distribution<double> normal_random;
    core::calc::statistics::P2QuantileEstimation quartile1(0.25),quartile2(0.5), quartile3(0.75);
    for (core::index4 n = 0; n < 10000; ++n) {
      double rr = normal_random(r);
      quartile1(rr);
      quartile2(rr);
      quartile3(rr);
    }
    std::cout << "Quantile 0.25 :"<<quartile1.p_value() << "\n"; // Should be -0.675
    std::cout << "Quantile 0.50 :"<<quartile2.p_value() << "\n"; // Should be  0.0
    std::cout << "Quantile 0.75 :"<<quartile3.p_value() << "\n"; // Should be  0.675

  } else {
    std::ifstream in(argv[1]);
    core::calc::statistics::P2QuantileEstimation stats(atof(argv[2]));
    double r;
    core::index4 cnt = 0;
    while (in >> r) {
      ++cnt;
      stats(r);
    }
    std::cout << "Quantile " << atof(argv[2]) << " " << stats.p_value() << " based on " << cnt << " observations\n";
  }
}
