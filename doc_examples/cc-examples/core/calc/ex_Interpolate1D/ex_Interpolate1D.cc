#include <cmath>
#include <iostream>
#include <vector>

#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/Interpolate1D.hh>
#include <core/data/io/DataTable.hh>
#include <utils/exit.hh>

using namespace core::calc::numeric;

std::string program_info = R"(

Unit test which reads a file with two columns of data and calculates interpolated values for given number of steps.

USAGE:
    ./ex_Interpolate1D file.txt n_points
EXAMPLE:
    ./ex_Interpolate1D input.txt 100

)";

/** @brief Reads a file with two columns of data and calculates interpolated values
 *
 * CATEGORIES: core::calc::numeric::Interpolate1D;
 * KEYWORDS:   interpolation
 */
int main(int argc, char* argv[]) {

  if(argc < 3) utils::exit_OK_with_message(program_info);

  std::vector<double> vx; // --- vector of function arguments: X axis
  std::vector<double> vy; // --- vector of function arguments: Y axis
  core::data::io::DataTable in_data(argv[1]);
  in_data.column(0, vx);
  in_data.column(1, vy);

  // --- Create the actual interpolator object
  CatmullRomInterpolator<double> cri;
  Interpolate1D<std::vector<double>, double, CatmullRomInterpolator<double> > ip(vx,vy, cri);
  double step = (vx.back() - vx.front())/ atof(argv[2]);
  for (double x = vx[0]; x <= vx.back(); x += step) {
    std::cout << x << " " << ip(x) << "\n";
  }
}
