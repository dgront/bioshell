#include <iostream>

#include <core/index.hh>
#include <core/calc/statistics/Random.hh>
#include <core/calc/statistics/OnlineStatistics.hh>

std::string program_info = R"(

ap_OnlineStatistics reads a file with real values and calculates simple statistics: min, mean, stdev, max.
The program uses method of Knuth and Welford for computing average and standard deviation in one pass through the data
If no input file is provided, the program calculates the statistics from a random sample.

USAGE:
    ap_OnlineStatistics infile

EXAMPLE:
    ap_WeightedOnlineStatistics random_normal.txt

REFERENCE:
    https://www.johndcook.com/blog/skewness_kurtosis/
    https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford%27s_online_algorithm
)";

/** @brief Reads a file with real values and calculates simple statistics: min, mean, stdev, max.
 * If no input file is provided, the program calculates the statistics from a random sample
 *
 * CATEGORIES: core::calc::statistics::OnlineStatistics; core::calc::statistics::Random
 * KEYWORDS:   random numbers; statistics
 * GROUP: Statistics;
 */
int main(const int argc, const char *argv[]) {

  core::calc::statistics::OnlineStatistics stats;
  if(argc < 2) {
    // --- complain about missing program parameter
    std::cerr << program_info;
    // ---------- Use the random engine if no data is provided
    core::calc::statistics::Random r = core::calc::statistics::Random::get();
    r.seed(12345);  // --- seed the generator for repeatable results
    core::calc::statistics::UniformRealRandomDistribution<double> uniform_random;
    for (core::index4 n = 0; n < 100000; ++n) stats(uniform_random(r));
  } else {
    std::ifstream in(argv[1]);
    double r;
    while(in) {
      in >> r;
      stats(r);
    }
  }

  std::cout << "#cnt   min        avg      sdev  skewness  kurtosis    max  bimodalitycoefficient\n";
  std::cout << utils::string_format("%d %f %f %f %f %f %f %f\n",stats.cnt(),stats.min(),stats.avg(),
    sqrt(stats.var()),stats.skewness(),stats.kurtosis(),stats.max(), stats.bimodality_coefficient());
}
