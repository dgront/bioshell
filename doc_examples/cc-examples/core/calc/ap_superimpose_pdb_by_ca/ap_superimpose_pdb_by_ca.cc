#include <memory>
#include <iostream>
#include <vector>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/protocols/PairwiseCrmsd.hh>
#include <utils/LogManager.hh>

using namespace core::data::structural;

std::string program_info = R"(

Superimposes protein structures by matching C-alphas.

All atoms of the second (and subsequent)  protein structures will be superimposed on the first protein based on the CA
positions. All structures must contain the same number of C-alphas atoms.

USAGE:
     ./ap_superimpose_pdb_by_ca reference pdb_file_1 [pdb_file_2 ...]

EXAMPLE:
    ./ap_superipose_pdb_by_ligand 4rm4A.pdb  model.pdb
)";

/** @brief Superimposes protein structures by matching ligand molecules.
 * *
 * CATEGORIES: core/calc/structural/transformations/PairwiseCrmsd
 * KEYWORDS:   PDB input; rototranslation; superimposition; crmsd; docking
 * GROUP: Structure calculations;
 */
int main(const int argc, const char *argv[]) {

  if(argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  utils::LogManager::get().FINE();
  selectors::AtomSelector_SP selector = std::make_shared<selectors::IsCA>(); // --- select a ligand residue by its 3-letter code

  core::data::io::Pdb read_native(argv[1], core::data::io::keep_all); // --- Read the native (reference) structure, keep all atoms
  Structure_SP native = read_native.create_structure(0);
  std::vector<Structure_SP> models; // --- Container for targets to be superimposed
  for (int i = 2; i < argc; ++i) {
    core::data::io::Pdb reader(argv[i], core::data::io::keep_all);
    for (int j = 0; j < reader.count_models(); ++j) // --- Read all models from each target PDB file
      models.push_back(reader.create_structure(j));
  }

  selectors::AtomSelector_SP select_all = std::make_shared<selectors::AtomSelector>();
  core::protocols::PairwiseCrmsd rms_calc(models, selector);
  std::shared_ptr<std::ostream> out = std::make_shared<std::ofstream>("out.pdb");
  rms_calc.calculate(native, out);
}
