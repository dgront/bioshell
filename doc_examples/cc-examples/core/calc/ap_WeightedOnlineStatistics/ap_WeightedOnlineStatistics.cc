#include <iostream>
#include <cmath>

#include <core/index.hh>
#include <core/calc/statistics/Random.hh>
#include <core/calc/statistics/WeightedOnlineStatistics.hh>

std::string program_info = R"(

ap_WeightedOnlineStatistics reads a file with two columns: real values and their weights.
It calculates average value and standard deviation of the data using an online algorithm (Welford method).

If no input file is provided, the program calculates the statistics from a random sample.
USAGE:
    ap_WeightedOnlineStatistics infile

EXAMPLE:
    ap_WeightedOnlineStatistics random_normal_weighted.txt

REFERENCE:
    https://www.johndcook.com/blog/skewness_kurtosis/
    https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford%27s_online_algorithm
)";

/** @brief Reads a file with two columns: real values and their weights, and calculates their mean and stdev.
 *
 * If no input file is provided, the program calculates the statistics from a random sample
 *
 * CATEGORIES: core/calc/statistics/ap_WeightedOnlineStatistics; core/calc/statistics/Random
 * KEYWORDS:   statistics
 * GROUP: Statistics;
 */
int main(const int argc, const char *argv[]) {

  core::calc::statistics::WeightedOnlineStatistics stats;
  if (argc < 2) {
    // --- complain about missing program parameter
    std::cerr << program_info;
    // ---------- Use the random engine if no data is provided
    core::calc::statistics::Random r = core::calc::statistics::Random::get();
    r.seed(12345);  // --- seed the generator for repeatable results
    std::normal_distribution<double> normal_random;
    for (core::index4 n = 0; n < 10000; ++n) {
      double x = normal_random(r);
      if (x <= 2.0) stats(x, 0.1); // --- insert the random point with an arbitrary weight = 0.1
      else for (int i = 0; i < 10; ++i) stats(x, 0.01); // in the tail insert points ten times with weight 1/10
    }
  } else {
    std::ifstream in(argv[1]);
    double x, w;
    while (in) {
      in >> x >> w;
      stats(x, w);
    }
  }

  std::cout << "#count sum_wghts   avg      sdev\n";
  std::cout << utils::string_format("%d  %lf %f %f \n", stats.cnt(), double(stats.sum_of_weights()), stats.avg(), sqrt(stats.var()));
}
