#include <iostream>
#include <map>

#include <core/data/io/Pdb.hh>
#include <core/calc/structural/interactions/VdWInteraction.hh>
#include <core/calc/structural/interactions/VdWInteractionCollector.hh>
#include <utils/string_utils.hh>
#include <utils/exit.hh>

std::string program_info = R"(

ap_vdw_interactions finds all van der Waals interactions in a given protein structure.


USAGE:
    ap_vdw_interactions input.pdb [input2.pdb ...]

EXAMPLE:
    ap_vdw_interactions 2gb1.pdb

OUTPUT (fragment):


)";

/** @brief Finds all van der Waals interactions in a given protein structure.
 *
 * CATEGORIES: core::calc::structural::interactions::VdWInteraction
 * KEYWORDS: PDB input; interactions
 * GROUP: Structure calculations;
 * IMG: ap_ligand_contacts.png
 * IMG_ALT: Contacts found between 5EDW protein and its ligand TTP
 */
int main(const int argc, const char* argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::calc::structural::interactions;

  VdWInteractionCollector collector;
  for (size_t i_protein = 1; i_protein < argc; ++i_protein) { // --- Iterate over all models in the input file
    core::data::io::Pdb reader(argv[i_protein]); // --- file name (PDB format, may be gzip-ped)

    for (size_t i_model = 0; i_model < reader.count_models(); ++i_model) { // --- Iterate over all models in the input file
      std::vector<ResiduePair_SP> sink;
      core::data::structural::Structure_SP strctr = reader.create_structure(i_model);
      collector.collect(*strctr, sink);
      
      std::cout << VdWInteraction::output_header()<<"\n";
      for (const ResiduePair_SP ri:sink) {
        VdWInteraction_SP bi = std::dynamic_pointer_cast<VdWInteraction>(ri);
        if (bi) std::cout << *bi << "\n";
      }
    }
  }
}
