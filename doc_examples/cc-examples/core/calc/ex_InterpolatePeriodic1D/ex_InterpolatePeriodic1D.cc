#include <cmath>
#include <iostream>
#include <vector>

#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/InterpolatePeriodic1D.hh>
#include <core/calc/structural/angles.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Simple example creates an interpolating polynomial for sin(x) function and computes
maximal interpolation error (i.e. the maximum of the difference between the true function and its interpolator)

USAGE:
./ex_InterpolatePeriodic1D

)";

using namespace core::calc::numeric;

/** @brief Simple test for interpolation of a periodic 1D function
 *
 * CATEGORIES: core::calc::numeric::InterpolatePeriodic1D;
 * KEYWORDS:   interpolation
 */
int main(int cnt, char *argv[]) {

  if ((cnt > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  std::vector<float> x, y; // --- vectors for points used for interpolation
  double step = M_PI / 50.0;  // --- interpolation data step
  for (double ix = 0.0; ix < 2 * M_PI; ix += step) { // --- generate data for interpolation knots
    x.push_back(ix);
    y.push_back(sin(ix));
  }

  CatmullRomInterpolator<float> cri; // --- Interpolating engine
  InterpolatePeriodic1D<std::vector<float>, float, CatmullRomInterpolator<float> > i1d2(x, y, cri, 2*M_PI);
  double max_error = 0.0; // --- used to keep track of the maximum interpolation error
  double worst_x = 0.0;
  // --- The function has been defined in the range \f$[0,\pi]\f$
  // --- interpolation goes in the range \f$[-20\pi,40\pi]\f$
  for (float x = -20 * M_PI; x <= 40 * M_PI; x += 0.1 * step) {
    double error = fabs(sin(x) - i1d2(x));
    if (error > max_error) {
      max_error = std::max(error, max_error);
      worst_x = x;
    }
  }
  std::cout << "Maximum interpolation error:" << max_error << " for x= " << worst_x << "\n";
}
