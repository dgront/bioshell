#include <iostream>
#include <random>
#include <math.h>

#include <core/calc/statistics/NormalDistribution.hh>
#include <core/calc/statistics/Random.hh>
#include <utils/options/OptionParser.hh>

std::string program_info = R"(

Unit test for NormalDistribution class.

The example withdraws 1000 random numbers from a normal distribution and later it estimates
a normal distribution from the sample.

USAGE:
./ex_NormalDistribution

)";

/** @brief Unit test for NormalDistribution class.
 *
 * CATEGORIES: core/calc/statistics/NormalDistribution; core/calc/statistics/Random
 * KEYWORDS:   NormalDistribution; random numbers
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::calc::statistics;

  Random rd = core::calc::statistics::Random::get();
  rd.seed(12345); // --- seed the generator for repeatable results
  std::mt19937 gen(rd());
  std::normal_distribution<> d(10.5, 2.0); // --- The original distribution we take a sample from
  // --- Note that the container for samples is two-dimensional! Each sample is placed in a separate row
  std::vector<std::vector<double> > data;
  std::vector<double> row(1);
  for (unsigned short i = 0; i < 1000; ++i) {
    row[0] = (d(gen));
    data.push_back(row); // --- This works only because C++ makes an implicit copy of the vector we place into the outer vector
  }

  // --- Here we estimate the distribution parameters
  core::calc::statistics::NormalDistribution n(0.0, 1.0);
  std::vector<double> E = n.estimate(data);
  std::cout << "True values:      average = 10.5; stdev = 2.0\n";
  std::cout << "Estimated values: average = "
            << E[0] << " sdev = " << E[1] << "\n";            // Calculate average and stdev of values in the vector
}








