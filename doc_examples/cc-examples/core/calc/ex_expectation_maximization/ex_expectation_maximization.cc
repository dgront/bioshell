#include <math.h>

#include <iostream>
#include <random>
#include <vector>

#include <core/calc/statistics/NormalDistribution.hh>
#include <core/calc/statistics/BivariateNormal.hh>
#include <core/calc/statistics/Combined_1D_2D_Normal.hh>
#include <core/calc/statistics/TrivariateNormal.hh>
#include <core/calc/statistics/expectation_maximization.hh>
#include <utils/options/OptionParser.hh>

std::string program_info = R"(

Example showing how to use expectation-maximization method
retrieve arbitrary data according to a sequence alignment object

USAGE:
  ./ex_expectation_maximization

REFERENCE:
Dempster, Arthur P., Nan M. Laird, and Donald B. Rubin.
"Maximum likelihood from incomplete data via the EM algorithm."
Journal of the Royal Statistical Society: Series B 39 (1977): 1-22. doi: 10.1111/j.2517-6161.1977.tb01600.x

)";

/** @brief Example showing how to use expectation-maximization method
 *
 * CATEGORIES: core::calc::statistics::NormalDistribution; core::calc::statistics::BivariateNormal;core/calc/statistics/expectation_maximization.hh
 * KEYWORDS: estimation; expectation-maximization
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::calc::statistics;

  double rd = 9876543;
  std::mt19937 gen(rd);
  core::index4 N = 10000; //--- the number of random points to use in tests

  // ---------- a few distributions to play with
  double ave1 = 2.0, ave2 = 4.0, ave3 = 6.0;
  double std1 = 0.2, std2 = 0.3, std3 = 0.5;
  std::normal_distribution<> m(ave1, std1);
  std::normal_distribution<> p(ave2, std2);
  std::normal_distribution<> t(ave3, std3);

  // ---------- First let's solve a 1D problem : mixture of 3 Gaussians
  std::vector<std::vector<double> > random_points;
  std::vector<double> r1(1), r2(1), r3(1);
  for (core::index4 i = 0; i < N; ++i) {
    r1[0] = (m(gen));
    r2[0] = (p(gen));
    r3[0] = (t(gen));
    random_points.push_back(r1);
    random_points.push_back(r2);
    random_points.push_back(r3);
  }

  std::vector<core::calc::statistics::NormalDistribution> distributions_1D;
  std::vector<core::index1> index_1D; // --- Distribution assignment computed by EM will be stored here
  core::calc::statistics::NormalDistribution d1(ave1, std1), d2(ave2, std2), d3(ave3, std1);
  distributions_1D.push_back(d1);
  distributions_1D.push_back(d2);
  distributions_1D.push_back(d3);

  std::cout << "1D distributions: starting params\n";		//print parameters_ of all distributions
  for (const auto & d : distributions_1D) std::cout << d << "\n";
  double score = expectation_maximization(random_points, distributions_1D, index_1D, true);
  std::cout << "1D distributions: resulting params\n";		//print parameters_ of all distributions
  for (const auto & d : distributions_1D) std::cout << d << "\n";
  std::cout << "\n";

  // ---------- now a mixture of 2 Gaussians in 2D
  std::vector<core::calc::statistics::BivariateNormal> distributions_2D;
  std::vector<core::index1> index_2D;
  core::calc::statistics::BivariateNormal d4(ave2, ave3, std2, std3, 0.1), d5(ave3, ave2, std3, std2, 0.1);
  distributions_2D.push_back(d4);
  distributions_2D.push_back(d5);
  std::vector<std::vector<double> > data_2D;
  std::vector<double> rr1(2), rr2(2);
  for (core::index4 i = 0; i < N; ++i) {
    rr1[0] = (p(gen));
    rr1[1] = (t(gen));
    rr2[0] = (t(gen));
    rr2[1] = (p(gen));
    data_2D.push_back(rr1);
    data_2D.push_back(rr2);
  }

  std::cout << "2D distributions: starting params\n";		//print parameters_ of all distributions
  for (const auto & d : distributions_2D) std::cout << d << "\n";
  expectation_maximization(data_2D, distributions_2D, index_2D, true);
  std::cout << "2D distributions: resulting params\n";		//print parameters_ of all distributions
  for (const auto & d : distributions_2D) std::cout << d << "\n";
}

