#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/data/io/pir_io.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Reads a PDB file and writes  protein sequence(s) in PIR format
USAGE:
    ap_pdb_to_pir 5edw.pdb

)";

/** @brief Reads a PDB file and writes  protein sequence(s) in PIR format
 *
 * CATEGORIES: core::data::io::PirEntry
 * KEYWORDS:   PDB input; PIR
 * GROUP:      File processing;Format conversion
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io;
  using namespace core::data::sequence;

  core::data::io::Pdb reader(argv[1],is_not_alternative, only_ss_from_header, true);
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

  // Iterate over all chains
  for (auto it_chain = strctr->begin(); it_chain!=strctr->end(); ++it_chain) {
    PirEntry e("",(*it_chain)->create_sequence()->sequence);
    e.type(PirEntryType::STRUCTURE_X);
    e.code(strctr->code());
    e.first_residue_id((*it_chain)->front()->id());
    e.first_chain_id((*it_chain)->char_id());
    e.last_residue_id((*it_chain)->back()->id());
    e.last_chain_id((*it_chain)->char_id());

    std::cout << e<<"\n";
  }
}
