#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ap_pdb_to_pir test_inputs/2gb1.pdb"
./ap_pdb_to_pir test_inputs/2gb1.pdb
echo -e "Running ./ap_pdb_to_pir test_inputs/2kwi.pdb"
./ap_pdb_to_pir test_inputs/2kwi.pdb
