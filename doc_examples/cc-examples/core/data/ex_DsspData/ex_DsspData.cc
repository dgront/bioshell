#include <iostream>
#include <core/data/io/fasta_io.hh>
#include <core/data/io/DsspData.hh>
#include <utils/exit.hh>

std::string program_info = R"(

ex_DsspData reads a DSSP file and writes secondary structure in FASTA format

USAGE:
    ex_DsspData input.dssp
EXAMPLE:
    ex_DsspData 5edw.dssp

)";

/** @brief Reads a DSSP file and prints the sequence and the secondary structure of each chain in FASTA format.
 *
 * @see ex_dssp_to_ss2.cc converts DSSP to SS2 format
 *
 * CATEGORIES: core/data/io/DsspData
 * KEYWORDS:   DSSP; FASTA output; Structure; secondary structure; Format conversion
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  core::data::io::DsspData dssp(argv[1], true);  // --- read a DSSP file - the first command line argument of the program
  for (const auto & ss2 : dssp.sequences()) // --- for each protein sequence found in the DSSP data ...
    std::cout << core::data::io::create_fasta_string(*ss2, 80) << "\n" // --- print the sequence as FASTA
        << core::data::io::create_fasta_secondary_string(*ss2, 80) << "\n";  // --- print the secondary structure as FASTA
}
