#include <core/data/io/Cif.hh>
#include <core/data/io/mmCif.hh>

#include <utils/Logger.hh>
#include <utils/LogManager.hh>

#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to read CIF files.

USAGE:
    ex_Cif file.cif
EXAMPLE:
    ex_Cif AA3.cif

)";

/** @brief ex_Cif tests reading CIF files
 *
 * CATEGORIES: core/data/io/Cif
 * KEYWORDS:   CIF input
 */
int main(const int argc, const char *argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  utils::LogManager::INFO(); // --- INFO is the default logging level; set it to FINE to see more
  core::data::io::mmCif reader(argv[1]);

  std::cout<<reader.pdb_code()<<"\n";

  core::data::structural::Structure_SP strc = reader.create_structure(0);
    for (auto a=strc->first_atom();a!=strc->last_atom();++a)
        std::cout<< (*a)->to_pdb_line()<<"\n";

}
