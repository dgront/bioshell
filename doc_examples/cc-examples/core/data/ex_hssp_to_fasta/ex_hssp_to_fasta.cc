#include <iostream>

#include <core/data/io/hssp_io.hh>
#include <core/data/io/fasta_io.hh>
#include <utils/exit.hh>
#include <utils/options/OptionParser.hh>

std::string program_info = R"(

Simple test which reads a Multiple Sequence Alignment in the HSSP file format and writes it
in the FASTA format.

USAGE:
    ./ex_hssp_to_fasta input.hssp
EXAMPLE:
    ./ex_hssp_to_fasta 1crn.hssp

REFERENCE:
Soding, J and Biegert, A and Lupas, A. N.,
"The HHpred interactive server for protein homology detection and structure prediction."
Nucleic acids research (2005) 33 W244--W248
)";

/** @brief Reads an MSA in HSSP format and writes a FASTA file.
 *
 *
 * USAGE:
 *     ex_hssp_to_fasta 1crn.pdb
 *
 * CATEGORIES: core::data::io::hssp_io;
 * KEYWORDS:   sequence alignment; FASTA; HSSP
 * GROUP:      File processing; Format conversion
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::data::io;
  std::vector<std::shared_ptr<core::data::sequence::Sequence>>  sink;
  core::data::io::read_hssp_file(argv[1], sink);
  for(const auto & seq:sink) {
    std::cout << create_fasta_string(seq->header(), seq->sequence)<<"\n";
  }
}
