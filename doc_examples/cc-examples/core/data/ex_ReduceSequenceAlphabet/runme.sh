#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "# Running ./ex_ReduceSequenceAlphabet to print all known alphabets"
./ex_ReduceSequenceAlphabet

echo -e "# Running ./ex_ReduceSequenceAlphabet lz-mj.16"
./ex_ReduceSequenceAlphabet lz-mj.16
