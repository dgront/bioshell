#include <iostream>
#include <iomanip>

#include <core/data/sequence/ReduceSequenceAlphabet.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

If no input is given, ex_ReduceSequenceAlphabet lists all reduced amino acid alphabets registered in BioShell library.
Alternatively, user can provide an alphabet name; in this case the relevant mapping is printed on the screen.

USAGE:
    ex_ReduceSequenceAlphabet [alphabet_name]

EXAMPLEs:
    ex_ReduceSequenceAlphabet
    ex_ReduceSequenceAlphabet lz-mj.16

)";

/** @brief ex_ReduceSequenceAlphabet lists all reduced amino acid alphabets registered in BioShell library
 *
 * CATEGORIES: core::data::sequence::ReduceSequenceAlphabet
 * KEYWORDS:   reduced alphabet
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using core::data::sequence::ReduceSequenceAlphabet;

  int i = 1;
  std::cout << "Known alphabets:\n";
  for (auto it = ReduceSequenceAlphabet::cbegin(); it != ReduceSequenceAlphabet::cend(); ++it) {
    std::cout << std::setw(8) << it->first << ((i % 10 == 0) ? "\n" : " ");
    ++i;
  }
  std::cout << "\n";
  for (int i = 1; i < argc; ++i) {
    std::cout << "Listing alphabet " << argv[i] << ":\n";
    core::data::sequence::ReduceSequenceAlphabet_SP alph = ReduceSequenceAlphabet::get_alphabet(argv[i]);
    std::cout << (*alph) << "\n";
  }
}
