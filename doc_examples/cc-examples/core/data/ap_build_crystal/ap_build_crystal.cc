#include <iostream>
#include <core/data/io/Pdb.hh>
#include <utils/exit.hh>
#include <iomanip>

std::string program_info = R"(

ap_create_crystal reads a given PDB file and prints all atoms in a unit cell.

USAGE:
    ap_create_crystal 5edw.pdb

)";

/** @brief ap_create_crystal reads a given PDB file and prints all atoms in a unit cell.
 *
 * CATEGORIES: core/data/io/Pdb;
 * KEYWORDS:   PDB input; PDB line filter; Structure
 * GROUP: Structure calculations;
 */
int main(const int argc, const char *argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  core::data::io::Pdb reader(argv[1], // file name (PDB format, may be gzip-ped)
    core::data::io::keep_all,         // a predicate - now read all atoms
      core::data::io::keep_all, true);                            // parse PDB header

  std::shared_ptr<core::data::io::Remark290> r290 = reader.symmetry_operators();
  core::data::structural::Structure_SP s = reader.create_structure(0);
  std::cout << "# Symmetry operators found: " << r290->count_operators() << "\n";
  core::data::basic::Vec3 tmp;
  core::index2 im = 0;
  for (const auto &rt: *r290) {
    std::cout << "MODEL  " << std::setw(6) << ++im << "\n";
    for (auto a_it = s->first_atom(); a_it != s->last_atom(); ++a_it) {
      tmp.set((**a_it));
      rt.apply(**a_it);
      std::cout << (*a_it)->to_pdb_line() << "\n";
      (*a_it)->set(tmp);
    }
    std::cout << "ENDMDL\n";
  }
}
