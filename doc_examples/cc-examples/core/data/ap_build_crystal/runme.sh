#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ap_build_crystal test_inputs/5edw.pdb > 5edw_uc.pdb"
./ap_build_crystal test_inputs/5edw.pdb > 5edw_uc.pdb
echo -e "Running ./ex_Remark290 test_inputs/3dcg.pdb > 3dcg_uc.pdb"
./ap_build_crystal test_inputs/3dcg.pdb > 3dcg_uc.pdb

mkdir -p outputs_from_test
mv *_uc.pdb outputs_from_test
