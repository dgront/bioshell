#include <memory>
#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/Chain.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

A very basic example showing how to use shared pointers (from standard C++ 11 library) when programming in BioShell.

USAGE:
./ex_shared_pointers

)";

using namespace core::data::structural;

// --- An example function that takes a reference to an object as an argument
void show_chain_code(const Chain & c) { std::cout << c.id() << "\n"; }

/** @brief A very basic example showing how to use shared pointers (from standard C++ 11 library) when programming in BioShell.
 *
 * CATEGORIES: std::make_shared
 * KEYWORDS:   STL
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  // --- This is how we create a shared pointer to a Chain object.
  // --- The object is empty i.e. it doesn't contain any residues
  Chain_SP chain_sp = std::make_shared<Chain>("A");
  // --- This is the same as above, but here we create a Chain object
  Chain chain_object("A");

  // --- This method creates a chain of a given amino acid sequence and returns a shared pointer to it
  Chain_SP longer_sp =  Chain::create_ca_chain("AGGACL","A");

  // --- call a method that takes a reference to an object
  show_chain_code(chain_object);
  // --- here we create a reference from a shared pointer
  show_chain_code(*chain_sp);
}
