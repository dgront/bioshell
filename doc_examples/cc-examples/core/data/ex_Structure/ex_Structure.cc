#include <iostream>
#include <iomanip>
#include <core/algorithms/predicates.hh>
#include <core/data/io/Pdb.hh>
#include <utils/exit.hh>

std::string program_info = R"(

ex_Structure reads a PDB file and prints a list of all atoms grouped by residues they belong to
EXAMPLE:
    ./ex_Structure 5edw.pdb

)";

/** @brief Reads a PDB file and prints a list of all atoms grouped by residues they belong to.
 *
 * CATEGORIES: core::data::structural::Structure
 * KEYWORDS:   PDB input; Structure; Chain; Residue; PdbAtom; STL
 */
int main(const int argc, const char* argv[]) {
 
    if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
    using namespace core::data::io;

    core::data::io::Pdb reader(argv[1],is_not_alternative); // file name (PDB format, may be gzip-ped)
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    
    // Iterate over all chains
    for (auto it_chain = strctr->begin(); it_chain!=strctr->end(); ++it_chain) {
      std::cout << "---------- chain " << (*it_chain)->id() << " ----------\n";
      // Iterate over all residues
      for (auto it_res = (*it_chain)->begin(); it_res!=(*it_chain)->end(); ++it_res) {
        std::cout << std::setw(5)<<(*it_res)->id()<<" "<<(*it_res)->residue_type().code3<<" :";
        for (auto it_atom = (*it_res)->begin(); it_atom!=(*it_res)->end(); ++it_atom) {
        if (((*it_atom)->alt_locator() == ' ') || ((*it_atom)->alt_locator() == 'A'))
          std::cout << " " << (*it_atom)->atom_name();
        }
        std::cout <<"\n";
      }
    }    
}
