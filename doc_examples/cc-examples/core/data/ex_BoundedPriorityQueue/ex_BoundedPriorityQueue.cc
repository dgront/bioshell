#include <iostream>
#include <random>

#include <core/index.hh>
#include <core/calc/statistics/Random.hh>
#include <core/data/basic/BoundedPriorityQueue.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to use the BoundedPriorityQueue data structure.

BoundedPriorityQueue is a sorted queue with pre-defined maximum capacity. It's purpose is to keep N best elements
of what was inserted to the queue. Overflow elements are removed from the queue

USAGE:
./ex_BoundedPriorityQueue

)";

using namespace core::data::basic;

/** @brief Simple demo for BoundedPriorityQueue class
 *
 * This program creates a BoundedPriorityQueue and fills it with random numbers.
 * When printed, they should be ordered descending
 *
 * CATEGORIES: core::data::basic::BoundedPriorityQueue
 * KEYWORDS:   algorithms; data structures; BoundedPriorityQueue
 */
int main(int cnt, char *argv[]) {

  if ((cnt > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  // ---------- test on real values
  typedef std::function<bool(const double, const double)> ComparatorType;
  core::data::basic::BoundedPriorityQueue<double, ComparatorType, ComparatorType> q(
    [&](double x, double y) { return x > y; },
    [&](double x, double y) { return x == y; }, 10, 20, -std::numeric_limits<double>::max());
  core::calc::statistics::Random r = core::calc::statistics::Random::get();
  std::uniform_real_distribution<float> flat_f(0, 10.0);
  for (core::index4 i = 0; i < 30; ++i)
    q.push(flat_f(r));
  for (core::index4 i = 1; i < q.size(); ++i) {
    std::cout << q[i] << " ";
    if (q[i - 1] < q[i])
      std::cerr
        << utils::string_format("Incorrect ordering in a bounded priority queue, %f before %f\n", q[i - 1], q[i]);
  }

  std::cout << "\n";

  // ---------- test on integers
  typedef std::function<bool(int, int)> ComparatorTypeI;
  core::data::basic::BoundedPriorityQueue<int, ComparatorTypeI, ComparatorTypeI> q_i(
    [&](int x, int y) { return x > y; },
    [&](int x, int y) { return x == y; }, 10, 20, -std::numeric_limits<int>::max());
  std::uniform_int_distribution<int> flat(0, 20);
  for (core::index4 i = 0; i < 30; ++i)
    q_i.push(flat(r));
  for (core::index4 i = 1; i < q_i.size(); ++i) {
    std::cout << q_i[i] << " ";
    if (q_i[i - 1] < q_i[i])
      std::cerr
        << utils::string_format("Incorrect ordering in a bounded priority queue, %f before %f\n", q_i[i - 1], q_i[i]);
  }
}
