#include <iostream>
#include <core/data/basic/Array2DSymmetric.hh>
#include <core/calc/statistics/Random.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which demonstrates how to use Array2DSymmetric class. The test fills a matrix with random data
and prints it on the screen.

USAGE:
    ./ex_Array2DSymmetric

)";

/** @brief Simple test for Array2DSymmetric class.
 *
 * The test fills a mtrix with random data and prints it on the screen.
 *
 * CATEGORIES: core::data::basic::Array2DSymmetric
 * KEYWORDS:   data structures; random numbers
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  core::calc::statistics::Random r = core::calc::statistics::Random::get();
  std::uniform_int_distribution<core::index1> uniform_bytes;
  r.seed(12345);
  core::data::basic::Array2DSymmetric<core::index1> m(10);
  for (core::index4 n = 0; n < 1000; ++n) {
    core::index1 i = uniform_bytes(r) % 10;
    core::index1 j = uniform_bytes(r) % 10;
    m.set(i, j, uniform_bytes(r));
  }
  m.print("%4d",std::cout);
}
