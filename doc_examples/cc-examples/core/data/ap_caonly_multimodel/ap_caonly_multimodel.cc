#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <cstring>

#include <core/data/io/Pdb.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <utils/exit.hh>

using namespace core::data::io; // PDB is from this namespace
using namespace core::data::structural;
using namespace core::calc::structural;
using namespace utils;

std::string program_info = R"(

Reads a file with names of PDB files  and creates a single multimodel PDB file. Each model
is stored as a separate model within that file. Only C-alpha atoms are written to the output PDB

EXAMPLE:
    ap_caonly_multimodel cat_list
where cat_list is a file with a content like:

2gb1-model1.pdb
2gb1-model2.pdb
2gb1-model3.pdb
2gb1-model4.pdb
)";

/** @brief Reads cat_list of pdb files and creates multimodel pdb with CA only 
 *
 * CATEGORIES: core::data::io::is_ca
 * KEYWORDS:   PDB input; CA only; structure selectors;PDB output; PDB line filter
 * GROUP:      File processing;Format conversion
 */
int main(const int argc, const char *argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info);

  PdbLineFilter filter = core::data::io::is_ca;
  std::ofstream out;
  out.open("all.pdb");
  std::ifstream pdb_list(argv[1]);
  std::string pdb_file;
  std::getline(pdb_list, pdb_file);
  Pdb pdb = Pdb(pdb_file, filter);
  Structure_SP strctr = pdb.create_structure(0);
  out << "MODEL   1\n";
  for (auto it = strctr->first_atom(); it != strctr->last_atom(); it++) out << (*it)->to_pdb_line() << "\n";
  out << "ENDMDL\n";
  core::index4 i = 2;
  while (std::getline(pdb_list, pdb_file)) {
    Pdb pdb = Pdb(pdb_file, filter);
    pdb.fill_structure(0, *strctr);
    out << utils::string_format("MODEL   %6d\n", i);
    for (auto it = strctr->first_atom(); it != strctr->last_atom(); it++)
      out << (*it)->to_pdb_line() << "\n";
    out << "ENDMDL\n";
    i++;
  }

  out.close();
}


