#include <core/data/io/Cif.hh>
#include <utils/Logger.hh>
#include <utils/LogManager.hh>

#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to read CIF files.

USAGE:
    ex_Cif file.cif
EXAMPLE:
    ex_Cif AA3.cif

)";

/** @brief ex_Cif tests reading CIF files
 *
 * CATEGORIES: core/data/io/Cif
 * KEYWORDS:   CIF input
 */
int main(const int argc, const char *argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  utils::LogManager::FINEST(); // --- INFO is the default logging level; set it to FINE to see more
  core::data::io::Cif reader(argv[1]);
  std::cout << reader;
}
