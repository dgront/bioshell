#include <iostream>

#include <core/algorithms/predicates.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/io/fasta_io.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Reads a PDB file and writes protein sequence(s) in FASTA format.

The program also writes secondary structure in FASTA format, if this data is available from PDB headers.
The sequence comprise only these amino acid residues which have C-alpha atom
User can select a chain by providing its code as the second argument of the program. The program also writes a PDB file
that corresponds to the sequence.

USAGE:
    ap_pdb_to_fasta_ss input.pdb chain-code

EXAMPLE:
    ap_pdb_to_fasta_ss 5edw.pdb A

OUTPUT:
>2GB1 A
MTYKLILNGKTLKGETTTEAVDAATAEKVFKQYANDNGVDGEWTYDDATKTFTVTE

>2GB1 A - secondary structure
CEEEEEECCCCCCEEEEEECCHHHHHHHHHHHHHHHCCCCCEEEEECCCCEEEEEC

)";

/** @brief Reads a PDB file and writes protein sequence(s) in FASTA format.
 *
 * The program also writes secondary structure in FASTA format, if this data is available from PDB headers.
 * User can select a chain by providing its code as the second argument of the program
 * USAGE:
 *     ap_pdb_to_fasta_ss 5edw.pdb A
 *
 * CATEGORIES: core::data::io::Pdb; core::algorithms::Not; core::data::sequence::SecondaryStructure
 * KEYWORDS:   PDB input; FASTA output; secondary structure; predicates
 * GROUP:      File processing; Format conversion
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io; // Pdb and create_fasta_string lives there
  using namespace core::data::structural; // Chain and

  Pdb reader(argv[1],is_not_alternative,core::data::io::keep_all,true);
  Structure_SP strctr = reader.create_structure(0);

  // Iterate over all chains
  for (auto it_chain = strctr->begin(); it_chain!=strctr->end(); ++it_chain) {
    Chain & c = **it_chain; // --- dereference iterator for easier access
    if ((argc > 2) && ((*it_chain)->id() != argv[2])) continue;

    // --- The line below uses STL algorithm with BioShell predicate to remove all the residues lacking c-alpha
    c.erase(std::remove_if(c.begin(), c.end(), core::algorithms::Not<selectors::ResidueHasCA>(selectors::ResidueHasCA())), c.end());

    if(c.size()>0) {
      // --- Create a sequence object (including secondary structure information)
      core::data::sequence::SecondaryStructure_SP s = (*it_chain)->create_sequence();
      // --- Write sequence as FASTA
      std::cout << create_fasta_string(*s) << "\n";
      // --- Write secondary structure as FASTA
      std::cout << create_fasta_secondary_string(*s) << "\n";
    }
  }
}
