#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ap_pdb_to_fasta_ss test_inputs/5edw.pdb"
./ap_pdb_to_fasta_ss test_inputs/5edw.pdb

echo -e "Running ./ap_pdb_to_pir test_inputs/5d95.pdb A"
./ap_pdb_to_fasta_ss test_inputs/5d95.pdb A

echo -e "# Running ./ap_pdb_to_fasta_ss test_inputs/5d95.pdb"
./ap_pdb_to_fasta_ss test_inputs/5d95.pdb

echo -e "# Running ./ap_pdb_to_fasta_ss test_inputs/2gb1.pdb"
./ap_pdb_to_fasta_ss test_inputs/2gb1.pdb

