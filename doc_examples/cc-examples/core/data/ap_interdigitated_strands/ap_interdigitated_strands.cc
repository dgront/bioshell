#include <core/data/io/Pdb.hh>
#include <core/algorithms/graph_algorithms.hh>
#include <core/data/structural/BetaStructuresGraph.hh>
#include <core/calc/structural/ProteinArchitecture.hh>

#include <utils/exit.hh>

std::string program_info = R"(

Reads a PDB file, creates a BetaStructuresGraph for it and finds all interdigitated strands.
A strand is interdigitated when its hydrogen-bonded neighbors within a beta sheet come from
different protein chains than that strand.

EXAMPLE:
    ap_interdigitated_strands 2fdo.pdb

REFERENCE:
Wang S. et al. "Crystal Structure of the Conserved Protein of Unknown Function AF2331 from Archaeoglobus fulgidus
DSM 4304 Reveals a New Type of Alpha/Beta Fold" Protein Sci. (2009) 18 2410–2419.
)";

void index_strands(core::data::structural::BetaStructuresGraph_SP g) {

  using core::data::structural::Strand_SP;

  // ---------- Firstly, let's find the first strand on the path: the one with just one partner
  Strand_SP first_strand = nullptr;
  for (auto it = g->begin_strand(); it != g->end_strand(); ++it) {
    if (g->count_partners(*it) == 1) {
      if (first_strand == nullptr) first_strand = *it;
      else if (first_strand->length() > (*it)->length()) // there are two edge strands, take the shorter one
        first_strand = *it;
    }
  }

  // ---------- If it's a barrel, take the shortest one
  if (first_strand == nullptr) {
    Strand_SP first_strand = *g->begin_strand();
    for (auto it = g->begin_strand(); it != g->end_strand(); ++it) {
      if (first_strand->length() > (*it)->length()) first_strand = *it;
    }
  }

  std::set<Strand_SP> visited;
  std::vector<Strand_SP> stack;
  std::vector<Strand_SP> scratch;
  stack.push_back(first_strand);
  core::index2 idx = 0;
  while(stack.size()>0) {
    // --- pop a strand from stack, mark as visited
    Strand_SP s = stack.back();
    s->strand_index_in_sheet = (++idx);
    visited.insert(s);
    stack.pop_back();

    // --- get its neighbors, push to scratch if not visited yet
    scratch.clear();
    for (auto it = g->begin_strand(s); it != g->end_strand(s); ++it)
      if(visited.find(*it)==visited.cend())
        scratch.push_back(*it);

    // --- sort neighbors
    std::sort(scratch.begin(), scratch.end(),
              [](Strand_SP lhs, Strand_SP rhs) { return rhs->length() < lhs->length(); });
    // --- push from the shortest
    for(Strand_SP si:scratch) stack.push_back(si);
  }

}

struct OrderStandsInSheet {

  bool operator()(core::data::structural::Strand_SP lhs, core::data::structural::Strand_SP rhs) { return lhs->strand_index_in_sheet < rhs->strand_index_in_sheet; }
};

/** @brief Creates a BetaStructuresGraph and finds interdigitated sheets
 *
 * CATEGORIES: core::data::structural::BetaStructuresGraph
 * KEYWORDS:   PDB input
 * GROUP: Structure calculations;
 * IMG: 2fdo-7-sq.png
 * IMG_ALT: Interdigitated beta-sheet of 2FDO deposit; the two chains A and B shown with different colors
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  using namespace core::data::structural;
  using namespace core::data::io;

//  core::data::io::Pdb reader(argv[1], (is_not_alternative), true);
  core::data::io::Pdb reader(argv[1], core::data::io::all_true(is_not_alternative, is_not_water), core::data::io::keep_all, true);
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

  std::string sec_str;
  for (auto chain : *strctr) sec_str += chain->create_sequence()->str();
  core::calc::structural::ProteinArchitecture a(*strctr, false);
  BetaStructuresGraph_SP g = a.create_strand_graph();

  g->print_adjacency_matrix(std::cerr);

  for (auto s_it = g->begin(); s_it != g->end(); ++s_it) {
    auto s = *s_it;
    for (auto nbr = g->begin_strand(s); nbr != g->end_strand(s); ++nbr) {
      core::data::structural::Strand_SP strnd = *nbr;
    }
  }

  std::vector<StrandPairing_SP> edges;
  for (auto it = g->cbegin_pairings(); it != g->cend_pairings(); ++it) edges.push_back((*it).second);
  for (StrandPairing_SP sp:edges) {
    Strand_SP first = sp->first_strand;
    Strand_SP second = sp->second_strand;
    if ((*first)[0]->owner()->id() == (*second)[0]->owner()->id()) {
      g->remove_strand_pairing(first, second);
    }
  }

  auto sheets = core::algorithms::connected_components<BetaStructuresGraph, Strand_SP, StrandPairing_SP>(*g, 2);
  int cnt = 0;
  for(const auto & sheet: sheets) {
    std::cout << utils::string_format("-------------- Sheet %d -------------------\n",++cnt);
    auto strnd = sheet->cbegin_strand();

    index_strands(g); // --- index strands in a current sheet

    std::vector<Strand_SP> strands;
    for(auto it=sheet->cbegin_strand();it!=sheet->cend_strand();++it)
      strands.push_back(*it);
    std::sort(strands.begin(), strands.end(),OrderStandsInSheet{});
    for (auto s:strands) std::cout << *s << ", has " << g->count_partners(s) << " edges\n";
  }
}
