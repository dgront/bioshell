#include <iostream>
#include <core/data/io/alignment_io.hh>
#include <core/data/io/fasta_io.hh>
#include <core/data/io/pir_io.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Reads alignments from HHPred output and prints then in Edinburgh, FASTA or PIR format, according to given flag.
The list of available flags:

 -e for Edinburgh output format
 -f for FASTA output format
 -p for PIR output format

USAGE:
    ap_hhpred_converter hhpred-file flag
EXAMPLE:
    ap_hhpred_converter hhpred.out -p

REFERENCE:
Soding, J and Biegert, A and Lupas, A. N.,
"The HHpred interactive server for protein homology detection and structure prediction."
Nucleic acids research (2005) 33 W244--W248
)";

/** @brief Extract alignments from HHPred output
 *
 * CATEGORIES: core::data::io::read_hhpred;
 * KEYWORDS:   sequence alignment; FASTA; PIR; Edinburgh
 * GROUP:      File processing; Format conversion
 */
int main(const int argc, const char* argv[]) {

  if(argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  std::vector<core::alignment::PairwiseSequenceAlignment_SP> alignments;
  core::data::io::read_hhpred(argv[1], alignments);

  char flag = argv[2][1]; // --- E, e, F, f, P, p
  switch(flag) {
    case 'E' :
    case 'e' :
      for(const auto & seq_ali : alignments)
        core::data::io::write_edinburgh(*seq_ali, std::cout, 80);
      break;
    case 'F' :
    case 'f' :
      for(const auto & seq_ali : alignments)
        std::cout << core::data::io::create_fasta_string(*seq_ali, 80) << "\n";
      break;
    case 'P' :
    case 'p' :
      for(const auto & seq_ali : alignments)
        std::cout << core::data::io::create_pir_string(*seq_ali, 80) << "\n";
      break;
    default: std::cerr << "Incorrect output format requested!\n";
  }

}
