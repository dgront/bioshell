#include <iostream>

#include <core/data/io/XML.hh>
#include <core/data/io/XMLElement.hh>
#include <utils/LogManager.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Demonstrate how to parse XML with BioShell utilities. The test runs on a predefined XML data

USAGE:
  ./ex_XML

)";

using namespace core::data::io;

std::string xml_data =
  R"(<product>
     <id>15</id>
     <name>Widgets</name>
     <description>Example text.</description>
     <options type="color">
          <item value="Purple" shade="bright" />
          <item>Green</item>
          <item>Orange</item>
     </options>
</product>
)";

/** @brief Simple for XML I/O utils.
 *
 * CATEGORIES: core/data/io/XML
 * KEYWORDS:   XML
 */
int main(int argc, char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  utils::LogManager::FINE();
  XML xxx;
  if (argc == 1) {
    std::istringstream input(xml_data);
    xxx.load_data(input);
  } else xxx.load_data(argv[1]);
  std::cout << xxx.document_root();

  return 0;
}
