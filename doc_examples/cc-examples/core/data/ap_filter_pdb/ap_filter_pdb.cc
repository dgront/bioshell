#include <iostream>
#include <core/data/io/Pdb.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Shows the concept of PDB line filters in BioShell: creates a PDB reader which accepts only desired atoms/groups.
The filter used by this example to read PDB file is created based on filter names (space separated). For each string
a distinct filter will be created; all filters will be joined with logical AND operation i.e. all must be true
to read in a PDB line.  Therefore the last example below will return an empty set of atoms because the two filters
it uses are contradictory.

USAGE:
    ex_filter_pdb 5edw.pdb filter-names

EXAMPLEs:
    ex_filter_pdb 5edw.pdb is_standard_atom
    ex_filter_pdb 5edw.pdb is_bb is_cb

)";

/** @brief Filters a PDB file by a given filter
 *
 * CATEGORIES: core::data::io::Pdb;
 * KEYWORDS:   PDB input; PDB line filter
 */
int main(const int argc, const char* argv[]) {

  if(argc < 3) {
    program_info += "\nKnown filters:\n";
    for (const std::string &name: core::data::io::Pdb::pdb_filter_names)
      program_info += "\t" + name;
    utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  }
  std::string filter_names(argv[2]);
  for (int i = 3; i < argc; ++i) filter_names += " " + std::string(argv[i]);
  core::data::io::Pdb reader(argv[1], // file name (PDB format, may be gzip-ped)
                             filter_names, // filter names combined into a single string
                             false); // don't parse header to achieve highest speed

  for (int im = 0; im < reader.count_models(); ++im)
    for (const core::data::io::Atom &pdb_line : (*reader.atoms[im]))
      std::cout << pdb_line.to_pdb_line() << "\n";

}
