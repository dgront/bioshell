#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "# Running ./ap_filter_pdb  test_inputs/5edw.pdb is_not_water"
./ap_filter_pdb  test_inputs/5edw.pdb is_not_water

echo -e "# Running ./ap_filter_pdb  test_inputs/5edw.pdb is_water"
./ap_filter_pdb  test_inputs/5edw.pdb is_water

echo -e "# Running ./ap_filter_pdb  test_inputs/5edw.pdb is_ca"
./ap_filter_pdb  test_inputs/5edw.pdb is_ca

echo -e "# Running ./ap_filter_pdb  test_inputs/5edw.pdb is_bb"
./ap_filter_pdb  test_inputs/5edw.pdb is_bb

echo -e "# Running ./ap_filter_pdb  test_inputs/5edw.pdb is_hetero_atom"
./ap_filter_pdb  test_inputs/5edw.pdb is_hetero_atom

echo -e "# Running ./ap_filter_pdb  test_inputs/2gb1.pdb is_hydrogen"
./ap_filter_pdb  test_inputs/2gb1.pdb is_hydrogen

echo -e "# Running ./ap_filter_pdb  test_inputs/2gb1.pdb is_not_hydrogen"
./ap_filter_pdb  test_inputs/2gb1.pdb is_not_hydrogen

echo -e "# Running./ap_filter_pdb test_inputs/5edw.pdb is_bb is_ca"
./ap_filter_pdb test_inputs/5edw.pdb is_bb is_ca

echo -e "# Running./ap_filter_pdb test_inputs/5edw.pdb is_bb is_cb"
./ap_filter_pdb test_inputs/5edw.pdb is_bb is_cb
