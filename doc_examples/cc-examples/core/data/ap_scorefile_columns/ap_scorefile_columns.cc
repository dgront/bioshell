#include <iostream>

#include <core/data/io/scorefile_io.hh>
#include <utils/exit.hh>
#include <utils/Logger.hh>

std::string program_info = R"(

Reads a score file or a silent file (produced by Rosetta) and extracts requested columns of scores
USAGE:
    ap_scorefile_columns default.out
    ap_scorefile_columns score.fsc
    ap_scorefile_columns 1pgxA-abinitio.fsc rms ss_pair rsigma

)";

/** @brief Reads a score-file or a silent file (produced by Rosetta) and extracts requested columns of scores
 *
 * CATEGORIES: core::data::io::scorefile_io
 * KEYWORDS:   Rosetta scorefile;
 * GROUP:      File processing;Data filtering
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io;

  utils::Logger logs("ap_scorefile_columns");

  std::shared_ptr<NamedDataTable> fsc = read_scorefile(argv[1]);
  std::vector<core::index1> columns;
  if (argc > 2) {
    for (core::index1 i = 2; i < argc; ++i)
      if (fsc->has_column(argv[i]))
        columns.push_back(fsc->column_index(argv[i]));
      else
        logs << utils::LogLevel::WARNING << "Unknown column ID: " << argv[i] << "\n";
  } else {
    columns.push_back(fsc->column_index("score"));
    columns.push_back(fsc->column_index("rms"));
  }
  std::vector<std::string> tags;
  std::cout << "#";
  for (core::index1 icol : columns) std::cout << " "<< fsc->column_name(icol) ;
  std::cout << "\n";
  for (const auto &row: *fsc) {
    for (core::index1 icol : columns) std::cout << row[icol] << " ";
    std::cout << "\n";
  }
}
