#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ap_scorefile_columns test_inputs/1pgxA-abinitio.fsc"
./ap_scorefile_columns test_inputs/1pgxA-abinitio.fsc

echo -e "Running ./ap_scorefile_columns test_inputs/1pgxA-abinitio.fsc rms ss_pair rsigma"
./ap_scorefile_columns test_inputs/1pgxA-abinitio.fsc rms ss_pair rsigma

echo -e "Running ./ap_scorefile_columns test_inputs/1pgxA-abinitio.fsc rms ss_pair rsigma whatever"
./ap_scorefile_columns test_inputs/1pgxA-abinitio.fsc rms ss_pair rsigma whatever
