#!/bin/bash

echo -e "# Running ./ap_SequenceProfile test_inputs/cyped.CYP109.aln > cyped.CYP109.profile"
./ap_SequenceProfile test_inputs/cyped.CYP109.aln > cyped.CYP109.profile

mkdir -p  outputs_from_test
mv cyped.CYP109.profile outputs_from_test/

echo -e "# Running ./ap_SequenceProfile test_inputs/cyped.CYP109.aln -w > cyped.CYP109.wght.profile"
./ap_SequenceProfile test_inputs/cyped.CYP109.aln -w > cyped.CYP109.wght.profile

mv cyped.CYP109.wght.profile outputs_from_test/

echo -e "# Running ./ap_SequenceProfile test_inputs/1crn.hssp > 1crn.profile"
./ap_SequenceProfile test_inputs/1crn.hssp > 1crn.profile

mv 1crn.profile outputs_from_test/

