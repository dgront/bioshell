#include <iostream>

#include <core/data/io/hssp_io.hh>
#include <core/data/io/fasta_io.hh>
#include <core/data/io/clustalw_io.hh>
#include <core/data/sequence/SequenceProfile.hh>
#include <core/protocols/SequenceWeightingProtocol.hh>
#include <utils/exit.hh>
#include <utils/io_utils.hh>

std::string program_info = R"(

ap_SequenceProfile reads a Multiple Sequence Alignment (MSA) in ClustalO or FASTA format
and prints a sequence profile made from it. The program detects the format of ain input file
by its extension: use either .fasta or .aln, for FASTA and ClustalO, respectively.

If the optional argument -w is used, sequences will be weighted before profile calculations.
The profile probabilities will be therefore weighted counts rather than just raw observations.

USAGE:
    ./ap_SequenceProfile infile.aln [-w]

EXAMPLE:
    ./ap_SequenceProfile cyped.CYP109.aln
    ./ap_SequenceProfile cyped.CYP109.fasta -w

)";

/** @brief Reads a MSA in ClustalW format  and prints a sequence profile
 *
 * CATEGORIES: core/data/sequence/SequenceProfile; core/protocols/SequenceWeightingProtocol
 * KEYWORDS:   sequence profile; Clustal input; MSA
 * GROUP: Sequence calculations;
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io;
  using namespace core::data::sequence;

  // ---------- Load all sequences into a vector
  std::vector<Sequence_SP> msa;   // --- Sequence_SP is just a shorter name for std::shared_ptr<Sequence>
  auto root_extn = utils::root_extension(argv[1]);
  if ((root_extn.second == "aln") || (root_extn.second == "clustalw")) {
    core::data::io::read_clustalw_file(argv[1], msa, true);
  } else if (root_extn.second == "hssp") {
    core::data::io::read_hssp_file(argv[1], msa, true, true);
  } else
    core::data::io::read_fasta_file(argv[1], msa);

  std::vector<double> seq_weights{1,1.0}; // --- just one weight of value 1.0
  // ---------- Set up and run sequence weighting protocol if needed
  if ((argc == 3) && (strcmp(argv[2], "-w") == 0)) {
    core::protocols::HenikoffSequenceWeights protocol;
    protocol.n_threads(4).add_input_sequences(msa);
    protocol.run();
    seq_weights.clear();
    for (core::index2 i = 0; i < msa.size(); ++i) seq_weights.push_back(protocol.get_weight(i));
  }

  // ---------- Create a sequence profile and print in on the screen
  SequenceProfile profile(*msa[0], SequenceProfile::aaOrderByPropertiesGapped(), msa, seq_weights);
  profile.write_table(std::cout);
}
