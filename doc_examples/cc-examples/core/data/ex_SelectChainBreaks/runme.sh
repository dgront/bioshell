#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "#Running ./ex_SelectChainBreaks test_inputs/4mcb.pdb"
./ex_SelectChainBreaks test_inputs/4mcb.pdb

echo -e "#Running ./ex_SelectChainBreaks test_inputs/5edw.pdb"
./ex_SelectChainBreaks test_inputs/5edw.pdb

echo -e "#Running ./ex_SelectChainBreaks test_inputs/2fdo.pdb"
./ex_SelectChainBreaks test_inputs/2fdo.pdb

echo -e "#Running ./ex_SelectChainBreaks test_inputs/2gb1.pdb"
./ex_SelectChainBreaks test_inputs/2gb1.pdb
