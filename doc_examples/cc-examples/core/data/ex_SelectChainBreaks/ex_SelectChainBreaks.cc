#include <iostream>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>
#include <utils/exit.hh>
#include <utils/LogManager.hh>


std::string program_info = R"(

Reads a PDB file and prints list of chain breaks found in every chain

EXAMPLE:
    ex_SelectChainBreaks 4mcb.pdb

)";

/** @brief Reads a PDB file and prints a list of chain breaks found in every chain
 *
 * CATEGORIES: core::data::structural::SelectChainBreaks
 * KEYWORDS:   structure selectors; PDB input
 */
int main(const int argc, const char *argv[]) {

  using namespace core::data::structural::selectors;
    utils::LogManager::INFO();


    if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  // ---------- Read a PDB file and create a Structure object
    core::data::io::PdbLineFilter filt1 = core::data::io::all_true(core::data::io::is_ca, core::data::io::is_standard_atom,core::data::io::is_not_alternative);
    core::data::io::PdbLineFilter filt2 = core::data::io::all_true(core::data::io::is_ca, core::data::io::is_hetero_atom,core::data::io::is_not_alternative);
    core::data::io::PdbLineFilter filt3 = core::data::io::one_true(filt1, filt2);


    core::data::io::Pdb reader(argv[1],filt3);
  auto strctr = reader.create_structure(0);

  SelectChainBreaks sel;
  bool chain_is_OK = true;
  for (const auto &chain : *strctr) {
    if (std::distance(chain->begin(),chain->terminal_residue()) < 3) {
      std::cerr << "Chain " << chain->id() << " of " << strctr->code() << " is too short\n";
      continue;
    }
    if (chain->count_aa_residues() < chain->size() * 0.8) {
      std::cerr << "Chain " << chain->id() << " of " << strctr->code() << " is not a protein\n";
      continue;
    }
    std::cerr << "# Processing " << strctr->code() << " chain " << chain->id() << "\n";
    size_t first_aa = 0;
    while ((*chain)[first_aa]->residue_type().type != 'P') ++first_aa;
    const auto last_it = chain->terminal_residue() - 1;
    for (auto res_it = chain->begin() + first_aa + 1; res_it != last_it; ++res_it) {
      auto next = (**res_it).next();
      if(next== nullptr) {
        next = *(++std::find(chain->begin(),chain->end(),*res_it));
        std::cerr << "Residue following "<<(**res_it)<<" is not an amino acid!\n";
      }
      auto prev = (**res_it).previous();
      if(prev == nullptr) {
        prev = *(--std::find(chain->begin(),chain->end(),*res_it));
        std::cerr << "Residue preceding "<<(**res_it)<<" is not an amino acid!\n";
      }

      if (sel(*res_it)) {
        chain_is_OK = false;
        if (sel.last_chainbreak_type == RIGHT) {
          std::cout << utils::string_format("%s %4s %4d%c %4d%c %6.2f\n", strctr->code().c_str(), chain->id().c_str(),
              (*res_it)->id(), (*res_it)->icode(),next->id(), next->icode(), sel.right_side_distance);
          ++res_it;
          if (res_it == last_it) break;
        }
        if (sel.last_chainbreak_type == LEFT) {
          std::cout << utils::string_format("%s %4s %4d%c %4d %c %6.2f\n", strctr->code().c_str(), chain->id().c_str(),
              prev->id(), prev->icode(), (*res_it)->id(), (*res_it)->icode(), sel.left_side_distance);
        }
        if (sel.last_chainbreak_type == BOTH) {
          std::cout << utils::string_format("%s %4s %4d%c %4d %c %6.2f %6.2f\n", strctr->code().c_str(), chain->id().c_str(),
              prev->id(), prev->icode(), (*res_it)->id(), (*res_it)->icode(), sel.left_side_distance);
          std::cout << utils::string_format("%s %4s %4d%c %4d %c\n", strctr->code().c_str(), chain->id().c_str(),
              (*res_it)->id(), (*res_it)->icode(), next->id(), next->icode(), sel.right_side_distance);
          ++res_it;
          if (res_it == last_it) break;
        }
      }
    }
    if(chain_is_OK) std::cout << utils::string_format("%s %4s OK\n", strctr->code().c_str(), chain->id().c_str());
  }
}
