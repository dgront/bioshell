#include <iostream>
#include <core/data/io/json_io.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Demonstrates how to handle JSON data.

USAGE:
    ./ex_JsonNode

)";

/** @brief Demo for handling JSON data
 *
 * The example tests whether a JSON data is parsed and printed correctly.
 *
 * CATEGORIES: core::data::io::JsonNode
 * KEYWORDS:   JSON
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::data::io;

  // Create JsonNode object using constructors
  JsonNode_SP n0 = std::make_shared<JsonNode>(std::make_shared<JsonValue>("options"),1);
  n0->add_branch(std::make_shared<JsonNode>(std::make_shared<JsonValue>("size","56"),2));
  n0->add_branch(std::make_shared<JsonNode>(std::make_shared<JsonValue>("color","red"),3));
  std::cout << n0; // This is how to print a full JSON tree

  std::string json = R"({"options" : {"size" : 56 , "color" : {"fg" : "red", "feature" : "none", "bg" : {"r":25,"g":124,"b":19} }, "do" : "all"})"
  ;
  JsonNode_SP root = read_json(json); // Here json string is parsed
  std::cout << root; // and here send back to a stream

  // Here a JsonArray instance is created; an empty array at first ...
  std::shared_ptr<JsonArray> jv1 = std::make_shared<JsonArray>("res-1");
  // and now that empty array is fileld with data; <code>"011"</code> string means that the first token (i.e. 37) does not
  // require quotes (hence logical 0) and the two latter tokens do (logical 1)
  jv1->values("011",37,"ALA",'A');
  JsonNode_SP jv2 = create_json_node("res-2","011",38,"PHE",'A');
  std::cout << (*jv1) << " " << (jv2) << "\n";

  // Create JsonNode object using helper methods, provided by <code>json_io.hh</code>
  JsonNode_SP another_root = create_json_node();
  another_root->add_branch( create_json_node(jv1) );
  another_root->add_branch( jv2 );
  std::cout << another_root;
}
