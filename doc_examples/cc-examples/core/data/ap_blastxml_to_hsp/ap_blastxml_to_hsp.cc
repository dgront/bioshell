#include <iostream>
#include <fstream>

#include <stdexcept>
#include <core/data/io/BlastXMLReader.hh>
#include <core/algorithms/trees/algorithms.hh>
#include <core/index.hh>
#include <core/data/io/XML.hh>

#include <core/data/io/XMLElement.hh>
#include <core/data/io/Hsp.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Reads a XML file produced by PsiBlast and extracts High Scoring Pairs (HSP). Program prints a table
where each row corresponds to a single HSP found in the input file. The table's columns provide:
  - hit sequence ID
  - hit length
  - alignment score
  - number of  gaps
  - gap percentage
  - number of identical positions
  - identity percentage
  - e-value
  - query start position
  - subject start position
  - subject sequence

USAGE:
    ap_blastxml_to_hsp blastout.xml
EXAMPLE:
    ap_blastxml_to_hsp "1K25_01+PBP_C2.psi"
OUTPUT:
       hit sequence ID    len score gaps  gap% ident ident%    evalue  qpos tpos sequence
[        UniRef50_A0A0E9GHR2]   139   220    0 (  0%)   28 ( 50%)   3.56e-22    3   84 --ELPDMYGWTKENVQVFGKWTGIEVTYQGNGSHVTAQSSDTGTALKKLKKLTITLGE
[        UniRef50_A0A111B192]   151   221    0 (  0%)   48 ( 82%)   4.26e-22    1   94 AEEVPDMYGWTKETAETLAKWLNIELEFQGSGSTVQKQDVRANTAIKDIKKITLTLGD
[            UniRef50_P59676]   750   229    0 (  0%)   48 ( 82%)   2.43e-21    1  693 AEEVPDMYGWTKETAETLAKWLNIELEFQGSGSTVQKQDVRANTAIKDIKKITLTLGD
[            UniRef50_T0UT66]   466   227    0 (  0%)   29 ( 50%)   3.87e-21    2  410 -DAVPDMYGWTKKNADIFGEWTGIEITYKGSGKKVTKQSVKMNTSLNKTKKITLTLGD
[        UniRef50_A0A0T8ADZ4]   322   223    0 (  0%)   58 (100%)   5.32e-21    1  265 VEEIPDMYGWKKETAETFAKWLDIELEFEGSGSVVQKQDVRTNTAIKNIKKIKLTLGD
[        UniRef50_A0A139PMG7]   412   222    0 (  0%)   48 ( 82%)   1.37e-20    1  355 AEEVPDMYGWTKATAETLAKWLNIELEFEGSGSTVQKQDVRANTAIKDIKKITLTLGD
[        UniRef50_A0A0E9EQ17]   236   212    0 (  0%)   29 ( 51%)   5.33e-20    3  181 --EMPDMYGWTKKNVETFGEWLGIKVHVKSKGSKVVAQSVKTNASLKKIKEITITLGD

)";

using namespace core::data::io;
/** @brief Reads XML produced by psiblast and creates High Scoring FASTA Pair
 *
 * CATEGORIES: core::data::io::XML; core::algorithms::trees::TreeNode
 * KEYWORDS:   XML; data structures; HSP
 * GROUP:      File processing;Format conversion
*/ 

int main(int argc, char *argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  // ---------- creates BlastXMLReader object
  BlastXMLReader p;
  // ---------- parse data from XML file and returns it to variable
  auto iterations = p.parse(argv[1]);
  std::cout << Hsp::output_header << "\n";
  // print Hsp line for every hit for every iteration
  for (core::index2 i = 0; i < iterations.size(); i++) {
    for (core::index2 j = 0; j < iterations[i].size(); j++) std::cout << iterations[i][j] << "\n";
  }

  return 0;
}
