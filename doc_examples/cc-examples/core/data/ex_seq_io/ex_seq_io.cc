#include <iostream>
#include <core/data/io/fasta_io.hh>
#include <core/data/io/seq_io.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which reads a SEQ file and prints it’s content in FASTA format.

USAGE:
    ./ex_seq_io SEQ-file
EXAMPLE:
    ./ex_seq_io 2gb1.seq

)";

/** @brief Example reads SEQ file and prints the data stored there in FASTA format
 *
 * CATEGORIES: core/data/io/read_seq
 * KEYWORDS:   sequence; FASTA output; secondary structure; Format conversion
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  core::data::sequence::SecondaryStructure_SP ss = core::data::io::read_seq(argv[1],"");
  ss->header(argv[1]);
  std::cout << core::data::io::create_fasta_string(*ss, 80)<<"\n";
  std::cout << core::data::io::create_fasta_secondary_string(*ss, 80)<<"\n";
}
