#include <iostream>
#include <iomanip>
#include <core/data/io/Pdb.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Simple example reads a PDB file and checks if all amino acid residues have complete backbone.
EXAMPLE:
    ex_Residue 5edw.pdb

)";

/** @brief Reads a PDB file and checks if all amino acid residues have complete backbone
 *
 * CATEGORIES: core::data::structural::Structure; core::data::structural::Residue
 * KEYWORDS:   PDB input; pre-processing
 */
int main(const int argc, const char* argv[]) {

    if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

    core::data::io::Pdb reader(argv[1]); // file name (PDB format, may be gzip-ped)
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    
    // Iterate over all residues in the structure
    bool is_OK = true;
    for (auto it_resid = strctr->first_residue(); it_resid!=strctr->last_residue(); ++it_resid) {
      const core::chemical::Monomer & m = (*it_resid)->residue_type();
      core::data::structural::PdbAtom_SP atom_sp;
      if(m.type=='P') {
        atom_sp = (*it_resid)->find_atom(" N  ");
        if(atom_sp==nullptr) { std::cout << "Missing backbone atom N \n"; is_OK = false; }
        atom_sp = (*it_resid)->find_atom(" CA ");
        if(atom_sp==nullptr) { std::cout << "Missing backbone atom CA \n"; is_OK = false; }
        atom_sp = (*it_resid)->find_atom(" C  ");
        if(atom_sp==nullptr) { std::cout << "Missing backbone atom C \n"; is_OK = false; }
        atom_sp = (*it_resid)->find_atom(" O  ");
        if(atom_sp==nullptr) { std::cout << "Missing backbone atom O \n"; is_OK = false; }
      }
    }
    if(is_OK) std::cout << "Backbone complete!\n";
}
