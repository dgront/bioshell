#include <core/data/io/Pdb.hh>
#include <core/calc/structural/angles.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/selectors/SelectPlanarCAGeometry.hh>

#include <utils/exit.hh>
#include <utils/io_utils.hh>

std::string program_info = R"(

Reads a PDB file and tests whether geometry at CA atom is tetrahedral or not.
The program also prints the actual values of the N-CA-C-CB dihedral angle.

USAGE:
    ./ex_SelectPlanarCAGeometry input.pdb

EXAMPLE:
    ./ex_SelectPlanarCAGeometry 5edw.pdb

OUTPUT (fragment):
 112 CYS D  OK     -2.22 3dcg
 140 ASN E WRONG   -2.42 3dcg
 141 LYS E  OK     -2.23 3dcg
 142 VAL E  OK     -2.17 3dcg
 144 SER E  OK     -2.16 3dcg
 145 LEU E  OK     -2.19 3dcg

)";

/** @brief Tests whether alpha-carbons actually have tetrahedral geometry as they should.
 *
 * CATEGORIES: core::data::structural::ResidueHasBBCB; core::data::structural::SelectResidueByName; core::data::structural::SelectPlanarCAGeometry
 * KEYWORDS:   residue geometry; structure selectors; PDB input; structure validation
 * IMG: 1NXB_A_56_Glu_pymol_5.png
 * IMG_ALT: GLU56 of 1NXB deposit has planar geometry of alpha-carbon (witch contradicts basic chemical knowledge)
 */
int main(const int argc, const char *argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  using namespace core::data::io;
  using core::calc::structural::to_degrees;

  core::data::io::Pdb reader(argv[1], is_not_alternative); // file name (PDB format, may be gzip-ped)
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

  // --- Selector that returns true if a residue has beta-carbon
  core::data::structural::selectors::ResidueHasBBCB has_bb_cb;
  // --- Selector that test the geometry on alpha carbon
  core::data::structural::selectors::SelectPlanarCAGeometry tester;
  // --- Selector that selects GLY residues
  core::data::structural::selectors::SelectResidueByName is_gly("GLY");
  for (auto res_it = strctr->first_residue(); res_it != strctr->last_residue(); ++res_it) {
    // --- If a residue is not GLY and has C-beta ...
    if ((has_bb_cb(**res_it)) && (!is_gly(**res_it)))
      std::cout << utils::string_format("%4d %3s %4s %s %7.2f %s\n", (*res_it)->id(),
        (*res_it)->residue_type().code3.c_str(), (*res_it)->owner()->id().c_str(), (tester(**res_it)) ? "WRONG" : " OK  ",
        to_degrees(tester.evaluate_angle(**res_it)), utils::basename(strctr->code()).c_str());
  }
}
