#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/Structure.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which reads a PDB file and writes protein sequence(s) in FASTA format.
Unlike, ap_pdb_to_fasta_ss.cc application, this doesn't print secondary structure strings

USAGE:
    ./ex_pdb_to_fasta input.pdb
EXAMPLE:
    ./ex_pdb_to_fasta 5edw.pdb

)";

/** @brief Reads a PDB file and writes protein sequence(s) in FASTA format.
 *
 * This is a simplified version of ap_pdb_to_fasta_ss.cc application
 * USAGE:
 *     ex_pdb_to_fasta 5edw.pdb
 *
 * CATEGORIES: core::data::io::Pdb
 * KEYWORDS:   PDB input
 * GROUP:      File processing; Format conversion
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::data::io; // Pdb and create_fasta_string lives there

  Pdb reader(argv[1], is_not_alternative, only_ss_from_header, true);
  core::data::structural::Structure_SP strctr = (reader.create_structure(0));

  // Iterate over all chains
  for (int ic = 0; ic < strctr->count_chains(); ++ic)
    std::cout << "> " << strctr->code() << (*strctr)[ic]->id() << "\n" // --- e.g. prints "> 2gb1 A"
      << (*strctr)[ic]->create_sequence()->sequence << "\n";           // --- prints the sequence itself
}
