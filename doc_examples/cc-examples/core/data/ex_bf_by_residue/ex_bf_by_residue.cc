#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <utils/string_utils.hh>
#include <utils/exit.hh>

std::string program_info = R"(

ex_bf_by_residue reads a PDB file and prints per-residue statistics of B-factors. The output provides:
amino acid type (1-letter code), residue ID, and minimum, average and maximum b-factors for that residue

USAGE:
    ex_bf_by_residue input.pdb
EXAMPLE:
    ex_bf_by_residue 2gb1.pdb

)";


/** @brief Reads a PDB file and per-residue statistics of B-factors
 *
 * CATEGORIES: core::data::io::Pdb;
 * KEYWORDS:   PDB input; B-factors; structure selectors
 * IMG: Bfactor_plot.png
 * IMG_ALT: B-factors of 2GB1 PDB deposit
 */
int main(const int argc, const char *argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  core::data::io::Pdb reader(argv[1]); // file name (PDB format, may be gzip-ped)
  core::data::structural::Structure_SP strctr = reader.create_structure(0);
  core::data::structural::selectors::IsBB is_bb;
  core::data::structural::selectors::IsAA is_aa;
  for (auto res_it = strctr->first_residue(); res_it != strctr->last_residue(); ++res_it) {
    if (!is_aa(**res_it)) continue;
    double min = 9999.0, max = -999.0, avg = 0.0, n = 0.0;
    for (auto atom : **res_it) {
//      if (is_bb(*atom)) continue; // --- uncommment that line to compute statistics for side chain only
      double bf = atom->b_factor();
      if (min > bf) min = bf;
      if (max < bf) max = bf;
      avg += bf;
      n += 1.0;
    }
    std::cout << (*res_it)->residue_type().code1 << " " <<
    utils::string_format("%4d %5.2f %5.2f %5.2f\n", (*res_it)->id(), min, avg / n, max);
  }
}
