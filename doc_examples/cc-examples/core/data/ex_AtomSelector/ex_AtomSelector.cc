#include <iostream>
#include <iomanip>	// for std::setw()
#include <ios>		// for std::boolalpha
#include <sstream>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Simple example showing how to use atom selectors.

Each selector returns true or false. This example uses selector to check, if:
       - an atom is an alpha-carbon  (core::data::structural::IsCA)
       - an atom is a beta-carbon  (core::data::structural::IsCB)
       - an atom is in backbone  (core::data::structural::IsBB)
       - an atom is of the specified element  (core::data::structural::IsElement)
       - an atom is either beta-carbon or a backbone atom  (core::data::structural::IsBBCB)
       - an atom is of the specified name  (core::data::structural::IsNamedAtom)
       - an atom is neither beta-carbon nor a backbone atom
          (core::data::structural::InverseAtomSelector of core::data::structural::IsBBCB)

USAGE:
./ex_AtomSelector

)";

std::string thr = R"(ATOM    726  N   THR A  49      16.822  -5.118  -7.249  1.00  0.00           N
ATOM    727  CA  THR A  49      18.249  -4.825  -7.180  1.00  0.00           C  
ATOM    728  C   THR A  49      18.495  -3.354  -6.872  1.00  0.00           C  
ATOM    729  O   THR A  49      19.599  -2.845  -7.066  1.00  0.00           O  
ATOM    730  CB  THR A  49      18.965  -5.191  -8.493  1.00  0.00           C  
ATOM    731  OG1 THR A  49      18.016  -5.723  -9.426  1.00  0.00           O  
ATOM    732  CG2 THR A  49      20.053  -6.223  -8.238  1.00  0.00           C  
ATOM    733  H   THR A  49      16.231  -4.547  -7.836  1.00  0.00           H  
ATOM    734  HA  THR A  49      18.702  -5.391  -6.366  1.00  0.00           H  
ATOM    735  HB  THR A  49      19.411  -4.291  -8.916  1.00  0.00           H  
ATOM    736  HG1 THR A  49      17.144  -5.733  -9.024  1.00  0.00           H  
ATOM    737 1HG2 THR A  49      20.548  -6.468  -9.177  1.00  0.00           H  
ATOM    738 2HG2 THR A  49      20.782  -5.816  -7.538  1.00  0.00           H  
ATOM    739 3HG2 THR A  49      19.607  -7.123  -7.817  1.00  0.00           H  
)";

/** @brief Demonstrates how to use atom selectors.
 *
 * CATEGORIES: core/data/structural/structure_selectors.hh
 * KEYWORDS:   PDB input; structure selectors
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

    std::stringstream in(thr);		// Create an input stream that will provide data from a string
    core::data::io::Pdb reader(in,      // data stream
        core::data::io::keep_all);      // a predicate to read ALL the ATOM lines (hydrogens are excluded by default)
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    
    core::data::structural::selectors::IsCA ca_test;
    core::data::structural::selectors::IsCB cb_test;
    core::data::structural::selectors::IsBB bb_test;
    core::data::structural::selectors::IsElement is_H("H");
    core::data::structural::selectors::IsBBCB bb_cb_test;
    core::data::structural::selectors::InverseAtomSelector not_bb_cb(bb_cb_test);
    core::data::structural::selectors::IsNamedAtom is_og1(" OG1"); // note the padding for four characters!
    std::cout <<"atom  is_CA     is_CB     is_BB     is_H     is_OG1  is_bb_CB !is_bb_CB\n";
    for(auto ai = strctr->first_atom(); ai != strctr->last_atom(); ++ai)
      std::cout << (*ai)->atom_name()<<"  "
    	    << std::setw(5) << std::boolalpha << ca_test(**ai)<<"  "
    	    << std::setw(5) << std::boolalpha << cb_test(**ai)<<"  "
    	    << std::setw(5) << std::boolalpha << bb_test(**ai)<<"  "
    	    << std::setw(5) << std::boolalpha << is_H(**ai)<<"  "
    	    << std::setw(5) << std::boolalpha << is_og1(**ai)<<"  "
    	    << std::setw(5) << std::boolalpha << bb_cb_test(**ai)<<" "
    	    << std::setw(5) << std::boolalpha << not_bb_cb(**ai)<<"\n";
}
