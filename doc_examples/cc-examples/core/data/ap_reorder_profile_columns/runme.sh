#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

mkdir -p outputs_from_test

echo -e "Running: ./ap_reorder_profile_columns test_inputs/d1or4A_.asn1"
./ap_reorder_profile_columns test_inputs/d1or4A_.asn1 > d1or4A_reordered.txt

mv d1or4A_reordered.txt outputs_from_test
