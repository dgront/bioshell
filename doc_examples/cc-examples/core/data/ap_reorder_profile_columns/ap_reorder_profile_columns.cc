#include <iostream>

#include <core/chemical/Monomer.hh>
#include <core/data/sequence/SequenceProfile.hh>
#include <utils/exit.hh>
#include <utils/LogManager.hh>
#include <utils/io_utils.hh>

std::string program_info = R"(

Reads a sequence profile (ASN.1 file format) and shuffles profile's columns as requested.
Resulting profile is writen in a tabular text format. If the new column order is not specified, amino acids
will appear in the order: GAP VILMC HWFY KR QD NQST, i.e. small, aromatic, positive, negative and other-polar

USAGE:
    ./ap_reorder_profile_columns input.asn1 [column-order]
EXAMPLE:
    ./ap_reorder_profile_columns d1or4A_.asn1

)";

// small aromatic positive negative other-polar
const std::string nice_order = "GAP"  "VILMC"  "HWFY"  "KR"  "QD"  "NQST";

/** @brief Reads a sequence profile (ASN.1 file format) and shuffles profile's columns
 *
 * CATEGORIES: core::data::sequence::SequenceProfile
 * KEYWORDS:   output file; sequence profile
 * GROUP:      File processing
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io;
  using namespace core::data::sequence;
  utils::Logger logs("ap_reorder_profile_columns");
  std::string order_string = (argc>2) ? argv[2] : nice_order;
  logs << utils::LogLevel::INFO << "new aa order is: " << order_string << "\n";
  SequenceProfile_SP profile_in = core::data::sequence::read_ASN1_checkpoint(argv[1]);
  SequenceProfile_SP profile_out = profile_in->create_reordered(order_string);
  profile_out->write_table(std::cout);
}
