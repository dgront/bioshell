#include <iostream>
#include <sstream>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Simple example showing how to select a structural fragment based on residue IDs

USAGE:
./ex_SelectResidueRange

)";

/** @brief Shows how to select a structural fragment based on residue IDs
 *
 * CATEGORIES:  core::data::structural::SelectResidueRange
 * KEYWORDS:   structure selectors; PDB input; STL; algorithms
 */
// --- Only C-alpha atoms are listed here to keep this example short and simple
std::string fragment =
    R"(ATOM    312  CA  ALA A  -1     -10.035   4.811   1.920  1.00  0.24           C  
ATOM    322  CA  VAL A   0     -13.437   5.248   0.258  1.00  0.33           C  
ATOM    338  CA  ASP A   1     -12.201   3.975  -3.121  1.00  0.24           C  
ATOM    350  CA  ALA A   1A     -9.237   2.226  -4.777  1.00  0.18           C  
ATOM    360  CA  ALA A   1B     -7.956   5.461  -6.338  1.00  0.24           C  
ATOM    370  CA  THR A   2      -7.460   7.449  -3.135  1.00  0.21           C  
ATOM    384  CA  ALA A   3      -6.080   4.229  -1.648  1.00  0.12           C  
)"
;

int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::data::structural;

  std::stringstream in(fragment);		    // Create an input stream that will provide data from a string
  core::data::io::Pdb reader(in);
  Structure_SP strctr = reader.create_structure(0);

  std::cout << "IDs of the residues available for selection:";
  std::for_each(strctr->first_residue(), strctr->last_residue(), [](Residue_SP r) {std::cout << r->residue_id()<<" ";});
  std::cout << "\n";

  selectors::SelectResidueRange range0("-1-1");
  std::cout << "selector " << range0.selector_string() << " selects: "
      << std::count_if(strctr->first_residue(), strctr->last_residue(), range0) << " residues\n";

  selectors::SelectResidueRange range1("-1-1A");
  std::cout << "selector " << range1.selector_string() << " selects: "
      << std::count_if(strctr->first_residue(), strctr->last_residue(), range1) << " residues\n";

  selectors::SelectResidueRange range2("*");
  std::cout << "selector " << range2.selector_string() << " selects: "
      << std::count_if(strctr->first_residue(), strctr->last_residue(), range2) << " residues\n";
}
