#include <iostream>
#include <core/data/io/ss2_io.hh>
#include <core/data/io/DsspData.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Reads a DSSP file and writes secondary structure in SS2 format.
To convert DSSP to FASTA format use ap_DsspData

EXAMPLE:
    ap_dssp_to_ss2 5edw.dssp

)";

/** @brief Reads a DSSP file and prints the secondary structure of each chain in SS2 format.
 *
 * @see ap_DsspData.cc converts DSSP to FASTA format
 * CATEGORIES:  core::data::io::DsspData
 * KEYWORDS:   DSSP; Structure; secondary structure; Format conversion
 * GROUP:      File processing; Format conversion
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  // --- read a DSSP file - the first command line argument of the program
  core::data::io::DsspData dssp(argv[1], true);
  for (const auto & ss2 : dssp.sequences())                // --- for each protein sequence found in the DSSP data ...
    core::data::io::write_ss2(*ss2,std::cout);          // --- print it as SS2!
}
