#include <iostream>

#include <core/data/io/pir_io.hh>
#include <core/data/io/fasta_io.hh>

#include <utils/exit.hh>

std::string program_info = R"(

Reads a file with sequences in PIR format and converts them to FASTA.

USAGE:
    ap_pir_to_fasta example.pir

REFERENCE:
https://salilab.org/modeller/9v8/manual/node454.html

)";

/** @brief  Reads a file with sequences in PIR format and converts them to FASTA.
 *
 * CATEGORIES: core/data/io/pir_io;
 * KEYWORDS:   PIR; FASTA output
 * GROUP:      File processing;Format conversion
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using core::data::sequence::Sequence_SP; // --- Sequence_SP is just a std::shared_ptr to core::data::sequence::Sequence type
  using namespace core::data::io;          // --- This is required so PirEntry can be printed with << operator

  // --- Create a container where the sequences will be stored
  std::vector<Sequence_SP> sequences;

  // --- Read a file with PIR sequences
  core::data::io::read_pir_file(argv[1], sequences);

  // --- Write them in FASTA
  for (const Sequence_SP s : sequences)
    std::cout << core::data::io::create_fasta_string(*s) << "\n";

  // --- The sequence data is actually in FASTA format; just upper-casted to Sequence_SP
  // --- Here we down-cast it back to the derived type
  std::cout << "The source PIR data was:\n";
  for (const Sequence_SP s : sequences)
    std::cout << *std::dynamic_pointer_cast<core::data::sequence::PirEntry>(s) << "\n";
}
