#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/data/io/fasta_io.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Reads a PDB file and prints the sequences stored in its SEQRES fields
These sequences in many cases differ from the sequences extracted from coordinates section

EXAMPLE:
    ./ex_Seqres 2kwi.pdb

)";

/** @brief Reads a PDB file and extracts its SEQRES sequence(s)
 * CATEGORIES: core::data::io::Pdb
 * KEYWORDS:   PDB input; Structure; sequence 
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io;

  Pdb reader(argv[1], // file name (PDB format, may be gzip-ped)
      is_ca,          // read only CA atoms
      keep_all, true);          //  parse PDB header !

  std::shared_ptr<Seqres> seq_res = std::static_pointer_cast<Seqres>(reader.header.find("SEQRES")->second);
  for(const auto & chain_seq : seq_res->sequences) {
    const std::string header = reader.pdb_code()+" : "+chain_seq.first;
    core::data::sequence::Sequence_SP s = seq_res->create_sequence(chain_seq.first,header);
    if((s->length()>20) && (s->get_monomer(2).type=='P'))
      std::cout << core::data::io::create_fasta_string(*s,-1)<<"\n";
  }
}
