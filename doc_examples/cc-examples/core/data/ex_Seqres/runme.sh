#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ex_Seqres test_inputs/2kwi.pdb"
./ex_Seqres test_inputs/2kwi.pdb
echo -e "Running ./ex_Seqres test_inputs/4mcb.pdb"
./ex_Seqres test_inputs/4mcb.pdb
