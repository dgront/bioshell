#include <iostream>
#include <string>

#include <core/data/basic/ThreadSafeMap.hh>

/** @brief Shows how to use ThreadSafeMap class
 *
 * CATEGORIES: core::data::basic::ThreadSafeMap
 * KEYWORDS:   data container
 */
int main(const int argc, const char* argv[]) {

  core::data::basic::ThreadSafeMap<std::string,int> map;
  int one = 1, two = 2;
  map.insert_or_assign("one",one);
  map.insert_or_assign("two",two);
}
