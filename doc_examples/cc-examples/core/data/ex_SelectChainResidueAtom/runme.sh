#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ex_SelectChainResidueAtom test_inputs/1ofz.pdb A:aa"
# Here we select all amino acid residues of chain A, water molecules, ligands etc. are removed
./ex_SelectChainResidueAtom test_inputs/1ofz.pdb A:aa

echo -e "Running ./ex_SelectChainResidueAtom test_inputs/2gb1.pdb A:1-20:_CA_+_N__+_O__+_C__"
# Here we select backbone atoms from the first 20 residues of 2gb1
./ex_SelectChainResidueAtom test_inputs/2gb1.pdb A:1-20:_CA_+_N__+_O__+_C__

echo -e "Running ./ex_SelectChainResidueAtom test_inputs/1ofz.pdb *:*:_CA_"
# Select all alpha carbons
./ex_SelectChainResidueAtom test_inputs/1ofz.pdb *:*:_CA_
