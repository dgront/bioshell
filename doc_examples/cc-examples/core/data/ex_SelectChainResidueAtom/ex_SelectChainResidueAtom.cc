#include <iostream>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Extracts a fragment of a PDB file by applying a SelectChainResidueAtom selector.

The selection string constists of chain code and residue range, separated by a colon, e.g.:
    - A:-1-10
    - AB:

USAGE:
    ex_SelectChainResidueAtom input.pdb selector-string
EXAMPLEs:
    ex_SelectChainResidueAtom 2gb1.pdb A:23-32
    ex_SelectChainResidueAtom 1ofz.pdb A:aa
    ex_SelectChainResidueAtom 2gb1.pdb A:1-20:_CA_+_N__+_O__+_C__
    ex_SelectChainResidueAtom 1ofz.pdb *:*:_CA_

)";

/** @brief Extracts a fragment of a PDB file.
 *
 * CATEGORIES: core::data::structural::StructureSelector
 * KEYWORDS:   structure selectors; PDB input; PDB output
 * IMG: ex_SelectChainResidueAtoms_1.png
 * IMG_ALT: Proline residue selected from 1OFZ deposit
 */
int main(const int argc, const char* argv[]) {

  using namespace core::data::structural;

  if(argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  // ---------- Read a PDB file and create a Structure object
  core::data::io::Pdb reader(argv[1],   // --- data file
      core::data::io::keep_all);        // --- a predicate to read ALL the ATOM lines (by default hydrogens are excluded)
  Structure_SP strctr = reader.create_structure(0);

  // --- Create a selector object from a selector string
  selectors::SelectChainResidueAtom sel(argv[2]);
  Structure_SP full_copy = strctr->clone(sel); // --- cloning with this selector makes a deep copy of everything
  for(auto atom_it=full_copy->first_atom();atom_it!=full_copy->last_atom();++atom_it)
    std::cout << (*atom_it)->to_pdb_line() << "\n";
}
