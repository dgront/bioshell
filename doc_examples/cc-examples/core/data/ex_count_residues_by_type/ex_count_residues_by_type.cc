#include <iostream>

#include <core/data/io/clustalw_io.hh>
#include <core/data/sequence/sequence_utils.hh>
#include <utils/exit.hh>
#include <utils/io_utils.hh>

std::string program_info = R"(

Reads a Multiple Sequence Alignment (MSA) in ClustalW format and counts residues by its type.

EXAMPLE:
    ./ex_count_residues_by_type cyped.CYP109.aln

)";

/** @brief Reads a MSA in ClustalW format  and prints by-residue counts
 *
 * CATEGORIES: core::data::sequence::sequence_utils
 * KEYWORDS:   clustal input; MSA
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io;
  using namespace core::data::sequence;

  std::vector<Sequence_SP> msa;   // --- Sequence_SP is just a shorter name for std::shared_ptr<Sequence>
  core::data::io::read_clustalw_file(argv[1],msa);

  std::map<core::chemical::Monomer,core::index4> counts = core::data::sequence::count_residues_by_type(msa);
  for (const auto &key_val : counts) std::cout << key_val.first.code3 << " " << key_val.second << "\n";
}
