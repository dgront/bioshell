#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/data/io/fasta_io.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which reads a PDB file and prints a requested sequence fragment.
USAGE:
    ./ex_Sequence input.pdb chain from to
EXAMPLE
    ./ex_Sequence 3wn7.pdb A 366 405

)";

/** @brief Reads a PDB file and prints a fragment of its sequence.
 *
 * CATEGORIES: core/data/io/Sequence
 * KEYWORDS:   PDB input; sequence
 */
int main(const int argc, const char* argv[]) {

  if(argc < 4) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
    
  using namespace core::data::io;

  // --- Try this test program with 3wn7 as the input structure !
  Pdb reader(argv[1], // file name (PDB format, may be gzip-ped)
    all_true(is_not_hydrogen,is_not_water),          // don't read hydrogens, skip water molecules
      core::data::io::keep_all, true);          //  parse PDB header !

  core::index2 from = atoi(argv[3]);
  core::index2 to = atoi(argv[4]);
  auto structure = reader.create_structure(0);
  auto chain = structure->get_chain(argv[2][0]);
  auto sequence = chain->create_sequence();

  // --- Should be 324 (324 is the ID of the very first residue of 3wn7 chain A)
  std::cout << "Id of the first residue: " << sequence->first_pos() << "\n";
  Sequence fragment(*sequence, from, to); // Cut from residue whose ID is 366
  std::cout << fragment.first_pos() << " " << fragment.sequence << "\n";
}
