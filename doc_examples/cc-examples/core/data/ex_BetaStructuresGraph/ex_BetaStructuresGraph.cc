#include <core/data/io/Pdb.hh>
#include <utils/exit.hh>
#include <core/algorithms/graph_algorithms.hh>
#include <core/data/structural/BetaStructuresGraph.hh>
#include <core/calc/structural/ProteinArchitecture.hh>

std::string program_info = R"(

Reads a PDB file, creates a BetaStructuresGraph for it and finds all strands as connected components of that graph
USAGE:
    ex_BetaStructuresGraph 5edw.pdb

)";

/** @brief Creates a BetaStructuresGraph and finds all strands
 *
 * CATEGORIES: core::data::structural::BetaStructuresGraph
 * KEYWORDS:   PDB input
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::structural;
  using namespace core::data::io;

  core::data::io::Pdb reader(argv[1],is_not_alternative,only_ss_from_header, true);
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

  core::calc::structural::ProteinArchitecture a(*strctr);
  BetaStructuresGraph_SP g = a.create_strand_graph();
  auto sheets = core::algorithms::connected_components<BetaStructuresGraph, Strand_SP, StrandPairing_SP>(*g);
  int cnt = 0;
  for(const auto & sheet: sheets) {
    std::cout << utils::string_format("-------------- Sheet %d -------------------\n",++cnt);
    for(auto it=sheet->cbegin_strand();it!=sheet->cend_strand();++it)
      std::cout << **it<<"\n";
  }
}
