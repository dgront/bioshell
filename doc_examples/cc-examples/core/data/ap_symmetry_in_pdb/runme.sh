#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ap_symmetry_in_pdb test_inputs/5edw.pdb"
./ap_symmetry_in_pdb test_inputs/5edw.pdb
echo -e "Running ./ap_symmetry_in_pdb test_inputs/3dcg.pdb"
./ap_symmetry_in_pdb test_inputs/3dcg.pdb
