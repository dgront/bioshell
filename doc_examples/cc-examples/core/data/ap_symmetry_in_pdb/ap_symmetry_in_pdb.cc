#include <iostream>
#include <core/data/io/Pdb.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Example shows how to access symmetry operators stored in a PDB file header. For every operation found,
it creates a Rototranslation object and prints it on a screen

USAGE:
    c input.pdb
EXAMPLE:
    ex_Remark290 5edw.pdb

)";

/** @brief ex_Remark290 demo shows how to access symmetry operators stored in a PDB file header.
 *
 * CATEGORIES: core/data/io/Pdb;
 * KEYWORDS:   PDB input; PDB line filter; Structure
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  for (int i = 1; i < argc; ++i) {
    core::data::io::Pdb reader(argv[i], // file name (PDB format, may be gzip-ped)
      core::data::io::is_bb,            // a predicate to read only the ATOM lines corresponding to backbone atoms
      core::data::io::keep_all,         // keep all header lines
      true);                            // parse PDB header

    std::shared_ptr<core::data::io::Remark290> r290 = reader.symmetry_operators();
      std::shared_ptr<core::data::io::Remark350> r350 = reader.biomolecule_symmetry();

    std::cout << "# Symmetry operators found: " << r290->count_operators() << "\n";
    for (const auto &rt: *r290) {
      std::cout << rt << "\n";
    }
    std::cout << "# Biological symmetry biomolecules found: " << r350->count_biomolecules() << "\n";
      for (const auto &sym: *r350) {
          std::cout << "For chains: ";
          for (auto c: sym.first) std::cout<< c<<" ";
          std::cout << "with size "<< sym.second.size()<<"\n";
          for (auto rt: sym.second) std::cout<< rt<<" ";
          std::cout <<"\n";
      }

  }

}
