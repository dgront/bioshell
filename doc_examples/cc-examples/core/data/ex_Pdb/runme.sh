#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ex_Pdb test_inputs/2gb1.pdb"
./ex_Pdb test_inputs/2gb1.pdb
echo -e "Running ./ex_Pdb test_inputs/5edw.pdb"
./ex_Pdb test_inputs/5edw.pdb
echo -e "Running ./ex_Pdb test_inputs/3dcg.pdb"
./ex_Pdb test_inputs/3dcg.pdb
