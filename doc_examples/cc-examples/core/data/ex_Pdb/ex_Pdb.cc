#include <iostream>
#include <core/data/io/Pdb.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to read a PDB file and create a Structure object.

The program reads a given file with a PDB line filter that passes only backbone atoms;
prints experimental method, resolution, R-value and R-free about the input file.

USAGE:
    ex_Pdb 5edw.pdb

)";

/** @brief Reads a PDB file and creates a Structure object.
 *
 * Input PDB data is filtered so only protein backbone atoms are loaded
 *
 * CATEGORIES: core::data::io::Pdb;
 * KEYWORDS:   PDB input; PDB line filter; Structure
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  for (int i = 1; i < argc; ++i) {
    core::data::io::Pdb reader(argv[i], // file name (PDB format, may be gzip-ped)
      core::data::io::is_bb,            // a predicate to read only the ATOM lines corresponding to backbone atoms
        core::data::io::only_ss_from_header, true);                            // parse PDB header
    core::data::structural::Structure_SP backbone = reader.create_structure(0);
    std::cout << "protein " << reader.pdb_code() << " has " << backbone->count_chains()
              << " chain(s), " << backbone->count_residues()
              << " residues and " << backbone->count_atoms() << " backbone atoms\n";
    std::cout << "title          : " << backbone->title() << "\n";
    std::cout << "compound       : " << backbone->compound() << "\n";
    std::cout << "classification : " << backbone->classification() << "\n";
    std::cout << "deposited      : " << backbone->deposition_date() << "\n";
    std::cout << "Is XRAY?       : " << ((backbone->is_xray()) ? "YES\n" : "No\n");
    std::cout << "Is NMR?        : " << ((backbone->is_nmr()) ? "YES\n" : "No\n");
    std::cout << "Is EM?         : " << ((backbone->is_em()) ? "YES\n" : "No\n");
    std::cout << "resolution     : " << backbone->resolution() << "\n";
    std::cout << "R-value        : " << backbone->r_value() << "\n";
    std::cout << "R-free         : " << backbone->r_free() << "\n";
    if(backbone->keywords().size()>0)
      std::cout << "keywords  : " << backbone->keywords()[0];
    for (auto it = ++backbone->keywords().cbegin(); it != backbone->keywords().cend(); ++it)
      std::cout << ", "<< *it;
    std::cout << "\n";
  }
}
