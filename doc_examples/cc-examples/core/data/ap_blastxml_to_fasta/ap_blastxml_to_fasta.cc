#include <iostream>
#include <fstream>

#include <stdexcept>
#include <core/data/io/BlastXMLReader.hh>
#include <core/algorithms/trees/algorithms.hh>
#include <core/index.hh>
#include <core/data/io/XML.hh>

#include <core/data/io/XMLElement.hh>
#include <core/data/io/Hsp.hh>
#include <utils/exit.hh>

std::string program_info = R"(

ap_blastxml_to_fasta reads a XML file produced by PsiBlast and extracts sequences of all hits.

The list of hits is divided into sections, according to the psiblast iteration when a given subject
sequence was detected. The sequences are written on the screen in FASTA format

USAGE:
    ap_blastxml_to_fasta blastout.xml
EXAMPLE:
    ap_blastxml_to_fasta "1K25_01+PBP_C2.psi"

)";

using namespace core::data::io;

struct BlastXMLVisitor {

  void operator()(std::shared_ptr<core::algorithms::trees::TreeNode<XMLElementData>> n) {

    if (n->element.name() == "Hsp") {
      auto xmlel = std::static_pointer_cast<XMLElement>(n);
      auto xmlel_root = std::static_pointer_cast<XMLElement>(n->get_root()->get_root());

      const std::string &sequence = xmlel->find_value("Hsp_hseq");
      const std::string &seq_name = xmlel_root->find_value("Hit_accession");
      std::cout << "> " << seq_name << "\n" << sequence << "\n";
    }
  }
};

/** @brief Reads XML produced by psiblast and creates FASTA file containing all hits
 *
 * CATEGORIES: core::data::io::XML; core::algorithms::trees::TreeNode
 * KEYWORDS:   XML; data structures
 * GROUP:      File processing;Format conversion
*/ 
int main(int argc, char *argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  XML xxx;
  std::shared_ptr<XMLElement> root = xxx.load_data(argv[1]);

  auto it = root->begin();
  while ((*it)->element.name() != "BlastOutput_iterations") ++it; // --- Visit branches until you find BlastOutput_iterations

  core::index2 iteration_counter = 0;
  for (const auto &v : **it) {
    if (v->element.name() == "Iteration") {
      std::cout << "\n# ------ iteration " << ++iteration_counter << " --------\n";
      core::algorithms::trees::depth_first_preorder((*it)->get_right(), BlastXMLVisitor());
    }
  }

  return 0;
}
