#include <iostream>
#include <iomanip>
#include <core/data/io/Pdb.hh>
#include <core/chemical/Molecule.hh>
#include <core/chemical/molecule_utils.hh>
#include <core/data/structural/PdbAtom.hh>

#include <utils/exit.hh>

std::string program_info = R"(

Example that shows how to iterate through structural components

USAGE:
    ex_structure_iterators 1dt7.pdb

where 1dt7.pdb id an input file (PDB format)

)";

/** @brief Shows how to iterate through structural components (residues, atoms, etc)
 *
 * CATEGORIES: core/data/structural/Structure
 * KEYWORDS: PDB input; Structure; Chain; Residue; PdbAtom; STL
 */
int main(const int argc, const char* argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::structural;
  core::data::io::Pdb reader(argv[1],core::data::io::is_not_alternative); // file name (PDB format, may be gzip-ped)
  Structure_SP strctr = reader.create_structure(0);

  // ------- Directly iterate over atoms of a structure, jump over chains, residues, etc.
  // ------- atom_it is an iterator, which points to a shared pointer to an atom
  int n_atoms_1 = 0;
  for (auto atom_it = strctr->first_atom(); atom_it != strctr->last_atom(); ++atom_it) ++n_atoms_1;

  // ------- Iterate over chains, residues, atoms
  int n_atoms_2 = 0;
  int n_chains = 0, n_residues = 0;
  for(auto chain_sp: *strctr) { // --- chain_sp is already a shared pointer to a chain
    ++n_chains;
    for(auto residue_sp: *chain_sp) { // --- residue_sp is already a shared pointer to a residue
      ++n_residues;
      for(auto atom_sp: *residue_sp)  // --- atom_sp is already a shared pointer to an atom
        ++n_atoms_2;
    }
  }

  int n_residues_2 = 0;
  // ------- Iterate over residues of a structure, jump over chains
  // ------- iter_res_i is an iterator, which points to a shared pointer to a residue
  for (auto iter_res_i = strctr->first_residue(); iter_res_i != strctr->last_residue(); ++iter_res_i)
    ++n_residues_2;

  std::cout << "These three atom counts should be equal: " << n_atoms_1 << ", " << n_atoms_2 << " and "
            << strctr->count_atoms() << "\n";
  std::cout << "These three residue counts should be equal: " << n_residues << ", " << n_residues_2 << " and "
            << strctr->count_residues() << "\n";
  std::cout << "These two chain counts should be equal: " << n_chains << " and " << strctr->count_chains() << "\n";
}
