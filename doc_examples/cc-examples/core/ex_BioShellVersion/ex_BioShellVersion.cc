#include <iostream>
#include <core/BioShellVersion.hh>
#include <utils/exit.hh>
#include <utils/options/OptionParser.hh>

std::string program_info = R"(

Unit test for BioShellVersion class which prints the BioShell version info - a string that unambiguously
describes code version (Git SHA and branch) and compilation time (Git timestamp).

Note, that the output changes with every git / cmake operation

USAGE:
./ex_BioShellVersion

)";

/** @brief Test for BioShellVersion class prints the BioShell version info
 *
 * CATEGORIES: core/BioShellVersion
 * KEYWORDS:   bioshell
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  std::cout << "BioShell boilerplate:\n";
  std::cout << core::BioShellVersion() << "\n";
}
