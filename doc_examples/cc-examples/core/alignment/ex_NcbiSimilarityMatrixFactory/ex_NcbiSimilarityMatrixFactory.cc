#include <iostream>

#include <core/alignment/scoring/SimilarityMatrix.hh>
#include <core/alignment/scoring/NcbiSimilarityMatrixFactory.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Test for loading substitution matrices available in BioShell.

The program eads a substitution matrix from a given file (NCBI file format) and prints
it back on the screen. The program can either load the input matrix from Biohell database
(data/alignments directory) or from a file specified by a user. 
One can manually install custom matrices just by copying them to data/alignments/

USAGE:
./ex_NcbiSimilarityMatrixFactory subst-matrix-name

EXAMPLES:
./ex_NcbiSimilarityMatrixFactory BLOSUM45
./ex_NcbiSimilarityMatrixFactory ./BLOSUM45.txt

)";


/** @brief Test for loading substitution matrices available in BioShell
 *
 * CATEGORIES: core::alignment::scoring::NcbiSimilarityMatrixFactory
 * KEYWORDS:   sequence alignment; substitution matrix
 * IMG: heatmap_1.png
 * IMG_ALT: BLOSUM62 matrix plotted
 */
int main(const int argc, const char *argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::alignment::scoring;

  NcbiSimilarityMatrixFactory sim_factory = NcbiSimilarityMatrixFactory::get();

  if (argc == 1) {
    std::vector<std::string> names;
    sim_factory.get().matrix_names(names);
    std::cout << "\nMatrices defined in BioShell:\n";
    for (const auto &n : names)
      std::cout << "\t" << n;
    std::cout << "\n";
  } else {
    NcbiSimilarityMatrix_SP m = sim_factory.load_matrix(argv[1]);
    std::stringstream out;
    out << "\n";
    m->print("%4d", 4, out);
    std::cout << out.str() << "\n";
  }
}
