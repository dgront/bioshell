#include <iostream>
#include <iterator>

#include <core/alignment/PairwiseAlignment.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Simple example showing how to work with PairwiseAlignment data structure, e.g. how to
retrieve arbitrary data according to a sequence alignment object

USAGE:
./ex_PairwiseAlignment

)";

/** @brief Simple example showing how to retrieve arbitrary data according to a sequence alignment object
 *
 * CATEGORIES: core::alignment::PairwiseAlignment;
 * KEYWORDS:   sequence alignment
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  // ---------- The two sequences that are already globally aligned, therefore indexes in both sequences start from 0
  core::alignment::PairwiseAlignment ali("FTFTALILL-AVAV", 0, "--FTAL-LLAAV--", 0);

  // ---------- query "objects" : that may be C-alpha atoms, residues, etc; here just chars
  std::vector<char> query_chars = {'F', 'T', 'F', 'T', 'A', 'L', 'I', 'L', 'L', 'A', 'V', 'A', 'V'}; // --- all the characters of the query sequence
  std::vector<char> tmplt_chars = {'F', 'T', 'A', 'L', 'L', 'L', 'A', 'A', 'V'};        // --- all the characters of the template sequence

  // ---------- container for the expected result
  std::vector<char> query_chars_aligned;
  std::vector<char> tmplt_chars_aligned;

  // ---------- set up query "objects" in the order as they appear in the alignment; print result on the screen
  ali.get_aligned_query(query_chars, '-', query_chars_aligned);

  // ---------- show results (it should be identical as the original alignment
  std::copy(query_chars_aligned.begin(), query_chars_aligned.end(), std::ostream_iterator<char>(std::cout, ""));
  std::cout << "\n"; // ---------- Should print FTFTALILL-AVAV

  // ---------- Now we extract both query and template objects; only the mutually aligned positions (no gaps)
  query_chars_aligned.clear();
  ali.get_aligned_query_template(query_chars, tmplt_chars, query_chars_aligned, tmplt_chars_aligned);

  // ---------- show results
  std::copy(query_chars_aligned.begin(), query_chars_aligned.end(), std::ostream_iterator<char>(std::cout, ""));
  std::cout << "\n"; // ---------- Should print FTALLLAV
  std::copy(tmplt_chars_aligned.begin(), tmplt_chars_aligned.end(), std::ostream_iterator<char>(std::cout, ""));
  std::cout << "\n"; // ---------- Should also print FTALLLAV

  // ---------- ... and finally print the alignment as a path
  std::cout << ali.to_path() << "\n";
}
