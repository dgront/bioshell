#include <ctime>
#include <iostream>
#include <sstream>

#include <utils/Logger.hh>
#include <utils/io_utils.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>

#include <core/data/basic/Vec3.hh>
#include <core/data/io/Pdb.hh>
#include <core/alignment/scoring/LocalStructure7.hh>
#include <core/alignment/scoring/LocalStructure5.hh>
#include <core/alignment/scoring/LocalStructureMatch.hh>
#include <utils/exit.hh>

utils::Logger l("ap_LocalStructureMatch");

std::string program_info = R"(

Finds contiguous structural segments that are similar between two structures.

The program creates contiguous structural segments of 5 or 7 CA atoms based on C-alpha coordinates from file1 and file2
(PDB format). The segment size must be given as the first input parameter. Then it looks for segments
that are structurally similar by computing LocalStructureMatch distance between them. This value is defined as
a squared difference between local inter-atomic distances. A small value means local structural similarity between
respective segments. The last (optional) parameter is the maximum value of a LocalStructureMatch distance to be printed.

USAGE:
    ./ap_LocalStructureMatch (5 or 7) file1.pdb file2.pdb [max_distance]

EXAMPLE:
    ./ap_LocalStructureMatch 7 4rm4A.pdb 5ofqA.pdb 9.0

)";

/** @brief Finds contiguous structural segments that are similar between two structures
 *
 * CATEGORIES: core/alignment/scoring/LocalStructureMatch;
 * KEYWORDS:   PDB input; structure match
 * GROUP:      Alignments
 */
int main(const int argc, const char *argv[]) {

  if(argc < 4) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter
  int match_size = atoi(argv[1]);

  double max_print_distance = (argc==5) ? atof(argv[4]) : 100000.0;
  using namespace core::alignment::scoring; // --- for LocalStructure7, LocalStructure5 and LocalStructureMatch
  using namespace core::data::basic;  // --- for Coordinates_SP and Vec3
  Coordinates_SP xyz_q = std::make_shared<std::vector<Vec3>>();
  Coordinates_SP xyz_t = std::make_shared<std::vector<Vec3>>();
  core::data::io::Pdb::read_coordinates(argv[2], *xyz_q, true, core::data::io::is_ca);
  if (match_size == 7) {
    LocalStructure7 local_query(xyz_q);
    core::data::io::Pdb::read_coordinates(argv[3], *xyz_t, true, core::data::io::is_ca);
    LocalStructure7 local_tmplt(xyz_t);
    LocalStructureMatch<LocalStructure7, 8> lm(local_query, local_tmplt);
    lm.print(std::cout, max_print_distance);
  } else if (match_size == 5) {
    LocalStructure5 local_query(xyz_q);
    core::data::io::Pdb::read_coordinates(argv[3], *xyz_t, true, core::data::io::is_ca);
    LocalStructure5 local_tmplt(xyz_t);
    LocalStructureMatch<LocalStructure5, 8> lm(local_query, local_tmplt);
    lm.print(std::cout, max_print_distance);
  }
}
