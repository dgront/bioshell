#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

./strc -ip=test_inputs/4rm4.pdb -op=4rm4A.pdb -strc:renumber -select::chains=A
./strc -ip=test_inputs/5ofq.pdb -op=5ofqA.pdb -strc:renumber -select::chains=A
echo -e "# Running ./ap_LocalStructureMatch 7 4rm4A.pdb 5ofqA.pdb | sort -nk3 | head -10000"
./ap_LocalStructureMatch 7 4rm4A.pdb 5ofqA.pdb | sort -nk3 | head -10000

rm *A.pdb
