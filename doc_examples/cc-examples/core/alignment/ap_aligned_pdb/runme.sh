#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
echo "#Running ./ap_aligned_pdb 1uox_1uox_1.pir 1uox.pdb 1uox.pdb"
./ap_aligned_pdb test_inputs/1uox_1uox_1.pir test_inputs/1uox.pdb test_inputs/1uox.pdb
mkdir -p outputs_from_test
mv query.pdb tmplt.pdb outputs_from_test