

import matplotlib.pyplot as plt


def get_score(s):
    """Parsuje linijke z alignment score, zwraca liste."""
    tmp = s.split()[3:]  # usun '#' i napis, pozostaw reszte
    return tmp

def get_pv_avg_sdev(s):
    """Parsuje linijke z p-value, avg sdec, zwraca liste."""
    tmp = s.split()[5:]  # usun '#' i napis, pozostaw reszte
    return [i for i in tmp]


def ogarnij_inne_dane(s):
    if '# alignment score:' in s:
        score = get_score(s)
    elif '# normal p-value, avg, sdev:' in s:
        val = get_pv_avg_sdev(s)
#    elif '# same value calculated by a library function:' in s:
    return score, val


plik = open('stdout.out', 'r').readlines()
data = []
for i in plik:
   if not i.startswith('#'):
       data.append(int(i))
   else:
       if '# alignment score:' in i:
           score = get_score(i)
       elif '# normal p-value, avg, sdev:' in i:
           val = get_pv_avg_sdev(i)
#plt.hist(data)

plt.hist(data, bins=range(min(data), max(data) + 1, 2), facecolor='#124653', alpha=0.5)
plt.title('Sequence alignment score')
plt.xlabel("score value")
plt.ylabel("counts")
plt.plot( [score], [10], marker='o', markersize=10, color="#124653")
plt.tight_layout()
plt.savefig("histogram.png", dpi=200)

