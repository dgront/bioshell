#include <iostream>

#include <core/alignment/FilterByHighlyGappedColumns.hh>
#include <core/data/io/clustalw_io.hh>
#include <utils/exit.hh>
#include <utils/io_utils.hh>
#include <core/data/io/fasta_io.hh>


std::string program_info = R"(

Reads a Multiple Sequence Alignment (MSA) in ClustalW or FASTA format and removes these sequences who produce
highly gapped columns. This filter first identifies Highly Gapped Columns (HGCs) as those MSA columns that have
at most HG-fraction*N_SEQ letters and all remaining characters are gaps. Then each sequence that participates
in at least sum_gap gapped columns is removed

USAGE:
./ap_filter_msa msa-file HG-fraction sum_gap

EXAMPLE:
./ap_filter_msa cyped.CYP109.aln 0.01 10

where cyped.CYP109.aln is the name of input MSA file; 0.01 means that in gapped columns 99% of sequences have a gap
and 1% has a letter. Finally, 10 means that sequences that participate in at least 10 HGPs are removed from the
input MSA

)";

/** @brief Reads a MSA in ClustalW format and removes these sequences who produce
 * highly gapped columns
 *
 * CATEGORIES: core::alignment::MSAColumnConservation
 * KEYWORDS: clustal input; MSA; FASTA input
 * GROUP:      Alignments
 */
int main(const int argc, const char* argv[]) {

  if(argc < 4) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io;
  using namespace core::data::sequence;

  double f_nnc = atof(argv[2]);
  core::index2 sum_gap = atoi(argv[3]);

  std::vector<Sequence_SP> msa;   // --- Sequence_SP is just a shorter name for std::shared_ptr<Sequence>
  const std::pair<std::string, std::string> name_ext = utils::root_extension(argv[1]);
  if((name_ext.second=="fasta")||(name_ext.second=="FASTA")||(name_ext.second=="fast"))
    core::data::io::read_fasta_file(argv[1], msa, true);
  else
    core::data::io::read_clustalw_file(argv[1],msa);

  core::alignment::FilterByHighlyGappedColumns filter{msa};
  filter.f_non_gapped(f_nnc);
  core::index2 n_seq = msa.size();
  filter.run(sum_gap);
  std::cout << "# " << filter.msa().size() << " sequences remained, " << (n_seq - filter.msa().size()) << " removed\n";
  std::cout << "# " << filter.highly_gapped_positions().size() << " highly gapped positions found\n";
  for (const core::data::sequence::Sequence_SP &seq:filter.msa())
    std::cout << core::data::io::create_fasta_string(*seq);
  int i_seq = -1;
  for (core::index2 cnt:filter.highly_gapped_for_sequence())
    std::cout << "# " << (++i_seq) << " " << cnt << "\n";
}
