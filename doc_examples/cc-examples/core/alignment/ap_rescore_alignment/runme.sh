#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "# Running ./ap_rescore_alignment test_inputs/2azaA_2pcyA-ali.fasta BLOSUM62 -10 -2"
./ap_rescore_alignment test_inputs/2azaA_2pcyA-ali.fasta BLOSUM62 -10 -2

echo -e "# Running ./ap_rescore_alignment test_inputs/optimal_global_ali.fasta BLOSUM62 -10 -2"
./ap_rescore_alignment test_inputs/optimal_global_ali.fasta BLOSUM62 -10 -1
