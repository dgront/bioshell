#include <iostream>
#include <iomanip>

#include <core/chemical/AtomicElement.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to use AtomicElement class. It prints all the element names

USAGE:
./ex_AtomicElement

)";

/** @brief Example showing how to use AtomicElement class
 *
 * CATEGORIES: core::chemical::AtomicElement
 * KEYWORDS: chemical elements
 */
int main(int argc, char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::chemical;

  if (argc < 2)
    for (const std::pair<std::string, AtomicElement> &e : AtomicElement::elements_by_symbol)
      std::cout << e.second << std::endl;
  else
    for (int i = 1; i < argc; i++) std::cout << AtomicElement::by_symbol(argv[i]) << "\n";
}
