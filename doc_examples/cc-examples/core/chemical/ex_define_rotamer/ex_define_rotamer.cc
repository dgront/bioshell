#include <iostream>
#include <iomanip>
#include <core/data/io/Pdb.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/chemical/ChiAnglesDefinition.hh>
#include <utils/exit.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

std::string program_info = R"(

Prints rotamer type (M-P-T code) for each amino acid residue in the input PDB structure
USAGE:
    ex_define_rotamer input.pdb

EXAMPLE:
    ex_define_rotamer 5edw.pdb

OUTPUT (fragment):
 277 ASP 2   TP
 278 LYS 4 incomplete
 279 ARG 4 TTMT
 280 ILE 2   MM
 281 PRO 3  PMP
 282 LYS 4 MTMM
 283 ALA 0
 284 ILE 2   TT

)";

/** @brief Prints rotamer type (M-P-T code) for each amino acid residue in the input PDB structure
 *
 * CATEGORIES: core::chemical::ChiAnglesDefinition; core::data::structural::ResidueHasAllHeavyAtoms
 * KEYWORDS:   PDB input; structural properties; rotamers; STL
 */
int main(const int argc, const char *argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::io;
  using namespace core::data::structural;

  Pdb reader(argv[1]); // file name (PDB format, may be gzip-ped)
  Structure_SP strctr = reader.create_structure(0);
  selectors::ResidueHasAllHeavyAtoms has_full_sc;
  selectors::IsAA is_aa;
  // Iterate over all residues
  for (auto ires = strctr->first_residue(); ires != strctr->last_residue(); ++ires) {
    core::data::structural::Residue &res_sp = (**ires);
    if (!is_aa(res_sp)) continue;
    std::cout << std::setw(4) << res_sp.id() << " " << res_sp.residue_type().code3 << " "
              << core::chemical::ChiAnglesDefinition::count_chi_angles(res_sp.residue_type());
    if (has_full_sc(res_sp)) std::cout << std::setw(5) << core::calc::structural::define_rotamer(res_sp);
    else std::cout << " incomplete";
    std::cout << "\n";
  }
}
