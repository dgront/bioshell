#!/bin/bash


export BIOSHELL_DATA_DIR="../../../../../data"

mkdir -p outputs_from_test

echo -e "# Running ./ap_AAHydrophobicity test_inputs/2gb1.pdb"
./ap_AAHydrophobicity test_inputs/2gb1.pdb
mv out.pdb outputs_from_test/2gb1.out.pdb

echo "# Running ./ap_AAHydrophobicity test_inputs/4rm4.pdb test_inputs/CYP109B1.aln YJIB_BACSU"
./ap_AAHydrophobicity test_inputs/4rm4.pdb test_inputs/CYP109B1.aln YJIB_BACSU
#rm out.pdb
mv out.pdb outputs_from_test/4rm4.out.pdb
