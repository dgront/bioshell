#include <iostream>
#include <memory>

#include <core/index.hh>
#include <core/algorithms/graph_algorithms.hh>
#include <core/chemical/Molecule.hh>
#include <core/chemical/molecule_utils.hh>
#include <core/calc/structural/angles.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/chemical/PdbMolecule.hh>
#include <core/chemical/Bond.hh>

core::chemical::PdbMolecule_SP create_toluene_molecule() {

  using namespace core::chemical;
  using namespace core::data::structural;

  // --- Define atoms that we use to build a molecule
  PdbAtom_SP atoms[] = {std::make_shared<PdbAtom>(1, " C1 ", 0, 0, 0),
                        std::make_shared<PdbAtom>(2, " C2 ", 1.24, 0.72, 0),
                        std::make_shared<PdbAtom>(3, " C3 ", 1.24, 2.16, 0),
                        std::make_shared<PdbAtom>(4, " C4 ", 0, 2.88, 0),
                        std::make_shared<PdbAtom>(5, " C5 ", -1.24, 2.16, 0),
                        std::make_shared<PdbAtom>(6, " C6 ", -1.24, 0.72, 0),
                        std::make_shared<PdbAtom>(7, " C7 ", 0, -1.52, 0)};
  PdbMolecule_SP toluene = std::make_shared<PdbMolecule>();

  // --- Insert atoms into the molecule
  for (PdbAtom_SP ai : atoms) toluene->add_atom(ai);
  // --- Create bonds between them
  toluene->bind_atoms(0, 1, BondType::AROMATIC);
  toluene->bind_atoms(1, 2, BondType::AROMATIC);
  toluene->bind_atoms(2, 3, BondType::AROMATIC);
  toluene->bind_atoms(3, 4, BondType::AROMATIC);
  toluene->bind_atoms(4, 5, BondType::AROMATIC);
  toluene->bind_atoms(0, 5, BondType::AROMATIC);
  toluene->bind_atoms(0, 6, BondType::SINGLE);

  return toluene;
}

/** @brief Demonstrates how to create a Molecule object based on PdbAtom data type (as nodes of the graph)
 *
 * This demo is similar to ex_Molecule_vec3, the difference is that here PdbAtom instances are used as graph nodes
 * rather than Vec3 instance. It creates a toluene molecule and detects planar and dihedral angles.
 *
 * CATEGORIES: core::chemical::Molecule
 * KEYWORDS: molecule
 * IMG: Toluen_dihedral_flat_angle.png
 * IMG_ALT: Planar angles in a toluen molecule
 */
int main(const int argc, const char *argv[]) {

  using namespace core::chemical;
  using namespace core::data::structural;

  PdbMolecule_SP molecule;
  if (argc == 1)
    molecule = create_toluene_molecule();
  else {
    // --- Read structure that we use to build a molecule
    core::data::io::Pdb reader(argv[1]); // file name (PDB format, may be gzip-ped)
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    // --- Create molecule object
    molecule = structure_to_molecule(*strctr);
    //molecule = create_molecule<Structure::atom_iterator>(strctr->first_atom(), strctr->last_atom(), 0.1);
  }

  // --- Print some info about the molecule
  std::cout << molecule->count_atoms() << " atoms, " << molecule->count_bonds() << " bonds\n";
  for (auto atom_it=molecule->begin_atom();atom_it!=molecule->end_atom();++atom_it) {
    PdbAtom_SP ai = *atom_it;
    std::cout << "atom " << ai->id() << " bonded to " << molecule->count_bonds(ai) << " atoms:";
    for (auto n_it = molecule->begin_atom(ai); n_it != molecule->end_atom(ai); ++n_it)
      std::cout << " " << (*n_it)->id();
    std::cout << "\n";
  }

  // --- Find all planar angles in the molecule
  std::vector<std::tuple<PdbAtom_SP, PdbAtom_SP, PdbAtom_SP>> planars;
  find_planar_angles(*molecule, planars);
  // --- Sort the angles just to make the output stable i.e. every time in the same order so it can be used for benchmarking
  std::sort(planars.begin(), planars.end(), ComparePlanarAngles());

  std::cout << "Detected planar angles:\n"; // --- Evaluate and print all the planars
  for (auto pi : planars) {
    PdbAtom &a1 = *std::get<0>(pi);
    PdbAtom &a2 = *std::get<1>(pi);
    PdbAtom &a3 = *std::get<2>(pi);
    std::cout << a1.id() << " -- " << a2.id() << " -- " << a3.id() << " " <<
    core::calc::structural::evaluate_planar_angle(a1, a2, a3) * 180.0 / 3.14159 << "\n";
  }

  // --- Find all torsion angles in the molecule
  std::vector<std::tuple<PdbAtom_SP, PdbAtom_SP, PdbAtom_SP, PdbAtom_SP>> torsions;
  find_torsion_angles(*molecule, torsions);
  // --- Sort also dihedral angles
  std::sort(torsions.begin(), torsions.end(), CompareDihedralAngles());
  std::cout << "Detected dihedral angles:\n"; // --- Evaluate and print all the planars
  for (auto ti : torsions) {
    PdbAtom &a1 = *std::get<0>(ti);
    PdbAtom &a2 = *std::get<1>(ti);
    PdbAtom &a3 = *std::get<2>(ti);
    PdbAtom &a4 = *std::get<3>(ti);
    std::cout << a1.id() << " -- " << a2.id() << " -- " << a3.id() << " -- " << a4.id() << " " <<
    core::calc::structural::evaluate_dihedral_angle(a1, a2, a3, a4) * 180.0 / 3.14159 << "\n";
  }

  // --- Here we find the benzene ring in the molecule - a cycle in a graph
  std::vector<std::vector<core::index4>> cycles =
      core::algorithms::find_cycles<PdbMolecule, PdbAtom_SP, std::shared_ptr<BondType >>(*molecule);

  std::cout << "Atoms in a cycle:";
  for (core::index4 i:cycles[0]) std::cout << " " << molecule->get_atom(i)->atom_name();
  std::cout << "\n";
}
