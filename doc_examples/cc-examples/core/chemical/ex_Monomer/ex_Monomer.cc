#include <iostream>
#include <algorithm> // for std::count_if
#include <iterator>  // for std::distance

#include <utils/string_utils.hh>
#include <core/chemical/Monomer.hh>
#include <core/chemical/monomer_io.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which demonstrates  functionality of core::chemical::Monomer data type.

USAGE:
./ex_Monomer

)";

// First we declare a function object used to count how many monomers have type = 'P' i.e. "protein"    
bool IsAA(const core::chemical::Monomer &m) { return (m.type == 'P') || (core::chemical::Monomer::get(m.parent_id).type == 'P'); }

/** @brief Example demonstrates functionality of core::chemical::Monomer data type.
 *
 * CATEGORIES: core::chemical::Monomer
 * KEYWORDS:   monomers
 */
int main(const int argc, const char *argv[]) {

  using namespace core::chemical;
  // First we iterate over all monomers and count, how many of them are actually amino acids
  // See how bioshell's iterators work together with std library
  int n_aa = std::count_if(Monomer::cbegin(), Monomer::cend(), IsAA);

  std::cout << std::distance(Monomer::cbegin(), Monomer::cend()) << " standard monomers found, including "
            << n_aa << " peptide-forming.\n";

  std::cout << "The order of standard amino acid residues is:\n";
  for (core::index2 i = 0; i < n_aa; ++i) {
    const Monomer &m = Monomer::get(i);
    std::cout << utils::string_format("%2d %c %3s %c\n", i, m.code1, m.code3.c_str(), m.type);
  }

  load_monomers_from_db(); // --- load the database of all known monomers
  // Count amino acid monomers again
  n_aa = std::count_if(Monomer::cbegin(), Monomer::cend(), IsAA);
  std::cout << "Monomer database loaded; " << std::distance(Monomer::cbegin(), Monomer::cend())
            << " monomers found, including " << n_aa << " peptide-forming.\n";

  // Now let's count how many non-standard residues are derived from ALA
  // Simply a parent_id of a monomer must be equal to ALA.id
  // This time we use a lambda expression rather than a functor
  n_aa = std::count_if(Monomer::cbegin(), Monomer::cend(), [](const Monomer &m) { return m.parent_id == Monomer::ALA.id; });
  std::cout << "There are " << n_aa << " derived from alanine\n";
}
