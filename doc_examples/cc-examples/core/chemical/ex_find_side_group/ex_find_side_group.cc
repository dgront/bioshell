#include <iostream>
#include <iomanip>
#include <core/data/io/Pdb.hh>
#include <core/chemical/Molecule.hh>
#include <core/chemical/molecule_utils.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Reads a PDB file and prints names of all atoms in each residue side chain.
The find_side_group() function, tested by this program, creates a molecular graph to detect a side chain and returns
copies side chain atoms on a vector.

USAGE:
    ex_find_side_group 2gb1.pdb

)";

/** @brief A simple example shows how to select a chemical group of a molecule using find_side_group() method.
 *
 * This example prints atoms for each side chain in a protein
 * CATEGORIES: core/chemical/find_side_group;
 * KEYWORDS:   data_structures;graphs ; residue side chains
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::structural;
  core::data::io::Pdb reader(argv[1],core::data::io::is_not_alternative); // file name (PDB format, may be gzip-ped)
  Structure_SP strctr = reader.create_structure(0); // create a Structure object from the first deposit found in the input file

  // --- Here we create a molecule object; 0.1 is the tolerance for bond lengths (used to detect bonds)
  auto molecule_sp = core::chemical::create_molecule(strctr->first_atom(),strctr->last_atom(),0.1);

  // --- Iterate over all residues in the structure
  for(auto res_it = strctr->first_residue();res_it!=strctr->last_residue();++res_it) {
    auto ca = (*res_it)->find_atom(" CA "); // alpha carbon is the preceding atom
    auto cb = (*res_it)->find_atom(" CB "); // beta carbon is the atom where a side chain is attached
    if((ca== nullptr)||(cb== nullptr)) continue;
    std::vector<PdbAtom_SP> sc;
    core::chemical::find_side_group<PdbAtom_SP>(ca,cb,*molecule_sp,sc);
    std::cout << utils::string_format("%4d %s :",(*res_it)->id(),(*res_it)->residue_type().code3.c_str());
    for(const PdbAtom_SP & a : sc)
      std::cout << " " << a->atom_name();
    std::cout << "\n";
  }

}
