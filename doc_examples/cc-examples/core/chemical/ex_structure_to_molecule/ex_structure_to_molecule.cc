#include <iostream>
#include <memory>

#include <core/algorithms/graph_algorithms.hh>
#include <core/chemical/Molecule.hh>
#include <core/chemical/molecule_utils.hh>
#include <core/calc/structural/angles.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(


Unit test which creates a Molecule object from a given PDB file. As a test, the program lists all covalent bonds
between a given ligand and the rest of the protein.

USAGE:
    ./ex_structure_to_molecule input.pdb ligand-code

EXAMPLE:
    ./ex_structure_to_molecule 4rm4.pdb HEM

)";

/** @brief Creates a Molecule object from a given PDB file.
 *
 * As a test, the program lists all covalent bonds between a given ligand and the rest of the protein
 *
 * CATEGORIES: core::chemical::structure_to_molecule
 * KEYWORDS: molecule
 */
int main(const int argc, const char *argv[]) {

  if (argc <3) utils::exit_OK_with_message(program_info);

  using namespace core::chemical;
  using namespace core::data::structural;

  PdbMolecule_SP molecule;

  // --- Read structure that we use to build a molecule
  core::data::io::Pdb reader(argv[1]); // file name (PDB format, may be gzip-ped)
  core::data::structural::Structure_SP strctr = reader.create_structure(0);
  // --- Create molecule object
  molecule = structure_to_molecule(*strctr);

  // --- Find the ligand object(s) for a given 3-letter code
  selectors::SelectResidueByName ligand_by_name(argv[2]);
  std::vector<Residue_SP> ligand;
  strctr->find_residues(ligand_by_name, ligand);

  for (const auto &l:ligand) {      // --- iterate over ligands found
    std::cout << "Bonds between " << l->residue_type().code3 << " and the rest of the protein:\n";
    for (const auto &atom : *l) {   // --- iterate over atoms of a ligand, find all its bounded partners
      for (auto it = molecule->cbegin_atom(atom); it != molecule->cend_atom(atom); ++it) {
        if ((**it).owner() != l)    // --- if the two atoms belong to different residues - print the output
          std::cout << (*atom).atom_name() << " - " << (**it).atom_name() << " " << (**it).owner()->residue_type().code3
                    << " " << (**it).owner()->residue_id() << " " << (**it).owner()->owner()->id() << "\n";
      }
    }
  }
}
