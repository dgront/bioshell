#!/bin/bash


export BIOSHELL_DATA_DIR="../../../../../data"

mkdir -p outputs_from_test

echo -e "# Running ./ex_structure_to_molecule test_inputs/4rm4.pdb HEM"
./ex_structure_to_molecule test_inputs/4rm4.pdb HEM

echo -e "# Running ./ex_structure_to_molecule test_inputs/1lw5.pdb LLP"
./ex_structure_to_molecule test_inputs/1lw5.pdb LLP
