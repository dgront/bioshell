#!/bin/bash

echo -e "Running: ./ex_monomer_io -in::monomers::cif=./test_inputs/HEM.cif -out:file=monomers_new.txt"

./ex_monomer_io --in::monomers::cif=./test_inputs/HEM.cif -out:file=monomers_new.txt

mkdir -p outputs_from_test
mv monomers_new.txt outputs_from_test/
