#include <fstream>
#include <iostream>

#include <core/chemical/Monomer.hh>
#include <core/chemical/monomer_io.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/output_options.hh>
#include <utils/exit.hh>

using namespace core::chemical;


std::string program_info = R"(

The program converts a monomer structure from CIF format to internal formats used by BioShell.

Use it to register your own monomer which is missing in BioShell library. The program is also used to create
'monomers.txt' file from BioShell distribution (located in ./data/ directory). In order to do so, download
the fresh repository of monomers in CIF format from:

http://ligand-expo.rcsb.org/dictionaries/Components-pub.cif

and run the program. Then replace the released monomers.txt file with the new one

USAGE:
./ex_monomer_io -in::monomers::cif=HEM.cif -out:file=hem.txt
./ex_monomer_io -in::monomers::cif=Components-pub.cif

)";

/** @brief The program converts a monomer structure from CIF format to internal formats used by BioShell.
 *
 * CATEGORIES: core/chemical/Monomer; utils/options/OptionParser; utils/options/Option
 * KEYWORDS:   monomers; option parsing
 */
int main(const int argc, const char* argv[]) {

  using namespace utils::options;
  utils::options::OptionParser & cmd = OptionParser::get();
  cmd.register_option(utils::options::help);
  cmd.register_option(verbose, mute);
  cmd.register_option(db_path);
  cmd.register_option(input_bin_monomers, input_cif_monomers,input_txt_monomers);
  cmd.register_option(output_file);
  cmd.program_info(program_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  if (input_cif_monomers.was_used()) read_monomers_cif(option_value<std::string>(input_cif_monomers));

  if (input_txt_monomers.was_used()) read_monomers_txt(option_value<std::string>(input_txt_monomers));

  if (input_bin_monomers.was_used()) read_monomers_binary(option_value<std::string>(input_bin_monomers));

  write_monomers_txt(option_value<std::string>(output_file,"monomers.txt"));
}

