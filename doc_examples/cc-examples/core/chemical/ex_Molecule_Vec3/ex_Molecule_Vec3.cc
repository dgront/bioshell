#include <iostream>
#include <memory>

#include <core/chemical/Molecule.hh>
#include <core/chemical/molecule_utils.hh>
#include <core/calc/structural/angles.hh>
#include <core/data/basic/Vec3.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to create a Molecule object based on Vec3 data type
(Vec3 objects are nodes of the graph).

USAGE:
    ./ex_Molecule_Vec3

)";

/** @brief Demonstrates how to create a Molecule object based on Vec3 data type (Vec3 are nodes of the graph)
 *
 * This demo is similar to ex_Molecule, the difference is that here Vec3 instances are used as graph nodes
 * rather than PdbAtom instance. It creates a toluene molecule and detects planar angles.
 *
 * CATEGORIES: core::chemical::Molecule
 * KEYWORDS: molecule
 * IMG: Toluen_dihedral_flat_angle.png
 * IMG_ALT: Planar angles in a toluen molecule
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::chemical;
  using namespace core::data::structural;

  Molecule<Vec3> toluene;
  Vec3 atoms[] = {Vec3(0, 0, 0),
                  Vec3(1.24, 0.72, 0),
                  Vec3(1.24, 2.16, 0),
                  Vec3(0, 2.88, 0),
                  Vec3(-1.24, 2.16, 0),
                  Vec3(-1.24, 0.72, 0),
                  Vec3(0, -1.52, 0)};

  // --- Mark atom numbers to check if the molecule is correct
  for (core::index2 i = 0; i < 7; ++i) atoms[i].register_ = i;

  // --- Insert atoms into the molecule
  for (Vec3 & ai : atoms) toluene.add_atom(ai);
  // --- Create bonds between them
  toluene.bind_atoms(0, 1, BondType::AROMATIC);
  toluene.bind_atoms(1, 2, BondType::AROMATIC);
  toluene.bind_atoms(2, 3, BondType::AROMATIC);
  toluene.bind_atoms(3, 4, BondType::AROMATIC);
  toluene.bind_atoms(4, 5, BondType::AROMATIC);
  toluene.bind_atoms(0, 5, BondType::AROMATIC);
  toluene.bind_atoms(0, 6, BondType::SINGLE);

  std::cout << "Connectivity (bonds):\n";
  for(auto atom_it=toluene.cbegin_atom();atom_it!=toluene.cend_atom();++atom_it) {
    std::cout << (*atom_it).register_ << " : ";
    for(auto atom_it2=toluene.cbegin_atom(*atom_it);atom_it2!=toluene.cend_atom(*atom_it);++atom_it2)
      std::cout << " "<<(*atom_it2).register_;
    std::cout << "\n";
  }

  // --- Find all planar angles in the molecule
  std::vector<std::tuple<Vec3, Vec3, Vec3>> planars;
  find_planar_angles(toluene, planars);

  std::vector<double> planar_values;
  std::cout << "Detected planar angles:\n"; // --- Evaluate and print all the planars
  for (auto pi : planars) {
    Vec3 &a1 = std::get<0>(pi);
    Vec3 &a2 = std::get<1>(pi);
    Vec3 &a3 = std::get<2>(pi);
    planar_values.push_back(core::calc::structural::evaluate_planar_angle(a1, a2, a3) * 180.0 / 3.14159);
  }

  // --- Sort the values before printing them to make the output stable
  std::sort(planar_values.begin(),planar_values.end());
  for (double value:planar_values) std::cout << value << "\n";
}
