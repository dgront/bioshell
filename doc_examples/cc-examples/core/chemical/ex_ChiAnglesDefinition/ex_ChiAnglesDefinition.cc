#include <iostream>

#include <core/chemical/ChiAnglesDefinition.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test that shows how to look up information on Chi angle definitions. It prints how many Chi angles
are defined for ARG and which atoms define Chi2 of TRP

USAGE:
./ex_ChiAnglesDefinition

)";

/** @brief Shows how to look up information on Chi angle definitions
 *
 * This example prints how many Chi angles are defined for ARG and wish atoms define Chi2 of TRP
 *
 * CATEGORIES: core::chemical::ChiAnglesDefinition
 * KEYWORDS:   structural properties
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  // List atoms that define the second Chi angle in TRP residue
  std::cout << "Chi_2 in TRP:";
  for (const std::string &a : core::chemical::ChiAnglesDefinition::chi_angle_atoms("TRP", 2)) // "TRP" defines a residue, "2" stands for Chi_2
    std::cout << a << " ";
  std::cout << "\n";
  const core::chemical::Monomer &m = core::chemical::Monomer::ARG; // Create a local reference to ARG monomer (just to make the following lines shorter)
  std::cout << "\nAll Chi angles for in " << m.code3 << " :\n";
  for (unsigned short i = 1; i <= core::chemical::ChiAnglesDefinition::count_chi_angles(m); ++i) {  // Count how many Chi angles ARG has
    std::cout << "Chi" << i << " ";
    for (const std::string &a : core::chemical::ChiAnglesDefinition::chi_angle_atoms(m, i)) // List atoms for each of them
      std::cout << a << " ";
    std::cout << "\n";
  }
}
