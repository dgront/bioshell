#include <utils/Logger.hh>
#include <utils/LogManager.hh>
#include <core/chemical/MonomerStructure.hh>
#include <core/chemical/MonomerStructureFactory.hh>


#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to read CIF files.

USAGE:
    ex_MonomerStructure file.cif
EXAMPLE:
    ex_MonomerStructure AA3.cif

)";

/** @brief ex_MonomerStructure tests reading CIF files
 *
 * CATEGORIES: core/chemical/MonomerStructure
 * KEYWORDS:   CIF input
 */
int main(const int argc, const char *argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  utils::LogManager::FINE(); // --- INFO is the default logging level; set it to FINE to see more
  core::chemical::MonomerStructure_SP str = core::chemical::MonomerStructure::from_cif(argv[1]);
  std::cout<<"\nPOLAR H: ";
  for (auto i: str->polar_hydrogens()) std::cout<<i->atom_name()<<" ";
  std::cout<<"\nNONPOLAR H: ";

  for (auto i: str->nonpolar_hydrogens()) std::cout<<i->atom_name()<<" ";
  std::cout<<"\nNONPOLAR: ";

  for (auto i: str->nonpolar_heavy()) std::cout<<i->atom_name()<<" ";
  std::cout<<"\nDONORS: ";

  for (auto i: str->hydrogen_donors()) std::cout<<i->atom_name()<<" ";
  std::cout<<"\nACCEPTORS: ";

  for (auto i: str->hydrogen_acceptors()) std::cout<<i->atom_name()<<" ";
  	std::cout<<"\n";

  core::chemical::MonomerStructureFactory m = core::chemical::MonomerStructureFactory::get_instance();
  core::chemical::MonomerStructure_SP mstr = m.get("PRO");
  std::cout<<mstr->code3<<"\n";
  std::cout<<"\nPOLAR H: ";
  for (auto i: mstr->polar_hydrogens()) std::cout<<i->atom_name()<<" ";
}
