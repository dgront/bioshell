#include <memory>
#include <iostream>
#include <string>

#include <core/algorithms/trees/TreeNode.hh>
#include <core/algorithms/trees/algorithms.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to use a TreeNode data structure defined in BioShell

USAGE:
./ex_TreeNode

)";

/** @brief Simple demo for TreeNode class
 *
 * This program creates a small tree with 7 nodes
 *
 * CATEGORIES: core::algorithms::trees::TreeNode
 * KEYWORDS:   algorithms; data structures; graphs
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::algorithms::trees;

  std::shared_ptr<TreeNode<std::string>> n1 = std::make_shared<TreeNode<std::string>>("A",1);
  std::shared_ptr<TreeNode<std::string>> n2 = std::make_shared<TreeNode<std::string>>("B",2);
  std::shared_ptr<TreeNode<std::string>> n3 = std::make_shared<TreeNode<std::string>>("C",3);
  std::shared_ptr<TreeNode<std::string>> n4 = std::make_shared<TreeNode<std::string>>("D",4);
  std::shared_ptr<TreeNode<std::string>> n5 = std::make_shared<TreeNode<std::string>>("E",5);
  std::shared_ptr<TreeNode<std::string>> n6 = std::make_shared<TreeNode<std::string>>("F",6);
  std::shared_ptr<TreeNode<std::string>> n7 = std::make_shared<TreeNode<std::string>>("G",7);
  n1->add_branch(n2);
  n1->add_branch(n3);
  n2->add_branch(n4);
  n2->add_branch(n5);
  n2->add_branch(n7);
  n5->add_branch(n6);
  depth_first_preorder(n1,[](std::shared_ptr<TreeNode<std::string>> n){ std::cout << n->id<< "\n";});

  std::cout << "Size of the tree: " << size(n1)<<"\n";

  return 0;
}
