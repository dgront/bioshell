#include <memory>
#include <iostream>
#include <random>

#include <core/algorithms/trees/kd_tree.hh>
#include <core/algorithms/trees/BinaryTreeNode.hh>
#include <core/algorithms/trees/algorithms.hh>
#include <core/data/io/DataTable.hh>
#include <utils/exit.hh>

std::string program_info = R"(

ex_ramachandran_kd_tree partitions observations from a Ramachandran map


USAGE:
    ex_ramachandran_kd_tree phi_psi.dat n_level width

where phi_psi.dat is an input file with two columns of data (Phi and Psi angles),
width - width of a square range for counting neighbors and n_level - maximum level on the kd-tree to assign.

The output consists of lines as below:
 -65.08  125.25  15  684
where the first two columns contain the Phi,Psi angles, respectively, that have been loaded from the input file.
The third value (15 in the example) is the number of a rectangular area resulting from the 2D-tree construction.
Finally 684 is the number of points found in a square area width x width centered at the given point. That value
provides in insight how probable is a given  Phi, Psi observation

)";

using namespace core::algorithms::trees;

class Point: public std::pair<float, float> {
public:

  Point(float phi, float psi) : std::pair<float, float>(phi, psi) {}

  float operator[](const size_t k) const { if (k == 0) return first; else return second; }
};

/// Operation that computes the distance between points on Ramachandran map
struct PhiPsiDistance {

  /// This operation will be called at every tree node considered during a tree traversal
  float operator() (const Point & n1,const Point & n2) const {
    float d = (n1.first - n2.first);
    float d2 = d * d;
    d = (n1.second - n2.second);
    d2 += d * d;
    return sqrt(d2);
  }
};

/** @brief Tree traversal operation prints a given point along with its node level and the number of neighbors
 */
struct PrintPoint {

  PrintPoint(std::shared_ptr<BinaryTreeNode<KDTreeNode<Point>>> root, float width) : root_(root), w_(width / 2.0) {}

  /// this operator prints a visited node on the screen
  void operator()(std::shared_ptr<BinaryTreeNode<KDTreeNode<Point>>> node) {

    Point q_low(node->element.element[0] - w_, node->element.element[1] - w_);
    Point q_up(node->element.element[0] + w_, node->element.element[1] + w_);
    std::vector<Point> hits;
    search_kd_tree(root_,  q_low, q_up, 2, hits);

    std::cout << utils::string_format("%7.2f %7.2f %3d %4d\n",
                  node->element.element.first, node->element.element.second, node->element.level, hits.size());
  }
private:
  std::shared_ptr<BinaryTreeNode<KDTreeNode<Point>>> root_;
  float w_;
};

/** @brief A simple example shows how to use BioShell kd-tree routines.
 *
 * The program reads a file with Phi, Psi observations and partitions them in a kd-tree.
 *
 * CATEGORIES: core::algorithms::trees::BinaryTreeNode; core::algorithms::trees::kd_tree.hh
 * KEYWORDS:   neighborhood detection; data structures; algorithms
 */
int main(const int argc, const char* argv[]) {

  if (argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameters

  core::index2 n_level = (argc > 2) ? atoi(argv[2]) : 4;
  core::index2 width = (argc > 3) ? atof(argv[3]) : 5;
  // ---------- First we read a file with Phi, Psi observations
  core::data::io::DataTable dt;
  dt.load(argv[1]);
  std::vector<Point> points;            // container for the points
  for(const auto & row:dt)
    points.emplace_back(row.get<float>(0), row.get<float>(1));

  // ---------- Here the actual kd-tree is constructed
  std::shared_ptr<BinaryTreeNode<KDTreeNode<Point>>> root =
      create_kd_tree<Point, std::vector<Point>::iterator, CompareAsReferences<Point>>(points.begin(), points.end(), 2);


  std::vector<std::shared_ptr<BinaryTreeNode<KDTreeNode<Point>>>> node_group_representatives;
  collect_given_level(root, n_level, node_group_representatives, 0);
  core::index2 group_id = 0;
  for(const auto node:node_group_representatives) {
    depth_first_preorder(node, [group_id](std::shared_ptr<BinaryTreeNode<KDTreeNode<Point>>> node) {
      node->element.level = group_id; });
    ++group_id;
  }

  // ---------- Here we print each node
  PrintPoint pp(root, width);
  breadth_first_preorder(root, pp); // finally, each node is printed
}
