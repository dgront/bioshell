#include <memory>
#include <iostream>
#include <random>
#include <core/algorithms/Combinations.hh>

#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test for BioShell's combination generator prints all possible tripeptides by taking all 3-element
combinations of 20-elements set.

USAGE:
./ex_Combinations

)";


/** @brief A simple example shows how to generate Combination
 *
 * The program generates all possible tripeptides as ${20}\choose {3}$ combinations
 *
 * CATEGORIES: core::algorithms::Combination;
 * KEYWORDS:  algorithms; random
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  std::vector<std::string> amino_acids{"ALA","ARG","ASP","ASN","CYS","PHE","GLU","GLN","GLY","HIS","ILE","LEU","LYS","MET","PRO","SER",
  "THR","TYR","TRP","VAL"};

  std::vector<std::string> a_combination(3);
  core::algorithms::Combinations<std::string> generator(3,amino_acids);
  int cnt = 0;
  while (generator.next(a_combination)) {
    std::cout << a_combination[0] << " " << a_combination[1] << " " << a_combination[2] << "\n";
    ++cnt;
  }
  std::cout << "# " << cnt << " combinations generated\n";
}
