#include <vector>
#include <iostream>
#include <iterator>

#include <core/algorithms/basic_algorithms.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to find islands of consecutive elements in a container.

USAGE:
./ex_consecutive_find

)";

struct AreConsecutive {
  bool operator()(int i, int n) { return (n - i) == 1; }
};


struct SSranges {
  bool operator()(char ci, char cn) { return (ci==cn); }
};

/** @brief Shows how to find islands of consecutive elements in a container
 *
 * CATEGORIES: core/algorithms/basic_algorithms.hh
 * KEYWORDS:   algorithms; data structures
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  std::vector<int> v{-3, 1, 2, 4, 5, 7, 8, 9, 10, 12, 12, 13, 16, 18, 20, 21, 22, 23};
  std::vector<std::pair<int, int>> islands;

  int n_islands = core::algorithms::consecutive_find(v.begin(), v.end(), AreConsecutive(), islands);

  for (const auto &is : islands) {
    for (int i = is.first; i <= is.second; ++i)
      std::cout << v[i] << " " ;
    std::cout << "\n";
  }

  std::string ss("CEEEEEECCCCCCEEEEEECCHHHHHHHHHHHHHHHCCCCCEEEEECCCCEEEEEC");
  std::vector<char> v_ss(ss.begin(), ss.end());
  islands.clear();
  n_islands = core::algorithms::consecutive_find(v_ss.begin(), v_ss.end(), SSranges(), islands);
  for (const auto &is : islands) 
    std::cout << v_ss[is.first] << " " << is.first << " " << is.second << "\n";
}
