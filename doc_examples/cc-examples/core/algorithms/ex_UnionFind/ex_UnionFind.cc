#include <memory>
#include <iostream>

#include <core/algorithms/UnionFind.hh>
#include <core/calc/statistics/Random.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to use the Union-Find algorithm.

USAGE:
./ex_UnionFind

REFERENCE:
    https://en.wikipedia.org/wiki/Disjoint-set_data_structure

)";

// ---------- Data type of objects that will be clustered
struct Point2D {
  float x, y;

  Point2D(float nx, float ny) : x(nx), y(ny) {}

  float distance_to(const Point2D &p) { return sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)); }
};

// ---------- this operator is necessary because core::algorithms::UnionFind keeps std::map of data points
bool operator<(const Point2D &lhs, const Point2D &rhs) { return lhs.x < rhs.x; }

/** @brief A simple example shows how to use UnionFind algorithm.
 *
 * The program calculates greedy clustering of points in 2D. The number of points can be provided from command line
 *
 * CATEGORIES: core::algorithms::UnionFind;
 * KEYWORDS:   data structures; data structures; algorithms
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::calc::statistics;

  // ---------- Here we generate N random points in 3D space to be partitioned
  const unsigned short N = (argc > 1) ? atoi(argv[1]) : 500;
  const float cutoff = (argc > 2) ? atof(argv[2]) : 0.05;
  std::vector<Point2D> points;        // container for the points

  Random::seed(0);                            // seed random number generator
  Random &gen = Random::get();                // get rnd generator singleton
  UniformRealRandomDistribution<double> rnd;  // uniform distribution will be used to assign coordinates

  core::algorithms::UnionFind<Point2D, core::index2> uf;
  for (unsigned int i = 0; i < N; ++i) {
    points.emplace_back(rnd(gen), rnd(gen)); // we use <code>emplace_back()</code> to create point objects directly in the container
    uf.add_element(points.back());
    for (unsigned int j = 0; j < i; ++j) {
      if (points[i].distance_to(points[j]) < cutoff) uf.union_set(i, j);
    }
  }

  std::cout << "# x-coord    y-coord  cluster_assignment\n";
  for (unsigned int i = 0; i < N; ++i)
    std::cout << points[i].x << " " << points[i].y << " " << uf.find_set(i) << "\n";
}
