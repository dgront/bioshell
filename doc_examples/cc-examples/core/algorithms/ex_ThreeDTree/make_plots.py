#!/usr/bin/env python

import sys, re
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

INFILE = sys.argv[1] if (len(sys.argv) == 2) else "expected_outputs/stdout.out"

x_before = []
y_before = []
z_before = []
n  = []

for line in open(INFILE) :
  tokens = re.split("\s+",line.strip())
  if len(tokens)==4 : 
    x_before.append(float(tokens[0]) )
    y_before.append(float(tokens[1]) )
    z_before.append(float(tokens[2]) )
    n.append( int(tokens[3]) )

color_types = [(1/255.0,1/255.0,1/255.0),(70/255.0,136/255.0,244/255.0),(234/255.0,67/255.0,53/255.0),
	(251/255.0,188/255.0,6/255.0), (52/255.0,168/255.0,83/255.0)]
colors = [color_types[i] for i in n ] 
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x_before, y_before, z_before, marker="o", c=colors, edgecolors=colors)
plt.xticks([0.0, 0.5, 1.0])
plt.yticks([0.0, 0.5, 1.0])
ax.set_zticks([0.0, 0.5, 1.0])
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
plt.tight_layout()
fig.savefig('scatter_3d.png', dpi=600)
#fig.savefig('scatter_3d.pdf')
plt.close(fig)
