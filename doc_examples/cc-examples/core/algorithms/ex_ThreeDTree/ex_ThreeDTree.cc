#include <memory>
#include <iostream>
#include <random>

#include <core/algorithms/trees/kd_tree.hh>
#include <core/algorithms/trees/BinaryTreeNode.hh>
#include <core/algorithms/trees/algorithms.hh>

#include <core/data/basic/Vec3.hh>
#include <core/calc/statistics/Random.hh>

using core::data::basic::Vec3;
using namespace core::algorithms::trees;
using namespace core::calc::statistics;


/// Tree traversal operation prints each node on the screen
struct PrintPoint {
  void operator()(std::shared_ptr<BinaryTreeNode<KDTreeNode<Vec3>> > node) {
    std::cout << node->element.element << " " << node->element.level << "\n";
  }
};


/** @brief A simple example shows how to use BioShell kd-tree routines.
 *
 * The program generates N=500 random points and partites them into KD-tree. Later the tree is used to find spatal
 * neighbors in 3D space.
 *
 * CATEGORIES: core::algorithms::trees::BinaryTreeNode; core::algorithms::trees::kd_tree.hh
 * KEYWORDS:   neighborhood detection; data structures; algorithms
 */
int main(const int argc, const char* argv[]) {

  // ---------- Here we generate N random points in 3D space to be partitioned
  const unsigned short N = 5000;
  Random::seed(0);                            // seed random number generator
  Random & gen = Random::get();               // get rnd generator singleton
  UniformRealRandomDistribution<double> rnd;  // uniform distribution will be used to assign coordinates
  std::vector<Vec3> atoms;                    // container for the points

  // ---------- We use <code>emplace_back()</code> to create Vec3 objects directly in the container
  for(unsigned int i=0;i<N;++i) atoms.emplace_back(rnd(gen),rnd(gen),rnd(gen));

  // ---------- Here the actual kd-tree is constructed
  std::shared_ptr<BinaryTreeNode<KDTreeNode<Vec3>> > root = create_kd_tree<Vec3,std::vector<Vec3>::iterator, CompareAsReferences<Vec3>>(atoms.begin(),atoms.end());

  // ---------- Here we divide all the points into \f$2^2=4\f$ groups
  std::vector<std::shared_ptr<BinaryTreeNode<KDTreeNode<Vec3>>>> node_groups;
  collect_given_level(root, 2, node_groups);
  unsigned short subcluster_id = 0;
  for(const auto node:node_groups) {    // then we mark all nodes in each subtree by a distinct ID number
    depth_first_preorder(node, [subcluster_id](std::shared_ptr<BinaryTreeNode<KDTreeNode<Vec3>>> node) {
      node->element.level = subcluster_id; });
    ++subcluster_id;
  }

  breadth_first_preorder(root, PrintPoint()); // finally, each node is printed

  // ---------- Here is an example how to search the tree
  Vec3 query(0.7,0.7,0.4); // a query point

  // ---------- Here is an example how to find the closes element
  Vec3 best_point;
  double d = search_kd_tree(root, [](const Vec3 &v1, const Vec3 &v2) { return v1.distance_to(v2); }, query, best_point);
  std::cout << "point closest to query: " << best_point << " : " << d << "\n";

  Vec3 q_low(0.68,0.68,0.38);
  Vec3 q_up(0.8,0.8,0.45);
  std::vector<Vec3> hits;
  search_kd_tree(root,  q_low, q_up, 3, hits);
  std::cout << "point within a box bounded by: " << q_low << " and " << q_up << ":\n";
  for (const auto &p:hits) std::cout << p << "\n";
}
