#include <memory>
#include <iostream>

#include <core/algorithms/trees/TreeNode.hh>
#include <core/algorithms/trees/algorithms.hh>
#include <core/algorithms/trees/trees_io.hh>


/** @brief Simple demo for BinaryTreeNode class
 *
 * This program creates a small tree with 6 nodes and performs various operations on it
 *
 * CATEGORIES: core/algorithms/trees/BinaryTreeNode
 * KEYWORDS:   algorithms; data structures; graphs
 * IMG: ex_BinaryTreeNode_1.png
 * IMG_ALT: Example tree node
 */
int main(const int argc, const char* argv[]) {

  using namespace core::algorithms::trees;

  typedef std::shared_ptr<BinaryTreeNode<char>> Node_SP; // --- Let's make the typename shorter
  Node_SP p1(new BinaryTreeNode<char>(0, 'A'));
  Node_SP p2(new BinaryTreeNode<char>(1, 'B'));
  Node_SP p3(new BinaryTreeNode<char>(2, 'C'));
  Node_SP p4(new BinaryTreeNode<char>(3, 'D'));
  Node_SP p5(new BinaryTreeNode<char>(4, 'E'));
  Node_SP p6(new BinaryTreeNode<char>(5, 'F'));

  p1->set_left_right(p2, p3);
  p3->set_left_right(p4, p5);
  p2->set_left(p6);
  std::cout << "Size of the whole tree and its right branch: " << size(p1) << " " << size(p3)
            << " (should be 6 and 3)\n";

  std::vector<char> elements;
  collect_leaf_elements(p1, elements);
  std::cout << "Leaf-only elements (E D F):\n";
  for (std::vector<char>::const_iterator i = elements.begin(); i != elements.end(); i++)
    std::cout << *i << ' ';
  std::cout << "\n";

  std::cout << "All elements stored on the tree (A C E D B F):\n";
  elements.clear();
  collect_elements(p1, elements);
  for (std::vector<char>::const_iterator i = elements.begin(); i != elements.end(); i++)
    std::cout << *i << ' ';
  std::cout << "\n";

  std::cout << "Leaf-only nodes (E D F):\n";
  elements.clear();
  std::vector<Node_SP> nodes;
  collect_leaf_nodes(p1, nodes);
  for (Node_SP & i : nodes)
    std::cout << i->element << ' ';
  std::cout << "\n";

  std::cout << "The tree was:\n";
  XMLFormatters<Node_SP> xml(std::cout);
  write_tree(p1, xml.start, xml.leaf, xml.stop);

  return 0;
}
