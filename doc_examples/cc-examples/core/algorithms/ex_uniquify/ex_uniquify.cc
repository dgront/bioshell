#include <vector>

#include <core/algorithms/basic_algorithms.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test for uniquify() method which removes redundant objects from a container

USAGE:
./ex_uniquify

)";

/** @brief Tests uniquify() method which removes redundant objects from a container.
 *
 * CATEGORIES: core/algorithms/basic_algorithms.hh
 * KEYWORDS:   data structures; algorithms
 */
int main(int cnt, char* argv[]) {

  if ((cnt > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  std::vector<int> datum = std::vector<int> { 1, 8, 4, 5, 9, 4, 5 };
  // ---------- Below we define >is_equal< operator
  auto eq = [](std::vector<int>::iterator a, std::vector<int>::iterator b) -> bool { return *a == *b; };
  // ---------- Below we define >less_then< operator
  auto lt = [](std::vector<int>::iterator a, std::vector<int>::iterator b) -> bool { return *a < *b; };

  // ---------- Below we apply uniquify() operation on a range of integers
  datum.erase(core::algorithms::uniquify(datum.begin(), datum.end(), eq, lt), datum.end());
  for (int c:datum) std::cout << c << " ";
  std::cout << "\n";

  // ---------- Below we apply uniquify() operation on a range of characters
  std::vector<char> chars = std::vector<char> {'a', 'g', 'd', 'r', 'a', 'd'};
  chars.erase(core::algorithms::uniquify(chars.begin(), chars.end()), chars.end());
  for (char c:chars) std::cout << c << " ";
  std::cout << "\n";
}
