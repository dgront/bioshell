#include <vector>
#include <iostream>
#include <iterator>
#include <core/algorithms/basic_algorithms.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test shows how to how to find an intersection of two sorted vectors of data

USAGE:
./ex_intersect_sorted

)";


/** @brief Shows how to find an intersection of two sorted vectors of data
 *
 * CATEGORIES: core/algorithms/basic_algorithms.hh
 * KEYWORDS:   algorithms; data structures
 */
int main(const int argc, const char* argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  std::vector<int> range1({1,2,3,5,6,7}), range2({5,6,7,8,9,10}), repeated;

  // Note that both <code>range1</code> and <code>range2</code> are already sorted!
  core::algorithms::intersect_sorted(range1.begin(), range1.end(), range2.begin(), range2.end(), repeated);

  // Print the element found as the intersection between the two ranges
  std::copy(repeated.begin(), repeated.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << "\n";
}
