#include <memory>
#include <iostream>
#include <iomanip>

#include <core/algorithms/GraphWithData.hh>
#include <core/algorithms/SimpleGraph.hh>

/** @brief A unit test for SimpleGraph and GraphWithData classes
 *
 * This program creates small graph data structures and test their methods
 *
 * CATEGORIES: core/algorithms/GraphWithData; core/algorithms/SimpleGraph
 * KEYWORDS:   algorithms; data structures
 * IMG_ALT: Example tree node
 */
int main(const int argc, const char* argv[]) {

  using namespace core::algorithms;

  GraphWithData<SimpleGraph,int,std::string> g;
  g.add_vertex(0);
  g.add_vertex(1);
  g.add_vertex(2);
  g.add_vertex(3);
  g.add_edge(0,2,"0-2");
  g.add_edge(1,2,"1-2");
  g.add_edge(3,2,"3-2");
  g.add_edge(core::index4(3),core::index4(0),"0-3");

  std::cout << "# adjacency matrix\n";
  g.print_adjacency_matrix(std::cout);
  std::cout << "# are 0 and 3 connected?\n" << std::boolalpha << g.are_connected(0,3)<<"\n";

  g.remove_edge(0,3);
  std::cout << "# are 0 and 3 still connected?\n" << std::boolalpha << g.are_connected(0,3)<<"\n";
  std::cout << "# adjacency matrix\n";
  g.print_adjacency_matrix(std::cout);

  std::cout << "# neighbors of 3\n";
  for(auto iter = g.begin(3);iter!=g.end(3);++iter)
    std::cout << *iter<<" ";
  std::cout << "\n";

  return 0;
}
