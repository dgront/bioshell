#include <iostream>

#include <core/algorithms/IterateIJ.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Unit test which shows how to use IterateIJ class, which is an iterator to a 2D container, e.g. a 2D array.

USAGE:
./ex_IterateIJ

)";

/** @brief A simple example shows how to use IterateIJ
 *
 *
 * CATEGORIES: core::algorithms::IterateIJ
 * KEYWORDS:  data structures; algorithms
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  // ---------- Here we declare an iterator that generates indexes for a square 5x5 2D array
  core::algorithms::IterateIJ ij1(5, false);
  for (auto ij:ij1) std::cout << ij.first << " " << ij.second << "\n";
  std::cout <<"\n";

  // ---------- Here we declare an iterator that generates indexes for the UR triangle of a 5x5 2D array
  core::algorithms::IterateIJ ij2(5, true);
  for (auto ij:ij2) std::cout << ij.first << " " << ij.second << "\n";
  std::cout <<"\n";

  // ---------- Now only selected rows of a square 5x5 2D array
  core::algorithms::IterateIJ ij3(5, false);
  ij3.add_selected_row(1).add_selected_row(2);
  for (auto ij:ij3) std::cout << ij.first << " " << ij.second << "\n";
  std::cout <<"\n";

  // ---------- Now only selected rows and only  UR triangle of a 5x5 2D array
  core::algorithms::IterateIJ ij4(5, true);
  ij4.add_selected_row(2).add_selected_row(3);
  for (auto ij:ij4) std::cout << ij.first << " " << ij.second << "\n";
}
