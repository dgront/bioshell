#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo "# Running ./ap_SequenceWeightingProtocol ./test_inputs/ferrodoxins.fasta"
./ap_SequenceWeightingProtocol ./test_inputs/ferrodoxins.fasta

echo "# Running ./ap_SequenceWeightingProtocol ./test_inputs/identical.fasta"
./ap_SequenceWeightingProtocol ./test_inputs/identical.fasta

echo "# Running ./ap_SequenceWeightingProtocol ./test_inputs/cyped.CYP109.aln"
./ap_SequenceWeightingProtocol ./test_inputs/cyped.CYP109.aln

