#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "Running ./ex_selection_protocols test_inputs/5edw.pdb"
./ex_selection_protocols test_inputs/5edw.pdb
echo -e "Running ./ex_selection_protocols test_inputs/3dcg.pdb"
./ex_selection_protocols test_inputs/3dcg.pdb
