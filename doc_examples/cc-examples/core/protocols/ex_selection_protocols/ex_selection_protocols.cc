#include <iostream>
#include <sstream>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/protocols/selection_protocols.hh>
#include <utils/exit.hh>

std::string program_info = R"(

Simple test shows how to use AtomSelector from selection protocols set.
As an example, selects atoms that belong to nucleic acid residues.

USAGE:
    ex_selection_protocols 5edw.pdb

)";

/** @brief Shows how to use selection protocols functions
 *
 * CATEGORIES: core::protocols::keep_selected_atoms()
 * KEYWORDS:   PDB input; Selection protocols; structure selectors
 */
int main(const int argc, const char* argv[]) {

  if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::structural;
  using namespace core::data::structural::selectors;
  using namespace core::protocols;

  core::data::io::Pdb reader(argv[1]);

  { // --- section which tests selecting nucleotides
    Structure_SP strctr = reader.create_structure(0);

    std::shared_ptr<AtomSelector> select_nt = std::make_shared<IsNT>();

    keep_selected_atoms(*select_nt, *strctr);
    for (auto chain_sp : *strctr)
      std::cout << utils::string_format("\tchain %s has %3d residues satisfying the selector\n", chain_sp->id().c_str(),
        chain_sp->size());
  }
}
