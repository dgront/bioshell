#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
echo -e "# Running ./ap_PairwiseSequenceIdentityProtocol test_inputs/small50_95identical.fasta 4 0.28 0 5"
./ap_PairwiseSequenceIdentityProtocol test_inputs/small50_95identical.fasta 4 0.28 0 5

echo -e "# Running ./ap_PairwiseSequenceIdentityProtocol test_inputs/small50_95identical.fasta 4 0.28 0"
./ap_PairwiseSequenceIdentityProtocol test_inputs/small50_95identical.fasta 4 0.28 0

echo -e "# Running ./ap_PairwiseSequenceIdentityProtocol test_inputs/small500_95identical.fasta 4 0.28"
./ap_PairwiseSequenceIdentityProtocol test_inputs/small500_95identical.fasta 4 0.28
