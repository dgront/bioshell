#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

DIR="test_inputs"
echo -e "# Running ./ap_docking_crmsd $DIR/2m56-ref.pdb CAM $DIR/00199.pdb $DIR/00963.pdb $DIR/04473.pdb"
./ap_docking_crmsd $DIR/2m56-ref.pdb CAM $DIR/00199.pdb $DIR/00963.pdb $DIR/04473.pdb

echo -e "# Running ./ap_docking_crmsd - CAM  $DIR/2m56-ref.pdb $DIR/00199.pdb $DIR/00963.pdb $DIR/04473.pdb"
./ap_docking_crmsd - CAM  $DIR/2m56-ref.pdb $DIR/00199.pdb $DIR/00963.pdb $DIR/04473.pdb

echo -e "# Running ./ap_docking_crmsd test_inputs/2kwi.pdb B test_inputs/2kwi.pdb"
./ap_docking_crmsd test_inputs/2kwi.pdb B test_inputs/2kwi.pdb
