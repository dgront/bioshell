#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"

echo -e "#Running ./ap_LigandsOnGridProtocol 2.0 test_inputs/cat_list"
./ap_LigandsOnGridProtocol 2.0 test_inputs/cat_list
mkdir -p outputs_from_test
mv *.out outputs_from_test
