#include <vector>
#include <string>
#include <fstream>
#include <iostream>

#include <cstring>


#include <core/data/io/Pdb.hh>
#include <utils/io_utils.hh>

#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <core/protocols/LigandsOnGridProtocol.hh>
#include <utils/exit.hh>

using namespace core::data::io;           // PDB is from this namespace
using namespace core::data::structural;
using namespace core::data::structural::selectors;
using namespace core::calc::structural;
using namespace utils;
using namespace std;


std::string program_info = R"(

ap_LigandsOnGridProtocol reads a list of pdb files and creates grid with ligands in it.

USAGE:
    ap_LigandsOnGridProtocol box_grid_width models-list
)";

/** @brief Reads list of pdb files and creates grid with ligands in it.
 *
 * The first model on list (index = 0 ) is the representative one.
 *
 * CATEGORIES: core::protocols::LigandsOnGridProtocol
 * GROUP: Structure calculations;  Docking;
 * KEYWORDS: PDB input;
 */
int main(const int argc, const char *argv[]) {

  if (argc < 3) utils::exit_OK_with_message(program_info);

  std::vector<std::string> pdb_files; //vector of string file names
  utils::read_listfile(argv[2], pdb_files);
  // assumes that ligand is in B chain
  AtomSelector_SP select_ligand = std::static_pointer_cast<AtomSelector>(std::make_shared<ChainSelector>("B"));
  // assumes that receptor is in A chain
  AtomSelector_SP select_receptor = std::static_pointer_cast<AtomSelector>(std::make_shared<ChainSelector>("A"));
  // creating LigandsOnGridProtocol object
  core::protocols::LigandsOnGridProtocol ligpro = core::protocols::LigandsOnGridProtocol(select_ligand, select_receptor);
  // setting box_grid_size
  ligpro.box_grid_size(atof(argv[1]));
  PdbLineFilter filter = core::data::io::is_ca;
  Pdb pdb = Pdb(pdb_files[0], filter);
  //creating structure from first file on a list
  Structure_SP strctr = pdb.create_structure(0);
  // adding structure to LigandsOnGridProtocol object
  ligpro.add_input_structure(strctr);
  // loading and adding rest of structures from cat_list
  for (int i = 1; i < pdb_files.size(); i++) {
    Pdb pdb = Pdb(pdb_files[i], filter);
    pdb.fill_structure(0, *strctr);
//    std::cout << pdb_files[i] << "\n";
    ligpro.add_input_structure(strctr);
  }

  // running the calculation to put ligands into grid
  ligpro.calculate();
  // creating a copy of a vector with hashes from filled grid cells
  std::vector<core::index4> grid_cells = ligpro.grid()->filled_cells();

  core::index4 index = 0; // variable to remember iterator for biggest cell
  int size = 100; //variable to remember SIZE
  while (grid_cells.size() > 0 and size >= 10) { //until vector is not empty and there are cells bigger than 10
    size = 0;
    for (core::index4 i = 0; i < grid_cells.size(); i++) { // iterating over cells
      //std::cout<<grid_cells[i]<<" "<<ligpro.grid()->get_cell(grid_cells[i]).size()<<"\n";
      if (ligpro.grid()->get_cell(grid_cells[i]).size() > size) { //checking if current cell size is bigger then SIZE
        size = ligpro.grid()->get_cell(grid_cells[i]).size(); //if yes, changing size and index values
        index = grid_cells[i];
      }
    }
    if (size >= 10) {
      // std::cout<<index<<" "<<ligpro.grid()->get_cell(index).size()<<"\n";

      std::vector<core::index4> hashes; //vector to store neighbor cells
      ligpro.grid()->get_neighbor_cells(index, hashes); //getting all hashes for neighbors cells
      for (core::index4 ind = 0; ind < hashes.size(); ind++) {
        grid_cells.erase(std::remove(grid_cells.begin(), grid_cells.end(), hashes[ind]),
                         grid_cells.end()); //attepmt to erase biggest cell from the vector
      }

      std::ofstream of(utils::to_string(index) + ".out");
      std::vector<core::data::structural::PdbAtom_SP> sink;
      ligpro.grid()->get_neighbors(index, sink);
      for (core::index4 a = 0; a < sink.size(); a++) {//iterating over Atoms in sink
        of << sink[a]->id() << "\n"; //writing to file
      }
      of.close();
    }
  }
}
