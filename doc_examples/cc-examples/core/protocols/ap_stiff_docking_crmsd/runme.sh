#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"



echo "# Running ./ap_stiff_docking_crmsd test_inputs/2m56-ref.pdb CAM test_inputs/00199.pdb test_inputs/00963.pdb test_inputs/04473.pdb"
./ap_stiff_docking_crmsd test_inputs/2m56-ref.pdb CAM test_inputs/00199.pdb test_inputs/00963.pdb test_inputs/04473.pdb

echo "# Running ./ap_stiff_docking_crmsd test_inputs/2kwi-1.pdb B test_inputs/2kwi.pdb"
./ap_stiff_docking_crmsd test_inputs/2kwi-1.pdb B test_inputs/2kwi.pdb

echo -e "# Running ./ap_stiff_docking_crmsd - B test_inputs/2kwi.pdb"
./ap_stiff_docking_crmsd - B test_inputs/2kwi.pdb
