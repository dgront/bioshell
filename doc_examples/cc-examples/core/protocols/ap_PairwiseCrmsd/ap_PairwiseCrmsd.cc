#include <iostream>

#include <core/protocols/PairwiseCrmsd.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <utils/exit.hh>

std::string program_info = R"(

ap_PairwiseCrmsd calculates  crmsd value between every pair of protein structures given at the input
(at least two structures must be provided). Only values smaller than 20 Angstroms are printed.

This example evaluates crmsd for each pair of proteins twice: on C-alpha atoms and on all backbone atoms

USAGE:
    ap_PairwiseCrmsd structureA.pdb structureB.pdb [structureC.pdb ... ]

EXAMPLE:
    ap_PairwiseCrmsd 2gb1.pdb 2gb1-model1.pdb 2gb1-model2.pdb

)";

/** @brief Calculates crmsd value for a set of protein structures (at least two)
 *
 * CATEGORIES: core::protocols::PairwiseCrmsd
 * KEYWORDS:   PDB input; crmsd; structure selectors
 * GROUP: Structure calculations;
 */
int main(const int argc, const char *argv[]) {

  if(argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using core::data::basic::Vec3;
  using namespace core::data::structural::selectors; // --- for all AtomSelector types
  using namespace core::data::io;
  using namespace core::protocols;

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> tags;
  for (int i = 1; i < argc; ++i) {
    core::data::io::Pdb reader(argv[i],all_true(is_not_alternative,is_not_water), keep_all, false); // --- note we read all atoms but skip alternate locators and waters
    structures.push_back(reader.create_structure(0));
    tags.push_back(structures.back()->code());
  }

  // ---------- crmsd on C-alpha : this is the
  std::cout <<"# crmsd on alpha carbons:\n";
  std::shared_ptr<AtomSelector> is_CA = std::make_shared<IsCA>();
  PairwiseCrmsd rmsd_ca(structures, is_CA, tags);
  rmsd_ca.crmsd_cutoff(20.0); // crmsd cutoff large enough to get some output
  rmsd_ca.calculate();

  // ---------- crmsd on backbone
  std::cout <<"# crmsd on heavy backbone atoms:\n";
  std::shared_ptr<AtomSelector> is_bb = std::make_shared<IsBB>();
  std::shared_ptr<AtomSelector> not_h = std::make_shared<NotHydrogen>();
  std::shared_ptr<LogicalANDSelector> heavy_bb = std::make_shared<LogicalANDSelector>();
  heavy_bb->add_selector(is_bb);
  heavy_bb->add_selector(not_h);
  PairwiseCrmsd rmsd_bb(structures, heavy_bb, tags);
  rmsd_bb.crmsd_cutoff(20.0); // crmsd cutoff large enough to get some output
  rmsd_bb.calculate();
}
