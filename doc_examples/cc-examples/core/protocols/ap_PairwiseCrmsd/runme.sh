#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"


echo -e "#Running ap_PairwiseCrmsd ./test_inputs/2gb1.pdb ./test_inputs/2gb1-model1.pdb \
    ./test_inputs/2gb1-model2.pdb ./test_inputs/2gb1-model3.pdb ./test_inputs/2gb1-model4.pdb"

./ap_PairwiseCrmsd ./test_inputs/2gb1.pdb ./test_inputs/2gb1-model1.pdb \
    ./test_inputs/2gb1-model2.pdb ./test_inputs/2gb1-model3.pdb ./test_inputs/2gb1-model4.pdb
