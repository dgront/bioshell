#!/bin/bash

BIOSHELL_DIR="/home/bioshell/src.git/bioshell"
OUT_DIR="/home/bioshell/public_html"

# Generate API documentation
function make_docs {
  cd $BIOSHELL_DIR
  ( cat $BIOSHELL_DIR/doc/Doxyfile ; echo "OUTPUT_DIRECTORY="$OUT_DIR"/doc/" ) | doxygen -
}

# Compile - clang; param1 is the build type
function make_clang {
  mkdir -p $OUT_DIR/builds/clang-$1
  cd $OUT_DIR/builds/clang-$1
  cmake -DCMAKE_CXX_COMPILER=clang++  -DCMAKE_C_COMPILER=clang -DCMAKE_BUILD_TYPE=$1 $BIOSHELL_DIR
  make >$OUT_DIR/output-clang-$1.txt 2>$OUT_DIR/errors-clang-$1.txt
  rm -rf $OUT_DIR/builds/bin.clang-$1
  mv $OUT_DIR/builds/bin $OUT_DIR/builds/bin.clang-$1
  cd
  rm -rf $OUT_DIR/builds/clang-$1
}

# Compile - gcc-4.8; param1 is the build type
function make_gcc-4.8 {
  mkdir -p $OUT_DIR/builds/gcc-4.8-$1
  cd $OUT_DIR/builds/gcc-4.8-$1
  cmake -DCMAKE_CXX_COMPILER=g++-4.8  -DCMAKE_C_COMPILER=gcc-4.8 -DCMAKE_BUILD_TYPE=$1 $BIOSHELL_DIR
  make >$OUT_DIR/output-gcc-4.8-$1.txt 2>$OUT_DIR/errors-gcc-4.8-$1.txt
  rm -rf $OUT_DIR/builds/bin.gcc-4.8-$1
  mv $OUT_DIR/builds/bin $OUT_DIR/builds/bin.gcc-4.8-$1
  cd
  rm -rf $OUT_DIR/builds/gcc-4.8-$1
}


status=`git status -uno`
if [[ $status == *"behind"* ]]
then
    echo "Need to pull"

#    git pull origin master
#    make_docs
#    make_clang Release
#    make_gcc-4.8 Release
fi
