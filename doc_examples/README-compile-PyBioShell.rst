=========================
How to compile PyBioShell
=========================

This document describes how to compile PyBioShell distribution 


1. Install Binder
================

Clone binder from repository and compile it
https://github.com/RosettaCommons/binder.git
and run ``build.py`` script

In my case Binder installation ison the same level as `bioshell` repository: 
``~/src.git/binder`` and ``~/src.git/bioshell``, respectively

Such a structure is assumed in configuration files, primarily by ``pybioshell.config``

2. Build bindings
=================

Make a directory ``build_bindings`` in the main BioShell directory (i.e in the directory where
``pybioshell.config`` is located:
::
    mkdir -p build_bindings

and compile python bindings to BioShell (aka. PyBioShell) by calling a script
::
    python2.7 build_pybioshell-clang.py

**Note** See the ``main()`` function at the bottom of the script; the last four lines should look as follows:
::
    if __name__ == "__main__" :

      create_bindings()
      run_cmake()
    #  link_library()

i.e. the two first calls should be uncommented. After running, the script should fail;
edit ``bioshell/bindings/std/locale_classes.cpp`` file and remove 
::
    #include <bits/exception_ptr.h> // std::__exception_ptr::exception_ptr

line. After that comment the ``create_bindings()`` call and run the script again
