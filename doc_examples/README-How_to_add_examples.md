How to add a new bioshell example to doc/examples set:

1) Enter the right directory, relevant for you test
 - if you test a public application, enter doc/examples/apps/
 - if you test bioshell library, enter a directory corresponding to the second-highest level of namespace hierarchy, e.g. when you test core/calc/statistics/BivariateNormal class, the directory will be core/calc/
 - if you test biosimulations library, currently there are two options: either cabs or surpass

2) create a main directory of the test

mkdir app_name

Notes:
- if you have a ex_* executable : the directory name should be exactly the same, eg. mkdir ex_BivariateNormal, ex_Crmsd etc.
- if you test a standard public application (such as str_calc or surpass), the directory name should be app_name-some-info-on-the-test, eg. mkdir str_calc-contact_maps

3) create the directory structure
mkdir expected_outputs test_inputs outputs_from_test

4a) Bring the executable - the case of ex_* tests:
  - open CMakeLists-bioshell.txt or CMakeLists-biosimulations.txt file, relevant to the example
  - put **just the name** (without any path) of the example to the right section
  - compile it, i.e. go to the build directory, and:

  ```
  cmake ..
  make ex_myexample
  ```

  This will automatically create a symbolic link in the example's directory

4b) Bring the executable  - the case of standard apps
Just make a symbolic link **using relative path**, e.g.
```
ln -s ../../../../bin/seqc ./
```

5) Link input files
Create a symbolic link of every input file you need for your test. Input data should be stored somewhere in
```doc/examples/inputs``` Again, remember to use **relative paths**

6) Create a runme.sh script
Prepare runme.sh bash script that runs your app, providing the relevant input files. Do not rely on your environment, in the case of other users it may be different. Most importantly, export BIOSHELL_DATA_DIR shell variable. The script should copy all resulting files to outputs_from_test directory

7) Check whether the output files (stored now in outputs_from_test) are OK. Also, double check the output printed on stdout

8) If all is OK, move the output files from outputs_from_test to expected_outputs.
Also, run the runme.sh script again, now storing the stdout in expected_outputs/stdout.txt file

9) Commit your test!

