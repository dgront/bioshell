Welcome to BioShell package documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Overview
   :name: overview
   :hidden:

   doc/introduction
   doc/installation
   doc/pybioshell-installation

.. toctree::
   :maxdepth: 2
   :caption: BioShell
   :name: bioshell
   :hidden:

   api/index
   doc/bioshell-programs
   doc/bioshell-examples-index
   doc/bioshell-library
   doc/bioshell-pylibrary

   
These pages provide documentation for BioShell package. Api documentation is given `here <api/index.html>`_ To answer most common questions, we have a list of shortcuts below:

* **Description of BioShell package components**:
  :doc:`doc/introduction`

* **Overview of BioShell functionality**:
  :doc:`doc/bioshell-by-function`

* **How to install BioShell package**:
  :doc:`doc/installation`

* **How to install PyBioShell package (Python bindings to BioShell)**:
  :doc:`doc/pybioshell-installation`


Our laboratory protocols, both  related to BioShell and Rosetta, are provided on our `labnotes website <https://labnotes.readthedocs.io/en/latest/>`_.

   
.. toctree::
   :maxdepth: 2
   :caption: BioSimulations
   :name: biosimulations
   :hidden:

   doc/surpass-model-toc

.. toctree::
   :maxdepth: 1
   :caption: For developers
   :name: for-developers
   :hidden:

   doc/for_developers
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
