# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# This file does only contain a selection of the most common options. For a
# full list see the documentation:
# http://www.sphinx-doc.org/en/master/config

import os, sys, glob, shutil					# Python modules to do stuff

import sphinx_rtd_theme					        # ReadTheDocs theme must be installed as Python module
sys.path.extend(["../../../","../", os.getcwd()])			# Paths to BioShell maintenance scripts
from scripts.utils import execute			    # import functions from BioShell scripts
from find_categories_and_keywords import create_rst

import setup                                    # import the local setup.py file to control the build process

# ========================  program configuration ============================
# --- Here we check for "readthedocs" word in the path - this is the only known way
# --- to test whether this documentation is run locally or on ReadTheDocs website.
RUN_ON_RTD = True if "readthedocs" in os.getcwd() else False
CONFIG     = setup.sphinx_setup()

# -- Project information used by Sphinx -----------------------------------------------------

project = 'BioShell 3.0'
copyright = '2018, D. Gront et al.'
author = 'D. Gront et al.'
version = '3.0'				# The short X.Y version
release = '3.0'				# The full version, including alpha/beta/rc tags


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.autosummary',
    'sphinx.ext.autosectionlabel',
    'sphinx_rtd_theme',
]
html_theme = "sphinx_rtd_theme"
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

autosummary_generate = True

# -- Hooks --------------------------
# Ensure that the __init__ method gets documented.
def skip(app, what, name, obj, skip, options):
    if name == "__init__":
        return False
    return skip

def setup(app):
    # ---------- Create RST files for BioShell examples
    create_rst(RUN_ON_RTD)

    os.chdir("../")
    print("Current path "+os.getcwd())
    # ---------- Run doxygen if requested by a user
    if CONFIG.if_run_doxygen:
        execute("", '( cat doc_examples/Doxyfile ; echo "OUTPUT_DIRECTORY=api-dox" ) | ' + CONFIG.doxygen_binary + ' -')
    print("TUU3")

    # ---------- Run xrest if requested by a user
    if CONFIG.if_run_xrest:
        if CONFIG.if_clone_xrest:
            if os.path.exists("xrest") and os.path.isdir("xrest"):
                shutil.rmtree("xrest")
            execute("","git clone https://jkrys@bitbucket.org/dgront/xrest.git")
        execute("","python3 xrest/xrest.py -i api-dox/ -o doc_examples/sphinx_docs/source/api/ --rst")
        os.chdir("doc_examples/")

    app.connect("autodoc-skip-member", skip)
#    app.add_javascript('brython.js')
    #execute("","cp -r ../api ../checkouts/latest/")
    app.add_javascript("https://www.googletagmanager.com/gtag/js?id=UA-136699555-1")
    app.add_javascript("google_analytics_tracker.js")
    app.add_stylesheet('css/custom.css')

autoclass_content = 'both'

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = None

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path .
exclude_patterns = []

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
#html_theme = 'alabaster'
path = os.getcwd()
if "readthedocs" in path:  # only import and set the theme if we're building docs locally
  import sphinx_rtd_theme
  html_theme = 'sphinx_rtd_theme'
  html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
# html_theme_options = {}

#html_logo = "logo-v3.svg"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_css_files = ['css/default.css']

# -- Options for HTMLHelp output ---------------------------------------------
htmlhelp_basename = 'bioshelldoc'           # Output file base name for HTML help builder.

