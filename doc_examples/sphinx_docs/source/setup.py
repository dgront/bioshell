"""Configuration for Sphinx that is used to build documentation

Functions of this file are called by conf.py to setup paths and options
"""

class Config:
    """Object of this class will be returned to provide config entries"""
    def __init__(self, **entries): self.__dict__.update(entries)

    def __str__(self):
        return str(vars(self))


def dgront_setup():
    cfg = {
        "if_run_doxygen": False,
        "if_clone_xrest": True,
        "if_run_xrest": True,
        "doxygen_binary": "/Users/dgront/bin/Doxygen.app/Contents/Resources/doxygen",
    }

    return Config(**cfg)


def read_the_docs_setup():
    """This is the global setup that is used by ReadTheDocs website to build BioShell documentation"""
    cfg = {
        "if_run_doxygen": True,
        "if_clone_xrest": True,
        "if_run_xrest": True,
        "doxygen_binary": "doxygen",
    }

    return Config(**cfg)


def sphinx_setup():
    """This function is called from conf.py to configure Sphinx.

    Here one can set up the appropriate config, just by calling the relevant function"""
    #return dgront_setup()
    return read_the_docs_setup()


if __name__ == "__main__":
    cfg = sphinx_setup()
    print(cfg)
