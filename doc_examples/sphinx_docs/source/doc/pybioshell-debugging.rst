.. _doc_pybioshell-pdb_io:

Help! My script crashes!
-----------------------------

Getting more logs from BioShell library
""""""""""""""""""""""""""""""""""""""""
There are nine levels of importance for log messages reported by BioShell methods. Seven of them are used for *general* reporting, in the order of decreasing importance: `CRITICAL`, `SEVERE`, `WARNING`, INFO, FINE, FINER and FINEST. The two additional levels: HTTP and FILE are used to report HTTP messages and information about disk I/O, respectively. By default, loggging level is set to `INFO` which means that only
`INFO` and more important messages show up. To see more logs, increase the verbosity as follows:
  
 .. code-block:: python

   from pybioshell.utils import LogManager
   LogManager.FINEST()

Checking C++ excpetion from Python
""""""""""""""""""""""""""""""""""""""""
Occasionally PyBioShell library throws an exception, which stops a Python script. To find out the reason, wrap a bioshell call into a **try / except** block and print out execution information as below:

  
 .. code-block:: python

   try: 
     pdb = find_pdb(pdb_fname, path, True, False)
   except:
     sys.stderr.write(str(sys.exc_info()[0])+" "+str(sys.exc_info()[1]))


