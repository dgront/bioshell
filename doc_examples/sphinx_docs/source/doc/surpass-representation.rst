.. _doc_surpass_representation:

SURPASS representation
======================


SURPASS is a new coarse-grained model of protein structure. Deep reduction of the number of atoms 
in the representation results in a powerful computational speed-up and in this context 
ranks the model as a low resolution. 

.. > [_Figure 1. Schematic illustration of the projection from all-atom to coarse-grained SURPASS representation for two protein fragments: helical (left side) and β-strand (right side). Panel A illustrates an idealized “ribbon diagram”, while panel B represents the all-atom structure of example fragments. Panel C shows the corresponding α-carbon chains. Panel D illustrates the idea of the four-residue averaging of alpha carbon positions, leading to the SURPASS pseudoresidue representation. The resulting SURPASS chains (green for helical fragments and blue for the beta strand) superimposed onto the original alpha-carbon traces are shown in the last row (panel E). Note the different radii of the pseudoresidues representing helical and beta fragments reflecting different thicknesses of such fragments in real (atomistic) structures.]

The number of pseudoresidues present in the modeled system corresponds to polipeptide chain size and is equal to N-3,
where N is the number of amino acids in the sequence. The positions of pseudo residues are defined by averaging
the coordinates of short secondary structure fragments. These fragments are replaced by a single center of interactions.
The choice of four residue averaging is crucial for the local geometry of the model because leads to an almost linear
shape of the SURPASS fragments representing helices or beta strands.

The SURPASS representation assumes three types of pseudo atoms depending on secondary structure assignment of the averaged 
fragments of protein structure:

- pseudo atom H (helix-like) for helical (HHHH) or almost helical (HHHC, CHHH) fragments

- pseudo atom S (like β-strand) representing centers of mass of EEEE, EEEC or CEEE, fragments

- pseudo atom C (coil-like) for all remaining secondary structure combinations (H, E and C)

.. > [_Figure 2. Visualization of protein structure (PDB code: 2gb1, chain A). Upper panel: all-atom representation by “ribbon diagram”; lower panel: SURPASS “ball-and-stick” model. Green – α-helix; cyan – loops; blue – β-sheet. definition of secondary structure assignment in the SURPASS model lead to a slight shortening (~ 2.0Å) of both helices and β-strands, and the proportional elongation of the loop fragments. A helical fragments of SURPASS pseudo atoms have a form of straight, rigid and tightly packed stick. United atoms for β-strands represent centers of more separated a-carbons. Therefore the distance between two consecutive pseudo residues type S or C is almost two times longer than such distance in helical fragments.]

  