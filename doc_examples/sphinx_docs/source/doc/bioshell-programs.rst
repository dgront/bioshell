.. _doc_bioshell_programs:

BioShell programs
=================

Currently, BioShell distribution provides the following programs:

**seqc** (:ref:`cookbook recipes <seqc_recipes>`):
      sequence converter : a utility to convert between sequence data formats

**strc** (:ref:`cookbook recipes <strc_recipes>`):
      structure converter : a utility to work with PDB files

**str_calc** (:ref:`cookbook recipes <str_calc_recipes>`):
      structure calculator; perform various calculations on a PDB file

**clust** (:ref:`cookbook recipes <clust_recipes>`):
      calculates hierarchical clustering of arbitrary objects based on a map of pairwise distances between them

**hist** (:ref:`cookbook recipes <hist_recipes>`):
      simple utulity to make 1D and 2D histograms

Now you can browse :ref:`BioShell cookbook<doc_bioshell_cookbook>`, or read tutorials, listed below

.. toctree::
  :maxdepth: 2
  :caption: Tutorials

  bioshell-tutorial-clust

.. toctree::
  :maxdepth: 2
  :caption: Cookbooks

  bioshell-cookbook
