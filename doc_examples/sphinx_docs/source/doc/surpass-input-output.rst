.. _doc_biosimulations_surpass_input:

Input files
-----------

Secondary structure profile file (``*.ss2`` file format) is the only mandatory
input to the program. Example input file for 2GB1 protein can be :download:`found here <../_static/2gb1.dssp.ss2>`.
Optionally, a starting conformation (in the PDB format) may be provided with ``-model::pdb`` flag.
    
`Please note` that these input files must be in all-residues representation, even though SURPASS models are shorter by 3 residues 

An input SS2 file may be conveniently generated from a PDB file as long as it contains secondary structure information in its header.
The following command uses ``seqc`` program of BioShell package:

.. code-block:: bash
    
   seqc -in::pdb=2gb1.pdb -select:chains=A -in:pdb:header -out:ss2 

.. _doc_biosimulations_surpass_output:

Output files
------------
After every *outer cycle* (see options below), **surpass_annealing** makes an observation of the current state of the
simulated system. Typically this means observing energy of the system, various evaluators, topology of a protein,
and the coordinates in ``.pdb`` file format.

``energy.dat``
    The file provides energy components for every observed frame

``movers.dat``
    The file provides movers acceptance ratio and range

``observers.dat``
    provides various measurements for every observed frame, such as elapsed time, temperature, radius of gyration,
    crsmd, etc.

``topology.dat``
    The file topology footprint 

``trajectory.pdb``
    File contains coordinates of the system recorded at every observation event
    (file name may be changed with ``-out:pdb`` option)

