.. _doc_pybioshell-pdb_io:

Reading and writing PDB files
-----------------------------

Reading PDB files
"""""""""""""""""""
Reading PDB data is a two-stage process. First you create a reader that loads PDB content into memory:

.. code-block:: python

   pdb = Pdb(pdb_code,"",False)

First argument is a string which is a path to a PDB file. Second is a string which represents a ``PdbLineFilter`` object eg. ``"is_ca"`` - reader will read only CA atoms or ``"is_not_water"`` - will read everything what is not water. In general, filtering PDB lines may considerably speed up loading time. Third argument is a flag whether reader should read header of a PDB file. Header is neccesary to read some additional information eg. about secondary structure, connectivity (CONNECT fields), etc.

Then the content is parsed according to user's requests. In the example below the FASTA string is printed out.

.. code-block:: python
  :linenos:

  structure = pdb.create_structure(0)
  for ic in range(structure.count_chains()) :
    chain = structure[ic]
    print(">",structure.code(), chain.id())
    print(chain.create_sequence().sequence)



Writing PDB files
"""""""""""""""""""""

PdbAtom class provides ``create_pdb_line()`` method. In the following example four nested loop iterate over models, chains, residues and (finally) atoms; 

.. code-block:: python
  :linenos:

  for i_model in range(pdb.count_models()) :
    structure = pdb.create_structure(i_model)
    for ic in range(structure.count_chains()):
      chain = structure[ic]
      for ir in range(chain.count_residues()):
        resid = chain[ir]
        for ia in range(resid.count_atoms()):
          if resid[ia].atom_name() == "CA" or resid[ia].atom_name() == "CB":
        	print(resid[ia].to_pdb_line())

Also, ``pybioshell.core.data.io`` module contains ``write_pdb()`` method which writes in example below model no. 5 of a ``structure`` object (**note**: models count from 0) to a file with a given name. Existing files will be overwriten.

.. code-block:: python
  :linenos:

  reader = Pdb(pdb_fname,"is_ca",False)
    structure = reader.create_structure(0)
    write_pdb(structure, out_fname, 5)
