.. _doc_installation:

Installation
============
This document describes, how to install binary programs of BioShell toolkit. See :ref:`PyBioShell Installation page<doc_pybioshell_installation>` for instruction regarding Python bindings.

**BioShell** package has been written in C++11 and must be built before use. This is a quite easy process, which
requires CMake (https://cmake.org) and a relatively modern C++ compiler such as gcc 5.0 or clang 10.0

Just follow the steps below to compile the package:

| :ref:`1. Install zlib <install>`
| :ref:`2. Clone bioshell <clone>`
| :ref:`3. Run CMake <run_cmake>`
| :ref:`4. Run Make <run_make>`
| :ref:`5. Set BIOSHELL_DATA_DIR path <set_data_path>`
|	

The two additional sections below provide more information on customization of the building process:

|
| :ref:`6. Additional parameters for compilation <parameters>`
| :ref:`7. Using IDE <ide>`
|


.. _install:
.. rubric:: 1. Install ``zlib``

BioShell requires zlib library so it can handle compressed files. You must install developer version 
of the library to be able to compile BioShell. On Ubuntu linux it can be installed by the command:

.. code-block:: console
	    
  sudo apt-get install zlib1g-dev


.. _clone:
.. rubric:: 2. Clone BioShell

If you haven't done it yet, **clone bioshell repository** (https://bitbucket.org/dgront/bioshell/src/master/)
from Bitbucket:

.. code-block:: bash
    
  git clone https://bitbucket.org/dgront/bioshell.git
  cd bioshell
    
This should create ``bioshell`` directory in your current location. The second line steps into this new directory

.. rubric:: 2.1 Clone submodules for Bioshell

Now Bioshell package contains submodules to use machine learning. Update neccecary submodules with this command:

.. code-block:: bash
    
  git submodule update --init
    

Submodules will be downloaded to ``external/`` directory in bioshell repository.

.. _run_cmake:
.. rubric:: 3. **Run CMake**:

.. code-block:: bash
	    
  cd build
  cmake ..
            
The ``build`` directory will contain compilation intermediate files and may be deleted once BioShell is compiled.
The first line enters that direcotry, the second command calls ``cmake`` to set up the compilation process. CMake
attempts to set up everything automatically, sometimes however it would require some guidance, e.g. to find
the right compiler (see below)

.. _run_make:	    
.. rubric:: 4. **Run Make**:

.. code-block:: bash
	    
  make -j 4
	    
where ``-j 4`` allows make use 4 cores to run parallel compilations. This command will attempt to compile **all targets**; the list of all targets can be printed by ``make help``. As one can see, each executable is a separate target. There are also predefined group targets:

.. glossary::

  bioshell
    compiles only *bioshell* library

  bioshell-apps
    compiles *bioshell* library and bioshell toolkit applications, such as **seqc** and **strc**

  examples
    compiles all examples, i.e. all **ap_** and **ex_** application 

.. _set_data_path:

.. rubric:: 5. **Set BIOSHELL_DATA_DIR path**

Last step is to add path to ``data/`` directory to your shell variables e.g. 

.. code-block:: bash

  export BIOSHELL_DATA_DIR="/Users/username/bioshell/data"

or add this variable to your ``~/.bashrc``:

.. code-block:: bash

  echo 'export BIOSHELL_DATA_DIR="/Users/username/bioshell/data" ' >> ~/.bashrc


.. _parameters:
.. rubric:: 6.  **Additional parameters for compilation**

The procedure described above compiles the package with the default settings: *Release* build with no profiling. To change
it, you should  **remove everything from** ``./build`` **directory** and generate new makefiles with new settings:

- in order to use a compiler other that the default one (e.g. gcc version 4.9), say:

    .. code-block:: console

	cmake -DCMAKE_CXX_COMPILER=g++-4.9  -DCMAKE_C_COMPILER=gcc-4.9 -DCMAKE_BUILD_TYPE=Release ..

or to use ``icc`` for instance:

    .. code-block:: console

	cmake -DCMAKE_CXX_COMPILER=icc  -DCMAKE_C_COMPILER=icc -DCMAKE_BUILD_TYPE=Release ..

- to selecting a different compiler and making a profile build

  .. code-block:: console

        -DCMAKE_CXX_COMPILER=icc -DCMAKE_C_COMPILER=icc -D PROFILE=ON -DCMAKE_BUILD_TYPE=Release ..

- to brew a **debug** build, turn ``-DCMAKE_BUILD_TYPE=Release`` into ``-DCMAKE_BUILD_TYPE=Debug``. 
  So to make a **debug** build **without changing the compiler**, say just:

    .. code-block:: console

	cmake -DCMAKE_BUILD_TYPE=Debug ..

- to make a profiling build (-pg option) for gcc or *Xcode Instruments*  add ``-D PROFILE=ON`` to the ``cmake`` command (the custom ``PROFILE`` variable test is implemented in the main ``CMakeLists.txt``).

    .. code-block:: console

        cmake -D PROFILE=ON ..

.. _ide:
.. rubric:: 7. **Using IDE**

In the above examples, ``cmake`` was used to produce makefiles for to compile BioShell. 
``cmake``  command may be also used to generate project files for other environments, in particular:

- to produce \*.xcodeproj file for xcode:

    .. code-block:: console

	cmake  -DCMAKE_BUILD_TYPE=Release -G Xcode 

- or to prepare *solution* files for Microsoft Visual Studio (must be run on a Windows machine):

    .. code-block:: console

	cmake  -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 2013" 





