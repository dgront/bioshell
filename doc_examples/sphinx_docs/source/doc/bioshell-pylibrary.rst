.. _doc_bioshell_pylibrary:

BioShell Python library
=======================

BioShell 3.0 comes also with Python bindings i.e. BioShell classes can be also used as Python modules. Let's consider the following C++ program that reads a PDB file and creates a ``Structure`` object that represents a biomacromolecular complex. Then it writes a FASTA sequence for every chain in the structure:

.. literalinclude:: ../../../cc-examples/core/data/ex_pdb_to_fasta/ex_pdb_to_fasta.cc
  :language: c++
  :linenos: 
  :lines: 1-7,29-30,34-

The same program written in Pyton looks much simpler. It calls nearly the same BioShell C++ objects as the one above, but due to simplicity of Python, the script is a bit shorter:

.. literalinclude:: ../../../py-examples/core/data/pdb_to_fasta/pdb_to_fasta.py
  :language: python
  :linenos: 
  :lines: 1-4,26-


.. toctree::
  :maxdepth: 1
  :caption: PyBioShell How-to

  pybioshell-pdb_io
  pybioshell-debugging
  pybioshell-pymol

