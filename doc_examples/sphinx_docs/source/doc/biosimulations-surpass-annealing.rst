.. _doc_biosimulations_surpass_annealing:

`surpass_annealing` program
---------------------------

You need just a ``.ss2`` file for your target protein to run the program. Provide it using ``-in:ss2`` command line option.
Other options are used to:

**specify starting conformation**

    .. option:: -model:random
    
       starts the simulation from a random chain (extended conformation) while 
    
    .. option:: -model:pdb
    
       provides an input starting conformation

**specify temperature set for simulated annealing run**

    ``-sample:t_start``, ``-sample:t_end`` and ``-sample:t_steps`` define a set of N+1 temperatures distributed uniformly
    between the starting and the ending temperature

specify the length of the simulation (Monte Carlo steps)
    ``-sample:mc_inner_cycles`` defines the amount of sampling `between` frames that are recorded
    ``sample:mc_cycle_factor`` makes every inner MC cycle longer (multiplying them by a given factor)
    ``-sample:mc_outer_cycles`` defines the number of frames recorded for every temperature value

    The total number of MC steps is then :math:`N_{temperatures} \times N_{inner} \times N_{factor} \times N_{outer}`
    
Example parameters for `ab-initio` simulation of a protein:

.. code-block:: console

    ./surpass_annealing -model:random \
        -in:ss2=test_inputs/2gb1A.ss2 \
        -sample:t_start=2.2 \
        -sample:t_end=0.9 \
        -sample:t_steps=15 \
        -sample:mc_outer_cycles=100 \
        -sample:mc_inner_cycles=10 \
        -sample:mc_cycle_factor=10 \
        -sample:perturb:range=0.7 

Example parameters to relax an input structure: 

.. code-block:: console

    ./surpass_annealing -model:pdb=2gb1A.pdb \
        -in:ss2=test_inputs/2gb1A.ss2 \
        -sample:t_start=2.2 \
        -sample:t_end=0.9 \
        -sample:t_steps=15 \
        -sample:mc_outer_cycles=100 \
        -sample:mc_inner_cycles=10 \
        -sample:mc_cycle_factor=10 \
        -sample:perturb:range=0.7 

