.. _doc_surpass_model_toc:

SURPASS model
=============


SURPASS model
    Single United Residue per Pre-Averaged Secondary Structure fragment
    is a coarse-grained low resolution model for protein simulations.

* **See**  :ref:`doc_surpass_representation`
        
* **Read about**:  :ref:`doc_surpass_force_field`
      
* **Necessary and optional**  :ref:`doc_biosimulations_surpass_input`
     
* **Resutling**  :ref:`doc_biosimulations_surpass_output`
    
* ``surpass_annealing`` command line :ref:`program <doc_biosimulations_surpass_annealing>`
    
.. toctree::  
  :maxdepth: 1  
  :hidden: 

  surpass-force-field
  surpass-input-output
  surpass-representation
  biosimulations-surpass-annealing
  