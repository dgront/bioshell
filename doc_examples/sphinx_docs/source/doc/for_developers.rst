.. _for_developers:

Notes for BioShell developers
=============================

Do you want to participate in the project? Have briliant idea what would be a cool extension? Or maybe you need a specific feature?

This page will help you with rolling your own copy of BioShell!

**1. Don't create branch, fork instead**
Make your own fork of BioShell repository

	    
**2. Work as usually**

**3. Merge with the upstream repository often**

Remember to sync your fork of the BioShell source tree to keep it up-to-date with the upstream repository. Use the command below:

.. code-block:: console

  git pull git@bitbucket.org:dgront/bioshell.git 
  
This will only update your local copy of the repository. Use ``git push`` to update your fork on BitBucket.

    
**4. Create a pull request**

When your work is done, you may contribute your changes to the main BioShell repository. Simply push your development branch to the forked remote repository and create the pull request. 