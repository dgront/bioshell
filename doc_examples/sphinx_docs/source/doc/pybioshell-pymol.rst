.. _doc_pybioshell-pymol:

Using PyBioShell in PyMOL
-----------------------------

PyBioShell can be loaded by a Python interpreter as any other library. This also applies to the interpreter
that is build in `PyMOL - a molecular visualization system`_.

Loading PyBioShell
"""""""""""""""""""

Load a  PyBioShell module by typing a respective command in a PyMOL command input area,
the same as you would use in a Python script. E.g. you can try the following:

.. code-block:: python

   from pybioshell.core import BioShellVersion
   print(BioShellVersion().to_string())

which should print information about your BioShell version, as you can see below:

.. image:: ../examples/pymol1.png

After a successful import, you can use any PyBioShell module inside PyMOL. But how to transfer data that is visible
in PyMOL 3D window to BioShell?

Loading PDB data from PyMOL
""""""""""""""""""""""""""""""

Fortunately PyMOL can export a desired part of a 3D view in a PDB format, which can be directly parsed
by ``core.data.io.Pdb`` reader, e.g:

.. code-block:: python

   from pybioshell.core.data.io import Pdb
   cmd.fetch("2gb1")
   pdb_txt = cmd.get_pdbstr('all')
   pdb = Pdb(pdb_txt, "")
   strstr = pdb.create_structure(0)

The detailed description how to read in and process PDB data is :ref:`given here<doc_pybioshell-pdb_io>`


.. rubric::
  References

.. target-notes::

.. _`PyMOL - a molecular visualization system` : https://pymol.org/
