.. _bioshell_examples_index:

BioShell examples
=================

There are three groups of examples for BioShell library: `ap_*` which are functional applications and are helpful for every user, `ex_*` show how to use a particular BioShell class or function in your own C++ program. Finally Python scripts (`*.py` files) show how to solve bioinformatics problems using PyBioShell. You can automatically run these test on your local machine. Use 

.. code-block:: console

  python3 call_all_tests.py

in your ``bioshell/doc_examples/cc-examples`` directory to run `ap_*` and `ex_*` or in ``bioshell/doc_examples/py-examples`` directory to run `*.py` scripts. You will find ``test_results.html`` in either ``cc-examples`` or ``py-examples`` directory, which you can open with your web browser to see the test results summary.

Overall there is more than 200 examples than can be accessed by the index pages below: 

.. toctree::
   :maxdepth: 1
   
   bioshell-examples

Alphabethical list of all BioShell examples grouped by `ap_*` `ex_*` and `*.py` category. 

.. toctree::
   :maxdepth: 1

   bioshell-by-function

BioShell `ap_*` examples grouped by their functionality.


.. toctree::
   :maxdepth: 1

   examples_by_keywords

BioShell examples comming from all the three `ap_*` `ex_*` and `*.py` categories sorted by keywords. Typically an example has more than one keyword assigned and thus appears more than once on the list.