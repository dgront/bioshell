.. _doc_pybioshell_installation:

PyBioShell Installation
=======================

PyBioShell is a set of Python bindings to **BioShell** library. It allows use of BioShell classes like any other Python modules.
The closest tool similar by functionality is Biopython, which however is partially written in Python.

The easiest option to get PyBioShell on your machine is to download precombiled library, available for the following Python versions. Click on an appropriate link below:

* `python 3.7 <http://bioshell.pl/~jkrys/pybioshell/pybioshell37/pybioshell.so>`_

* `python 3.6 <http://bioshell.pl/~jkrys/pybioshell/pybioshell36/pybioshell.so>`_

* `python 3.5 <http://bioshell.pl/~jkrys/pybioshell/pybioshell35/pybioshell.so>`_

* `python 2.7 <http://bioshell.pl/~jkrys/pybioshell/pybioshell27/pybioshell.so>`_

or type this command in your terminal:

.. code-block:: bash
    
  curl -O http://bioshell.pl/~jkrys/pybioshell/pybioshell37/pybioshell.so

You also need ``data/`` directory, which contains files necessary to run BioShell. Download `data.tar.gz <http://bioshell.pl/downloads/bioshell/data.tar.gz>`_ , uncompress it and put it somewhere BioShell will be able to find it, see  :ref:`here <set_data_path>` for details.

**Remember** to add path with ``pybioshell.so`` to your shell variables e.g. 

.. code-block:: bash

  export PYTHONPATH="$PYTHONPATH:$HOME/bioshell/bin"

or add this variable to your ``~/.bashrc``:

.. code-block:: bash

  echo 'export PYTHONPATH="$PYTHONPATH:$HOME/bioshell/bin" ' >> ~/.bashrc

**Remember also** to add ``data/`` directory to your shell variables. Look :ref:`here <set_data_path>` for details.

Another way is to compile it from sources, following the steps given below. The procedure assumes your ``bioshell`` repository is located in ``src.git/bioshell/`` and ``binder`` in ``src.git/binder/``; these paths are arbitrary but the commands must be adjusted accordingly.


0. Prequisities
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In order to compile ``binder``, you need to have ``Ninja`` building tool (`website <https://ninja-build.org/>`_) and cmake.  You will also need python headers,
available from ``python-dev`` package or similar (e.g. ``python3.5-dev``). On Ubuntu Linux you can install them with ``apt-get``:

.. code-block:: bash
    
  sudo apt-get install ninja-build  cmake python-dev

The use of ``clang`` compiler is advised. Try to get ``clang-6.0`` or newer (see `this link <https://blog.kowalczyk.info/article/k/how-to-install-latest-clang-6.0-on-ubuntu-16.04-xenial-wsl.html>`_)


1. Clone and compile ``binder``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To clone binder from its *github* repository:

.. code-block:: bash

  git clone https://github.com/RosettaCommons/binder
  cd binder
  python3 ./build.py -j 4


where the last command actually builds *binder* using four CPU cores for that. Note, that *binder* uses more than 1GB of disc space and its compilation may take a few hours.

2. Build PyBioShell
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Open ``scripts/build_pybioshell.py`` file and edit variables, adapting it to your system. In particular, you most likely have to fix
``clang++`` version (``LINKER_CMD`` variable) as well as the path where the ``binder`` executable is located (``BINDER_PATH`` variable)
Make a directory ``build_bindings`` in the main BioShell directory, i.e in the directory where ``pybioshell.config`` is located.
Choose your Python version and run the compilation as follow:

.. code-block:: bash

  python3 ./scripts/build_pybioshell.py -v 3.5

You should find your compiled version in ``bin/pybioshell.so``. If you have any problems with compilation, please do not hesitate to contact us.
