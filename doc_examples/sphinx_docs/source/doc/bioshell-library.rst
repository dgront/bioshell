.. _doc_bioshell_library:

BioShell C++ library
====================

BioShell is a versatile C++11 library for structural bioinformatics. Its struture has been shown in the figure below:

.. image:: ../tex/bioshell3.png
   :scale: 50%
   :alt: BioShell library structure
   :align: center

See the `API documentation <http://bioshell.pl/downloads/documentation/>`_ generated with Doxygen.

.. toctree::
  :maxdepth: 1
  :caption: Tutorials
  :name: Tutorials

  bioshell-tutorial-read_pdb
