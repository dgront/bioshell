#! /usr/bin/env python3

""" WARNING This script was changed on 26.04.2019 
Now creates separate .rst file for every ap_ ex_ and ww_ example 
Creates also bioshell-examples.rst file
Script creates these files in the right place in a documentation source
Must be run in doc_exammples/cc-examples/ and files goes to OUT_PATH
"""
import glob, re, sys, argparse, os, time

keywords_to_examples={}
examples_by_function={}
#pwd = os.getcwd()
#os.chdir("../../")

#OUT_PATH = glob.glob("**/source",recursive=True)[0]

#EXAMPLES_PATH = OUT_PATH +"/examples/"

PY_RST_TEMPLATE = """
{EXAMPLE_TITLE}
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


{EXAMPLE_INFO}

.. rubric:: Keywords:

{KEYWORDS}

.. rubric:: Categories:

{CATEGORIES}

{INPUTS}

{OUTPUTS}

.. rubric:: Program source:

.. literalinclude:: ../../../{EXAMPLE_PATH}
   :language: {LANGUAGE}
   :linenos:

.. image:: {EXAMPLE_IMAGE}
"""

RST_TEMPLATE = """
{EXAMPLE_TITLE}
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


    {EXAMPLE_INFO}

.. rubric:: Keywords:

{KEYWORDS}

.. rubric:: Categories:

{CATEGORIES}

.. image:: {EXAMPLE_IMAGE}
"""

#os.chdir(pwd)

def get_categories(s):
    """Parsuje linijkę z kategoriami, zwraca listę."""
    tmp = re.split(": ", s)[1:]
    return [i.strip().strip(";") for i in tmp]


def get_keywords(s):
    """Parsuje linijkę ze słowami kluczowymi, zwraca listę"""
    tmp = re.split(": ", s)[1].strip().split(';')
    return [i.strip() for i in tmp]


def get_description(s):
    """Parsuje linijkę z opisem, zwraca string"""
    tmp = s[10:].strip()
    return tmp


def get_image(s):
    """Parsuje linijkę z nazwą ilustracji, zwraca listę"""
    #    tmp = s[8:].strip()  # usuń pierwsze 8 znaków, usuń otaczające białe znaki
    tmp = s.split(':')[1].strip()
    return [tmp]


def get_image_alt(s):
    """Parsuje linijkę z nazwą ilustracji, zwraca listę"""
    tmp = s.split(':')[1].strip()
    return [tmp]

def add_bash_listing(msg, *quoted_blocks) :

    out = ""
    line_to_skip = ""
    block_started = False
    for line in msg.splitlines():
        for b in quoted_blocks:
          if line.startswith(b) :
            line_to_skip = line
            out += "\n\n"+line+"\n\n.. code-block:: bash\n\n"
            block_started = True
        #if to only not add the same line with something in quoted_blocks one more time
        #if line.strip(":") in quoted_blocks or line.strip(":").split(" ")[0] in quoted_blocks: continue    
        if line == line_to_skip : continue
        if len(line) == 0 : block_started = False
        if line.startswith("REF"):
            out+=line+"\n"
            continue
        out += " " + line if block_started else line 
        out += "\n"
    return out


# --- Read a whole file into a string
def read_file(fname):
    with open(fname, 'r') as a_file: return a_file.read()


def get_categories_and_keywords(filee,py_flag):
    #print(filee)
    """Bierze plik i zwraca listy kategorii i słów kluczowych"""
    # To są flagi, które sprawdzamy po to by stwierdzić, że dalej już nie musimy przeglądać tego pliku
    # Zakładamy, że w jednym pliku kategorie i słowa kluczowe pojawiają się tylko raz, jeśli więcej to trzeba poprawić.
    description_in_file = ""
    description_string = ""
    categories_in_file = ["no_categories"]
    keywords_in_file = ["no_keywords"]
    image_in_file = ["file_icon.png"]
    image_alt_in_file = ["no_alt_img"]
    functions = []
    # zbieramy nasze kategorie i słowa kluczowe
    if py_flag:
        my_file = filee.read()
        #print(my_file)
        p= re.compile('""".*"""',flags=re.MULTILINE|re.DOTALL)
        match = p.search(my_file)
        description_in_file = match.group(0)[:-10].strip('"""')
        my_file = my_file.split("\n")
    
    if not py_flag:
        my_file=filee.readlines()
        #print(my_file)
        # Here we remove '*' characters and look for  `program_info` string
        lines = [l.strip().strip('*') for l in my_file]
        txt = '\n'.join(lines)
        rx = re.compile(r"std::string\s+program_info\s+=\s+R\"\((.*)\)\";", re.M | re.S)
        match = rx.search(txt)
        if match : description_string = match.group(1)
        #print(description_string)
    
    #print(my_file)

    for line in my_file:
        #print(line)
        line = line.strip()
        if "@brief" in line:
            description_in_file = get_description(line)
        elif "CATEGORIES" in line:
            categories_in_file = get_categories(line)
            if py_flag:
                p1= re.compile('""".*CATEGORIES',flags=re.MULTILINE|re.DOTALL)
                #lines = [l for l in my_file]
                match = p1.search("\n".join(my_file))
                description_in_file = match.group(0)[:-10].strip('"""')
        elif "KEYWORDS" in line:
            keywords_in_file = get_keywords(line)
        elif "GROUP" in line:
            functions = get_keywords(line)[-1] if get_keywords(line)[-1]!="" else get_keywords(line)[0]
        elif "IMG:" in line:
            image_in_file = get_image(line)
        elif "IMG_ALT" in line:
            image_alt_in_file = get_image_alt(line)
        #if "main" or "sys.exit" in line:
        #    break
    if not py_flag:
        if len(description_string) > 0 : description_in_file = description_string
    #print(description_in_file, categories_in_file, keywords_in_file, image_in_file, image_alt_in_file,functions)
    return description_in_file, categories_in_file, keywords_in_file, image_in_file, image_alt_in_file,functions


def list_links(filename):
    outputs=[]
    inputs=[]
    file=filename.split("/")[-1]
    in_out={'OUTPUTS':"""
.. rubric:: Output files:
""",
'INPUTS':"""
.. rubric:: Input files:
"""}
  #  print(len(in_out['INPUTS']))
   # print(len(in_out['OUTPUTS']))
    link_begin = 'https://bitbucket.org/dgront/bioshell/src/master/doc_examples/'
    output_path=filename[:-len(file)]+'expected_outputs/'
    input_path = filename[:-len(file)]+'test_inputs/'
    if os.path.exists(input_path): 
      for fi in os.listdir(input_path):
        if os.path.islink(input_path+fi): relative=os.readlink(input_path+fi).strip("./")
        else: relative=input_path+fi
        in_out['INPUTS']+="\n"+"""* {a}_ \n\n.. _{a}: {b}\n""".format(a=fi,b=link_begin+relative)
        #print(link_begin+relative.strip("./"))
   #   print(in_out['INPUTS'])
    if os.path.exists(output_path): 
      for fj in os.listdir(output_path):
        in_out['OUTPUTS']+="\n"+"""* {a}_ \n\n.. _{a}: {b}\n""".format(a=fj,b=link_begin+output_path+fj)
 #     print(in_out['OUTPUTS'])

    in_out['OUTPUTS']+="\n"
    in_out['INPUTS']+="\n"
    return in_out


def create_rst(rtd_flag):
    print("--rst argument used")
    py_flag=None
    pwd = os.getcwd()
    if rtd_flag:
      os.chdir("../../")
    else:
      os.chdir("../")

    OUT_PATH = glob.glob("**/source",recursive=True)[0]
#    print(OUT_PATH)
    EXAMPLES_PATH = OUT_PATH +"/examples/"
    fname = glob.glob("**/bioshell-examples.tmplt",recursive=True)[0]
    len_of_name = len(fname.split("/")[-1]) #length of filename to strip from a path
    path_to_save_rst = fname[:-len_of_name]
    print("Path to save rst ",path_to_save_rst)
 #   print(fname,"FNAME")
    # clear file from previous run
    f = open(fname, "r")
    tmp=f.read()
  #  print(tmp)
    f.close()

    cards = []
    BIOSHELL_PY_EXAMPLES_PATH = glob.glob("**/py-examples/core/",recursive=True)[0]
    BIOSHELL_CC_EXAMPLES_PATH = glob.glob("**/cc-examples/",recursive=True)[0]
    all_list = glob.glob(BIOSHELL_PY_EXAMPLES_PATH + '**/*.py', recursive=True)
    all_list.extend(glob.glob(BIOSHELL_CC_EXAMPLES_PATH + '**/*.cc', recursive=True))
  #  print(BIOSHELL_CC_EXAMPLES_PATH+'**/*.cc')
    for filename in all_list:
        plik = open(filename, 'r+')
        file = filename.split('/')[-1]
        #print("########")
        #print(filename)
        in_out=list_links(filename)
        if ".cc" in file: 
            py_flag=False
            file = file.split('.cc')[0]
        else:
            py_flag = True

        description_in_file, categories_in_file, keywords_in_file, image_in_file, image_alt_in_file,functions = get_categories_and_keywords(plik,py_flag)
        description_in_file = add_bash_listing(description_in_file, "USAGE", "EXAMPLE")
        w = """{}
===============================
description: {} 
keywords: {}
categories: {}
image: {}
""".format(file, description_in_file, keywords_in_file, categories_in_file, image_in_file)  # , image_alt_in_file)
        #print(w)
        subst = {"EXAMPLE_TITLE": file,
                 "EXAMPLE_INFO": str(description_in_file),
                 "KEYWORDS": "\n\n".join("""* :ref:`{}`""".format(kword) for kword in keywords_in_file),
                 "CATEGORIES": "\n\n".join("""* {}""".format(categ) for categ in categories_in_file),
                 "EXAMPLE_IMAGE": image_in_file[0],
                 "EXAMPLE_PATH": filename,
                 "INPUTS":"" if len(in_out['INPUTS'])<35 else in_out['INPUTS'],
                 "OUTPUTS":"" if len(in_out['OUTPUTS'])<35 else in_out['OUTPUTS'],
                 "LANGUAGE":"cpp" if not py_flag else "python"
                 }
        #with open(fname, "a") as out:
        to_write = PY_RST_TEMPLATE.format(**subst) 
        #print("write")
        if image_in_file[0] == "file_icon.png":
            cards.append((1, to_write,file))
        else:
            cards.append((0, to_write,file))

        for kat in keywords_in_file:
          if kat in keywords_to_examples:
            keywords_to_examples[kat].append(file)
          else:
            keywords_to_examples[kat]=[file]

        if len(functions)!=0:
          if functions in examples_by_function:
            examples_by_function[functions].append(file)
          else:
            examples_by_function[functions]=[file]

    examples_to_write={}

    func_name = glob.glob("**/bioshell-by-function.tmplt",recursive=True)[0]
    len_of_name = len(func_name.split("/")[-1]) #length of filename to strip from a path
    path_to_save_rst = func_name[:-len_of_name]
    print("Path to save rst function",path_to_save_rst)

    func = open(func_name,"r")
    func_tmp = func.read()
    func.close()


    #print(examples_by_function)
    functions_list=[]
    for fnc in examples_by_function:
        functions_list.append(fnc)
        examples_by_function[fnc].sort()
    functions_list.sort()
    for fnc in functions_list:
        to_wr = ""
        for i in examples_by_function[fnc]:
            to_wr+="  ../examples/"+i+"\n"
        examples_to_write[fnc]=to_wr

    #print(examples_to_write)

    fun_to_wr = func_tmp.format(**examples_to_write)


    with open(path_to_save_rst+ "bioshell-by-function.rst","w") as f:
      f.write(fun_to_wr)
      f.write("\nThis file has been automatically generated on "+  time.strftime("%b %d %Y %H:%M:%S"))
    #os.chdir(pwd)

    #with open(glob.glob("**/bioshell-by-function.rst",recursive=True)[0],"a") as f:
    #  f.write(fnc)

    cards.sort()
    #print(tmp)

    ap=''
    ex=''
    py=''

    for c in cards:
        if c[2].startswith("ap"):
            ap+="  ../examples/"+c[2]+"\n"
         #   print("c2", c[2])
            outf = open(EXAMPLES_PATH+c[2] + ".rst", "w")
            outf.write(c[1])
        elif c[2].startswith("ex"):
            ex+="  ../examples/"+c[2]+"\n"
            outf = open(EXAMPLES_PATH+c[2] + ".rst", "w")
            outf.write(c[1])
        else:
            py+="  ../examples/"+c[2]+"\n"
            outf = open(EXAMPLES_PATH+c[2] + ".rst", "w")
            outf.write(c[1])
        outf.close()

    sub={"EX":ex,"AP":ap,"PY":py}
    tmpok = tmp.format(**sub)
    
 #   print("PWD is ",os.getcwd())
    
    with open(path_to_save_rst+ "bioshell-examples.rst","w") as f:
      f.write(tmpok)
    #os.chdir(pwd)

    BEGIN="""
.. _doc_examples_by_keywords:

Examples by keywords
====================

"""

    ONEKEY="""
{KEY}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. toctree::
  :maxdepth: 1

{FILES}

"""
    fnames=''
    sorted_keyword=[]
    for k in keywords_to_examples:
        sorted_keyword.append(k)
        keywords_to_examples[k].sort()
    sorted_keyword.sort()
    print(os.getcwd())
    with open(path_to_save_rst+"examples_by_keywords.rst","w+") as ff:
      ff.write(BEGIN)
      for k in sorted_keyword:
        fnames=""
        if (len(keywords_to_examples[k])>2) and (k!="no_keywords"):
          for f in keywords_to_examples[k]:
            fnames+="  ../examples/"+f+"\n"
          d={'FILES':fnames,'KEY':k}
          to_write = ONEKEY.format(**d)
          ff.write(to_write)
        else: print("skipping keyword: ",k,"found in",keywords_to_examples[k])


      
# =============================== MAIN  ============================

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Skrypt służy do aktualizacji www ap_ i ex_')
    parser.add_argument('-html', '--html', help="Generuje galerię ap_ ex_ dla bioshell.pl ", action='store_true',
                        required=False)
    parser.add_argument('-rst', '--rst', help="Generuje ReStructuredText", action='store_true', required=False)
    parser.add_argument('-d', '--out_dir', help="path where output .rst files will be written", required=False)

    #print(parser.parse_args())
    parser = parser.parse_args()
    print(parser)

    if parser.out_dir : OUT_PATH = parser.out_dir
    if parser.rst: 
      create_rst(False) #to create .cc files
      #print(pwd)
      #os.chdir(pwd)

