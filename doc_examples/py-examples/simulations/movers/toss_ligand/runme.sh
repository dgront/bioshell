#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"


echo -e "#Running python3 toss_ligand.py  test_inputs/2m56-ref.pdb X"
python3 toss_ligand.py  test_inputs/2m56-ref.pdb X
