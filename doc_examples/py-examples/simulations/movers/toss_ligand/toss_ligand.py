import sys

from pybioshell.core.data.io import Pdb
from pybioshell.simulations.movers import LigandTossingMover_core_data_basic_Vec3_t as LigandTossingMoverVec3
from pybioshell.simulations.systems import AtomRange
from pybioshell.simulations.systems import ResidueChain_core_data_basic_Vec3_t as ResidueChainVec3
from pybioshell.simulations.sampling import AlwaysAccept
from pybioshell.simulations.observers.cartesian import PdbObserver_core_data_basic_Vec3_t as PdbObserver

if len(sys.argv) < 3 :
  print("""

Creates a multimodel PDB with random orientations of a ligand in respect to the protein.


USAGE:
    python3 toss_ligand.py input.pdb ligand_chain


EXAMPLE:
    python3 toss_ligand.py 2m56-ref.pdb X


CATEGORIES: simulations/movers/LigandTossingMover
KEYWORDS:   PDB input; ligand
IMG: ap_LigandTossingMover.png
  """)

  sys.exit()


struct = Pdb(sys.argv[1],"").create_structure(0)
system = ResidueChainVec3(struct)
moving = AtomRange(0,0)

moved_chain = sys.argv[2]
for ic in range(struct.count_chains()) :
  chain = struct[ic]
  if chain.id() != moved_chain : moving.first_atom += chain.count_atoms()
  else :
    moving.last_atom = moving.first_atom + chain.count_atoms() - 1
    break
  print(chain.id(),chain.count_atoms())

print(moving)
always_move =  AlwaysAccept()
output = PdbObserver(system,struct,"out.pdb")
mover  = LigandTossingMoverVec3(system, moving)
for i in range(50):
  mover.move(always_move)
  output.observe()
