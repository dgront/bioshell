from pybioshell.simulations.systems.ising import Ising2D_unsigned_char_unsigned_short_t as Ising2DBB
from pybioshell.simulations.movers.ising import SingleFlip2D_unsigned_char_unsigned_short_t
from pybioshell.simulations.movers.ising import WolffMove2D_unsigned_char_unsigned_short_t
from pybioshell.simulations.movers import MoversSet
from pybioshell.simulations.sampling import SimulatedAnnealing
from pybioshell.std import vector_float


# /* @brief Simple but fully functional Ising simulating program.
#  *
#  * Runs simulated annealing simulations for 100x100
#  *
#  * CATEGORIES: simulations::systems::ising::Ising2D
#  * KEYWORDS:   Mover;Simulated Annealing;Ising2D
#  */

# Simulation controlling variables
n_cols,n_rows = 10, 10                 # size of system

# Other settings necessary for the simulation
seed = 12345;                          # seed for rng
#core::calc::statistics::Random::seed(seed);

# Initializing the system
system = Ising2DBB(n_rows, n_cols)
system.initialize()   # Populate system with random spins

movers = MoversSet()
movers.add_mover( SingleFlip2D_unsigned_char_unsigned_short_t(system),system.count_spins())
movers.add_mover( WolffMove2D_unsigned_char_unsigned_short_t(system),int(system.count_spins()*0.2))

temperatures = vector_float([5,5])
sa = SimulatedAnnealing(movers,temperatures)
for i in range(system.count_spins()):
    print(system[i],end = " ")
print("\n")
for outer in range(10):
    for inner in range(10):
        sa.cycles(outer,inner)
        sa.run()
        print(system.calculate())
    for i in range(system.count_spins()):
        print(system[i],end = " ")
