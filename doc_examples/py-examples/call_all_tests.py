#!/usr/bin/env python3

# Python standard libraries
import os, sys, filecmp, difflib, time, traceback, subprocess

# os.system('head -n 26 test_results.html> test_results.html.tmp && mv test_results.html.tmp test_results.html')
w="""
<html>
<head> 
<title>Example tests results table</title>
<style>
th {
    color: white;
    background-color: #76D7C4;
}
table tr:nth-child(even) {
    background-color:#fff;
}
table tr:nth-child(odd) {
   background-color:#E8F8F5;
}
</style>
</head>
<body>
<table style="width:75%"; align="center">
  <col width="30%">
  <col width="40%">
  <col width="30%">
  <tr>
    <th>Example</th>
    <th>Result</th> 
    <th>Time [s]</th>
  </tr>
"""

f=open('test_results.html','w')
f.write(w)
f.close()

os.system('rm -r test_outputs/* > /dev/null 2>&1') # delete all previous tests files

outs_path = os.path.dirname(os.path.abspath(__file__)) + '/test_outputs' # aboslute path to test outputs dir

excl=['expected_outputs','test_inputs','inputs','outputs_from_test', 'test_outputs']
untested_tests=[line.rstrip('\n') for line in open('ignore_list')]
exclude=excl + untested_tests
bioshell_apps=['seq_clust' ,'trac' ,'alignc' ,'blast_analyse', 'hist', 'seq_align' ,'seqc', 'strc', 'str_calc', 'str_align','tasks']
HTML_table=difflib.HtmlDiff(wrapcolumn=70)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    
def get_test_name(): # get 1 parent dir and test subdir
    current_path = os.path.dirname(os.path.abspath(__file__))
    change_path = os.path.abspath(subdir)
    os.chdir(change_path)
    test_name = os.path.join(os.path.basename(os.path.abspath('..')),os.path.basename(os.path.abspath('.'))).split('/')[-1]
    return current_path,test_name

for subdir,dirs,files in os.walk('.', topdown=True):
    error_log=''
    smth_wrong_comparing = False
    PROGRAM_NAME=''
    total_time = 0.0
    error_path=''
   # print >>sys.stderr, bcolors.OKGREEN + "Entering:",subdir + bcolors.ENDC
    print(bcolors.OKGREEN + "Entering:",subdir + bcolors.ENDC, file=sys.stderr)
    dirs[:]=[d for d in dirs if d not in exclude]
    
    if os.listdir(subdir)==[]: # empty dir
        #print >>sys.stderr, bcolors.FAIL + "No files in directory: " + os.path.abspath(subdir) + bcolors.ENDC
        print(bcolors.FAIL + "No files in directory: " + os.path.abspath(subdir) + bcolors.ENDC, file = sys.stderr)
        error_log="No files in this directory"
        current_path, PROGRAM_NAME = get_test_name()
        os.chdir(current_path)

    elif len(files) == 0 or os.path.basename(subdir) == '.': continue # dir . or dir with subdirs but no files
         
    elif 'runme.sh' in files:
        
        current_path, PROGRAM_NAME = get_test_name()
        
        if not os.path.exists('%s/%s' %(outs_path, PROGRAM_NAME)):
            os.makedirs('%s/%s' %(outs_path, PROGRAM_NAME))
       
        start_time = time.time()
        subprocess.call(['/bin/bash','-i','-c','./runme.sh >>%s/%s/stdout.out 2>>%s/%s/stderr.out' %(outs_path, PROGRAM_NAME, outs_path, PROGRAM_NAME)])
        total_time += (time.time() - start_time)
        total_time = "%.4f" % round(total_time,4) # trailing zeros
        
        SegFault_check = 'grep "Segmentation fault" %s/%s/stderr.out'%(outs_path, PROGRAM_NAME)
        
        try:
            subprocess.check_output(SegFault_check, shell=True)
            grep_returncode = 0

        except subprocess.CalledProcessError as ex:
            grep_returncode = ex.returncode
        
        if grep_returncode == 0: # grep searching for SegmentationFault in stderr.out found it
            error_log='Segmentation fault'
            #print >>sys.stderr, bcolors.FAIL + 'Segmentation fault in: ' + subdir + bcolors.ENDC
            print( bcolors.FAIL + 'Segmentation fault in: ' + subdir + bcolors.ENDC,file=sys.stderr)
        err = open('%s/%s/test_errors.html' %(outs_path, PROGRAM_NAME), 'w')

        for filename in os.listdir('%s/%s/' %(outs_path, PROGRAM_NAME)): # note that we compare outputs from test with expected outputs, not the other way
            if filename != 'stderr.out' and filename != 'test_errors.html':
                
                g=os.path.join('expected_outputs', filename)
                h=os.path.join('%s/%s/' %(outs_path, PROGRAM_NAME), filename)

                try:
                    diff = filecmp.cmp(g,h)
                    if not diff:
                        smth_wrong_comparing = True
                        a=open(g).readlines()
                        b=open(h).readlines()
                        err.write('Files: ' + '%s/%s' %(PROGRAM_NAME, str(g)) + ' and test_outputs/%s/%s are not the same.'%(PROGRAM_NAME, filename) + '<br><br>')
                        c=HTML_table.make_file(a,b)
                        err.write(c)
                except OSError:
                    etype, value, tb = sys.exc_info()
                    err.write('Unable to compare files: ' + '%s/%s' %(PROGRAM_NAME, str(g)) + ' and test_outputs/%s/%s' %(PROGRAM_NAME, filename) + '<br>'+ traceback.format_exception(etype, value, tb)[3]  + '<br>' + ' '  + '<br>')
                    smth_wrong_comparing = True
        err.close()
        error_path = 'test_outputs/%s/test_errors.html' %PROGRAM_NAME # relative path
        os.chdir(current_path)

    else:
       # print >>sys.stderr, bcolors.FAIL + "No runme.sh in: " + subdir + bcolors.ENDC
        print(bcolors.FAIL + "No runme.sh in: " + subdir + bcolors.ENDC, file=sys.stderr)
        error_log='No runme.sh'
        current_path, PROGRAM_NAME = get_test_name()
        os.chdir(current_path)
        
    f=open('test_results.html','a')
    
    if len(error_log) == 0: # test was performed
         if not smth_wrong_comparing:
             f.write('<tr>\n<td>%s</td>\n<td><center>OK</center></td>\n<td><center>%s</center></td>\n</tr>\n' %(PROGRAM_NAME,total_time))
         else:
             f.write('<tr>\n<td>%s</td>\n<td><center><a href = %s >Check what is wrong<a/></center></td>\n<td><center>%s</center></td>\n</tr>\n' %(PROGRAM_NAME,error_path,total_time))
    else: # test was not performed, there was another issue
         f.write('<tr>\n<td>%s</td>\n<td><center>%s</center></td>\n<td><center>ERR</center></td>\n</tr>\n' %(PROGRAM_NAME,error_log))
    f.close()

f = open('test_results.html','a')
f.write('</table>\n</body>\n</html>')
f.close()
