#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
mkdir -p outputs_from_test
echo "#Running python3 ./score_rms_plot.py -f test_inputs/1pgxA-abinitio.fsc test_inputs/1pgxA-abinitio-bis.fsc"
python3 score_rms_plot.py -f test_inputs/1pgxA-abinitio.fsc test_inputs/1pgxA-abinitio-bis.fsc -x 0.0 15.0
