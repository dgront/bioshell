import sys,argparse

from pybioshell.core.data.io import find_pdb, read_scorefile

from os.path import expanduser, join
home_dir = expanduser("~")
# It is assumed  VisuaLife library is installed in your $HOME/src.git/visualife/src/ directory
sys.path.append(join(home_dir, "src.git/visualife/src/"))

from core.Plot import Plot
from core.SvgViewport import SvgViewport
from core.styles import ColorRGB, color_by_name

from pybioshell.core.calc.statistics import HistogramD4

if len(sys.argv) < 2 :
    print("""

Reads Rosetta scorefile and make an energy to rms plot.


USAGE:
    python3 score_rms_plot.py -f file1 file2 ... [-x from to -y from to]


EXAMPLE:
    python3 score_rms_plot.py -f 1pgx-abinitio.fsc 1pgx-abinitio-bis.fsc

CATEGORIES: core/data/io/read_scorefile
KEYWORDS:   Rosetta scorefile; every vs. rms plot
GROUP: Statistics
IMG: score_to_rms.svg

  """)
    sys.exit()

#-----------argument parsing
parser = argparse.ArgumentParser(description='Reads scorefile from Rosetta and prepares score to rms plot for all given files as a series in one picture')

parser.add_argument('-f', '--file', help="input .fsc file", nargs='+', required=True)
parser.add_argument('-x', '--x_range', help="range for X axis of a plot (two values: min max)", nargs=2, required=False)
parser.add_argument('-y', '--y_range', help="range for X axis of a plot (two values: min max)", nargs=2, required=False)
  
parser = parser.parse_args()


energy = []
rms = []
allenergy = []
allrms = []
s=""

#-----------filling lists with data from file
print("Plotting score vs. rms for:",end="")
for i in range(len(parser.file)):
  s+=" "+parser.file[i]

  p = read_scorefile(parser.file[i])
  
  en = p.column_index("score")
  xrms = p.column_index("rms")

  e = []
  r = []

  for row in p:
    e.append(float(row[en]))
    r.append(float(row[xrms]))

  energy.append(e)
  rms.append(r)
  allenergy.extend(e)
  allrms.extend(r)

#-----------plotting energy plot

xfrom = float(parser.x_range[0]) if parser.x_range else min(allrms)-1
xto = float(parser.x_range[1]) if parser.x_range else max(allrms)+1
yfrom = float(parser.y_range[0]) if parser.y_range else min(allenergy)-10
yto = float(parser.y_range[1]) if parser.y_range else max(allenergy)+10


drawing = SvgViewport("outputs_from_test/score_to_rms.svg", 0, 0, 800, 650,color="white")
pl = Plot(drawing,100,700,100,600,xfrom,xto,yfrom,yto,axes_definition="UBLR")

stroke_color = color_by_name("SteelBlue").create_darker(0.3)
pl.axes["B"].label = "rmsd"
pl.axes["L"].label = "score"

for key,ax in pl.axes.items() :
  ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 2.0
  ax.tics(0,5)
pl.axes["U"].tics(5,0)
pl.axes["R"].tics(5,0)
pl.draw_axes()
pl.plot_label = "score_to_rms"
pl.draw_plot_label()
print(s)
for i in range(len(energy)):
  pl.scatter(rms[i],energy[i], markersize=2, markerstyle='c', title="serie-%s"%(i))
  
drawing.close()
