#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo -e "#Running python3 cif_to_mol2.py  test_inputs/HEM.cif HAEM"
python3 cif_to_mol2.py test_inputs/HEM.cif HAEM

