import sys

from pybioshell.core.data.io import Cif, write_mol2
from pybioshell.core.chemical import MonomerStructure


if len(sys.argv) < 2 :
    print("""

Converts a small molecule structure from CIF to MOL2 file format.
The last, optional parameter of the script provides the name of a given
molecule, that will be stored in MOL2 file


USAGE:
    python3 cif_to_mol2.py input.cif [molecule_name]


EXAMPLE:
    python3 cif_to_mol2.py HEM.cif [molecule_name]
    python3 cif_to_mol2.py HEM.cif HAEM

CATEGORIES: core/data/io/write_mol2
KEYWORDS:   CIF input; MOL2 output; Format conversion
GROUP:      File processing; Format conversion
  """)
    sys.exit()

mm = MonomerStructure.from_cif(sys.argv[1])
if len(sys.argv) > 2:                   # --- set molecule name if provided from command line
    mm.molecule_name = sys.argv[2]
write_mol2(mm, "stdout")

