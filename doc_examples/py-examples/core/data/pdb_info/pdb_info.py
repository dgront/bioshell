import sys

from pybioshell.core.data.io import Pdb

if len(sys.argv) < 2 :
    print("""

Reads a PDB file and extracts some basic information from its header


USAGE:
    python3 pdb_info.py input.pdb [input2.pdb]


EXAMPLE:
    python3 pdb_info.py 2kwi.pdb


CATEGORIES: core/data/io/Pdb
KEYWORDS:   PDB input;
GROUP:      File processing; 

  """)
    sys.exit()

for pdb_fname in sys.argv[1:] :
    s = Pdb(pdb_fname, "", True).create_structure(0)

    print(s.classification())
    print("protein", s.code(), "has", s.count_chains(), "chain(s),", s.count_residues(),
          "residues and", s.count_atoms(), "atoms\n")
    print("deposited : ", s.deposition_date())
    print("Is XRAY?  : ", (s.is_xray()))
    print("Is NMR?   : ", (s.is_nmr()))
    print("Is EM?    : ", (s.is_em()))
    print("resolution: ", s.resolution())
    print("R-value   : ", s.r_value())
    print("R-free    : ", s.r_free())
    if len(s.keywords()) > 0:
        print("Keywords  : ", s.keywords()[0], end="")
        for k in s.keywords()[1:]:
            print(", ", k, end="")
        print()
