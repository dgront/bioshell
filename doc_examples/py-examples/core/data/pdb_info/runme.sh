#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo "#Running python3 pdb_info.py test_inputs/2kwi.pdb test_inputs/4mcb.pdb test_inputs/5edw.pdb"
python3 pdb_info.py test_inputs/2kwi.pdb test_inputs/4mcb.pdb test_inputs/5edw.pdb test_inputs/6xra.pdb
