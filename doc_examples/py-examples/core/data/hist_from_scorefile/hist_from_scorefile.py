import sys,argparse

from pybioshell.core.data.io import find_pdb, read_scorefile

from os.path import expanduser, join
home_dir = expanduser("~")
# It is assumed  VisuaLife library is installed in your $HOME/src.git/visualife/src/ directory
sys.path.append(join(home_dir, "src.git/visualife/src/"))

from core.Plot import Plot
from core.SvgViewport import SvgViewport
from core.styles import ColorRGB, color_by_name

from pybioshell.core.calc.statistics import HistogramDD

if len(sys.argv) < 2 :
    print("""

Reads Rosetta scorefile and plot histogram of given column name (default is rms) and energy plot.

EXAMPLE:
    python3 hist_from_scorefile.py -f 1pgx-abinitio.fsc 1pgx-abinitio-bis.fsc -c score -min -50.0 -max 40.0
    
Call python3 hist_from_scorefile.py -h for full help  


CATEGORIES: core/calc/statistics/HistogramDD; core/data/io/read_scorefile
KEYWORDS: Rosetta scorefile; histogram; energy plot
GROUP: Statistics;
IMG: rms_hist.svg

  """)
    sys.exit()

# -----------argument parsing
parser = argparse.ArgumentParser(description='Reads Rosetta scorefiles and plot histogram of given column name (default is rms) for all files in one plot')

parser.add_argument('-f', '--file', help="input .fsc file", nargs='+', required=True)
parser.add_argument('-c', '--column', help="column name for histogram", nargs=1, required=False, default=["rms"])
parser.add_argument('-min', '--min', help="minimum data value", nargs=1, required=False)
parser.add_argument('-max', '--max', help="maximum data value", nargs=1, required=False)
parser.add_argument('-b', '--bin_width', help="bin width", nargs=1, required=False, default=[1.0])
  
parser = parser.parse_args()

xmin = float(parser.min[0]) if parser.min else min(alldata)
xmax = float(parser.max[0]) if parser.max else max(alldata)
step = float(parser.bin_width[0])

data = []
alldata = []

# -----------filling lists with data from file
for i in range(len(parser.file)):
  p = read_scorefile(parser.file[i])
  indx = p.column_index(parser.column[0])
  print("#Making histogram for ",parser.column[0]," column at index ",indx)
  lhist = []
  for row in p:
    lhist.append(float(row[indx]))
  data.append(lhist)
  alldata.extend(lhist)

# ------------ploting-----------
drawing = SvgViewport("%s_hist.svg"%(parser.column[0]), 0, 0, 800, 650,color="white")
pl = Plot(drawing,100,700,100,550,xmin, xmax,0,0.5,axes_definition="UBLR")

stroke_color = color_by_name("SteelBlue").create_darker(0.3)
pl.axes["B"].label = parser.column[0]

for key,ax in pl.axes.items() :
  ax.fill, ax.stroke, ax.stroke_width = stroke_color, stroke_color, 2.0
  ax.tics(0,5)
pl.axes["U"].tics(5,0)
pl.axes["R"].tics(5,0)
pl.draw_axes()
pl.plot_label = "%s histogram" %(parser.column[0])
pl.draw_plot_label()


box_width = 0.9 * step if len(data) == 1 else step/(len(data)+1.0)
for i in range(len(data)):
  # ------------------ here we actually make a histogram -----------------
  h = HistogramDD(step,xmin,xmax)
  for j in data[i]: h.insert(j)
  h.normalize()
  x_data = []
  y_data = []
  for b in range(h.count_bins()):
    x_data.append(h.bin_min_val(b)+box_width*i)
    y_data.append(h.get_bin(b))

  print(h)

  pl.bars(x_data, y_data, width=step, title="hist%s"%(i))

drawing.close()
