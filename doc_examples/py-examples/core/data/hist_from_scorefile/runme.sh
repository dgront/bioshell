#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"

mkdir -p ./outputs_from_test
echo "#Running python3 hist_from_scorefile.py -f test_inputs/1pgxA-abinitio.fsc test_inputs/1pgxA-abinitio-bis.fsc -c score -min -50.0 -max -10.0"
python3 hist_from_scorefile.py -f test_inputs/1pgxA-abinitio.fsc test_inputs/1pgxA-abinitio-bis.fsc -c score -min -50.0 -max -10.0

