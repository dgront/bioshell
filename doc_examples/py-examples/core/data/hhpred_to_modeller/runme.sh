#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo "#Running python3 hhpred_to_modeller.py test_inputs/CYP51F.hhpred"
python3 hhpred_to_modeller.py test_inputs/CYP51F.hhpred 1 2

mkdir -p outputs_from_test
mv *.pir outputs_from_test/

