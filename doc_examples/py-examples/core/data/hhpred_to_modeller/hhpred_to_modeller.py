import sys

from pybioshell.core.data.io import read_hhpred, create_pir_string


if len(sys.argv) < 2 :
  print("""

    Reads an output file produced by HHPred, that contains alignments between a query protein and template protein structues.
    Writes PIR input files necessary for Modeller to build structural models of the query based on a given alignment 
    (by default the first alignment is used)


USAGE:
    python3 hhpred_to_modeller.py hhpred_output [which-alignment [other alignments ... ] ]


EXAMPLE:    
    python3 hhpred_to_modeller.py CYP51F.hhpred 1 2 


CATEGORIES: core/data/io/read_hhpred
KEYWORDS:   HHPred; comparative modelling
GROUP: File processing; Format conversion

  """)
  sys.exit()

alignments = read_hhpred(sys.argv[1])
which_ali  = sys.argv[2:] if len(sys.argv) > 2 else [1]
print(len(alignments),"alignments found in",sys.argv[1])
for i in which_ali :
  i = int(i)
  print("retriving alignment:",i,"as %d.pir" % (i))
  f = open("%d.pir" % (i), "w")
  f.write(create_pir_string(alignments[i-1], 80))
  f.close()
