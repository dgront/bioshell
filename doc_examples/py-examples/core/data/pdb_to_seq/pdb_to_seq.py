import sys

from pybioshell.core.data.io import Pdb,create_seq_string

if len(sys.argv) < 2 :
  print("""

Converts sequence in a PDB format to SEQ format.


USAGE:
    python3 pdb_to_ss2.py input.pdb [input.ss2]


EXAMPLE:
    python3 pdb_to_ss2.py 2gb1.pdb 2gb1.out
    python3 pdb_to_ss2.py 2kwi.pdb

CATEGORIES: core/data/io/write_seq
KEYWORDS:   PDB input; secondary structure; Format conversion
GROUP:      File processing; Format conversion
  """)
  sys.exit()

structure = Pdb(sys.argv[1],"").create_structure(0)
outname = sys.argv[2] if len(sys.argv) > 2 else "stdout"
for ic in range(structure.count_chains()) :
    chain = structure[ic]
    ss = chain.create_sequence()
    a = create_seq_string(ss)
    print(a)
    
