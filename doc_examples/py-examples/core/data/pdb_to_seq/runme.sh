#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
mkdir -p outputs_from_test

echo "#Running python3 pdb_to_seq.py test_inputs/2gb1.pdb 2gb1.ss2"
python3 pdb_to_seq.py test_inputs/2gb1.pdb 2gb1.ss2
mv 2gb1.ss2 outputs_from_test

echo "#Running python3 pdb_to_seq.py test_inputs/2kwi.pdb"
python3 pdb_to_seq.py test_inputs/2kwi.pdb


