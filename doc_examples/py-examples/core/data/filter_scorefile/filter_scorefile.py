import sys, argparse

from pybioshell.core.data.io import read_scorefile

if len(sys.argv) < 2 :
  print("""

Reads Rosetta scorefile and prints only it's requested part 

EXAMPLE:
    python3 filter_scorefile.py 1pgx-abinitio.fsc 1pgx-abinitio-bis.fsc score -50.0 40.0

Call python3 filter_scorefile.py -h for full help  

CATEGORIES: core/data/io/read_scorefile
KEYWORDS: Rosetta scorefile; 
GROUP: Statistics;

  """)
  sys.exit()

# -----------argument parsing
parser = argparse.ArgumentParser(description="Reads Rosetta scorefile and prints only it's requested part")

parser.add_argument('-f', '--file', help="input .fsc file", nargs='+', required=True)
parser.add_argument('-c', '--column', help="column name(s) to keep", nargs='+', required=False, default=["score", "rms"])
parser = parser.parse_args()

columns = [col_name for col_name in parser.column]

# ---------- Print scorefile header
print("SCORE: ", end="")
for col_name in columns:
    print(col_name, end=" ")
print()

# ---------- Print scorefile data
for file_name in parser.file:
    sf = read_scorefile(file_name)
    for i_row in range(len(sf)) :
        row = sf[i_row]
        print("SCORE: ",end="")
        for col_name in columns:
            print(row[sf.column_index(col_name)],end=" ")
        print()

