#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo "#Running python3 betastructures_graph.py test_inputs/2gb1.pdb"
python3 betastructures_graph.py test_inputs/2gb1.pdb

echo "#Running python3 betastructures_graph.py test_inputs/5edw.pdb"
python3 betastructures_graph.py test_inputs/5edw.pdb
