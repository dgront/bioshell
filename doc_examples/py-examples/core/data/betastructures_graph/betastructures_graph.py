import sys

from pybioshell.core.data.io import Pdb
from pybioshell.core.calc.structural import ProteinArchitecture


if len(sys.argv) < 2 :
    print("""

Shows how to use BetaStructuresGraph class from Python
    
    
USAGE:
    python3 betastructures_graph.py input.pdb


EXAMPLE:
    python3 betastructures_graph.py 5edw.pdb

CATEGORIES: core/calc/structural/ProteinArchitecture
KEYWORDS:   PDB input; graphs

  """)
    sys.exit()

pdb_fname = sys.argv[1]
structure = Pdb(pdb_fname, "").create_structure(0)
architecture = ProteinArchitecture(structure)
beta_graph = architecture.create_strand_graph()
strands = beta_graph.get_strands_copy()

print("    ",end ="")
for i in range(len(strands)) :  print(" S%2d " % (i),end ="")
print()

for i in range(len(strands)) :
  pairings = []
  print("S%2d: " % (i),end ="")
  for j in range(len(strands)) :
    are_paired = False
    try :
      p = beta_graph.get_strand_pairing(strands[i],strands[j])
      are_paired = True
    except:  pass
    if are_paired : 
      print("  X  ",end ="")
      pairings.append(j)
    else : print("     ",end ="")
  print("| %-45s paired with %d" % (str(strands[i]),pairings[0]),end ="")
  for pi in range(1,len(pairings)) :
    print(", %d" % (pairings[pi]),end ="")
  print()
