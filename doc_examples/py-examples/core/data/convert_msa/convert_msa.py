import sys

from pybioshell.std import vector_std_shared_ptr_core_data_sequence_Sequence_t
from pybioshell.core.data.io import read_hssp_file, write_clustalo_file, read_fasta_file, read_clustalw_file

from pybioshell.utils import LogManager

LogManager.FINEST()

if len(sys.argv) < 3 :
    print("""

Converts multiple sequence alignment (MSA) data from one format to another.
Known input formats:  FASTA (.fasta), HSSP (.hssp) and ClustalW/ClustalO (.aln)
Known output formats: FASTA (.fasta)

Input and output file formats are detected by file extension

USAGE:
    python3 convert_msa.py input_file output_file
    
    
EXAMPLE:    
    python3 convert_msa.py cyped.CYP109.aln cyped.CYP109.fasta 


CATEGORIES: core/data/io/read_hssp_file
KEYWORDS:   MSA;  Format conversion
GROUP: File processing;

  """)
    sys.exit()

msa = vector_std_shared_ptr_core_data_sequence_Sequence_t() # --- vector to hold sequences obtained from an input file
extension_in = sys.argv[1].split('.')[-1]                   # --- detect the input format
if extension_in == 'aln':
    read_clustalw_file(sys.argv[1], msa)
elif extension_in == 'hssp':
    read_hssp_file(sys.argv[1], msa)
elif extension_in == 'fasta':
    read_fasta_file(sys.argv[1], msa)

f = open(sys.argv[2],"w")
for seq in msa:
    print(">", seq.header(), file=f)
    print(seq.sequence, file=f)
f.close()

