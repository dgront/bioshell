#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo -e "#Running python3 convert_msa.py test_inputs/1crn.hssp  1crn.aln"
python3 convert_msa.py test_inputs/1crn.hssp 1crn.fasta
echo -e "#Running python3 convert_msa.py CYP109B1.aln CYP109B1.fasta"
python3 convert_msa.py test_inputs/CYP109B1.aln CYP109B1.fasta

mkdir -p outputs_from_test
mv *.aln *.fasta outputs_from_test

