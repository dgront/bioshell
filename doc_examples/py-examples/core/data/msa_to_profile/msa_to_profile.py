import sys

from pybioshell.std import vector_std_shared_ptr_core_data_sequence_Sequence_t
from pybioshell.core.data.io import read_clustalw_file
from pybioshell.core.data.sequence import SequenceProfile


if len(sys.argv) < 2 :
  print("""

Reads a multiple sequence alignment (MSA) (in .aln  format) produced by ClustalO,
calculates a sequence profile and prints in a flat tabular format


USAGE:
    python3 msa_to_profile.py input.aln
    
    
EXAMPLE:    
    python3 msa_to_profile.py cyped.CYP109.aln


CATEGORIES: core/data/io/read_clustalw_file
KEYWORDS:   MSA; sequence profile; Format conversion
GROUP: Sequence calculations;

  """)
  sys.exit()

msa = vector_std_shared_ptr_core_data_sequence_Sequence_t()
read_clustalw_file(sys.argv[1], msa)
profile = SequenceProfile(msa[0], SequenceProfile.aaOrderByPropertiesGapped(), msa)
profile.write_table()
