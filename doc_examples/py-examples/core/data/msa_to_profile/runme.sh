#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo "#Running python3 msa_to_profile.py test_inputs/cyped.CYP109.aln"
python3 msa_to_profile.py test_inputs/cyped.CYP109.aln
