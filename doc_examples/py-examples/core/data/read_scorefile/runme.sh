#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"

mkdir -p ./outputs_from_test
echo "#Running python3 ./read_scorefile.py test_inputs/1pgxA-abinitio.fsc"
python3 read_scorefile.py test_inputs/1pgxA-abinitio.fsc

echo "#Running python3 ./read_scorefile.py test_inputs/1pgxA-abinitio-bis.fsc"
python3 read_scorefile.py  test_inputs/1pgxA-abinitio-bis.fsc 
