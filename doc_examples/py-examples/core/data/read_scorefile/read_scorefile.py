import sys
from pybioshell.core.data.io import read_scorefile

if len(sys.argv) < 2 :
  print("""

Simple example that parses a score file (Rosetta output)


USAGE:
    python3 read_scorefile.py score-file

EXAMPLE:
    python3 read_scorefile.py scores.sf

CATEGORIES: core/data/io/read_scorefile
KEYWORDS:   scorefile input;
GROUP:      File processing; Format conversion
  """)
  sys.exit()

sf = read_scorefile(sys.argv[1])

print("Number of rows: %d" % len(sf))
print("Number of columns: %d" % sf[0].size())
print("Known columns:")
for i in range(sf[0].size()) :
  print(sf.column_name(i))

