#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo "#Running python3 pdb_from_clustering.py test_inputs/clusters-10.00.txt Clo"
python3 pdb_from_clustering.py test_inputs/clusters-10.00.txt Clo

mkdir -p outputs_from_test
mv c*.pdb outputs_from_test/
