import sys

from pybioshell.core.data.io import Pdb

if len(sys.argv) < 2 :
    print("""

Extracts PDB clusters from clustering results produced by ap_cluster_ligands output

SEE:
 ap_cluster_ligands program to see how to run clustering

USAGE:
    python3 pdb_from_clusters.py clustering_output.txt ligand_code


EXAMPLE:
    python3 pdb_from_clustering.py clustering_output.txt Clo


CATEGORIES: core/data/io/Pdb
KEYWORDS:   PDB output; clustering
GROUP:      File processing; Structure calculations

  """)
    sys.exit()

chain_ids = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvw"
clusters_file = open(sys.argv[1])
ligand_code = sys.argv[2]
iline = 0
for line in clusters_file:
    iline += 1
    tokens = line.strip().split()
    outp = open("c"+str(iline)+tokens[0]+".pdb","w")
    p = Pdb(tokens[1],"", False)
    s = p.create_structure(0)
    i_chain = 0
    for ic in range(s.count_chains()):
        c = s[ic]
        for ir in range(c.count_residues()):
            r = c[ir]
            for ia in range(r.count_atoms()):
                a = r[ia]
                outp.write(a.to_pdb_line() + "\n")

    for fname in tokens[2:]:
        p = Pdb(fname,"", False)
        s = p.create_structure(0)
        for ic in range(s.count_chains()):
            c = s[ic]
            for ir in range(c.count_residues()):
                r = c[ir]
                if r.residue_type().code3 != ligand_code: continue
                r.owner().id(chain_ids[i_chain])
                for ia in range(r.count_atoms()):
                    a = r[ia]
                    outp.write(a.to_pdb_line() + "\n")
    outp.close()
