import sys

from pybioshell.core.data.sequence import read_ASN1_checkpoint

if len(sys.argv) < 2 :
  print("""

Converts a sequence profile (in ASN.1 format) produced by psiblast to a flat tabular format


USAGE:
    python asn1_to_profile.py input.asn1


EXAMPLE:
    python asn1_to_profile.py d1or4A_.asn1


CATEGORIES: core/data/sequence/read_ASN1_checkpoint
KEYWORDS:   sequence profile; Format conversion
GROUP:      File processing; Format conversion

  """)
  sys.exit()

profile = read_ASN1_checkpoint(sys.argv[1])
profile.write_table_header()
profile.write_table()
