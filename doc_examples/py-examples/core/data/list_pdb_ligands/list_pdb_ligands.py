import sys

from pybioshell.core.data.io import find_pdb


if len(sys.argv) < 2 :
    print("""

Prints names of ligand molecules found in a given PDB file.
    
A ligand is defined as a residue located after TER field in a PDB chain

   
USAGE:
    python3 list_pdb_ligands.py input.pdb


EXAMPLE:
   python3 list_pdb_ligands.py 5edw.pdb

    
CATEGORIES: core/data/io/find_pdb
KEYWORDS:   PDB input; ligand
GROUP:      File processing; Data filtering

  """)
    sys.exit()

for pdb_fname in sys.argv[1:] :
    structure = find_pdb(pdb_fname, "./").create_structure(0)
    for ic in range(structure.count_chains()) :
        chain = structure[ic]
        #print(chain.terminal_residue_index())

        for ir in range(chain.terminal_residue_index() + 1,chain.size()) :
            resid = chain[ir]
            code3 = resid.residue_type().code3
            if resid.residue_type().code3 == "HOH" : continue # Skip water molecules, they are so obvious and abundant
            formula = structure.formula(code3)
            hetname = structure.hetname(code3)
            print("%3s %c %4d %s %s" %(code3, chain.id(), resid.id(), formula.strip(), hetname.strip()))
