#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo "#Running python3 ./list_pdb_ligands.py test_inputs/5edw.pdb"
python3 ./list_pdb_ligands.py test_inputs/5edw.pdb

echo "#Running python3 ./list_pdb_ligands.py test_inputs/5ofq.pdb"
python3 ./list_pdb_ligands.py test_inputs/5ofq.pdb