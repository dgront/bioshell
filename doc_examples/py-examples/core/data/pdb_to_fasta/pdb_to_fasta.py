import sys

from pybioshell.core.data.io import find_pdb

# change that setting to False to include ligands in the output sequence
IF_EXCLUDE_LIGANDS = True

if len(sys.argv) < 2 :
  print("""

Extracts amino acid (or nucleotide) sequence from a PDB file.

Note, that by default ligands are not included in the output sequence even if they
are amino acids (e.g. 7dk3 deposit)

USAGE:
    python3 pdb_to_fasta.py input.pdb [input2.pdb]


EXAMPLE:
    python3 pdb_to_fasta.py 2kwi.pdb


CATEGORIES: core/data/io/find_pdb
KEYWORDS:   PDB input; FASTA; Format conversion
GROUP:      File processing; Format conversion

  """)
  sys.exit()

for pdb_fname in sys.argv[1:] :
  structure = find_pdb(pdb_fname, "./").create_structure(0)
  for ic in range(structure.count_chains()) :
    chain = structure[ic]
    print(">",structure.code(), chain.id())
    print(chain.create_sequence(IF_EXCLUDE_LIGANDS).sequence)
