#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo -e "#Running python3 caonly_multimodel.py test_inputs/cat_list o.pdb"
python3 caonly_multimodel.py test_inputs/cat_list o.pdb
mkdir -p outputs_from_test
mv o.pdb outputs_from_test
