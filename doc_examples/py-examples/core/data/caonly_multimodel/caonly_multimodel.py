import sys

from pybioshell.core.data.io import Pdb, write_pdb

if len(sys.argv) < 2 :
    print("""

Reads multiple PDB files and writes C-alpha atom of all structures into a single multimodel pdb file.

The input file is a simple text file providing PDB file names (one string per line). 

    
USAGE:
    python3 caonly_multimodel.py.py input_structres_list [output_fname.pdb]

EXAMPLE:
    python3 caonly_multimodel.py.py cat_lits o.pdb


CATEGORIES: core/data/io/Pdb
KEYWORDS:   PDB input; structure selectors; PDB output
GROUP: File processing

  """)
    sys.exit()

input_fnames = open(sys.argv[1])
reader = Pdb(input_fnames.readline().strip(),"is_ca",False)
structure = reader.create_structure(0)
out_fname = "out.pdb" if len(sys.argv) == 2 else sys.argv[2]
write_pdb(structure, out_fname, 1)
i_model = 2
print("Reading PDB files: ",end="")
for pdb_fname in input_fnames :
    print(pdb_fname.strip().split("/")[-1],end=" ")
    reader = Pdb(pdb_fname.strip(),"is_ca",False)
    reader.fill_structure(0,structure)
    write_pdb(structure, out_fname, i_model)
    i_model += 1
