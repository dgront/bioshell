import sys

from pybioshell.core.data.io import read_fasta_file, Pdb


if len(sys.argv) < 3 :
  print("""

Reads a FASTA file with two aligned sequences: a query and a template, and a template structure.
Prints a partial thread of the template, i.e. the fragment of a template structure that is
aligned with a query


USAGE:
    python3 partial_thread.py ali.fasta template.pdb [chain-id]


EXAMPLE:
    python3 partial_thread.py 2azaA_2pcyA-ali.fasta 2aza.pdb A


CATEGORIES: core/alignment
KEYWORDS:   FASTA input; sequence
GROUP:      File processing; Data filtering

  """)
  sys.exit()

def select_aligned_residues(query_seq, template_seq, template_chain):
    j = 0
    out = []
    for i in range(len(query_seq)):
        if template_seq[i] != '-':
            if query_seq[i] != '-':  out.append(template_chain[j])
            j += 1
    return out


def print_atoms(residues):
    for r in residues:
        for i in range(r.count_atoms()):
            print(r[i].to_pdb_line())


fasta = read_fasta_file(sys.argv[1])
seq1 = fasta[0].sequence
seq1_gapless = fasta[0].create_ungapped_sequence().sequence
seq2 = fasta[1].sequence
seq2_gapless = fasta[1].create_ungapped_sequence().sequence

print(fasta[0].sequence,fasta[1].sequence)

strctr = Pdb(sys.argv[2],"").create_structure(0)
if len(sys.argv) > 3:
    chain = strctr.get_chain(sys.argv[3])
else:
    chain = strctr[0] 
seq_pdb = chain.create_sequence().sequence
if seq_pdb == seq1_gapless:
    atoms = select_aligned_residues(seq2, seq1, chain)
    print_atoms(atoms)
elif seq_pdb == seq2_gapless:
    atoms = select_aligned_residues(seq1, seq2, chain)
    print_atoms(atoms)
else:
    print("template sequence can't be identified in the given alignment")
    


