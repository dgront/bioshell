#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"

echo "#Running python3 partial_thread.py test_inputs/2azaA_2pcyA-ali.fasta test_inputs/2aza.pdb A > 2azaA-thread.pdb"
python3 partial_thread.py test_inputs/2azaA_2pcyA-ali.fasta test_inputs/2aza.pdb A > 2azaA-thread.pdb

echo "#Running python3 partial_thread.py test_inputs/2azaA_2pcyA-ali.fasta test_inputs/2pcy.pdb  > 2pcyA-thread.pdb"
python3 partial_thread.py test_inputs/2azaA_2pcyA-ali.fasta test_inputs/2pcy.pdb  > 2pcyA-thread.pdb

mkdir -p outputs_from_test
mv *.pdb outputs_from_test/
