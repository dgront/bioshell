#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"

echo -e "#Running python3 ./align_sequences.py  test_inputs/2azaA.fasta test_inputs/2pcyA.fasta"
python3 ./align_sequences.py  test_inputs/2azaA.fasta test_inputs/2pcyA.fasta


