import sys

from pybioshell.core.alignment import SequenceNWAligner, SequenceSWAligner
from pybioshell.core.data.io import read_fasta_file

if len(sys.argv) < 3 :
  print("""

Computes pairwise sequence alignment 


USAGE:
    python3 align_sequences.py input-1.fasta input-2.fasta [gap_open gap_cont]


EXAMPLE:
    python3 align_sequences.py 2azaA.pdb 2pcyA.pdb


CATEGORIES: core/alignment/SequenceNWAligner
KEYWORDS:   alignment
GROUP: Sequence calculations

  """)
  sys.exit()
q = read_fasta_file(sys.argv[1])[0]
t = read_fasta_file(sys.argv[2])[0]
# ---------- calculate a global alignment
aligner = SequenceNWAligner(max(q.length(), t.length()))
# ---------- calculate a local alignment
#aligner = SequenceSWAligner(max(q.length(), t.length()))
if len(sys.argv) < 4 :
    score = aligner.align(q, t, -10, -1, "BLOSUM62")
else:
    score = aligner.align(q, t, int(sys.argv[3]), int(sys.argv[4]), "BLOSUM62")

alignment = aligner.backtrace_sequence_alignment()
print("# score:", score)
print("> query\n" + alignment.get_aligned_query())
print("> template\n" + alignment.get_aligned_template())
