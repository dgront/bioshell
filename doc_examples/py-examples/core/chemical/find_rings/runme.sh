#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"

echo -e "#Running python3 ./find_rings.py  test_inputs/MES_ideal.pdb"
python3 ./find_rings.py test_inputs/MES_ideal.pdb

echo -e "#Running python3 ./find_rings.py  test_inputs/9ZB_ideal.pdb"
python3 ./find_rings.py test_inputs/9ZB_ideal.pdb


