import sys

from pybioshell.core.chemical import PdbMolecule
from pybioshell.core.chemical import find_rings

if len(sys.argv) < 2 :
  print("""

Reads in a mall molecule (PDB file format) and prints all cycles (i.e. rings) that can be found.
Note, that rings may be nested, e.g. naphthalene molecule has actually three rings!

USAGE:
    python3 find_rings.py molecule.pdb


EXAMPLE:
    python3 find_rings.py 9ZB_ideal.pdb 

    CATEGORIES: core/chemical/
    KEYWORDS: PDB input; small molecules
    GROUP: small molecules;

  """)
  sys.exit()

mol = PdbMolecule.from_pdb(sys.argv[1])
rings = find_rings(mol)
for ring in rings:
  print("# ------------")
  for atom in ring:
    print(mol.get_atom(atom).to_pdb_line())

