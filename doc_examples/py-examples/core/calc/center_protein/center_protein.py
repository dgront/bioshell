import sys

from pybioshell.core.data.io import find_pdb

if len(sys.argv) < 2 :
  print("""

Moves a given protein structure so its geometric center is located at (0,0,0).

USAGE:
    python3 center_protein.py input.pdb [which_model]


EXAMPLE:
    python3 center_protein.py 2kwi.pdb 51

    CATEGORIES: core/data/io/find_pdb
    KEYWORDS: PDB input; center protein; internal coordinates
    GROUP: Structure calculations;

  """)
  sys.exit()

which_model = 0 if len(sys.argv) == 2 else (int(sys.argv[2])-1)

pdb_reader = find_pdb(sys.argv[1], "./")
structure = pdb_reader.create_structure(which_model)
cx, cy, cz, n = 0, 0, 0, 0
for ic in range(structure.count_chains()) :
    chain = structure[ic]
    for ir in range(chain.count_residues()) :
      resid = chain[ir]
      for ai in range(resid.count_atoms()) :
        cx += resid[ai].x
        cy += resid[ai].y
        cz += resid[ai].z
        n+=1.0
        
cx /= n
cy /= n
cz /= n
print("# Center was:",cx,cy,cz)

for ic in range(structure.count_chains()) :
    chain = structure[ic]
    for ir in range(chain.count_residues()) :
      resid = chain[ir]
      for ai in range(resid.count_atoms()) :
        resid[ai].x -= cx
        resid[ai].y -= cy
        resid[ai].z -= cz
        print(resid[ai].to_pdb_line())
