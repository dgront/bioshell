import sys, math

from pybioshell.core.data.io import find_pdb
from pybioshell.core.calc.structural import SaturatedRing6Geometry

if len(sys.argv) < 3 :
    print("""

Validates a hexagonal saturated ring, defined by 6 atoms.

USAGE:
    python3 validate_saturated_ring6.py input.pdb ligand _atom1_ _atom2_ _atom3_ _atom4_ _atom5_ _atom6_


EXAMPLE:
    python3 validate_saturated_ring6.py 4jm3.pdb EPE _N1_ _C2_ _C3_ _N4_ _C5_ _C6_

CATEGORIES: core/calc/structural/SaturatedRing6Geometry
KEYWORDS:   PDB input; structural properties
GROUP: Structure calculations

  """)
    sys.exit()


pdb = find_pdb(sys.argv[1], "./")
strctr = pdb.create_structure(0)
for i_chain in range(strctr.count_chains()) :
  chain = strctr[i_chain]
  for i_res in range(chain.count_residues()) :
    if chain[i_res].residue_type().code3 == sys.argv[2] :
      atoms = []
      for at_name in sys.argv[3:] : 
        try :
          at_name_fixed = at_name.replace("_"," " ) 
          atoms.append( chain[i_res].find_atom(at_name_fixed))
          if not atoms[-1] :
            sys.stderr.write("Can't find atom "+at_name_fixed+" in "+sys.argv[2]+" residue\n")
        except :
          sys.stderr.write("Can't find atom "+at_name+" in "+sys.argv[2]+" residue\n")
      s = SaturatedRing6Geometry(atoms[0],atoms[1],atoms[2],atoms[3],atoms[4],atoms[5])
      print(s.first_wing_angle(),s.second_wing_angle())

