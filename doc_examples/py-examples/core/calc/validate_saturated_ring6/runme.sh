#!/bin/bash
  
export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo -e "#Running python3 ./validate_saturated_ring6.py  test_inputs/pdb6bge.ent.gz EPE  _N1_ _C2_ _C3_ _N4_ _C5_ _C6_"
python3 ./validate_saturated_ring6.py  test_inputs/pdb6bge.ent.gz EPE  _N1_ _C2_ _C3_ _N4_ _C5_ _C6_

echo -e "#Running python3 ./validate_saturated_ring6.py  test_inputs/4jm3.pdb EPE  _N1_ _C2_ _C3_ _N4_ _C5_ _C6_"
python3 ./validate_saturated_ring6.py  test_inputs/4jm3.pdb EPE  _N1_ _C2_ _C3_ _N4_ _C5_ _C6_


