import sys

from pybioshell.core.data.io import Pdb
from pybioshell.core.calc.structural import evaluate_phi, evaluate_psi, ContactMap
import time

if len(sys.argv) < 3 :
  print("""

Calculates contact map for a number of models from a single PDB file and prints how often any two residues
are in contact


USAGE:
    python3 contact_map.py input.pdb cutoff


EXAMPLE:
    python3 contact_map.py 2kwi.pdb 4.5


CATEGORIES: core/calc/structural/ContactMap
KEYWORDS:   PDB input; contact map
GROUP: Structure calculations;
IMG: ap_contact_map.png

  """)
  sys.exit()

pdb = Pdb(sys.argv[1],"")
cutoff = float(sys.argv[2])
contact_map = ContactMap(pdb.create_structure(0), cutoff)
for i_model in range(1,pdb.count_models()) :
  strctr = pdb.create_structure(i_model)
  contact_map.add(strctr)
  print("model",i_model,"added", file=sys.stderr)

res_max = contact_map.max_row_index()
print("  i  ci res_i    j  cj resj n_cont")
for i in range(res_max) :
# --- Get residue index for residue indexed by i; residue index is a structure holding chain ID, residue ID and an insertion code
  ri = contact_map.residue_index(i)
  for j in range(i-1) :
    rj = contact_map.residue_index(j)
    if contact_map.has_element(i,j) :
      print("%4d  %c %4d%c  %4d  %c %4d%c  %4d" %(i,ri.chain_id, ri.residue_id, ri.i_code,
        j,rj.chain_id, rj.residue_id, rj.i_code,contact_map.at(i,j)))
