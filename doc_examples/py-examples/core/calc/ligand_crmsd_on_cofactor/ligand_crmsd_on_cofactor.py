import sys, math

sys.path.append("../../../../../bin/")

from pybioshell.core.data.basic import Vec3
from pybioshell.core.data.io import Pdb
from pybioshell.std import vector_core_data_basic_Vec3
from pybioshell.core.calc.structural.transformations import *
from pybioshell.utils import LogManager

LogManager.INFO()

if len(sys.argv) < 3:
    print("""

Calculates cRMSD (coordinate Root-Mean-Square Deviation) value on all atoms of a ligand conformations
after superimposition based on another group, e.g. a cofactor.

This scripts has been used in P450 analysis project: all PDB deposits with a drug (e.g. itraconazole) were pulled from PDB
For each pair of structures, the optimal superimposition for haeme groups is found. Then the very transformation
is used to transfrom coordinates a molecule of a drug and to compute crsmd on the two itraconazole molecules

USAGE:
    python3 ligand_crmsd_on_cofactor.py cofactor-code3 ligand-code3  input1.pdb [input2.pdb]


EXAMPLE:
    python3 crmsd_on_ligands.py HEM 1YN  5ofq.pdb 4rm4.pdb


CATEGORIES: core/calc/structural/transformations/CrmsdOnVec3
KEYWORDS:   PDB input; ligand; crmsd
GROUP: Structure calculations

  """)
    sys.exit()

rms = CrmsdOnVec3()


class Entry:

    def __init__(self, code, structure):

        self.pdb_code = code  # --- PDB code for every input structure: for informative output
        self.structure = structure  # --- contains all input structures
        self.ligand = None
        self.cofactor = None
        self.atoms_superimposed = vector_core_data_basic_Vec3()  # --- a list of atoms to define rototranslation
        self.atoms_crmsd = vector_core_data_basic_Vec3()  # --- a list of atoms to compute crmsd

    def is_OK(self):
        return self.ligand and self.cofactor

    def add_ligand(self,ligand):
        for ia in range(ligand.count_atoms()):
            e.atoms_crmsd.append(ligand[ia])
        self.ligand = ligand

    def add_cofactor(self,cofactor):
        for ia in range(cofactor.count_atoms()):
            e.atoms_superimposed.append(cofactor[ia])
        self.cofactor = cofactor

rototranslation_code3 = sys.argv[1]
crmsd_code3 = sys.argv[2]

entries = []
for pdb_code in sys.argv[3:]:
    pdb = Pdb(pdb_code, "is_not_alternative is_not_water", False)
    structure = pdb.create_structure(0)
    for ic in range(structure.count_chains()):
        chain = structure[ic]
        # start from chain.terminal_residue_index() + 1 if you are sure the PDB file has TER lines
        e = Entry(pdb_code.split("/")[-1], structure)
        for ir in range(0, chain.size()):
            code3 = chain[ir].residue_type().code3
            if code3 == rototranslation_code3: e.add_cofactor(chain[ir])
            elif code3 == crmsd_code3: e.add_ligand(chain[ir])
        if e.is_OK(): entries.append(e)

tmp_vec = Vec3()
output = open("%s-by-%s.pdb" % (crmsd_code3, rototranslation_code3), "w")
n_superimposed = len(entries[0].atoms_superimposed)
n_crmsd = len(entries[0].atoms_crmsd)
for ei in entries:
    for ej in entries:
        if ei == ej: continue
        try :
            if len(ei.atoms_superimposed) != len(ej.atoms_superimposed):
                print("superimposed sets differ in size")
                continue
            crmsd_val_1 = rms.crmsd(ei.atoms_superimposed, ej.atoms_superimposed, n_superimposed, True)
            if len(ei.atoms_crmsd) != len(ej.atoms_crmsd):
                print("crsmd sets differ in size")
                continue
            crmsd_val_2 = rms.calculate_crmsd_value(ei.atoms_crmsd, ej.atoms_crmsd, n_crmsd)
            print("%s %4d %3s %c - %s %4d %3s %c : %7.3f  %7.3f" %
                  (ei.pdb_code, ei.ligand.id(), ei.ligand.residue_type().code3, ei.ligand.owner().id(),
                   ej.pdb_code, ej.ligand.id(), ej.ligand.residue_type().code3, ej.ligand.owner().id(), crmsd_val_1, crmsd_val_2))
            if ej == entries[0]:
                output.write("MODEL     %1\n")
                for ai in range(ei.ligand.count_atoms()):
                    rms.apply(ei.ligand[ai])
                    output.write(ei.ligand[ai].to_pdb_line() + "\n")
                output.write("ENDMDL\n")
        except:
            pass

output.write("MODEL     %d\n" % (1))
for ai in range(entries[0].ligand.count_atoms()):
    output.write(entries[0].ligand[ai].to_pdb_line() + "\n")
output.write("ENDMDL\n")

output.write("MODEL     %d\n" % (2))
for ai in range(entries[0].cofactor.count_atoms()):
    output.write(entries[0].cofactor[ai].to_pdb_line() + "\n")
output.write("ENDMDL\n")

output.close()
