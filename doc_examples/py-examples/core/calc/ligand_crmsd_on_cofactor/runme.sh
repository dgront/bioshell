#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo -e "#Running python3 ./ligand_crmsd_on_cofactor.py HEM 1YN test_inputs/*.pdb"

mkdir -p outputs_from_test
python3 ligand_crmsd_on_cofactor.py HEM 1YN test_inputs/*.pdb
mv 1YN-by-HEM.pdb ./1YN-by-HEM.pdb


