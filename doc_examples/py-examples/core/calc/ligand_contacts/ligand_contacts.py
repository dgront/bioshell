import sys, math, json

from pybioshell.core.data.io import Pdb
from pybioshell.utils import LogManager
from pybioshell.core.chemical import monomer_type_name, HydrogenBondFilter

LogManager.INFO()

if len(sys.argv) < 4:
    print("""

Finds contacts between a ligand molecule and a protein.

This scripts reads one or more PDB files, extracts all ligands that matches a given 
three-letter code and finds contacts between a ligand molecule and a protein for given cutoff.
The script can also detectplausible hydrogen bonds between a ligand and a protein, but user
must provide two JSON dictionaries: of hydrogen bond donors and acceptors. Use '-' (dash character)
to omit either of the two files and provide just one of them 

Note, that both files with JSON must have .json extension, otherwise the script will attempt 
to load them as PDB

USAGE:
    python3 ligand_contacts.py ligand distance [donors.json acceptors.json] input.pdb [input2.pdb]


EXAMPLE:
    python3 ligand_contacts.py HEM 3.5 5ofq.pdb 4rm4.pdb
    python3 ligand_contacts.py TDZ 3.5 donors.json acceptors.json 2vn0.pdb


CATEGORIES: core/data/io/Pdb
KEYWORDS:   PDB input; ligand; structural properties
GROUP: Structure calculations

  """)
    sys.exit()

cutoff = float(sys.argv[2])

first_pdb = 3
extra_acceptors, extra_donors = {}, {}
if sys.argv[3] == '-':
    first_pdb = 5
elif sys.argv[3].endswith(".json"):
    extra_donors = json.loads(open(sys.argv[3]).read())
    first_pdb = 5

if sys.argv[4].endswith(".json"):
    extra_acceptors = json.loads(open(sys.argv[4]).read())

hb_filter = HydrogenBondFilter()
for code3 in extra_acceptors.keys():
    for atom_name in extra_acceptors[code3]:
        hb_filter.add_acceptor_definition(code3, atom_name)
for code3 in extra_donors.keys():
    for atom_name in extra_donors[code3]:
        hb_filter.add_donor_definition(code3, atom_name)

for pdb_code in sys.argv[first_pdb:]:  # --- Iterate over PDB input files
    if len(sys.argv[first_pdb:]) > 1: print("# Pdb file %s" % (pdb_code.split("/")[-1].split(".")[0]))
    pdb = Pdb(pdb_code, "", False)
    print(" ---- ligand ---- | --------- partner -------- | distance")
    print("c  res  id atname |  c  res  id   type  atname | in Angstrom")

    for m in range(pdb.count_models()):  # --- Iterate over all models in the input file
        if pdb.count_models() > 1: print("# Model %d" % (i + 1))
        structure = pdb.create_structure(m)

        for ic in range(structure.count_chains()):
            lig_chain = structure[ic]
            for ir in range(lig_chain.count_residues()):
                ligand = lig_chain[ir]
                code3 = ligand.residue_type().code3

                if code3 != sys.argv[1]: continue  # Skip other ligands
                for iic in range(structure.count_chains()):
                    other_chain = structure[iic]
                    for r in range(other_chain.count_residues()):  # ----Iterate over residues
                        res = other_chain[r]
                        if res == ligand: continue
                        d = res.min_distance(ligand)  # ---- If residue is close enough to ligand
                        if d < cutoff:
                            for ilig in range(ligand.count_atoms()):
                                for ioth in range(res.count_atoms()):
                                    ligand_atom = ligand[ilig]
                                    other_atom = res[ioth]
                                    if ligand_atom.distance_to(other_atom) <= cutoff:
                                        extras = ""
                                        if hb_filter(ligand_atom, other_atom,ligand_atom.distance_to(other_atom)):
                                           extras = "HYDROGEN_BOND"
                                        print("%s  %3s %4d %4s     %s  %3s %4d %6s %4s   %6.3f %s" % (ligand.owner().id(),
                                               ligand.residue_type().code3, ligand.id(), ligand_atom.atom_name(),
                                               res.owner().id(), res.residue_type().code3, res.id(),
                                               monomer_type_name(res.residue_type()), other_atom.atom_name(),
                                               ligand_atom.distance_to(other_atom), extras))
