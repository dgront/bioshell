#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo -e "#Running python3 ./ligand_contacts.py HEM 3.5 test_inputs/5ofq.pdb test_inputs/4rm4.pdb"

python3 ligand_contacts.py HEM 3.5 test_inputs/5ofq.pdb test_inputs/4rm4.pdb

echo -e "#Running python3 ligand_contacts.py TDZ 3.5 donors.json acceptors.json test_inputs/2vn0.pdb"
python3 ligand_contacts.py TDZ 3.5 donors.json acceptors.json test_inputs/2vn0.pdb

