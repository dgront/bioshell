import sys

sys.path.append("/Users/dgront/src.git/bioshell/bin")
from pybioshell.core.data.io import Pdb

if len(sys.argv) < 2 :
    print("""

Checks if a given structure has chain breaks


USAGE:
    python3 check_structure.py input.pdb


EXAMPLE:
    python3 check_structure.py 2gb1.pdb


CATEGORIES: core/calc/structural/
KEYWORDS:   PDB input; structural properties
GROUP: Structure calculations

  """)
    sys.exit()

N_gaps = 0
structure = Pdb(sys.argv[1],"",False).create_structure(0)
for code in structure.chain_codes() :
    chain = structure.get_chain(code)
    r_prev, r = chain[0], chain[0]
    prev_ca = r.find_atom(" CA ")
    the_ca = prev_ca
    for ires in range(1,chain.size()):
        r_prev = r
        prev_ca = the_ca
        if not prev_ca:
            continue
        r = chain[ires]
        the_ca = r.find_atom(" CA ")
        if not the_ca:
            continue

        d = the_ca.distance_to(prev_ca)
        if d > 4.0:
            N_gaps += 1
            print("chain %c: too long distance between CA of  %s%d and %s%d residue: %6.3f" %
                  (r.owner().id(), r_prev.residue_type().code3, r_prev.id(), r.residue_type().code3, r.id(), d))

print("# Summary for %s: n_gaps - %d" % (sys.argv[1], N_gaps))
