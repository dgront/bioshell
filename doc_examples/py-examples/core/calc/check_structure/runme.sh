#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"

echo -e "#Running python3 ./check_structure.py  test_inputs/3dcg.pdb"
python3 ./check_structure.py test_inputs/3dcg.pdb


echo -e "#Running python3 ./check_structure.py  test_inputs/2gb1.pdb"
python3 ./check_structure.py test_inputs/2gb1.pdb

