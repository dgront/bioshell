import sys, math

sys.path.append("../../../../../bin/")

from pybioshell.core.data.basic import Vec3
from pybioshell.core.data.io import Pdb
from pybioshell.core.data.structural.selectors import SelectResidueByName
from pybioshell.core.protocols import copy_selected_atoms, copy_selected_coordinates
from pybioshell.core.calc.structural.transformations import *
from pybioshell.utils import LogManager

LogManager.INFO()
IF_ROW_OUTPUT = False

if len(sys.argv) < 3:
    print("""

Calculates rototranslation transformation that superimposes a ligand molecule from one reference frame to another.
As an output, prints the rototranslation

USAGE:
    python3 ligand_rototranslation.py  ligand-code3  reference.pdb input1.pdb [input2.pdb ...]


EXAMPLE:
    python3 ligand_rototranslation.py CAM 2m56-ref.pdb 00199.pdb 00963.pdb 04473.pdb


CATEGORIES: core/calc/structural/transformations/CrmsdOnVec3
KEYWORDS:   PDB input; ligand; crmsd
GROUP: Structure calculations

  """)
    sys.exit()

rms = CrmsdOnVec3()
select_ligand = SelectResidueByName(sys.argv[1])
ref_strctr = Pdb(sys.argv[2], "").create_structure(0)
ref_atoms = copy_selected_coordinates(ref_strctr, select_ligand)

for f_model in sys.argv[3:]:
    strctr = Pdb(f_model, "").create_structure(0)
    ligand_atoms = copy_selected_coordinates(strctr, select_ligand)
    crsmd_val = rms.crmsd(ligand_atoms, ref_atoms, len(ref_atoms), True)  # superimpose a reference onto a model
    # crsmd_val = rms.crmsd(ref_atoms, ligand_atoms, len(ref_atoms), True)      # superimpose a model onto a reference
    if IF_ROW_OUTPUT:
        print("%7.4f %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f" %
          (rms.rot_x().x, rms.rot_x().y, rms.rot_x().z,
           rms.rot_y().x, rms.rot_y().y, rms.rot_y().z,
           rms.rot_z().x, rms.rot_z().y, rms.rot_z().z,
           rms.tr_before().x, rms.tr_before().y, rms.tr_before().z,
           rms.tr_after().x, rms.tr_after().y, rms.tr_after().z))
    else:
        print(rms)
