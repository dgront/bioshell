import sys, math

from pybioshell.core.data.io import find_pdb
from pybioshell.core.data.basic import Vec3Cubic

from pybioshell.std import vector_core_data_basic_Vec3Cubic

if len(sys.argv) < 3 :
    print("""

Calculates radial distribution function for a trajectory from a molecular simulation.   


USAGE:
    python3 radial_distribution_function.py input_tra.pdb cutoff 


EXAMPLE:
    python3 radial_distribution_function.py ar_tra.pdb 27.6214


CATEGORIES: core/data/basic/Vec3Cubic
KEYWORDS:   PDB input; structural properties
GROUP: Structure calculations;

  """)
    sys.exit()


pdb = find_pdb(sys.argv[1], "./")
n_atoms = pdb.count_atoms(0)
cutoff = float(sys.argv[2])

Vec3Cubic.set_box_len(cutoff)
xyz = vector_core_data_basic_Vec3Cubic()
for i in range(n_atoms) : xyz.append( Vec3Cubic() )
histogram = [0 for i in range(121)]
for i_model in range(0, pdb.count_models()) :
  # print i_model
  pdb.fill_structure(i_model, xyz)
  for i_atom in range(n_atoms) :
      for j_atom in range(i_atom) :
          d = xyz[i_atom].closest_distance_square_to(xyz[j_atom],12*12)
          if d < 144 : histogram[ int(math.sqrt(d)*10) ] += 1

for i in range(1,120) : print("%5f %7.2f" % (i*0.1, histogram[i]/(i*i*0.01)))
