#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"

echo -e "#Running python3 ./crmsd_on_c-alpha.py test_inputs/1cey.pdb"
python3 ./crmsd_on_c-alpha.py test_inputs/1cey.pdb

echo -e "#Running python3 ./crmsd_on_c-alpha.py test_inputs/2gb1-model1.pdb	test_inputs/2gb1-model2.pdb	test_inputs/2gb1-model3.pdb	test_inputs/2gb1-model4.pdb"
python3 ./crmsd_on_c-alpha.py test_inputs/2gb1-model1.pdb	test_inputs/2gb1-model2.pdb	test_inputs/2gb1-model3.pdb	test_inputs/2gb1-model4.pdb


