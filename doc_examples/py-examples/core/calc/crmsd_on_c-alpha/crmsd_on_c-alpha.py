import sys, math

from pybioshell.core.data.io import Pdb
from pybioshell.core.data.basic import Vec3
from pybioshell.std import vector_core_data_basic_Vec3

from pybioshell.core.calc.structural.transformations import *
from pybioshell.utils import LogManager

LogManager.INFO()

if len(sys.argv) < 2:
    print("""

Calculates cRMSD (coordinate Root-Mean-Square Deviation) value on C-alpha coordinates. 
If one file is given, each-vs-each cRMSD between models is calculated.
If two or more file is given, crmsd for first pdb vs. the rest is calculated.


USAGE:
    python3 crmsd_on_c-alpha.py file1.pdb [file2.pdb...]


EXAMPLE:
    python3 crmsd_on_c-alpha.py 1cey.pdb


CATEGORIES: core/calc/structural/transformations/Crmsd
KEYWORDS:   PDB input; crmsd
GROUP: Structure calculations
IMG: ap_Crmsd_deepteal_brown_1.png


  """)
    sys.exit()

rms = CrmsdOnVec3()

if len(sys.argv) == 2:  # --- The case of each-vs-each calculations between models of a single PDB file

    pdb = Pdb(sys.argv[1], "is_ca", False)
    n_atoms = pdb.count_atoms(0)

    structure = pdb.create_structure(0)
    models = []

    for i_model in range(0, pdb.count_models()):
        xyz = vector_core_data_basic_Vec3()
        for i in range(n_atoms): xyz.append(Vec3())
        models.append(xyz)

    for i_model in range(0, pdb.count_models()):
        pdb.fill_structure(i_model, models[i_model])
        for j in range(i_model):
            try:
                print("%2d %2d %6.3f" % (i_model, j, rms.crmsd(models[i_model], models[j], len(models[j]))))
            except:
                sys.stderr.write(str(sys.exc_info()[0]) + " " + str(sys.exc_info()[1]))

else:  # --- The case when two or more PDB files are given, calculates first vs. the rest

    pdb = Pdb(sys.argv[1], "is_ca", False)
    n_atoms = pdb.count_atoms(0)
    structure = pdb.create_structure(0)

    xyz = vector_core_data_basic_Vec3()
    for i in range(n_atoms): xyz.append(Vec3())
    pdb.fill_structure(0, xyz)

    for pdb_fname in sys.argv[2:]:
        other_pdb = Pdb(pdb_fname, "is_ca", False)
        other_structure = other_pdb.create_structure(0)
        if n_atoms != other_pdb.count_atoms(0):
            print("The two structures have different number of CA atoms!\n")
        other_xyz = vector_core_data_basic_Vec3()
        for i in range(n_atoms): other_xyz.append(Vec3())
        other_pdb.fill_structure(0, other_xyz)
        try:
            print("%s: %6.3f" % (pdb_fname.split("/")[-1].split(".")[0], rms.crmsd(xyz, other_xyz, n_atoms)))
        except:
            sys.stderr.write(str(sys.exc_info()[0]) + " " + str(sys.exc_info()[1]))
