#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"
echo -e "#Running python3 ./crmsd_on_ligands.py HEM test_inputs/5ofq.pdb test_inputs/4rm4.pdb"


python3 crmsd_on_ligands.py HEM test_inputs/5ofq.pdb test_inputs/4rm4.pdb


