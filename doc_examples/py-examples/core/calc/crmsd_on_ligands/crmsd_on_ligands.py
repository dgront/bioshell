import sys, math

from pybioshell.core.data.io import Pdb
from pybioshell.std import vector_core_data_basic_Vec3
from pybioshell.core.calc.structural.transformations import *
from pybioshell.utils import LogManager
LogManager.INFO()

if len(sys.argv) < 3 :
    print("""

Calculates cRMSD (coordinate Root-Mean-Square Deviation) value on all atoms of a ligand conformations.

This scripts reads one or more PDB files, extracts all ligands that matches a given 
three-letter code and calculates cRMSD between them on all atoms.


USAGE:
    python3 crmsd_on_ligands.py ligand input1.pdb [input2.pdb]


EXAMPLE:
    python3 crmsd_on_ligands.py HEM 5ofq.pdb 4rm4.pdb


CATEGORIES: core/calc/structural/transformations/CrmsdOnVec3
KEYWORDS:   PDB input; ligand; crmsd
GROUP: Structure calculations

  """)
    sys.exit()

rms = CrmsdOnVec3()

pdb_codes = []          # --- PDB code for every input structure: for informative output
structures = []         # --- contains all input structures
residues = []           # --- ligand residue objects (to keep information about residue number and chain)
atoms_by_ligand = []    # --- a list of atoms for every ligand
for pdb_code in sys.argv[2:] :
  pdb = Pdb(pdb_code,"",False)
  structure = pdb.create_structure(0)
  structures.append(structure)
  for ic in range(structure.count_chains()) :
    chain = structure[ic]
    for ir in range(chain.terminal_residue_index() + 1,chain.size()) :
        resid = chain[ir]
        code3 = resid.residue_type().code3
        if code3 != sys.argv[1] : continue # Skip other ligands
        residues.append(resid)
        pdb_codes.append(pdb_code)
        atoms = vector_core_data_basic_Vec3()
        for ia in range(resid.count_atoms()):
            atoms.append(resid[ia])
        atoms_by_ligand.append(atoms)

for i_ligand in range(0, len(atoms_by_ligand)) :
      ir = residues[i_ligand]
      for j_ligand in range(i_ligand):
          jr = residues[j_ligand]
          crmsd_val = rms.crmsd(atoms_by_ligand[i_ligand], atoms_by_ligand[j_ligand], len(atoms_by_ligand[j_ligand]))
          print("%s %4d %3s %c - %s %4d %3s %c : %7.3f" % 
            (pdb_codes[i_ligand], ir.id(), ir.residue_type().code3, ir.owner().id(), 
            pdb_codes[j_ligand], jr.id(), jr.residue_type().code3, jr.owner().id(), crmsd_val))
