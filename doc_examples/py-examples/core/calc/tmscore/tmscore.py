import sys, math

from pybioshell.core.data.io import Pdb
from pybioshell.core.data.basic import Vec3
from pybioshell.std import vector_core_data_basic_Vec3

from pybioshell.core.calc.structural.transformations import *
from pybioshell.utils import LogManager

LogManager.INFO()

if len(sys.argv) < 2:
    print("""

Calculates TMScore value on two or more structures. 
First file is a referance structure and the second can be multimodel pdb.
Calculations is running between reference structure and every model from the second file.


USAGE:
    python3 tmscore.py file1.pdb [file2.pdb...]


EXAMPLE:
    python3 tmscore.py 2gb1-model1.pdb 2gb1-model2.pdb


CATEGORIES: core/calc/structural/transformations/TMScore
KEYWORDS:   PDB input; TMScore
GROUP: Structure calculations


  """)
    sys.exit()


if len(sys.argv) == 3: 

    pdb = Pdb(sys.argv[1], "is_not_water", False)
    n_atoms = pdb.count_atoms(0)
    ref_structure = pdb.create_structure(0)
    ref_xyz = vector_core_data_basic_Vec3()
    for i in range(n_atoms): ref_xyz.append(Vec3())
    pdb.fill_structure(0, ref_xyz)

    pdb = Pdb(sys.argv[2], "is_not_water", False)
    n_atoms = pdb.count_atoms(0)

    structure = pdb.create_structure(0)
    models = []

    for i_model in range(0, pdb.count_models()):
        xyz = vector_core_data_basic_Vec3()
        for i in range(n_atoms): xyz.append(Vec3())
        models.append(xyz)

    for i_model in range(0, pdb.count_models()):
        pdb.fill_structure(i_model, models[i_model])
        tmscore = TMScore(models[i_model],ref_xyz)
        try:
          print("%2d %6.3f" % (i_model,tmscore.tmscore()))
        except:
          sys.stderr.write(str(sys.exc_info()[0]) + " " + str(sys.exc_info()[1]))