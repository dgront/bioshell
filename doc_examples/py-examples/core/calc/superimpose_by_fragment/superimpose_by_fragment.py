import sys, math, os

from pybioshell.core.data.io import Pdb, write_pdb
from pybioshell.core.data.basic import Vec3
from pybioshell.std import vector_core_data_basic_Vec3

from pybioshell.core.calc.structural.transformations import *
from pybioshell.utils import LogManager

REFERENCE_FROM, REFERENCE_TO = 23, 32 # 25, 390

LogManager.INFO()

if len(sys.argv) < 3:
    print("""

Superimposes protein structures based on a structural fragment.

 
This script superimposes all models given at command line (at least one) on the reference structure.
The superimposition is based on C-alpha atoms of residues from %d to %d. If you need another fragment,
change these values in the script!

USAGE:
    python3 superimpose_by_fragment.py reference.pdb model1.pdb [model2.pdb...]


EXAMPLE:
    python3 superimpose_by_fragment.py 2gb1.pdb 2gb1-model1.pdb 2gb1-model2.pdb


CATEGORIES: core/calc/structural/transformations/Crmsd
KEYWORDS:   PDB input; crmsd
GROUP: Structure calculations


  """ % (REFERENCE_FROM, REFERENCE_TO))
    sys.exit()

rms = CrmsdOnVec3()

pdb = Pdb(sys.argv[1], "", False)              # --- read the reference PDB file - only C-alfas
structure = pdb.create_structure(0)
n_atoms = REFERENCE_TO - REFERENCE_FROM + 1
xyz = vector_core_data_basic_Vec3()			        # --- std::vector of Vec3 object is required to calculate superimposition
for i in range(REFERENCE_FROM, REFERENCE_TO+1):		# --- fill the vector with the selected reference coordinates
    r = structure[0][i]                             # --- i-th residue of the first chain
    xyz.append(r.find_atom(" CA "))

#out_fname = "rot.pdb"
for pdb_fname in sys.argv[2:]:				        # --- iterate over all models
    out_fname = "rot-" + pdb_fname.split(os.path.sep)[-1]
    other_pdb = Pdb(pdb_fname, "", False)
    other_structure = other_pdb.create_structure(0)
    other_xyz = vector_core_data_basic_Vec3()		# --- container for coordinates of a model

    try:
        for i in range(REFERENCE_FROM, REFERENCE_TO+1):	# --- fill the vector with the selected  coordinates
            r = other_structure[0][i]                   # --- i-th residue of the first chain
            other_xyz.append(r.find_atom(" CA "))
        rms_val = rms.crmsd(xyz, other_xyz, n_atoms, True)
        rms.apply_inverse(other_structure)
        write_pdb(other_structure, out_fname, 0)
    except:
        sys.stderr.write(str(sys.exc_info()[0]) + " " + str(sys.exc_info()[1]))
