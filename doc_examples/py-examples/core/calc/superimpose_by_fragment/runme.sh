#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"

echo -e "#Running python3 ./superimpose_by_fragment.py  test_inputs/2gb1.pdb test_inputs/2gb1-model1.pdb	test_inputs/2gb1-model2.pdb	test_inputs/2gb1-model3.pdb	test_inputs/2gb1-model4.pdb"
python3 ./superimpose_by_fragment.py test_inputs/2gb1.pdb test_inputs/2gb1-model1.pdb	test_inputs/2gb1-model2.pdb	test_inputs/2gb1-model3.pdb	test_inputs/2gb1-model4.pdb

mkdir -p outputs_from_test
mv rot-*.pdb outputs_from_test
