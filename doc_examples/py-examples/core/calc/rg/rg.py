import sys, math

from pybioshell.core.data.io import find_pdb
from pybioshell.core.data.basic import Vec3
from pybioshell.std import vector_core_data_basic_Vec3

from pybioshell.core.calc.structural import *
from pybioshell.utils import LogManager
LogManager.INFO()



if len(sys.argv) < 2 :
    print("""

Calculates the radius of gyration from given pdb file coordinates.


USAGE:
    python3 rg.py input.pdb


EXAMPLE:
    python3 rg.py 1cey.pdb


CATEGORIES: core/calc/structural/calculate_Rg_square
KEYWORDS:   PDB input; structural properties
GROUP: Structure calculations;

  """)
    sys.exit()

for pdb_fname in sys.argv[1:] :
    pdb=find_pdb(pdb_fname, "./")
    n_atoms = pdb.count_atoms(0)
  
    structure = pdb.create_structure(0)
    models=[]

    for i_model in range(0, pdb.count_models()) :
      xyz=vector_core_data_basic_Vec3()
      for i in range(n_atoms) : xyz.append( Vec3() )
      models.append(xyz)

    for i_model in range(0, pdb.count_models()) :
      pdb.fill_structure(i_model, models[i_model])
      try:
        print("Rg for %s, model # %5d : %7.3f" % (pdb_fname.split("/")[-1].split(".")[0],i_model,
    	math.sqrt(calculate_Rg_square(models[i_model][0], models[i_model][n_atoms-1]))))
      except:
        sys.stderr.write(str(sys.exc_info()[0])+" "+str(sys.exc_info()[1]))



