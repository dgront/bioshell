#!/bin/bash

export BIOSHELL_DATA_DIR="../../../../../data"
export PYTHONPATH="../../../../../bin"

for i in 2gb1.pdb 2kwi.pdb 4mcb.pdb 5edw.pdb
do
  echo -e "#Running python3 ./phi_psi.py  test_inputs/"$i
  python3 ./phi_psi.py test_inputs/$i
done


