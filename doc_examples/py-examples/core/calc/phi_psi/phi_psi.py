import sys

from pybioshell.core.data.io import Pdb
from pybioshell.core.calc.structural import evaluate_phi, evaluate_psi

if len(sys.argv) < 2 :
  print("""

Calculates Phi, Psi dihedral angles of a given input PDB structure.


USAGE:
    python3 phi_psi.py input.pdb


EXAMPLE:
    python3 phi_psi.py 2gb1.pdb


CATEGORIES: core/calc/structural/evaluate_phi; core/calc/structural/evaluate_psi
KEYWORDS:   PDB input; structural properties
GROUP: Structure calculations
IMG: Toluen_dihedral_flat_angle.png

  """)
  sys.exit()

factor = 180.0/3.14159
structure = Pdb(sys.argv[1],"",False).create_structure(0)
for code in structure.chain_codes() :
  chain = structure.get_chain(code)
  n_res = chain.count_aa_residues()
  for i_res in range(1,n_res-1) :
    try :
      r = chain[i_res]
      r_prev = chain[i_res-1]
      r_next = chain[i_res+1]
      phi = evaluate_phi(r_prev,r)
      psi = evaluate_psi(r,r_next)
      print("%d %s %c %7.2f %7.2f" % (r.id(), r.residue_type().code3, r.owner().id(),phi*factor, psi*factor))
    except :
      print("can't evaluate Phi/Psi at position",i_res, file=sys.stderr)
