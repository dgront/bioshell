#include <utils/UnitTests.hh>
#include <utils/base64_encoding.hh>

using utils::UnitTests;
using utils::DefineTest;

DefineTest encode_base64("encode and decode data in Base64 format", []() {

    bool test_result = true;
    std::vector<unsigned char> data{1, 2, 3, 123, 232, 255, 76};

    std::string encoded = utils::base64_encode(data);
    test_result &= UnitTests::assert_equals(std::string{"AQIDe+j/TA=="}, encoded);

    std::vector<unsigned char> ret = utils::base64_decode(encoded);
    for (int i = 0; i < data.size(); ++i)
      test_result &= UnitTests::assert_equals(data[i], ret[i]);

    //  ---------- test if the output likes like we extect
    return test_result;
});


RUN_TESTS
