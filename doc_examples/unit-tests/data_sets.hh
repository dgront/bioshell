#ifndef UNIT_TESTS_CORE_data_sets_HH
#define UNIT_TESTS_CORE_data_sets_HH

#include <string>

/// Two ligands CAK and NAD with cobalt ion in a single PDB-formatted text
extern std::string two_ligands_as_pdb;

/// 03G monmer in PDB format
extern std::string monomer_03G_as_pdb;

/// 9YZ monmer in PDB format
extern std::string monomer_9YZ_as_pdb;

/// 9ZB monmer in PDB format
extern std::string monomer_9ZB_as_pdb;

/// Full 2GB1 deposit as PDB text - just coordinates, no header
extern std::string pdb_2gb1;

/// Full 1CEY deposit as PDB text - just coordinates, no header
extern std::string pdb_1cey;

/// Full 6ATS deposit as PDB text - coordinates plus HELIX fields from the header
extern std::string pdb_6ats;

/// Benzene molecule in MOL2 format
extern std::string mol2_benzene;

/// Test MSA data in HSSP format
extern std::string hssp_1ans;

/// Few residues from 2gb1 in PDB format
extern std::string partial_2gb1;

/// Just a single ALA residue in PDB format
extern std::string ala_in_pdb;

/// Three Ar atoms in PDB format
extern std::string cl_in_pdb;

/// An ALA-GLY dimer in PDB format
extern std::string dimer_in_pdb;

/// Test SEQ format
extern std::string seq_format;

/// long atom numbers in CONNECT lines
extern std::string connects_5lbo;
#endif //UNIT_TESTS_CORE_data_sets_HH
