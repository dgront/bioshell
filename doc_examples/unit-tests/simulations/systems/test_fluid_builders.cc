#include <utils/UnitTests.hh>

#include <core/data/basic/Vec3I.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/BioShellEnvironment.hh>

#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/systems/CartesianChains.hh>
#include <simulations/systems/BuildFluidSystem.hh>
#include <simulations/systems/BuildPolymerChain.hh>
#include <simulations/systems/SingleAtomType.hh>
#include <simulations/forcefields/mm/MMForceField.hh>

#include <simulations/forcefields/mm/WaterModelParameters.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace simulations::systems;
using namespace core::data::structural;
using core::data::basic::Vec3I;

DefineTest test_argon_system( "build system with Ar atoms", []() {

    bool test_result = true;
    PdbAtom_SP a = std::make_shared<PdbAtom>(1," AR ");
    Vec3I::set_box_len(9.0);
    AtomTypingInterface_SP ar_type = std::make_shared<SingleAtomType>(" AR");
    CartesianChains system(ar_type,27);
    auto grid = std::make_shared<SimpleCubicGrid>(9.0, 27);
    grid->center(0,0,0);
    BuildFluidSystem::generate(system, *a, grid);

    test_result &= UnitTests::assert_equals(-3, system[0].x(), 0.000001);
    test_result &= UnitTests::assert_equals(-3, system[0].y(), 0.000001);
    test_result &= UnitTests::assert_equals(-3, system[0].z(), 0.000001);
    test_result &= UnitTests::assert_equals(3, system[26].x(), 0.000001);
    test_result &= UnitTests::assert_equals(3, system[26].y(), 0.000001);
    test_result &= UnitTests::assert_equals(3., system[26].z(), 0.000001);

    return test_result;
});

DefineTest test_tip3p_system( "build system with TIP3P water", []() {

    bool test_result = true;
    using simulations::forcefields::WaterModelParameters;
    using namespace simulations::forcefields::mm;

    WaterModelParameters::load_models();
    WaterModelParameters tip3p = WaterModelParameters::get_model("TIP3P");

    Residue_SP hoh = tip3p.create_residue();
    Chain_SP c = std::make_shared<Chain>("A");
    c->push_back(hoh);
    Structure_SP s = std::make_shared<Structure>("water");
    s->push_back(c);

    Vec3I::set_box_len(9.0);
    MMForceField mmff("AMBER03");
    const MMAtomTyping_SP atom_typing_sp = (mmff.mm_atom_typing());
    CartesianChains system(atom_typing_sp, 27*3);

    // --- generate the system of 3x3x3 TIP3P water molecules
    PointGridGenerator_SP grid = std::make_shared<SimpleCubicGrid>(9.0, 27);
    grid->center(0,0,0);
    BuildFluidSystem::generate(system, *hoh, grid);

    // --- The test below calculates minimum distance between a water molecule and any other, must be equal 3.0A
    Vec3I cm(system[0]);
    for (int i = 1; i < 27; ++i) {
      double min = 10;
      cm += system[i*3];
      for (int j = 0; j < i; ++j) min = std::min(min,system[i*3].distance_to(system[j*3]));
      test_result &= UnitTests::assert_equals(3.0, min, 0.00001);
    }
    cm /= 27;
    // --- here we check CM of oxygen atoms - should be in the center of the box (except X which is shifted)
    test_result &= UnitTests::assert_equals(-0.0572549, cm.x(), 0.0001);
    test_result &= UnitTests::assert_equals(0.0, cm.y(), 0.0001);
    test_result &= UnitTests::assert_equals(0.0, cm.z(), 0.0001);

    // --- check if the number of residues and chains is OK
    test_result &= UnitTests::assert_equals(core::index2(27), system.count_residues());
    test_result &= UnitTests::assert_equals(core::index2(1), system.count_chains());

    // --- check if the residue at index = 4 has the rights atoms (i.e. check if atom numbering is consistent)
    test_result &= UnitTests::assert_equals(core::index4(12),system.atoms_for_residue(4).first_atom);
    test_result &= UnitTests::assert_equals(core::index4(14),system.atoms_for_residue(4).last_atom);

    // --- check if the atom 4 belongs to the residue 1 (i.e. check if atom numbering is consistent)
    test_result &= UnitTests::assert_equals(core::index2(1),system.residue_for_atom(4));
    return test_result;
});


DefineTest test_tip3p_structure( "build a Structure object with TIP3P water molecules", []() {

    bool test_result = true;
    using simulations::forcefields::WaterModelParameters;

    Vec3I::set_box_len(9.0);

    WaterModelParameters::load_models();
    WaterModelParameters tip3p = WaterModelParameters::get_model("TIP3P");

    // --- generate the system of 3x3x3 TIP3P water molecules
    Residue_SP hoh = tip3p.create_residue();
    PointGridGenerator_SP grid = std::make_shared<SimpleCubicGrid>(9.0, 27);
    Structure_SP s = BuildFluidSystem::build_structure(*hoh, grid);

    // --- The test below calculates minimum distance between a water molecule and any other, must be equal 3.0A
    Vec3 cm;
    for (auto ai = s->first_const_atom(); ai != s->last_const_atom(); ai++) {
      cm += (**ai);
      // --- Test whether i-th residue has atoms i, i+1, i+2 and i+3; test it by dividing by 4
      test_result &= UnitTests::assert_equals((**ai).owner()->id()-1, int(((**ai).id()-1) / 3));
    }
    cm /= (3*27);

    // --- here we check CM of oxygen atoms - should be in the center of the box (except X which is shifted)
    test_result &= UnitTests::assert_equals(4.5, cm.x, 0.0001);
    test_result &= UnitTests::assert_equals(4.5, cm.y, 0.0001);
    test_result &= UnitTests::assert_equals(4.5, cm.z, 0.0001);

    return test_result;
});

DefineTest test_BuildPolymerChain( "initialize CartesianChains coordinates with BuildPolymerChain", []() {

    bool test_result = true;
    using namespace simulations::systems;

    Vec3I::set_box_len(20.0);

    // --- generate a system of CA-only chains
    AtomTypingInterface_SP ca_typing = std::make_shared<SingleAtomType>(" CA ");
    CartesianChains system(ca_typing,100);
    BuildPolymerChain builder(system);
    builder.generate(1.6, 2.2);
    for (int i = 1; i < system.n_atoms; ++i) {
      test_result &= UnitTests::assert_equals(system[i].distance_to(system[i - 1]), 1.6, 0.0001);
      for (int j = 0; j < i - 1; ++j)
        test_result &= UnitTests::assert_true(system[i].distance_to(system[j]) >= 2.2, "Resulting chain has a clash!");
    }

    return test_result;
});

RUN_TESTS
