#include <core/index.hh>
#include <utils/UnitTests.hh>
#include <simulations/systems/ising/Ising2D.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace simulations::systems::ising;


DefineTest test_Ising2D_serialisation( "serialise and deserialize Ising model", []() {

    bool test_result = true;
    Ising2D<core::index1, core::index2> system(3,3);
    for(core::index2 i=0;i<system.count_spins();i+=2)
      system.flip_spin(i);
    std::string out = system.write();
    test_result &= UnitTests::assert_equals(std::string{"AQABAAEAAQAB"}, out);
    system.load(out);
    for (size_t i = 0; i < system.count_spins(); i += 2)
      test_result &= UnitTests::assert_equals(core::index1(1), system[i]);
    for (size_t i = 1; i < system.count_spins(); i += 2)
      test_result &= UnitTests::assert_equals(core::index1(0), system[i]);
//    std::cout << out<<"\n";
    return test_result;
});

DefineTest test_Ising2D_energy( "test energy of Ising2D model", []() {

    bool test_result = true;
    std::vector<core::index1> ising1 = {0,1,1,0};
    std::vector<core::index1> ising2 = {1,1,1,1};
    std::vector<core::index1> ising3 = {1,1,0,0};
    Ising2D<core::index1, core::index2> system(2,2);
    system.set_all(ising1);
    test_result &= UnitTests::assert_equals(8.0, system.calculate(), 0.0000001);
    system.set_all(ising2);
    test_result &= UnitTests::assert_equals(-8.0, system.calculate(), 0.0000001);
    system.set_all(ising3);
    test_result &= UnitTests::assert_equals(0.0, system.calculate(), 0.0000001);

    return test_result;
});

RUN_TESTS
