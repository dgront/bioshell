#include <memory>

#include <core/index.hh>
#include <utils/UnitTests.hh>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/Structure.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>
#include <simulations/forcefields/ConstByAtomEnergy.hh>
#include <simulations/movers/TranslateAtom.hh>
#include <simulations/sampling/MetropolisAcceptanceCriterion.hh>
#include <simulations/movers/TailBeadMove.hh>
#include <simulations/movers/PerturbChainFragment.hh>

#include "../../data_sets.hh"

using utils::UnitTests;
using utils::DefineTest;

using namespace simulations::systems::surpass;
using core::data::basic::Vec3Cubic;


bool is_surpass_backup_identical_to_system(const SurpassAlfaChains & system, const SurpassAlfaChains & backup) {
    bool test_result = true;
    // ---------- Check whether backup is identical to the system
    for (int i = 0; i < system.n_surpass_atoms; ++i) {
        test_result &= UnitTests::assert_true(system.surpass_atoms()[i] == backup.surpass_atoms()[i],
                                              utils::string_format("the system should be identical to it's backup copy after a move, position %d",i));
    }

    return test_result;
}

DefineTest test_2gb1_surpass_alfa_chains( "build SURPASS model from 2gb1 data", []() {

    bool test_result = true;

    core::data::io::Pdb reader(pdb_2gb1, core::data::io::is_ca, core::data::io::keep_all, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    (*strctr)[0]->annotate_ss("CEEEEEECCCCCCEEEEEECCCHHHHHHHHHHHHHCCCCCCEEEEECCCCEEEEEC");
    auto model = std::make_shared<SurpassAlfaChains>(*strctr);
    SurpassAlfaChains backup(*model);
    test_result &= is_surpass_backup_identical_to_system(*model, backup);
    test_result &= UnitTests::assert_equals(-8.515,model->surpass_atoms()[0].x(),0.0001);
    test_result &= UnitTests::assert_equals(-1.6535,model->surpass_atoms()[0].y(),0.0001);
    test_result &= UnitTests::assert_equals(3.94425,model->surpass_atoms()[0].z(),0.0001);
    test_result &= UnitTests::assert_equals(1,(int)model->surpass_atoms()[0].atom_type);
    test_result &= UnitTests::assert_equals(2,(int)model->surpass_atoms()[7].atom_type);
    test_result &= UnitTests::assert_equals(0,(int)model->surpass_atoms()[21].atom_type);
    test_result &= UnitTests::assert_equals(1,(int)model->surpass_atoms()[51].atom_type);

    model->prepare_for_scoring();
    test_result &= UnitTests::assert_equals(core::index4(56 - 3), model->n_surpass_atoms);
    core::index4 n_moved=6;

    // --- create a swing mover, energy is constant to have all the move attempts accepted
    simulations::forcefields::ConstByAtomEnergy en(0,0);
    simulations::movers::TranslateAtom mover(*model, backup,en);
    mover.max_move_range(0.3);

    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);
    for (int i = 0; i < 1000; ++i) {
        mover.move(mc);
    }
    test_result &= is_surpass_backup_identical_to_system(*model, backup);
    test_result &= UnitTests::assert_equals(std::string{"EEEEECCCCCCCEEEEECCCCHHHHHHHHHHHHCCCCCCCEEEECCCCCEEEE"},model->surpass_ss2());
 //   for (int i=0;i<model->n_surpass_atoms;++i) std::cout<<(int)model->surpass_atoms()[i].residue_type;
    test_result &= UnitTests::assert_equals(2,(int)model->surpass_atoms()[12].residue_type);


    return test_result;
});

RUN_TESTS