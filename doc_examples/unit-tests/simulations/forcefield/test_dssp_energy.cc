#include <core/index.hh>
#include <utils/UnitTests.hh>

#include <core/data/io/Pdb.hh>
#include <core/BioShellEnvironment.hh>
#include <core/data/structural/Structure.hh>

#include <simulations/systems/CartesianChains.hh>
#include <simulations/forcefields/cartesian/DSSPEnergy.hh>
#include <simulations/systems/SimpleAtomTyping.hh>
#include <simulations/systems/surpass/SurpassAlfaAtomTyping.hh>
#include <simulations/systems/PdbAtomTyping.hh>

#include "../../data_sets.hh"

using utils::UnitTests;
using utils::DefineTest;

utils::SuiteTestName name{"test_dssp_energy.cc"};

DefineTest test_dssp_energy("test DSSP energy for one hydrogen bond", []() {

    bool test_result = true;

    using namespace simulations::forcefields::cartesian;

    core::data::io::Pdb reader(partial_2gb1, core::data::io::is_not_alternative, core::data::io::keep_all, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    std::vector<std::string> atom_vec{" CA "," N  ", " O  "," C  "};
    AtomTypingInterface_SP type = std::make_shared<simulations::systems::SimpleAtomTyping>(simulations::systems::list_3codes_aminoacids(), atom_vec,atom_vec);
    simulations::systems::CartesianChains system(type,*strctr);
    DSSPEnergy energy(system);
    double e = energy.energy(system);
    test_result &= UnitTests::assert_equals(-398.513,e,0.001);

    return test_result;
});

DefineTest test_2gb1_dssp_energy("test DSSP energy for 2gb1 ", []() {

    bool test_result = true;

    using namespace simulations::forcefields::cartesian;

    core::data::io::Pdb reader(pdb_2gb1, core::data::io::is_not_alternative, core::data::io::keep_all, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    std::vector<std::string> atom_vec{" CA "," N  ", " O  "," C  "};
    AtomTypingInterface_SP type = std::make_shared<simulations::systems::SimpleAtomTyping>(simulations::systems::list_3codes_aminoacids(), atom_vec,atom_vec);
    simulations::systems::CartesianChains system(type,*strctr);
    DSSPEnergy energy(system);
    double e = energy.energy(system);
    test_result &= UnitTests::assert_equals(-2607.9,e,0.1);


    return test_result;
});


RUN_TESTS;
