#include <core/index.hh>
#include <utils/UnitTests.hh>

#include <core/data/io/Pdb.hh>
#include <core/BioShellEnvironment.hh>
#include <core/data/structural/Structure.hh>

#include <simulations/systems/CartesianChains.hh>
#include <simulations/forcefields/cartesian/PhiPsiEnergy.hh>
#include <simulations/systems/SimpleAtomTyping.hh>
#include <simulations/systems/AtomTypingInterface.hh>
#include <simulations/systems/PdbAtomTyping.hh>

#include "../../data_sets.hh"

using utils::UnitTests;
using utils::DefineTest;

utils::SuiteTestName name{"test_phipsi_energy.cc"};

DefineTest test_dssp_energy("test phipsi energy for one hydrogen bond", []() {

    bool test_result = true;

    using namespace simulations::forcefields::cartesian;

    core::data::io::Pdb reader(partial_2gb1, core::data::io::is_not_alternative, core::data::io::keep_all, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    simulations::systems::AtomTypingInterface_SP typing = std::make_shared<simulations::systems::PdbAtomTyping>();
    simulations::systems::CartesianChains system(typing,*strctr);
    PhiPsiEnergy energy(system, "-");
    double e = energy.energy(system);
    test_result &= UnitTests::assert_equals(-83.2703,e,0.0001);

    return test_result;
});

DefineTest test_2gb1_dssp_energy("test phipsi energy for 2gb1 ", []() {

    bool test_result = true;

    using namespace simulations::forcefields::cartesian;

    core::data::io::Pdb reader(pdb_2gb1, core::data::io::is_not_alternative, core::data::io::keep_all, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    simulations::systems::AtomTypingInterface_SP typing = std::make_shared<simulations::systems::PdbAtomTyping>();
    simulations::systems::CartesianChains system(typing,*strctr);
    PhiPsiEnergy energy(system, "-");
    double e = energy.energy(system);

    test_result &= UnitTests::assert_equals(-505.271,e,0.001);


    return test_result;
});


RUN_TESTS;
