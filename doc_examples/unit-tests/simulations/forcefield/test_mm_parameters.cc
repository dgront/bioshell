#include <utils/UnitTests.hh>

#include <simulations/forcefields/mm/MMAtomTyping.hh>
#include <simulations/forcefields/mm/MMForceField.hh>
#include <simulations/forcefields/mm/MMBondedParameters.hh>

#include <core/BioShellEnvironment.hh>
#include <simulations/forcefields/mm/MMResidueType.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace simulations::systems;
using namespace simulations::forcefields::mm;


DefineTest test_residue_typing( "read the standard AMBER03 topology file", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    // --- Read amber topology
    std::string topo_fname = core::BioShellEnvironment::from_file_or_db("forcefield/mm/amber03_aminoacids.rtp");
    std::map<std::string, MMResidueType> res_types;
    read_mm_topology_file(topo_fname, res_types);
    test_result &= UnitTests::assert_equals(size_t(93), res_types.size());

    // --- Read AMBER atom types
    std::map<std::string, MMNonBondedParameters> atom_types;
    std::string atom_fname = core::BioShellEnvironment::from_file_or_db("forcefield/mm/amber03_ffnonbonded.itp");
    read_mm_atom_types(atom_fname, atom_types);
    test_result &= UnitTests::assert_equals(size_t(67), atom_types.size());

    // --- Create BioShell's atom typing object
    MMAtomTyping atom_typing(atom_types, res_types);
    test_result &= UnitTests::assert_equals(core::index1(93), atom_typing.count_residue_types());

    return test_result;
});

DefineTest test_bonded_parameters( "read bonds parameters from AMBER03 ff", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    // --- MM force field: bonded energy parameters
    MMForceField mmff("forcefield/mm/amber03_ffnonbonded.itp", "forcefield/mm/amber03_ffbonded.itp",
                      "forcefield/mm/amber03_aminoacids.rtp","forcefield/mm/amber03_translation_rules.rtp");

    std::string bonded_fname = core::BioShellEnvironment::from_file_or_db("forcefield/mm/amber03_ffbonded.itp");
    MMBondedParameters bonded_params{bonded_fname, mmff.mm_atom_typing()};

    return test_result;
});



RUN_TESTS
