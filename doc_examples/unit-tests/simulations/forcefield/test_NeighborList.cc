#include <core/index.hh>
#include <utils/UnitTests.hh>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/Structure.hh>

#include <simulations/systems/CartesianChains.hh>
#include <simulations/forcefields/NeighborList.hh>
#include <simulations/systems/SingleAtomType.hh>
#include <simulations/movers/TranslateAtom.hh>
#include <simulations/sampling/MetropolisAcceptanceCriterion.hh>
#include <simulations/forcefields/ConstByAtomEnergy.hh>


#include "../../data_sets.hh"

using utils::UnitTests;
using utils::DefineTest;

using core::data::basic::Vec3Cubic;

utils::SuiteTestName name{"test_NeighborList.cc"};

DefineTest test_NeighborList("test NeighborList class for 2GB1 in CA representation", []() {

    bool test_result = true;

    core::data::io::Pdb reader(pdb_2gb1, core::data::io::is_ca, core::data::io::only_ss_from_header, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    (*strctr)[0]->annotate_ss("CEEEEEECCCCCCEEEEEECCCHHHHHHHHHHHHHCCCCCCEEEEECCCCEEEEEC");
    simulations::systems::AtomTypingInterface_SP ca_type = std::make_shared<simulations::systems::SingleAtomType>();
    simulations::systems::CartesianChains chains(ca_type,*strctr);
    simulations::systems::CartesianChains backup(chains);

    using namespace simulations::forcefields;
    core::index2 seq_separation = 4;
    NeighborList nb_list(chains.coordinates(),chains.n_atoms,  6, 3);
    NeighborListRules_SP exl_bonded =
        std::dynamic_pointer_cast<NeighborListRules>(std::make_shared<ExcludeSequenceNeighbors>(seq_separation, nb_list));
    nb_list.neighbor_rules(exl_bonded);
    nb_list.update_all();

    test_result &= UnitTests::assert_equals(56,(int)nb_list.n_atoms());
    test_result &= UnitTests::assert_equals(12,(int)nb_list.count_neighbors(2));

    simulations::forcefields::ConstByAtomEnergy en(0, 0);
    simulations::movers::TranslateAtom mover{chains, backup, en};
    mover.max_move_range(0.1);
    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);
    core::data::basic::Vec3I dx{0, 0, 3};
    chains[2]+=dx;
    test_result &= UnitTests::assert_equals(12,(int)nb_list.count_neighbors(2));
    nb_list.update(2);
    test_result &= UnitTests::assert_equals(8,(int)nb_list.count_neighbors(2));
return  test_result;
});


RUN_TESTS
