#include <string>

#include <core/index.hh>
#include <utils/UnitTests.hh>

#include <core/BioShellEnvironment.hh>
#include <simulations/systems/SingleAtomType.hh>
#include <simulations/systems/CartesianChains.hh>

#include <simulations/forcefields/cartesian/ContactEnergy.hh>
#include <simulations/forcefields/cartesian/CAHydrogenBond.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>
#include "../../data_sets.hh"


using utils::UnitTests;
using utils::DefineTest;

utils::SuiteTestName name("test_generic_forcefiels");

DefineTest test_ContactEnergy("test ContactEnergy : evaluate energy of a system of 5 atoms", []() {

    bool test_result = true;

    using namespace simulations::forcefields;
    auto atom_typing_sp = std::make_shared<simulations::systems::SingleAtomType>();
    simulations::systems::CartesianAtoms system(atom_typing_sp, 5);

    simulations::forcefields::cartesian::ContactEnergy cnt(system, 1000, -1, 1, 2, 4.5);

    // ******************** test for clashes ****************
    // ---------- Energy in this fragment is 1000 per pair because all atoms are in [0,0,0] y default so they clash
    // ---------- by default sequence separation should be 2, i.e. pairs like 3-4 pr 5-6 are not scored
    //test_result &= UnitTests::assert_equals(6000.0, cnt.energy(system), 0.0000001); // --- only six pairs
    //cnt->sequence_separation(1);
    test_result &= UnitTests::assert_equals(10000.0, cnt.energy(system), 0.0000001); // --- now 10 pairs

    // ******************** test beyond contacts ****************
    for (int i = 0; i < 5; ++i) system[i].x(i * 5);
    test_result &= UnitTests::assert_equals(0.0, cnt.energy(system), 0.0000001); // --- always 0

    // ********************  contacts ****************
    for (int i = 3; i < 5; ++i) system[i].x(i * 4);
    test_result &= UnitTests::assert_equals(-2, cnt.energy(system), 0.0000001); // --- atoms 3 and 4 interacts

    return test_result;
});

DefineTest test_CAHydrogenBond("test CAHydrogenBond :", []() {

    bool test_result = true;

    using namespace simulations::forcefields;
    core::data::io::Pdb reader(pdb_2gb1, core::data::io::is_ca, core::data::io::only_ss_from_header,  true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    std::string secondary{"CEEEEEECCCCCCEEEEEECCCHHHHHHHHHHHHHCCCCCCEEEEECCCCEEEEEC"};
    auto atyping = std::make_shared<simulations::systems::SingleAtomType>();
    simulations::systems::surpass::SurpassAlfaChains chains(*strctr);
    simulations::forcefields::cartesian::CAHydrogenBond hb_energy(chains, secondary);

    test_result &= UnitTests::assert_equals( -0.0335489, hb_energy.evaluate_raw_energy(chains, 2, 17, false), 0.0000001);
    test_result &= UnitTests::assert_equals( -0.102542,  hb_energy.evaluate_raw_energy(chains, 2, 17, true), 0.000001);

    test_result &= UnitTests::assert_equals(-log( 266.0/ 285.0), hb_energy.sequence_correction(chains[2].residue_type,chains[17].residue_type), 0.0000001);
    test_result &= UnitTests::assert_equals(-log( 422.0 /370.0), hb_energy.sequence_correction(chains[3].residue_type,chains[16].residue_type), 0.0000001);

    return test_result;
});

RUN_TESTS
