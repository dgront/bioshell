#include <core/index.hh>
#include <utils/UnitTests.hh>

#include <core/data/io/Pdb.hh>
#include <core/BioShellEnvironment.hh>
#include <core/data/structural/Structure.hh>

#include <simulations/forcefields/mm/MMBondedParameters.hh>
#include <simulations/forcefields/mm/MMBondEnergy.hh>

#include <simulations/systems/CartesianChains.hh>
#include <simulations/forcefields/mm/MMPlanarEnergy.hh>
#include <simulations/forcefields/mm/MMDihedralEnergy.hh>
#include <simulations/forcefields/mm/MMForceField.hh>
#include <simulations/forcefields/mm/MMNonBonded.hh>

#include "../../data_sets.hh"

using utils::UnitTests;
using utils::DefineTest;

utils::SuiteTestName name{"test_mm_force_field.cc"};

DefineTest test_MMBondType("test MMBondType : load AMBER parameters from DB", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    MMForceField mmff("AMBER03");
    const MMAtomTyping_SP atom_typing_sp = (mmff.mm_atom_typing());

    std::string param_fname = core::BioShellEnvironment::from_file_or_db("forcefield/mm/amber03_ffbonded.itp");
    MMBondedParameters bond_param = MMBondedParameters(param_fname,atom_typing_sp);

    core::index1 idx = atom_typing_sp->atom_type(" CA ", "TRP",
                                             simulations::systems::AtomTypingVariants::STANDARD);
    core::index1 idx2 = atom_typing_sp->atom_type(" C  ", "TRP",
                                                 simulations::systems::AtomTypingVariants::STANDARD);
    core::index1 idx3 = atom_typing_sp->atom_type(" N  ", "TRP",
                                                  simulations::systems::AtomTypingVariants::STANDARD);
    core::index1 idx4 = atom_typing_sp->atom_type(" O  ", "TRP",
                                                  simulations::systems::AtomTypingVariants::STANDARD);

    test_result &= UnitTests::assert_equals(11,(int)idx);
    test_result &= UnitTests::assert_equals(2,(int)idx2);
    test_result &= UnitTests::assert_equals(34,(int)idx3);
    test_result &= UnitTests::assert_equals(41,(int)idx4);

    auto bond_typing = bond_param.find_bond(idx,idx2);
    auto planar_typing = bond_param.find_planar(idx,idx2,idx2);
    auto di_typing = bond_param.find_dihedral(idx3,idx3,idx,idx4);

    core::data::io::Pdb reader(dimer_in_pdb, core::data::io::is_not_alternative, core::data::io::keep_all, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);

    simulations::systems::CartesianChains system(atom_typing_sp, *strctr);

    MMBondEnergy bond_energy(system,bond_param);
    test_result &= UnitTests::assert_equals(19,(int)bond_energy.get_bonds().size());
    test_result &= UnitTests::assert_equals(8.39942,bond_energy.energy(system),0.00001);

    simulations::forcefields::mm::MMPlanarEnergy planar_energy(system, bond_param, bond_energy);
    test_result &= UnitTests::assert_equals(33,(int)planar_energy.get_planar_angles().size());
    test_result &= UnitTests::assert_equals(70.2461,planar_energy.energy(system),0.0001);

    // --- Create molecular mechanic dihedral energy objects
    simulations::forcefields::mm::MMDihedralEnergy dihedral_energy(system, bond_param, bond_energy);
    test_result &= UnitTests::assert_equals(40,(int)dihedral_energy.get_dihedral_angles().size());
    test_result &= UnitTests::assert_equals(19.1021,dihedral_energy.energy(system),0.0001);


    return test_result;
});

DefineTest test_MMNonBonded("test MMNonBonded ", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    // --- Read AMBER atom types
    std::map<std::string, MMNonBondedParameters> atom_types;

    std::string atom_fname = core::BioShellEnvironment::from_file_or_db("forcefield/mm/amber03_ffnonbonded.itp");
    read_mm_atom_types(atom_fname, atom_types);
    std::string param_fname = core::BioShellEnvironment::from_file_or_db("forcefield/mm/amber03_ffbonded.itp");
    MMForceField mmff("AMBER03");

    MMBondedParameters bond_param = MMBondedParameters(param_fname,mmff.mm_atom_typing());
    test_result &= UnitTests::assert_equals(size_t(67), atom_types.size());
    core::data::io::Pdb reader(cl_in_pdb,"",true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    (*strctr)[0]->erase((*strctr)[0]->begin());
    (*strctr)[0]->begin()+=1;
    simulations::systems::CartesianChains system(mmff.mm_atom_typing(), *strctr);
    core::index4 i=0;

    MMBondEnergy bond_energy(system,bond_param);
    MMNonBonded nonbonded_energy(system, bond_energy);
    double en=0.0;
    double r1 = system[0].closest_distance_square_to(system[1]) ;
    double r3 = system[1].closest_distance_square_to(system[0]) ;
    double sigma_i = 4.40104;
    double epsilon_i = 0.4184;
    double q_i = -1*0.0001 - 1.0;
    double FACTOR_KCAL=332.0;
    for (auto r2:{r1,r3}) {
        double r = sqrt(r2);
        double sigma = (sigma_i + sigma_i) * 0.5;
        double r2_s = sigma / r2;
        r2_s = r2_s * r2_s;
        double r6 = r2_s * r2_s * r2_s;
        double r12 = r6 * r6;
        const double epsilon_four = 4.0 * sqrt(epsilon_i * epsilon_i);
        en += epsilon_four * (r12 - r6);
        en += FACTOR_KCAL * q_i * q_i / r;
    }

    test_result &= UnitTests::assert_equals(en/2, nonbonded_energy.energy(system),0.001);



    return test_result;
});

RUN_TESTS
