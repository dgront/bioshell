#include <core/index.hh>
#include <utils/UnitTests.hh>

#include <core/data/io/Pdb.hh>
#include <core/BioShellEnvironment.hh>
#include <core/data/structural/Structure.hh>

#include <simulations/forcefields/mm/MMForceField.hh>
#include <simulations/forcefields/mm/MMResidueType.hh>
#include <simulations/systems/CartesianChains.hh>
#include <simulations/systems/surpass/SurpassAlfaAtomTyping.hh>
#include <simulations/systems/PdbAtomTyping.hh>


#include "../../data_sets.hh"

using utils::UnitTests;
using utils::DefineTest;

utils::SuiteTestName name{"test_atom_typing.cc"};

DefineTest test_MMAtomTyping("test MMAtomTyping : load AMBER parameters from DB", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    MMForceField mmff("AMBER03");
    const MMAtomTyping & atom_typing = *(mmff.mm_atom_typing());

    core::index1 idx = atom_typing.atom_type(" CA ", "TRP",
        simulations::systems::AtomTypingVariants::STANDARD);
    test_result &= UnitTests::assert_equals(int(11), int(idx));
    test_result &= UnitTests::assert_equals(std::string{"CT"}, atom_typing.atom_internal_name(idx));

    // --- check if all residues types are registered
    test_result &= UnitTests::assert_equals(int(93), int(atom_typing.count_residue_types()));

    // --- check if all atop types are registered
    test_result &= UnitTests::assert_equals(int(67), int(atom_typing.count_atom_types()));

    // --- check if residues are properly registered with their variants
    bool has_atom = atom_typing.is_known_atom_type(" OXT", "ALA", simulations::systems::AtomTypingVariants::C_TERMINAL);
    test_result &= UnitTests::assert_true(has_atom, "OXT atom must be present in the C-terminal variant of ALA");
    has_atom = atom_typing.is_known_atom_type(" OXT", "ALA", simulations::systems::AtomTypingVariants::STANDARD);
    test_result &= UnitTests::assert_true(!has_atom, "OXT atom must NOT be present in the standard variant of ALA");

    // --- check if all atoms can be recognized
    core::data::io::Pdb reader(pdb_2gb1, core::data::io::is_not_alternative, core::data::io::keep_all, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    for(auto it = strctr->first_const_atom();it != strctr->last_const_atom();++it) {
      has_atom = atom_typing.is_known_atom_type(**it);
      test_result &= UnitTests::assert_true(has_atom, "unknown atom!");
    }

    return test_result;
});

DefineTest test_CartesianChains("create CartesianChains from 2gb1 and check if atoms are properly typed", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;
    using simulations::systems::AtomTypingVariants;

    MMForceField mmff("AMBER03");
    const MMAtomTyping & atom_typing = *(mmff.mm_atom_typing());

    // --- check if all atoms in 2gb1 can be recognized
    core::data::io::Pdb reader(pdb_2gb1, core::data::io::is_not_alternative, core::data::io::keep_all, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);

    // --- test if the system has the same number of atoms at the source protein
    size_t n_pdb_atoms = std::distance(strctr->first_const_atom(), strctr->last_const_atom());
    simulations::systems::CartesianChains system(mmff.mm_atom_typing(), *strctr);
    test_result &= UnitTests::assert_equals(n_pdb_atoms, size_t(system.n_atoms));
    auto pdb_atoms_it = strctr->first_const_atom();
    for (int i = 0; i < system.n_atoms; ++i) {
      double q = atom_typing.charge(system[i].atom_type,system[i].residue_type);
      test_result &= UnitTests::assert_true(q<=1.0,"all charges in a protein must be not greater than 1.0");
      test_result &= UnitTests::assert_true(q>=-1.0,"all charges in a protein must be not smaller than -1.0");

      core::index1 element = atom_typing.element(system[i].atom_type);
      test_result &= UnitTests::assert_true(element==(**pdb_atoms_it).element_index(),
          utils::string_format("atomic element for atom type %d named %s in structure and system doesn't match: %d vs %d\n",
              system[i].atom_type, atom_typing.atom_internal_name(system[i].atom_type).c_str(),
              (**pdb_atoms_it).element_index(),element));

      const std::string & typed_residue = atom_typing.residue_internal_name(system[i].residue_type);
      const auto & res = *(**pdb_atoms_it).owner();
      const std::string & patched_residue = patch_residue_name(res.residue_type().code3, simulations::systems::AtomTypingInterface::residue_patch(res));
      test_result &= UnitTests::assert_true(typed_residue==patched_residue,"residue names must match");
      ++pdb_atoms_it;
    }
    core::index1 index_n = atom_typing.atom_type(" N  ", "ALA", AtomTypingVariants::STANDARD);
    core::index1 index_ala = atom_typing.residue_type("ALA", AtomTypingVariants::STANDARD);
    test_result &= UnitTests::assert_equals(-0.404773, atom_typing.charge(index_n, index_ala),0.00001);

    index_n = atom_typing.atom_type(" NE1", "TRP", AtomTypingVariants::STANDARD);
    core::index1 index_trp = atom_typing.residue_type("TRP", AtomTypingVariants::STANDARD);
    test_result &= UnitTests::assert_equals(-0.298433, atom_typing.charge(index_n, index_trp),0.00001);

    return test_result;
});

DefineTest test_SurpassAlfaAtomTyping("test SurpassAlfaAtomTyping", []() {

    bool test_result = true;
    auto typing = simulations::systems::surpass::SurpassAlfaAtomTyping();
    core::data::io::Pdb reader(pdb_2gb1, core::data::io::is_ca, core::data::io::keep_all, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    core::index1 residue_type = typing.simulations::systems::AtomTypingInterface::residue_type(**strctr->first_residue());
    test_result &= UnitTests::assert_equals(std::string{"MET"},typing.residue_internal_name(residue_type));
    auto cp = (*strctr)[0];
    residue_type = typing.simulations::systems::AtomTypingInterface::residue_type(*(*cp)[1]);
    test_result &= UnitTests::assert_equals(std::string{"THR"},typing.residue_internal_name(residue_type));
    return test_result;

});

DefineTest test_PdbAtomTyping("test PdbAtomTyping", []() {

    bool test_result = true;
    core::data::io::Pdb reader(pdb_2gb1, core::data::io::keep_all, core::data::io::keep_all, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    auto typing = simulations::systems::PdbAtomTyping("data/forcefield/pdb_atom_types.dat");
    core::index1 residue_type = typing.simulations::systems::AtomTypingInterface::residue_type(**strctr->first_residue());
    test_result &= UnitTests::assert_equals(std::string{"MET"},typing.residue_internal_name(residue_type));
    auto cp = (*strctr)[0];
    auto res = (*cp)[0];
    residue_type = typing.simulations::systems::AtomTypingInterface::residue_type(*(*cp)[1]);
    test_result &= UnitTests::assert_equals(std::string{"THR"},typing.residue_internal_name(residue_type));
    test_result &= UnitTests::assert_equals(1,int(typing.atom_type(*(*res)[0])));
    test_result &= UnitTests::assert_equals(2,(int)typing.atom_type(*(*res)[1]));
    test_result &= UnitTests::assert_equals(3,(int)typing.atom_type(*(*res)[2]));
    test_result &= UnitTests::assert_equals(4,(int)typing.atom_type(*(*res)[3]));
    test_result &= UnitTests::assert_equals(5,(int)typing.atom_type(*(*res)[4]));
    return test_result;

});

RUN_TESTS
