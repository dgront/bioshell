#include <core/index.hh>
#include <utils/UnitTests.hh>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/Structure.hh>

#include <simulations/systems/SingleAtomType.hh>
#include <simulations/observers/cartesian/SimplePdbFormatter.hh>
#include <simulations/observers/cartesian/ExplicitPdbFormatter.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace simulations::systems;
using namespace simulations::observers::cartesian;
using core::data::basic::Vec3;

std::vector<std::string> expected_water{
    "ATOM      1  O   HOH A   1       0.000   0.000  -0.000  1.00  1.00          O   ",
    "ATOM      2  O   HOH A   2       3.100   0.100  -0.100  1.00  1.00          O   ",
    "ATOM      3  O   HOH A   3       6.200   0.200  -0.200  1.00  1.00          O   ",
    "ATOM      4  O   HOH A   4       9.300   0.300  -0.300  1.00  1.00          O   ",
    "ATOM      5  O   HOH A   5      12.400   0.400  -0.400  1.00  1.00          O   "};

DefineTest test_simple_pdb_formatter( "water molecules in PDB format", []() {

    bool test_result = true;

    AtomTypingInterface_SP at_type = std::make_shared<SingleAtomType>(" O  ");
    simulations::systems::CartesianAtoms h2o(at_type, 5);
    for(core::index4 i=0;i<5;++i) {
        h2o[i].set(i*3.1,i*0.1,-i*0.1);
    }

    SimplePdbFormatter fmt(" O  ","HOH","O ");
    std::vector<std::string> output;
    fmt.format(h2o,output);
    test_result &= UnitTests::assert_equals(expected_water, output);

    return test_result;
});

DefineTest test_simple_pdb_formatter_sp( "test AbstractPdbFormatter pointer", []() {

    bool test_result = true;

    AtomTypingInterface_SP at_type = std::make_shared<SingleAtomType>(" O  ");
    simulations::systems::CartesianAtoms h2o(at_type, 5);
    for(core::index4 i=0;i<5;++i) {
        h2o[i].set(i*3.1,i*0.1,-i*0.1);

    }

    std::shared_ptr<AbstractPdbFormatter> fmt_sp =
        std::static_pointer_cast<AbstractPdbFormatter>(std::make_shared<SimplePdbFormatter>(" O  ", "HOH", "O "));

    std::vector<std::string> output;
    fmt_sp->format(h2o,output);
    test_result &= UnitTests::assert_equals(expected_water, output);


    return test_result;
});

std::string test_pdb = R"(ATOM    664  N   SER A  85      26.182   1.264  15.762  1.00 37.63           N
ATOM    665  CA  SER A  85      27.014   2.450  15.938  1.00 38.38           C
ATOM    666  C   SER A  85      27.970   2.285  17.111  1.00 38.04           C
ATOM    667  O   SER A  85      29.139   2.681  17.028  1.00 35.46           O
ATOM    668  CB  SER A  85      26.136   3.683  16.139  1.00 33.00           C
ATOM    669  OG  SER A  85      26.919   4.808  16.500  1.00 29.04           O
ATOM    670  N   SER A  86      27.494   1.699  18.212  1.00 38.27           N
ATOM    671  CA  SER A  86      28.352   1.514  19.379  1.00 39.86           C
ATOM    672  C   SER A  86      29.530   0.602  19.061  1.00 42.28           C
ATOM    673  O   SER A  86      30.654   0.850  19.514  1.00 46.08           O
ATOM    674  CB  SER A  86      27.539   0.953  20.545  1.00 40.83           C
ATOM    675  OG  SER A  86      26.653   1.928  21.067  1.00 45.21           O )";

DefineTest test_explicit_pdb_formatter( "create an explicit PDB format", []() {

    bool test_result = true;
    std::stringstream in(test_pdb);
    core::data::io::Pdb reader(in);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);

    AtomTypingInterface_SP at_type = std::make_shared<SingleAtomType>(" C  ");
    simulations::systems::CartesianAtoms at(at_type, strctr->count_atoms());
    int i = 0;
    for(auto it=strctr->first_atom(); it!=strctr->last_atom();++it) {
      at[i].set(**it);
      ++i;
    }

    std::vector<std::string> expected = utils::split(test_pdb,{'\n'});
    ExplicitPdbFormatter fmt(*strctr);
    std::vector<std::string> output;
    fmt.format(at,output);
    test_result &= UnitTests::assert_equals(expected, output);

    return test_result;
});

RUN_TESTS
