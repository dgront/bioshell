#include <utils/UnitTests.hh>
#include <core/calc/statistics/Random.hh>

#include <simulations/observers/ToHistogramObserver.hh>
#include <simulations/observers/HandleToVector.hh>

#include <simulations/evaluators/CallEvaluator.hh>


using utils::UnitTests;
using utils::DefineTest;

using namespace simulations::observers;
using namespace simulations::evaluators;

utils::SuiteTestName name{"test_observers.cc"};

/// @brief Call operator that will be evaluated by a CallEvaluator in the observe_call_evaluator_to_histogram test
struct RndOP {
  RndOP() : generator(core::calc::statistics::Random::get()), rnd(-0.1, 0.1) {}
  double operator()() { return rnd(generator); }

private:
  core::calc::statistics::Random &generator;
  std::uniform_real_distribution<double> rnd;

};


DefineTest observe_call_evaluator_to_histogram( "observe evaluator to a histogram", []() {

    bool test_result = true;

    RndOP rnd_op;
    Evaluator_SP rnd_eval = std::make_shared<CallEvaluator<RndOP>>(rnd_op, "RndVal");
    auto handler = std::make_shared<HandleToVector>();
    ToHistogramObserver obs(rnd_eval, -0.1, 0.1, 0.025, handler);
    for(int i=0;i<1000;i++) {
      obs.observe();
    }
    obs.finalize();
    test_result &= UnitTests::assert_equals(int(handler->lines.size()), 8);   // --- there should be 8 lines of the histogram
    test_result &= UnitTests::assert_equals(handler->lines[0], std::string{"  -0.100   -0.075 136"}); // --- match the first line

    return test_result;
});


RUN_TESTS
