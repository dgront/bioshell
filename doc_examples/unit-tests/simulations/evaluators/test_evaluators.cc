#include <chrono> // to test Timer evaluator
#include <thread>

#include <utils/UnitTests.hh>

#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/systems/BuildFluidSystem.hh>
#include <simulations/systems/SingleAtomType.hh>

#include <simulations/systems/CartesianChains.hh>
#include <simulations/systems/BuildPolymerChain.hh>

#include <simulations/evaluators/cartesian/CM.hh>
#include <simulations/evaluators/cartesian/RgSquare.hh>
#include <simulations/evaluators/cartesian/REndSquare.hh>
#include <simulations/evaluators/cartesian/CrmsdEvaluator.hh>
#include <simulations/evaluators/Timer.hh>
#include <simulations/evaluators/EchoEvaluator.hh>
#include <simulations/evaluators/cartesian/EvaluateR1N.hh>

#include <simulations/observers/ToHistogramObserver.hh>
#include <simulations/observers/HandleToVector.hh>

using utils::DefineTest;

using namespace simulations::systems;
using namespace simulations::evaluators;

utils::SuiteTestName name{"test_evaluators.cc"};

CartesianAtoms_SP create_system() {
  // --- generate the system of 3x3x3 argon atoms
  core::data::structural::PdbAtom_SP a = std::make_shared<core::data::structural::PdbAtom>(1," AR ");

  AtomTypingInterface_SP ar_type = std::make_shared<SingleAtomType>(" AR ", " AR");
  CartesianAtoms_SP  system = std::make_shared<CartesianAtoms>(ar_type,27);
  auto grid = std::make_shared<SimpleCubicGrid>(9.0, 27);
  BuildFluidSystem::generate(*system, *a, grid);

  return system;
}

DefineTest test_cm_evaluation( "test evaluations of CM vector", []() {

    bool test_result = true;
    double tolerance = 0.00001;

    CartesianAtoms_SP system = create_system();

    cartesian::CM cm(*system);
    cm.precision(3);
    cm.width(6);
    cartesian::CMDisplacement cmd(*system);

    test_result &= utils::UnitTests::assert_equals(21, int(cm.header().size()));
    cm.width(8);
    test_result &= utils::UnitTests::assert_equals(27, int(cm.header().size()));
    test_result &= utils::UnitTests::assert_equals(27, int(cm.to_string().size()));
    test_result &= utils::UnitTests::assert_equals(std::string("    4.500    4.500    4.500"), cm.to_string());

    std::vector<double> cm_pos = cm.evaluate();
    test_result &= utils::UnitTests::assert_equals(4.5, cm_pos[0], tolerance);
    test_result &= utils::UnitTests::assert_equals(4.5, cm_pos[1], tolerance);
    test_result &= utils::UnitTests::assert_equals(4.5, cm_pos[2], tolerance);

    cm_pos = cmd.evaluate();
    test_result &= utils::UnitTests::assert_equals(0.0, cm_pos[0], tolerance);

    for (int i = 0; i < system->n_atoms; ++i)
      (*system)[i].add(0.5, 0, 0);
    cm_pos = cm.evaluate();
    test_result &= utils::UnitTests::assert_equals(5.0, cm_pos[0], tolerance);
    test_result &= utils::UnitTests::assert_equals(4.5, cm_pos[1], tolerance);
    test_result &= utils::UnitTests::assert_equals(4.5, cm_pos[2], tolerance);

    cm_pos = cmd.evaluate();
    test_result &= utils::UnitTests::assert_equals(0.5, cm_pos[0], tolerance);
    for (int i = 0; i < system->n_atoms; ++i)
      (*system)[i].add(0.0, 0.5, 0);
    cm_pos = cmd.evaluate();
    test_result &= utils::UnitTests::assert_equals(0.7071, cm_pos[0], tolerance);
    for (int i = 0; i < system->n_atoms; ++i)
      (*system)[i].add(0.0, 0, 0.5);
    cm_pos = cmd.evaluate();
    test_result &= utils::UnitTests::assert_equals(0.866022, cm_pos[0], tolerance);

    return test_result;
});



DefineTest test_rg_evaluation( "test evaluations of Rg squared value", []() {

    bool test_result = true;
    double tolerance = 0.00001;

    // --- generate the system of 3x3x3 argon atoms
    CartesianAtoms_SP system = create_system();

    cartesian::RgSquare rg(*system);
    rg.precision(3);
    rg.width(6);

    test_result &= utils::UnitTests::assert_equals(9, int(rg.header().size()));
    rg.width(10);
    test_result &= utils::UnitTests::assert_equals(11, int(rg.header().size()));

    test_result &= utils::UnitTests::assert_equals(18.0, rg.evaluate()[0], tolerance);

    return test_result;
});

DefineTest test_rend_evaluation( "test evaluations of end-to-end distance", []() {

    bool test_result = true;

    CartesianAtoms_SP system = create_system();
    cartesian::REndSquare rend(*system);
    test_result &= utils::UnitTests::assert_equals(10.3923, rend.evaluate()[0], 0.001);

    return test_result;
});

DefineTest test_timer_evaluation( "test Timer evaluator", []() {

    bool test_result = true;

    Timer timer;

    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    double t = timer.evaluate()[0];
    test_result &= utils::UnitTests::assert_equals(0.2, t, 0.01);

    return test_result;
});


DefineTest test_echo_evaluation( "test Echo evaluator", []() {

    bool test_result = true;

    double x;
    EchoEvaluator<double> echo(x, "X");

    for(int i=0;i<10;++i) {
      test_result &= utils::UnitTests::assert_equals(x, echo.evaluate()[0], 0.00001);
      x+= 0.1;
    }

    return test_result;
});

DefineTest test_crmsd_evaluation( "test crmsd evaluator", []() {

    bool test_result = true;

    CartesianAtoms_SP system = create_system();
    using namespace core::data::structural;
    auto s = std::make_shared<Structure>("1234");
    Chain_SP c = std::make_shared<Chain>("A");
    Residue_SP r = std::make_shared<Residue>(1,'A');
    for (int i = 0; i < system->n_atoms; ++i) {
      const auto & c = (*system)[i];
      r->push_back(std::make_shared<PdbAtom>(i + 1, " CA ", c.x(), c.y(), c.z()));
    }
    c->push_back(r);
    s->push_back(c);
    cartesian::CrmsdEvaluator crmsd(s, *system);
    test_result &= utils::UnitTests::assert_equals(0.0, crmsd.evaluate()[0], 0.00001);

    core::calc::statistics::Random &generator = core::calc::statistics::Random::get();
    std::uniform_real_distribution<double> rnd(-0.1, 0.1);
    for(int i=0;i<system->n_atoms;++i) {
      (*system)[i].add(rnd(generator),rnd(generator),rnd(generator));
    }
    test_result &= utils::UnitTests::assert_true(crmsd.evaluate()[0] > 0.01, "crmsd is too small after random perturbation");
    test_result &= utils::UnitTests::assert_true(crmsd.evaluate()[0] < 0.2, "crmsd is too high after random perturbation");

    return test_result;
});


DefineTest test_R1N_evaluation( "test R1N evaluator", []() {

    bool test_result = true;

    core::data::basic::Vec3I::set_box_len(512.0);
    // --- system to be evaluated: a chain of 100 beads
    AtomTypingInterface_SP ca_typing = std::make_shared<SingleAtomType>(" CA ");
    CartesianChains system(ca_typing,{10, 10, 10, 10, 10});
    BuildPolymerChain builder(system);
    builder.generate(1.6, 2.2);

    auto r15_sp = std::make_shared<simulations::evaluators::cartesian::EvaluateR1N>(system,5);
    std::vector<double> val = r15_sp->evaluate();
    test_result &= utils::UnitTests::assert_equals(30, int(val.size()));  // --- we expect 30 R15 distances, 6 from each chain

    auto handler = std::make_shared<HandleToVector>();
    simulations::observers::ToHistogramObserver obs(r15_sp, 0, 15, 0.25, handler);
    obs.observe();
    test_result &= utils::UnitTests::assert_equals(30, int(obs.histogram().count_entries()));  // --- we expect 30 R15 distances, 6 from each chain

    return test_result;
});

RUN_TESTS