#include <utils/UnitTests.hh>

#include <core/calc/structural/angles.hh>
#include <core/data/basic/Vec3I.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/io/Pdb.hh>

#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/systems/BuildFluidSystem.hh>
#include <simulations/systems/SingleAtomType.hh>
#include <simulations/systems/BuildPolymerChain.hh>

#include <simulations/forcefields/mm/MMAtomTyping.hh>
#include <simulations/forcefields/mm/MMForceField.hh>
#include <simulations/forcefields/mm/MMBondEnergy.hh>
#include <simulations/forcefields/mm/WaterModelParameters.hh>
#include <simulations/forcefields/ConstByAtomEnergy.hh>

#include <simulations/movers/RotateRigidMolecule.hh>
#include <simulations/movers/TranslateMolecule.hh>
#include <simulations/movers/TranslateAtom.hh>
#include <simulations/movers/RotateAASideChains.hh>
#include <simulations/movers/SwingChainFragment.hh>
#include <simulations/movers/PerturbChainFragment.hh>
#include <simulations/movers/BackrubMover.hh>
#include <simulations/movers/PhiPsiMover.hh>
//#include <simulations/movers/SurpassAlfaMover.hh>


#include <simulations/sampling/MetropolisAcceptanceCriterion.hh>
#include <core/BioShellEnvironment.hh>
#include <simulations/forcefields/mm/MMBondEnergy.hh>
#include <simulations/movers/TailBeadMove.hh>
#include <simulations/movers/LambdaMover.hh>
#include <simulations/forcefields/cartesian/DSSPEnergy.hh>
#include <simulations/systems/SimpleAtomTyping.hh>
#include <simulations/systems/PdbAtomTyping.hh>

#include "../../data_sets.hh"

using utils::UnitTests;
using utils::DefineTest;

using namespace simulations::systems;
using namespace core::data::structural;
using core::data::basic::Vec3I;

utils::SuiteTestName name{"test_movers.cc"};

template<typename C>
bool is_backup_identical_to_system(const C & system, const C & backup) {
  bool test_result = true;
  // ---------- Check whether backup is identical to the system
  for (int i = 0; i < system.n_atoms; ++i) {
    test_result &= UnitTests::assert_true(system[i] == backup[i],
        "the system should be identical to it's backup copy after a move");
  }

  return test_result;
}

DefineTest test_atom_moves( "translate and undo 27 argon atoms", []() {

    double tolerance = 0.00001;
    bool test_result = true;
    using simulations::forcefields::WaterModelParameters;

    // ---------- Make a box [-4.5,4.5] (i.e. length 9 centered at 0) each direction
    Vec3I::set_box_len(9.0);

    // --- generate the system of 3x3x3 argon atoms
    PdbAtom_SP a = std::make_shared<PdbAtom>(1," AR ");
    AtomTypingInterface_SP ar_type = std::make_shared<SingleAtomType>(" AR ", " AR");
    CartesianAtoms system(ar_type,27);
    auto grid = std::make_shared<SimpleCubicGrid>(9.0, 27);
    BuildFluidSystem::generate(system, *a, grid);
    CartesianAtoms backup{system};
    CartesianAtoms start{system};

    simulations::forcefields::ConstByAtomEnergy en(0, 0);
    simulations::movers::TranslateAtom mover{system, backup, en};
    mover.max_move_range(0.1);
    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);

    test_result &= UnitTests::assert_equals(1.5, system[0].x(), tolerance);
    test_result &= UnitTests::assert_equals(1.5, system[0].y(), tolerance);
    test_result &= UnitTests::assert_equals(1.5, system[0].z(), tolerance);
    test_result &= UnitTests::assert_equals(-1.5, system[26].x(), tolerance);
    test_result &= UnitTests::assert_equals(-1.5, system[26].y(), tolerance);
    test_result &= UnitTests::assert_equals(-1.5, system[26].z(), tolerance);

    for (int i = 0; i < 100; ++i) mover.move(mc);

    // ---------- Check whether backup is identical to the system
    test_result &= is_backup_identical_to_system(system, backup);
    // ---------- Check whether every molecule has been moved
    for(int i=0;i<system.n_atoms;++i)
      test_result &= UnitTests::assert_true(! (system[i]==start[i]),
                                            "the system should be identical to it's backup copy after a move");

    return test_result;
});

Structure_SP create_water_structure() {

  using simulations::forcefields::WaterModelParameters;

  Vec3I::set_box_len(9.0);

  WaterModelParameters::load_models();
  WaterModelParameters tip3p = WaterModelParameters::get_model("TIP3P");

  // --- generate the system of 3x3x3 TIP3P water molecules
  Residue_SP hoh = tip3p.create_residue();
  PointGridGenerator_SP grid = std::make_shared<SimpleCubicGrid>(9.0, 27);
  Structure_SP s = BuildFluidSystem::build_structure(*hoh, grid);

  simulations::forcefields::mm::MMForceField mmff("AMBER03");
  CartesianChains system(mmff.mm_atom_typing(), *s);

  return s;
}

void print_system_as_pdb(const CartesianChains &system, const Structure &strctr, int n_model) {

  std::cout << "MODEL      " << n_model << "\n";
  int iatom = 0;
  for (auto a_it = strctr.first_const_atom(); a_it != strctr.last_const_atom(); ++a_it) {
    (**a_it).x = system[iatom].x();
    (**a_it).y = system[iatom].y();
    (**a_it).z = system[iatom].z();
    std::cout << (**a_it).to_pdb_line() << "\n";
    ++iatom;
  }
  std::cout << "ENDMDL\n";
}

CartesianChains structure_to_system(const core::data::structural::Structure_SP strctr,
    simulations::forcefields::mm::MMForceField &mmff) {

  CartesianChains system(mmff.mm_atom_typing(), *strctr);
  // --- override atom types of N-terminal AA - just make them non-N-Temirnal
  core::index4 i=0;
  for(const auto & chain: *strctr) {
    for(auto a_it=chain->first_const_atom();a_it!=chain->last_const_atom();++a_it) {
      const std::string & res_name =  (*a_it)->owner()->residue_type().code3;
      system[i].residue_type = mmff.mm_atom_typing()->residue_type(res_name, AtomTypingVariants::STANDARD);
      system[i].atom_type = mmff.mm_atom_typing()->atom_type((**a_it).atom_name(), res_name,
          AtomTypingVariants::STANDARD);
      ++i;
    }
  }

  return system;
}

DefineTest test_tip3p_rotation( "rotate 27 water molecules", []() {

    bool test_result = true;

    Structure_SP s =  create_water_structure();

    simulations::forcefields::mm::MMForceField mmff("AMBER03");
    CartesianChains system(mmff.mm_atom_typing(), *s);
    CartesianChains backup{system};
    CartesianChains start{system};

    simulations::forcefields::ConstByAtomEnergy en(0,0);
    simulations::movers::RotateRigidMolecule mover(system, backup, en, 0);
    mover.max_move_range(3.14159/10);
    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);

    core::data::basic::Vec3 v;
    double r = 0;
    for (int i = 0; i < 1000; ++i) {
      mover.move(mc);
      v += mover.recent_move_axis();
      r += mover.recent_move_radians();
    }
    // ---------- Check whether backup is identical to the system
    test_result &= is_backup_identical_to_system(system, backup);
    r /= 1000.0;
    v /= 1000;
    test_result &= UnitTests::assert_true(r<0.01,"average rotation angle should be close to 0");
    test_result &= UnitTests::assert_true(v.length()<0.1,"average rotation axis should be close to 0");
    // ---------- Check whether every molecule has been moved: coordinates of hydrogens should differ, oxygens should remain unchanged (they do not rotate)
    for(int i=0;i<system.n_atoms;++i) {
      if (i%3!=0)
        test_result &= UnitTests::assert_true(! (system[i]==start[i]),
        utils::string_format("the system should NOT be identical to the stating conformation after a move; problem at pos %d ", i));
      else
        test_result &= UnitTests::assert_true((system[i]==start[i]),
                                              utils::string_format("oxygen atom should not move; problem at pos %d ", i));
    }

    return test_result;
});


DefineTest test_tip3p_translation( "translate 27 water molecules", []() {

    bool test_result = true;
    double tolerance = 0.00001;

    Structure_SP s =  create_water_structure();
    simulations::forcefields::mm::MMForceField mmff("AMBER03");
    CartesianChains system(mmff.mm_atom_typing(), *s);
    CartesianChains backup{system};
    CartesianChains start{system};

    simulations::forcefields::ConstByAtomEnergy en(0,0);
    simulations::movers::TranslateMolecule mover(system, backup, en);
    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);

    test_result &= UnitTests::assert_equals(1.10941, system[0].x(), tolerance);
    test_result &= UnitTests::assert_equals(1.5, system[0].y(), tolerance);
    test_result &= UnitTests::assert_equals(1.5, system[0].z(), tolerance);

    for (int i = 0; i < 1000; ++i) {
      mover.move(mc);
    }
    // ---------- Check whether backup is identical to the system
    test_result &= is_backup_identical_to_system(system, backup);
    // ---------- Check whether every molecule has been moved
    for(int i=0;i<system.n_atoms;++i)
      test_result &= UnitTests::assert_true(! (system[i]==start[i]),
                                            "the system should be identical to it's backup copy after a move");

    return test_result;
});


DefineTest test_sidechain_rotation( "rotate amino acid sidechains", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    Vec3I::set_box_len(256.0);
    // --- a fragment of 2GB1 fill be sampled
    core::data::io::Pdb reader(partial_2gb1, core::data::io::is_not_alternative, core::data::io::keep_all,true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);

    // --- create FF, atom typing and a modelled system
    simulations::forcefields::mm::MMForceField mmff("AMBER03");
    CartesianChains system(mmff.mm_atom_typing(), *strctr);
    CartesianChains backup(system);

    // --- test if the system has the same number of atoms at the source protein
    size_t n_pdb_atoms = std::distance(strctr->first_const_atom(), strctr->last_const_atom());
    test_result &= UnitTests::assert_equals(n_pdb_atoms, size_t(system.n_atoms));

    // --- MM force field: upload definition of rotable dihedrals
    std::string rotables_fname = core::BioShellEnvironment::from_file_or_db("forcefield/mm/rotable_dihedrals.rtp");
    mmff.ammend_nonbonded_parameters(rotables_fname);

    simulations::forcefields::ConstByAtomEnergy en{0.0, 0.0};
    simulations::movers::RotateAASideChains mover{system, *strctr, backup, mmff.resiude_topology(), en};

    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);
    for (int i = 0; i < 1000; ++i) {
      mover.move(mc);
    }
    // ---------- Check whether backup is identical to the system
    test_result &= is_backup_identical_to_system(system, backup);

    return test_result;
});


DefineTest test_phi_psi_rotation( "rotate a fragment of 2gb1 protein chain around Phi,Psi dihedrals", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    Vec3I::set_box_len(64.0);
    // --- a fragment of 2GB1 fill be sampled
    core::data::io::Pdb reader(partial_2gb1, core::data::io::is_not_alternative,core::data::io::keep_all, true); // partial_2gb1
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    core::data::structural::Chain_SP second_chain = strctr->get_chain(0)->clone();
    second_chain->id("B");
    for(auto a_it=second_chain->first_atom();a_it!=second_chain->last_atom();++a_it)
      (**a_it).z += 12.0;
    strctr->push_back(second_chain);

    // --- create FF, atom typing and a modelled system
    simulations::forcefields::mm::MMForceField mmff("AMBER03");
    CartesianChains system(structure_to_system(strctr, mmff));
    CartesianChains backup(system);
    CartesianChains start(system);

    simulations::forcefields::mm::MMBondEnergy bond_energy(system,mmff.bonded_parameters());
    simulations::forcefields::ConstByAtomEnergy en{0.0, 0.0};
    simulations::movers::PhiPsiMover mover{system, *strctr, backup,  en};

    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);
    std::vector<int> phi_cnt(system.count_residues());
    std::vector<int> psi_cnt(system.count_residues());
    for (int i = 0; i < 1000; ++i) {
      mover.move(mc);
//      if(i%100==0) print_system_as_pdb(system, *strctr, i);
      if (mover.recent_move_type()==simulations::movers::PhiPsiMover::DOFs::Phi)
        ++phi_cnt[mover.recently_moved_residue()];
      else
        ++psi_cnt[mover.recently_moved_residue()];
    }

    // --- bond energy change after the moves should be zero as the bonds don't change
    test_result &= UnitTests::assert_equals(0.0,bond_energy.delta_energy(start, system, true),0.005);
    // ---------- Check whether backup is identical to the system
    test_result &= is_backup_identical_to_system(system, backup);

    for(int i=0;i<system.count_residues();++i) {
      test_result &= UnitTests::assert_true(phi_cnt[i] > 10,
          "Each residue must rotate around Phi at least 10 times of 1000 mover calls");
      test_result &= UnitTests::assert_true(psi_cnt[i] > 10,
          "Each residue must rotate around Psi at least 10 times of 1000 mover calls");
//      std::cerr << i<<" "<<phi_cnt[i] << " " << psi_cnt[i] << "\n";
    }
    return test_result;
});

DefineTest test_chain_swing_motion( "test chain swing motion of a short polymer", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    Vec3I::set_box_len(512.0);
    // --- system to be sampled: a chain of 100 beads
    AtomTypingInterface_SP ca_typing = std::make_shared<SingleAtomType>(" CA ");

    int N_atoms = 6;
    CartesianChains system(ca_typing,N_atoms);
    // --- The initial conformation is a zig-zag like on XY plane
    double r = sqrt(2.0);                                     // --- the length of C-C bond
    double a = core::calc::structural::to_radians(30);  // --- 30 [deg] to [rad]
    for (int i = 1; i < N_atoms; ++i)
      system[i].set(i*r * cos(a), r * sin(a) * (i % 2), 0);

    CartesianChains backup(system);

    // --- create a swing mover, energy is constant to have all the move attempts accepted
    simulations::forcefields::ConstByAtomEnergy en(0, 0);
    simulations::movers::SwingChainFragment mover(system, backup, en, 5);
    mover.max_move_range(0.5);

    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);

    // --- try some well-defined moves
    mover.move(mc, 0, 4, core::calc::structural::to_radians(30));
    test_result &= is_backup_identical_to_system(system, backup);
    test_result &= UnitTests::assert_equals(0, int(mover.recently_moved_from()));
    test_result &= UnitTests::assert_equals(4, int(mover.recently_moved_to()));

    mover.move(mc, 1, 5, core::calc::structural::to_radians(30));
    test_result &= is_backup_identical_to_system(system, backup);
    test_result &= UnitTests::assert_equals(1, int(mover.recently_moved_from()));
    test_result &= UnitTests::assert_equals(5, int(mover.recently_moved_to()));
    test_result &= UnitTests::assert_equals(core::calc::structural::to_radians(30), mover.recent_rotation_angle(), 0.0001);

    // --- check if rototranslation is correct (we just test its Z versor)
    const core::calc::structural::transformations::Rototranslation & rt = mover.recent_transformation();
    test_result &= UnitTests::assert_equals(Vec3{-0.019, 0.498, 0.867}, rt.rot_z(), 0.001);

    // --- and now some random sampling
    for (int i = 0; i < 10; ++i) mover.move(mc);

    // ---------- Check whether backup is identical to the system
    test_result &= is_backup_identical_to_system(system, backup);

    return test_result;
});

DefineTest test_chain_swing_motion_100( "test chain swing motion of a simple polymer", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    Vec3I::set_box_len(512.0);
    // --- system to be sampled: a chain of 100 beads
    AtomTypingInterface_SP ca_typing = std::make_shared<SingleAtomType>(" CA ");
    CartesianChains system(ca_typing,100);
    BuildPolymerChain builder(system);
    builder.generate(1.6, 2.2);
    CartesianChains backup(system);

    // --- create a swing mover, energy is constant to have all the move attempts accepted
    simulations::forcefields::ConstByAtomEnergy en(0, 0);
    simulations::movers::SwingChainFragment mover(system, backup, en);

    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);
    for (int i = 0; i < 200; ++i) {
      mover.move(mc);
    }

    // ---------- Check whether backup is identical to the system
    test_result &= is_backup_identical_to_system(system, backup);

    return test_result;
});


DefineTest test_chain_perturbation( "test PerturbChainFragment on a simple polymer", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    Vec3I::set_box_len(512.0);
    // --- system to be sampled: a chain of 100 beads
    AtomTypingInterface_SP ca_typing = std::make_shared<SingleAtomType>(" CA ");
    core::index2 N_atoms = 100;
    CartesianChains system(ca_typing,N_atoms);
    BuildPolymerChain builder(system);
    builder.generate(1.6, 2.2);
    CartesianChains backup(system);

    // --- create a swing mover, energy is constant to have all the move attempts accepted
    simulations::forcefields::ConstByAtomEnergy en(0, 0);
    simulations::movers::PerturbChainFragment mover(system, backup, 7, en);
    mover.max_move_range(0.3);

    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);
    std::vector<int> move_cnts(N_atoms);
    for (int i = 0; i < 100000; ++i) {
      mover.move(mc);
      core::index4 from =mover.recently_moved_from();
      core::index4 to = mover.recently_moved_to();
        test_result &=UnitTests::assert_true(from >= 0 && from < N_atoms, "moved atom index higher than the number of atoms in the system");
        test_result &=UnitTests::assert_true(to >= 0 && to < N_atoms, "moved atom index higher than the number of atoms in the system");
      for (int k = from; k <= to; ++k) {
        ++move_cnts[k];
      }
    }
    for (int i = 0; i < N_atoms; ++i) {
        test_result &= UnitTests::assert_true(move_cnts[i] > 1000,
          utils::string_format("not enough moves at pos %d: measured %d but expected above 1000", i, move_cnts[i]));
    }

    // ---------- Check whether backup is identical to the system
    test_result &= is_backup_identical_to_system(system, backup);

    return test_result;
});

DefineTest test_tail_move( "test TailBeadMove on a simple polymer", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    Vec3I::set_box_len(512.0);
    // --- system to be sampled: a chain of 100 beads
    AtomTypingInterface_SP ca_typing = std::make_shared<SingleAtomType>(" CA ");
    core::index2 N_atoms = 100;
    CartesianChains system(ca_typing,{50,50});
    BuildPolymerChain builder(system);
    builder.generate(1.6, 2.2);
    CartesianChains backup(system);
    core::index4 n_moved=6;

    // --- create a swing mover, energy is constant to have all the move attempts accepted
    simulations::forcefields::ConstByAtomEnergy en(0, 0);
    simulations::movers::TailBeadMove mover(system, backup, en,n_moved);
    mover.max_move_range(0.3);

    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);
    std::vector<int> move_cnts(N_atoms);
    for (int i = 0; i < 100000; ++i) {
        mover.move(mc);
        core::index4 from =mover.tail;
        core::index4 to = mover.other_end;
        test_result &=UnitTests::assert_true(from >= 0 && from < N_atoms, "moved atom index higher than the number of atoms in the system");
        test_result &=UnitTests::assert_true(to >= 0 && to < N_atoms, "moved atom index higher than the number of atoms in the system");
        for (int k = from; k <= to; ++k) {
            ++move_cnts[k];
        }
    }
    for (int i = 0; i < n_moved; ++i) {
        test_result &= UnitTests::assert_true(move_cnts[i] > 1000,
                                              utils::string_format("not enough moves at pos %d: measured %d but expected above 1000", i, move_cnts[i]));
    }
    for (int i = 50-n_moved; i < 50+n_moved; ++i) {
        test_result &= UnitTests::assert_true(move_cnts[i] > 1000,
                                              utils::string_format("not enough moves at pos %d: measured %d but expected above 1000", i, move_cnts[i]));
    }
    for (int i = N_atoms-n_moved; i < N_atoms; ++i) {
        test_result &= UnitTests::assert_true(move_cnts[i] > 1000,
                                              utils::string_format("not enough moves at pos %d: measured %d but expected above 1000", i, move_cnts[i]));
    }
    // ---------- Check whether backup is identical to the system
    test_result &= is_backup_identical_to_system(system, backup);

    return test_result;
});


DefineTest test_backrub_mover( "test backrub mover on a 2GB1 fragment", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    Vec3I::set_box_len(256.0);
    // --- a fragment of 2GB1 will be sampled
    core::data::io::Pdb reader(partial_2gb1, core::data::io::is_not_alternative,core::data::io::keep_all, true,true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    // --- create FF, atom typing and a modelled system
    simulations::forcefields::mm::MMForceField mmff("AMBER03");
    CartesianChains system(structure_to_system(strctr, mmff));
    CartesianChains backup(system);

    // --- test if the system has the same number of atoms at the source protein
    size_t n_pdb_atoms = std::distance(strctr->first_const_atom(), strctr->last_const_atom());
    test_result &= UnitTests::assert_equals(n_pdb_atoms, size_t(system.n_atoms));

    simulations::forcefields::ConstByAtomEnergy en{0.0, 0.0};
    simulations::movers::BackrubMover mover{system, *strctr, backup, en, " CA ", 3, 0.1};

    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);
    std::vector<int> move_cnts(system.n_atoms);
    for (int i = 0; i < 100; ++i) {
      mover.move(mc);
      for (int k = mover.recently_moved_from(); k <= mover.recently_moved_to(); ++k)
        ++move_cnts[k];
//      print_system_as_pdb(system, *strctr, i);
    }
    // ---------- Check whether backup is identical to the system
    test_result &= is_backup_identical_to_system(system, backup);

    core::index2 nres = system.count_residues();
    const auto & range = system.atoms_for_residue(nres-1);
    for (int i = 1; i < range.first_atom - 1; ++i) {
      UnitTests::assert_true(move_cnts[i] > 10,
          utils::string_format("not enough moves at pos %d: measured %d but expected above 10", i, move_cnts[i]));
//      std::cout << i << " " << move_cnts[i] << "\n";
    }

    return test_result;
});

DefineTest test_lambda_rotation( "rotate a fragment of 2gb1 protein chain around Lambda dihedral", []() {

    bool test_result = true;

    using namespace simulations::forcefields::mm;

    Vec3I::set_box_len(64.0);
    // --- a fragment of 2GB1 fill be sampled
    core::data::io::Pdb reader(partial_2gb1, core::data::io::is_bb,core::data::io::keep_all, true); // partial_2gb1
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    core::data::structural::Chain_SP second_chain = strctr->get_chain(0)->clone();
    second_chain->id("B");
    for(auto a_it=second_chain->first_atom();a_it!=second_chain->last_atom();++a_it)
        (**a_it).z += 12.0;
    strctr->push_back(second_chain);

    // --- create FF, atom typing and a modelled system
    simulations::forcefields::mm::MMForceField mmff("AMBER03");

    CartesianChains system(structure_to_system(strctr, mmff));
    CartesianChains backup(system);
    CartesianChains start(system);

    simulations::forcefields::mm::MMBondEnergy bond_energy(system,mmff.bonded_parameters());
    simulations::forcefields::ConstByAtomEnergy en{0,0};
    simulations::movers::LambdaMover mover{system, *strctr, backup,  en};

    simulations::sampling::MetropolisAcceptanceCriterion mc(1.0);
    std::vector<int> lambda_cnt(system.count_residues(),0);
    for (int i = 0; i < 1000; ++i) {
        mover.move(mc);
      //if(i%100==0) print_system_as_pdb(system, *strctr, i);
        ++lambda_cnt[mover.recently_moved_residue()];
    }

    // --- bond energy change after the moves should be zero as the bonds don't change
    test_result &= UnitTests::assert_equals(0.0,bond_energy.delta_energy(start, system, true),0.005);
    // ---------- Check whether backup is identical to the system
    test_result &= is_backup_identical_to_system(system, backup);

    for(int i=0;i<system.count_residues()-1;++i) {
        std::cout<<lambda_cnt[i]<<"\n";
        test_result &= UnitTests::assert_true(lambda_cnt[i] > 10,
                                              "Each residue must rotate around Phi at least 10 times of 1000 mover calls");

    }
    return test_result;
});

RUN_ONE_TEST(10)
