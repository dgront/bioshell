#include <utils/UnitTests.hh>

#include <core/calc/structural/angles.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/data/basic/Vec3.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::data::structural;

std::vector<Vec3> generate_zigzac_coordinates(int N_atoms, double r, double a) {

  std::vector<Vec3> points(N_atoms);
  // --- The initial conformation is a zig-zag like on XY plane
  points[1].set(1, 0, 0);                             // --- the first two points placed on the X axis
  for (int i = 2; i < N_atoms; ++i)
    points[i].set(i*r * cos(a), r * sin(a) * (i % 2), 0);

  return points;
}

std::vector<Vec3> generate_beta_strand(int N_atoms, double r=1.5, double a=core::calc::structural::to_radians(30)) {

  std::vector<Vec3> points(N_atoms);
  for (int i = 1; i < N_atoms; ++i)
    points[i].set(i*r * cos(a), r * sin(a) * (i % 2), 0);

  return points;
}

bool check_if_equals_to_backup(std::vector<Vec3> &points, std::vector<Vec3> &backup) {

  bool test_result = true;
  for (int j = 0; j < points.size(); ++j)
    test_result &= UnitTests::assert_equals(backup[j], points[j], 0.00001);
  return test_result;
}

/**  @brief test around_axis() method.
 *
 * The points subjected to rotations look like:
 *             3     5     7       9      <- y = 1
 *           /  \  /  \  /   \   /  \
 * 0 - 1 - 2     4     6       8     10   <- y = 0
 * and lie on XY plane
 */
DefineTest test_chain_swing_motion( "rotate a chain of points around axis", []() {

    using core::calc::structural::transformations::Rototranslation;

    bool test_result = true;

    int N_atoms = 11;
    double r = sqrt(2.0);                               // --- the length of C-C bond
    double a = core::calc::structural::to_radians(45);  // --- 45 [deg] to [rad]

    std::vector<Vec3> points = generate_zigzac_coordinates(N_atoms, r, a);
    std::vector<Vec3> backup = generate_zigzac_coordinates(N_atoms, r, a);

    Rototranslation rt;

    // ---------- Rotate 8 times by 45 deg around X axis
    Rototranslation::around_axis(points[1], a, points[0], rt);  // --- rotate around X axis, centered at (0,0,0)
    for (int i = 0; i < 8; ++i) {                               // --- 8 rotations 45 deg each makes full circle
      for (Vec3 &p: points) rt.apply(p);
      if (i == 1)  // --- test if two rotations permutate Y coord with Z
        test_result &= UnitTests::assert_equals(backup[3].y, points[3].z, 0.00001);
    }
    // --- After 8 rotation each by 45 deg the point should come back to the initial position
    test_result &= check_if_equals_to_backup(points, backup);

    // ---------- Rotate 12 times by 30 deg around axis defined by points [0] and [2], which is also X axis
    a = core::calc::structural::to_radians(30);                               // --- 30 [deg] to [rad]
    Rototranslation::around_axis(points[0], points[2], a, points[1], rt);     // --- rotate around X axis, centered at (1,0,0)
    for (int i = 0; i < 12; ++i) {
      for(Vec3 & p:points) rt.apply(p);   // --- twelve rotations by 30 deg makes the point come back
    }
    test_result &= check_if_equals_to_backup(points, backup);

    // --- Now rotation around axis defined by points [1] and [3], which is NOT X axis
    Rototranslation::around_axis(points[1], points[3], a, points[1], rt);   // --- rotate around 45 deg axis
    for (int i = 0; i < 12; ++i) {
      for (Vec3 &p: points) rt.apply(p);
      for (int j = 3; j < N_atoms; ++j)    // --- test distance between subsequent atoms
        test_result &= UnitTests::assert_equals(points[j].distance_to(points[j-1]), r, 0.00001);

      if (i < 11)
        for (int j = 4; j < N_atoms; ++j)
          test_result &= UnitTests::assert_true(backup[j].distance_to(points[j]) > 0.01,
              "Point after move should differ from initial conformation");
    }
    test_result &= check_if_equals_to_backup(points, backup);

    return test_result;
});

/**  @brief test around_axis() method.
 *
 * The points subjected to rotations look like:
 *             1     3     5       9      <- y = 1
 *           /  \  /  \  /   \   /  \
 *          0    2     4       6     10   <- y = 0
 * and lie on XY plane
 */
DefineTest test_backrub_motion("backrub-style motion of a chain fragment", []() {

    // ---------- Now a backrub-style rotation of a middle part of a chain

    using core::calc::structural::transformations::Rototranslation;

    double tolerance = 0.00001;
    bool test_result = true;

    int N_atoms = 7;
    double r = sqrt(2.0);                               // --- the length of C-C bond
    double a = core::calc::structural::to_radians(30);  // --- 30 [deg] to [rad]

    std::vector<Vec3> points = generate_beta_strand(N_atoms, r, a);
    std::vector<Vec3> backup = generate_beta_strand(N_atoms, r, a);

    double a_rot = core::calc::structural::to_radians(30);
    int first = 0, last = 4;
    Rototranslation rt;
    Rototranslation::around_axis(points[first], points[last], a_rot, points[first], rt);  // --- rotate around X axis, centered at (0,0,0)
    for (int j = first; j < last; ++j) rt.apply(points[j]);
    for (int j = first + 1; j < last; ++j)    // --- test distance between subsequent atoms
      test_result &= UnitTests::assert_equals(r,points[j].distance_to(points[j - 1]), tolerance);

    first = 1;
    last = 5;
    Rototranslation::around_axis(points[first], points[last], a_rot, points[first], rt);  // --- rotate around X axis, centered at (0,0,0)
    // --- check if rototranslation is correct (we just test its Z versor)
    test_result &= UnitTests::assert_equals(Vec3{-0.019, 0.498, 0.867}, rt.rot_z(), 0.001);
    for (int j = first; j < last; ++j) rt.apply(points[j]);
    for (int j = 1; j < N_atoms; ++j)    // --- test distance between subsequent atoms
      test_result &= UnitTests::assert_equals(r, points[j].distance_to(points[j - 1]), tolerance);

      return test_result;
});
RUN_TESTS

