#include <core/algorithms/trees/BinaryTreeNode.hh>
#include <core/algorithms/trees/algorithms.hh>
#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

// --- tests whether a (sub)tree has correct size
DefineTest t3( "test tree size", []() {

  using namespace core::algorithms::trees;

  std::shared_ptr<BinaryTreeNode<char>> bt = std::make_shared<BinaryTreeNode<char>>(0,'A');
  bt->set_left(std::make_shared<BinaryTreeNode<char>>(1,'B'));
  std::shared_ptr<BinaryTreeNode<char>> r = std::make_shared<BinaryTreeNode<char>>(2,'C');
  bt->set_right(r);
  r->set_left(std::make_shared<BinaryTreeNode<char>>(3,'D'));
  r->set_right(std::make_shared<BinaryTreeNode<char>>(4,'E'));

  // --- do sth complicated here, e.g. create some objects
  bool result = UnitTests::assert_equals(core::index4(5), size(bt));
  result &= UnitTests::assert_equals(core::index4(3), size(r));

  result &= UnitTests::assert_equals(core::index4(3), count_leaves(bt));
  result &= UnitTests::assert_equals(core::index4(2), count_leaves(r));

  return result;
});

RUN_TESTS
