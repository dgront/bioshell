#include <core/algorithms/trees/kd_tree.hh>
#include <core/algorithms/trees/algorithms.hh>
#include <core/index.hh>
#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::algorithms::trees;

class Point: public std::pair<float, float> {
public:
  Point(float x, float y) : std::pair<float, float>(x, y) {}

  float operator[](const size_t k) const { if (k == 0) return first; else return second; }
};

std::ostream &operator<<(std::ostream &out, const Point &p) {

  out << "(" << p.first << "," << p.second << ")";
  return out;
}

// --- tests create_kd_tree() function for a set of 6 points
DefineTest create_simple_graph( "2D test for kd-tree", []() {

    std::vector<Point> input{{2,3}, {5,4}, {9,6}, {4,7}, {8,1}, {7,2}};
    bool test_result = true;

    auto root = core::algorithms::trees::create_kd_tree<Point,std::vector<Point>::iterator,
            core::algorithms::trees::CompareAsReferences<Point>>(input.begin(), input.end(), 2, 0);

    test_result &= UnitTests::assert_equals(core::index4(3), count_leaves(root));
    test_result &= UnitTests::assert_equals(core::index4(6), size(root));
    test_result &= UnitTests::assert_equals(core::index4(3), height(root));
    return test_result;
});

RUN_TESTS
