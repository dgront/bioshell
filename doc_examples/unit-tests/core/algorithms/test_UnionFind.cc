#include <set>
#include <string>

#include <core/algorithms/UnionFind.hh>
#include <core/index.hh>
#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::algorithms;


// --- tests UnionFind
DefineTest create_simple_graph( "test UnionFind", []() {

    std::string input{"asdhshskdksjdksjaasdj"};
    bool test_result = true;

    UnionFindSI4 uf(input.size());
    for(char c:input)
      uf.add_element(std::string{c});
    for (int i = 0; i < input.size(); ++i)
      for (int j = 0; j < i; ++j)
        if (input[i] == input[j]) uf.union_set(i, j);

    std::set<char> unique_chars(input.begin(), input.end());
    auto set_a = uf.retrieve_set(0);
    test_result &= UnitTests::assert_equals(size_t (3), set_a.size());
    test_result &= UnitTests::assert_equals(core::index4(unique_chars.size()), uf.count_sets());

    std::vector<int> expected_counts{3, 6, 4, 2, 3, 3};
    auto out = uf.retrieve_sets();
    int i = -1;
    for(const auto & p:out) {
      test_result &= UnitTests::assert_equals(size_t(expected_counts[++i]), p.second.size());
    }

    return test_result;
});

RUN_TESTS
