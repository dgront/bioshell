#include <core/algorithms/IterateIJ.hh>

#include <core/index.hh>
#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::algorithms;

// --- tests find_cycles() function
DefineTest iterate_IJ( "test IterateIJ iterators", []() {

    bool test_result = true;

    // ---------- test for a regular 2D n x n iteration
    IterateIJ it = IterateIJ(4,false);
    std::vector<IterateIJ::value_type> output_1(it.begin(),it.end());
//    for(const auto &r:output_1) std::cerr << r.first<<","<<r.second <<" ";

    test_result &= UnitTests::assert_equals(size_t(16), output_1.size(), "2D iteration failed");
    test_result &= UnitTests::assert_equals(core::index4(0), output_1[0].first, "2D iteration failed");
    test_result &= UnitTests::assert_equals(core::index4(0), output_1[0].second, "2D iteration failed");
    test_result &= UnitTests::assert_equals(core::index4(3), output_1[15].first, "2D iteration failed");
    test_result &= UnitTests::assert_equals(core::index4(3), output_1[15].second, "2D iteration failed");

    // ---------- a regular 2D iteration - selected rows only
    it.add_selected_row(0).add_selected_row(1);
    std::vector<IterateIJ::value_type> output_1s(it.begin(),it.end());
    test_result &= UnitTests::assert_equals(size_t(8), output_1s.size());

    // ---------- test for a triangular 2D iteration
    IterateIJ it_2 = IterateIJ(4,true);

    std::vector<IterateIJ::value_type> output_2(it_2.begin(),it_2.end());
//    for(const auto &r:output_2) std::cerr << r.first<<","<<r.second <<" ";
    test_result &= UnitTests::assert_equals(size_t(6), output_2.size(), "2D triangular iteration failed");
    test_result &= UnitTests::assert_equals(core::index4(0), output_2[0].first, "2D triangular iteration failed");
    test_result &= UnitTests::assert_equals(core::index4(1), output_2[0].second, "2D triangular iteration failed");
    test_result &= UnitTests::assert_equals(core::index4(2), output_2[5].first, "2D triangular iteration failed");
    test_result &= UnitTests::assert_equals(core::index4(3), output_2[5].second, "2D triangular iteration failed");

    // ---------- a 2D triangular iteration - selected rows only
    it_2.add_selected_row(0).add_selected_row(1);
    std::vector<IterateIJ::value_type> output_2s(it_2.begin(),it_2.end());
    test_result &= UnitTests::assert_equals(size_t(5), output_2s.size(), "2D triangular iteration over selected pairs failed");

    // ---------- a 2D iteration - selected pairs only
    IterateIJ it_3 = IterateIJ(4,false);
    it_3.add_selected_pair(0,2).add_selected_pair(3,2);
    std::vector<IterateIJ::value_type> output_2p(it_3.begin(),it_3.end());
//    for(const auto &r:output_2p) std::cerr << r.first<<","<<r.second <<" ";
    test_result &= UnitTests::assert_equals(size_t(2), output_2p.size(), "2D iteration over selected pairs failed");

    return test_result;
});


RUN_TESTS
