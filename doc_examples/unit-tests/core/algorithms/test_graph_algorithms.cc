#include <core/algorithms/GraphWithData.hh>
#include <core/algorithms/SimpleGraph.hh>
#include <core/algorithms/graph_algorithms.hh>

#include <core/index.hh>
#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::algorithms;

// --- tests find_cycles() function
DefineTest try_find_cycles( "find cycles ", []() {

    bool test_result = true;

    GraphWithData<SimpleGraph, char, int> g;
    g.add_edge('A', 'B', 1);
    g.add_edge('B', 'C', 1);
    g.add_edge('C', 'D', 1);
    g.add_edge('D', 'A', 1);

    std::vector<core::index4> expected_cycle{0, 1, 2, 3};
    std::vector<std::vector<core::index4>> cycles = find_cycles<SimpleGraph, char, int>(g);
    test_result &= UnitTests::assert_equals(size_t(1), cycles.size());
    test_result &= UnitTests::assert_equals(expected_cycle, cycles[0]);

    g.clear_flags(0);
    g.add_edge('D','E',1);
    g.add_edge('E','F',1);
    g.add_edge('F','C',1);
    cycles = find_cycles<SimpleGraph,char,int>(g);
    test_result &= UnitTests::assert_equals(size_t(3), cycles.size());

    return test_result;
});

// --- tests find_clique() function
DefineTest try_find_clique( "find clique ", []() {

    bool test_result = true;

    GraphWithData<SimpleGraph, char, int> g;
    g.add_edge('A', 'B', 1);
    g.add_edge('B', 'C', 1);
    g.add_edge('C', 'A', 1);
    g.add_edge('C', 'D', 1);
    g.add_edge('D', 'E', 1);
    g.add_edge('E', 'C', 1);
    g.add_edge('F', 'A', 1);
    g.add_edge('F', 'B', 1);
    g.add_edge('F', 'C', 1);

    std::vector<core::index4> expected1{0, 1, 2, 5};
    std::vector<core::index4> expected2{2, 3, 4};
    std::vector<core::index4> clique1 = find_clique<SimpleGraph,char,int>(3,g,0);
    test_result &= UnitTests::assert_equals(expected1, clique1);
    std::vector<core::index4> clique2 = find_clique<SimpleGraph,char,int>(3,g,3);
    test_result &= UnitTests::assert_equals(expected2, clique2);

    return test_result;
});

RUN_TESTS
