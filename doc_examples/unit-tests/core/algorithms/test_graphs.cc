#include <core/algorithms/GraphWithData.hh>
#include <core/algorithms/SimpleGraph.hh>
#include <core/algorithms/graph_algorithms.hh>

#include <core/index.hh>
#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::algorithms;

// --- tests find_cycles() function
DefineTest create_simple_graph( "create simple graph", []() {

    bool test_result = true;

    SimpleGraph g;
    g.add_edge(0,1);
    g.add_edge(1,2);
    g.add_edge(2,3);
    g.add_edge(3,0);

    test_result &= UnitTests::assert_equals(core::index4(4), g.count_vertices());
    test_result &= UnitTests::assert_equals(core::index4(4), g.count_edges());
    test_result &= UnitTests::assert_equals(core::index4(2), g.count_edges(0));
    test_result &= UnitTests::assert_true(g.are_connected(0, 3), "vertices 0 and 3 should be connected");

    g.remove_edge(0,3);
    test_result &= UnitTests::assert_true(!g.are_connected(0, 3), "vertices 0 and 3 should be connected");
    test_result &= UnitTests::assert_equals(core::index4(3), g.count_edges());
    test_result &= UnitTests::assert_equals(core::index4(1), g.count_edges(0));

    return test_result;
});

// --- tests find_cycles() function
DefineTest create_graph_with_data( "create graph with data", []() {

    bool test_result = true;

    GraphWithData<SimpleGraph, std::string, int> g;
    g.add_vertex("A");
    g.add_vertex("B");
    test_result &= UnitTests::assert_equals(core::index4(2),g.count_vertices());
    g.add_edge("A", "B", 1);
    g.add_edge("B", "C", 1);
    g.add_edge("C", "A", 1);
    g.add_edge("C", "D", 1);
    g.add_edge("D", "E", 1);
    g.add_edge("E", "C", 1);
    g.add_edge("F", "A", 1);
    g.add_edge("F", "B", 1);
    g.add_edge("F", "C", 1);

    test_result &= UnitTests::assert_equals(core::index4(6),g.count_vertices());
    test_result &= UnitTests::assert_equals(core::index4(9), g.count_edges());
    test_result &= UnitTests::assert_equals(core::index4(3), g.SimpleGraph::count_edges(core::index4(5)));

    return test_result;

});

RUN_TESTS
