#include <core/alignment/SequenceSWAligner.hh>
#include <core/alignment/SequenceNWAligner.hh>
#include <core/alignment/SequenceSemiglobalAligner.hh>
#include <core/data/io/fasta_io.hh>

#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::alignment;

void print_arrows(const std::shared_ptr<core::data::basic::Array2D<unsigned char>> arrows,
                  size_t n_rows = 0, size_t n_cols = 0) {

  n_rows = (n_rows == 0) ? arrows->count_rows() : n_rows;
  n_cols = (n_cols == 0) ? arrows->count_columns() : n_cols;
  std::cerr << "\n";
  for (int i = 0; i <= n_rows; ++i) {
    std::cerr << utils::string_format("%3d : ", i);
    for (int j = 0; j <= n_cols; ++j) {
      std::cerr << utils::string_format("%2d ", arrows->get(i, j));
    }
    std::cerr << "\n";
  }
  std::cerr << "     0  .  .  .  .  |  .  .  .  . 10  .  .  .  .  |  .  .  .  . 20  .  .  .  .  |  .  .  .  . 30\n";
}

std::string out_local = R"(#      7.00    1   0.50
>query
LK
>tmplt
MK
)";

std::string out_global = R"(#     -1.00    2   0.22
>query
MAVRLLKTHL
>tmplt
M--KNITCYL
)";
// --- tests align() method in NW alignment
DefineTest sequence_global_alignment( "global alignment of two short sequences ", []() {

    bool test_result = true;

    SequenceNWAligner ga(10);
    short score = ga.align_for_score("MAVRLLKTHL","MKNITCYL",-10, -1, "BLOSUM62");
    UnitTests::assert_equals(short(-1), score);

    ga.align("MAVRLLKTHL","MKNITCYL",-10, -1, "BLOSUM62");
    test_result &= UnitTests::assert_equals(short(-1), ga.recent_score());
    auto seq_ali = ga.backtrace_sequence_alignment();
    std::string out = core::data::io::create_fasta_string(*seq_ali);
    test_result &= UnitTests::assert_equals(out_global, out);
    return test_result;
});

// --- tests align() method in SW alignment
DefineTest sequence_local_alignment( "local alignment of two short sequences ", []() {

    bool test_result = true;

    SequenceSWAligner la(10);
    short score = la.align_for_score("MAVRLLKTHL","MKNITCYL",-10, -1, "BLOSUM62");
    test_result &= UnitTests::assert_equals(short(7), score);

    la.align("MAVRLLKTHL","MKNITCYL",-10, -1, "BLOSUM62");
    test_result &= UnitTests::assert_equals(short(7), la.recent_score());
    auto seq_ali = la.backtrace_sequence_alignment();
    std::string out = core::data::io::create_fasta_string(*seq_ali);
    test_result &= UnitTests::assert_equals(out_local, out);

    return test_result;
});

std::string q("TMSYLFQHANLDSCKRVLNVVCKTCGQQQTTLKGVEAVMYMGTLSYEQFKKGVQIPCKQATKYLVQQESPFVMMSAPPAQYELKHGTFTCASEYTGNYQCGHYKHITSKETLYCIDGALLTKSSEYKGPITDVFYKENSYTTTIK");
std::string t("TMSYLFQHANLDSCKRVQTTLKGVEAVMYMGTLSYEQFKKLVQQESPFVMMSAPPAQYELKHGTFTCASEYTGNYQCGHYKHITSKETLYCIDGALLTKSSEYKGPITDVFYKENS");

std::string expected2a = R"(#    564.00  116   0.89
>query
TMSYLFQHANLDSCKRVLNVVCKTCGQQQTTLKGVEAVMYMGTLSYEQFKKGVQIPCKQATKYLVQQESPFVMMSAPPAQYELKHGTFTCASEYTGNYQCGHYKHITSKETLYCIDGALLTKSSEYKGPITDVFYKENSYTTTIK
>tmplt
TMSYLFQHANLDSCKRV-----------QTTLKGVEAVMYMGTLSYEQFKK------------LVQQESPFVMMSAPPAQYELKHGTFTCASEYTGNYQCGHYKHITSKETLYCIDGALLTKSSEYKGPITDVFYKENS------
)";

std::string expected2b = R"(#    564.00  116   0.89
>query
TMSYLFQHANLDSCKRV-----------QTTLKGVEAVMYMGTLSYEQFKK------------LVQQESPFVMMSAPPAQYELKHGTFTCASEYTGNYQCGHYKHITSKETLYCIDGALLTKSSEYKGPITDVFYKENS------
>tmplt
TMSYLFQHANLDSCKRVLNVVCKTCGQQQTTLKGVEAVMYMGTLSYEQFKKGVQIPCKQATKYLVQQESPFVMMSAPPAQYELKHGTFTCASEYTGNYQCGHYKHITSKETLYCIDGALLTKSSEYKGPITDVFYKENSYTTTIK
)";

//std::string q("RVVVVVCQQT");
//std::string t("RVQT");

// --- tests align() method in NW alignment
DefineTest sequence_global_alignment2( "global alignment of two long sequences ", []() {

    bool test_result = true;

    SequenceNWAligner ga(q.size());
    short score = ga.align_for_score(q, t,-10, -1, "BLOSUM62");
    UnitTests::assert_equals(short(564), score);

    ga.align(q, t,-10, -1, "BLOSUM62");
    test_result &= UnitTests::assert_equals(short(564), ga.recent_score());
    auto seq_ali = ga.backtrace_sequence_alignment();
    std::string out = core::data::io::create_fasta_string(*seq_ali);
    test_result &= UnitTests::assert_equals(expected2a, out);

    ga.align(t, q,-10, -1, "BLOSUM62");
    seq_ali = ga.backtrace_sequence_alignment();
    out = core::data::io::create_fasta_string(*seq_ali);
    test_result &= UnitTests::assert_equals(expected2b, out);

    return test_result;
});

std::string expected_q_ali = "TMSYLFQHANLDSCKRV-----------QTTLKGVEAVMYMGTLSYEQFKK------------LVQQESPFVMMSAPPAQYELKHGTFTCASEYTGNYQCGHYKHITSKETLYCIDGALLTKSSEYKGPITDVFYKENS------";
std::string expected_t_ali = "TMSYLFQHANLDSCKRVLNVVCKTCGQQQTTLKGVEAVMYMGTLSYEQFKKGVQIPCKQATKYLVQQESPFVMMSAPPAQYELKHGTFTCASEYTGNYQCGHYKHITSKETLYCIDGALLTKSSEYKGPITDVFYKENSYTTTIK";

//// --- tests align() method in semiglobal NW alignment
DefineTest sequence_semiglobal_alignment( "semiglobal alignment of two long sequences ", []() {

    bool test_result = true;

    SequenceSemiglobalAligner ga(q.size());
    short score = ga.align_for_score(q, t,-10, -1, "BLOSUM62");
    UnitTests::assert_equals(short(579), score);

    ga.align(t, q,-10, -1, "BLOSUM62");
    test_result &= UnitTests::assert_equals(short(579), ga.recent_score());
    auto seq_ali = ga.backtrace_sequence_alignment();

    std::string ali_q = seq_ali->get_aligned_query('-');
    std::string ali_t = seq_ali->get_aligned_template('-');
    test_result &= UnitTests::assert_equals(expected_q_ali, ali_q);
    test_result &= UnitTests::assert_equals(expected_t_ali, ali_t);

    ga.align(q, t,-10, -1, "BLOSUM62");
    seq_ali = ga.backtrace_sequence_alignment();
    ali_q = seq_ali->get_aligned_query('-');
    ali_t = seq_ali->get_aligned_template('-');
    test_result &= UnitTests::assert_equals(expected_t_ali, ali_q);
    test_result &= UnitTests::assert_equals(expected_q_ali, ali_t);

    return test_result;
});

std::string seq_1bc6 = "AYVITEPCIGTKDASCVEVCPVDCIHEGEDQYYIDPDVCIDCGACEAVCPVSAIYHEDFVPEEWKSYIQKNRDFFKK";
std::string seq_5fd1 = "AFVVTDNCIKCKYTDCVEVCPVDCFYEGPNFLVIHPDECIDCALCEPECPAQAIFSEDEVPEDMQEFIQLNAELAEVWPNITEKKDPLPDAEDWDGVKGKLQHLER";

// --- tests align() method in NW alignment
DefineTest ferrodoxin_global_alignment( "global alignment of ferrodoxins ", []() {

    bool test_result = true;

    SequenceNWAligner ga(seq_5fd1.size());
    ga.align(seq_1bc6,seq_5fd1, -10, -1, "BLOSUM62");
    test_result &= UnitTests::assert_equals(short(182), ga.recent_score());
    auto seq_ali = ga.backtrace_sequence_alignment();
    std::string out = core::data::io::create_fasta_string(*seq_ali);
//    std::cerr << out << "\n";
    return test_result;
});
RUN_TESTS
//RUN_ONE_TEST("global alignment of ferrodoxins ")
