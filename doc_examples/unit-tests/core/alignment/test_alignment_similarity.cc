#include <core/alignment/scoring/alignment_similarity.hh>

#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::alignment;

DefineTest aln_k_similarity("test aln-k alignment similarity ", []() {

    bool test_result = true;

    PairwiseSequenceAlignment ali_i{"q", "MAVRLLKTHL", 1, "t", "M--KNITCYL", 1, 0.0};
    PairwiseSequenceAlignment ali_j{"q", "MAVRLLKTHL", 1, "t", "--MKNITCYL", 1, 0.0};

    test_result &= UnitTests::assert_equals(7 / 8.0, scoring::aln_k_similarity(ali_i, ali_j, 0), 0.00001);

    test_result &= UnitTests::assert_equals(7 / 8.0, scoring::aln_0_similarity(ali_i, ali_j), 0.00001);

    test_result &= UnitTests::assert_equals(7 / 8.0, scoring::aln_k_similarity(ali_i, ali_j, 1), 0.00001);

    test_result &= UnitTests::assert_equals(7 / 8.0, scoring::aln_k_similarity(ali_i, ali_j, 2), 0.00001);

    return test_result;
});

RUN_TESTS
