#include <core/alignment/PairwiseAlignment.hh>

#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::alignment;

DefineTest test_pairwise_alignment("test pairwise class alignment consistency", []() {

    bool test_result = true;

    std::string input_q{"MAVRLLKTHL"};
    std::string input_t{"M--KNITCYL"};
    std::string q{"MAVRLLKTHL"};
    std::string t{"MKNITCYL"};
    PairwiseAlignment ali = {input_q, 0, input_t,0};

    test_result &= UnitTests::assert_equals((core::index2) 0, ali.n_query_gaps());
    test_result &= UnitTests::assert_equals((core::index2) 2, ali.n_template_gaps());

    std::string aq = ali.get_aligned_query(q,'-');
    std::string at = ali.get_aligned_template(t,'-');
    test_result &= UnitTests::assert_equals(input_t, at);
    test_result &= UnitTests::assert_equals(input_q, aq);

    std::vector<char> t_chars(t.begin(), t.end());
    std::vector<char> t_gapped_chars;
    std::vector<char> q_chars(q.begin(), q.end());
    std::vector<char> q_gapped_chars;

    ali.get_aligned_query_template(q_chars, t_chars, '-', q_gapped_chars, t_gapped_chars);
    std::string out_t = std::string(t_gapped_chars.begin(), t_gapped_chars.end());
    std::string out_q = std::string(q_gapped_chars.begin(), q_gapped_chars.end());
    test_result &= UnitTests::assert_equals(input_t, out_t);
    test_result &= UnitTests::assert_equals(input_q, out_q);

    ali.get_aligned_query_template(q_chars, t_chars, q_gapped_chars, t_gapped_chars);
    out_t = std::string(t_gapped_chars.begin(), t_gapped_chars.end());
    out_q = std::string(q_gapped_chars.begin(), q_gapped_chars.end());
    test_result &= UnitTests::assert_equals(t, out_t);
    std::string ali_q{"MRLLKTHL"};
    test_result &= UnitTests::assert_equals(ali_q, out_q);

    return test_result;
});

RUN_TESTS
