#include <core/alignment/on_alignment_computations.hh>
#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::alignment;

// --- test hamming_distance function
DefineTest compute_hamming_distance("compute Hamming distance", []() {

    bool test_result = true;

    double d = hamming_distance("ABBA", "CBCB");

    test_result &= UnitTests::assert_equals(3.0/4, d, 0.0001);

    return test_result;
});

RUN_TESTS
