#include <string>
#include <sstream>

#include <core/chemical/PdbMolecule.hh>
#include <core/chemical/molecule_utils.hh>
#include <core/algorithms/graph_algorithms.hh>
#include <core/index.hh>
#include <utils/UnitTests.hh>

#include "../../data_sets.hh" // --- provides input data as std::string objects

using utils::UnitTests;
using utils::DefineTest;

using namespace core::chemical;
using namespace core::data::structural;

DefineTest create_molecule( "create molecule from atoms and bonds ", []() {

    bool test_result = true;

    PdbMolecule m0;
    PdbAtom_SP a = std::make_shared<PdbAtom>();
    m0.add_atom(a);

    // --- Cyclopropyl methyl ketone
    std::vector<std::string> atom_names{"C1", "O", "C2", "C3", "C4"};
    PdbMolecule m(atom_names, {{1, 2, "1"}, // C1 - O
                              {2, 3, "1"}, // O - C2
                              {3, 4, "1"}, // C2 - C3
                              {4, 5, "1"}, // C3 - C4
                              {5, 3, "1"}}); // C4 - C2

    std::vector<std::string> names_again;
    for (auto it = m.cbegin_atom(); it != m.cend_atom(); ++it)
      names_again.push_back((**it).atom_name());
    test_result &= UnitTests::assert_equals(atom_names, names_again);

    test_result &= UnitTests::assert_equals(core::index4(5),m.count_bonds());

    test_result &= UnitTests::assert_true(m.are_connected(2,4),"atoms 2 and 4 should be connected with a bond!");
    core::data::structural::PdbAtom_SP o = m.get_atom(1);
    core::data::structural::PdbAtom_SP c1 = m.get_atom(0);
    test_result &= UnitTests::assert_equals(std::string("O"),o->atom_name());
    test_result &= UnitTests::assert_equals(std::string("C1"),c1->atom_name());
    test_result &= UnitTests::assert_true(m.are_bonded(o,c1),"atoms O and C1 should be connected with a bond!");

    core::data::structural::PdbAtom_SP c2 = m.get_atom(2);
    test_result &= UnitTests::assert_equals(core::index4(3),m.count_bonds(c2));

    return test_result;
});

// --- loads a molecule from PDB data and tests find_cycles() function
DefineTest molecule_from_pdb( "create molecule from a PDB content ", []() {


  bool test_result = true;
  std::istringstream in(monomer_03G_as_pdb);
  PdbMolecule_SP mol = PdbMolecule::from_pdb(in);
  test_result &= UnitTests::assert_equals(core::index4(48), mol->count_bonds());
  test_result &= UnitTests::assert_equals(core::index4(47), mol->count_atoms());

  std::vector<std::vector<core::index4>> cycles = core::algorithms::find_cycles<PdbMolecule, PdbAtom_SP, BondType>(*mol);
  test_result &= UnitTests::assert_equals(size_t(2), cycles.size());
  std::vector<std::string> expected_benzene{" C1 "," C3 "," C3 "," C4 "," C5 "," C6 "};
  std::vector<std::string> found_benzene;
  for(auto v:cycles[0])
    found_benzene.push_back(mol->get_atom(v)->atom_name());
  test_result &= UnitTests::assert_equals(expected_benzene, found_benzene);

  std::vector<std::string> expected_cyclo{" C9 "," C13"," C12"," N3 "," C11"," C10"};
  std::vector<std::string> found_cyclo;
  for(auto v:cycles[1])
    found_cyclo.push_back(mol->get_atom(v)->atom_name());
  test_result &= UnitTests::assert_equals(expected_cyclo, found_benzene);

  return test_result;
});


RUN_TESTS
