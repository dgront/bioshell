#include <core/algorithms/graph_algorithms.hh>

#include <core/index.hh>
#include <core/chemical/PdbMolecule.hh>
#include <core/chemical/molecule_utils.hh>
#include <utils/UnitTests.hh>

#include "../../data_sets.hh" // --- provides input data as std::string objects

using utils::UnitTests;
using utils::DefineTest;

using namespace core::algorithms;
using namespace core::chemical;


DefineTest try_maximal_common_submolecule( "maximal common submolecule - easy case", []() {

    bool test_result = true;

    // --- Cyclopropyl methyl ether
    PdbMolecule m1({"C1", "O", "C2", "C3", "C4"}, {{1, 2, "1"}, // C1 - O
                                                                {2, 3, "1"}, // O - C2
                                                                {3, 4, "1"}, // C2 - C3
                                                                {4, 5, "1"}, // C3 - C4
                                                                {5, 3, "1"}}); // C4 - C2
    m1.get_atom(1)->element_index(8);

    PdbMolecule m2({"C4", "C3", "C2", "O", "C1"}, {{1, 2, "1"}, // C3 - C4
                                                                 {2, 3, "1"}, // C4 - C2
                                                                 {3, 1, "1"}, // C2 - C4
                                                                 {3, 4, "1"}, // C2 - O
                                                                 {5, 4, "1"}}); // C4 - O
    m2.get_atom(3)->element_index(8);


    std::vector<std::pair<core::index4, core::index4>> expected{{0,4},{1, 3}, {2, 2}, {3, 0}, {4, 1}};
    auto match = maximal_common_submolecule(m1, m2);
    test_result &= UnitTests::assert_equals(expected, match);

    return test_result;
});

DefineTest try_maximal_common_submolecule_hard( "maximal common submolecule - hard case", []() {

    bool test_result = true;

    std::istringstream in1(monomer_9YZ_as_pdb);
    PdbMolecule_SP mol1 = PdbMolecule::from_pdb(in1);
    test_result &= UnitTests::assert_equals(core::index4(18), mol1->count_vertices());
    test_result &= UnitTests::assert_equals(core::index4(19), mol1->count_bonds());

    std::istringstream in2(monomer_9ZB_as_pdb);
    PdbMolecule_SP mol2 = PdbMolecule::from_pdb(in2);
    test_result &= UnitTests::assert_equals(core::index4(16), mol2->count_vertices());
    test_result &= UnitTests::assert_equals(core::index4(17), mol2->count_bonds());

    std::vector<std::pair<core::index4, core::index4>> match = maximal_common_submolecule(*mol1, *mol2);
//    for(const std::pair<core::index4, core::index4> & p:match) {
//      std::cerr << mol1->get_atom(p.first)->atom_name() <<" "<< mol2->get_atom(p.second)->atom_name()<<"\n";
//    }
    return test_result;
});

DefineTest test_find_cycles( "find cycles in a PdbMolecule ", []() {

    bool test_result = true;

    // --- Cyclopropyl methyl ether
    PdbMolecule m1({"C1", "O", "C2", "C3", "C4"}, {{1, 2, "1"}, // C1 - O
                                                                 {2, 3, "1"}, // O - C2
                                                                 {3, 4, "1"}, // C2 - C3
                                                                 {4, 5, "1"}, // C3 - C4
                                                                 {5, 3, "1"}}); // C4 - C2

    std::vector<std::vector<PdbAtom_SP>> cycles1 = find_ring_atoms(m1);
    test_result &= UnitTests::assert_equals(size_t(1), cycles1.size());

    std::istringstream in(monomer_03G_as_pdb);
    PdbMolecule_SP mol = PdbMolecule::from_pdb(in);
    std::vector<std::vector<PdbAtom_SP>> cycles2 = find_ring_atoms(*mol);
    test_result &= UnitTests::assert_equals(size_t(2), cycles2.size());


    return test_result;
});

// --- loads a molecule from PDB data and tests find_cycles() function
DefineTest test_split_molecules( "create two molecules from a PDB content and splits them", []() {

    bool test_result = true;
    std::istringstream in(two_ligands_as_pdb);
    PdbMolecule_SP mol = PdbMolecule::from_pdb(in);
    test_result &= UnitTests::assert_equals(core::index4(61), mol->count_atoms());

    std::vector<std::string> expected_mol1{" CAA"," CAB"," CAC"," CAD"," CAE"," CAF"," CAK"," OAG"," OAH"," OAI"," OAJ"," OAL"," OAN"," OAO"," OAP"," PAM","CO  "};
    auto molecules = split_molecules(*mol,2);
    test_result &= UnitTests::assert_equals(size_t(2), molecules.size());
    // --- check the first molecule
    std::vector<std::string> atom_names;
    for(auto atom:(*molecules[0])) atom_names.push_back(atom->atom_name());
    std::sort(atom_names.begin(),atom_names.end());
    test_result &= UnitTests::assert_equals(expected_mol1,atom_names);

    std::istringstream in2(two_ligands_as_pdb);
    PdbMolecule_SP mol_CAK = PdbMolecule::from_pdb(in2, core::data::structural::selectors::SelectResidueByName("CAK"));
    test_result &= UnitTests::assert_equals(core::index4(16), mol_CAK->count_atoms());
    molecules = split_molecules(*mol_CAK,2);
    test_result &= UnitTests::assert_equals(size_t(1), molecules.size());

    return test_result;
});

RUN_TESTS
