#include <string>
#include <sstream>

#include <core/chemical/MonomerStructure.hh>
#include <core/chemical/MonomerStructureFactory.hh>
#include <core/index.hh>
#include <utils/io_utils.hh>
#include <utils/UnitTests.hh>
#include <core/BioShellEnvironment.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::chemical;

DefineTest test_MonomerStructureFactory( "test MonomerStructureFactory ", []() {

    bool test_result = true;
    MonomerStructureFactory & factory = MonomerStructureFactory::get_instance();
    auto m = factory.get("ASN");
    test_result &= UnitTests::assert_true(factory.is_known_monomer("ASN"), "ASN should be loaded but it's not");

    return test_result;
});

DefineTest test_monomer_loading( "test if all monomers get loaded ", []() {

    bool test_result = true;
    MonomerStructureFactory & factory = MonomerStructureFactory::get_instance();
    std::vector<std::string> code3;
    for(auto it=factory.cbegin(); it!=factory.cend();++it)
      code3.push_back(it->first);

    std::string monomers_path = core::BioShellEnvironment::bioshell_db_path() + "/monomers/";
    std::vector<std::string> expected = utils::read_listfile(monomers_path + "monomers_list");
    test_result &= UnitTests::assert_equals(expected.size(), code3.size());
    std::sort(code3.begin(),code3.end());
    std::sort(expected.begin(),expected.end());

    for (int i = 0; i < expected.size(); ++i)
      test_result &= UnitTests::assert_equals(expected[i], code3[i]);

    return test_result;
});

RUN_TESTS
