#include <core/chemical/guess_pdb_element.hh>

#include <core/index.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/io/Pdb.hh>
#include <utils/UnitTests.hh>

#include "../../data_sets.hh" // --- provides input data as std::string objects

using utils::UnitTests;
using utils::DefineTest;

using namespace core::chemical;

DefineTest test_guess_pdb_element( "guess_pdb_element", []() {

    bool test_result = true;

    std::istringstream in1(monomer_03G_as_pdb);
    core::data::structural::Structure_SP s = core::data::io::Pdb(in1).create_structure(0);

    for(auto it = s->first_const_atom(); it!=s-> last_const_atom(); ++it){
      AtomicElement e = guess_pdb_element((**it).atom_name());
      test_result &= UnitTests::assert_equals((**it).element_index(), (unsigned short) e.z);
    }

    return test_result;
});


RUN_TESTS
