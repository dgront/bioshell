#include <memory>
#include <vector>

#include <core/protocols/SequenceWeightingProtocol.hh>
#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::protocols;

std::vector<std::string> input_seq{"ABBA", "ABCA", "CBCB"};
std::vector<std::string> input_seq2{"GYVGS","GFDGF","GYDGF","GYQGG"};

std::vector<double> inv_identity_results{0.329843, 0.2932, 0.3770};

// --- test sequence weights: inverse identity method
DefineTest test_inv_identity_sequence_weights("sequence weights: inverse identity method", []() {

    bool test_result = true;

    InverseIdentitySequenceWeights wghts;
    for (const std::string &s:input_seq) wghts.add_input_sequence(s);
    wghts.run();
    for (int i = 0; i < input_seq.size(); ++i)
      test_result &= UnitTests::assert_equals(inv_identity_results[i], wghts.get_weight(i), 0.001);

    return test_result;
});

std::vector<double> va_identity_results{0.3333, 0.2500, 0.4167};
// --- test sequence weights: Vingron & Argos method
DefineTest test_Vingron_Argos_sequence_weights("sequence weights: Vingron & Argos method", []() {

    bool test_result = true;

    VingronArgosSequenceWeights wghts;
    for (const std::string &s:input_seq) wghts.add_input_sequence(s);
    wghts.run();
    for (int i = 0; i < input_seq.size(); ++i)
      test_result &= UnitTests::assert_equals(va_identity_results[i], wghts.get_weight(i), 0.001);

    return test_result;
});

std::vector<double> henikof_identity_results{0.2667, 0.2667, 0.20, 0.2667};
// --- test sequence weights: Henikof method
DefineTest test_Henikof_sequence_weights("sequence weights: Henikof method", []() {

    bool test_result = true;

    HenikoffSequenceWeights wghts;
    for (const std::string &s:input_seq2) wghts.add_input_sequence(s);
    wghts.run();
    for (int i = 0; i < input_seq2.size(); ++i)
      test_result &= UnitTests::assert_equals(henikof_identity_results[i], wghts.get_weight(i), 0.001);

    return test_result;
});

RUN_TESTS
