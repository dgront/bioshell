#include <memory>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/ResidueSegmentSelector.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/ResidueSegment.hh>

#include <utils/UnitTests.hh>

#include "../../data_sets.hh" // --- provides input data as std::string objects

using utils::UnitTests;
using utils::DefineTest;

using namespace core::data::structural::selectors;

// --- select chain fragments
DefineTest select_contiguous_helix("test ResidueSelector classes", []() {

    bool test_result = true;
    core::data::io::Pdb reader(pdb_6ats, core::data::io::keep_all, core::data::io::only_ss_from_header, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    core::data::structural::Chain_SP c = (*strctr)[0];

    IsSSType selector(HasProperlyConnectedCA(), 'H');
    // ---------- test if we have a helix at residues 14-22
    core::data::structural::ResidueSegment rs(c,15,23);   // --- residue numbers start from -1 in that deposit
    test_result &= UnitTests::assert_true(selector(rs),"There should be a helical fragment in 6ats protein at pos 14-22");

    int n_hits = 0;
    HasSequence select_XGX(selector, "XEX");
    for (core::index2 i = 0; i < c->size() - 2; ++i) {
      core::data::structural::ResidueSegment rsi(c, i, i + 2);
      if(select_XGX(rsi)) ++n_hits;
//      std::cerr << rsi.sequence()->sequence << " " << rsi.sequence()->str() << " " << select_XGX(rsi) << "\n";
    }
    test_result &= UnitTests::assert_equals(3,n_hits,"There should be 3 XEX helical fragments");

    return test_result;
});

RUN_TESTS
