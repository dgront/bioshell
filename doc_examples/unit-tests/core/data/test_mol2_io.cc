#include <memory>

#include <core/data/io/mol2_io.hh>
#include <utils/UnitTests.hh>

#include "../../data_sets.hh"

using utils::UnitTests;
using utils::DefineTest;

using namespace core::data::io;

std::string expected = R"(@<TRIPOS>MOLECULE
benzene
12 12
@<TRIPOS>ATOM
         1       C1      1.207     2.091     0.000     C    1      benzene 0.000
         2       C2      2.414     1.394     0.000     C    1      benzene 0.000
         3       C3      2.414     0.000     0.000     C    1      benzene 0.000
         4       C4      1.207    -0.697     0.000     C    1      benzene 0.000
         5       C5      0.000     0.000     0.000     C    1      benzene 0.000
         6       C6      0.000     1.394     0.000     C    1      benzene 0.000
         7       H1      1.207     3.175     0.000     C    1      benzene 0.000
         8       H2      3.353     1.936     0.000     C    1      benzene 0.000
         9       H3      3.353    -0.542     0.000     C    1      benzene 0.000
        10       H4      1.207    -1.781     0.000     C    1      benzene 0.000
        11       H5     -0.939    -0.542     0.000     C    1      benzene 0.000
        12       H6     -0.939     1.936     0.000     C    1      benzene 0.000
@<TRIPOS>BOND
         1         1         2        ar
         2         1         6        ar
         3         1         7         1
         4         2         3        ar
         5         2         8         1
         6         3         4        ar
         7         3         9         1
         8         4         5        ar
         9         4        10         1
        10         5         6        ar
        11         5        11         1
        12         6        12         1
)";
// --- read benzene molecule from mol2 format
DefineTest read_benzene_mol2("read benzene molecule from mol2 format", []() {

    bool test_result = true;

    std::istringstream inp(mol2_benzene);
    core::chemical::PdbMolecule_SP  mol = read_mol2(inp);
    // ---------- test if we have 12 atoms
    test_result &= UnitTests::assert_equals((core::index4)12, mol->count_atoms());
    // ---------- test if we have 12 bonds
    test_result &= UnitTests::assert_equals((core::index4)12, mol->count_bonds());

    std::ostringstream out;
    write_mol2(mol, out);
    // ---------- test if the output likes like we extect
    test_result &= UnitTests::assert_equals(out.str(), expected);
    return test_result;
});
RUN_TESTS
