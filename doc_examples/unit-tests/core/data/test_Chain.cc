#include <string>
#include <sstream>

#include <core/data/structural/Chain.hh>
#include <core/data/structural/SecondaryStructureElement.hh>
#include <core/data/io/Pdb.hh>
#include <core/BioShellEnvironment.hh>
#include <utils/UnitTests.hh>

#include "../../data_sets.hh" // --- provides input data as std::string objects

using utils::UnitTests;
using utils::DefineTest;

DefineTest create_seq_str_from_chain( "create SecondaryStructure object from Chain object ", []() {

    bool test_result = true;
    std::stringstream in(pdb_1cey);
    core::data::io::Pdb pdb(in);
    core::index2 i=0;
    core::data::structural::Structure_SP s = pdb.create_structure(0);
    core::data::sequence::SecondaryStructure_SP seq_str = s->get_chain(i)->create_sequence();
    test_result &= UnitTests::assert_equals(std::string{"A"}, seq_str->chain_id());
    test_result &= UnitTests::assert_equals(core::index2(2), seq_str->indexes(0));
    test_result &= UnitTests::assert_equals(core::index2(129), seq_str->indexes(seq_str->length()-1));
    test_result &= UnitTests::assert_equals(size_t(128), seq_str->length());

    // --- test has_secondary_structure() method
    std::stringstream in2(pdb_6ats);
    core::data::io::Pdb pdb2(in2, core::data::io::keep_all, core::data::io::keep_all, true);
    s = pdb2.create_structure(0);

    core::data::structural::Chain_SP ci = s->get_chain(0);
    test_result &= UnitTests::assert_true(ci->has_secondary_structure(),"chain must contain secondary structure data");

    auto sse = ci->create_sse();
    test_result &= UnitTests::assert_equals(size_t(2), sse.size());
    test_result &= UnitTests::assert_equals(size_t(13), sse[0]->size());

    for(core::data::structural::Residue_SP ri:*ci) ri->ss('C');
    test_result &= UnitTests::assert_true(!ci->has_secondary_structure(),"chain must NOT contain secondary structure data");

    return test_result;
});


RUN_TESTS
