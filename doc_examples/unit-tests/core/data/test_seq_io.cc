#include <memory>

#include <core/data/io/seq_io.hh>
#include <core/data/io/Pdb.hh>
#include <utils/UnitTests.hh>

#include "../../data_sets.hh"

using utils::UnitTests;
using utils::DefineTest;

using namespace core::data::io;

std::string expected=R"(    5   LEU A  1
    6   ILE A  1
    7   LEU A  1
    8   ASN A  1
    9   GLY A  1
   10   LYS A  1
   11   THR A  1
   12   LEU A  1
   13   LYS A  1
   14   GLY A  1
)";


DefineTest write_seq_format("write SecondaryStructure object to SEQ format file", []() {

    bool test_result = true;
    std::stringstream in(partial_2gb1);
    core::data::io::Pdb pdb(in);
    core::index2 i=0;
    core::data::structural::Structure_SP s = pdb.create_structure(0);
    core::data::structural::Chain_SP ch = s->get_chain(i);
    core::data::sequence::SecondaryStructure_SP seq_str = ch->create_sequence();
    std::ostringstream out;
    write_seq(*seq_str, out);
   //  ---------- test if the output likes like we extect
    test_result &= UnitTests::assert_equals(out.str(), expected);
    return test_result;
});

DefineTest read_seq_format("read SEQ format file and create SecondaryStructure ", []() {

    bool test_result = true;
    std::istringstream in(seq_format);
   // std::string expec=R"(11112222222222211113344122222223112223122221331444444411331111113311444441111111111144444331144441133111441114444433311122222222222331133111122222222333111144444432222223333322222222222222311444441)";
    core::data::sequence::SecondaryStructure_SP seq = read_seq(in);
    //  ---------- test if the output likes like we extect
    test_result &= UnitTests::assert_equals(core::index2(3),seq->indexes(0));
    test_result &= UnitTests::assert_equals(core::index2(110),seq->indexes(seq->length()-1));
    test_result &= UnitTests::assert_equals(std::string{"A"},seq->chain_id());
    test_result &= UnitTests::assert_equals(size_t(108),seq->length());
    test_result &= UnitTests::assert_equals('H',seq->ss(4));
    test_result &= UnitTests::assert_equals('E',seq->ss(21));
    test_result &= UnitTests::assert_equals('C',seq->ss(107));

    return test_result;
});

RUN_TESTS
