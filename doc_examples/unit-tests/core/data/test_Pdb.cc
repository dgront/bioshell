#include <string>
#include <sstream>

#include <core/data/io/Pdb.hh>
#include <utils/UnitTests.hh>
#include <core/chemical/PdbMolecule.hh>
#include <core/chemical/molecule_utils.hh>

#include "../../data_sets.hh" // --- provides input data as std::string objects

using utils::UnitTests;
using utils::DefineTest;

DefineTest parse_pdb_raw_data( "create Pdb from raw string ", []() {

    bool test_result = true;
    core::data::io::Pdb pdb(two_ligands_as_pdb);
    core::data::structural::Structure_SP s = pdb.create_structure(0);
    test_result &= UnitTests::assert_equals(core::index2(3), s->count_residues());
    test_result &= UnitTests::assert_equals(core::index2(1), s->count_chains());
    return test_result;
});

DefineTest read_pdb_stream( "create Pdb from a string stream", []() {

    bool test_result = true;
    std::stringstream in(two_ligands_as_pdb);
    core::data::io::Pdb pdb(in);
    core::data::structural::Structure_SP s = pdb.create_structure(0);
    test_result &= UnitTests::assert_equals(core::index2(3), s->count_residues());
    test_result &= UnitTests::assert_equals(core::index2(1), s->count_chains());
    return test_result;
});

DefineTest test_connect_reader( "read ligands with complicated CONNECT entries", []() {

    using namespace core::chemical;

    bool test_result = true;
    std::stringstream in(connects_5lbo);
    core::data::io::Pdb pdb(in);
    core::data::structural::Structure_SP s = pdb.create_structure(0);
    test_result &= UnitTests::assert_equals(core::index2(39), s->count_residues());

    std::stringstream in2(connects_5lbo);
    PdbMolecule_SP mol = PdbMolecule::from_pdb(in2);
    test_result &= UnitTests::assert_equals(core::index4 (352),mol->count_atoms());
    auto molecules = split_molecules(*mol,2);
    test_result &= UnitTests::assert_equals(size_t(39), molecules.size());

    std::vector<std::string> lines;
    utils::split_into_strings(connects_5lbo, lines,'\n');

    lines.erase(std::remove_if(lines.begin(), lines.end(),
                               [](const std::string &line) { return line.substr(0, 6) != "CONECT"; }),
                lines.end());
    auto from_to = pdb.header.equal_range("CONECT");
    core::index4 i = 0;
    for (auto it = from_to.first; it != from_to.second; ++it) {
      test_result &= UnitTests::assert_equals(lines[i], (*it->second).to_pdb_line());
      ++i;
    }
    test_result &= UnitTests::assert_equals(size_t(352), lines.size());

    return test_result;
});

RUN_TESTS
