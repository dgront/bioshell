#include <string>
#include <sstream>

#include <core/data/basic/Vec3I.hh>

#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using core::data::basic::Vec3I;

DefineTest basic_math( "basic math operations on Vec3I type", []() {

    bool test_result = true;
    double tolerance = 0.00001;  // --- tolerance for coordinates must be a value greater than int_to_real_factor() !

    test_result &= UnitTests::assert_equals(8192, int(Vec3I::get_box_len()));
    test_result &= UnitTests::assert_equals(Vec3I::get_box_len() / std::numeric_limits<int>::max(),
                                            Vec3I::int_to_real_factor()*2, 0.0000001);

    Vec3I::set_box_len(128);

    Vec3I v1{1.5, 2.5, 3.5};
    v1 += 0.5;
    test_result &= UnitTests::assert_equals(2.0, v1.x(), tolerance);
    test_result &= UnitTests::assert_equals(3.0, v1.y(), tolerance);
    test_result &= UnitTests::assert_equals(4.0, v1.z(), tolerance);

    v1 -= 1.9999999;
    test_result &= UnitTests::assert_equals(0.0, v1.x(), tolerance);
    test_result &= UnitTests::assert_equals(1.0, v1.y(), tolerance);
    test_result &= UnitTests::assert_equals(2.0, v1.z(), tolerance);

    v1 *=1.5;
    test_result &= UnitTests::assert_equals(0.0, v1.x(), tolerance);
    test_result &= UnitTests::assert_equals(1.5, v1.y(), tolerance);
    test_result &= UnitTests::assert_equals(3.0, v1.z(), tolerance);

    Vec3I v2{1.5, 2.5, 3.5};
    v1 += v2;
    test_result &= UnitTests::assert_equals(1.5, v1.x(), tolerance);
    test_result &= UnitTests::assert_equals(4.0, v1.y(), tolerance);
    test_result &= UnitTests::assert_equals(6.5, v1.z(), tolerance);

    v1 -= v2 * 0.5;

    test_result &= UnitTests::assert_equals(1.5 - 1.5 * 0.5, v1.x(), tolerance);
    test_result &= UnitTests::assert_equals(4.0 - 2.5 * 0.5, v1.y(), tolerance);
    test_result &= UnitTests::assert_equals(6.5 - 3.5 * 0.5, v1.z(), tolerance);

    test_result &= UnitTests::assert_equals(false, v1==v2);


    v1.set(Vec3I::get_box_len() / 2.0 - 2.0 * Vec3I::int_to_real_factor());
    test_result &= UnitTests::assert_equals(true, v1.x() > 0);
    v1.add( 3.0 * Vec3I::int_to_real_factor(), 0, 0);
    test_result &= UnitTests::assert_equals(true, v1.x() < 0);
    v1.add( -3.0 * Vec3I::int_to_real_factor(), 0, 0);
    test_result &= UnitTests::assert_equals(true, v1.x() > 0);


    std::vector<double> q{-1, -62, -63, -64, -65, -66, 64, 64.5, 66.5};
    std::vector<double> a{-1, -62, -63, -64, 63, 62, 64, -63.5, -61.5};
    for (int i = 0; i < a.size(); ++i) {
      Vec3I v3{q[i], 0, 0};
      test_result &= UnitTests::assert_equals(a[i], (v3.x()), tolerance);
    }

    return test_result;
});

DefineTest compute_distances( "compute distances between Vec3I points", []() {

    bool test_result = true;
    double tolerance = 0.0001;  // --- tolerance for coordinates must be a value greater than int_to_real_factor() !

    Vec3I::set_box_len(4096);

    Vec3I v1{4095.00001, 0, 0};
    Vec3I v2{4096.99999, 0, 0};
    test_result &= UnitTests::assert_equals(4096.99999 - 4096.00001 + 1, sqrt(v1.distance_square_to(v2)), tolerance);
    Vec3I v3(v1);
    v3 -=v2;

    v1.set(4090, 0, 0);
    v2.set(4096, 0, 0);
    Vec3I dx{1, 0, 0};
    for (int i = 0; i < 11; ++i) {
      v1 += dx;
      double d = 5-i;
      d = sqrt(d*d);
      double di = v1.distance_to(v2);
//      std::cout << d<<" "<<di << " "<<v1<<" "<<v2<<"\n";
      test_result &= UnitTests::assert_equals(d, di, tolerance);
      if(di > 3.0001)
        test_result &= UnitTests::assert_equals(9,v1.distance_square_to(v2,9), tolerance);
      else
        test_result &= UnitTests::assert_equals(di*di,v1.distance_square_to(v2,9), tolerance);
    }
    return test_result;
});



RUN_TESTS
