#include <memory>

#include <core/data/io/hssp_io.hh>
#include <utils/UnitTests.hh>

#include "../../data_sets.hh"

using utils::UnitTests;
using utils::DefineTest;

using namespace core::data::io;

// --- read benzene molecule from mol2 format
DefineTest test_read_hssp("read MSA in HSSP format", []() {

    bool test_result = true;

    std::istringstream inp(hssp_1ans);
    std::vector<std::shared_ptr<core::data::sequence::Sequence>>  sink;
    core::data::io::read_hssp_file(inp, sink);
    // ---------- test if we have 6 sequences
    test_result &= UnitTests::assert_equals(sink.size(), (size_t) 6);
    for(const auto & seq:sink) {
      test_result &= UnitTests::assert_equals(seq->length(), (size_t) 27);
    }
    return test_result;
});
RUN_TESTS
