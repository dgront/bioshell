#include <string>
#include <sstream>

#include <core/data/structural/Chain.hh>
#include <core/data/io/Pdb.hh>
#include <core/BioShellEnvironment.hh>
#include <utils/UnitTests.hh>
#include <core/data/structural/ResidueSegment.hh>
#include <core/data/structural/ResidueSegmentProvider.hh>

#include "../../data_sets.hh" // --- provides input data as std::string objects

using utils::UnitTests;
using utils::DefineTest;

DefineTest create_segment_from_chain( "create ResidueSegment object from a Chain object ", []() {

    bool test_result = true;
    std::stringstream in(pdb_1cey);
    core::data::io::Pdb pdb(in);
    core::data::structural::Structure_SP s = pdb.create_structure(0);

    core::data::structural::Chain_SP chain = s->get_chain(0);
    core::data::structural::ResidueSegment rs(chain, 5, 10);
    test_result &= UnitTests::assert_equals(std::string{"A"}, rs.chain_id());
    test_result &= UnitTests::assert_equals(core::index2(6), rs.length());
    // --- test if the first residue of the segment is LYS7 (numbering starts from 2 in this protein)
    test_result &= UnitTests::assert_equals(7, rs[0]->id());
    std::string seq = rs.sequence()->sequence;
    test_result &= UnitTests::assert_equals(std::string{"KFLVVD"}, seq);
    test_result &= UnitTests::assert_true(rs.contains(*(*chain)[5]),"Residue 5 must belong to this segment");
    test_result &= UnitTests::assert_true(rs.contains(*(*chain)[10]),"Residue 10 must belong to this segment");
    test_result &= UnitTests::assert_true(!rs.contains(*(*chain)[11]),"Residue 11 must NOT belong to this segment");

    // --- test if iteration over segment's residues works
    seq = "";
    for(core::data::structural::Residue_SP r:rs)
      seq += r->residue_type().code1;
    test_result &= UnitTests::assert_equals(std::string{"KFLVVD"}, seq);
    // --- test also const-iteration
    seq = "";
    for(const core::data::structural::Residue_SP r:rs)
      seq += r->residue_type().code1;
    test_result &= UnitTests::assert_equals(std::string{"KFLVVD"}, seq);

    // --- try a segment spanning the whole chain
    core::data::structural::ResidueSegment chain_rs(chain, 0, chain->size()-1);
    seq = "";
    for(const core::data::structural::Residue_SP r:chain_rs)
          seq += r->residue_type().code1;
    test_result &= UnitTests::assert_equals(chain->create_sequence()->sequence, seq);

    return test_result;
});

DefineTest create_segment_provider( "test ResidueSegmentProvider ", []() {

    bool test_result = true;
    std::stringstream in(partial_2gb1);     // --- residues 5-14 of 2gb1
    core::data::io::Pdb pdb(in);
    core::data::structural::Structure_SP frag = pdb.create_structure(0);
    std::stringstream in2(pdb_2gb1);     // --- full 2gb1
    core::data::io::Pdb pdb2(in2);
    core::data::structural::Structure_SP full = pdb2.create_structure(0);
    frag->push_back(full->get_chain(0)->clone());
    frag->push_back(frag->get_chain(0)->clone());
    frag->push_back(full->get_chain(0)->clone());
    frag->push_back(frag->get_chain(0)->clone());

    test_result &= UnitTests::assert_equals(core::index2(5), frag->count_chains());
    std::vector<size_t> chain_size{10, 56, 10, 56, 10};
    for(int i=0;i<5;++i)
      test_result &= UnitTests::assert_equals(chain_size[i], frag->get_chain(i)->size());

    core::data::structural::ResidueSegmentProvider rsp(frag, 50); // --- 2gb1 has 2 fragments, the fragment none
    test_result &= UnitTests::assert_true(rsp.has_next(),"ResidueSegmentProvider must have segments!");
    int i_frag = 0;
    while (rsp.has_next()) {
      ++i_frag;
      rsp.next();
    }
    test_result &= UnitTests::assert_equals(12, i_frag);

    return test_result;
});


DefineTest use_SecondaryStructureElementProvider( "test SecondaryStructureElementProvider ", []() {

    bool test_result = true;
    std::stringstream in2(pdb_6ats);
    core::data::io::Pdb pdb2(in2, core::data::io::keep_all, core::data::io::keep_all, true);
    core::data::structural::Structure_SP s = pdb2.create_structure(0);

    core::data::structural::SecondaryStructureElementProvider sses(s);
    int n=0;
    while (sses.has_next()) {
      auto sse = sses.next();
      ++n;
    };
    test_result &= UnitTests::assert_equals(2, n);

    return test_result;
});



RUN_TESTS
