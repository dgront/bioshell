#include <string>
#include <sstream>

#include <core/data/io/Cif.hh>
#include <core/BioShellEnvironment.hh>
#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

DefineTest read_cif_ALA( "read the ALA monomer in CIF format ", []() {

    bool test_result = true;
    core::data::io::Cif m(core::BioShellEnvironment::from_file_or_db("monomers/ALA.cif"));
    test_result &= UnitTests::assert_equals(core::index4(1), m.size());
    test_result &= UnitTests::assert_equals(std::string{"data_ALA"}, m.begin()->first);
    test_result &= UnitTests::assert_equals(core::index4(6), m.begin()->second->size());
    int i = -1;
    std::vector<bool> expct{false, true, true, true, true, true};
    for(const auto & section:*m.begin()->second)
      test_result &= UnitTests::assert_equals(bool(expct[++i]),section->is_loop());

    return test_result;
});

DefineTest read_cif_HEM( "read the HEM monomer in CIF format ", []() {

    bool test_result = true;

    core::data::io::Cif hem(core::BioShellEnvironment::from_file_or_db("monomers/HEM.cif"));
    test_result &= UnitTests::assert_equals(core::index4(1), hem.size());
    test_result &= UnitTests::assert_equals(std::string{"data_HEM"}, hem.begin()->first);
    test_result &= UnitTests::assert_equals(size_t(8), hem.begin()->first.size());
    return test_result;
});

RUN_TESTS
