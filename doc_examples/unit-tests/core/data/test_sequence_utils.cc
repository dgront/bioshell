#include <memory>

#include <core/data/sequence/sequence_utils.hh>
#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::data::sequence;

// --- check if HEC string is correctly split into SSEs
DefineTest collect_bb_hbonds("count helices and strands in a HEC string", []() {

    bool test_result = true;

    std::string sec_str = "CCEEEEEECCHHHHHHHHHCHHHCCCCEEEEEECCCCCCCEEEEEECHHHHCCCCCCCCCHHHHHHHCCCCEEEECCHHHHHHHHHHHHHHHCCCCCCEEEEECCEEEEECCCCCCCHHHHHHHHHHHHHHHHHHHCCCEEEEEECCHHHHHHHHHHCCCCCEEECCHHHHHHHHHHCCHHHCCCCCHHHHHHHHHCCCCCCCHHHHCCHHHHHHHHCHHHHHHHHHHHCCCCCCCCCCCCCCEEEEEEEEEEEECCHHHHHHHHHHHHHHHHHHHCCCCEEEEEEEEEECCCCEEEEEEECCCCCCHHHHHHHHHHHHHHHHHHCCCCEEEEEEEEEEEC";
    const auto cnt = count_helices_strands(sec_str);
    // ---------- test if we have 15 helices
    test_result &= UnitTests::assert_equals((core::index2)15, cnt.first);
    // ---------- test if we have 12 strands
    test_result &= UnitTests::assert_equals((core::index2)12, cnt.second);

    return test_result;
});
RUN_TESTS
