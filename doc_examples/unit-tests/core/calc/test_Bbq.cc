#include <core/calc/structural/Bbq.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/data/structural/Residue.hh>

#include <utils/UnitTests.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/transformations/Crmsd.hh>
#include <core/protocols/PairwiseCrmsd.hh>

#include "../../data_sets.hh" // --- provides input data as std::string objects
#include <core/calc/structural/transformations/Crmsd.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::calc::structural;

// --- tests hierarchical clustering on a case of clustering letters
DefineTest rebuild_one_residue( "test reconstruction of a single peptide plate", []() {

    using namespace core::data::structural;
    bool test_result = true;
    float epsilon = 0.001;

    std::stringstream in(pdb_2gb1);
    core::data::io::Pdb pdb(in);
    Structure_SP s = pdb.create_structure(0);
    const core::data::structural::Chain & c = *s->get_chain(0);
    std::vector<std::string> atoms{" C  "," N  "};
    double n_error = 0;
    double c_error = 0;
    double o_error = 0;
    double cnt = 0;
    for (auto i=0;i<c.size()-2;++i) {
        double lambda = evaluate_lambda(*c[i], *c[i+1], *c[i+2]);
        Bbq bbq;
        cnt+=1;
        Residue_SP r_out = std::make_shared<Residue>(c[i+1]->id(), c[i+1]->residue_type());
        Residue_SP r_out_next = std::make_shared<Residue>(c[i+2]->id(), c[i+2]->residue_type());
        r_out->owner(c[i+1]->owner());
        r_out_next->owner(c[i+1]->owner());

        transformations::Rototranslation_SP lcs = transformations::local_BBQ_coordinates(*c[i]->find_atom(" CA "),
                                                                                         *c[i+1]->find_atom(" CA "),
                                                                                         *c[i+2]->find_atom(" CA "));
        bbq.rebuild(lambda, lcs, r_out, r_out_next);
        core::data::structural::selectors::IsBB s1{};
        Residue_SP a = c[i+1]->clone(s1);
        Residue_SP b = c[i+2]->clone(s1);
        c_error+=abs(a->find_atom(" C  ")->x-r_out->find_atom(" C  ")->x);
        n_error+=abs(b->find_atom(" N  ")->x-r_out_next->find_atom(" N  ")->x);
        o_error+=abs(a->find_atom(" O  ")->x-r_out->find_atom(" O  ")->x);
        c_error+=abs(a->find_atom(" C  ")->y-r_out->find_atom(" C  ")->y);
        n_error+=abs(b->find_atom(" N  ")->y-r_out_next->find_atom(" N  ")->y);
        o_error+=abs(a->find_atom(" O  ")->y-r_out->find_atom(" O  ")->y);
        c_error+=abs(a->find_atom(" C  ")->z-r_out->find_atom(" C  ")->z);
        n_error+=abs(b->find_atom(" N  ")->z-r_out_next->find_atom(" N  ")->z);
        o_error+=abs(a->find_atom(" O  ")->z-r_out->find_atom(" O  ")->z);

    }
    test_result &= UnitTests::assert_equals(0.019, n_error/cnt/3, epsilon);
    test_result &= UnitTests::assert_equals(0.016, c_error/cnt/3, epsilon);
    test_result &= UnitTests::assert_equals(0.031, o_error/cnt/3, epsilon);

   // std::cout<<"N error "<<n_error/cnt/3<<" C error "<<c_error/cnt/3<<" O error "<<o_error/cnt/3<<"\n";
    return test_result;
});
// --- tests hierarchical clustering on a case of clustering letters
DefineTest rebuild_one_chain( "test reconstruction of a chain", []() {

    using namespace core::data::structural;
    using namespace core::calc::structural::transformations;
    bool test_result = true;
    float epsilon = 0.001;

    std::stringstream in(pdb_2gb1);
    core::data::io::Pdb pdb(in,core::data::io::is_bb);
    Structure_SP s = pdb.create_structure(0);
    const core::data::structural::Chain c = *(s->get_chain(0));
    Bbq bbq;
    double lambda;
    std::vector<double> lambdas;
    for (auto i=0;i<c.size()-2;++i) {
        lambda = evaluate_lambda(*c[i], *c[i + 1], *c[i + 2]);
        lambdas.push_back(lambda);
    }

    Chain_SP new_chain = bbq.rebuild(c,lambdas);
//    std::cout<<"\n";
//        for (auto res:*new_chain)
//            for (auto a:*res)
//            std::cout<<a->to_pdb_line()<<"\n";
    test_result &= UnitTests::assert_equals(56, new_chain->size(), epsilon);
    test_result &= UnitTests::assert_equals(5, (*new_chain)[0]->count_atoms(), epsilon);
    test_result &= UnitTests::assert_equals(5, (*new_chain)[new_chain->size()-1]->count_atoms(), epsilon);
    Crmsd<std::vector<Vec3>,std::vector<Vec3>> rms;
    std::vector<Vec3> q, t;
    for (auto atom_it = new_chain->first_atom(); atom_it != new_chain->last_atom(); ++atom_it)
        if ((*atom_it)->atom_name()!=" CB ") q.push_back(**atom_it);
    for (auto atom_it = s->first_atom(); atom_it != s->last_atom(); ++atom_it) t.push_back(**atom_it);
    double crms =rms.crmsd(q, t, s->count_atoms());
    test_result &= UnitTests::assert_equals(0.096, crms, epsilon);
    return test_result;
});


RUN_TESTS
