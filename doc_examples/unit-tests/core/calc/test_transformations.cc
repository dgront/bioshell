#include <core/data/basic/Vec3.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>

#include <utils/UnitTests.hh>

using core::data::basic::Vec3;
using utils::UnitTests;
using utils::DefineTest;

// --- loads a molecule from PDB data and tests find_cycles() function
DefineTest test_lcs_3atoms("create a coordinate system based on three well-known atoms", []() {

    bool test_result = true;

    Vec3 a(0.0, 0.0, 0.0);
    Vec3 b(2.0, 0.0, 2.0);
    Vec3 c(4.0, 0.0, 0.0);

    auto rot = core::calc::structural::transformations::local_coordinates_three_atoms(a, b, c);
    test_result &= UnitTests::assert_equals(rot->rot_x(), Vec3{1.0, 0.0, 0.0}, 0.0000001);
    test_result &= UnitTests::assert_equals(rot->rot_y(), Vec3{0.0, 1.0, 0.0}, 0.0000001);
    test_result &= UnitTests::assert_equals(rot->rot_z(), Vec3{0.0, 0.0, 1.0}, 0.0000001);

//    std::cout << *rot;
    return test_result;
});

RUN_TESTS
