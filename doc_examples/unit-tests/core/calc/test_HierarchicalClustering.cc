#include <core/calc/clustering/DistanceByValues.hh>
#include <core/calc/clustering/HierarchicalClustering.hh>
#include <core/calc/clustering/HierarchicalCluster.hh>
#include <utils/UnitTests.hh>

using utils::UnitTests;
using utils::DefineTest;

using namespace core::calc::clustering;

// --- tests hierarchical clustering on a case of clustering letters
DefineTest cluster_letters( "cluster letters  (complete link)", []() {

    bool test_result = true;
    float epsilon = 0.0001;

    const std::vector<std::string> labels{"A","B","D","E","F","I","J","K"};
    std::vector<float> vals;
    for(const std::string &s:labels) {
      float i = int(s[0]-'A');
      i += 0.1 * i * i;
      vals.push_back(i);
    }
    DistanceByValues<float> dist(labels,20,21);
    for(size_t i=1;i<labels.size();++i) {
      for(size_t j=0;j<i;++j) {
        float d = vals[i] - vals[j];
        d = sqrt(d * d);
        dist.set(i, j, d);
        dist.set(j, i, d);
      }
    }
    // ---------- test if values corresponding to letters were computed correctly
    // --- value for element "D" at index 2 should be 3 + 3*3*0.1 = 3.9
    test_result &= UnitTests::assert_equals(3.9, vals[2], epsilon);
    // --- value for the last element "K" should be 20
    test_result &= UnitTests::assert_equals(20.0, vals.back(), epsilon);
    // --- check distance between elements 2 and 5
    test_result &= UnitTests::assert_equals(10.5, dist.get(2, 5), epsilon);

    // ---------- Run the clustering
    HierarchicalClustering<float,std::string> clustering(labels," ");
    CompleteLink<float> complete_link;
    clustering.run_clustering(dist,complete_link);
    std::shared_ptr<HierarchicalCluster<float,std::string> > root = clustering.get_root();

    // ---------- Test the clustering process
    test_result &= UnitTests::assert_equals(size_t(8), clustering.count_objects());
    test_result &= UnitTests::assert_equals(size_t(15), clustering.count_steps());
    std::vector<float> expected_dist{1.1,1.7,2.7,3.6,5.6,7.5,20.0};
    test_result &= UnitTests::instance().assert_equals(0.0, clustering.clustering_step(7)->merging_distance, epsilon);
    for (int i = 8; i < 15; ++i)
      test_result &= UnitTests::assert_equals(expected_dist[i - 8],
                                                          clustering.clustering_step(i)->merging_distance, epsilon);

    return test_result;
});

std::string crmsd_distances = R"(6w9c 6wrh  0.887
6w9c 6wuu  1.316
6w9c 6wx4  0.816
6w9c 6wzu  0.923
6wrh 6wuu  1.538
6wrh 6wx4  0.647
6wrh 6wzu  0.175
6wuu 6wx4  1.537
6wuu 6wzu  1.608
6wx4 6wzu  0.672
)";

std::vector<float> exp_dist{0.175, 0.672, 0.923, 1.608};
std::vector<std::string> exp_elem{"6wx4", "6wzu", "6wrh", "6w9c"};

// --- tests hierarchical clustering starting from a given matrix of crmsd values
DefineTest cluster_crmsd( "cluster proteins by crmsd distances (complete link)", []() {

    bool test_result = true;
    float epsilon = 0.0001;

    std::istringstream ss(crmsd_distances);
    DistanceByValues<float> dist(ss,5,1.9,2.0);

    // ---------- Run the clustering
    HierarchicalClustering<float,std::string> clustering(dist.labels()," ");
    CompleteLink<float> complete_link;
    clustering.run_clustering(dist,complete_link);
    std::shared_ptr<HierarchicalCluster<float,std::string> > root = clustering.get_root();

    // ---------- Test the clustering process
    test_result &= UnitTests::assert_equals(size_t(5), clustering.count_objects());
    test_result &= UnitTests::assert_equals(size_t(9), clustering.count_steps());

    for (int i = 5; i < 9; ++i) {
      test_result &= UnitTests::assert_equals(exp_dist[i - 5],
                                              clustering.clustering_step(i)->merging_distance, epsilon);
    }
    auto clusters = clustering.get_clusters(1.6, 1);
    std::vector<std::string> elements;
    collect_leaf_elements(std::static_pointer_cast<BinaryTreeNode<std::string>>(clusters[0]), elements);

    for (int i = 0; i < 4; ++i) test_result &= UnitTests::assert_equals(exp_elem[i], elements[i]);

    DistanceByValues<float> dist2(ss, 5, 1.9, 2.0);

    return test_result;
});

RUN_TESTS
