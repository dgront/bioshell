#include <memory>

#include <core/data/io/Pdb.hh>
#include <core/calc/structural/interactions/HydrogenBondCollector.hh>
#include <core/calc/structural/interactions/BackboneHBondCollector.hh>
#include <utils/UnitTests.hh>

#include "../../data_sets.hh" // --- provides input data as std::string objects

using utils::UnitTests;
using utils::DefineTest;

using namespace core::calc::structural::interactions;

// --- detect side chain - side chain hydrogen bonds
DefineTest collect_hbonds("collect hydrogen bonds", []() {

  bool test_result = true;
  core::data::structural::Structure_SP strctr = core::data::io::Pdb(pdb_1cey, "").create_structure(0);
  HydrogenBondCollector collect_hbonds(false);

  std::vector<ResiduePair_SP> hbonds;
    collect_hbonds.collect(*strctr, hbonds);

  // ---------- test if we have 14 hbonds detected
  test_result &= UnitTests::assert_equals((size_t)14, hbonds.size());

  return test_result;
});

// --- detect backbone hydrogen bonds
DefineTest collect_bb_hbonds("collect backbone hydrogen bonds", []() {

  bool test_result = true;
  core::data::structural::Structure_SP strctr = core::data::io::Pdb(pdb_2gb1, "").create_structure(0);
  BackboneHBondCollector collect_hbonds;

  std::vector<ResiduePair_SP> hbonds;
  collect_hbonds.dssp_energy_cutoff(-0.4);
    collect_hbonds.collect(*strctr, hbonds);

  // ---------- test if we have 38 hydrogen bonds in 2GB1 protein
  test_result &= UnitTests::assert_equals((size_t)38, hbonds.size());
  // ---------- test if we have 55 donors (amide groups)
  test_result &= UnitTests::assert_equals((size_t)55, collect_hbonds.recent_donor_atoms().size());
  // ---------- test if we have 56 acceptors (carbonyl groups)
  test_result &= UnitTests::assert_equals((size_t)56, collect_hbonds.recent_acceptor_atoms().size());

  return test_result;
});
RUN_TESTS
