#include <memory>

#include <core/data/io/Pdb.hh>
#include <core/calc/structural/calculate_from_structure.hh>
#include <utils/UnitTests.hh>

#include "../../data_sets.hh" // --- provides input data as std::string objects

using utils::UnitTests;
using utils::DefineTest;


DefineTest compute_rg("calculate the Rg of a Structure", []() {

    bool test_result = true;
    core::data::structural::Structure_SP strctr = core::data::io::Pdb(pdb_2gb1, "").create_structure(0);

    double rg = sqrt(core::calc::structural::calculate_Rg_square(*strctr));

    test_result &= UnitTests::assert_equals(10.7, rg, 0.1);
    return test_result;
});


DefineTest compute_cm("calculate the center of mass of a Structure", []() {

    bool test_result = true;
    core::data::structural::Structure_SP strctr = core::data::io::Pdb(pdb_2gb1, "").create_structure(0);

    Vec3 cm{1.0,2.0,3.0};
    core::calc::structural::center_at(*strctr, cm);
    cm = core::calc::structural::calculate_cm(*strctr);
    test_result &= UnitTests::assert_equals(1.0, cm.x, 0.0001);
    test_result &= UnitTests::assert_equals(2.0, cm.y, 0.0001);
    test_result &= UnitTests::assert_equals(3.0, cm.z, 0.0001);

    // --- test on Vec3 data
    std::vector<Vec3> v{{1,0,0}, {0,1,0}, {1,1,0},{0,0,0}};
    cm = core::calc::structural::calculate_cm(v.cbegin(),v.cend());
    test_result &= UnitTests::assert_equals(0.5, cm.x, 0.0001);
    test_result &= UnitTests::assert_equals(0.5, cm.y, 0.0001);
    test_result &= UnitTests::assert_equals(0.0, cm.z, 0.0001);

    return test_result;
});
RUN_TESTS
