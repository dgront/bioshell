0. Jak zwykle przed pracą z BioShell'em - pobierz najnowszą wersją z repozytorium i skompiluj
```
        git pull
        cd build
        make
```
1. Załóż katalog na swoje aplikacje; powinien on się znajdować w `./src/apps/user-apps`, obok katalogu `dgront`.
 W tym katalogu będziesz umieszczać swoje pliki źródłowe (`.cc` oraz `.hh`). Załóżmy, że Twój user-ID to **annak** - załóż więc katalog o tej nazwie

2. Stwórz swój pierwszy plik `.cc`. Możesz w tym celu napisać proste _Hello, World_, albo skopiować jedną z aplikacji przykładowych.

3. Dodaj swój program do listy kompilowanych plików. Właściwym miejscem jest plik `CMakeLists-pilot_apps.txt`.
Stwórz nową sekcję, analogicznie do już istniejących<br>
Pierwsza linia to komentarz, w drugiej deklarowany jest **zbiór plików źródłowych** (jednoelementowy w tym przypadku).
Trzecia linijka mówi, że ma powstać program o nazwie `hello_world` powstały z plików źródłowych z powyżej zadeklarowanego zbioru.
Ostatnia linijka zaś wymienia biblioteki które trzeba zlinkować z programem.

```
        # --- annak ---
        set (hello_world_SOURCES apps/user-apps/annak/hello_world.cc)
        add_executable (hello_world ${hello_world_SOURCES})
        TARGET_LINK_LIBRARIES(hello_world bioshell ${ZLIB_LIBRARY})
```

4. Teraz już możesz przejść do katalogu `build` i kiedy wpiszesz tam `make hello_world`, Twój program zostanie skompilowany i pojawi się w latalogu `bin`