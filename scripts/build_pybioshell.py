#!/usr/bin/env python

import sys, os, subprocess, platform, argparse
from argparse import RawTextHelpFormatter
from glob import glob
from utils import execute
from os.path import expanduser, join


class BuildPython:
  """ Base class to set paths to pybioshell compilation

    Use BuildPythonXY.py to complile PyBioShell for Python in version X.Y, e.g. BuildPython35.py
    to get a library compatible with Python3.5
  """

  def __init__(self):
    self.HOME_DIR = expanduser("~")
    self.BIOSHELL_DIR       = "./"
    self.PROJECT_SOURCES    = "./src"
    self.PROJECT_BUILD      = "./build_bindings"
    self.PROJECT_OUTPUT_DIR = "./bin"
    self.PROJECT_BINDINGS   = "./bindings"
    self.PROJECT_NAME       = "pybioshell"
    self.BUILD_TYPE         = '.release' # The other option is '.debug'
    self._machine_name_     = os.uname()[1]
    self.release            = 'release_40'
#    self.prefix             = '../binder/build/llvm-4.0'
    self.prefix             = '../binder/build/llvm-6.0.1'
    self.debug_binder       = " -annotate-includes -trace "
    self.debug_flag         = False
    self.DO_NOT_BIND        = {"apps","doc-examples"}
    self.version            = "3.7"
    self.python_m           = "m"
    self.SUBMODULE_PATH_ROOT = self.HOME_DIR+"/src.git/bioshell/external/"

    if sys.platform.startswith("linux"):     # Ubuntu settings
        self.Platform = "linux"
        self.LINKER_CMD         = "clang++-6.0"
        self.CMAKE_C_COMPILER   = "clang-6.0"
        self.CMAKE_CXX_COMPILER = "clang++-6.0"
        self.PYBIND_INCLUDE_PATH = "../binder/build/pybind11/include"
        self.BINDER_SOURCE       = join(self.HOME_DIR,"src.git/binder")
        self.BINDER_PATH = "../binder/build/llvm-4.0.0/build_4.0.0.linux."+platform.node()+".release/bin/"
        self.MACOS_INCLUDES      = ""
        self.MACOS_LIBRARY       = ""
    elif sys.platform == "darwin": #MacOS settings
        self.Platform = "macos" 
        self.BINDER_SOURCE       = join(self.HOME_DIR,"src.git/binder")
        self.BINDER_PATH = "../binder/build/llvm-4.0.0/build_4.0.0.macos.mbp.local.release/bin"
#        self.BINDER_PATH = "../binder/build/llvm-6.0.1/build_6.0.1.macos.MacBook-Pro.local.release/bin"
        self.CMAKE_C_COMPILER   = "clang"
        self.CMAKE_CXX_COMPILER = "clang++"
        self.LINKER_CMD = "clang++"
        self.PYBIND_INCLUDE_PATH = "../binder/build/pybind11/include"
        self.MACOS_INCLUDES      = "-I/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include/c++/v1/"
        self.MACOS_INCLUDES     += " -I/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/usr/include"
        self.MACOS_INCLUDES     += " -I/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.15.sdk/usr/include"
        self.MACOS_INCLUDES     += " -I/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/10.0.0/include"
        self.MACOS_INCLUDES     += " -I/usr/include -I/usr/local/include"
        self.MACOS_INCLUDES     += " -I/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include"
#        self.MACOS_INCLUDES     += " -I/Library/Developer/CommandLineTools/usr/include/c++/v1/"
        self.MACOS_INCLUDES     += " -I/usr/local/Cellar/llvm/9.0.0_1/include/c++/v1/"
        self.MACOS_INCLUDES     += " -I/Library/Developer/CommandLineTools/SDKs/MacOSX10.15.sdk/usr/include/"
        self.MACOS_INCLUDES     += " -I/Library/Developer/CommandLineTools/usr/lib/clang/13.0.0/include/"
        self.MACOS_INCLUDES     += " -I/usr/local/Cellar/llvm/9.0.0_1/Toolchains/LLVM9.0.0.xctoolchain/usr/include/c++/v1/"
        
        self.MACOS_LIBRARY       = "-L/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/usr/lib"
        self.MACOS_LIBRARY      += " -L/Library/Developer/CommandLineTools/SDKs/MacOSX10.15.sdk/usr/lib"


    elif sys.platform == "cygwin" : self.Platform = "cygwin"
    elif sys.platform == "win32" : self.Platform = "windows"
    else: self.Platform = "unknown"
    self.MODULE_INCLUDES      = " -I" + self.SUBMODULE_PATH_ROOT + "frugally-deep/include/"
    self.MODULE_INCLUDES     += " -I" + self.SUBMODULE_PATH_ROOT + "eigen/"
    self.MODULE_INCLUDES     += " -I" + self.SUBMODULE_PATH_ROOT + "json/include/"
    self.MODULE_INCLUDES     += " -I" + self.SUBMODULE_PATH_ROOT + "FunctionalPlus/include/"

    print("------------------------ "+self.Platform+" --------------------------")

    
  def run_cmake(self,node) :
    """Creates a CMakeLists.txt file and compile the library obj files """

    cmakefile = open(os.path.join(self.PROJECT_BINDINGS,"CMakeLists.txt"),"w")
    cmakefile.write("ADD_LIBRARY( pybioshell SHARED\n\n# Project's includes\n")
    print("ADD_LIBRARY( pybioshell SHARED")
    print("\n# Project's includes")
    sources = [ src for src in open(os.path.join(self.PROJECT_BINDINGS,self.PROJECT_NAME+".sources"))]
    sources.sort()
    for src in sources:
      cmakefile.write(" "+src.strip()+"\n")
    
    cmakefile.write("\n# BioShell .cc files\n")
    for cc_src in  self.find_all_files(self.PROJECT_SOURCES,".cc") :
      cmakefile.write(" ../"+cc_src.strip()+"\n")
    
    cmakefile.write(")\nTARGET_LINK_LIBRARIES( pybioshell ${ZLIB_LIBRARY} python%s)\n"%(self.version+self.python_m))
    cmakefile.close()

    if not os.path.exists(self.PROJECT_BUILD): os.makedirs(self.PROJECT_BUILD)
    os.chdir(self.PROJECT_BUILD)
    if self.debug_flag :
      execute("","cmake ..  -DPYBIOSHELL=%s -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_COMPILER=%s -DCMAKE_C_COMPILER=%s" % (self.version, self.CMAKE_CXX_COMPILER, self.CMAKE_C_COMPILER))
    else :
      execute("","cmake ..  -DPYBIOSHELL=%s -DCMAKE_CXX_COMPILER=%s -DCMAKE_C_COMPILER=%s" % (self.version, self.CMAKE_CXX_COMPILER, self.CMAKE_C_COMPILER))
    execute("","make -j %s" % (node))
    os.chdir("../")
    if sys.platform.startswith("linux"): 
      os.rename("bin/libpybioshell.so","bin/pybioshell.so")
    elif sys.platform == "darwin" : 
       os.rename("bin/libpybioshell.dylib","bin/pybioshell.so")

  def find_all_files(self,which_dir,extension_suffix):

    hh_list = []
    
    for subdir,dirs,files in os.walk(which_dir, topdown=True):
      if subdir == "./" : continue
      if_skip = False
      for dirn in self.DO_NOT_BIND :
        if subdir.find(dirn) != -1 : 
          if_skip = True
          sys.stderr.write("Skipping "+subdir+"\n")
          break
      if if_skip : continue
      
      subdir_new = subdir.replace("./","")
      for fn in files : 
        if fn.endswith(extension_suffix) : hh_list.append(os.path.join(subdir_new,fn))
      
    return hh_list
    
  def create_bindings(self):
    """Creates PROJECT_NAME.hh file and runs binder"""
    
    os.chdir(self.PROJECT_SOURCES)
    hh_list = self.find_all_files("./",".hh")
    listfile = open(self.PROJECT_NAME+".hh","w")
    for hh_file in hh_list : 
        listfile.write("#include <"+hh_file+">\n")
    listfile.close()
    os.chdir("../")

    execute("","(echo %s ; cat %s.config) >%s.myconfig" %("'+include <"+self.BINDER_SOURCE+"/source/stl_binders.hpp>'", self.PROJECT_NAME, self.PROJECT_NAME))
    binder_cmd = os.path.join(self.BINDER_PATH,"binder") + \
      " -v --root-module %s --config %s  --max-file-size=1000000  --prefix %s/ " % (self.PROJECT_NAME, self.PROJECT_NAME+".myconfig", self.PROJECT_BINDINGS)
    if self.debug_flag : binder_cmd += " --annotate-includes --trace "
    includes = ""
    for inc_path in self.PYTHON_INCLUDE_PATH.split() :
      includes += " -I " + inc_path
    binder_cmd += "%s/%s.hh -- -x c++ -std=c++14 -I %s -I %s  %s %s %s" \
      % (self.PROJECT_SOURCES, self.PROJECT_NAME, self.PROJECT_SOURCES, self.PYBIND_INCLUDE_PATH, includes, self.MACOS_INCLUDES, self.MODULE_INCLUDES)

    execute("",binder_cmd)
    execute("","rm %s" %(self.PROJECT_NAME+".myconfig"))

  def link_library(self) :

    includes = ""
    for inc_path in self.PYTHON_INCLUDE_PATH.split() :
          includes += " -I " + inc_path

    linker_cmd  = """%s  -shared -std=gnu++14 -std=c++14 -v -fPIC -Wl,-undefined -Wl,dynamic_lookup  \
      -I %s  %s -I %s  """ % (self.LINKER_CMD, self.PYBIND_INCLUDE_PATH, includes, self.PROJECT_SOURCES)

    obj_files = self.find_all_files(self.PROJECT_BUILD,".o")
    for obj in obj_files : linker_cmd += " " + obj
    python_libs = ""
    for inc_path in self.PYTHON_LIBRARY_PATH.split() :
        python_libs += " -L " + inc_path
    linker_cmd += " -o %s/%s.so %s -lpython%s -lz " % (self.PROJECT_OUTPUT_DIR, self.PROJECT_NAME, python_libs, self.version+self.python_m)
    linker_cmd += self.MACOS_LIBRARY

    execute("",linker_cmd)

  def comment_locale_classes(self):

    os.chdir("%s/bindings/std/" % (self.BIOSHELL_DIR))
    if os.path.isfile("locale_classes.cpp"):
      execute("","mv locale_classes.cpp plikkk" )
      execute("","(echo // | paste -d' ' - plikkk) >locale_classes.cpp" )
    elif os.path.isfile("stl_map.cpp"):
      execute("","mv stl_map.cpp plikkk" )
      execute("","(echo // | paste -d' ' - plikkk) >stl_map.cpp" )
    execute("","rm plikkk")

    os.chdir("../../")

  def delete_old_dirs(self):
    if os.path.exists(self.PROJECT_BUILD):
     execute("","rm -rf %s" % self.PROJECT_BUILD)
    if os.path.exists(self.PROJECT_BINDINGS):
     execute("","rm -rf %s" % self.PROJECT_BINDINGS)
    
  def build(self,node):
    self.create_bindings()
    if not self.Platform == "macos" : self.comment_locale_classes()
    self.run_cmake(node)
    self.link_library()

class BuildPython27(BuildPython):
# Class to compile Pybioshell to Python2.7
    
  def __init__(self):

      BuildPython.__init__(self)
      self.version = "2.7"
      self.python_m = ""
      if sys.platform.startswith("linux"):     # Ubuntu settings
         
          self.PYTHON_LIBRARY_PATH = "/usr/lib/python2.7/"
          self.PYTHON_INCLUDE_PATH = "/usr/include/python2.7"
          
      elif sys.platform == "darwin" :  #MacOS settings
          
          self.PYTHON_LIBRARY_PATH = "/System/Library/Frameworks/Python.framework/Versions/2.7/lib"
          self.PYTHON_INCLUDE_PATH = "/System/Library/Frameworks/Python.framework/Versions/2.7/include/python2.7/"

class BuildPython35(BuildPython):
# Class to compile Pybioshell to Python3.5
  
  def __init__(self):

    BuildPython.__init__(self)
    self.version="3.5"
    if sys.platform.startswith("linux"):     # Ubuntu settings
        self.PYTHON_LIBRARY_PATH = "/usr/lib/x86_64-linux-gnu/"
        self.PYTHON_INCLUDE_PATH = "/usr/include/python3.5"

    elif sys.platform == "darwin" :  #MacOS settings
        self.PYTHON_LIBRARY_PATH = "/usr/local/Cellar/python/3.7.4/Frameworks/Python.framework/Versions/3.7/lib/"
        self.PYTHON_INCLUDE_PATH = "/usr/local/Cellar/python/3.7.4/Frameworks/Python.framework/Versions/3.7/include/python3.7m/"

class BuildPython36(BuildPython):
# Class to compile Pybioshell to Python3.6
  
  def __init__(self):

    BuildPython.__init__(self)
    self.version="3.6"
    if sys.platform.startswith("linux"):     # Ubuntu settings
        self.PYTHON_LIBRARY_PATH = "/usr/lib/x86_64-linux-gnu/"
        self.PYTHON_INCLUDE_PATH = "/usr/include/python3.6"

    elif sys.platform == "darwin" :  #MacOS settings
        self.PYTHON_LIBRARY_PATH = "/usr/local/Cellar/python/3.7.4/Frameworks/Python.framework/Versions/3.7/lib/"
        self.PYTHON_INCLUDE_PATH = "/usr/local/Cellar/python/3.7.4/Frameworks/Python.framework/Versions/3.7/include/python3.7m/"

class BuildPython39(BuildPython):
# Class to compile Pybioshell to Python3.9
  
  def __init__(self):

    BuildPython.__init__(self)
    self.version="3.9"
    self.python_m = ""
    if sys.platform.startswith("linux"):     # Ubuntu settings
        self.PYTHON_LIBRARY_PATH = "/usr/lib/x86_64-linux-gnu/"
        self.PYTHON_INCLUDE_PATH = "/usr/include/python3.9"

    elif sys.platform == "darwin" :  #MacOS settings
        self.PYTHON_LIBRARY_PATH = "/usr/local/Cellar/python/3.7.4/Frameworks/Python.framework/Versions/3.7/lib/"
        self.PYTHON_INCLUDE_PATH = "/usr/local/Cellar/python/3.7.4/Frameworks/Python.framework/Versions/3.7/include/python3.7m/"
        self.PYTHON_INCLUDE_PATH = " /opt/homebrew/Cellar/python\@3.9/3.9.8/Frameworks/Python.framework/Headers"

class BuildPython37(BuildPython):
# Class to compile Pybioshell to Python3.7

    def __init__(self):

        BuildPython.__init__(self)
        self.version="3.7"
        if sys.platform.startswith("linux"):     # Ubuntu settings
            self.PYTHON_LIBRARY_PATH = "/usr/lib/x86_64-linux-gnu/"
            self.PYTHON_INCLUDE_PATH = "/usr/local/include/python3.7m"
        elif sys.platform == "darwin" :  #MacOS settings
            self.PYTHON_LIBRARY_PATH =   "/usr/local/Cellar/python/3.7.4/Frameworks/Python.framework/Versions/3.7/lib/"
            self.PYTHON_LIBRARY_PATH += " /usr/local/Cellar/python/3.7.7/Frameworks/Python.framework/Versions/3.7/lib/"
            self.PYTHON_LIBRARY_PATH += " /usr/local/Cellar/python@3.7/3.7.9_2/Frameworks/Python.framework/Versions/3.7/lib/"
            #self.PYTHON_LIBRARY_PATH = "/opt/local/Library/Frameworks/Python.framework/Versions/3.7/lib"
            self.PYTHON_INCLUDE_PATH =   "/usr/local/Cellar/python/3.7.4/Frameworks/Python.framework/Versions/3.7/include/python3.7m/"
            self.PYTHON_INCLUDE_PATH += " /usr/local/Cellar/python/3.7.7/Frameworks/Python.framework/Versions/3.7/include/python3.7m/"
            self.PYTHON_INCLUDE_PATH += " /usr/local/Cellar/python@3.7/3.7.9_2/Frameworks/Python.framework/Versions/3.7/include/python3.7m"


if __name__ == "__main__" :

  parser = argparse.ArgumentParser(description="""
    Compiles PyBioshell to one of Python version.\n

    Usage example:
    python3.7 build_pybioshell.py -v 3.6
    """,formatter_class=RawTextHelpFormatter)

  parser.add_argument('-l', '--link', help="only links the library",action="store_true" ,required=False)
  parser.add_argument('-m', '--make', help="run cmake and make", action="store_true",required=False)
  parser.add_argument('-b', '--bind', help="creates bindings", action="store_true",required=False)
  parser.add_argument('-r', '--rm', help="deletes old bindings and build_bindings directories and prepares pybioshell compilation", action="store_true",required=False)
  parser.add_argument('-j', '--j', help="defines how mane cores will be use to build pybioshell",required=False,default=4)
  parser.add_argument('-d', '--debug', help="create a debug bulid",action="store_true",required=False,default=False)
  parser.add_argument('-v', '--version', help="choose a Python version",required=False,default="37")
  
  
  parser = parser.parse_args()
  versions=["27","35","36","37","2.7","3.5","3.6","3.7","3.9","39"]
  print(parser.version," version")
  if parser.version in versions:
    version = parser.version if float(parser.version)>20 else str(int(float(parser.version)*10))
  else:
    print("Unknown python version. Build is possible for versions: 2.7, 3.5, 3.6, 3.7.")
    sys.exit()
  print("Build for version ",version)
  #module = __import__("BuildPython"+version)
  #my_class = getattr(module, "BuildPython"+version)
  my_class = globals()["BuildPython"+version]
  p = my_class()
  print(p)

  if parser.debug : p.debug_flag = True
  
  if parser.rm:
    print("Removing old bindings and build_bindings directories...")
    p.delete_old_dirs()
  if not parser.bind and not parser.link and not parser.make:
    print("Building Pybioshell...")
    p.build(parser.j)
  else:
    if parser.bind:
      print("Creating bindings...")
      p.create_bindings()
      if not p.Platform == "macos" : p.comment_locale_classes()
    if parser.make:
      print("Running cmake and make...")
      p.run_cmake(parser.j)
    if parser.link:
      print("Linking library...")
      p.link_library()


  
