#!/usr/bin/env python

#$ -cwd
#$ -S /usr/bin/python

# ---------- beta paths
#PSIBLASTDIR = "/mnt/storage/TOOLS/LEGACY/bin"
#DATABASE    = "/mnt/storage/TOOLS/DB/nr/nr"
#PSIPREDDIR  = "/mnt/storage/TOOLS/psipred/bin"
#DATADIR     = "/mnt/storage/TOOLS/psipred/data"

# ---------- funk paths
#PSIBLASTDIR = "/home/users/dgront/STORAGE/TOOLS/LEGACY/bin"
#DATABASE    = "/home/users/dgront/STORAGE/TOOLS/DB/nr/nr"
PSIPREDDIR  = "/home/users/dgront/STORAGE/TOOLS/psipred/bin"
DATADIR     = "/home/users/dgront/STORAGE/TOOLS/psipred/data"

# ---------- halny paths
#PSIBLASTDIR = "/usr/local/TOOLS/blast/bin"
#DATABASE    = "/usr/local/TOOLS/blast/data/nr"
#PSIPREDDIR  = "/usr/local/TOOLS/psipred/bin"
#DATADIR     = "/usr/local/TOOLS/psipred/data"



import sys, commands, os, os.path
#from run_psiblast import *
from run_jobs_commons import *

def runPsiPredOnly(inputASN1) :

  fname = os.path.basename(inputASN1).replace(".asn1","")
  
  out = runCommand("%s/chkparse  %s > %s" % ( PSIPREDDIR, inputASN1, fname+".mtx"), fname+".mtx" )
  if out == False :
    print >>sys.stderr, "FATAL: Error whilst running psipred (chkparse failed) - script terminated!"
    return False
  
  out = runCommand("%s/psipred %s.mtx %s/weights.dat %s/weights.dat2 %s/weights.dat3 > %s.psipred.ss" % (PSIPREDDIR,fname,DATADIR,DATADIR, DATADIR,fname), fname + ".psipred.ss")
  if out == False :
    print >>sys.stderr, "FATAL: Error whilst running psipred (check if missing output file) - script terminated!"
    return False

  out = runCommand("%s/psipass2 %s/weights_p2.dat 1 1.0 1.0 %s.psipred.ss2 %s.psipred.ss > %s.horiz" % (PSIPREDDIR, DATADIR, fname, fname, fname), fname+".psipred.ss2", fname+".horiz" )
  for i in ["aux","psipred.ss","horiz","mtx"] : 
    if os.path.isfile(fname + "." + i ) : os.remove( fname + "." + i )
  
  return out
    
if __name__ == "__main__" :

  inASN1 = getArgument(1,"INASN1")
  if len(inASN1) == 0 :
    print "No input .asn1 file given!"
  else :
    runPsiPredOnly(inASN1)

