#!/usr/bin/env python

#$ -cwd
#$ -S /usr/bin/python

SPINEXCODIR = "/home/users/dgront/STORAGE/TOOLS/spineXpublic/"

import os, re, sys, commands
from run_jobs_commons import *

"""
INFO: dla inputu: sciezka/file.fasta wyniki:
	predykcji struktury drugorzedowej w ./file.spinex.ss2
	predykcji dostepu do rozpuszczalnika w ./file.spinex.asa
"""

codes  = ['B','b','e','E']
maxASA = {'A' :  88.6,'R' : 173.4,'D' : 150.0,'N' : 114.1,'C' : 108.5,'E' : 190.0,'Q' : 143.8,'G' :  60.1,
	  'H' : 153.2,'I' : 166.7,'L' : 166.7,'K' : 168.6,'M' : 162.9,'F' : 189.9,'P' : 112.7,'S' :  89.0,
	  'T' : 116.1,'W' : 227.8,'Y' : 193.6,'V' : 140.0,'X' : 227.8} # C.Chothia, J. Mol. Biol., 105(1975)1-14

def extractSSPred(spinex_out, ss_fname):
  out_f = open(ss_fname, 'w')
  output = "# PSIPRED VFORMAT (PSIPRED V2.6 by David Jones) converted from Spine X result"
  for number, line in enumerate(spinex_out):
    line = line[:-1].split()
    output += "\n% 4d %s %s   %1.3f  %1.3f  %1.3f" % (number+1, line[1], line[2], float(line[6]), float(line[7]), float(line[5]))
  out_f.write(output)
  out_f.close()

def extractASAPred(spinex_out, asa_fname):
  out_f = open(asa_fname, 'w')
  output = "# ASA in modified PSIPRED VFORMAT (PSIPRED V2.6 by David Jones) - output converted from Spine X result"
  for number, line in enumerate(spinex_out):
    line = line[:-1].split()
    rsa = float(line[10]) / maxASA[line[1]]
    rsa = min(0.999, rsa)
    code = int(rsa*len(codes))
    output += "\n% 4d %s %s   %5.1f %5.3f" % (number+1, line[1], codes[code], float(line[10]), rsa)
  out_f.write(output)
  out_f.close()

def runSpineX(inputMAT):
  basename = os.path.basename(inputMAT)
  fname = basename.replace(".mat","")
  fastadir = os.path.dirname(inputMAT)
  if fastadir == "":
    fastadir = '.'
  inputfname = 'spinex_input_'+fname
  inputf = open(inputfname, 'w')
  inputf.write(fname)
  inputf.close()

  print(SPINEXCODIR, fastadir,inputfname)


 
  # running phipsi_ss0
  ret = runCommand("%s/bin/phipsi_ss0.e  0 %s %s %s %s" % (SPINEXCODIR, SPINEXCODIR,fastadir, inputfname, fastadir), fastadir+"/out_ss0")
  # running phipsi_rsa
  ret = runCommand("%s/bin/phipsi_rsa.e  0 %s %s %s" % (SPINEXCODIR, SPINEXCODIR,fastadir, inputfname), fastadir+"/out_rsa")
  # running phipsi_phipsi0
  ret = runCommand("%s/bin/phipsi_phipsi0.e  0 %s %s %s %s" % (SPINEXCODIR, SPINEXCODIR,fastadir, inputfname, fastadir), fastadir+"/out_phipsi" )
  # running phipsi_ss1
  ret = runCommand("%s/bin/phipsi_ss1.e  0 %s %s %s" % (SPINEXCODIR, SPINEXCODIR,fastadir, inputfname), fastadir+"/out_ss1")
  # running phipsi_phipsi1
  ret = runCommand("%s/bin/phipsi_phipsi1.e  0 %s %s %s %s" % (SPINEXCODIR, SPINEXCODIR,fastadir, inputfname, fastadir), fastadir+"/out_phipsi1")

  return ret

def runSpineXForSSAndASA(inputFasta):
  ret = runSpineXForSS(inputFasta)
  if ret:
    return runSpineXForASA(inputFasta)
  return ret

def runSpineXForSS(inputFasta):
  fname = os.path.basename(inputFasta).replace(".fasta","")
  ret = runSpineX(inputFasta)
  if ret:
    spinex_out_fname = os.path.basename(inputFasta)+'.spXout'
    spinex_out_f = open(spinex_out_fname, 'r')
    spinex_out = spinex_out_f.readlines()[1:]
    spinex_out_f.close()
    ss_fname = fname + '.spinex.ss2'
    extractSSPred(spinex_out, ss_fname)
  return ret

def runSpineXForASA(inputFasta):
  fname = os.path.basename(inputFasta).replace(".fasta","")
  ret = runSpineX(inputFasta)
  if ret:
    spinex_out_fname = os.path.basename(inputFasta)+'.spXout'
    spinex_out_f = open(spinex_out_fname, 'r')
    spinex_out = spinex_out_f.readlines()[1:]
    spinex_out_f.close()
    asa_fname = fname + '.spinex.asa'
    extractASAPred(spinex_out, asa_fname)
  return ret


  
if __name__ == "__main__" :

  inMAT = getArgument(1,"INMAT")
  if len(inMAT) == 0 :
    print "No input .mat file given!"
  else :
#    runSpineXForASA
    runSpineX(inMAT)
#    runSpineXForSS(inputFasta)
#    runSpineXForSSAndASA(inFasta)
