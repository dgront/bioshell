#!/usr/bin/env python

import os, sys, time, datetime

def getDate():
  return time.strftime("%a, %d %b %Y %H:%M:%S %Z")


def formatTime(seconds):
  return str(datetime.timedelta(seconds = seconds))


"""def runCommand(command, output_filename):
  if os.path.exists(output_filename) and os.path.getsize(output_filename)>0: #nothing to do
    return True
  os.system(command)
  if os.path.exists(output_filename) and os.path.getsize(output_filename)>0: #correctly done command
    return True
  else:
    return False #incorrectly done command
"""

def runCommand(command, *output_filenames):
  """New version of function runCommand.
More than one output file can be checked.
Fully compatible with the previous version.
Output files can be specified as a list or separate arguments."""
  if isinstance(output_filenames[0], list):
        output_filenames = output_filenames[0]
  for output_filename in output_filenames:
    if not (os.path.exists(output_filename) and os.path.getsize(output_filename)>0):
      break
  else: #nothing to do
    return True
  os.system(command)
  for output_filename in output_filenames:
    if not (os.path.exists(output_filename) and os.path.getsize(output_filename)>0):
      return False #incorrectly done command
  else:
    return True #correctly done command


def getArgument(argIndex,argName) :

  if len(sys.argv) >= argIndex + 1 :
    return sys.argv[argIndex]
  elif os.environ.has_key(argName) :
    return os.environ[argName]
  else :
    return ""
    
