#!/usr/bin/env python

import sys, os, subprocess, platform
from glob import glob

def execute(message, command_line, return_='status', until_successes=False, terminate_on_failure=True, silent=False):
      print(message);  print(command_line)
      while True:

          p = subprocess.Popen(command_line, bufsize=0, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
          output, errors = p.communicate()

          output = output + errors

          if sys.version_info[0] == 2: output = output.decode('utf-8', errors='replace').encode('utf-8', 'replace') # Python-2
          else: output = output.decode('utf-8', errors='replace')  # Python-3

          exit_code = p.returncode

          if exit_code  or  not silent: print(output)

          if exit_code and until_successes: pass  # Thats right - redability COUNT!
          else: break

          print( "Error while executing {}: {}\n".format(message, output) )
          print("Sleeping 60s... then I will retry...")
          time.sleep(60)

      if return_ == 'tuple': return(exit_code, output)

      if exit_code and terminate_on_failure:
          print("\nEncounter error while executing: " + command_line)
          if return_==True: return True
          else: print("\nEncounter error while executing: " + command_line + '\n' + output); sys.exit(1)

      if return_ == 'output': return output
      else: return False

