#!/usr/bin/env python

import sys, os, subprocess, platform, re
from glob import glob
from utils import execute

BIOSHELL_DIR = "./"
BUILD_DIR = "./build"

if sys.platform.startswith("linux"): # Ubuntu settings
  CMAKE_C_COMPILER   = "clang-6.0"
  CMAKE_CXX_COMPILER = "clang++-6.0" 
elif sys.platform == "darwin": #MacOS settings
  CMAKE_C_COMPILER   = "clang"
  CMAKE_CXX_COMPILER = "clang++"

# Generate API documentation
def make_docs():
  execute("","cd %s" % (BIOSHELL_DIR))
  execute("","(cat doc_examples/Doxyfile ;echo OUTPUT_DIRECTORY=%s/doc_examples/ )|doxygen -" %(BIOSHELL_DIR))

# Compile - clang; param1 is the build type
def cmake_and_make(version):

  if not os.path.exists(BUILD_DIR): os.makedirs(BUILD_DIR)
  os.chdir(BUILD_DIR)
  execute("","cmake .. -DCMAKE_CXX_COMPILER=%s -DCMAKE_C_COMPILER=%s -DCMAKE_BUILD_TYPE=%s" % (CMAKE_CXX_COMPILER, CMAKE_C_COMPILER,version))
  execute("","make -j 12")
  os.chdir("../")

# Runs cc-examples and py-examples (runs call_all_tests.py script)
def run_tests():
  os.chdir("%s/doc_examples/cc-examples" %(BIOSHELL_DIR))
  execute("","python3 call_all_tests.py")
  os.chdir("../py-examples")
  execute("","python3 call_all_tests.py")
  os.chdir("../../")

# Runs every needed funcion after pulling from git
def auto_run():
  print("Running auto_run")
  execute("","git pull origin master")
  make_docs()
  cmake_and_make("Release")
  pybioshell=BuildPython37()
  pybioshell.build(8)
  run_tests()

if __name__ == "__main__" :
  execute("","date >log")
  status=execute("","git pull",'output')
  print(status)
  if not re.search("up-to-date",status):
    auto_run()
  execute("","date >>log")
  
