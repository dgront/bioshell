### How to get BioShell on your machine? ###

   **Note**, that because `bioshell` is written in C++11, you will need CMake (https://cmake.org) and relatively modern C++ compiler such as gcc 5.0 or clang 10.0.

1. **Install `zlib` library in developer version**
    On Ubuntu linux it can be installed by the command:
        sudo apt-get install zlib1g-dev

2. **Clone the repository to make a local copy** if you haven not done that yet:

        git clone git@bitbucket.org:dgront/bioshell.git bioshell
        cd bioshell

    then checkout the branch you need, e.g. `master`:

        git checkout master

    If you already have the local repository set up, just make sure you have the newest version on the code:

        git pull origin master

    Now Bioshell package contains submodules to use machine learning. Update neccecary submodules with this command:

        git submodule update --init

    Submodules will be downloaded to ``external/`` directory in bioshell repository.


3. **Compile the code**
        
        mkdir -p build
        cd build/
        cmake ..
        make -j 4
        
     Parameter `-j 4` allows `make` use four cores to compile the package.
     This command will attempt to compile all targets; the list of all targets can be printed by `make help`.

     Application(s) will appear in `bin` directory

  
4. **Make a custom build:**
 
    If you need a custom build visit https://bioshell.readthedocs.io/en/latest/doc/installation.html for futher options and instructions.

### How to get PyBioShell on your machine? ###

The easiest option is to download precompiled library, available for python3.5, python3.6, python3.7 and python2.7

        curl -O http://bioshell.pl/downloads/bioshell/Python37/pybioshell.so
    
You can place it in the `bin` directory and add PYTHONPATH to your .bashrc on Linux

        export PYTHONPATH="$PYTHONPATH:$HOME/bioshell/bin"
        
or .bash_profile on MacOS

        export PYTHONPATH="$PYTHONPATH:$HOME/bioshell/bin"
        
otherwise you should remember to add the path with pybioshell.so to all PyBioShell script eg.
        
        sys.path.append('~/bioshell/bin/')

Another way is to compile it from sources and to do that visit https://bioshell.readthedocs.io/en/latest/doc/pybioshell-installation.html#prequisities

Remember to also add path to bioshell data directory to your .bashrc or .bash_profile (you can find it at http://bioshell.pl/downloads/bioshell/data.tar.gz)

        export BIOSHELL_DATA_DIR="$HOME/bioshell/data"
        
or to your python script:
        
        sys.path.append('~/bioshell/data/')

### What is the structure of directories and files of bioshell? ###

- `bin` - contains exacutables for all compiled aplications
- `config` - utilities used by BioShell developers (various scripts and configuration files)
- `data` - bioshell database directory which provides files necessary to run BioShell programs. E.g. substitution matrices, monomer definitions, and many other.
- `doc_examples` - contains C++ and python3 examaples of bioshell usage. Each example (ex_) and application (ap_) has separate directory with minimal input files so you can easily modify existing runme.sh to your needs and check if it works. 
- `src` - the directory where the BioShell source code is actually stored

In order to  compile bioshell, you have to create `build` directory.  PyBioShell compilation requires `build_bindings` and `bindings` directories.

### How to check if my (Py)BioShell is working? ###

To do this you can run python3 `call_all_test.py` scripts from cc_examples and py_examples directories, which are present in doc_examples catalog. As a result, you'll get a `html` file with a table that summarizes all ex_ and ap_ runs.
