#ifndef UTILS_EXCEPTIONS_NoSuchFile_H
#define UTILS_EXCEPTIONS_NoSuchFile_H

#include <stdexcept>
#include <string>

namespace utils {
namespace exceptions {

/** @brief Exception thrown when a file cannot be found
 *
 */
class NoSuchFile : public std::runtime_error {
public:
  const std::string missing_file_name; ///< The name of the missing file
  /** @brief Creates an exception
   *
   * @param missing_file_name - name of the missing file
   */
  NoSuchFile(const std::string & missing_file_name) :
      std::runtime_error("Can't locate the file: " + missing_file_name), missing_file_name(missing_file_name) {}
};

}
}

#endif
