#ifndef UTILS_unit_tests_HH
#define UTILS_unit_tests_HH

#include <string>
#include <iostream>
#include <sstream>
#include <cmath>
#include <ctime>
#include <memory>
#include <functional>
#include <vector>

#include <core/data/basic/Vec3.hh>
#include <core/data/basic/Vec3I.hh>

namespace utils {

typedef std::function<bool(void)> TestFunction;

class UnitTests {
public:

  static UnitTests &instance() {

    static UnitTests INSTANCE;
    return INSTANCE;
  }

  static size_t add_test(const std::string &test_name, TestFunction f);

  const std::vector<std::pair<std::string, TestFunction> > &tests() { return tests_; }

  static void suite_title(const std::string & title) { instance().title_ = title; }

  static const std::string & suite_title() { return instance().title_; }

  static bool assert_true(const bool expression, const std::string &error_msg);

  static bool assert_equals(const double expected, const double actual, const double tolerance, const std::string & msg = "");

  static bool assert_equals(const core::data::basic::Vec3 & expected, const core::data::basic::Vec3 actual, const double tolerance);

  static bool assert_equals(const core::data::basic::Vec3I & expected, const core::data::basic::Vec3I actual);

  template<typename T>
  static bool assert_equals(const T expected, const T actual, const std::string & msg = "");

  template<typename T>
  static bool assert_equals(const std::vector<T> & expected, const std::vector<T> & actual, const std::string & msg = "");

  bool run(bool turn_off_logs = true);

  bool run_selected(const std::string & which_test, bool turn_off_logs = true);

  bool run_selected(const int which_test, bool turn_off_logs = true);

  void show_suite_title();

private:
  bool timer_on = false;
  bool timer_off = false;
  const int screen_width;
  int n_failed;
  std::string title_;
  std::stringstream last_msg;
  std::clock_t b_time = std::clock();
  std::clock_t e_time = std::clock();

  std::vector<std::pair<std::string, TestFunction> > tests_;

  UnitTests() : screen_width(78), n_failed(0) {}


  bool assert_true_(const bool expression, const std::string &error_msg);

  bool assert_equals_(const double expected, const double actual, const double tolerance,
          const std::string &msg_prefix = "The real values differs more than the allowed threshold: ");

  template<typename T>
  bool assert_equals_(const T expected, const T actual, const std::string & msg) {

    if (expected != actual) {
      last_msg << "The  values are different: expected " << expected << " but  got " << actual << "\n";
      if (msg.size() > 0) last_msg << msg << "\n";
      return false;
    }
    return true;
  }

  void start_test(const std::string &test_name);

  void end_test(bool result);

  void timer_start() {
    timer_on = true;
    timer_off = false;
    b_time = std::clock();
  }

  void timer_stop() {
    timer_on = false;
    timer_off = true;
    e_time = std::clock();
  }
};


struct SuiteTestName {
  SuiteTestName(const std::string & name) { UnitTests::suite_title(name); }
};

class DefineTest {
public:

  DefineTest(const std::string &test_name, TestFunction f) {
    UnitTests::add_test(test_name, f);
  }

  ~DefineTest() {}
};

template<typename T>
bool UnitTests::assert_equals(const T expected, const T actual, const std::string & msg) {

  if (expected != actual) {
    instance().last_msg << "The  values are different: expected " << expected << " but  got " << actual << "\n";
    if (msg.size() > 0) instance().last_msg << msg << "\n";
    return false;
  }
  return true;
}

template<typename T>
bool UnitTests::assert_equals(const std::vector<T> & expected, const std::vector<T> & actual, const std::string & msg) {

  if (expected.size() != actual.size()) {
    instance().last_msg << "The  size of the expected vector " << expected.size()
                                   << " differs from actual " << actual.size() << "\n";
    if (msg.size() > 0) instance().last_msg << msg << "\n";
    return false;
  }
  return true;
}

}

#define RUN_TESTS int main() { utils::UnitTests::instance().run(); }
#define RUN_TESTS_VERBOSE int main() { utils::UnitTests::instance().run(false); }
#define RUN_ONE_TEST(NAME_OR_INDEX) int main() { utils::UnitTests::instance().run_selected(NAME_OR_INDEX); }


#endif

