#ifndef UTILS_ThreadPool_H
#define UTILS_ThreadPool_H

#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>
#include <chrono>

#include <core/index.hh>
#include <utils/Logger.hh>

namespace utils {

/** \brief A simple pool of threads for queuing tasks
 */
class ThreadPool {
public:

  /** \brief Starts a pool of <code>n_threads</code> threads
   *
   * @param n_threads - the number of threads to be run simultaneously
   */
  ThreadPool(const core::index2 n_threads);

  /** \brief Append a job to this queue
   *
   * @param f - o method to be executed
   * @param args - method's arguments
   */
  template<class F, class ... Args>
  auto enqueue(F &&f, Args &&... args)
  -> std::future<typename std::result_of<F(Args...)>::type>;

  /** \brief Destructor closes the queue and joins all threads.
   */
  ~ThreadPool();

  /** @brief Write progress info to the logger every k jobs finished
   * @param k - how often timing statitics  should be reported
   */
  void report_every_k(core::index4 k) { k_ = k; }

private:
  // need to keep track of threads so we can join them
  std::vector<std::thread> workers;
  // the task queue
  std::queue<std::function<void()> > tasks;

  // synchronization
  std::mutex queue_mutex;
  std::condition_variable condition;
  bool stop;
  // reporting
  core::index4 k_;
  std::chrono::time_point<std::chrono::high_resolution_clock>  start = std::chrono::high_resolution_clock::now(); // --- timer starts!
  utils::Logger logs;
  core::index4 n_jobs_done;
};

inline ThreadPool::ThreadPool(const core::index2 n_threads) : stop(false), logs("ThreadPool") {

  n_jobs_done = 0;
  k_ = 1000;
  start = std::chrono::high_resolution_clock::now(); // --- timer starts!

  logs << utils::LogLevel::INFO << "pool of " << n_threads << " started\n";
  for (size_t i = 0; i < n_threads; ++i)
    workers.emplace_back([this] {
      for (;;) {
        std::function<void()> task;
        {
          std::unique_lock<std::mutex> lock(this->queue_mutex);
          this->condition.wait(lock, [this] { return this->stop || !this->tasks.empty(); });
          if (this->stop && this->tasks.empty()) return;
          task = std::move(this->tasks.front());
          this->tasks.pop();
          ++n_jobs_done;
          if (n_jobs_done% k_ == 0) {
            auto end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
            logs << utils::LogLevel::INFO << "Jobs done: " << size_t(n_jobs_done) << " after " << time_span.count() << " [s]\n";
          }
        }
        task();
      }
    });
}

template<class F, class... Args>
auto ThreadPool::enqueue(F &&f, Args &&... args)
-> std::future<typename std::result_of<F(Args...)>::type> {
  using return_type = typename std::result_of<F(Args...)>::type;

  auto task = std::make_shared<std::packaged_task<return_type()> >(
    std::bind(std::forward<F>(f), std::forward<Args>(args)...)
  );

  std::future<return_type> res = task->get_future();
  {
    std::unique_lock<std::mutex> lock(queue_mutex);
    if (stop) throw std::runtime_error("The queue has been already closed!");
    tasks.emplace([task]() { (*task)(); });
  }
  condition.notify_one();
  return res;
}

inline ThreadPool::~ThreadPool() {
  {
    std::unique_lock<std::mutex> lock(queue_mutex);
    stop = true;
  }
  condition.notify_all();
  for (std::thread &worker : workers) worker.join();
}

}
#endif
