#include <utils/Logger.hh>
#include <fstream>

utils::Logger fileLogger = utils::Logger("FILES");

bool isDir(std::string dir) {
    struct stat fileInfo;
    stat(dir.c_str(), &fileInfo);
    return (S_ISDIR(fileInfo.st_mode));
}

void listFiles(std::string dir, std::shared_ptr<std::set<std::string>> files, bool recursive = true) {
    DIR *dp; //create the directory object
    struct dirent *entry; //create the entry structure
    dp = opendir(dir.c_str()); //open directory by converting the string to const char*
    if (dir.at(dir.length() - 1) != '/') {
        dir = dir + "/";
    }
    if (dp != NULL) { //if the directory isn't empty
        while ((entry = readdir(dp))) { //while there is something in the directory
            std::string file_name = entry->d_name;
            int file_name_len = file_name.length();
            if (strcmp(entry->d_name, ".") != 0 &&
                strcmp(entry->d_name, "..") != 0) { //and if the entry isn't "." or ".."
                if (isDir(dir + entry->d_name) &&
                    recursive) {
                    // check if the new path is a directory,
                    // and if it is (and recursion is specified as true), recurse.
                    listFiles(dir + entry->d_name, files, true); //recurse
                }
                else if (file_name_len > 7) {
                    if (file_name.substr(file_name_len - 4, 4) == ".pdb" ||
                        file_name.substr(file_name_len - 4, 4) == ".ent" ||
                        file_name.substr(file_name_len - 7, 7) == ".pdb.gz" ||
                        file_name.substr(file_name_len - 7, 7) == ".ent.gz") {
                        file_name = dir + file_name;
                        files->insert(file_name);//add the entry to the list of files
                    }
                }
                else if (file_name_len > 4) {
                    if (file_name.substr(file_name_len - 4, 4) == ".pdb" ||
                        file_name.substr(file_name_len - 4, 4) == ".ent") {
                        file_name = dir + file_name;
                        files->insert(file_name);//add the entry to the list of files
                    }
                }
            }
        }
        (void) closedir(dp); //close directory
    }
    else {
        fileLogger << utils::LogLevel::FILE << "Couldn't open the directory \n";
    }
}

void subsetFiles(std::string file_list, std::shared_ptr<std::set<std::string>> files) {
    std::ifstream files_file;
    files_file.open(file_list);
    std::string line;
    while (files_file.good()) {
        getline(files_file, line);
        files->insert(line);
    }
}
