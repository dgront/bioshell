#ifndef UTILS_Logger_HH
#define UTILS_Logger_HH

#include <string>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <map>
#include <mutex>
#include <thread>

#include <utils/string_utils.hh>

namespace utils {

/** @brief Define logging levels and assign integers to them
 * There are nine levels of importance for log messages reported by BioShell methods.
 * Eight of them are used for general reporting, in the order of decreasing importance:
 * OFF, CRITICAL, SEVERE, WARNING, INFO, FINE, FINER and FINEST. The  FILE level
 * is used to report information about disk I/O.
 */
enum class LogLevel {
  OFF = 0,
  CRITICAL = 1,
  SEVERE = 2,
  WARNING = 3,
  FILE = 4,
  INFO = 5,
  FINE = 6,
  FINER = 7,
  FINEST = 8
};

/** @brief Return the name of a given log level
 *
 * @param level - the level enum
 * @return name of this log level
 */
const std::string &log_level_name(const LogLevel &level);

/** @brief Return the name of a given log level as a string colored by terminal controlling characters
 *
 * @param level - the level enum
 * @return name of this log level
 */
const std::string &log_level_name_colored(const LogLevel &level);

/** @brief Return the log level corresponding to a given name
 *
 * @param level - name of a level
 * @return a log level
 */
const LogLevel &log_level_by_name(const std::string &level);

/** @brief Logger is a named channel where a single class sends its error messages and warnings
 *
 */
class Logger {
private:
  static std::mutex mtx;
  static std::thread::id blocking_thread;

public:

  /** @brief Creates a new logger
   *
   * @param module_name - name assigned to this logger; typically the name of a class who owns it
   */
  Logger(const std::string &module_name) : module_name_(module_name), recent_level(LogLevel::INFO) {}

  /// Returns the name of this logger
  inline const std::string &module_name() const { return module_name_; }

  /// Virtual bare destructor
  virtual ~Logger() {}

  /** @brief Returns true if messages with a given priority level will be actually sent to the output stream
   *
   * @param level - logging (priority) level
   * @return true if this logger will log messages of the given priority
   */
  bool is_logable(LogLevel level) const;

  friend Logger &operator<<(Logger &logger, const LogLevel level);

  friend Logger &operator<<(Logger &logger, const char *message);

  friend Logger &operator<<(Logger &logger, const std::string &message);

  friend Logger &operator<<(Logger &logger, const double value);

  friend Logger &operator<<(Logger &logger, const int number);

  friend Logger &operator<<(Logger &logger, const size_t number);

  friend Logger &operator<<(Logger &logger, const unsigned long long number);

  friend Logger &operator<<(Logger &logger, const unsigned number);

  friend Logger &operator<<(Logger &logger, const core::index2 number);

  friend Logger &operator<<(Logger &logger, const core::index1 number);

  friend Logger &operator<<(Logger &logger, const float value);

  friend Logger &operator<<(Logger &logger, const char c);

  template<class ...Args>
  void log(const Args &...args) {

    std::lock_guard<std::mutex> lock(mtx);
    blocking_thread = std::this_thread::get_id();
    log_one(args...);
    sink.flush();
  }

private:
  const std::string module_name_;
  std::ostream &sink = std::cerr;
  LogLevel recent_level;

  template<class A0, class ...Args>
  void log_one(const A0 &a0, const Args &...args) {
    *this << a0;
    log_one(args...);
  }

  inline void log_one() {}
};

}
#endif /* UTILS_LOGGER_HH */
