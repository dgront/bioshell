#include <string>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <mutex>

#include <utils/Logger.hh>
#include <utils/LogManager.hh>
#include <utils/string_utils.hh>

namespace utils {

std::mutex Logger::mtx;
std::thread::id Logger::blocking_thread;

std::map<LogLevel, std::string> create_log_level_names_map() {

  std::map<LogLevel, std::string> tmp;
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::CRITICAL, "[CRITICAL] "));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::SEVERE, "  [SEVERE] "));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::WARNING, " [WARNING] "));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::INFO, "    [INFO] "));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::FINE, "    [FINE] "));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::FINER, "   [FINER] "));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::FINEST, "  [FINEST] "));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::FILE, "    [FILE] "));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::OFF,  "     [OFF] "));

  return tmp;
}

std::map<LogLevel, std::string> create_log_level_colored_names_map() {

  std::map<LogLevel, std::string> tmp;
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::CRITICAL,
    std::string(TEXT_RED) + std::string("[CRITICAL] ") + std::string(TEXT_RESET)));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::SEVERE,
    std::string(TEXT_RED) + std::string("  [SEVERE] ") + std::string(TEXT_RESET)));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::WARNING,
    std::string(TEXT_YELLOW) + std::string(" [WARNING] ") + std::string(TEXT_RESET)));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::INFO,
    std::string(TEXT_YELLOW) + std::string("    [INFO] ") + std::string(TEXT_RESET)));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::OFF,
    std::string(TEXT_WHITE) + std::string("    [HTTP] ") + std::string(TEXT_RESET)));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::FINE,
    std::string(TEXT_WHITE) + std::string("    [FINE] ") + std::string(TEXT_RESET)));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::FINER,
    std::string(TEXT_WHITE) + std::string("   [FINER] ") + std::string(TEXT_RESET)));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::FINEST,
    std::string(TEXT_WHITE) + std::string("  [FINEST] ") + std::string(TEXT_RESET)));
  tmp.insert(std::pair<LogLevel, std::string>(LogLevel::FILE,
    std::string(TEXT_WHITE) + std::string("    [FILE] ") + std::string(TEXT_RESET)));

  return tmp;
}


std::map<std::string, LogLevel> create_names_log_level_map() {

  std::map<std::string, LogLevel> tmp;
  tmp.insert(std::pair<std::string, LogLevel>("CRITICAL", LogLevel::CRITICAL));
  tmp.insert(std::pair<std::string, LogLevel>("SEVERE", LogLevel::SEVERE));
  tmp.insert(std::pair<std::string, LogLevel>("WARNING", LogLevel::WARNING));
  tmp.insert(std::pair<std::string, LogLevel>("INFO", LogLevel::INFO));
  tmp.insert(std::pair<std::string, LogLevel>("OFF", LogLevel::OFF));
  tmp.insert(std::pair<std::string, LogLevel>("FINE", LogLevel::FINE));
  tmp.insert(std::pair<std::string, LogLevel>("FINER", LogLevel::FINER));
  tmp.insert(std::pair<std::string, LogLevel>("FINEST", LogLevel::FINEST));
  tmp.insert(std::pair<std::string, LogLevel>("FILE", LogLevel::FILE));

  return tmp;
}

std::map<LogLevel, std::string> log_level_names = create_log_level_names_map();
std::map<LogLevel, std::string> log_level_names_colored = create_log_level_colored_names_map();
std::map<std::string, LogLevel> log_levels_for_name = create_names_log_level_map();

const std::string &log_level_name(const LogLevel &level) {

  return log_level_names[level];
}

const std::string &log_level_name_colored(const LogLevel &level) {

  return log_level_names_colored[level];
}

const LogLevel &log_level_by_name(const std::string &level) {
  return log_levels_for_name[level];
}


bool Logger::is_logable(LogLevel level) const { return (level <= LogManager::log_level); }

Logger &operator<<(Logger &logger, const char c) {

  if (logger.recent_level <= LogManager::log_level)
    if (!LogManager::is_muted(logger.module_name_)) logger.sink << c;

  return logger;
}

Logger &operator<<(Logger &logger, const float value) {

  if (logger.recent_level <= LogManager::log_level)
    if (!LogManager::is_muted(logger.module_name_)) logger.sink << value;

  return logger;
}

Logger &operator<<(Logger &logger, const char *message) {

  if (logger.recent_level <= LogManager::log_level) {
    if (!LogManager::is_muted(logger.module_name_)) logger.sink << message;
  }

  return logger;
}

Logger &operator<<(Logger &logger, const size_t number) {

  if (logger.recent_level <= LogManager::log_level) {
    if (!LogManager::is_muted(logger.module_name_)) logger.sink << number;
  }

  return logger;
}

#ifndef _WIN64
Logger &operator<<(Logger &logger, const unsigned long long number) {

  if (logger.recent_level <= LogManager::log_level) {
    if (!LogManager::is_muted(logger.module_name_)) logger.sink << number;
  }

  return logger;
}
#endif

Logger &operator<<(Logger &logger, const core::index1 number) {

  if (logger.recent_level <= LogManager::log_level) {
    if (!LogManager::is_muted(logger.module_name_)) logger.sink << number;
  }

  return logger;
}

Logger &operator<<(Logger &logger, const core::index2 number) {

  if (logger.recent_level <= LogManager::log_level) {
    if (!LogManager::is_muted(logger.module_name_)) logger.sink << number;
  }

  return logger;
}


Logger &operator<<(Logger &logger, const unsigned number) {

  if (logger.recent_level <= LogManager::log_level) {
    if (!LogManager::is_muted(logger.module_name_)) logger.sink << number;
  }

  return logger;
}

Logger &operator<<(Logger &logger, const int number) {

  if (logger.recent_level <= LogManager::log_level) {
    if (!LogManager::is_muted(logger.module_name_)) logger.sink << number;
  }

  return logger;
}

Logger &operator<<(Logger &logger, const double value) {

  if (logger.recent_level <= LogManager::log_level) {
    if (!LogManager::is_muted(logger.module_name_)) logger.sink << value;
  }

  return logger;
}

Logger &operator<<(Logger &logger, const std::string &message) {

  if (logger.recent_level <= LogManager::log_level) {
    if (logger.blocking_thread != std::this_thread::get_id()) {
      std::lock_guard<std::mutex> lock(Logger::mtx);
      if (!LogManager::is_muted(logger.module_name_)) logger.sink << message;
    } else if (!LogManager::is_muted(logger.module_name_)) logger.sink << message;
  }

  return logger;
}

Logger &operator<<(Logger &logger, const LogLevel level) {

  if (LogManager::is_muted(logger.module_name_)) return logger;

  if (logger.blocking_thread != std::this_thread::get_id()) {
    std::lock_guard<std::mutex> lock(Logger::mtx);

    logger.recent_level = level;
    if (logger.recent_level <= LogManager::log_level) {
      if (LogManager::use_colors())
        logger.sink << log_level_names_colored.at(level) << TEXT_BOLD << logger.module_name_ << TEXT_RESET << " ";
      else
        logger.sink << log_level_names.at(level) << logger.module_name_ << " ";
    }
  } else {
    logger.recent_level = level;
    if (logger.recent_level <= LogManager::log_level) {
      if (LogManager::use_colors())
        logger.sink << log_level_names_colored.at(level) << TEXT_BOLD << logger.module_name_ << TEXT_RESET << " ";
      else
        logger.sink << log_level_names.at(level) << logger.module_name_ << " ";
    }
  }
  return logger;
}

}