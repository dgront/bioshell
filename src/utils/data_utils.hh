/** @file data_utils.hh
 * @brief Simple utilities on primitive variables
 *
 */
#ifndef CORE_DATA_SEQUENCE_data_utils_H
#define CORE_DATA_SEQUENCE_data_utils_H

namespace utils {

/// Simple function that reverses bytes in a double variable
double swap_double(const double d);

}

#endif
