#ifndef UTILS_Profiler_HH
#define UTILS_Profiler_HH

#include <iostream>
#include <ctime>
#include <string>
#include <memory>

#include <utils/string_utils.hh>

namespace utils {

/// Simple class used to profile BioSHell applications
class Profiler {
public:
  /** @brief Start a profiler
   *
   * @param tag - string used to identify this profiler; it will be shown in the output table
   */
  Profiler(const std::string &tag) : tag(tag) {}

  const std::string tag; ///< The string ID for this profiler

  /// Start profiler's time counting
  inline void start() {
    recent_start = std::clock();
    n_calls++;
  }

  /// Stop profiler's time counting
  inline void stop() { total += (double(std::clock() - recent_start)) / CLOCKS_PER_SEC; }

  /// Print the  time recorded by this profiler
  inline std::string show() { return utils::string_format("%60s %10.3f %8d", tag.c_str(), total, n_calls); }

private:
  clock_t recent_start = -1;
  double total = 0;
  size_t n_calls = 0;
};

}

#endif
