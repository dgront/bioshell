#ifndef UTILS_exit_H
#define UTILS_exit_H

#include <string>

#define EXIT(m) utils::exit_with_error( __FILE__, __LINE__, m )

#define runtime_assert(_Expression, msg) \
        if ( !(_Expression) ) { \
                utils::exit_with_error(__FILE__, __LINE__, msg ); \
        }

namespace utils {

/** @brief Prints an error message on stderr end exits the program with an error
 * @param fname - name of the source file that raised the error
 * @param line - line number that raised the error
 * @param message - error message
 */
void exit_with_error(const std::string &fname, const int line, const std::string &message);

/** @brief  Prints an error message on stderr end exits the program with OK status
 * @param message - to be printed on the screen
 */
void exit_OK_with_message(const std::string &message);

}

#endif
