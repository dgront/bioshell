#include <vector>
#include <memory>

#include <utils/ProfilingManager.hh>
#include <utils/Profiler.hh>

namespace utils {


void ProfilingManager::show(std::ostream & outstream) {

	for (const auto & p : profilers_map) {
		outstream << p.second->show() << "\n";
	}
}

}
