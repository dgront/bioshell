/** @file base64_encoding.hh
 * @brief Provides Base64 encoding / decoding utilities
 *
 * Based on https://stackoverflow.com/questions/180947/base64-decode-snippet-in-c
 */
#ifndef UTILS_base64_encoding_H
#define UTILS_base64_encoding_H

#include <string>
#include <vector>

namespace utils {

/** @brief Encodes binary data buffer into a printable string
 * @param buf - input data
 * @return a printable string that encodes the given data
 */
std::string base64_encode(const std::vector<unsigned char> &buf);

/** @brief Encodes binary data buffer into a printable string
 * @param buf - input data
 * @return a printable string that encodes the given data
 */
std::string base64_encode(const unsigned char *buf, unsigned int buf_len);

/** @brief Decodes a string back into a binary data buffer
 * @param buf - encoded string
 * @return a buffer with binary data
 */
std::vector <unsigned char> base64_decode(std::string & encoded_string);

}

#endif
