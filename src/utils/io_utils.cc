#ifdef _WIN64
#include <windows.h>
#include <strsafe.h>
#include <fileapi.h>
#pragma comment(lib, "User32.lib")
#else
#include <glob.h>
#endif
#include <zlib.h>
#include <cstring>
#include <cstdio>
#include <ctime>
#include <sys/stat.h>

#include <memory>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

#include <utils/exit.hh>
#include <utils/io_utils.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>
#include <ui/www/web_client.hh>

namespace utils {

utils::Logger io_utils_logger("io_utils");

bool if_file_exists(const std::string& fname) {

  std::ifstream ifile(fname.c_str());
  bool ret = (bool) ifile;
  if (ret) ifile.close();
  return ret;
}

std::string basename(const std::string& str) {

  return str.substr(str.find_last_of("/\\") + 1);
}

std::string join_paths(const std::string& p1, const std::string& p2) {

  std::string tmp = p1;
  if (p1.back() != dir_separator) {
    tmp += dir_separator;
    return (tmp + p2);
  } else return (p1 + p2);
}
#ifndef _WIN64
std::vector<std::string> glob(const std::string & mask) {

  io_utils_logger << utils::LogLevel::FILE << "listing files: " << mask << "\n";

  std::vector<std::string> out;
  glob_t globbuf;
  int err = ::glob(mask.c_str(), 0, NULL, &globbuf);
  if (err == 0) {
    io_utils_logger << globbuf.gl_pathc << " files found\n";
    for (size_t i = 0; i < globbuf.gl_pathc; i++) {
      out.push_back(globbuf.gl_pathv[i]);
    }

    globfree(&globbuf);
  }
  return out;
}
#else
    std::vector<std::string> glob(const std::string & mask)
    {

        WIN32_FIND_DATA ffd;
        char szDir[MAX_PATH];
        HANDLE hFind = INVALID_HANDLE_VALUE;
        std::vector<std::string> out;
        // Prepare string for use with FindFile functions.  First, copy the
        // string to a buffer, then append '\*' to the directory name.

        StringCchCopy(szDir, MAX_PATH, mask.c_str());
        StringCchCat(szDir, MAX_PATH, TEXT("\\*"));
// Find the first file in the directory.

        hFind = FindFirstFile(szDir, &ffd);


        // List all the files in the directory with some info about them.

        do
        {
            out.push_back(mask + ffd.cFileName);
        }
        while (FindNextFile(hFind, &ffd) != 0);

        FindClose(hFind);
        return out;
    }
#endif

std::vector<std::string> read_listfile(const std::string & fname) {

  std::vector<std::string> out;
  read_listfile(fname, out);

  return out;
}

void read_listfile(const std::string & fname, std::vector<std::string> & destination) {

  io_utils_logger.log(utils::LogLevel::FILE, "Reading a list-file named: ", fname, "\n");
  std::ifstream in;
  in_stream(fname, in);
  size_t cnt = 0;
  std::string line, token;
  try {
    while (std::getline(in, line)) {
      if (line.length() < 1) continue;
      if (line[0] == '#') continue;
      std::stringstream ss(line);
      ss >> token;
      destination.push_back(token);
      cnt++;
    }
  } catch (std::ifstream::failure & e) {
    if ((!in.fail()) || (!in.eof()) || (in.bad()))
      io_utils_logger.log(utils::LogLevel::CRITICAL, "Exception captured: ", e.what(), "\n");
  }
  io_utils_logger.log(utils::LogLevel::FILE, "found ", cnt, " file names in ", fname, "\n");
}

std::shared_ptr<std::ostream> out_stream(const std::string &fname) {

  if (fname.length() > 0) {
    if ((fname.compare("stderr") == 0) || (fname.compare("cerr") == 0)) {
      std::shared_ptr<std::ostream> p(&std::cerr, [](std::ostream*){});
      return p;
    }
    if ((fname.compare("stdout") == 0) || (fname.compare("cout") == 0)) {
      std::shared_ptr<std::ostream> p(&std::cout, [](std::ostream*){});
      return p;
    }
    if ((fname.compare("null") == 0) || (fname.compare("/dev/null") == 0)) {
      std::shared_ptr<std::ostream> p(new std::ofstream(0));
    }
    std::shared_ptr<std::ostream> p(new std::ofstream(fname));
    io_utils_logger.log(utils::LogLevel::FILE, "opening a file ", fname, "\n");
    return p;
  } else {
    std::shared_ptr<std::ostream> p(new std::ostream(std::cout.rdbuf()));
    return p;
  }
}

void in_stream(const std::string &fname, std::ifstream & in_stream, std::ios_base::openmode mode) {

  io_utils_logger.log(utils::LogLevel::FILE, "Attempting to open a text file: ", fname, "\n");
  in_stream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  if (fname.length() == 0) {      // --- file name not empty
    io_utils_logger.log(utils::LogLevel::SEVERE, "File name is empty\n\n");
    throw std::runtime_error("The file name is empty!\n");
  }

  if (!if_file_exists(fname)) {   // --- file not found
    io_utils_logger.log(utils::LogLevel::SEVERE, "Can't find file: ", fname, " file not opened!\n\n");
    throw std::runtime_error("Can't find file: " + fname + ", file not opened!\n");
  }

  in_stream.open(fname, mode);

}

bool check_error_bits(const std::istream &f) {
  if (f.eof()) {
    return false;
  }
  if (f.fail()) {
    return false;
  }
  if (f.bad()) {
    return false;
  }
  return true;
}

std::string load_text_file(std::istream & ifs) {

  std::string out;
  load_text_file(ifs, out);

  return out;
}

std::string load_text_file(const std::string &fileName) {

  std::ifstream ifs;
  std::string out;
  in_stream(fileName, ifs);
  load_text_file(ifs, out);

  return out;
}

std::string & load_text_file(const std::string &fileName, std::string &sink) {

  std::ifstream ifs;
  in_stream(fileName, ifs);
  return load_text_file(ifs,sink);
}

std::string &load_text_file(std::istream &ifs, std::string &sink) {

  std::vector<std::string> v;
  load_text_lines(ifs, v);
  for (const std::string & l: v)
    sink += l + "\n";
  return sink;
}

core::index4 load_text_lines(std::istream &ifs, std::vector<std::string> &lines) {

  std::string line;
  core::index4 getlinecount = 0;

  while ( getline( ifs, line ) ) {
    if(!check_error_bits(ifs)) break;
    lines.push_back(line);
    ++getlinecount;
    if (ifs.peek() < 0) break;
  }
  return getlinecount;
}

std::map<std::string, std::vector<std::string>> & read_properties_file(const std::string& fname,
    std::map<std::string, std::vector<std::string>> & storage_map, const bool replace_underscores_with_spaces) {

  std::ifstream in;
  in_stream(fname, in);
  size_t cnt = 0;
  std::string line, token;
  try {
    while (std::getline(in, line)) {
      if (line.length() < 2) continue;
      if (line[0] == '#') continue;
      if (line[0] == '!') continue;
      std::vector<std::string> tokens = split(line, {':'});
      if (tokens.size() != 2) continue;
      std::vector<std::string> tokens2 = split(tokens[1]);
      if (replace_underscores_with_spaces) std::replace(tokens[0].begin(), tokens[0].end(), '_', ' ');
      if (replace_underscores_with_spaces)
        for (std::string &s : tokens2)
          std::replace(s.begin(), s.end(), '_', ' ');
      storage_map[tokens[0]] = tokens2;
      cnt++;
    }
  } catch (std::ifstream::failure &e) {
    if ((!in.fail()) || (!in.eof()) || (in.bad()))
      io_utils_logger.log(utils::LogLevel::CRITICAL, "Exception captured: ", e.what(), "\n");
  }
  io_utils_logger.log(utils::LogLevel::FILE, "found ", cnt, " tokens in ", fname, "\n");

  return storage_map;
}

std::map<std::string, std::vector<std::string>> read_properties_file(const std::string& fname,
    const bool replace_underscores_with_spaces) {

  std::map<std::string, std::vector<std::string>> output;
  return read_properties_file(fname, output, replace_underscores_with_spaces);
}

std::string & load_binary_file(const std::string & filename, std::string & buffer) {

  if (!if_file_exists(filename)) {
    io_utils_logger.log(utils::LogLevel::SEVERE, "Can't find file: ", filename, ", returning null-pointer!\n\n");
    throw std::runtime_error("Can't find file: " + filename + ", returning null-pointer!\n");
  }
  std::ifstream input(filename, std::ios::binary);
  std::copy(std::istreambuf_iterator<char>(input), (std::istreambuf_iterator<char>()),
      std::inserter(buffer, buffer.begin()));
  return buffer;
}

std::string & zip_string(const std::string& str, std::string& dest, int compressionlevel) {

  z_stream zs;                        // z_stream is zlib's control structure
  memset(&zs, 0, sizeof(zs));

  if (deflateInit(&zs, compressionlevel) != Z_OK) throw(std::runtime_error("deflateInit failed while compressing."));

  zs.next_in = (Bytef*) str.data();
  zs.avail_in = str.size();           // set the z_stream's input

  int ret;
  char outbuffer[32768];
  dest.clear();

  // retrieve the compressed bytes blockwise
  do {
    zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
    zs.avail_out = sizeof(outbuffer);

    ret = deflate(&zs, Z_FINISH);

    if (dest.size() < zs.total_out) {
      // append the block to the output string
      dest.append(outbuffer, zs.total_out - dest.size());
    }
  } while (ret == Z_OK);

  deflateEnd(&zs);

  if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
    std::ostringstream oss;
    oss << "Exception during zlib compression: (" << ret << ") " << zs.msg;
    throw(std::runtime_error(oss.str()));
  }

  return dest;
}

std::string & unzip_string(const std::string& str, std::string & dest) {

  z_stream zs;                        // z_stream is zlib's control structure
  memset(&zs, 0, sizeof(zs));

  if (inflateInit(&zs) != Z_OK) throw(std::runtime_error("inflateInit failed while decompressing."));

  zs.next_in = (Bytef*) str.data();
  zs.avail_in = str.size();

  int ret;
  char outbuffer[32768];
  dest.clear();

  // get the decompressed bytes blockwise using repeated calls to inflate
  do {
    zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
    zs.avail_out = sizeof(outbuffer);
    ret = inflate(&zs, 0);
    if (dest.size() < zs.total_out) dest.append(outbuffer, zs.total_out - dest.size());

  } while (ret == Z_OK);

  inflateEnd(&zs);

  if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
    std::ostringstream oss;
    oss << "Exception during zlib decompression: (" << ret << ") " << zs.msg;
    throw(std::runtime_error(oss.str()));
  }

  return dest;
}

std::string& ungzip_string(const std::string& compressedBytes, std::string& uncompressedBytes) {

  if (compressedBytes.size() == 0) return uncompressedBytes;

  uncompressedBytes.clear();

  unsigned full_length = compressedBytes.size();
  unsigned half_length = compressedBytes.size() / 2;

  unsigned uncompLength = full_length;
  char* uncomp = (char*) calloc(sizeof(char), uncompLength);

  z_stream strm;
  strm.next_in = (Bytef *) compressedBytes.c_str();
  strm.avail_in = compressedBytes.size();
  strm.total_out = 0;
  strm.zalloc = Z_NULL;
  strm.zfree = Z_NULL;

  bool done = false;
  int ret = 0;
  if ((ret = inflateInit2(&strm, (16+MAX_WBITS))) != Z_OK) {
    free(uncomp);
    std::ostringstream oss;
    oss << "Exception during zlib decompression: (" << ret << ") " << strm.msg;
    throw(std::runtime_error(oss.str()));
  }

  while (!done) {
    // If our output buffer is too small
    if (strm.total_out >= uncompLength) {
      // Increase size of output buffer
      char* uncomp2 = (char*) calloc(sizeof(char), uncompLength + half_length);
      memcpy(uncomp2, uncomp, uncompLength);
      uncompLength += half_length;
      free(uncomp);
      uncomp = uncomp2;
    }

    strm.next_out = (Bytef *) (uncomp + strm.total_out);
    strm.avail_out = uncompLength - strm.total_out;

    // Inflate another chunk.
    int err = inflate(&strm, Z_SYNC_FLUSH);
    if (err == Z_STREAM_END) done = true;
    else if (err != Z_OK) {
      std::ostringstream oss;
      oss << "Exception during zlib decompression: (" << err << ") " << strm.msg;
      throw(std::runtime_error(oss.str()));
    }
  }

  if ((ret = inflateEnd(&strm)) != Z_OK) {
    free(uncomp);
    std::ostringstream oss;
    oss << "Exception during zlib decompression: (" << ret << ") " << strm.msg;
    throw(std::runtime_error(oss.str()));
  }

  for (size_t i = 0; i < strm.total_out; ++i) {
    uncompressedBytes += uncomp[i];
  }
  free(uncomp);
  return uncompressedBytes;
}

std::stringstream & unzip_string(const std::string& str, std::stringstream & dest) {

  z_stream zs;                        // z_stream is zlib's control structure
  memset(&zs, 0, sizeof(zs));

  if (inflateInit(&zs) != Z_OK) throw(std::runtime_error("inflateInit failed while decompressing."));

  zs.next_in = (Bytef*) str.data();
  zs.avail_in = str.size();

  int ret;
  char outbuffer[32768];
  dest.clear();

  // get the decompressed bytes block-wise using repeated calls to inflate
  do {
    zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
    zs.avail_out = sizeof(outbuffer);

    ret = inflate(&zs, 0);
    dest << outbuffer;

  } while (ret == Z_OK);

  inflateEnd(&zs);

  if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
    std::ostringstream oss;
    oss << "Exception during zlib decompression: (" << ret << ") " << zs.msg;
    throw(std::runtime_error(oss.str()));
  }

  return dest;
}

std::stringstream & ungzip_string(const std::string& compressedBytes, std::stringstream & uncompressedBytes) {

  if (compressedBytes.size() == 0) return uncompressedBytes;

  uncompressedBytes.clear();

  unsigned full_length = compressedBytes.size();
  unsigned half_length = compressedBytes.size() / 2;

  unsigned uncompLength = full_length;
  char* uncomp = (char*) calloc(sizeof(char), uncompLength);

  z_stream strm;
  strm.next_in = (Bytef *) compressedBytes.c_str();
  strm.avail_in = compressedBytes.size();
  strm.total_out = 0;
  strm.zalloc = Z_NULL;
  strm.zfree = Z_NULL;

  bool done = false;
  int ret = 0;
  if ((ret = inflateInit2(&strm, (16+MAX_WBITS))) != Z_OK) {
    free(uncomp);
    std::ostringstream oss;
    oss << "Exception during zlib decompression: (" << ret << ") " << strm.msg;
    throw(std::runtime_error(oss.str()));
  }

  while (!done) {
    // If our output buffer is too small
    if (strm.total_out >= uncompLength) {
      // Increase size of output buffer
      char* uncomp2 = (char*) calloc(sizeof(char), uncompLength + half_length);
      memcpy(uncomp2, uncomp, uncompLength);
      uncompLength += half_length;
      free(uncomp);
      uncomp = uncomp2;
    }

    strm.next_out = (Bytef *) (uncomp + strm.total_out);
    strm.avail_out = uncompLength - strm.total_out;

    // Inflate another chunk.
    int err = inflate(&strm, Z_SYNC_FLUSH);
    if (err == Z_STREAM_END) done = true;
    else if (err != Z_OK) {
      std::ostringstream oss;
      oss << "Exception during zlib decompression: (" << err << ") " << strm.msg;
      throw(std::runtime_error(oss.str()));
    }
  }

  if ((ret = inflateEnd(&strm)) != Z_OK) {
    free(uncomp);
    std::ostringstream oss;
    oss << "Exception during zlib decompression: (" << ret << ") " << strm.msg;
    throw(std::runtime_error(oss.str()));
  }

//  for (size_t i = 0; i < strm.total_out; ++i) {
//    uncompressedBytes << uncomp[i];
//  }
  uncompressedBytes << uncomp;

  free(uncomp);
  return uncompressedBytes;
}

bool find_file(const std::string & fname, std::string & result, const std::string & path) {

  if (if_file_exists(fname)) {
    result = fname;
    return true;
  }
  std::string lastname = basename(fname);

  result = join_paths(path, fname);
  if (if_file_exists(result)) return true;

  result = join_paths(path, basename(fname));
  if (if_file_exists(result)) return true;

  return false;
}

std::string time_stamp() {

  time_t ltime;
  struct tm *Tm;
  ltime = time(NULL);
  Tm = localtime(&ltime);
  return utils::string_format("%d-%d-%d,%d:%d:%d", Tm->tm_mday, Tm->tm_mon + 1, Tm->tm_year + 1900, Tm->tm_hour,
      Tm->tm_min, Tm->tm_sec);
}

void trim_extensions(std::string & fname,std::vector<std::string> extensions ) {

  for (const std::string &ext : extensions)
    if (fname.find(ext) != std::string::npos) utils::replace_substring(fname, ext, "");
}

std::string download_pdb(const std::string & code) {

  using namespace ui::www;

  http_t *request = http_get("http://files.rcsb.org/view/"+code+".pdb", NULL);
  if (!request) {
    io_utils_logger<<utils::LogLevel::SEVERE<< "Invalid request.\n";
    return "";
  }

  http_status_t status = HTTP_STATUS_PENDING;
  int prev_size = -1;
  while (status == HTTP_STATUS_PENDING) {
    status = http_process(request);
    if (prev_size != (int) request->response_size) {
      io_utils_logger << utils::LogLevel::INFO << utils::string_format("%d byte(s) received.\n", (int) request->response_size);
      prev_size = (int) request->response_size;
    }
  }

  if (status == HTTP_STATUS_FAILED) {
    io_utils_logger<<utils::LogLevel::SEVERE<< utils::string_format("HTTP request failed (%d): %s.\n", request->status_code, request->reason_phrase);
    http_release(request);
    return "";
  }

  std::string out((char const *) request->response_data);
  http_release(request);

  return out;
}

std::string download_pdb(const std::string & code, const std::string & local_path) {

  utils::Logger logger("download_pdb");
  std::string fname = core::data::io::find_pdb_file_name(code, local_path);
  if(fname.size() == 0)
    return download_pdb(code);
  else {
    if (utils::has_suffix(fname, ".gz")) {
      std::string tmp;
      utils::load_binary_file(fname, tmp);
      if (tmp.size() == 0) EXIT("Zero bytes loaded from a gzip'ed file: " + fname + "\n");

      std::stringstream ssp;
      utils::ungzip_string(tmp,ssp);
      return ssp.str();
    } else {
      logger << utils::LogLevel::FILE << "Reading PDB from: " << fname << "\n";
      return load_text_file(fname);
    }
  }
}


std::map<std::string, std::string> read_two_column_file(const std::string &fname) {

  std::map<std::string, std::string> storage_map;

  std::ifstream in;
  in_stream(fname, in);
  size_t cnt = 0;
  std::string line, token;
  try {
    while (std::getline(in, line)) {
      if (line.length() < 2) continue;
      if (line[0] == '#') continue;
      if (line[0] == '!') continue;
      std::vector<std::string> tokens = split(line);
      storage_map[tokens[0]] = tokens[1];
      cnt++;
    }
  } catch (std::ifstream::failure &e) {
    if ((!in.fail()) || (!in.eof()) || (in.bad()))
      io_utils_logger.log(utils::LogLevel::CRITICAL, "Exception captured: ", e.what(), "\n");
  }
  io_utils_logger.log(utils::LogLevel::FILE, "found ", cnt, " tokens in ", fname, "\n");

  return storage_map;
}

}

