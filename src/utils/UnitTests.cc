#include <string>
#include <iostream>

#include <utils/UnitTests.hh>
#include <utils/LogManager.hh>

namespace utils {

template<>
bool UnitTests::assert_equals<std::string>(const std::string expected, const std::string actual, const std::string & msg) {

  if (expected.size() != actual.size()) {
    instance().last_msg << "The  size of the two given strings differs:  " << expected.size() << " vs " << actual.size() << "\n";
    if (msg.size() > 0) instance().last_msg << msg << "\n";

    return false;
  }
  if (expected.compare(actual) != 0) {
    for (size_t k = 0; k < expected.size(); k++) {
      if (expected[k] != actual[k]) {
        instance().last_msg << "The  values are different at index " << k << ": expected " << expected[k] << " but  got "
                            << actual[k] << "\n";
        return false;
      }
    }
  }
  return true;
}


bool UnitTests::assert_equals(const core::data::basic::Vec3 & expected, const core::data::basic::Vec3 actual, const double tolerance) {

  bool out = UnitTests::instance().assert_equals_(expected.x, actual.x, tolerance,
                                                  "The X coordinate of a given vector differs more than the allowed threshold: ");
  out &= UnitTests::instance().assert_equals_(expected.y, actual.y, tolerance,
                                              "The Y coordinate of a given vector differs more than the allowed threshold: ");
  out &= UnitTests::instance().assert_equals_(expected.z, actual.z, tolerance,
                                              "The Y coordinate of a given vector differs more than the allowed threshold: ");
  return out;
}

bool UnitTests::assert_equals(const core::data::basic::Vec3I &expected, const core::data::basic::Vec3I actual) {
  if (expected.ix() != actual.ix()) return false;
  if (expected.iy() != actual.iy()) return false;
  if (expected.iz() != actual.iz()) return false;
  return true;
}


bool UnitTests::assert_equals(const double expected, const double actual, const double tolerance, const std::string & msg) {
  return UnitTests::instance().assert_equals_(expected, actual, tolerance, msg);
}

bool UnitTests::assert_equals_(const double expected, const double actual, const double tolerance, const std::string &msg_prefix) {

  if (fabs(expected - actual) > tolerance) {
    last_msg <<  msg_prefix << expected << " !=  " << actual << "\n";
    return false;
  }
  return true;
}

bool UnitTests::assert_true(const bool expression, const std::string &error_msg) {
  return UnitTests::instance().assert_true_(expression,error_msg);
}

bool UnitTests::assert_true_(const bool expression, const std::string &error_msg) {

  if (!expression) {
    last_msg << error_msg;
    return false;
  }
  return true;
}

void UnitTests::start_test(const std::string & test_name) {

  std::cerr << test_name << ' ';
  for (size_t i = 0; i < screen_width - test_name.length() - 2; i++) std::cerr << '.';
  timer_start();
}

void UnitTests::end_test(bool result) {

  if (timer_on && !timer_off) timer_stop();
  if (result) {
    if (timer_off)
      std::cerr << " OK ........... in " << (double(e_time - b_time) / (CLOCKS_PER_SEC * 0.001)) << "[ms]\n";
    else std::cerr << " OK\n";
  }
  else {
    if (timer_off) {
      std::cerr << " FAILED ....... after " << (double(e_time - b_time) / (CLOCKS_PER_SEC * 0.001)) << "[ms]\n";
      n_failed++;
      std::cerr <<((last_msg.str().size() >0) ? last_msg.str() : "UNKNOWN REASON" )<< std::endl;
    } else {
      std::cerr << " FAILED!\n";
      std::cerr << last_msg.str() << std::endl;
      n_failed++;
    }
  }

  last_msg.str(std::string());
  timer_off = timer_on = false;
}


size_t UnitTests::add_test(const std::string & test_name, TestFunction f) {

  UnitTests & ut = instance();
  ut.tests_.push_back(std::make_pair(test_name,f));

  return ut.tests_.size();
}

void UnitTests::show_suite_title() {

  int n_sep = screen_width - title_.length() - 2;
  for (size_t i = 0; i < n_sep/2; i++) std::cerr << '=';
  std::cerr << " " << title_<< " ";
  for (size_t i = 0; i < n_sep/2; i++) std::cerr << '=';
  std::cerr << "\n";
}

bool UnitTests::run(bool turn_off_logs) {

  show_suite_title();

  if(turn_off_logs)
    LogManager::OFF();                    // --- switch off all logs because they mess up with unit test output table
  else LogManager::FINER();
  bool and_result = true;
  utils::UnitTests &ut = utils::UnitTests::instance();
  for (const auto &f : ut.tests()) {
    ut.start_test(f.first);
    bool result = f.second();
    and_result &= result;
    ut.end_test(result);
  }
  std::cerr << "\n";

  return and_result;
}

bool UnitTests::run_selected(const std::string & which_test, bool turn_off_logs) {

  show_suite_title();

  int i = 0;
  utils::UnitTests &ut = utils::UnitTests::instance();
  for (const auto &f: ut.tests()) {
    if (f.first == which_test)
      return run_selected(i, turn_off_logs);
    ++i;
  }

  return false;
}


bool UnitTests::run_selected(const int which_test, bool turn_off_logs) {

  show_suite_title();

  if (turn_off_logs)
    LogManager::OFF();                    // --- switch off all logs because they mess up with unit test output table
  else LogManager::FINER();
  utils::UnitTests &ut = utils::UnitTests::instance();
  const auto &f = ut.tests()[which_test];
  ut.start_test(f.first);
  bool and_result = f.second();
  ut.end_test(and_result);

  return and_result;
}
}
