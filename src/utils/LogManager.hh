#ifndef UTILS_LogManager_HH
#define UTILS_LogManager_HH

#include <unistd.h>

#include <unordered_set>
#include <mutex>

#include <utils/Logger.hh>

namespace utils {

/** @brief Logging utility for BioShell apps.
 *
 */
class LogManager {
public:
  /// Provides a singleton reference to the logging manager
  static LogManager & get() {
    static LogManager manager;
    return manager;
  }

  /// Returns true if loggers print in color
  static bool use_colors();

  /** @brief Switch ON/OFF color output
   *
   * @param value - say true to turn on output coloring (make sure your termina supports that)
   */
  static void use_colors(bool value) { use_colors_ = value;}

  /** @brief Turn of a given logger
   *
   * @param logger_name - name of a logger to be switched off
   */
  static void mute(const std::string & logger_name) {
    logger <<LogLevel::INFO << "muted channel "<<logger_name<<"\n";
    muted.insert(logger_name);
  }

  /** @brief check is a given logger is muted
   *
   * @param logger_name - name of a logger
   * @return state of the logger
   */
  static bool is_muted(const std::string & logger_name) {
    return (muted.find(logger_name) != muted.end());
  }

  /** @brief Sets a new logging level
   *
   * @param new_level - importance level of messages that will be recorded
   */
  static void set_level(LogLevel & new_level) {
    manager.log_level = new_level;
  }

  /** @brief Sets a new logging level
   *
   * @param new_level_name - importance level of messages that will be recorded
   */
  static void set_level(const std::string &new_level_name) {

    std::map<LogLevel, std::string>::const_iterator it;
    std::string n(new_level_name);

    try {
      LogLevel new_level = log_level_by_name(new_level_name);
      manager.log_level = new_level;
      logger << "Logging level set to " << new_level_name << "\n";
    } catch (std::runtime_error e) {
      logger << LogLevel::WARNING << "Unknown log level " << new_level_name << "\n";
    }
  }

  /// No logging at all
  static void OFF() { manager.log_level = LogLevel::OFF; }

  /// Record only CRITICAL messages
  static void CRITICAL() { manager.log_level = LogLevel::CRITICAL; }

  /// Record at least SEVERE messages
  static void SEVERE() { manager.log_level = LogLevel::SEVERE; }

  /// Record at least WARNING messages
  static void WARNING() { manager.log_level = LogLevel::WARNING; }

  /// Record at least FILE messages
  static void FILE() { manager.log_level = LogLevel::FILE; }

  /// Record at least INFO messages
  static void INFO() { manager.log_level = LogLevel::INFO; }

  /// Record at least FINE messages
  static void FINE() { manager.log_level = LogLevel::FINE; }

  /// Record at least FINER messages
  static void FINER() { manager.log_level = LogLevel::FINER; }

  /// Record all messages, even the FINEST
  static void FINEST() { manager.log_level = LogLevel::FINEST; }

private:
  static bool use_colors_;
  static bool use_colors_setup_;
  static const LogManager manager;
  std::ostream &sink;
  static LogLevel log_level;
  static utils::Logger logger;
  static std::unordered_set<std::string> muted;

  LogManager() : sink(std::cerr) {

    if (!isatty(fileno(stderr))) use_colors(false);
    else use_colors(true);
  }

  LogManager(LogManager const&);
  void operator=(LogManager const&);

  friend Logger;

  friend Logger &operator <<(Logger &logger, const LogLevel level);

  friend Logger &operator <<(Logger &logger, const char* message);

  friend Logger &operator <<(Logger &logger, const std::string & message);

  friend Logger &operator <<(Logger &logger, const double value);

  friend Logger &operator <<(Logger &logger, const int number);

  friend Logger &operator <<(Logger &logger, const size_t number);

  friend Logger &operator <<(Logger &logger, const unsigned long long number);

  friend Logger &operator <<(Logger &logger, const float value);

  friend Logger &operator <<(Logger &logger, const char c);

  friend Logger &operator<<(Logger &logger, const unsigned number);

  friend Logger &operator<<(Logger &logger, const core::index2 number);

  friend Logger &operator<<(Logger &logger, const core::index1 number);};

}

#endif // ~ UTILS_LogManager_HH
