#include <algorithm>

#include <core/BioShellEnvironment.hh>
#include <core/data/io/load_decoys.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <core/protocols/selection_protocols.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/select_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <utils/exceptions/NoSuchFile.hh>
#include <utils/LogManager.hh>
#include <utils/Logger.hh>
#include <utils/io_utils.hh>


namespace utils {
namespace options {

static utils::Logger input_utils_logger("structures_from_cmdline");

void structures_from_cmdline(std::vector<std::string> & structure_names,
    std::vector<core::data::structural::Structure_SP> & structures) {

  utils::options::OptionParser & cmd = utils::options::OptionParser::get();

  std::vector<std::string> fnames;
  std::vector<char> selected_chains;
  if (cmd.is_registered(input_pdb_list)) {
      if (input_pdb_list.was_used()) read_listfile(option_value<std::string>(input_pdb_list), fnames);
      if (cmd.was_used(select_chains))
          for (auto i=0;i<fnames.size();i++)
          selected_chains.push_back(utils::options::option_value<std::string>(select_chains)[0]);
  }

  if (cmd.is_registered(input_pdb)) {
    if (input_pdb.was_used()) {
        utils::split(utils::options::option_value<std::string>(input_pdb), fnames);
        if (cmd.was_used(select_chains))
            selected_chains.push_back(utils::options::option_value<std::string>(select_chains)[0]);
    }
  }

  if (cmd.is_registered(input_chainid_list)) {
    std::vector<std::string> tmp;
    if (input_chainid_list.was_used()) read_listfile(option_value<std::string>(input_chainid_list), tmp);
    for(const std::string & s : tmp) {
      if (s.length() > 4) {
        size_t pos = s.find(':');
        if(pos!=s.npos) { // --- chains defined as 2kwi:A or 2kwi:AB
          for (core::index1 i = pos + 1; i < s.length(); ++i) {
            fnames.push_back(s.substr(0, pos));
            selected_chains.push_back(s[i]);
          }
        } else {
          if (isupper(s[4])) {
            fnames.push_back(s.substr(0, 4));
            selected_chains.push_back(s[4]);
          }
        }
      }
    }
  }

  // --- If we have selected_chains then the number of input structures must match the number of the selected chains
  if((selected_chains.size()>0) &&(selected_chains.size() != fnames.size())) {
    input_utils_logger << utils::LogLevel::WARNING
                       << "The number of selected chains must be equal to the number of input structures! ("
                       << fnames.size() << " != " << selected_chains.size() << ")\n";
    return;
  }
  input_utils_logger << utils::LogLevel::FILE << "Loading " << fnames.size() << " files\n";

  const core::data::io::PdbLineFilter  atom_filters = get_atom_line_filter();
  const core::data::io::PdbLineFilter  header_filters = get_header_line_filter();

  bool if_all_models = (cmd.is_registered(all_models) && all_models.was_used()) ? true : false;
  std::vector<core::index4> model_ids;
  if(cmd.is_registered(select_models) && select_models.was_used()) {
    utils::split(option_value<std::string>(select_models),model_ids, {','});
    if_all_models = true;
    input_utils_logger << utils::LogLevel::FILE << "Only " << model_ids.size() << " model(s) selected\n";
  }
  std::string path = (cmd.is_registered(input_pdb_path) && input_pdb_path.was_used()) ? utils::options::option_value<std::string>(
    input_pdb_path) : "";
  core::index4 str_cnt = 0;
  for (const std::string & fname : fnames) {
    std::string fn = fname;
    if(cmd.is_registered(input_pdb_path) && input_pdb_path.was_used()) {
      fn = core::data::io::find_pdb_file_name(fname, path);
      if (fn.length() == 0) {
        input_utils_logger << utils::LogLevel::SEVERE << "Can't locate file: " + fname + "\n";
        ++str_cnt;
        continue;
      }
    }

    core::data::io::Pdb pdb(fn, atom_filters, header_filters,
        (cmd.is_registered(input_pdb_header) && input_pdb_header.was_used()));
    if (pdb.count_models() <= 0) {
      input_utils_logger << utils::LogLevel::SEVERE << "The file: " + fname + " doesn't provide any structure\n";
      ++str_cnt;
      continue;
    }
    if (pdb.count_atoms(0) <= 0) {
      input_utils_logger << utils::LogLevel::SEVERE << "The file: " + fname + " doesn't provide any atom\n";
      ++str_cnt;
      continue;
    }
    if (fn.find(".gz") != std::string::npos) utils::replace_substring(fn, ".gz", "");
    if (!if_all_models) {
      structures.push_back(pdb.create_structure(0));
      structure_names.push_back(fn);
      input_utils_logger << utils::LogLevel::FILE << "Loading structure named: " << fn << "\n";
      if(selected_chains.size()>0) {
        input_utils_logger << utils::LogLevel::FILE << "Selecting chain: " << selected_chains[str_cnt] << "\n";
        core::data::structural::selectors::ChainSelector chain_selector(selected_chains[str_cnt]);

        core::protocols::keep_selected_chains(chain_selector, *structures.back());
        std::remove_if(structures.back()->begin(), structures.back()->end(),
          [&chain_selector](core::data::structural::Chain_SP c) { return !chain_selector(c); });
      }
    } else {
      std::pair<std::string, std::string> re = utils::root_extension(fn);
      for (core::index4 n = 0; n < pdb.count_models(); ++n) {
        if ((model_ids.size() > 0) && (std::find(model_ids.cbegin(), model_ids.cend(), n) == model_ids.cend())) {
          ++str_cnt;
          continue;
        }
        structures.push_back(pdb.create_structure(n));
        structure_names.push_back(utils::string_format("%s-%d.%s", re.first.c_str(), n + 1, re.second.c_str()));
        input_utils_logger << utils::LogLevel::FILE << "Loading structure named: " << structure_names.back() << "\n";
        if(selected_chains.size()>0) {
          core::data::structural::selectors::ChainSelector chain_selector(selected_chains[str_cnt]);
          std::remove_if(structures.back()->begin(), structures.back()->end(),
            [&chain_selector](core::data::structural::Chain_SP c) { return !chain_selector(c); });
        }
      }
    }
    ++str_cnt;
  }

  if(cmd.is_registered(input_pdb_sort) && input_pdb_sort.was_used())
    for(core::data::structural::Structure_SP s : structures) s->sort();


  input_utils_logger << utils::LogLevel::INFO << structures.size() << " structures loaded\n";
}

core::data::structural::Structure_SP models_from_cmdline(std::vector<std::string> &model_names,
                                                         std::vector<core::data::basic::Coordinates_SP> &models) {

  utils::options::OptionParser &cmd = utils::options::OptionParser::get();

  std::vector<std::string> fl; // --- files to read
  // --- check for a single multi-model PDB file
  if (cmd.is_registered(input_pdb_models) && input_pdb_models.was_used())
    fl.push_back(option_value<std::string>(input_pdb_models));

  // --- load PDB frames listed in a list-file
  if (cmd.is_registered(input_pdb_modelslist) && input_pdb_modelslist.was_used()) {
    for (const std::string &s : utils::read_listfile(option_value<std::string>(input_pdb_modelslist)))
      fl.push_back(s);
    input_utils_logger << utils::LogLevel::FILE << fl.size() << " files found on a list from "
                       << option_value<std::string>(input_pdb_modelslist) << "\n";
  }

  // --- what to read?
  auto atom_predicate = utils::options::get_atom_line_filter();
  auto header_predicate = utils::options::get_header_line_filter();

  // --- read the very first Structure object (if given)
  if(fl.size()>0) {
    core::data::io::Pdb reader(fl.front(), atom_predicate, header_predicate, true, true);
    core::data::structural::Structure_SP s = reader.create_structure(0);

    core::data::io::read_decoys(fl, s->count_atoms(), models, model_names, atom_predicate);

    input_utils_logger << utils::LogLevel::FILE << models.size() << " models loaded\n";
    return s;

  }

  return nullptr;
}

const core::data::io::PdbLineFilter get_atom_line_filter() {

  utils::options::OptionParser & cmd = utils::options::OptionParser::get();

  std::vector<core::data::io::PdbLineFilter> filters;

  if (cmd.is_registered(ca_only) && ca_only.was_used()) {
    input_utils_logger << utils::LogLevel::FILE << "is_ca PDB line filter used to read a PDB file \n";
    filters.push_back(core::data::io::is_ca);
  }
  if (cmd.is_registered(bb_only) && bb_only.was_used()) {
    input_utils_logger << utils::LogLevel::FILE << "is_BB PDB line filter used to read a PDB file \n";
    filters.push_back(core::data::io::is_bb);
  }
  if (cmd.is_registered(include_hydrogens) && (!include_hydrogens.was_used())) {
    input_utils_logger << utils::LogLevel::FILE << "is_not_hydrogen PDB line filter used to read a PDB file \n";
    filters.push_back(core::data::io::is_not_hydrogen);
  }

  if (!cmd.is_registered(keep_alternate) || (!keep_alternate.was_used())) {
    input_utils_logger << utils::LogLevel::FILE << "is_not_alternative PDB line filter used to read a PDB file \n";
    filters.push_back(core::data::io::is_not_alternative);
  }
  if (cmd.is_registered(keep_water) && (keep_water.was_used())) {
    input_utils_logger << utils::LogLevel::FILE << "Reading in water molecules!\n";
    filters.push_back(core::data::io::is_water);
  } else filters.push_back(core::data::io::is_not_water);

  switch(filters.size()) {
    case 1:
      return filters[0];
    case 2:
      return core::data::io::all_true(filters[0], filters[1]);
    case 3:
      return core::data::io::all_true(filters[0], filters[1], filters[2]);
    case 4:
      return core::data::io::all_true(filters[0], filters[1], filters[2], filters[3]);
    case 5:
      return core::data::io::all_true(filters[0], filters[1], filters[2], filters[3],filters[4]);
    default : return core::data::io::keep_all;
  }
}


const core::data::io::PdbLineFilter get_header_line_filter() {

  utils::options::OptionParser & cmd = utils::options::OptionParser::get();

  std::vector<core::data::io::PdbLineFilter> filters;

  if (cmd.is_registered(input_pdb_header_ss) && input_pdb_header_ss.was_used()) {
    input_utils_logger << utils::LogLevel::FILE << "input_pdb_header_ss PDB line filter used to filter PDB header \n";
    filters.push_back(core::data::io::only_ss_from_header);
  }

  switch(filters.size()) {
    case 1:
      return filters[0];
    case 2:
      return core::data::io::all_true(filters[0], filters[1]);
    case 3:
      return core::data::io::all_true(filters[0], filters[1], filters[2]);
    case 4:
      return core::data::io::all_true(filters[0], filters[1], filters[2], filters[3]);
    case 5:
      return core::data::io::all_true(filters[0], filters[1], filters[2], filters[3],filters[4]);
    default : return core::data::io::keep_all;
  }
}

core::data::structural::Structure_SP native_from_cmdline() {

  utils::options::OptionParser & cmd = utils::options::OptionParser::get();

  if (input_pdb_native.was_used()) {
    core::data::io::Pdb reader(option_value<std::string>(input_pdb_native), utils::options::get_atom_line_filter());

    if (cmd.is_registered(select_models) && select_models.was_used()) {
      std::vector<core::index4> model_ids;
      utils::split(option_value<std::string>(select_models), model_ids, {','});
      return reader.create_structure(model_ids[0]);
    } else return reader.create_structure(0);
  }
  return nullptr;
}

core::data::basic::Coordinates_SP native_coordinates_from_cmdline() {

  core::data::structural::Structure_SP native = native_from_cmdline();
  if (native != nullptr) {
    core::data::basic::Coordinates_SP xyz = std::make_shared<core::data::basic::Coordinates>(native->count_atoms());
    int i = -1;
    std::for_each(native->first_atom(), native->last_atom(),
        [&](core::data::structural::PdbAtom_SP a) {(*xyz)[++i].set(*a);});

    return xyz;
  }
  return nullptr;
}

}
}
