/** \file scoring_options.hh
 * @brief defines option objects that control energy evaluations
 */
#ifndef UTILS_OPTIONS_scoring_options_HH
#define UTILS_OPTIONS_scoring_options_HH

#include <utils/options/Option.hh>

namespace utils {
namespace options {

/** @name Options that control which energy function will be used for scoring
 */
///@{
/// Read a score function config file
static Option scfx_config("-x", "-in:scfx", "provide a config file that defines energy function");
/// Turn on a Go-like term in CABS application
static Option cabs_bb_go("-cabs_bb_go", "-scfx::cabs_bb_go", "use default CABS-bb with additional Go-like term; requires reference structure.");
/// Use CABS in include-backbone mode
static Option cabs_bb("-cabs_bb", "-scfx::cabs_bb", "use default CABS-bb (CABS with explicit backbone) energy for scoring");
/// Use CABS in its original (no backbone) mode
static Option cabs("-cabs", "-scfx::cabs", "use default CABS energy for scoring");
/// Scoring config for SURPASS model
static Option surpass("-surpass", "-scfx:surpass", "use default SURPASS energy for scoring");
///@}

}
}

#endif
