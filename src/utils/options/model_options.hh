/** \file model_options.hh
 * @brief defines option objects that set up model for simulations
 */
#ifndef UTILS_OPTIONS_model_options_HH
#define UTILS_OPTIONS_model_options_HH

#include <utils/options/Option.hh>

namespace utils {
namespace options {

/** @name "model" options
 *  Command line options used to set up starting conformation for a simulation
 */
///@{

/// number of atoms of a modeled system; e.g. argon atoms or beads of a homopolymer
static Option n_atoms("-n_atoms", "-model:n_atoms", "the number of atoms in the sampled system");

/// number of n_chains of a modeled system
static Option n_chains("-n_chains", "-model:n_chains", "the number of chains in the sampled system");

/// staring a simulation from a conformation from a PDB file
static Option staring_model_pdb("-mpdb", "-model:pdb", "staring conformation from a PDB file");

/// start a chain (polymer or protein) simulation from a straight conformation"
static Option staring_model_linear("-model::linear", "-model:linear", "start a chain (polymer or protein) simulation from a straight conformation");

/// start a chain (polymer or protein) simulation from a random conformation
static Option staring_model_random("-model::random", "-model:random", "start a chain (polymer or protein) simulation from a random conformation");

///@}
}
}

#endif
