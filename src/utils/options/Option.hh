#ifndef UTILS_OPTIONS_Option_HH
#define UTILS_OPTIONS_Option_HH

#include <iostream>
#include <stdexcept>
#include <string>
#include <memory>

#include <utils/string_utils.hh>

namespace utils {
namespace options {

class OptionParser;

/// Object that corresponds to a single option (flag) that may be specified at command line for any BioShell program
class Option {
public:

  /** Creates a new option object
   *
   * @param short_name - short (single-character) name for this option, e.g. <code>-h</code> or <code>-n</code>
   * @param name - long name for this option e.g. <code>-help</code>
   * @param info - description string for this option that will be printed in a help message
   * @param default_value_string - default value for this option
   * @param if_mandatory - if true, the option is mandatory.
   */
  Option(const std::string &short_name, const std::string &name, const std::string &info,
    const std::string &default_value_string = "", const bool if_mandatory = false) :
    mandatory_(if_mandatory), short_name_(short_name), name_(name), info_(info), default_value_string_(default_value_string) {}

  /// Virtual destructor
  virtual ~Option() { }

  /** @brief Generic method sets the new default value for this option.
   *
   * @tparam T - type of the value
   * @param value - the new dafault value for this option which will be stored in <code>default_value_string</code>
   * field after conversion to <code>std::string</code>
   */
  template <typename T>
  void set_default(T value) { default_value_string_ = utils::to_string(value); }

  /** @brief Generic operator to sets the new default value for this option.
   *
   * The operator simply calls <code>void set_default<T>(T)</code> and returns the reference to this object.
   * This allows very handy call when registering options for an application:
   *
   * @tparam T - type of the value
   * @param value - the new default value for this option which will be stored in <code>default_value_string</code>
   * field after conversion to <code>std::string</code>
   */
  template <typename T>
  Option & operator()(T value) {

    default_value_string_ = utils::to_string(value);
    return *this;
  }

  /// Returns the short name of this option, e.g. <code>-ip</code> for <code>-in::pdb</code>
  const std::string & short_name() const { return short_name_;}

  /// Returns the long name of this option, e.g. <code>-in::pdb</code>
  const std::string & name() const { return name_; }

  /// help message for this option
  const std::string & info() const { return info_; }

  /// default value for this option - as a string (will be parsed to its final type later)
  const std::string & default_value_string() const { return default_value_string_; }

  /// Returns the name of the XML element corresponding to this option, e.g. <code>in:pdb</code> for <code>-in::pdb</code>
  std::string get_element_name() const;

  /// Returns the description string associated with this option
  inline const std::string & get_info() const { return info_; }

  /// True if this option was used at the command line
  bool was_used() const;

  /// Method that will be executed on an option (if provided in a derived class)
  virtual void execute() {}

  /// Declare this option as mandatory
  void mandatory(const bool if_mandatory) { mandatory_ = if_mandatory; }

  /// Returns true if this option was declared mandatory
  bool mandatory() { return mandatory_; }

private:
  bool mandatory_;        ///< if set to true, a program will require this option to show up at command line
  std::string short_name_;   ///< a shortcut for this option, e.g. <tt>-h</tt> for <tt>-help</tt>
  std::string name_;         ///< option name i.e. the text given at command line, e.g.  <tt>-help</tt> for <tt><pre>-in::pdb</pre></tt>
  std::string info_;         ///< help message for this option
  std::string default_value_string_; ///< default value for this option - as a string (will be parsed to its final type later)

};

}
}
#endif
