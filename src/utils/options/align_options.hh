/** \file align_options.hh
 * @brief defines option objects (command line flags) related to sequence/structure alignment
 */
#ifndef UTILS_OPTIONS_align_options_HH
#define UTILS_OPTIONS_align_options_HH

#include <utils/options/Option.hh>
#include <utils/Logger.hh>
#include <utils/LogManager.hh>

namespace utils {
namespace options {

/** @name Options used by sequence and structure alignment applications: query input data
 */
///@{
static Option query_infile("-in:query", "-in::query", "input query data");
static Option query_name("-in:query:name", "-in::query::name", "string describing a query, e.g. a protein name");
static Option query_fasta("-in:query:fasta", "-in::query::fasta", "query sequence in the FASTA format");
static Option query_chk("-in:query:chk", "-in::query::chk", "query sequence profile in the CHK format");
static Option query_ss2("-in:query:ss2", "-in::query::ss2", "query secondary structure in the SS2 format");
static Option query_pdb("-in:query:pdb", "-in::query::pdb", "query structure in the PDB format");
static Option query_outfile("-out:query", "-out::query", "output query data");
static Option query_asn1("-in:query:asn1", "-in::query::asn1", "query sequence profile in the ASN.1 format (psiblast output)");
static Option query_fasta_string("-in:query:fasta::string", "-in::query::fasta::string", "query sequence as a string (1 char code)");
///@}

/** @name Options that provide template input file(s)
 */
///@{
static Option template_infile("-in:template", "-in::template", "input template data");
static Option template_name("-in:template:name", "-in::template::name", "string describing a template, e.g. a protein name");
static Option template_listfile("-in:template:listfile", "-in::template::listfile", "file that lists template files");
static Option template_fasta("-in:template:fasta", "-in::template::fasta", "template sequence in the FASTA format");
static Option template_fasta_string("-in:template:fasta::string", "-in::template::fasta::string", "template sequence as a string (1 char code)");
static Option template_chk("-in:template:chk", "-in::template::chk", "template sequence profile in the CHK format");
static Option template_asn1("-in:template:asn1", "-in::template::asn1", "template sequence profile in the ASN.1 format (psiblast output)");
static Option template_ss2("-in:template:ss2", "-in::template::ss2", "template secondary structure in the SS2 format");
static Option template_pdb("-in:template:pdb", "-in::template::pdb", "template structure in the PDB format");
static Option template_outfile("-out:template", "-out::template", "output template data");
///@}

/** @name Options that control  alignment calculations
 */
///@{
static Option align_global("-align:global", "-align::global", "calculate a global alignment");
static Option align_semiglobal("-align:semiglobal", "-align::semiglobal", "calculate a semiglobal alignment");
static Option align_local("-align:local", "-align::local", "calculate a local alignment");
static Option substitution_matrix("-align:matrix", "-align::matrix",
  "substitution matrix name to use; a file name may also be provided (NCBI file format)", "BLOSUM62");
static Option gap_open("-align:gap_open", "-align::gap_open", "gap opening penalty");
static Option gap_extend("-align:gap_extend", "-align::gap_extend", "gap extension penalty");
///@}

}
}

#endif
