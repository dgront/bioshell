#include <algorithm>

#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <utils/options/selector_from_cmdline.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/select_options.hh>
#include <utils/io_utils.hh>
#include <utils/Logger.hh>
#include <utils/LogManager.hh>

namespace utils {
namespace options {

utils::Logger select_utils_logger("select_utils");

std::vector<std::shared_ptr<core::data::structural::selectors::AtomSelector>> atoms_selection_from_cmdline() {

  using namespace core::data::structural;

  std::vector<std::shared_ptr<selectors::AtomSelector>> selectors;
  utils::options::OptionParser & cmd = utils::options::OptionParser::get();

  if(cmd.is_registered(select_aa) && select_aa.was_used()) {
    selectors.push_back(std::make_shared<selectors::IsAA>());
    select_utils_logger << utils::LogLevel::INFO << "selecting amino acids\n";
  }
  if(cmd.is_registered(select_nt) && select_nt.was_used()) {
    selectors.push_back(std::make_shared<selectors::IsNT>());
    select_utils_logger << utils::LogLevel::INFO << "selecting nucleotides\n";
  }
  if (cmd.is_registered(select_ca) && select_ca.was_used()) {
    selectors.push_back(std::make_shared<selectors::IsCA>());
    select_utils_logger << utils::LogLevel::INFO << "selecting alpha-carbons\n";
  }
  if (cmd.is_registered(select_cb) && select_cb.was_used()) {
    selectors.push_back(std::make_shared<selectors::IsCB>());
    select_utils_logger << utils::LogLevel::INFO << "selecting beta-carbons\n";
  }
  if (cmd.is_registered(select_bb) && select_bb.was_used()) {
    selectors.push_back(std::make_shared<selectors::IsBB>());
    select_utils_logger << utils::LogLevel::INFO << "selecting backbone atoms\n";
  }
  if (cmd.is_registered(select_bb_cb) && select_bb_cb.was_used()) {
    selectors.push_back(std::make_shared<selectors::IsBBCB>());
    select_utils_logger << utils::LogLevel::INFO << "selecting backbone with beta-carbons\n";
  }
  if (cmd.is_registered(select_atoms_by_name) && select_atoms_by_name.was_used()) {
    std::string name = utils::options::option_value<std::string>(select_atoms_by_name);
    selectors.push_back(std::make_shared<selectors::IsNamedAtom>(name));
    select_utils_logger << utils::LogLevel::INFO << "selecting atoms by name: "<<name<<"\n";
  }
  if (cmd.is_registered(select_substructure) && select_substructure.was_used()) {
    std::string selector = utils::options::option_value<std::string>(select_substructure);
    selectors.push_back(std::make_shared<selectors::SelectChainResidues>(selector));
    select_utils_logger << utils::LogLevel::INFO << "selecting a fragment: " << selector << "\n";
  }
  if(cmd.is_registered(select_chains) && select_chains.was_used()) {
    std::string selector = utils::options::option_value<std::string>(select_chains);
    selectors.push_back(std::make_shared<selectors::SelectChainResidues>(selector));
    select_utils_logger << utils::LogLevel::INFO << "selected chains: " << selector << "\n";
  }

  return selectors;
}

core::data::structural::selectors::AtomSelector_SP atom_selection_by_locator(std::string & sel_string) {

  using namespace core::data::structural;
  std::shared_ptr<selectors::CompositeSelector<selectors::SelectChainResidueAtom>> cs = nullptr;

  cs = std::make_shared<selectors::CompositeSelector<selectors::SelectChainResidueAtom>>(sel_string);
  if (select_utils_logger.is_logable(utils::LogLevel::INFO)) {
    std::stringstream ss;
    ss << "created selector for atoms: " << (*cs) << '\n';
    select_utils_logger << utils::LogLevel::INFO << ss.str();
  }

  return cs;
}

core::data::structural::selectors::AtomSelector_SP atom_selection_by_locator(utils::options::Option & op) {

  if (op.was_used()) {
    std::string sel_string = option_value<std::string>(op);
    return atom_selection_by_locator(sel_string);
  }

  return nullptr;
}

core::data::structural::selectors::AtomSelector_SP atom_selection_by_locator_file(utils::options::Option & op) {

  if (op.was_used()) {
    std::string sel_string = utils::load_text_file(option_value<std::string>(op));
    return atom_selection_by_locator(sel_string);
  }

  return nullptr;
}

std::vector<std::shared_ptr<core::data::sequence::SequenceFilter>> create_sequence_filters() {

  using namespace utils::options;
  using namespace core::data::sequence;

  utils::Logger l("create_sequence_filters");

  std::vector<std::shared_ptr<core::data::sequence::SequenceFilter>> filters;

  if (select_sequence_by_complexity.was_used()) {
    filters.push_back( std::make_shared<IsNotTooSimple>(option_value<core::index2>(select_sequence_by_complexity)) ) ;
    l << utils::LogLevel::INFO << "Created a filter for sequences having at least: "
      << option_value<core::index2>(select_sequence_by_complexity) << " distinct residue types\n";
  }

  if (select_sequence_by_name.was_used()) {
    filters.push_back(std::make_shared<FindInSequenceName>(option_value<std::string>(select_sequence_by_name)));
    l<<utils::LogLevel::INFO << "Created a filter for sequences based on name substring: "<< option_value<std::string>(
      select_sequence_by_name)<<"\n";
  }

  if (select_sequence_by_length.was_used()) {
    std::shared_ptr<NoShorterThan> ptr = std::make_shared<NoShorterThan>(
      option_value<core::index2>(select_sequence_by_length));
    l << utils::LogLevel::INFO << "Created a filter for sequences : shorter than "
      << option_value<std::string>(select_sequence_by_length)
      << " will be removed\n";
    filters.push_back(ptr);
  }

  if (select_nucleic_sequence.was_used()) {
    std::shared_ptr<IsNucleicSequence> ptr = std::make_shared<IsNucleicSequence>();
    l << utils::LogLevel::INFO << "Created filter will pass only nucleic acid sequences\n";
    filters.push_back(ptr);
  }

  if (select_protein_sequence.was_used()) {
    std::shared_ptr<IsProteinSequence> ptr = std::make_shared<IsProteinSequence>();
    l << utils::LogLevel::INFO << "Created filter will pass only protein sequences\n";
    filters.push_back(ptr);
  }

  if (select_by_known_res_fraction.was_used()) {
    std::shared_ptr<ByUnknownRatio> ptr = std::make_shared<ByUnknownRatio>(
      option_value<double>(select_by_known_res_fraction));
    l << utils::LogLevel::INFO
      << "Created filter will pass sequences only when the fraction of unknown residues is lower than"
      << option_value<double>(select_by_known_res_fraction) << "\n";
    filters.push_back(ptr);
  }

  return filters;
}
}
}
