#ifndef UTILS_OPTIONS_select_utils_HH
#define UTILS_OPTIONS_select_utils_HH

#include <algorithm>
#include <memory>

#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/sequence/SequenceFilter.hh>

#include <utils/options/Option.hh>

namespace utils {
namespace options {

/** @brief Returns atom selectors created based on a command line.
 *
 * This method tests the following options:
 *     - <code>select_aa</code>
 *     - <code>select_nt</code>
 *     - <code>select_ca</code>
 *     - <code>select_cb</code>
 *     - <code>select_bb</code>
 *     - <code>select_bb_cb</code>
 *     - <code>select_atoms_by_name</code>
 *     - <code>select_substructure</code>
 *
 * @return a vector of selector objects
 */
std::vector<std::shared_ptr<core::data::structural::selectors::AtomSelector>> atoms_selection_from_cmdline();

/** @brief Returns atom selector based on a command line.
 *
 * This method tests the given option attempting to parse its argument string as a file name
 * holding a set of atom locators.
 *
 * @return a pointer to a selector object
 */
std::shared_ptr<core::data::structural::selectors::AtomSelector> atom_selection_by_locator_file(utils::options::Option & op);

/** @brief Returns atom selector based on a command line.
 *
 * This method tests the given option attempting to parse its argument string as an atom locator.
 *
 * @return a pointer to a selector object
 */
std::shared_ptr<core::data::structural::selectors::AtomSelector> atom_selection_by_locator(utils::options::Option & op);

/** @brief Returns atom selector based on a locator string.
 *
 * This method is used by atom_selection_by_locator(utils::options::Option &) and atom_selection_by_locator_file(utils::options::Option &)
 *
 * @return a pointer to a selector object
 */
core::data::structural::selectors::AtomSelector_SP atom_selection_by_locator(std::string & sel_string);

/** @brief Returns a vector of sequence filters
 *
 * This method tests the following options:
 *     - <code>select_sequence_by_name</code>
 *     - <code>select_sequence_by_name_from_file</code>
 *     - <code>select_sequence_by_length</code>
 *     - <code>select_sequence_by_complexity</code>
 *     - <code>select_protein_sequence</code>
 *     - <code>select_nucleic_sequence</code>
 *     - <code>select_by_known_res_fraction</code>
 *
 * @return a vector of sequence filters
 */
std::vector<std::shared_ptr<core::data::sequence::SequenceFilter>> create_sequence_filters();

}
}

#endif
