/** \file input_utils.hh
 * @brief provides routines that read various data based on input specified at command line
 *
 * The methods checks input command line options
 */
#ifndef UTILS_OPTIONS_input_utils_HH
#define UTILS_OPTIONS_input_utils_HH

#include <core/data/basic/Vec3.hh>
#include <core/data/structural/Structure.hh>

namespace utils {
namespace options {

/** @brief Reads structures from files in PDB format.
 *
 * This method checks the following command line option objects:
 *  - <code>input_pdb</code>
 *  - <code>input_pdb_path</code>,
 *  - <code>input_pdb_listfile</code>
 *  - <code>if_all_models</code>
 *  - <code>select_models</code>
 *  - <code>input_pdb_header</code>
 *  - <code>input_pdb_sort</code>
 * which must be registered within the main() method. This method also loads pdb-line filters
 * by calling <code>get_pdb_line_filter()</code>
 */
void structures_from_cmdline(std::vector<std::string> & structure_names,
    std::vector<core::data::structural::Structure_SP> & structures);

/** @brief Reads models from files in PDB format.
 *
 * Models are structures of the same protein i.e. having the same sequence and number of residues. Therefore
 * they may be processed differently than just structures of different proteins.
 *
 * This method checks the following command line flags :
 *   - <code>input_pdb_modelslist</code>
 *   - <code>input_pdb_models</code>
 * which must be registered within the main() method. At least one of them must be used to read any models
 *
 * @param model_names - string ID for each model is created and stored in this vector
 * @param sink - container for the models obtained from input. The order of models corresponds to the order of model names
 * @return the very first model from the input as a Structure object. All other structures may be created just by substituting
 * coordinates of this structure with an appropriate model's coordinates.
 */
core::data::structural::Structure_SP models_from_cmdline(std::vector<std::string> & model_names, std::vector<core::data::basic::Coordinates_SP> & sink);

/** @brief Creates a PDB-line filter based on command-line flags.
 *
 * This method checks <code>ca_only</code>, <code>bb_only</code> and <code>include_hydrogens</code> flags
 * and returns <code>core::data::io::is_ca</code>, <code>core::data::io::is_bb</code>, <code>core::data::io::is_not_hydrogen</code>
 * or <code>core::data::io::keep_all</code> predicate, depending on the input.
 */
const core::data::io::PdbLineFilter get_atom_line_filter();

/** @brief Creates a PDB-line filter for filtering header based on command-line flags.
 *
 * This method checks <code>input_pdb_header_ss</code> predicate
 */
const core::data::io::PdbLineFilter get_header_line_filter();

/** @brief Reads the native PDB structure.
 *
 * This method checks <code>input_pdb_native</code> and all filters(options called by <code>get_pdb_line_filter()</code>
 * This method also checks <code>select_models</code> so it is possible to select a reference model out of a PDB trajectory.
 * Only the first model index given by this option is used
 */
core::data::structural::Structure_SP native_from_cmdline();

/** @brief Reads the native PDB structure and returns bare coordinates of all the selected atoms.
 *
 * This method checks <code>input_pdb_native</code> and all filters(options called by <code>get_pdb_line_filter()</code>
 */
core::data::basic::Coordinates_SP native_coordinates_from_cmdline();

}
}

#endif
