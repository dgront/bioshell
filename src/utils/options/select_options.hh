/** \file select_options.hh
 * @brief  option objects to select things inside BioShell program, e.g. a substructure, a sequence fragment, chain, etc.
 */
#ifndef UTILS_OPTIONS_select_options_HH
#define UTILS_OPTIONS_select_options_HH

#include <utils/options/Option.hh>
#include <utils/Logger.hh>
#include <utils/LogManager.hh>

namespace utils {
namespace options {

/** @name "select" options
 *  Command line options used by BioShell applications to select a part of their input: sequences, models, substructures, atoms, etc
 */
///@{
/// select a substructure
static Option select_substructure("-select::substructure", "-select::substructure", "selects a fragment of a biomolecular structure");
/// select one or more chains
static Option select_chains("-select::chains", "-select::chains", "selects given chains (e.g -select::chains=BC)")  ;
/// elect models by their ID
static Option select_models("-select::models", "-select::models", "selects particular models for calculations");

/// select a sequence by name
static Option select_sequence_by_name("-select:sequence:by_name", "-select::sequence::by_name", "selects sequences whose header contains a given string");
/// select a sequence(s) by name(s) loaded from a file
static Option select_sequence_by_name_from_file("-select:sequence:by_names_file", "-select::sequence::by_names_file", "selects sequences whose header contains a string from a given input file");
/// select sequences long enough
static Option select_sequence_by_length("-select:sequence:long_at_least", "-select::sequence::long_at_least", "selects sequences at least that many residues long");
/// skip low-complexity sequences
static Option select_sequence_by_complexity("-select:sequence:by_complexity", "-select::sequence::by_complexity", "selects sequences that comprise at least that many residue types (e.g. so poly-Q may be removed", "10");
/// select only protein sequences
static Option select_protein_sequence("-select:sequence:protein", "-select::sequence::protein", "selects only protein sequences");
/// select only nucleic acid chains
static Option select_nucleic_sequence("-select:sequence:nucleic", "-select::sequence::nucleic", "selects only nucleic acid sequences (DNA or RNA)");
/// select sequences that do not contain unknown residues
static Option select_by_known_res_fraction("-select:sequence:known", "-select::sequence::known", "selects sequences only when fraction of non-standard residues ('X') is low enough");


/// Option to select amino acids only
static Option select_aa("-aa", "-select::aa", "selects all amino acids");
/// Option to select bases only
static Option select_nt("-nt", "-select::nt", "selects all nucleic acids");
/// Option to select an amino acid by its three-letter code
static Option select_residues_by_code3("-select:residues:by_code3", "-select::residues::by_code3", "selects all residues identified by this three-letter code");

/// Option to select alpha-carbons only
static Option select_ca("-ca", "-select::ca", "selects all alpha-carbons");
/// Option to select backbone atoms
static Option select_bb("-bb", "-select::bb", "selects all backbone atoms");
/// Option to select beta-carbons only
static Option select_cb("-cb", "-select::cb", "selects all beta-carbons");
/// Option to select backbone atoms and beta-carbons
static Option select_bb_cb("-bb_cb", "-select::bb_cb", "selects all backbone atoms and CBetas");
/// Option to select atoms by name
static Option select_atoms_by_name("-select:atoms:by_name", "-select::atoms::by_name", "selects all atoms for a given name");
/// Option to select atoms by locator
static Option select_atom_by_locator("-select::atoms::by_locator", "-select::atoms::by_locator", "selects particular atoms (provide atom locators separated with a coma)");
/// Option to select atoms by locators loaded from a file
static Option select_atom_by_locatorfile("-select::atoms::by_locator:file", "-select::atoms::by_locator::file", "selects particular atoms (list of locators loaded from file)");

///@}

}
}

#endif
