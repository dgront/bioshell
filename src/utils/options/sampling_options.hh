/** \file sampling_options.hh
 * @brief defines option objects that defines sampling parameters
 */
#ifndef UTILS_OPTIONS_sampling_options_HH
#define UTILS_OPTIONS_sampling_options_HH

#include <utils/options/Option.hh>

namespace utils {
namespace options {

/** @name "sampling" options
 *  Command line options used by simulation applications to control sampling process
 */
///@{
/// Random seed
static Option rnd_seed("-seed", "-sample:seed", "sets random generator seed for MC sampling");

/// Request total time of MD run
static Option md_time("-md_time", "-sample:md_time", "the MD time to run the system");
/// Request than number of inner MC cycles
static Option mc_inner_cycles("-i_cycles", "-sample:mc_inner_cycles", "the number of small MC cycles (inner MC loop) to perform");
/// Request than number of outer MC cycles
static Option mc_outer_cycles("-o_cycles", "-sample:mc_outer_cycles", "the number of large MC cycles (outer MC loop) to perform");
/// Multiply the number of MC moves in every MC inner cycle by this factor
static Option mc_cycle_factor("-cycles_x", "-sample:mc_cycle_factor", "make each MC cycle N times longer");
/// Request than number of replica MC exchanges
static Option replica_exchanges("-exchanges", "-sample:exchanges", "the number of my_sampler exchanges");

/// Temperature of MC isothermal run
static Option temperature("-t", "-sample:temperature", "temperature of an isothermal simulation");
/// Initial temperature of MC annealing
static Option begin_temperature("-t_start", "-sample:t_start", "initial temperature of the simulation");
/// Final temperature of MC annealing
static Option end_temperature("-t_end", "-sample:t_end", "final temperature of the simulation");
/// Number of MC temperature steps in  MC annealing
static Option temp_steps("-t_steps", "-sample:t_steps", "the number of isothermal steps to make");
/// Number of replicas in REMC run
static Option replicas("-replicas", "-sample:replicas", "temperatures for replicas in REMC simulation (the number of temperature values defines the number of replicas)");
/// Define REMC observations mode
static Option replica_observation_mode("-observation_mode", "-sample:replicas:observation_mode",
  "observation mode: ISOTHERMAL - same temperature (default); ISOTEMPORAL - contiguous time trajectory");

/// Maximum move range for backrub mover (in radians)
static Option backrub_range("-sample:backrub:range", "-sample::backrub::range", "sets the maximum rotation angle [in radians] for backrub moves");
/// Maximum move range for phi-psi mover (in radians)
static Option phi_psi_range("-sample:phi_psi:range", "-sample::phi_psi::range", "sets the maximum rotation angle [in radians] for phi-psi moves");
/// Maximum move range for chain swing mover  (in radians)
static Option chain_swing_range("-sample:chain_swing:range", "-sample::chain_swing::range", "sets the maximum rotation angle [in radians] for chain-swing moves");

/// Request for random perturbation mover of a single bead (atom)
static Option use_random_jump("-sample:perturb", "-sample::perturb", "single-bead random perturbation mover will be used for sampling");
/// Maximum move range for random jump mover (in Angstroms)
static Option random_jump_range("-sample:perturb:range", "-sample::perturb::range", "sets the maximum move range for a Cartesian perturbation mover");

/// Maximum move range for random jump mover of N atoms (in Angstroms)
static Option random_n_jump_range("-sample:n_perturb:range", "-sample::n_perturb::range", "sets the maximum move range for a Cartesian N-residues perturbation mover");
/// Number of atoms moved in a N-bead jump move
static Option random_n_jump_len("-sample:n_perturb:n", "-sample::n_perturb::n", "sets the number of residues (N) for a Cartesian N-residues perturbation mover");

/// size of periodic box
static utils::options::Option box_size("-c", "-sample::box_size", "dimensions of the simulation box (wx,wy,wz - separated by a comma)");

/// density in mol/dm3 - after setting this option, box_size is adjusted
static utils::options::Option density("-sample::d", "-sample::density", "density of simulated system)");
///@}
}
}

#endif
