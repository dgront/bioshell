#include <stdexcept>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>

namespace utils {
namespace options {

bool Option::was_used() const {
  return OptionParser::get().was_used(*this);
}

std::string Option::get_element_name() const {

  std::string val = (name_[0]=='-') ? name_.substr(1) : name_;
  utils::replace_substring(val, "::", ":");

  return val;
}

}
}
