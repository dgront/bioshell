#ifndef UTILS_ProfilingManager_HH
#define UTILS_ProfilingManager_HH

#include <iostream>
#include <memory>
#include <string>
#include <algorithm>
#include <map>

#include <core/index.hh>

#include <utils/Logger.hh>

namespace utils {

class Profiler;

class ProfilingManager {
public:
	static ProfilingManager & get() {
		static ProfilingManager manager;
		return manager;
	}

	std::shared_ptr<Profiler> get_profiler(const std::string & tag) {

    if (profilers_map.find(tag) == profilers_map.end()) {
      std::shared_ptr<Profiler> p = std::make_shared<Profiler>(tag);
      profilers_map[tag] = p;
      logger << utils::LogLevel::INFO << "Creating a profiler named: " << tag <<"\n";
      return p;
    } else return profilers_map[tag];
  }

	void show(std::ostream & outstream);

private:
	static const ProfilingManager manager;
	std::map<std::string,std::shared_ptr<Profiler>> profilers_map;
	ProfilingManager() : logger("ProfilingManager") {}
	ProfilingManager(ProfilingManager const&);
	void operator=(ProfilingManager const&);
  utils::Logger logger;
};

}

#endif
