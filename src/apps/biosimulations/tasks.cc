#include <cstdio>
#include <ctime>
#include <iostream>
#include <memory>
#include <functional>

#include <core/data/io/XML.hh>
#include <core/data/io/XMLElement.hh>

#include <ui/tasks/Protocol.hh>
#include <ui/tasks/ExternalTask.hh>
#include <ui/tasks/Variables.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/Logger.hh>
#include <utils/io_utils.hh>

utils::Logger l("tasks");

int main(const int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace ui::tasks;

  utils::options::Option variables("-D", "-tasks:variables",
      "assign values for variables that are to be used within XML scripts");

  utils::options::OptionParser & cmd = utils::options::OptionParser::get("tasks");
  cmd.register_option(utils::options::help);
  cmd.register_option(utils::options::verbose);
  cmd.register_option(utils::options::input_file,variables);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  if (variables.was_used()) {
    std::string v = utils::options::option_value<std::string>(variables);
    ui::tasks::Variables::get().set(v);
  }
  // ********** Read the XML config **********
  XML tasks_cfg;
  if (input_file.was_used()) tasks_cfg.load_data(option_value<std::string>(input_file));
  l << utils::LogLevel::FILE << "loading protocol from " << option_value<std::string>(input_file) << "\n";

  if (Variables::get().size() > 0) l << utils::LogLevel::INFO << "Variables declared from command line:\n" << Variables::get() << "\n";
  else l << utils::LogLevel::INFO << "No variables declared from command line\n";

  if (tasks_cfg.document_root() == nullptr) l << utils::LogLevel::SEVERE << "No XML script was provided!\n";
  else {
    Protocol x(tasks_cfg.document_root());
    x.run();
  }
}
