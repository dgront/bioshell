#include <iostream>

#include <core/data/structural/Structure.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <utils/io_utils.hh>

#include <simulations/representations/cg_representation.hh>

utils::Logger l("representations");
int main(int argc, const char* argv[]) {

  using namespace utils::options;
  using namespace core::data::structural;

  utils::LogManager::INFO();

  utils::options::OptionParser & cmd = utils::options::OptionParser::get("representations");
  cmd.register_option(help, verbose); // basic options
  cmd.register_option(define_output_representation);
  cmd.register_option(input_pdb,output_name_prefix);  // I/O options

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // ********** INPUT STRUCTURES **********
  std::vector<Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);

  // ********** SET-UP OUTPUT **********
  std::string out_fname;
  if (utils::options::output_name_prefix.was_used())
    out_fname = utils::options::option_value<std::string>(utils::options::output_name_prefix);

  std::map<std::string, std::vector<std::string>> map;
  simulations::representations::load_representation(option_value<std::string>(define_output_representation), map);

  // ********** PROCESS FILES **********
  for(size_t i=0;i<structure_ids.size();++i) {
    std::string fname = utils::basename(structure_ids[i]);
    l << utils::LogLevel::INFO << "Processing " << fname << "\n";
    Structure_SP s = simulations::representations::change_representation(map, *(structures[i]));
    std::ofstream out(out_fname + fname + ".pdb");
    for (auto ai = s->first_atom(); ai != s->last_atom(); ++ai)
      out << (*ai)->to_pdb_line() << "\n";
    out.close();
  }

}
