#include <algorithm>
#include <vector>
#include <iostream>
#include <memory>

#include <core/index.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/output_options.hh>
#include <utils/options/scoring_options.hh>
#include <utils/options/input_utils.hh>

#include <utils/Logger.hh>
#include <utils/io_utils.hh>

#include <core/data/basic/Vec3.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>

#include <simulations/cartesian/ResidueChain.hh>
#include <simulations/systems/surpass/SurpassAtomTyping.hh>
#include <simulations/cartesian/CABSAtomTyping.hh>

#include <simulations/generic/ff/TotalEnergyByResidue.hh>
#include <simulations/generic/evaluators/EvaluatorsSet.hh>
#include <simulations/generic/evaluators/Evaluator.hh>
#include <simulations/generic/evaluators/EchoEvaluator.hh>
#include <simulations/cartesian/evaluators/CrmsdEvaluator.hh>
#include <simulations/representations/surpass_utils.hh>

utils::Logger logs("check_energy");

//static unsigned char cabs_atom_type(const std::string atom_name) {
//
//  if (atom_name.compare(" CA ") == 0) return 0;
//  if (atom_name.compare(" CB ") == 0) return 1;
//  if (atom_name.compare(" SC ") == 0) return 2;
//  if (atom_name.compare(" bb ") == 0) return 3;
//
//  return 255;
//}

using namespace utils::options;
using namespace simulations::generic::ff;
using namespace simulations::cartesian;
using namespace simulations::cartesian::ff;

/** @brief Evaluates SURPASS energy for all models requested at cmd line
 */
void score_surpass(const std::vector<std::string> &ids,
    const std::vector<core::data::structural::Structure_SP> & structures) {

  // --- convert representation of the first pose to surpass and create the system to be scored
  core::data::structural::Structure_SP s_surpass = simulations::representations::surpass_representation(*structures[0]);
  AtomTyping_SP a_typing = std::make_shared<SurpassAtomTyping>();
  ResidueChain<core::data::basic::Vec3> rc(a_typing,*s_surpass);

  // --- Prepare the scoring function
  std::string fname = core::BioShellEnvironment::from_file_or_db("surpass.wghts", "forcefield");
  std::string scfx = utils::load_text_file(fname);
  utils::replace_substring(scfx, "${INPUT_SS2}", get_option<std::string>(input_ss2)); // input_ss2 option provides SS2 data for A13 energy
  std::stringstream ss(scfx);
  std::shared_ptr<TotalEnergyByResidue> en = TotalEnergyByResidue::from_cfg_file(rc, ss);

  // --- Score each model
  auto s_name = ids.begin();
  for (const auto s : structures) {
    core::data::structural::Structure_SP s_surpass = simulations::representations::surpass_representation(*s);
    core::data::structural::Chain_SP chain = s_surpass->get_chain((core::index2) 0);
    chain = s->get_chain((core::index2) 0);
    set_conformation(chain->first_atom(), chain->last_atom(), rc);
    std::cout << (*s_name)<<" "<<*en << "\n";
    ++s_name;
  }
}

/** @brief Evaluates CABS++ energy for all models requested at cmd line
 */
void score_cabs_bb(const std::vector<std::string> &ids,
    const std::vector<core::data::structural::Structure_SP> & structures) {

  // --- convert representation of the first pose to CABS-bb and create the system to be scored
  core::data::structural::Structure_SP s_cabs = structures[0];
  AtomTyping_SP a_typing = std::make_shared<CABSAtomTyping>();
  ResidueChain<core::data::basic::Vec3> rc(a_typing,*s_cabs);

  // --- Prepare the scoring function
  std::string fname =
      (cabs_bb_go.was_used()) ?
          core::BioShellEnvironment::from_file_or_db("cabs_bb_go.wghts", "forcefield") :
          core::BioShellEnvironment::from_file_or_db("cabs_bb.wghts", "forcefield");
  std::string scfx = utils::load_text_file(fname);
  utils::replace_substring(scfx, "${INPUT_SS2}", get_option<std::string>(input_ss2)); // input_ss2 option provides SS2 data for A13 energy

  if (input_pdb_native.was_used()) utils::replace_substring(scfx, "${INPUT_REFERENCE_PDB}",
      get_option<std::string>(input_pdb_native)); // input_pdb_native option provides the reference PDB structure
  else {
    if (cabs_bb_go.was_used()) {
      logs << utils::LogLevel::SEVERE << "A reference structure must be provided to evaluate Go-like energy\n"
          << "\tUse -in::pdb::native option for this purpose\n";
      return;
    }
  }

  std::stringstream ss(scfx);
  std::shared_ptr<TotalEnergyByResidue> en = TotalEnergyByResidue::from_cfg_file(rc, ss);

  // --- Score each model
  auto s_name = ids.begin();
  for (const auto s : structures) {
    core::data::structural::Structure_SP s_cabs = simulations::representations::surpass_representation(*s);
    core::data::structural::Chain_SP chain = s->get_chain((core::index2) 0); // CONVERT HERE!
    set_conformation(chain->first_atom(), chain->last_atom(), rc);
    std::cout << (*s_name) << " " << *en << "\n";
    ++s_name;
  }
}

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace core::data::basic;
  using namespace utils::options;
  using namespace simulations::cartesian;
  using namespace simulations::cartesian::ff;

  utils::options::OptionParser & cmd = OptionParser::get();
  cmd.register_option(utils::options::help);
  cmd.register_option(verbose,mute);
  cmd.register_option( db_path);
  cmd.register_option(input_pdb, input_pdb_native, input_ss2, input_pdb_list);
  cmd.register_option(scfx_config, cabs_bb, cabs_bb_go, surpass); // Scoring ....

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  logs << utils::LogLevel::FINE << "The command line was:\n\t" << cmd.build_cmdline()<<"\n\n";

  // ********** STARTUP SECTION **********
  // ---------- Read the starting structure (PDB format)
  std::vector<std::string> structure_names;
  std::vector<core::data::structural::Structure_SP> structures;
  utils::options::structures_from_cmdline(structure_names, structures);

  // ---------- Energy function from a file - it is assumed that the PDB format matches the scoring system.
//  if (scfx_config.was_used()) {
//    en = simulations::generic::ff::TotalEnergyByResidue::from_cfg_file(rc, get_option<std::string>(scfx_config));
//  }

  // ---------- CABS-bb scoring function: A13, T14, Ramachandran; possibly with a Go-like term
  if ((cabs_bb.was_used())||(cabs_bb_go.was_used())) score_cabs_bb(structure_names,structures);

  // ---------- SURPASS scoring function: R12, R13, R14
  if (surpass.was_used()) score_surpass(structure_names,structures);

}

