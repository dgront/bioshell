#include <string>
#include <chrono>

#include <core/index.hh>
#include <core/data/basic/Vec3.hh>
#include <core/BioShellEnvironment.hh>

#include <utils/string_utils.hh>
#include <utils/LogManager.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>

#include <simulations/movers/MoversSetSweep.hh>
#include <simulations/forcefields/TotalEnergy_OBSOLETE.hh>
#include <simulations/forcefields/TotalEnergyByResidue.hh>
#include <simulations/observers/ObserveEnergyComponents.hh>
#include <simulations/observers/ObserveMoversAcceptance.hh>
#include <simulations/observers/cartesian/PdbObserver_OBSOLETE.hh>
#include <simulations/evaluators/Timer.hh>
#include <simulations/evaluators/EchoEvaluator.hh>
#include <simulations/sampling/SimulatedAnnealing.hh>
#include <simulations/sampling/MetropolisAcceptanceCriterion.hh>
#include <simulations/systems/ResidueChain_OBSOLETE.hh>
#include <simulations/movers/PerturbResidue.hh>
#include <simulations/movers/RotateAASideChains.hh>
#include <simulations/movers/BackrubMover.hh>
#include <simulations/movers/PhiPsiMover.hh>
#include <simulations/movers/RotateAroundBond.hh>
#include <simulations/forcefields/cartesian/RamachandranFF.hh>
#include <simulations/forcefields/mm/MMNonBonded.hh>
#include <simulations/forcefields/mm/MMBondEnergy.hh>
#include <simulations/forcefields/mm/MMPlanarEnergy.hh>
#include <simulations/forcefields/mm/MMDihedralEnergy.hh>
#include <simulations/forcefields/mm/MMBondedParameters.hh>
#include <simulations/observers/ObserveEvaluators.hh>
#include <simulations/evaluators/cartesian/CrmsdEvaluator.hh>
#include <utils/options/sampling_options.hh>

const std::string mobility_info = R"(
      Calculates local dynamics of a given protein structure.)";

int main(const int argc, const char *argv[]) {

  using core::data::basic::Vec3;
  using namespace utils::options; // for all the cmdline options
  using namespace simulations::forcefields::mm; // for all MM - related force field classes
  using namespace core::data::structural; // for Structure, Residue, PdbAtom
  using namespace simulations::systems; // for ResidueChain
  using namespace simulations::evaluators;
  using namespace simulations::evaluators::cartesian;
  using namespace simulations::observers;
  using namespace simulations::observers::cartesian;

  utils::options::OptionParser & cmd = OptionParser::get();
  // ---------- environment etc.
  cmd.register_option(help, markdown_help, verbose, db_path, rnd_seed);
  // ---------- input options
  cmd.register_option(input_pdb, input_pdb_path); // input PDB structures
  // ---------- output options
  cmd.register_option(output_pdb);
  // ---------- sampling options
  cmd.register_option(mc_inner_cycles, mc_outer_cycles, mc_cycle_factor);

  cmd.program_info(mobility_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // --- Read atom typing and bond parameters
  MMAtomTyping_SP atom_typing = std::make_shared<MMAtomTyping>(core::BioShellEnvironment::from_file_or_db("amber03_atoms.par","forcefield/mm/"));
  MMBondedParameters ff(atom_typing);
  ff.read_params_file(core::BioShellEnvironment::from_file_or_db("amber03_ffbonded.itp","forcefield/mm/"));

  // --- Read structure for which calculate bond energy
  core::data::io::Pdb reader(option_value<std::string>(input_pdb)); // file name (PDB format, may be gzip-ped)
  Structure_SP strctr = reader.create_structure(0);
  std::shared_ptr<simulations::systems::ResidueChain<Vec3>> rc;
  rc = std::make_shared<simulations::systems::ResidueChain<Vec3>>(atom_typing, *strctr);

  size_t i = 0;
  for (auto atom_it = strctr->first_const_atom(); atom_it != strctr->last_const_atom(); ++atom_it) {
    auto at = atom_typing->mm_atom_type(**atom_it);
    (*rc)[i].register_ = (1.0 + at.charge()) * 10000.0;
    ++i;
  }

  simulations::forcefields::TotalEnergyByResidue energy;
  // --- Create molecular mechanic bond energy object
  std::shared_ptr<MMBondEnergy<Vec3>> bond_energy = std::make_shared<MMBondEnergy<Vec3>>(*rc, ff);
  energy.add_component(bond_energy,1.0);
  // --- Create molecular mechanic planar energy objects
  energy.add_component(std::make_shared<MMPlanarEnergy<Vec3>>(*rc, ff, *bond_energy),1.0);
  // --- Create molecular mechanic dihedral energy objects
  energy.add_component(std::make_shared<MMDihedralEnergy<Vec3>>(*rc, ff, *bond_energy),1.0);
  // --- Create non-bonded energy
  energy.add_component(std::make_shared<MMNonBonded<Vec3>>(*rc, *bond_energy),1.0);
  // --- Also Ramachandran energy - just for tests
  energy.add_component(std::make_shared<simulations::forcefields::cartesian::RamachandranFF<Vec3>>(*rc, "-"),1.0);

  // --- Read residue types - necessary for side chain sampler
  std::map<std::string, MMResidueType> residue_types;
  simulations::forcefields::mm::load_name_translation(core::BioShellEnvironment::from_file_or_db("translation_rules","forcefield/mm/"));
  simulations::forcefields::mm::read_mm_topology_file(core::BioShellEnvironment::from_file_or_db("amber03_aminoacids.rtp","forcefield/mm/"), residue_types);
  simulations::forcefields::mm::read_mm_topology_file(core::BioShellEnvironment::from_file_or_db("rotable_dihedrals.rtp","forcefield/mm/"), residue_types);

  // --- Create movers
  simulations::movers::MoversSet_SP ms = std::make_shared<simulations::movers::MoversSet>();
  std::shared_ptr<simulations::movers::PerturbResidue<Vec3>> perturb =
    std::make_shared<simulations::movers::PerturbResidue<Vec3>>(*rc,energy);
  perturb->max_move_range(0.01);
  ms->add_mover(perturb,rc->count_residues());

  std::shared_ptr<simulations::movers::RotateAASideChains<Vec3>> rotate_sc =
    std::make_shared<simulations::movers::RotateAASideChains<Vec3>>(*rc,residue_types,ff,energy);
//  ms.add_mover(rotate_sc, rotate_sc->count_dofs());

  std::shared_ptr<simulations::movers::BackrubMover<Vec3>> backrub =
    std::make_shared<simulations::movers::BackrubMover<Vec3>>(*rc, energy," CA ",5);
  ms->add_mover(backrub,rc->count_residues());

  std::shared_ptr<simulations::movers::PhiPsiMover<Vec3>> phipsi =
    std::make_shared<simulations::movers::PhiPsiMover<Vec3>>(*rc, energy);
  phipsi->freeze(true);
  phipsi->freeze(false,{0,1,2,53,54,55});
  ms->add_mover(phipsi,phipsi->count_dofs());

  // --- Create observer for energy components and movers
  std::shared_ptr<simulations::observers::ObserveEnergyComponents<simulations::forcefields::ByResidueEnergy>> obs_en =
    std::make_shared<simulations::observers::ObserveEnergyComponents<simulations::forcefields::ByResidueEnergy>>(energy,"energy.dat");
  obs_en->observe_header();
  simulations::observers::ObserveMoversAcceptance_SP obs_ms =
    std::make_shared<simulations::observers::ObserveMoversAcceptance>(*ms,"movers.dat");
  obs_ms->observe_header();

  // --- Observe systems trajectory and save it to a PDB file
  auto traf = std::make_shared<PdbObserver<Vec3>>(*rc, *strctr,"tra.pdb");

  // --- Observe also some properties
  simulations::observers::ObserveEvaluators_SP stats = std::make_shared<simulations::observers::ObserveEvaluators>("out.dat");
  std::shared_ptr<simulations::evaluators::Timer> timer = std::make_shared<simulations::evaluators::Timer>();
  stats->add_evaluator(timer);
  std::shared_ptr<CrmsdEvaluator<Vec3>> rms =
    std::make_shared<CrmsdEvaluator<Vec3>>(strctr, *rc);
  stats->add_evaluator(rms);
  stats->observe_header();
  
  // --- Create a sampler
  simulations::sampling::SimulatedAnnealing sampler(ms,{0.58});
  sampler.cycles(option_value<core::index2>(mc_inner_cycles, 10), option_value<core::index2>(mc_outer_cycles, 1000));
  sampler.outer_cycle_observer(obs_en);
  sampler.outer_cycle_observer(obs_ms);
  sampler.outer_cycle_observer(stats);
  sampler.outer_cycle_observer(traf);

  sampler.run();
  write_pdb_conformation(*rc,*strctr, "final.pdb");
}
