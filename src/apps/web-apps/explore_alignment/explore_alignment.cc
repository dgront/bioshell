#include <stdlib.h>
#include <stdio.h>

#include <thread>
#include <mutex>
#include <string>
#include <memory>

#include <core/alignment/PairwiseAlignment.hh>
#include <core/alignment/PairwiseSequenceAlignment.hh>

#include <core/data/io/pir_io.hh>
#include <core/data/io/fasta_io.hh>
#include <core/data/sequence/sequence_utils.hh>

#include <utils/Logger.hh>
#include <utils/io_utils.hh>
#include <ui/www/web_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/align_options.hh>
#include <utils/options/input_options.hh>

#include <external/web++.hh>
#include "../../../ui/www/serverside/SequenceAlignmentComponent.hh"

using namespace WPP;

namespace apps {
namespace web_apps {

utils::Logger log_t1d("AlignmentWidget");

// ------ names of files used by the apps, e.g. static HTML pages and templates
std::string submit_job_html_file = "threading1D-submit.html";

// ----- keys used in GET requests
std::string input_alignment_key = "alignment";
std::string input_alignment_format_key = "format";
std::string show_key = "show";
std::string email_key = "email";
std::string queue_key = "queue";
std::string job_key = "job";

std::shared_ptr<ui::www::serverside::SequenceAlignmentWidget> alignment = nullptr;

void front_end(Request* req, Response* res) {

  // Nothing in query => send start page
  if (req->query.size() == 0) {
    res->body << "";
    return;
  }

  std::string in_ali = utils::get_query_value(req, input_alignment_key);
  std::string in_ali_format = utils::get_query_value(req, input_alignment_format_key);

  // Create alignment object
  if (in_ali.size() > 0) {
    return;
  }

  // If don't have any alignment, further dispatch makes no sense
  if (alignment == nullptr) {
    res->body << "";
    return;
  }

}

}
}

int main(const int argc, const char* argv[]) {

  using namespace utils::options;

  utils::options::OptionParser & cmd = OptionParser::get();
  cmd.register_option(utils::options::help);
  cmd.register_option(verbose, db_path);
  cmd.register_option(input_alignment_pir, input_alignment_fasta, input_clustalw);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // ********** START WEBSERVER **********
  try {
    std::cerr << "Listening on port 5000" << "\n";

    WPP::WebServer server;
    server.all("/explore_alignment", &apps::web_apps::front_end);
    server.all("/css", "./bootstrap/css/");
    server.start(5000);
  } catch (WPP::Exception & e) {
    std::cerr << "WebServer: " << e.what() << "\n";
  }
}

