#include <core/BioShellVersion.hh>
#include <core/data/basic/Vec3I.hh>
#include <core/data/structural/Chain.hh>

#include <simulations/systems/PdbAtomTyping.hh>

#include <simulations/forcefields/TotalEnergyByAtom.hh>
#include <simulations/forcefields/cabs/CabsR13.hh>
#include <simulations/forcefields/cabs/CabsR14.hh>
#include <simulations/forcefields/cabs/CabsR15.hh>
#include <simulations/forcefields/cartesian/CAHydrogenBond.hh>
#include <simulations/forcefields/mf/R12.hh>
#include <simulations/movers/MoversSetSweep.hh>
#include <simulations/sampling/SimulatedAnnealing.hh>
#include <simulations/observers/cartesian/PdbObserver.hh>
#include <utils/LogManager.hh>
#include <simulations/observers/ObserveEnergyComponents.hh>
#include <simulations/forcefields/cartesian/ContactEnergy.hh>
#include <simulations/movers/PerturbChainFragment.hh>
#include <simulations/movers/TailBeadMove.hh>
#include <simulations/observers/ToHistogramObserver.hh>

#include <simulations/systems/BuildPolymerChain.hh>
#include <simulations/movers/TranslateAtom.hh>
#include <simulations/observers/ObserveEvaluators.hh>
#include <simulations/evaluators/CallEvaluator.hh>
#include <simulations/evaluators/Timer.hh>

#include <simulations/observers/cartesian/ExplicitPdbFormatter.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/scoring_options.hh>
#include <utils/options/sampling_options.hh>
#include <utils/options/model_options.hh>
#include <utils/options/sampling_from_cmdline.hh>
#include <core/data/io/fasta_io.hh>
#include <core/data/io/ss2_io.hh>
#include <simulations/observers/AdjustMoversAcceptance.hh>
#include <simulations/systems/surpass/SurpassAlfaAtomTyping.hh>
#include <simulations/movers/SwingChainFragment.hh>

using namespace core::data::basic;
using namespace simulations;
using namespace simulations::systems;
using namespace simulations::forcefields;
using namespace simulations::movers;
using namespace core::data::structural;
using namespace core::data::io;
using namespace core::data::sequence;
using namespace simulations::observers::cartesian; // for all observers

using core::data::basic::Vec3I;

utils::Logger logs("ca_only_model");


int main(const int argc, const char *argv[]) {
    using namespace utils::options; // --- All the options are in this namespace

    utils::LogManager::INFO();
    logs << utils::LogLevel::INFO << "BioShell version:\n" << core::BioShellVersion().to_string() << "\n";

    utils::options::OptionParser &cmd = utils::options::OptionParser::get();
    cmd.register_option(utils::options::help, verbose, db_path, rnd_seed);
    cmd.register_option(mc_outer_cycles(100), mc_inner_cycles(100), mc_cycle_factor(10), box_size(100.0));
    cmd.register_option(random_jump_range(1.0), random_n_jump_range(1.0), random_n_jump_len(5));
    cmd.register_option(staring_model_pdb, staring_model_random, input_pdb_native, input_ss2, scfx_config);  // Input options
    cmd.register_option(input_pdb,input_fasta_ss, n_chains);  // More input options
    cmd.register_option(output_pdb, output_pdb_min, output_pdb_min_fraction, output_pdb_min_value); // output options
    cmd.register_option(begin_temperature(5), end_temperature(2), temp_steps(4),density);

    if (!cmd.parse_cmdline(argc, argv)) return 1;

    simulations::SimulationSettings settings;
    settings.insert_or_assign(cmd, false);
    std::string sequence = "", secondary = "";
    core::data::structural::Structure_SP strctr;
    core::data::sequence::SecondaryStructure_SP sec_str;
    std::string chain_codes = "ABCDEFGHIJKLMNOPQRSTUWXYZ";

    // ---------- Here we create a system to be modelled
    if (settings.has_key(staring_model_pdb)) {
        strctr = core::data::io::Pdb(option_value<std::string>(staring_model_pdb),
            core::data::io::is_ca, core::data::io::only_ss_from_header, true).create_structure(0);
        for (auto chain: *strctr) {      // --- combine sequence from all chains
            sec_str = chain->create_sequence();
            sequence += sec_str->sequence;
            secondary += sec_str->str();
        }
        if(settings.has_key(input_ss2)) { // ---------- Read SS2 format
            std::string input_ss2_file = settings.get<std::string>(input_ss2);
            sec_str = core::data::io::read_ss2(input_ss2_file);
            secondary = sec_str->str();
            sequence = sec_str->sequence;
        }
    } else if(settings.has_key(input_ss2)) { // ---------- Read SS2 format
        std::string input_ss2_file = settings.get<std::string>(input_ss2);
        sec_str = core::data::io::read_ss2(input_ss2_file);
        secondary = sec_str->str();
        sequence = sec_str->sequence;
        strctr = std::make_shared<core::data::structural::Structure>("");
        core::index2 nc = settings.get<core::index2>(n_chains, 1);
        for (auto i=0;i < nc;++i ) {
            Chain_SP peptide = Chain::create_ca_chain(sequence, std::string{chain_codes[i]});
            strctr->push_back(peptide);
        }
        for (auto i=1;i < nc;++i ) {
            secondary += sec_str->str();
            sequence += sec_str->sequence;
        }
    } else if (settings.has_key(input_fasta_ss)) { // ---------- Read SS fasta format
        const std::string input_ss_file = settings.get<std::string>(input_fasta_ss);
        sec_str = core::data::io::read_fasta_ss_file(input_ss_file);
        secondary = sec_str->str();
        sequence = (sec_str->sequence.size() == 0) ? std::string('A', sec_str->str().size()) : sec_str->sequence;
        logs << utils::LogLevel::INFO << "Secondary structure read from file:" << secondary << "\n";
        strctr = std::make_shared<core::data::structural::Structure>("");
        core::index2 nc = settings.get<core::index2>(n_chains, 1);
        for (auto i = 0; i < nc; ++i) {
            core::data::structural::Chain_SP peptide =
                core::data::structural::Chain::create_ca_chain(sec_str->sequence, std::string{chain_codes[i]});
            strctr->push_back(peptide);
        }
        for (auto i=1;i < nc;++i ) {
            secondary += sec_str->str();
            sequence += sec_str->sequence;
        }
    }
    else { logs << utils::LogLevel::SEVERE << "No input file. Provide pdb, SS2 or fasta_ss file.\n";
        exit(1);
    }

    double new_box_size;
    if (settings.has_key(density)) {
        double density_ = settings.get<double>(density);
        // part 10000/6.02 is connected with unit recalculations, from mol/dm3 to 1/A^3 so it should be 10^(27)/(6.02*10^(23)) but here it is simplyfied
        double box_size_cube = strctr->count_chains() / density_ * 10000 / 6.02;
        new_box_size = pow(box_size_cube, 1.0 / 3.0); //cube sqrt
        Vec3I::set_box_len(new_box_size);
        logs << utils::LogLevel::INFO << "Density set to: " << density_ << ". Box size set to " << new_box_size << "\n";
    }
    std::vector<std::string> ca_vec( {" CA "});
    AtomTypingInterface_SP ca_type = std::make_shared<simulations::systems::SimpleAtomTyping>(simulations::systems::list_3codes_aminoacids(), ca_vec,ca_vec);  // atom typing is used to assign FF parameters to each atom
    CartesianChains chains(ca_type, *strctr);
    if (settings.has_key(staring_model_random)) {
        BuildPolymerChain chain_builder(chains);
        bool a = chain_builder.generate(3.8, 5.5);
    }

    CartesianChains chains_backup(chains);           // --- make a backup system

    // ---------- Energy function: container for the total
    auto total_en = std::make_shared<simulations::forcefields::TotalEnergyByAtom>();
    // ---------- R12 i.e. pseudobonds
    auto r12 = std::make_shared<mf::R12>(chains);
    total_en->add_component(r12, 1.0);
    // ---------- R13
    auto r13 = std::make_shared<cabs::CabsR13>(chains, sequence, secondary);
    total_en->add_component(r13, 1.0);
    // ---------- R14
    auto r14 = std::make_shared<cabs::CabsR14>(chains, sequence, secondary);
    total_en->add_component(r14, 1.0);
    // ---------- R15
    auto r15 = std::make_shared<cabs::CabsR15>(chains, sequence, secondary);
    total_en->add_component(r15, 1.0);
    // ---------- Contact energy
    auto contact = std::make_shared<cartesian::ContactEnergy>(chains, 100, -0.1, 3.5, 4, 8);
    total_en->add_component(contact, 1.0);
    // ---------- Contact energy
    auto ca_hb = std::make_shared<cartesian::CAHydrogenBond>(chains, secondary,"HE");
    total_en->add_component(ca_hb, 10.0);

    // ---------- Movers
    auto moves = std::make_shared<MoversSetSweep>();
    std::shared_ptr<TranslateAtom> translate = std::make_shared<TranslateAtom>(chains, chains_backup, *total_en);
  translate->max_move_range(0.8);

    auto perturb_frag = std::make_shared<PerturbChainFragment>(chains, chains_backup, 6, *total_en);
  perturb_frag->max_move_range(0.9);
  auto tail_move = std::make_shared<TailBeadMove>(chains,chains_backup,*total_en, 1);
    auto swing_chain = std::make_shared<SwingChainFragment>(chains, chains_backup, *total_en);
    swing_chain->max_move_range(0.3);


    moves->add_mover(translate, sequence.length());
 //   moves->add_mover(perturb_frag, sequence.length());
 //   moves->add_mover( swing_chain, sec_str->length() );
 //   moves->add_mover( tail_move,sequence.length());
    core::calc::statistics::Random::seed(12345);
    // ---------- Sampler
    simulations::sampling::SimulatedAnnealing sa(moves);

    // ---------- Observers
    std::shared_ptr<AbstractPdbFormatter> fmt = std::make_shared<ExplicitPdbFormatter>(*strctr);
    auto trajectory = std::make_shared<simulations::observers::cartesian::PdbObserver>(chains, fmt, "trajectory.pdb");
    sa.outer_cycle_observer(trajectory);

    typedef typename std::function<double(void)> ObservedFunctor;
    ObservedFunctor recent_energy = ([total_en, &chains]() { return total_en->energy(chains); });
    ObservedFunctor recent_temp = ([&sa]() { return sa.temperature(); });

    // ---------- Observerve evaluators: energy, r-end, cm
    auto obs_evaluators = std::make_shared<simulations::observers::ObserveEvaluators>("evaluators.dat");
    auto evaluate_energy_sp = std::make_shared<evaluators::CallEvaluator<ObservedFunctor>>(recent_energy, "energy");
    auto evaluate_temp_sp = std::make_shared<evaluators::CallEvaluator<ObservedFunctor>>(recent_temp, "temperature");
    auto timer = std::make_shared<evaluators::Timer>();
    obs_evaluators->add_evaluator(evaluate_energy_sp);
    obs_evaluators->add_evaluator(evaluate_temp_sp);
    obs_evaluators->add_evaluator(timer);
    sa.outer_cycle_observer(obs_evaluators);

    // ---------- make energy histogram as well
    auto en_hist = std::make_shared<observers::ToHistogramObserver>(evaluate_energy_sp, -1000, 0, 1,"energy_hist.dat");
    sa.inner_cycle_observer(en_hist);

    // ---------- Record starting conformation
    simulations::observers::cartesian::PdbObserver start(chains, fmt, "start.pdb");
    start.observe();

    // --- Create observer for energy components and movers
    auto obs_en = std::make_shared<observers::ObserveEnergyComponents>(*total_en, chains, "energy.dat", true);
    sa.outer_cycle_observer(obs_en);

//    obs->observe_header();
//  obs->observe();
    std::shared_ptr<simulations::observers::AdjustMoversAcceptance> observe_moves
            = std::make_shared<simulations::observers::AdjustMoversAcceptance>(*moves,"movers.dat", 0.4);

    sa.outer_cycle_observer(observe_moves);

    // --- RUN !
    sa.reset(settings);
    sa.run();
    trajectory->finalize();
    obs_evaluators->finalize();
    en_hist->finalize();

    simulations::observers::cartesian::PdbObserver final(chains, fmt, "final.pdb");
    final.observe();

}
