#include <iostream>

#include <utils/io_utils.hh>
#include <utils/string_utils.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <simulations/representations/surpass_utils.hh>
#include <core/data/basic/Vec3.hh>

using namespace core::data::structural;
using namespace core::data::io;
using namespace core::data::basic;

PdbAtom_SP atom_sp;
Residue_SP residue_sp, res;
Chain_SP ch_sp;
Structure_SP structure_sp;
core::index2 ch_size, id_;


int main(const int argc, const char* argv[]) {
    core::data::io::Pdb reader(argv[1], is_not_alternative, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);

    structure_sp = simulations::representations::surpass_representation(*strctr);

// Iterate over all chains
    for (auto it_chain = structure_sp->begin(); it_chain!=structure_sp->end(); ++it_chain) {
      ch_sp = (*it_chain);

// Calculate distance between each H-type atom and n+8
      ch_size = ch_sp->Chain::count_residues();
      std::fstream plik("distance_R19", std::ios::app);

//Checks if helix is longer then 8
    int longer_then_8(auto chain, auto start){
      is_longer = true
      for (core::index2 i=0;i<=8;i++) {
        if (chain[start+i] != 'H') {
          is_longer = false;
          break;
          }
        }
      return is_longer
      }

      for(core::index2 z=1; z<ch_size; ++z) {                                     //for each SG atom...
        res = (*ch_sp)[z];
        id_ = res->id();
        PdbAtom_SP it_SG = res->find_atom(" SG ");
    //    plik0 <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" ";

              if( (res->ss()=='H')&&(longer_then_8(*ch_sp,z)) ) {                                                        //distance to atom n+8
                PdbAtom_SP SG_r9 = (ch_sp->get_residue(id_+8))->find_atom(" SG ");
                double dist_r9 = it_SG->distance_to(*SG_r9);
                plik <<"# "<<dist_r9<<" ";
                }

              plik <<"\n";
      }
    plik.close();
    }
}