#include <iostream>

#include <core/index.hh>
#include <core/data/basic/Vec3Cubic.hh>
#include <core/BioShellEnvironment.hh>
#include <core/data/io/Pdb.hh>
#include <utils/LogManager.hh>
#include <utils/exit.hh>

#include <simulations/forcefields/surpass/SurpassContactEnergy.hh>
#include <simulations/forcefields/surpass/SurpassNeighborList.hh>
#include <simulations/systems/surpass/SurpassModel.hh>

using core::data::basic::Vec3Cubic;
using namespace simulations::systems::surpass;
using namespace simulations::forcefields::surpass;

std::string program_info = R"(

Evaluates SURPASS contact energy terms for a given PDB model.

This program reads a SURPASS structure (PDB file format) and calculates its contact energy

USAGE:
    ./find_closest_contact test_inputs/2gb1.pdb

)";

/** @brief Reads an all-atom structure from a PDB file and calculates its contact energy
 *
 * CATEGORIES: simulations/forcefields/surpass/SurpassContactEnergy
 * KEYWORDS:   SURPASS
 */
int main(const int argc, const char* argv[]) {

    if(argc < 2) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

    utils::LogManager::INFO();

    // --- Read the input PDB and create a structure object
    core::data::basic::Vec3Cubic::set_box_len(100.0);
    core::data::io::PdbLineFilter filt1 = core::data::io::all_true(core::data::io::is_ca, core::data::io::is_standard_atom,core::data::io::is_not_alternative);
    core::data::io::PdbLineFilter filt2 = core::data::io::all_true(core::data::io::is_ca, core::data::io::is_hetero_atom,core::data::io::is_not_alternative);
    core::data::io::PdbLineFilter filt3 = core::data::io::one_true(filt1, filt2);
//core::data::io::PdbLineFilter filt = core::data::io::all_true(core::data::io::is_ca, core::data::io::is_standard_atom,core::data::io::is_not_alternative);
    core::data::io::Pdb reader(argv[1], filt3 , true);
    auto strc = reader.create_structure(0);
    std::shared_ptr<SurpassModel<Vec3Cubic>> model = std::make_shared<SurpassModel<Vec3Cubic>>(*strc);
   // std::cout<<"nres "<<model->count_residues()<<"/n";
    model->center_at(50, 50, 50);

    std::vector<double> results_by_kernel(model->count_residues(), 0.0);
    std::vector<double> results_by_nblist(model->count_residues(), 0.0);

    std::cout << "#   i   j surp_dist d01  d02  d03  d12  d13  d23\n";

    auto chain=(*strc)[0];
    for (core::index2 i = 0; i < model->count_residues(); ++i) {
        for (core::index2 j = 1; j < model->count_residues(); ++j) {
            double surp_dist = (*model)[i].distance_to((*model)[j]);
            if (surp_dist > 12) {
                double d11 = (*chain)[i]->find_atom("CB")->distance_to(*(*chain)[j]->find_atom("CB"));
                double d12 = (*chain)[i]->find_atom("CB")->distance_to(*(*chain)[j+1]->find_atom("CB"));
                double d13 = (*chain)[i]->find_atom("CB")->distance_to(*(*chain)[j+2]->find_atom("CB"));
                double d14 = (*chain)[i]->find_atom("CB")->distance_to(*(*chain)[j+3]->find_atom("CB"));

                double d21 = (*chain)[i + 1]->find_atom("CB")->distance_to(*(*chain)[j]->find_atom("CB"));
                double d22 = (*chain)[i + 1]->find_atom("CB")->distance_to(*(*chain)[j+1]->find_atom("CB"));
                double d23 = (*chain)[i + 1]->find_atom("CB")->distance_to(*(*chain)[j+2]->find_atom("CB"));
                double d24= (*chain)[i + 1]->find_atom("CB")->distance_to(*(*chain)[j+3]->find_atom("CB"));

                double d31 = (*chain)[i + 2]->find_atom("CB")->distance_to(*(*chain)[j]->find_atom("CB"));
                double d32 = (*chain)[i + 2]->find_atom("CB")->distance_to(*(*chain)[j+1]->find_atom("CB"));
                double d33 = (*chain)[i + 2]->find_atom("CB")->distance_to(*(*chain)[j+2]->find_atom("CB"));
                double d34 = (*chain)[i + 2]->find_atom("CB")->distance_to(*(*chain)[j+3]->find_atom("CB"));

                double d41 = (*chain)[i + 3]->find_atom("CB")->distance_to(*(*chain)[j]->find_atom("CB"));
                double d42 = (*chain)[i + 3]->find_atom("CB")->distance_to(*(*chain)[j+1]->find_atom("CB"));
                double d43 = (*chain)[i + 3]->find_atom("CB")->distance_to(*(*chain)[j+2]->find_atom("CB"));
                double d44 = (*chain)[i + 3]->find_atom("CB")->distance_to(*(*chain)[j+3]->find_atom("CB"));

                std::cout << utils::string_format("%4d %4d %7.3f %7.2f %7.2f  %7.2f  %7.2f  %7.2f  %7.2f %7.2f  %7.2f  %7.2f  %7.2f  %7.2f  %7.2f %7.2f %7.2f  %7.2f  %7.2f \n", i, j,
                                                  surp_dist, d11,d12,d13,d14,d21,d22,d23,d24,d31,d32,d33,d34,d41,d42,d43,d44);
            }
        }
    }
    std::cout << "#   i   j i_type j_type   angle distance iss_len jss_len\n";


    double en = 0.0,  en_sum = 0.0;
    for (core::index2 i = 1; i < model->alfa_ranges().size(); i=i+2) {
        for (core::index2 ai = model->alfa_ranges()[i-1]; ai <= model->alfa_ranges()[i]; ai++) {
            double min_distance=100.0,the_angle,angle_in_degree;
            core::index2 other=0;
        for (core::index2 j = 1; j < model->alfa_ranges().size(); j=j+2) {
            for (core::index2 aj = model->alfa_ranges()[j - 1]; aj <= model->alfa_ranges()[j]; aj++) {

                if ((model->ss_element_for_atoms()[ai] != model->ss_element_for_atoms()[aj])) {
                    double d = ((*model)[ai]).distance_to((*model)[aj]);
                    if (d < min_distance) {
                        min_distance = d;
                        other = aj;
                    }
                }
            }}
                core::index2 i_ss_len = 0;
                core::index2 other_ss_len = 0;
                if (other == 0) {
                    min_distance = 0;
                    the_angle = 0;
                } else {
                    Vec3 vec1 = (*model)[ai + 1] - (*model)[ai - 1];
                    Vec3 vec2 = (*model)[other + 1] - (*model)[other - 1];

                    vec1.norm();
                    vec2.norm();
                    the_angle = vec1.dot_product(vec2);
                    angle_in_degree=acos(the_angle)*180/3.14;

                }
                std::cout << utils::string_format("%4d %4d %6d %6d %7.3f %8.3f %4d %4d\n", ai, other,
                                                  ((*model)[ai]).atom_type, ((*model)[other]).atom_type, angle_in_degree,
                                                  min_distance, i_ss_len, other_ss_len);
            }
        }
}

