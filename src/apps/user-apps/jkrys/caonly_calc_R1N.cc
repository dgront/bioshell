#include <iostream>
#include <utility>

#include <utils/io_utils.hh>
#include <utils/string_utils.hh>
#include <core/algorithms/basic_algorithms.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>
//#include <simulations/representations/surpass_utils.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/basic/Vec3Cubic.hh>
#include <utils/exit.hh>

using namespace core::data::structural;
using namespace core::data::io;
using namespace core::data::basic;

struct SameSS {

  bool operator()(Residue_SP residue_i, Residue_SP residue_j) {
    return (residue_j->ss() == residue_i->ss() );
  }
};

int main(const int argc, char* argv[]) {

  PdbAtom_SP atom_sp;
  Residue_SP residue_sp, res;
  Chain_SP ch_sp;
  Structure_SP structure_sp;
  //core::index2 ch_size;
  std::string chain_id;
  core::index2 N;
  char* ss_code;

  if (argc<5){
    std::cout<<"Some arguments missing!\n[USING] surpass_calc_R1N pdb_file chain_id N ss_code\n";
    return 1;
  }
  else{
    chain_id = argv[2];
    N = atoi(argv[3]);
    ss_code = argv[4];
  };


  core::data::io::Pdb reader(argv[1], is_not_alternative, only_ss_from_header, true);
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

  selectors::SelectChainBreaks sel;
  for (const auto &chain : *strctr) {
  //std::cerr << "# Processing " << strctr->code() << " chain " << chain->id() << "\n";
    if (chain->id() == chain_id){
      size_t first_aa = 0;
      while ((*chain)[first_aa]->residue_type().type != 'P') ++first_aa;
      const auto last_it = chain->terminal_residue() - 1;
      for (auto res_it = chain->begin() + first_aa + 1; res_it != last_it; ++res_it) {
        auto next = (**res_it).next();
        if (sel(*res_it)) {
          //std::cout <<"BROKEN "<<argv[1]<<" "<<chain_id<<"\n";
          return 1;
        }
      }
      //std::cout<<"OK "<<argv[1]<<" "<<chain_id<<"\n";
    }
  }
    core::index2 i_resid = 0;
      for (Chain_SP ci : *strctr)
        std::for_each(ci->begin(), ci->end(), [&](Residue_SP e) {(e)->id(++i_resid);});

  //Part to generate PDB
  //for(auto atom_it = structure_sp->first_atom(); atom_it!=structure_sp->last_atom(); ++atom_it)
  //  std::cout << (*atom_it)->to_pdb_line()<<"\n";
  
// Iterate over all chains
  for (int ic = 0; ic < strctr->size(); ++ic) {
    ch_sp = (*strctr)[ic];
    if (ch_sp->id() == chain_id){
      std::vector<std::pair<int, int>> islands;
      core::algorithms::consecutive_find(ch_sp->begin(), ch_sp->end(), SameSS(), islands);
      for (const auto island : islands) {
        if ((*ch_sp)[island.first]->ss() == *ss_code)
          //std::cout << ch_sp->id()<< " "<<island.first << " " << island.second << "\n";          
          for (core::index2 i = island.first; i <= island.second-N+1; ++i) {
            //std::cout << ch_sp->id()<< " "<<island.first << " " << island.second << " " << i << "\n";
            
            if (N==4){
              PdbAtom_SP it_1 = (*(*ch_sp)[i]).find_atom(" CA ");
              PdbAtom_SP it_2 = (*(*ch_sp)[i + 1]).find_atom(" CA ");
              PdbAtom_SP it_3 = (*(*ch_sp)[i + 2]).find_atom(" CA ");
              PdbAtom_SP it_4 = (*(*ch_sp)[i + 3]).find_atom(" CA ");
              
              double dist_rN = core::data::basic::r14x(*it_1, *it_2, *it_3, *it_4);
              std::cout << dist_rN << " "<<(*strctr).code()<<" "<<chain_id<<"\n";
            }
            else{
              PdbAtom_SP it_1 = (*(*ch_sp)[i]).find_atom(" CA ");
              PdbAtom_SP it_N = (*(*ch_sp)[i + N-1]).find_atom(" CA ");

              double dist_rN = it_1->distance_to(*it_N);
              std::cout << dist_rN << " "<<(*strctr).code()<<" "<<chain_id<<"\n";
          }
            
            //<<island.first<<"-"<<island.second<<" "<<i<<" "<<i+N-1<<" "<<(*(*(*strctr)[ic])[i]).id()<<"-"<<(*(*(*strctr)[ic])[i+N-1]).id()<<"\n";
        }
      }
    }
    else{
      //std::cout<<"Skipping "<<ch_sp->id()<<" chain\n";
    }    
    }
}