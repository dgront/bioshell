#include <ostream>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/interactions/BackboneHBondMap.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>

#include <simulations/systems/surpass/SurpassAlfaChains.hh>
#include <core/protocols/selection_protocols.hh>

#include <utils/io_utils.hh>
#include <utils/Logger.hh>


utils::Logger l("contact_energy");

const std::string program_info = R"(
      Calculates the contact energy in the SurpassAlfa model for a given PDB file.)";

/// Returns true if a residue has neighbors on each side and all the three are 'E'
bool is_residue_OK(core::data::structural::Residue_SP res) {
  core::data::structural::Residue_SP prev = (*res).previous();
  core::data::structural::Residue_SP next = (*res).next();
  if(prev== nullptr || next == nullptr) return false;
  if(prev->ss()!='E' || next->ss()!='E' || (*res).ss() != 'E') return false;

  if (prev->find_atom(" CA ", nullptr) == nullptr) return false;
  if (next->find_atom(" CA ", nullptr) == nullptr) return false;
  if (res->find_atom(" CA ", nullptr) == nullptr) return false;
  return true;
}

struct HBond {
  core::index2 i_pos;
  core::index2 j_pos;
  float energy;

  bool operator<(const HBond & lhs) const { return energy < lhs.energy; }
};

std::ostream & operator<<(std::ostream & out, const HBond & hb) {
  out << utils::string_format("%3d %6.6f", hb.j_pos, hb.energy);

  return out;
}

double calc_dot_product(core::data::structural::Structure_SP s,int i,int j, double & angle_i, double & angle_j){
    auto i_ca = *(*(*s)[0])[i]->find_atom_safe(" CA ");
    auto i_prev = *(*(*s)[0])[i-1]->find_atom_safe(" CA ");
    auto i_next = *(*(*s)[0])[i+1]->find_atom_safe(" CA ");
    auto j_ca = *(*(*s)[0])[j]->find_atom_safe(" CA ");
    auto j_prev = *(*(*s)[0])[j-1]->find_atom_safe(" CA ");
    auto j_next = *(*(*s)[0])[j+1]->find_atom_safe(" CA ");
    auto i_rt_sp = core::calc::structural::transformations::local_coordinates_three_atoms(i_prev,i_ca,i_next);
    auto j_rt_sp = core::calc::structural::transformations::local_coordinates_three_atoms(j_prev,j_ca,j_next);
  core::data::structural::Vec3 tmp;
  i_rt_sp->apply(j_ca, tmp);
  angle_j = atan2(tmp.z, tmp.y);
  j_rt_sp->apply(i_ca, tmp);
  angle_i = atan2(tmp.z, tmp.y);

  return i_rt_sp->rot_z().dot_product(j_rt_sp->rot_z());

}

void collect_contacts(core::data::structural::Structure_SP s, simulations::systems::surpass::SurpassAlfaChains& surpass) {
    using namespace core::data::structural;

    std::vector<std::string> backbone({ " N  ", " C  ", " O  "});
    std::vector<double> d_val,angle_val;
    for (auto i_res_it = s->first_residue(); i_res_it != s->last_residue(); ++i_res_it) {
        if (!is_residue_OK(*i_res_it)) continue;
        for (auto it = (**i_res_it).begin(); it != (**i_res_it).cend(); ++it) {
            if (std::find(backbone.begin(), backbone.end(), (*it)->atom_name()) != backbone.end())
                (**i_res_it).erase(it);
        }
    }
    double min_val,angle;
    double angle_i, angle_j;
    std::string seq1("...."), seq2("....");
    for (int i=1;i<surpass.n_surpass_atoms-1;++i) {
      //std::cout<<"SIZE "<<d_val.size()<<"\n";
        for (int j=i+4;j<surpass.n_surpass_atoms-1;++j) {
            if (surpass.surpass_atoms()[i].residue_type==surpass.surpass_atoms()[j].residue_type ||
            surpass.surpass_atoms()[i].atom_type==2 || surpass.surpass_atoms()[j].atom_type==2) continue;
            d_val.clear();
            angle_val.clear();
            min_val=1000;
            for (int ii=1;ii<3;++ii) {
                seq1[ii]=(*((*s)[0]))[i + ii]->residue_type().code1;
                for (int jj = 1; jj < 3; ++jj) {
                    double dp = calc_dot_product(s,i+ii,j+jj,angle_i, angle_j);
                    angle_val.push_back(dp);
                    seq2[jj]=(*((*s)[0]))[j + jj]->residue_type().code1;
                    d_val.push_back((*((*s)[0]))[i + ii]->min_distance((*((*s)[0]))[j + jj]));
                    if (d_val[d_val.size() - 1] < min_val) {
                        min_val = d_val[d_val.size() - 1];
                        angle=angle_val[d_val.size() - 1];
                    }
                  std::cerr << utils::string_format("%3d %3d %s %s %5.2f %1d %1d %7.3f %7.3f  %7.4f\n",
                      i+ii,j+jj,seq1.c_str(), seq2.c_str(),d_val.back(), ii, jj, angle_i*180/3.1415, angle_j*180/3.1415, dp);

                }
            }
            if (min_val<4.5) {
                std::cout << i << " " << j << " " <<surpass.surpass_ss2()[i]<<surpass.surpass_ss2()[j]<<" "<<seq1<<" "<<seq2<<" "
                << surpass.surpass_atoms()[i].distance_to(surpass.surpass_atoms()[j])<<" "<<min_val<<" "<<angle<< "\n";
                std::cout<<"distance ";
                for (auto a:d_val) std::cout<<a<<" ";
                std::cout<<"\n";
                std::cout<<"angle ";
                for (auto a:angle_val) std::cout<<a<<" ";
                std::cout<<"\n";
            }
        }
    }
}

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace core::data::structural;
  using namespace utils::options;
  using namespace simulations::systems::surpass;

  utils::options::OptionParser & cmd = OptionParser::get("ca_hb_energy");
  cmd.register_option(utils::options::help, show_examples, verbose,db_path);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header); // input PDB structures

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate energy for a give PDB file:", "contact_energy -in:pdb=2gb1.pdb");
  cmd.program_info(program_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures_ca;
    std::vector<core::data::structural::Structure_SP> structures;

  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);
    utils::options::structures_from_cmdline(structure_ids, structures_ca);

  for (auto s:structures_ca) core::protocols::keep_selected_atoms(core::data::structural::selectors::IsCA(), *s);

    std::vector<std::shared_ptr<simulations::systems::surpass::SurpassAlfaChains>> surpass_models;

  // ---------- Process input structures
  std::vector<HBond> h_bonds;
  std::map<core::index2, std::vector<HBond>> h_bonds_by_pos;
  core::index4 struc_cnt=0;
  for (auto s : structures_ca) {
    l << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for(auto ci:*s) ci->remove_ligands();
    SurpassAlfaChains model(*s);
    h_bonds.clear();
    collect_contacts(structures[struc_cnt],model);
    struc_cnt++;
//    for(const HBond & hb:h_bonds) {
//      if(h_bonds_by_pos.find(hb.i_pos)==h_bonds_by_pos.end())
//        h_bonds_by_pos[hb.i_pos] = {};
//      h_bonds_by_pos[hb.i_pos].push_back(hb);
//    }
//    for(std::map<core::index2, std::vector<HBond>>::iterator iter = h_bonds_by_pos.begin(); iter != h_bonds_by_pos.end(); ++iter) {
//      core::index2 k =  iter->first;
//      std::vector<HBond> & hbs = iter->second;
//      std::sort(hbs.begin(),hbs.end());
//      if(hbs.size()>1)
//        std::cout << k << " " << hbs[0] << " " << hbs[1] << "\n";
//      else
//        std::cout << k << " " << hbs[0] << "\n";
//    }

  }

}
