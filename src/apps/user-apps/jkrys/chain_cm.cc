#include <iostream>

#include <core/data/io/Pdb.hh>
#include <simulations/representations/cabs/cabs_utils.hh>

using namespace core::data::structural;
using namespace core::data::io;

/** @brief Reads an all-atom structure from a PDB file and computes CM for each chain
 */
int main(const int argc, const char* argv[]) {

  // --- Read the input PDB and create a structure object
  core::data::io::Pdb reader(argv[1], all_true(is_not_hydrogen,is_not_water,is_not_alternative), keep_all, true);
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

 for(const auto & chain_sp : *strctr) {
   core::data::basic::Vec3 cm;
   double n_atoms = 0;
   for(auto it = chain_sp->first_const_atom(); it != chain_sp->last_const_atom();++it) {
     cm += (**it);
     ++n_atoms;
   }
   cm /=n_atoms;
   std::cout << chain_sp->id()<<" "<<cm<<"\n";
 }

}
