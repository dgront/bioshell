#include <ostream>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/protocols/selection_protocols.hh>
#include <core/data/structural/Structure.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>

#include <simulations/forcefields/cartesian/CAHydrogenBond.hh>
#include <simulations/forcefields/surpass/SurpassContactEnergy.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>

#include <utils/io_utils.hh>
#include <utils/Logger.hh>
#include <core/data/io/fasta_io.hh>
#include <core/calc/statistics/Random.hh>


utils::Logger l("calc_hbonds_statistics");

const std::string program_info = R"(
      Calculates H-bonds statistics  for given PDB files.)";


void shuffle(std::vector<char> &residue_types, std::map<std::string, int> &M_map_in, core::index2 n_shuffles) {

  std::string key("XX");
  std::random_device rd;
  std::mt19937 g(rd());

  for (int cnt = 0; cnt < n_shuffles; ++cnt) {
    std::shuffle(residue_types.begin(), residue_types.end(), g);

    for (auto h = 0; h < residue_types.size() - 1; h += 2) {
      key[0] = residue_types[h];
      key[1] = residue_types[h + 1];
      M_map_in[key] += 1;
    }
  }
}

int main(int argc, const char *argv[]) {

  using namespace core::data::io;
  using namespace core::data::structural;
  using namespace utils::options;
  using namespace simulations::forcefields::cartesian;

  utils::options::OptionParser &cmd = OptionParser::get("calc_hbonds_statistics");
  cmd.register_option(utils::options::help, show_examples, verbose, db_path, input_fasta_ss);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path,
      input_pdb_header); // input PDB structures

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id, "calculate energy for a given PDB file (native):", "calc_hbonds_statistics -in:pdb=2gb1.pdb");
  cmd.add_example(id, "calculate energy for a given PDB model:",
      "calc_hbonds_statistics  -ip=1693.pdb -in:fasta:secondary=2gb1A.fasta_ss");
  cmd.program_info(program_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header, "true");

  // ---------- global settings defining how we collect the statistics:
  core::index2 n_shuffles = 10000;     // --- number of random samples for the reference population
  double max_hb_energy = -0.1;         // --- maximum energy to say we still have an H-bond

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);

  core::data::structural::selectors::IsCA is_ca;

  // ---------- Process input sequence with secondary structure
  std::string sequence, secondary;
  if (cmd.was_used(input_fasta_ss)) { // ---------- Read SS fasta format
    const std::string input_ss_file = option_value<std::string>(input_fasta_ss);
    auto sec_str = core::data::io::read_fasta_ss_file(input_ss_file);
    secondary = sec_str->str();
    sequence = (sec_str->sequence.size() == 0) ? std::string('A', sec_str->str().size()) : sec_str->sequence;
  }

  // ---------- Process input structures
  std::map<std::string, int> L_map_in_E;   // --- observations for the native in E (L = licznik)
  std::map<std::string, int> M_map_in_E;   // --- observations for the randomised reference in E (M = mianownik)
  std::map<std::string, int> L_map_in_H;   // --- observations for the native in E (L = licznik)
  std::map<std::string, int> M_map_in_H;   // --- observations for the randomised reference in E (M = mianownik)
  std::vector<char> residue_types_E;
  std::vector<char> residue_types_H;
  std::string key("XX");
  // ---------- fill maps with zeros
  for (core::index2 i = 0; i < 20; ++i)
    for (core::index2 j = 0; j < 20; ++j) {
      key[0] = core::chemical::Monomer::get(i).code1;
      key[1] = core::chemical::Monomer::get(j).code1;
      L_map_in_E[key] = 0;
      M_map_in_E[key] = 0;
      L_map_in_H[key] = 0;
      M_map_in_H[key] = 0;
    }

  for (auto s: structures) {
    l << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for (auto ci: *s) ci->remove_ligands();
    if (!cmd.was_used(input_fasta_ss)) {
      for (auto chain: *s) {      // --- combine sequence from all chains if input fasta_ss data was given
        auto sec_str = chain->create_sequence();
        sequence += sec_str->sequence;
        secondary += sec_str->str();
        l << utils::LogLevel::INFO << "Secondary structure for " << s->code() << " chain " << chain->id() <<
          " from PDB header:" << secondary << "\n";
      }
    }
    core::protocols::keep_selected_atoms(is_ca, *s);
    simulations::systems::surpass::SurpassAlfaChains chains(*s);
    simulations::forcefields::cartesian::CAHydrogenBond hb(chains, secondary, "HE");

    residue_types_E.clear();
    residue_types_H.clear();

    // ---------- biological statictics
    for (core::index4 i = 0; i < chains.n_atoms; ++i) {
      for (core::index4 j = 0; j < chains.n_atoms; ++j) {
        double e_hb = hb.evaluate_raw_energy(chains, i, j);
        if (e_hb < max_hb_energy) {
          if (hb.secondary(i)=='E') {
            residue_types_E.push_back(core::chemical::Monomer::get((core::index2) chains[i].residue_type).code1);
            residue_types_E.push_back(core::chemical::Monomer::get((core::index2) chains[j].residue_type).code1);
            key[0] = core::chemical::Monomer::get((core::index2) chains[i].residue_type).code1;
            key[1] = core::chemical::Monomer::get((core::index2) chains[j].residue_type).code1;
            L_map_in_E[key] += 1;
            l << utils::LogLevel::FINE << "HB(E): " << key<<" "<<e_hb<<"\n";
          } else if (hb.secondary(i)=='H') {
            residue_types_H.push_back(core::chemical::Monomer::get((core::index2) chains[i].residue_type).code1);
            residue_types_H.push_back(core::chemical::Monomer::get((core::index2) chains[j].residue_type).code1);
            key[0] = core::chemical::Monomer::get((core::index2) chains[i].residue_type).code1;
            key[1] = core::chemical::Monomer::get((core::index2) chains[j].residue_type).code1;
            L_map_in_H[key] += 1;
            l << utils::LogLevel::FINE << "HB(H): " << key<<" "<<e_hb<<"\n";
          }
        }
      }
    }

    // ---------- randomly shuffled statistics
    // ---------- Skip the randomisation part if a current structure has no H-bonds of a given type
    l << utils::LogLevel::INFO << "reshuffling for: " << s->code() << " nH, nE: " << residue_types_H.size() << " "
      << residue_types_E.size() << "\n";
    if (residue_types_E.size() > 0)
      shuffle(residue_types_E, M_map_in_E, n_shuffles);
    if (residue_types_H.size() > 0)
      shuffle(residue_types_H, M_map_in_H, n_shuffles);
    //      for (auto it=L_map.begin();it!=L_map.end();++it) {
//          if (it->second!=0)std::cout<<it->first<<" "<<it->second<<"\n";
//      }
  }

  for (auto it = M_map_in_E.begin(); it != M_map_in_E.end(); ++it) {
    it->second /= double(n_shuffles);
  }
  for (auto it = M_map_in_H.begin(); it != M_map_in_H.end(); ++it) {
    it->second /= double(n_shuffles);
  }
  for (core::index2 i = 0; i < 20; ++i) {
    for (core::index2 j = 0; j < 20; ++j) {
      key[0] = core::chemical::Monomer::get(i).code1;
      key[1] = core::chemical::Monomer::get(j).code1;
      std::cout << utils::string_format("%2s %5d %5d   %5d %5d\n", key.c_str(), L_map_in_H[key], M_map_in_H[key], L_map_in_E[key], M_map_in_E[key]);
    }
  }
}
