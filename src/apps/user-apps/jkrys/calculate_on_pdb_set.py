import sys, re, json, logging
import os.path
import argparse

sys.path.extend(["/Users/jkrys/src.git/bioshell/bin","/home/jkrys/src.git/bioshell/bin"])
sys.path.extend(["/Users/dgront/src.git/visualife/","/home/dgront/src.git/visualife/"])

from visualife.data import amino_acid_code3_to_code1
from pybioshell.std import vector_double
from pybioshell.std import vector_core_data_basic_Vec3

from pybioshell.core.data.basic import Vec3
from pybioshell.core.data.io import Pdb
from pybioshell.core.calc.structural import evaluate_phi, evaluate_psi, evaluate_chi, evaluate_lambda, NeighborGrid3D
from pybioshell.core.data.structural.selectors import ResidueHasAllHeavyAtoms, ResidueHasBB, ProperlyConnectedCA, ResidueHasCA
from pybioshell.core.calc.structural.transformations import  local_coordinates_three_atoms
from pybioshell.core.calc.structural.interactions import peptide_hydrogen

from pybioshell.core.calc.structural import *

def process_all_pdb(pisces_list,**kwargs):

    """Loads all PDB files listed and calls a given routine for each structure

    :param pisces_list: (``list[tuple]``) - list of (pdb_id, chain_id, resolution) tuples loaded from a PISCES data
    :param routine: (``function``) - a function to be called for every protein structure
    :param kwargs: (``dict``) - the same dictionary will be passed to ``get_pdb_structure()``
        and the given ``routine()``; this function uses only ``stream=`` parametr,
        which must provide an object with ``append()`` and ``extend()`` methods; it can be a list
        (and then it will be extended with observation from each protein) or a custom object

    :return: list of observations taken as tuples - 2D data
    """

    chains = kwargs.get("chains", [])
    ress = kwargs.get("ress", [])
    last_reached = False
    chain_all = 0
    res_all = 0
    new_chain = 0
    dates=[]
    pdbs=[]
    for protein in pisces_list:
        #print(protein)
        if os.path.getsize(protein)==0: continue 
        # ---------- scroll down the list of proteins as the top ones were already processed
        # ---------- process a structure by calling a given routine
        strctr = Pdb(protein,"",True).create_structure(0)
        #print(strctr.deposition_date())
        codes = strctr.chain_codes()
        for chain in codes:
          s = strctr.get_chain(chain)
          dates.append(strctr.deposition_date())
          pdbs.append(strctr.code())
          chain_all+=1
          res_all+=s.count_aa_residues()
          oo = count_ca_ratio( s)
          chains.append(oo)
          oo = count_ca_only_res_ratio( s)
          ress.append(oo)
    chain_cnt = 0
    res_cnt = 0
    cnt = 0
    c=0
    new_res17 = 0
    new_res20 = 0
    for i in chains:
      #print(i) 
      if i>0.9 : 
        chain_cnt+=1
        #print(dates[cnt][7:])
        if 22>=int(dates[cnt][7:])>=17:
          new_chain+=1
          print(pdbs[cnt])
      cnt+=1
    for i in ress: 
      res_cnt+=i
      if 22>=int(dates[c][7:])>=17:
          new_res17+=i
      if 22>=int(dates[c][7:])>=20:
          new_res20+=i
      c+=1
    return chain_cnt,res_cnt,res_all, chain_all, new_chain,new_res17,new_res20

def count_ca_ratio(chain):
    ca_cnt = 0
    has_ca = ResidueHasCA()
    chain.remove_ligands()
    all_atm = chain.count_atoms()
    for i in range(chain.count_aa_residues()):
        if has_ca(chain[i]):
            ca_cnt+=1
#            all_ca.append(chain[i].find_atom(" CA "))
#    print(ca_cnt)
#    print(all_atm)
    return ca_cnt/float(all_atm)

def count_ca_only_res_ratio(chain):
    ca_cnt = 0
    has_ca = ResidueHasCA()
    chain.remove_ligands()
    all_atm = chain.count_aa_residues()
    for i in range(chain.count_aa_residues()):
        if has_ca(chain[i]) and chain[i].count_atoms()==1:
            ca_cnt+=1
#    print(ca_cnt)
#    print(all_atm)
    return ca_cnt

def parse_list(list_file):
    out = []
    with open(list_file) as f:
      for line in f.readlines()[1:]:
        out.append("/mnt/storage/DATABASES/PDB_MIRROR/wwpdb/pdb"+line.strip()+".ent.gz")

    return out


if __name__ == "__main__":
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Prepare training data from a PDB file(s)', add_help=True)

    # Add the arguments
    my_parser.add_argument('-i', '--pdb', metavar='fname or pdb_id', type=str, help='input PDB file')
    my_parser.add_argument('-p', '--path', type=str, help='input PDB file location')
    my_parser.add_argument('-c', '--chain', type=str, help='chain code')
    my_parser.add_argument('-l', '--list', metavar='fname', type=str, help='input files list')
    
    args = my_parser.parse_args()
    only_ca_chain = 0

    if args.list:
        inputs = parse_list(args.list)
        chains,ress,c_all,r_all,new_chain,new,n = process_all_pdb(inputs)
        print(chains,ress,c_all,r_all,new_chain,new,n)

    if args.pdb:
        if args.chain:
            path = args.path if args.path else ""
            strctr = Pdb(args.pdb,"",True).create_structure(0)
            print(strctr.deposition_date())
            ca_only_res =  count_ca_only_res_ratio(strctr.get_chain(args.chain))
            ca_ratio = count_ca_ratio(strctr.get_chain(args.chain))
            if ca_ratio==1: only_ca_chain+=1
            print("%s %s %3.2f, %3.2f"%(args.pdb, args.chain,only_ca_chain,ca_only_res))
