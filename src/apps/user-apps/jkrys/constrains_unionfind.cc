#include <memory>
#include <iostream>
#include <random>
#include <algorithm>    // std::sort
#include <fstream>

#include <core/algorithms/UnionFind.hh>
#include <core/calc/statistics/Random.hh>
#include <utils/options/OptionParser.hh>
#include <utils/exit.hh>
#include <utils/string_utils.hh>
#include <utils/io_utils.hh>

std::string program_info = R"(

Unit test which shows how to use the Union-Find algorithm.

USAGE:
./ex_UnionFind

REFERENCE:
    https://en.wikipedia.org/wiki/Disjoint-set_data_structure

)";

// ---------- Data type of objects that will be clustered
struct Point2D {
  float x, y;

  Point2D(float nx, float ny) : x(nx), y(ny) {}

  float distance_to(const Point2D &p) { return sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)); }
};

// ---------- this operator is necessary because core::algorithms::UnionFind keeps std::map of data points
bool operator<(const Point2D &lhs, const Point2D &rhs) { return lhs.x < rhs.x; }

/** @brief A simple example shows how to use UnionFind algorithm.
 *
 * The program calculates greedy clustering of points in 2D. The number of points can be provided from command line
 *
 * CATEGORIES: core::algorithms::UnionFind;
 * KEYWORDS:   data structures; data structures; algorithms
 */
int main(const int argc, const char *argv[]) {

  if ((argc > 1) && utils::options::call_for_help(argv[1]))
    utils::exit_OK_with_message(program_info);

  using namespace core::calc::statistics;

  std::vector<std::vector<double>> constrains;

    std::ifstream in;
    std::string fname = argv[1];
    utils::in_stream(fname, in);
    size_t cnt = 0;
    std::string line, token;
    try {
        while (std::getline(in, line)) {
            if (line.length() < 2) continue;
            if (line[0] == '#') continue;
            if (line[0] == '!') continue;
            std::vector<std::string> tokens = utils::split(line, {' '});
            std::vector<double> c;
            if (utils::from_string<int>(tokens[1])-utils::from_string<int>(tokens[0])>=7) {
                for (auto i = 0; i < 5; i++) c.push_back(utils::from_string<double>(tokens[i]));
                constrains.push_back(c);

                cnt++;
            }
        }
    } catch(std::ifstream::failure &e) {}
    std::vector<Point2D> points;
    int N = constrains.size();

  core::algorithms::UnionFind<Point2D, core::index2> uf;
  for (unsigned int i = 0; i < N; ++i) {

    points.emplace_back(int(constrains[i][0]), int(constrains[i][1])); // we use <code>emplace_back()</code> to create point objects directly in the container
    uf.add_element(points.back());
    for (unsigned int j = 0; j < i; ++j) {
      if (points[i].distance_to(points[j]) < 1.42) uf.union_set(i, j);
    }
  }


int N_wysp = constrains[constrains.size()-1][1]+1;
  std::cout<<"N_wysp "<<N_wysp<<"\n";

int k = uf.count_sets();
int j = uf.count_elements();
std::cout<<"size "<<k<<" "<<j<<"\n";
std::vector<int> sizes;
std::vector<std::vector<int>> elements;
for (auto i=0;i<N;i++) {
    std::vector<int> empty;
    sizes.push_back(0);
    elements.push_back(empty);
}
for (auto i=0;i<N;i++){
    int g = uf.find_set(i);
    sizes[g]+=1;
    elements[g].push_back(i);
    }
double every=0;
    for (auto i=0;i<N;i++)
        if (elements[i].size()!=0){
        std::cout<<i<<" "<<elements[i].size()<<"\n";
        every+=elements[i].size();}
    std::cout<<"razem "<<every<<"\n";


    std::vector<int> to_write;

        if (N_wysp<k) {

            std::sort(sizes.begin(),sizes.end());
            int min_size = std::max(sizes[sizes.size() - N_wysp], 1);
            std::cout << "min_size " << min_size << "\n";

            for (auto i = 0; i < N; i++)
                if (elements[i].size() >= min_size) {

                    //   std::cout<<i<<": ";
                    double min_sigma = constrains[elements[i][0]][3];
                    double the_number = elements[i][0];

                    for (auto a:elements[i]) {
                        //    std::cout << a << " ";

                        if (constrains[a][3] < min_sigma) the_number = a;
                    }
                    to_write.push_back(the_number);

                    //     std::cout << "\n";

                }
        }
        else if (N_wysp>j){
            for (int i=0;i<N;i++)
                to_write.push_back(i);
        }
        else
            {
            //linear transformation to tell how many constrains must be taken from every island
            std::vector<int> how_many;
            for (auto i=0;i<sizes.size();i++) {
                double n_cnstr = N_wysp * sizes[i] / every;
               // std::cout<<"double "<<n_cnstr<<"\n";

                how_many.push_back(round(n_cnstr));

            }

            for (auto i=0;i<N;i++)
                if (elements[i].size()!=0) {
                    //generating random index for a constrains from one island
                    //std::uniform_int_distribution<int> rand_index(0,elements[i].size()-1);
                    core::calc::statistics::Random &generator = core::calc::statistics::Random::get();
                    for (auto j=0;j<how_many[i];j++) {
                        std::uniform_int_distribution<int> rand_index(0,elements[i].size()-1);
                        auto x = rand_index(generator);
                        to_write.push_back(elements[i][x]);
                        elements[i].erase (elements[i].begin()+x);
                    }
                }
        }


    std::cout<< "wiezow "<<argv[1]<<" "<<to_write.size()<<" "<<N_wysp<<"\n";

    std::ofstream myfile;
    myfile.open (utils::string_format("%s%s",argv[1],".n"));
    for (auto n:to_write) {
        myfile << utils::string_format("%3.0f %3.0f 1.0 LORENTZIAN %5.3f 1.0\n",constrains[n][0],constrains[n][1],constrains[n][2]) << " ";
        //myfile << "\n";
    }
    myfile.close();
}
