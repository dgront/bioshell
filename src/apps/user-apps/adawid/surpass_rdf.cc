#include <iostream>

#include <utils/io_utils.hh>
#include <utils/string_utils.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <simulations/representations/surpass_utils.hh>
#include <core/data/basic/Vec3.hh>

using namespace core::data::structural;
using namespace core::data::io;
using namespace core::data::basic;

PdbAtom_SP it_SG, next_SG;
Residue_SP residue_sp, res, next_res;
Chain_SP ch_sp;
Structure_SP structure_sp;
core::index2 next_id;

int main(const int argc, const char* argv[]) {
    core::data::io::Pdb reader(argv[1], is_not_alternative, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);

    structure_sp = simulations::representations::surpass_representation(*strctr);

// Iterate over all chains
    for (auto it_chain = structure_sp->begin(); it_chain!=structure_sp->end(); ++it_chain) {
      ch_sp = (*it_chain);

// Calculate distance between each SG atom and atoms from i+4 to n, where n = ch_size
    core::index4 ch_size = ch_sp->Chain::count_residues();
    std::fstream plik_HH("dist_HH", std::ios::app);                             //file for HH pair rdf
    std::fstream plik_EE("dist_EE", std::ios::app);                             //file for EE pair rdf
    std::fstream plik_CC("dist_CC", std::ios::app);                             //file for CC pair rdf
    std::fstream plik_HE("dist_HE", std::ios::app);                             //file for HE pair rdf
    std::fstream plik_HC("dist_HC", std::ios::app);                             //file for HC pair rdf
    std::fstream plik_EC("dist_EC", std::ios::app);                             //file for EC pair rdf
    std::fstream plik_HX("dist_HX", std::ios::app);                             //file for HX pair rdf (H - whatever)
    std::fstream plik_EX("dist_EX", std::ios::app);                             //file for EX pair rdf (E - whatever)
    std::fstream plik_CX("dist_CX", std::ios::app);                             //file for CX pair rdf (C - whatever)

    for(core::index4 i = 0; i < ch_size ; ++i) {                                //for each SG atom...
        res = (*ch_sp)[i];
        it_SG = res->find_atom(" SG ");
        for(core::index4 j = i+4; j < ch_size ; ++j) {
            next_res = (*ch_sp)[j];
            next_id = next_res->id();
            next_SG = (ch_sp->get_residue(next_id))->find_atom(" SG ");
            double dist = it_SG->distance_to(*next_SG);
            if(dist <= 15.000) {
                if( (res->ss() == 'H')&&(next_res->ss() == 'H') ) {plik_HH << argv[1] <<" id1 = "<<it_SG->id()<<" id2 = "<<next_SG->id()<<" "<< dist << "\n";}
                else if ( (res->ss() == 'E')&&(next_res->ss() == 'E') ) {plik_EE << argv[1] <<" id1 = "<<it_SG->id()<<" id2 = "<<next_SG->id()<<" "<< dist << "\n";}
                else if ( (res->ss() == 'C')&&(next_res->ss() == 'C') ) {plik_CC << argv[1] <<" id1 = "<<it_SG->id()<<" id2 = "<<next_SG->id()<<" "<< dist << "\n";}
                else if ( ((res->ss() == 'H')&&(next_res->ss() == 'E'))||((res->ss() == 'E')&&(next_res->ss() == 'H')) ) {plik_HE << argv[1] <<" id1 = "<<it_SG->id()<<" id2 = "<<next_SG->id()<<" "<< dist << "\n";}
                else if ( ((res->ss() == 'H')&&(next_res->ss() == 'C'))||((res->ss() == 'C')&&(next_res->ss() == 'H')) ) {plik_HC << argv[1] <<" id1 = "<<it_SG->id()<<" id2 = "<<next_SG->id()<<" "<< dist << "\n";}
                else if ( ((res->ss() == 'E')&&(next_res->ss() == 'C'))||((res->ss() == 'C')&&(next_res->ss() == 'E')) ) {plik_EC << argv[1] <<" id1 = "<<it_SG->id()<<" id2 = "<<next_SG->id()<<" "<< dist << "\n";}
                if ( ((res->ss() == 'H')||(next_res->ss() == 'H'))&&(res->ss() !=next_res->ss()) ) {plik_HX << argv[1] <<" id1 = "<<it_SG->id()<<" id2 = "<<next_SG->id()<<" "<< dist << "\n";}
                if ( ((res->ss() == 'E')||(next_res->ss() == 'E'))&&(res->ss() !=next_res->ss()) ) {plik_EX << argv[1] <<" id1 = "<<it_SG->id()<<" id2 = "<<next_SG->id()<<" "<< dist << "\n";}
                if ( ((res->ss() == 'C')||(next_res->ss() == 'C'))&&(res->ss() !=next_res->ss()) ) {plik_CX << argv[1] <<" id1 = "<<it_SG->id()<<" id2 = "<<next_SG->id()<<" "<< dist << "\n";}
            }
        }
    }
    plik_HH.close();
    plik_EE.close();
    plik_CC.close();
    plik_HE.close();
    plik_HC.close();
    plik_EC.close();
    plik_HX.close();
    plik_EX.close();
    plik_CX.close();
    std::cerr<<"SUCCESSFUL DONE!\n";
    }
}