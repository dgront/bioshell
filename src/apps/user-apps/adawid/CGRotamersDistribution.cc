#include <math.h>
#include <iostream>
#include <random>
#include <vector>
#include <fstream>
#include <string>

#include <utils/io_utils.hh>
#include <utils/string_utils.hh>
#include <core/data/io/DataTable.hh>

#include <core/calc/statistics/RobustDistributionDecorator.hh>
#include <core/calc/statistics/expectation_maximization.hh>
#include <simulations/representations/cabs/CGRotamerDistribution.hh>

int main(const int argc, const char *argv[]) {

//  const double deg_to_rad = M_PI/180.0;
  using namespace core::calc::statistics;
  using namespace simulations::representations::cabs;

  std::vector<std::vector<double> > data_3D;
  std::vector<double> row(3);
  std::vector<core::index1> index_1D;

  std::ifstream file(argv[1]);
  std::string str;
  while (std::getline(file, str)) {
    std::vector<std::string> tokens = utils::split(str, {' '});
    if (tokens.size() != 4) {
      std::cerr << "Incorrect rotamer definition line: "<<str<<"\n";
      continue;
    }
    row[0] = (utils::from_string<double>(tokens[0]));
    row[1] = (utils::from_string<double>(tokens[1]));
    row[2] = (utils::from_string<double>(tokens[2]));
    data_3D.push_back(row);
  }

  std::ofstream plik_data;
  plik_data.open ("data.txt");
  std::ofstream plik_params;
  plik_params.open ("params.txt");
  std::ofstream plik_pdf;
  plik_pdf.open ("pdf.txt");

  // --- starting parameters for distribution estimation
  std::vector<double>
      D1{2.8, 0.7, -0.75, 0.2, 0.30, 240.0},
      D2{2.8, 1.5, -1.50, 0.2, 0.15, 90.0},
      D3{2.8, 0.7, -2.45, 0.2, 0.30, 170.0};

  std::vector<RobustDistributionDecorator<CGRotamerDistribution>> distributions_3D;
  distributions_3D.push_back( RobustDistributionDecorator<CGRotamerDistribution>(D1) );
  distributions_3D.push_back( RobustDistributionDecorator<CGRotamerDistribution>(D2) );
  distributions_3D.push_back( RobustDistributionDecorator<CGRotamerDistribution>(D3) );

  double score = expectation_maximization(data_3D, distributions_3D, index_1D, 0.0001, 1000);
  core::index4 cnt0 = std::count(index_1D.cbegin(), index_1D.cend(), 0);
  core::index4 cnt1 = std::count(index_1D.cbegin(), index_1D.cend(), 1);
  core::index4 cnt2 = std::count(index_1D.cbegin(), index_1D.cend(), 2);
  double Ntot = cnt0 + cnt1 + cnt2;

  core::index4 cnt;
  std::vector<double> D{0.0, 0.0, 0.0, 0.0, 0.0};
  plik_params << "# counts  ave_r ave_a1   mi_a2   sd_r  sd_a1 kappa_a2\n";
  for (unsigned int i = 0; i < distributions_3D.size(); ++i) {
    distributions_3D[i].backup_parameters(D);
    cnt = std::count(index_1D.cbegin(), index_1D.cend(), i);
    plik_params << utils::string_format("%8d %8.6f %6.4f %6.4f %7.4f %6.4f %6.4f %7.4f \n", cnt, cnt / Ntot, D[0], D[1], D[2], D[3], D[4], D[5]);
  }
  plik_pdf << "#    r     a1      a2 1:count*pdf 2:count*pdf 3:count*pdf\n";
  for (unsigned int i=0; i<data_3D.size(); ++i)
    plik_pdf << utils::string_format("%6.4f %6.4f %7.4f %#11.5g %#11.5g %#11.5g \n", data_3D[i][0], data_3D[i][1], data_3D[i][2], index_1D[i], cnt0 / Ntot * distributions_3D[0].evaluate(data_3D[i]),
	cnt1 / Ntot * distributions_3D[1].evaluate(data_3D[i]), cnt2 / Ntot * distributions_3D[2].evaluate(data_3D[i]));

  for (unsigned int i=0; i<data_3D.size(); ++i)
	plik_data << utils::string_format("%6.4f %6.4f %7.4f %1d \n",data_3D[i][0], data_3D[i][1], data_3D[i][2], index_1D[i]);
  plik_data.close();
  plik_params.close();
  plik_pdf.close();
}
