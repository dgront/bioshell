#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdio.h>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>

int main(const int argc, const char* argv[]) {

    using namespace core::data::structural;
    using namespace core::calc::structural::transformations;

    core::data::io::Pdb reader(argv[1]);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    char i = *(argv[2]);
    Chain_SP chain_sp = strctr->core::data::structural::Structure::get_chain(i);	//argv[2] = 1-literowy kod łańcucha, const char code

    std::string s = argv[1];								//nazwa pliku z pdb-cabsbb
    std::string delimiter = "-";							//znak oddzielający pdb_code od reszty nazwy pliku
    std::string code = s.substr(0, s.find(delimiter));					//pierwszy token = pdb_code
//    std::cout<<code<<"\n";
    std::string name = code +"-SC.pdb";							//nazwa pliku dla outfile.pdb dla atomów SC (do oglądania w pymolu)
    std::ofstream myfile;
    myfile.open(name);

//    std::cout<<chain_sp->Chain::id()<<"\n";
    std::cout<<"#     x    "<<"      y    "<<"      z    "<<"  ID"<<"   Bf"<<" CH "<<"RES \n";

    for (auto it_resid = chain_sp->begin(); it_resid!=chain_sp->end(); ++it_resid) {
        Residue_SP resid_sp = (*it_resid);
////	Chain_SP chain_sp = resid_sp->Residue::owner();					//łańcuch, do którego przypisana jest reszta
        PdbAtom_SP n_sp = (*it_resid)->find_atom(" N  ");
        if(n_sp==nullptr) { std::cerr << "Missing backbone atom N \n"; continue; }
        PdbAtom_SP ca_sp = (*it_resid)->find_atom(" CA ");
        if(ca_sp==nullptr) { std::cerr << "Missing backbone atom CA \n"; continue; }
        PdbAtom_SP c_sp = (*it_resid)->find_atom(" C  ");
        if(c_sp==nullptr) { std::cerr << "Missing backbone atom C \n"; continue; }
        Rototranslation_SP rt = core::calc::structural::transformations::local_coordinates_three_atoms(*n_sp,*ca_sp,*c_sp);
        PdbAtom_SP sc_sp = (*it_resid)->find_atom(" SC ");
        if(sc_sp!=nullptr) { 
          rt->apply(*sc_sp);
	  const core::chemical::Monomer m = resid_sp->Residue::residue_type();
	  //wydruk na ekran nowych współrzędnych oraz innych danych dla atomów SC; do dalszej analizy i klasteryzacji
	  fprintf (stdout, "%10.6f %10.6f %10.6f %4u %5.2f", sc_sp->x, sc_sp->y, sc_sp->z, resid_sp->Residue::id(), sc_sp->PdbAtom::b_factor());
	  std::cout <<" "<<chain_sp->Chain::id()<<" "<<m.code3<<"\n";

	  myfile <<sc_sp->PdbAtom::to_pdb_line()<<"\n";				//zapis nowych współrzędnych atomów SC w formacie pdb (do oglądania w pymolu)
        }
    }
    myfile.close();
}
