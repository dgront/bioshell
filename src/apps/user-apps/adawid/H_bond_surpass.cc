#include <iostream>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/calc/structural/interactions/BackboneHBondInteraction.hh>
#include <core/calc/structural/interactions/HydrogenBondInteraction.hh>
#include <core/data/basic/Vec3.hh>

using namespace core::data::structural;
using namespace core::data::io;
using namespace core::data::basic;

int main(const int argc, const char* argv[]) {
    core::data::io::Pdb reader(argv[1], is_not_alternative, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);

    double dssp;
    std::vector<std::vector<double> > H_bond;
    std::vector<double> row(0);
    if(row.size()==0) {row.resize(6, 0.0);}

// Iterate over all chains
    for (auto it_chain = strctr->begin(); it_chain!=strctr->end(); ++it_chain) {
      Chain_SP ch_sp = *it_chain;
      double ch_size = (*it_chain)->Chain::count_residues();
      for(core::index2 k=0; k<ch_size; ++k) {
        H_bond.push_back(row);
      }
// Iterate over all residues in the chain
      for(core::index2 i=0; i<ch_size; ++i) {
        H_bond[i][0] = (*ch_sp)[i]->id();
        for(core::index2 j=0; j<ch_size-1; ++j) {
	  if ( (i == j)||(i == j+1)||(i == j-1)||(i == 0) ) {continue;}
          if ( (**it_chain)[i]->ss() == 'H' ) {continue;}
          if ( (**it_chain)[j]->ss() == 'H' ) {continue;}
	  if ( ((*ch_sp)[i]->id() - (*ch_sp)[i-1]->id() != 1)||((*ch_sp)[j+1]->id() - (*ch_sp)[j]->id() != 1) ) {continue;}
	  core::calc::structural::BackboneHBondGeometry hbg((*ch_sp)[i-1],(*ch_sp)[i],(*ch_sp)[j],(*ch_sp)[j+1]);
	  if (hbg.is_qualified()) {
            dssp = hbg.dssp_energy();
            if (dssp > -0.4) {continue;}
	    if (dssp < H_bond[i][4]) {
              H_bond[i][4] = dssp;
	      H_bond[i][2] =1;
              H_bond[i][3] = (**it_chain)[j]->id();
            }
	  }
	}
// Check continues of 4 next residues to make surpass pseudoatom
	if (i<ch_size-3) {
          if ( ((*ch_sp)[i+1]->id() - (*ch_sp)[i]->id() == 1)
            &&((*ch_sp)[i+2]->id() - (*ch_sp)[i]->id() == 2)
            &&((*ch_sp)[i+3]->id() - (*ch_sp)[i]->id() == 3) ) {H_bond[i][1] = 1;}
	}
      }
    }
    for (core::index2 i=0; i<H_bond.size(); ++i) {
      if (H_bond[i][1] == 1) {			//dla każdego pseudoatomu surpass
	core::index4 donor = H_bond[i][0];		//3
	core::index4 acceptor = H_bond[i][3];		//18
//	bool is_para, is_anty_Mi = false, is_para_Pi = false, is_anty_Pi = false, is_para_Mi = false, is_anty_Pj = false, is_para_Mj = false, is_anty_Mj = false, is_para_Pj = false;
	for (core::index2 j=0; j<H_bond.size(); ++j) {
	  if ( (i == j)||(i == j+1)||(i == j-1) ) {continue;}
	  if ( (H_bond[j][0] == acceptor)&&(H_bond[j][1] == 1) ) {		//takie j dla którego id2 = id_akceptora i istnieje pseudoatom surpasowy o tym indeksie
//	    if (H_bond[j][3] == donor) {is_double_A = true;}			//wiązanie wodorowe antyrównoległe podwójne między i-j oraz j-i
	    if (H_bond[j][3] == donor+2) {H_bond[i][5] = acceptor; H_bond[j][5] = donor+2;}			//wiązanie wodorowe równoległe przekątne pomiedzy j-(i+2)
	    if (H_bond[j][3] == donor-2) {H_bond[i][5] = acceptor; H_bond[j][5] = donor-2;}			//wiązanie wodorowe równoległe przekątne pomiedzy j-(i-2)
	  }
	  if ( (H_bond[j][0] == donor+2)&&(H_bond[j][1] == 1) ) {
	    if (H_bond[j][3] == acceptor-2) {H_bond[i][5] = acceptor; H_bond[j][5] = acceptor-2;}		//wiązanie wodorowe antyrównoległe pomiędzy (i+2)-(j-2)
	    if (H_bond[j][3] == acceptor+2) {H_bond[i][5] = acceptor; H_bond[j][5] = acceptor+2;}		//wiązanie wodorowe równoległe pomiędzy (i+2)-(j+2)
	  }
	  if ( (H_bond[j][0] == donor-2)&&(H_bond[j][1] == 1) ) {
	    if (H_bond[j][3] == acceptor+2) {H_bond[i][5] = acceptor; H_bond[j][5] = acceptor+2;}		//wiązanie wodorowe antyrównoległe pomiędzy (i-2)-(j+2)
	    if (H_bond[j][3] == acceptor-2) {H_bond[i][5] = acceptor; H_bond[j][5] = acceptor-2;}		//wiązanie wodorowe równoległe pomiędzy (i-2)-(j-2)
	  }
	  if ( (H_bond[j][0] == acceptor-2)&&(H_bond[j][1] == 1) ) {
	    if (H_bond[j][3] == donor+2) {H_bond[i][5] = acceptor; H_bond[j][5] = donor+2;}			//wiązanie wodorowe antyrównoległe pomiędzy (j-2)-(i+2)
	    if (H_bond[j][3] == donor-2) {H_bond[i][5] = acceptor; H_bond[j][5] = donor-2;}			//wiązanie wodorowe równoległe pomiędzy (j-2)-(j-2)
	  }
	  if ( (H_bond[j][0] == acceptor+2)&&(H_bond[j][1] == 1) ) {
	    if (H_bond[j][3] == donor-2) {H_bond[i][5] = acceptor; H_bond[j][5] = donor-2;}			//wiązanie wodorowe antyrównoległe pomiędzy (j+2)-(i-2)
	    if (H_bond[j][3] == donor+2) {H_bond[i][5] = acceptor; H_bond[j][5] = donor+2;}			//wiązanie wodorowe równoległe pomiędzy (j+2)-(i+2)
	  }
	}
      }
    }
    for (core::index2 i=0; i<H_bond.size(); ++i) {
      if (H_bond[i][5] != 0){
        std::cerr<<H_bond[i][0]<<" "<<H_bond[i][1]<<" "<<H_bond[i][2]<<" "<<H_bond[i][3]<<" "<<H_bond[i][4]<<" "<<H_bond[i][5]<<"\n";
      }
    }
}


