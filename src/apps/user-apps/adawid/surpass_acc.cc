#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>

#include <utils/io_utils.hh>
#include <utils/string_utils.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/chemical/ChiAnglesDefinition.hh>

using namespace core::data::structural;
using namespace core::data::io;

   // Parameters
PdbAtom_SP next_CA_sp, next_SG;
Residue_SP residue_sp, resid_sp, res, next_res, next_res_sp;
Chain_SP ch_sp, chain_sp;
int is_exposed;
std::vector<int> ACC_SG;
bool is_OK = true;
char it_ss, chain_id = 'o';
core::index2 resid_id, resid_number = 0, SG_id = 0, N = 1, n_res = 0;
double BF = 0.0;

int main(const int argc, const char* argv[]) {
    core::data::io::Pdb reader(argv[1], is_not_alternative, true); 					// file name (PDB format, may be gzip-ped)
    core::data::structural::Structure_SP strctr = reader.create_structure(0);

    std::cerr<<"INFO: STEP 1: Reading data from file with ACC\n";
// Read vector of vectors with data of each residue: 1)id; 2)name (one_letter_code); 3)is_exposed ("1")
	  std::vector<std::vector<real>> ACC;
	  std::vector<real> row(3);
	  std::ifstream file(argv[2]);
	  std::string acc;
	  while (std::getline(file, acc)) {
            std::vector<std::string> tokens = utils::split(acc,' ');
    	    if (tokens.size() != 4) {
    	      std::cerr << "Incorrect parameters: "<<acc<<"\n";
    	    continue;
    	    }
	    row[0] = (utils::from_string<double>(tokens[0]));			//id_rsid
	    row[1] = (utils::from_string<double>(tokens[3]));			//ACC
	    row[2] = 0;
	    if( (tokens[2] == "A")&&(row[1]/145.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "C")&&(row[1]/257.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "D")&&(row[1]/190.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "E")&&(row[1]/187.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "F")&&(row[1]/139.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "G")&&(row[1]/216.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "H")&&(row[1]/216.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "I")&&(row[1]/133.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "K")&&(row[1]/226.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "L")&&(row[1]/172.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "M")&&(row[1]/181.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "N")&&(row[1]/234.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "P")&&(row[1]/236.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "Q")&&(row[1]/209.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "R")&&(row[1]/172.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "S")&&(row[1]/167.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "T")&&(row[1]/177.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "V")&&(row[1]/246.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "W")&&(row[1]/230.0 >= 0.60) ) {row[2] = 1;}
	    else if( (tokens[2] == "Y")&&(row[1]/160.0 >= 0.60) ) {row[2] = 1;}
	    ACC.push_back(row);
	  }
	  std::cerr<<"INFO: Step 1 - done\n";
////	  for (unsigned int i = 0; i < ACC.size(); ++i ) {                          //////////////////////////////////////////////////////
////	      std::cerr<<"ACC "<<ACC[i][0]<<" "<<ACC[i][1]<<" "<<ACC[i][2]<<"\n";}  //////////////////////////////////////////////////////
	  std::cerr<<"INFO: Step 2: Iterating over all chains and residues\n";


// Iterate over all chains
    for (auto it_chain = strctr->begin(); it_chain!=strctr->end(); ++it_chain) {
      chain_sp = *it_chain;
      if( (chain_sp->id()==chain_id)&&(is_OK == false) ) break;
      core::index2 chain_size = chain_sp->count_aa_residues();
      ch_sp = std::make_shared<core::data::structural::Chain>(chain_sp->Chain::id());

// Iterate over all residues in the chain
    for (auto it_resid = (*it_chain)->begin(); it_resid != (*it_chain)->end(); ++it_resid) {
      PdbAtom_SP atom_CA_sp;
      ++n_res;
      if (n_res >= chain_size - 3) {
        chain_id = chain_sp->id();
        is_OK = false;
        break;
      }
      resid_sp = *it_resid;
      resid_id = resid_sp->id();
      if (resid_sp->residue_type().type == 'P') {

// Check the completeness of residues in the chain and atoms in the residue
        if (resid_number + 1 == resid_id) {
          if ((resid_number + 2 != (*chain_sp)[n_res]->id())
              || (resid_number + 3 != (*chain_sp)[n_res + 1]->id())
              || (resid_number + 4 != (*chain_sp)[n_res + 2]->id())) {
            ++SG_id;
            ++resid_number;
            continue;
          }
          ++resid_number;
            atom_CA_sp = resid_sp->find_atom(" CA ");
          if (atom_CA_sp == nullptr) {
            std::cerr<< "Missing backbone atom CA from residue: " << resid_id << " " << resid_sp->residue_type().code3 << "\n";
            ++SG_id;
            continue;
          }
        } else if (resid_number + 1 != resid_id) {
          std::cerr<< "Missing all residue(s) from number " << resid_number + 1 << " to " << resid_id - 1 << "\n";
          resid_number = resid_id;
          ++SG_id;
            atom_CA_sp = resid_sp->find_atom(" CA ");
          if ((resid_number + 1 != (*chain_sp)[n_res]->id())
              || (resid_number + 2 != (*chain_sp)[n_res + 1]->id())
              || (resid_number + 3 != (*chain_sp)[n_res + 2]->id())) {
            ++SG_id;
            ++resid_number;
            continue;
          }
          if (atom_CA_sp == nullptr) {
            std::cerr<< "Missing backbone atom CA from residue: " << resid_id << " " << resid_sp->residue_type().code3 << " Can't make atom SG number " << SG_id << "\n";
            ++SG_id;
            continue;
          }
        }

// Make new representation (one ball for 4 subsequence residues; if sequence size is n, than you have n-1 balls)
        PdbAtom_SP atom_SG_sp = std::make_shared<core::data::structural::PdbAtom>(SG_id, " SG ");
        atom_SG_sp->occupancy(1.00);
        atom_SG_sp->b_factor(0.00);
        atom_SG_sp->owner(resid_sp);
        *(atom_SG_sp) += *(atom_CA_sp);
        atom_SG_sp->core::data::structural::PdbAtom::id(SG_id);
        BF += atom_CA_sp->core::data::structural::PdbAtom::b_factor();
        it_ss = resid_sp->ss();
        N = 1;                                             //liczba wegli Ca (aminokwasów) tworzących wspólny pseudoatom
        for (core::index2 i = 1; i < 4; ++i) {
          next_res_sp = chain_sp->get_residue(resid_id + i);
          next_CA_sp = next_res_sp->find_atom(" CA ");
          *(atom_SG_sp) += *(next_CA_sp);
          BF += next_CA_sp->core::data::structural::PdbAtom::b_factor();
          if (it_ss == (chain_sp->Chain::get_residue(resid_id + i))->ss()) {
            it_ss = resid_sp->ss();
          } else if ((i == 1) && (it_ss == 'C') && (chain_sp->Chain::get_residue(resid_id + i))->ss() != 'C') {
            it_ss = chain_sp->Chain::get_residue(resid_id + i)->ss();
            if (chain_sp->Chain::get_residue(resid_id + 3)->ss() == 'C') {
              it_ss = 'C';
            }
          } else if ((i == 3) && (it_ss != 'C') && (chain_sp->Chain::get_residue(resid_id + i))->ss() == 'C') {
            it_ss = resid_sp->ss();
            if (chain_sp->Chain::get_residue(resid_id + 1)->ss() == 'C') {
              it_ss = 'C';
            }
          } else it_ss = 'C';
          ++N;
          if (N == 4) {
            *(atom_SG_sp) /= N;
            atom_SG_sp->b_factor(BF / N);
            residue_sp = std::make_shared<core::data::structural::Residue>(resid_sp->id(),core::chemical::Monomer::GLY);
            atom_SG_sp->id(resid_sp->id());
            (*residue_sp).push_back(atom_SG_sp);
            residue_sp->ss(it_ss);
            (*ch_sp).push_back(residue_sp);
	    if( (resid_id+1 == ACC[n_res][0])&&(resid_id+2 == ACC[n_res+1][0]) ) {			//nadanie pseudoatomowi statusu "0=buried" lub "1=exposed"
		if( (ACC[n_res][2] == 0)&&(ACC[n_res+1][2] == 0)) {is_exposed = 0;}
		else is_exposed = 1;
	    }
	    ACC_SG.push_back(is_exposed);
          }
        }
      }
    }
    }
    std::cerr<<"INFO: Step 2 - done\n";
    std::cerr<<"INFO: STEP 3: Searching first neighbours area from exposed balls \n";
// Search neighbours of exposed ("2")
    core::index2 ch_size = ch_sp->Chain::count_residues();
    for(core::index2 i=0; i<ch_size; ++i) {				//for each SG atom...
	if(ACC_SG[i] == 1) {
	    res = (*ch_sp)[i];
	    PdbAtom_SP it_SG = res->Residue::find_atom(" SG ");
            for(core::index2 j = i+1; j < ch_size ; ++j) {
		if(ACC_SG[j] == 0) {
	    	    next_res = (*ch_sp)[j];
    	    	    int next_id = next_res->id();
    		    next_SG = (ch_sp->Chain::get_residue(next_id))->Residue::find_atom(" SG ");
        	    double dist = it_SG->distance_to(*next_SG);
        	    if(dist <= 4.5) {ACC_SG[j] = 2;}
        	}
	    }
        }
    }
    std::cerr<<"INFO: Step 3 - done\n";
    std::cerr<<"INFO: STEP 4: Counting number of pseudoatoms per ceneter_of_buried\n";
    std::fstream plik0("acc_all_45", std::ios::app);				//file for all structures; radius = 4.5A
    std::fstream plik1("acc_all_50", std::ios::app);				//file for all structures; radius = 5.0A
    std::fstream plik2("acc_all_55", std::ios::app);				//file for all structures; radius = 5.5A
    std::fstream plik3("acc_all_60", std::ios::app);				//file for all structures; radius = 6.0A
    std::fstream plik4("acc_all_65", std::ios::app);				//file for all structures; radius = 6.5A
    std::fstream plik5("acc_all_70", std::ios::app);				//file for all structures; radius = 7.0A
    std::fstream plik_H0("acc_H_45", std::ios::app);				//file for H structures for radius = 4.5A
    std::fstream plik_H1("acc_H_50", std::ios::app);				//file for H structures for radius = 5.0A
    std::fstream plik_H2("acc_H_55", std::ios::app);				//file for H structures for radius = 5.5A
    std::fstream plik_H3("acc_H_60", std::ios::app);				//file for H structures for radius = 6.0A
    std::fstream plik_H4("acc_H_65", std::ios::app);				//file for H structures for radius = 6.5A
    std::fstream plik_H5("acc_H_70", std::ios::app);				//file for H structures for radius = 7.0A
    std::fstream plik_E0("acc_E_45", std::ios::app);				//file for E structures for radius = 4.5A
    std::fstream plik_E1("acc_E_50", std::ios::app);				//file for E structures for radius = 5.0A
    std::fstream plik_E2("acc_E_55", std::ios::app);				//file for E structures for radius = 5.5A
    std::fstream plik_E3("acc_E_60", std::ios::app);				//file for E structures for radius = 6.0A
    std::fstream plik_E4("acc_E_65", std::ios::app);				//file for E structures for radius = 6.5A
    std::fstream plik_E5("acc_E_70", std::ios::app);				//file for E structures for radius = 7.0A
    std::fstream plik_C0("acc_C_45", std::ios::app);				//file for C structures for radius = 4.5A
    std::fstream plik_C1("acc_C_50", std::ios::app);				//file for C structures for radius = 5.0A
    std::fstream plik_C2("acc_C_55", std::ios::app);				//file for C structures for radius = 5.5A
    std::fstream plik_C3("acc_C_60", std::ios::app);				//file for C structures for radius = 6.0A
    std::fstream plik_C4("acc_C_65", std::ios::app);				//file for C structures for radius = 6.5A
    std::fstream plik_C5("acc_C_70", std::ios::app);				//file for C structures for radius = 7.0A
    int n_0, n_1, n_2, n_3, n_4, n_5;
    for(core::index2 i=0; i<ch_size; ++i) {				//for each SG atom...
	if(ACC_SG[i] == 0) {
	n_0 = 0; n_1 = 0; n_2 = 0; n_3 = 0; n_4 = 0; n_5 = 0;
	res = (*ch_sp)[i];
	PdbAtom_SP it_SG = res->Residue::find_atom(" SG ");
            for(core::index2 j = 0; j < ch_size ; ++j) {
		if( (ACC_SG[j] == 0)||(ACC_SG[j] == 2) ) {
	    	    next_res = (*ch_sp)[j];
    	    	    int next_id = next_res->id();
    		    next_SG = (ch_sp->Chain::get_residue(next_id))->Residue::find_atom(" SG ");
        	    double dist = it_SG->distance_to(*next_SG);
        	    if(dist <= 4.5) {++n_0;}
        	    if(dist <= 5.0) {++n_1;}
        	    if(dist <= 5.5) {++n_2;}
        	    if(dist <= 6.0) {++n_3;}
        	    if(dist <= 6.5) {++n_4;}
        	    if(dist <= 7.0) {++n_5;}
        	}
	    }
	    plik0 <<argv[1]<<" "<<ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_0<<"\n";
	    plik1 <<argv[1]<<" "<<ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_1<<"\n";
	    plik2 <<argv[1]<<" "<<ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_2<<"\n";
	    plik3 <<argv[1]<<" "<<ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_3<<"\n";
	    plik4 <<argv[1]<<" "<<ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_4<<"\n";
	    plik5 <<argv[1]<<" "<<ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_5<<"\n";
	    if(res->ss() == 'H') {
		plik_H0  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_0<<"\n";
		plik_H1  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_1<<"\n";
		plik_H2  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_2<<"\n";
		plik_H3  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_3<<"\n";
		plik_H4  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_4<<"\n";
		plik_H5  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_5<<"\n";
	    }
	    else if(res->ss() == 'E') {
		plik_E0  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_0<<"\n";
		plik_E1  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_1<<"\n";
		plik_E2  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_2<<"\n";
		plik_E3  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_3<<"\n";
		plik_E4  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_4<<"\n";
		plik_E5  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_5<<"\n";
	    }
	    else if(res->ss() == 'C') {
		plik_C0  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_0<<"\n";
		plik_C1  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_1<<"\n";
		plik_C2  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_2<<"\n";
		plik_C3  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_3<<"\n";
		plik_C4  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_4<<"\n";
		plik_C5  <<argv[1]<<" "<< ch_sp->id()<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<n_5<<"\n";
	    }
	}
    }
    std::cerr<<"INFO: Step 4 - done\n";
    plik0.close();
    plik1.close();
    plik2.close();
    plik3.close();
    plik4.close();
    plik5.close();
    plik_H0.close();
    plik_H1.close();
    plik_H2.close();
    plik_H3.close();
    plik_H4.close();
    plik_H5.close();
    plik_E0.close();
    plik_E1.close();
    plik_E2.close();
    plik_E3.close();
    plik_E4.close();
    plik_E5.close();
    plik_C0.close();
    plik_C1.close();
    plik_C2.close();
    plik_C3.close();
    plik_C4.close();
    plik_C5.close();
}



