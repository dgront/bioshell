#include <iostream>

#include <utils/io_utils.hh>
#include <utils/string_utils.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <simulations/representations/surpass_utils.hh>
#include <core/data/basic/Vec3.hh>

using namespace core::data::structural;
using namespace core::data::io;
using namespace core::data::basic;

PdbAtom_SP atom_sp;
Residue_SP residue_sp, res;
Chain_SP ch_sp;
Structure_SP structure_sp;
core::index2 ch_size, id_;

int main(const int argc, const char* argv[]) {
    core::data::io::Pdb reader(argv[1], is_not_alternative, true);
    core::data::structural::Structure_SP strctr = reader.create_structure(0);

    structure_sp = simulations::representations::surpass_representation(*strctr);

// Iterate over all chains
    for (auto it_chain = structure_sp->begin(); it_chain!=structure_sp->end(); ++it_chain) {
      ch_sp = (*it_chain);

// Calculate distance between each SG atom and n-2, n-1, n+1, n+2, n+3 atoms
      ch_size = ch_sp->Chain::count_residues();
      std::fstream plik0("dist_all", std::ios::app);                              //file for all structures
      std::fstream plik_H1("dist_H_m2", std::ios::app);                           //file for H structures for distance to atom n-2
      std::fstream plik_H2("dist_H_m1", std::ios::app);                           //file for H structures for distance to atom n-1
      std::fstream plik_H3("dist_H_p1", std::ios::app);                           //file for H structures for distance to atom n+1
      std::fstream plik_H4("dist_H_p2", std::ios::app);                           //file for H structures for distance to atom n+2
      std::fstream plik_H5("dist_H_p3", std::ios::app);                           //file for H structures for distance to atom n+3
      std::fstream plik_E1("dist_E_m2", std::ios::app);                           //file for E structures for distance to atom n-2
      std::fstream plik_E2("dist_E_m1", std::ios::app);                           //file for E structures for distance to atom n-1
      std::fstream plik_E3("dist_E_p1", std::ios::app);                           //file for E structures for distance to atom n+1
      std::fstream plik_E4("dist_E_p2", std::ios::app);                           //file for E structures for distance to atom n+2
      std::fstream plik_E5("dist_E_p3", std::ios::app);                           //file for E structures for distance to atom n+3
      std::fstream plik_C1("dist_C_m2", std::ios::app);                           //file for C structures for distance to atom n-2
      std::fstream plik_C2("dist_C_m1", std::ios::app);                           //file for C structures for distance to atom n-1
      std::fstream plik_C3("dist_C_p1", std::ios::app);                           //file for C structures for distance to atom n+1
      std::fstream plik_C4("dist_C_p2", std::ios::app);                           //file for C structures for distance to atom n+2
      std::fstream plik_C5("dist_C_p3", std::ios::app);                           //file for C structures for distance to atom n+3

      for(core::index2 z=1; z<ch_size; ++z) {                                     //for each SG atom...
        res = (*ch_sp)[z];
        id_ = res->id();
        PdbAtom_SP it_SG = res->find_atom(" SG ");
        plik0 <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" ";

              if( (z>=2)&&(it_SG->id() == ((*ch_sp)[z-2]->find_atom(" SG ")->id())+2) ) {                                                        //distance to atom n-2
                PdbAtom_SP SG_m2 = (ch_sp->get_residue(id_-2))->find_atom(" SG ");
                double dist_m2 = it_SG->distance_to(*SG_m2);
                plik0 <<"# "<<dist_m2<<" "<<(ch_sp->get_residue(id_-2))->ss()<<"+2 ";
                if(res->ss() == (ch_sp->get_residue(id_-2))->ss()) {
                  if(res->ss() == 'H') {plik_H1  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_m2<<"\n";}
                  else if(res->ss() == 'E') {plik_E1  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_m2<<"\n";}
                  else if(res->ss() == 'C') {plik_C1  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_m2<<"\n";}
                }
              }

              if( (z>=1)&&(it_SG->id() == ((*ch_sp)[z-1]->find_atom(" SG ")->id())+1) ) {                                                        //distance to atom n-1
                PdbAtom_SP SG_m1 = (ch_sp->get_residue(id_-1))->find_atom(" SG ");
                double dist_m1 = it_SG->distance_to(*SG_m1);
                plik0 <<"# "<<dist_m1<<" "<<(ch_sp->get_residue(id_-1))->ss()<<"+1 ";
                if(res->ss() == (ch_sp->get_residue(id_-1))->ss()) {
                  if(res->ss() == 'H') {plik_H2  <<argv[1]<<" "<< it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_m1<<"\n";}
                  else if(res->ss() == 'E') {plik_E2  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_m1<<"\n";}
                  else if(res->ss() == 'C') {plik_C2  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_m1<<"\n";}
                }
              }

              if( (z<=ch_size-2)&&(it_SG->id() == ((*ch_sp)[z+1]->find_atom(" SG ")->id())-1) ) {                                                //distance to atom n+1
                PdbAtom_SP SG_p1 = (ch_sp->get_residue(id_+1))->find_atom(" SG ");
                double dist_p1 = it_SG->distance_to(*SG_p1);
                plik0 <<"# "<<dist_p1<<" "<<(ch_sp->get_residue(id_+1))->ss()<<"-1 ";
                if(res->ss() == (ch_sp->get_residue(id_+1))->ss()) {
                  if(res->ss() == 'H') {plik_H3  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_p1<<"\n";}
                  else if(res->ss() == 'E') {plik_E3  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_p1<<"\n";}
                  else if(res->ss() == 'C') {plik_C3  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_p1<<"\n";}
                }
              }

              if( (z<=ch_size-3)&&(it_SG->id() == ((*ch_sp)[z+2]->find_atom(" SG ")->id())-2) ) {                                                        //distance to atom n+2
                PdbAtom_SP SG_p2 = (ch_sp->get_residue(id_+2))->find_atom(" SG ");
                double dist_p2 = it_SG->distance_to(*SG_p2);
                plik0 <<"# "<<dist_p2<<" "<<(ch_sp->get_residue(id_+2))->ss()<<"-2 ";
                if(res->ss() == (ch_sp->get_residue(id_+2))->ss()) {
                  if(res->ss() == 'H') {plik_H4  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_p2<<"\n";}
                  else if(res->ss() == 'E') {plik_E4  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_p2<<"\n";}
                  else if(res->ss() == 'C') {plik_C4  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_p2<<"\n";}
                }
              }
              if( (z<=ch_size-4)&&(it_SG->id() == ((*ch_sp)[z+3]->find_atom(" SG ")->id())-3) ) {                                                        //distance to atom n+3
                  PdbAtom_SP SG_p3 = (ch_sp->get_residue(id_+3))->find_atom(" SG ");
                  double dist_p3 = r14x(*it_SG, *(ch_sp->get_residue(id_+1)->find_atom(" SG ")), *(ch_sp->get_residue(id_+2)->find_atom(" SG ")), *(ch_sp->get_residue(id_+3)->find_atom(" SG ")));
                  plik0 <<"# "<<dist_p3<<" "<<(ch_sp->get_residue(id_+3))->ss()<<"+3 ";
                  if(res->ss() == (ch_sp->get_residue(id_+3))->ss()) {
                    if(res->ss() == 'H') {plik_H5  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_p3<<"\n";}
                    else if(res->ss() == 'E') {plik_E5  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_p3<<"\n";}
                    else if(res->ss() == 'C') {plik_C5  <<argv[1]<<" "<<it_SG->atom_name()<<" "<<it_SG->id()<<" "<<res->residue_type().code3<<" "<<res->id()<<" "<<res->ss()<<" "<<dist_p3<<"\n";}
                  }
              }

              plik0 <<"\n";
      }
    plik0.close();
    plik_H1.close();
    plik_H2.close();
    plik_H3.close();
    plik_H4.close();
    plik_H5.close();
    plik_E1.close();
    plik_E2.close();
    plik_E3.close();
    plik_E4.close();
    plik_E5.close();
    plik_C1.close();
    plik_C2.close();
    plik_C3.close();
    plik_C4.close();
    plik_C5.close();
    }
}