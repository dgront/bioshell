#include <iostream>
#include <iomanip>
#include <vector>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/basic/Vec3.hh>

using namespace core::data::structural;
using namespace core::data::io;

bool min_dist(core::data::structural::Residue_SP, core::data::structural::Residue_SP);
bool min_dist(core::data::structural::Residue_SP res_sp1, core::data::structural::Residue_SP res_sp2) {
  double Pvdw1 = 1.55, Pvdw2 = 1.55;									//van der Waals radius
  std::string P = "PRO";
  const core::chemical::Monomer & m = (res_sp2)->residue_type();
      core::data::structural::selectors::IsBB is_bb;
  bool is_OK = true;
  for (auto it_at1 = res_sp1->begin(); it_at1!= res_sp1->end(); ++it_at1) {
  core::data::structural::PdbAtom_SP atom_sp1;
  atom_sp1 = (*it_at1);
  if (m.code3 != P) {continue;}
  if (is_bb(*atom_sp1) == true) {continue;}
  unsigned char A1 = atom_sp1->PdbAtom::element_index();
  if (A1 == 6) {Pvdw1 = 1.70;}
  else if (A1 == 7) {Pvdw1 = 1.55;}
  else if (A1 == 8) {Pvdw1 = 1.52;}
  else if (A1 == 1) {Pvdw1 = 1.20;}
  else if (A1 == 16) {Pvdw1 = 1.80;}
    for (auto it_at2 = res_sp2->begin(); it_at2!= res_sp2->end(); ++it_at2) {
      core::data::structural::PdbAtom_SP atom_sp2;
      atom_sp2 = (*it_at2);
      unsigned char A2 = atom_sp2->PdbAtom::element_index();
      if (A2 == 6) {Pvdw2 = 1.70;}
      else if (A2 == 7) {Pvdw2 = 1.55;}
      else if (A2 == 8) {Pvdw2 = 1.52;}
      else if (A2 == 1) {Pvdw2 = 1.20;}
      else if (A2 == 16) {Pvdw2 = 1.80;}
      double dist = atom_sp1->core::data::basic::Vec3::distance_to(*atom_sp2);
      if ( ((A1 == 1)&&(A2 == 8))||((A1 == 8)&&(A2 == 1))||((A1 == 1)&&(A2 == 7))||((A1 == 7)&&(A2 == 1)) ) {
	if (dist < 1.50) {
	    is_OK = false;
	    std::cerr<<dist<<" "<<(Pvdw1+Pvdw2-0.20)<<" Clash! between "<<atom_sp1->PdbAtom::atom_name()<<" "<<
	    atom_sp1->PdbAtom::id()<<" from residue "<<res_sp1->Residue::id()<<
	    " and "<<atom_sp2->PdbAtom::atom_name()<<" from residue "<<res_sp2->Residue::id()<<"\n";
	}
	if (dist >= 1.50) {is_OK = true;}
      }
      else if (dist <= (Pvdw1+Pvdw2-0.20)) {
	is_OK = false;
	std::cerr<<dist<<" "<<(Pvdw1+Pvdw2-0.20)<<" Clash! between "<<atom_sp1->PdbAtom::atom_name()<<" "<<
	    atom_sp1->PdbAtom::id()<<" from residue "<<res_sp1->Residue::id()<<
	    " and "<<atom_sp2->PdbAtom::atom_name()<<" from residue "<<res_sp2->Residue::id()<<"\n";
	break;
      }
      else if (dist > (Pvdw1+Pvdw2-0.20)) {is_OK = true;}
    }
    if (is_OK == false) {break;}
  }
  return is_OK;
}

int main(const int argc, const char* argv[]) {
    core::data::io::Pdb reader(argv[1], is_not_alternative); // file name (PDB format, may be gzip-ped)
    core::data::structural::Structure_SP strctr = reader.create_structure(0);
    std::vector < core::data::structural::Residue_SP > RESIDUES;
    core::data::structural::Residue_SP res_sp1;
    core::data::structural::Residue_SP res_sp2;
    bool is_ok = true;
    for (auto it_resid = strctr->first_residue(); it_resid!=strctr->last_residue(); ++it_resid) {
      res_sp1 = (*it_resid);
      core::index2 res1 = res_sp1->Residue::id();
      for (auto it_res = strctr->first_residue(); it_res!=strctr->last_residue(); ++it_res) {
        res_sp2 = (*it_res);
	core::index2 res2 = res_sp2->Residue::id();
	if (res1 == res2) {std::cerr<<"Residues i and j are the same!\n"; continue;}
	else if (res1 != res2) {
	  if (min_dist(res_sp1, res_sp2) == false) {is_ok = false; break;}
	  else if (min_dist(res_sp1, res_sp2) == true) {is_ok = true;}
	}
      }
      if (is_ok == true) {RESIDUES.push_back(res_sp1);}
    }
    for(unsigned short i = 0; i < RESIDUES.size(); i++) {
      for (auto it_atom = RESIDUES[i]->begin(); it_atom!= RESIDUES[i]->end(); ++it_atom) {
        core::data::structural::PdbAtom_SP atom_sp;
        atom_sp = (*it_atom);
        std::cout << atom_sp->PdbAtom::to_pdb_line() << std::endl;
      }
    }
}



