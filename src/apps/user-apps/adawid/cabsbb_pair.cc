#include <iostream>
#include <iomanip>
#include <fstream>

#include <core/algorithms/predicates.hh>
#include <core/calc/statistics/Histogram.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/io/Pdb.hh>
#include <utils/io_utils.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <simulations/representations/cabs/cabs_utils.hh>
#include <simulations/representations/cabs/CGRotamerDistribution.hh>
#include <simulations/forcefields/cabs/CabsRotamersEnergy.hh>
#include <simulations/systems/cabs/CabsBbAtomTyping.hh>

using namespace core::data::structural;
using namespace simulations::representations::cabs;

double AA_CONTACT_DISTANCE = 4.5;
double AA_CUTOFF_DISTANCE = 8.0;

// Create a map with indexes of amino acids names
std::map<std::string,std::shared_ptr<std::vector<double>>> map_in_contact;
std::map<std::string,std::shared_ptr<std::vector<double>>> map_out_contact;
std::vector<std::string> all_keys;

std::string key(char aa_code_i,core::index2 rot_id_i,char aa_code_j,core::index2 rot_id_j) {

  if ((rot_id_i > 9) || (rot_id_j > 9)) {
    std::cerr << "Suspicious key " << " " << aa_code_i << " " << rot_id_i << " " << aa_code_j << " " << rot_id_j << "\n";
  }
  return utils::string_format("%c%1d%c%1d",aa_code_i,rot_id_i,aa_code_j,rot_id_j);
}

std::string key(core::index2 atom_type_id, char aa_code_i,core::index2 rot_id_i) {

  return utils::string_format("%2d%c%1d",atom_type_id,aa_code_i,rot_id_i);
}

void process_one_pdb(const std::string & fname) {

  using namespace simulations::systems;
  using namespace simulations::representations::cabs;

  core::data::io::Pdb reader(fname, core::data::io::is_standard_atom);
  Structure_SP allatom_strctr_sp = reader.create_structure(0);
  selectors::IsAA is_aa;
  selectors::IsBBCB bb_cb_test;
  std::remove_if(allatom_strctr_sp->first_atom(), allatom_strctr_sp->last_atom(),
                 core::algorithms::DereferencedNot<selectors::AtomSelector>(is_aa));

  // ---------- Convert the Structure into CABSBB representation
  Structure_SP cabs_structure_sp = simulations::representations::cabsbb_representation(*allatom_strctr_sp);
  // ---------- Create the system and its energy
  AtomTypingInterface_SP mapping = std::make_shared<cabs::CabsBbAtomTyping>();
  ResidueChain_OBSOLETE<Vec3> rc(mapping, *cabs_structure_sp);
  simulations::forcefields::cabs::CabsRotamersEnergy<Vec3> CRE(rc);
  CRE.calculate();

  std::vector<core::index2> rotamer_types;
  std::vector<char> residue_type_code1;
  std::vector<core::data::basic::Vec3> sg_coords;
  core::index2 i_res_index = 0;

  for (auto it_res_aa = allatom_strctr_sp->first_residue(); it_res_aa != allatom_strctr_sp->last_residue(); ++it_res_aa) {

    if ((**it_res_aa).residue_type().is_standard()) residue_type_code1.push_back((**it_res_aa).residue_type().code1);
    else  residue_type_code1.push_back('X');

    if (residue_type_code1.back() == 'G' || residue_type_code1.back() == 'A'|| residue_type_code1.back() == 'X') {
      rotamer_types.push_back(256); // --- we don't expect more than 256 rotamers for a residue!
      sg_coords.emplace_back(0.0);
    } else {
      rotamer_types.push_back(CRE.recent_best_rotamer(i_res_index));
      char code = residue_type_code1.back();
      try {
        sg_coords.push_back(rc.find_atom(i_res_index, utils::string_format(" S%c1", code)));
      } catch (std::range_error e) {
        std::cerr << "Problem with residue "<<i_res_index<<" "<<code<<"\n";
        std::cerr << e.what()<<"\n";
      }
    }
    ++i_res_index;
  }

  // Iterate over all residues in the structure - outer loop
  i_res_index = 0;
  for (auto it_res_aa = allatom_strctr_sp->first_residue(); it_res_aa != allatom_strctr_sp->last_residue(); ++it_res_aa) {

    if (residue_type_code1[i_res_index] == 'X') continue;
    const Residue &i_res_aa = (**it_res_aa); // --- Residue in the all-atom structure : i-indexed

    // --- inner loop over all residues in the structure
    core::index2 j_res_index = 0;
    for (auto jt_res_aa = allatom_strctr_sp->first_residue(); jt_res_aa != allatom_strctr_sp->last_residue(); ++jt_res_aa) {
      if (i_res_index == j_res_index) break;
      if (residue_type_code1[j_res_index] == 'X') continue;

      const Residue &j_res_aa = (**jt_res_aa); // --- Residue in the all-atom structure : j-indexed
      double aa_distance = i_res_aa.min_distance(j_res_aa);
      float sg_distance = 100.0;
      for(auto i_a=i_res_aa.cbegin();i_a!=i_res_aa.cend();++i_a)
        for(auto j_a=j_res_aa.cbegin();j_a!=j_res_aa.cend();++j_a)
          if ((!bb_cb_test(**i_a)) && (!bb_cb_test(**j_a))) sg_distance = std::min(sg_distance,(*i_a)->distance_to(**j_a));

      if (aa_distance < AA_CUTOFF_DISTANCE) {

        bool if_contact = (sg_distance < AA_CONTACT_DISTANCE);

        char ci = residue_type_code1[i_res_index];
        char cj = residue_type_code1[j_res_index];
        core::index2 ri = rotamer_types[i_res_index];
        core::index2 rj = rotamer_types[j_res_index];

        if ((ri < 255) && (rj < 255)&& (i_res_index-j_res_index>1)) {
          try {
            if (if_contact )
              (*map_in_contact[key(ci, ri, cj, rj)]).push_back(sg_coords[i_res_index].distance_to(sg_coords[j_res_index]));
            else
              (*map_out_contact[key(ci, ri, cj, rj)]).push_back(sg_coords[i_res_index].distance_to(sg_coords[j_res_index]));
          } catch (std::out_of_range e) {
            std::cerr << key(ci, ri, cj, rj) << "\n";
          }
        }
        // SC atom vs all bbcb atoms both ways
        // SC atom vs SC atom
      }
      ++j_res_index;
    }
    ++i_res_index;
  }
}

void write_distances() {

  for (const auto &p : map_in_contact) {
    std::ofstream out("tp_"+p.first);
    std::vector<double> & v = *map_in_contact.at(p.first);
    std::sort(v.begin(), v.end());
    for (auto d : *p.second) out << d << "\n";
    out.close();
  }
  for (const auto &p : map_out_contact) {
    std::ofstream out("tn_"+p.first);
    std::vector<double> & v = *map_out_contact.at(p.first);
    std::sort(v.begin(), v.end());
    for (auto d : *p.second) out << d << "\n";
    out.close();
  }
}

void load_distances() {

  double r;
  for (const auto &p : map_in_contact) {
    std::ifstream in("tp_"+p.first);
    while (in >> r) (p.second)->push_back(r);
    in.close();
  }
  for (const auto &p : map_out_contact) {
    std::ifstream in("tn_"+p.first);
    while (in >> r) (p.second)->push_back(r);
    in.close();
  }
}

// Initialises <code>map_in_contact</code> container for results
void create_maps() {

  using namespace simulations::forcefields::cabs;

  const CGRotamersLibrary & rotamers = CGRotamersLibrary::get_instance();

  ///  \todo_code Write a method that creates an empty structure for an arbitrary AA sequence and call it here
  Structure_SP strctr = std::make_shared<Structure>("");
  strctr->push_back( std::make_shared<core::data::structural::Chain>(std::string{"A"}));
  strctr->back()->push_back(std::make_shared<core::data::structural::Residue>(1,"GLY"));
  auto resid = strctr->back()->back();
  resid->push_back(std::make_shared<core::data::structural::PdbAtom>(1," N  ", 7));
  resid->push_back(std::make_shared<core::data::structural::PdbAtom>(1," CA "));
  resid->push_back(std::make_shared<core::data::structural::PdbAtom>(1," C  "));
  resid->push_back(std::make_shared<core::data::structural::PdbAtom>(1," O  ", 8));
  simulations::systems::AtomTypingInterface_SP mapping = std::make_shared<simulations::systems::cabs::CabsBbAtomTyping>();
  simulations::systems::ResidueChain_OBSOLETE<Vec3> rc(mapping,*strctr);


  CabsRotamersEnergy<Vec3> CRE(rc);
  for (auto aa_it_i = core::chemical::Monomer::aa_cbegin(); aa_it_i != core::chemical::Monomer::aa_cend(); ++aa_it_i) {
    char id1 = (*aa_it_i).code1;
    core::index2 n_rot1 = rotamers.count_rotamers((*aa_it_i).id);
    for (core::index2 i_rot = 0; i_rot < n_rot1; ++i_rot) {
//      map_in_contact[key(6,id1, i_rot)] = std::vector<double>(); // C
//      map_in_contact[key(7,id1, i_rot)] = std::vector<double>(); // N
//      map_in_contact[key(8,id1, i_rot)] = std::vector<double>(); // O
      for (auto aa_it_j = core::chemical::Monomer::aa_cbegin(); aa_it_j != core::chemical::Monomer::aa_cend(); ++aa_it_j) {
        char id2 = (*aa_it_j).code1;
        core::index2 n_rot2 = rotamers.count_rotamers((*aa_it_j).id);
        for (core::index2 j_rot = 0; j_rot < n_rot2; ++j_rot) {
          std::string tmp_key = key(id2, j_rot, id1, i_rot);
          if (map_in_contact.find(tmp_key) == map_in_contact.end()) {
            auto vect_in_sp = std::make_shared<std::vector<double>>();
            map_in_contact[key(id1, i_rot, id2, j_rot)] = vect_in_sp;
            map_in_contact[tmp_key] = vect_in_sp;
            auto vect_out_sp = std::make_shared<std::vector<double>>();
            map_out_contact[key(id1, i_rot, id2, j_rot)] = vect_out_sp;
            map_out_contact[tmp_key] = vect_out_sp;
          }
        }
      }
    }
  }
}

int main(const int argc, const char* argv[]) {

  // --- initialize all the lists that are used to store distances
  create_maps();

//  int i=0;
//  for(const auto & p : map_in_contact) std::cerr << p.first<<(((++i)%10==9) ? "\n" : "") <<" ";

  for (int i = 1; i < argc; ++i) {
    // --- Did a user give a PDB file name as an argument?
    if ((utils::ends_with(argv[i], ".pdb")) || (utils::ends_with(argv[i], ".pdb.gz"))) process_one_pdb(argv[i]);
    else {
      // --- No, its a listfile; process each entry
      for(const std::string & fname : utils::read_listfile(argv[i]))
        process_one_pdb(fname);
    }
  }
std::cerr << map_in_contact.size()<<" keys in MAP\n";
  // --- Print results
  write_distances();
}
