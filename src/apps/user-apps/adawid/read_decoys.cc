#include <iostream>
#include <utils/io_utils.hh>
#include <utils/string_utils.hh>
#include <iomanip>
#include <core/algorithms/predicates.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/io/load_decoys.hh>
#include <core/data/structural/Structure.hh>
#include <utils/exit.hh>

std::string program_info = R"(

ex_Structure reads a PDB file and prints a list of all atoms grouped by residues they belong to
USAGE:
    ./ap_read_decoys tra.pdb models_list
    ./ap_read_decoys tra.pdb 100 500000

)";

int main(const int argc, const char* argv[]) {

    using namespace core::data::io;
    if (argc == 1) {
      std::cerr<<"USAGE:\n";
      std::cerr<<"	./ap_read_decoys tra.pdb models_list > out.pdb	 	#load only the models from the list given in 1-column file 'models_list'\n";
      std::cerr<<"	./ap_read_decoys tra.pdb 10 1000 > out.pdb		#load every n-th model from all N decoys; it is starting from the first\n";
    } else {
      std::vector<short unsigned int> models;
      std::string str;
      core::data::io::Pdb reader(argv[1],is_not_alternative); // file name (PDB format, may be gzip-ped)
      core::data::structural::Structure_SP strctr = reader.create_structure(0);
      core::index4 n_atoms = strctr->count_atoms();
      std::vector<core::data::basic::Coordinates_SP> destination;
      std::vector<std::string> model_names, words;

      if (argc > 3)  {
         std::cerr << "There will be loaded only the every "<<argv[2]<<"-th model!\n";
         unsigned int x = strtol(argv[2], NULL, 10);
         unsigned int y = strtol(argv[3], NULL, 10);
         models.push_back(1);
         for (unsigned int i = 1; i < y/x; ++i) {
           models.push_back(x*i+1);
         }
      } else {
        std::cerr << "There will be loaded only the models from list in file: "<<argv[2]<<"!\n";
        std::ifstream file(argv[2]);				//file with models' numbers
        while (std::getline(file, str)) {
          std::vector<std::string> tokens = utils::split(str,' ');   //model1
          if (tokens.size() == 1) {
            models.push_back(std::stod(tokens[0]));   //model no
          }
        }
      }
      std::string word = argv[1];
      words.push_back(word);
      core::data::io::read_decoys(words, n_atoms, destination, model_names, is_not_alternative, models);
      for (unsigned int i=0; i < destination.size(); ++i) {
        core::data::structural::coordinates_to_structure(*(destination[i]), *strctr);
        std::cout<<"MODEL"<<utils::string_format("%7d\n", models[i]);
        for (auto atom_sp = strctr->first_atom(); atom_sp != strctr->last_atom(); ++atom_sp) {
          std::cout<< (*atom_sp)->to_pdb_line() << "\n";
        }
        std::cout<<"ENDMDL\n";
      }
    }
}
