#ifndef BIOSHELL_HOMSTRADENTRY_HH
#define BIOSHELL_HOMSTRADENTRY_HH

#include <iostream>
#include <memory>
#include <vector>

#include <core/index.hh>

#include <core/data/io/pir_io.hh>
#include <core/data/io/Pdb.hh>
#include <core/alignment/MultipleSequenceAlignment.hh>
#include <core/alignment/on_alignment_computations.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/transformations/Crmsd.hh>

#include <utils/Logger.hh>

using namespace core::data::sequence;
using namespace core::data::structural;
using namespace core::alignment;
using namespace core::data::io;

namespace apps {
namespace user_apps {
namespace dgront {

class HomstradEntry;

class HomstradPairData {
public:
  const std::string & query_name;
  const std::string & tmplt_name;

  HomstradPairData(HomstradEntry & entry, const std::string & query_id, const std::string & tmplt_id);

  double crmsd() const { return crmsd_; }

  core::index2 count_identical() const { return n_identical_; }

  core::index2 count_query_gaps() const { return n_query_gaps_; }

  core::index2 count_tmplt_gaps() const { return n_tmplt_gaps_; }

  core::index2 tmplt_length() const { return t_len_; }
  core::index2 query_length() const { return q_len_; }

private:
  core::index1 query_index;
  core::index1 tmplt_index;
  core::index2 n_identical_;
  core::index2 q_len_;
  core::index2 t_len_;
  double crmsd_;
  double tm_score;
  core::index2 n_query_gaps_;
  core::index2 n_tmplt_gaps_;
  HomstradEntry & entry_;
};

std::ostream & operator<<(std::ostream & out, const HomstradPairData & e);

class HomstradEntry : public core::alignment::MultipleSequenceAlignment {
public:

  HomstradEntry(const std::string &fname);

  const std::string & get_key(const int which_one) const { return *(cbegin() + which_one); }
  const Sequence_SP get_sequence(int which_one) { return MultipleSequenceAlignment::get_sequence(get_key(which_one)); }
  const Sequence_SP get_sequence(const std::string & key) { return MultipleSequenceAlignment::get_sequence(key); }
  const std::string & get_aligned_sequence(int which_one) { return MultipleSequenceAlignment::get_aligned_sequence(get_key(which_one)); }

  void load_structures();

  void aligned_coordinates(core::index2 which_query,core::index2 which_tmplt,
                           std::vector<Residue_SP> & aligned_query,std::vector<Residue_SP> & aligned_tmplt) ;

  std::vector<HomstradPairData> pairwise_results;
  std::map<std::string,Chain_SP> structures;
private:
  utils::Logger l;
};


}
}
}

#endif //BIOSHELL_HOMSTRADENTRY_HH
