#include <fstream>
#include <string>

#include <core/index.hh>
#include <utils/exit.hh>
#include <utils/Logger.hh>
#include <core/data/io/fasta_io.hh>
#include <core/protocols/PairwiseSequenceIdentityProtocol.hh>

std::string program_info = R"(

Evaluates pairwise sequence identity between sequences found in a given FASTA file.

Calculations may be executed in several parallel threads, calculated values are printed on the screen if they
are greater than given cutoff.

By default, the program runs on 4 threads, with cutoff 0.28, i.e. printing only these pairs where sequence identity
is higher than 28%

USAGE:
./pairwise_identity_for_clustering in.fasta pairs_list [n_threads [cutoff] ]

EXAMPLEs:
./pairwise_identity_for_clustering small50_95identical.fasta pairs.txt 4 0.28


)";

int main(const int argc, const char* argv[]) {

  if(argc < 3) utils::exit_OK_with_message(program_info); // --- complain about missing program parameter

  using namespace core::data::sequence;
  using namespace core::protocols;
  using namespace core::alignment;

  utils::Logger logs("pairwise_identity_for_clustering");
  int my_argc = argc;

  core::index2 n_threads = (my_argc > 3) ? atoi(argv[3]) : 4;
  float cutoff = (my_argc > 4) ? atof(argv[4]) : 0.25;

  logs << utils::LogLevel::INFO << "number of threads used : " << n_threads << "\n";
  logs << utils::LogLevel::INFO << "seq. similarity cutoff : " << cutoff << "\n";

  core::protocols::PairwiseSequenceIdentityProtocol protocol;
  protocol.printed_seqname_length(5).gap_open(-14).gap_extend(-1).substitution_matrix("BLOSUM62").
      keep_alignments(false).alignment_method(AlignmentType::SEMIGLOBAL_ALIGNMENT).n_threads(n_threads);
  protocol.if_use_fasta_filter(false).seq_identity_cutoff(cutoff).batch_size(10000).printed_seqname_length(5);

  // ---------- Load input sequences
  std::vector<Sequence_SP> input_sequences;
  core::data::io::read_fasta_file(argv[1], input_sequences);
  for(Sequence_SP si: input_sequences) protocol.add_input_sequence(si);

  // ---------- Load the list of pairs that will be aligned
  core::index4 cnt_jobs = 0;
  std::ifstream myfile (argv[2]);
  if (!myfile.is_open()) {
    utils::exit_OK_with_message("Can't open input file with sequence pairs: " + std::string(argv[2]) + "\n");
    return 0;
  }
  std::string p1,p2;
  std::set<std::pair<std::string,std::string>> pairs;
  while(myfile) {
    myfile >> p1 >> p2;
    if (p1 > p2) pairs.insert({p1, p2});
    else pairs.insert({p2, p1});
    ++cnt_jobs;
    if ((cnt_jobs % 1000000 == 0) && cnt_jobs > 0)
      logs << utils::LogLevel::INFO << cnt_jobs << " pairs loaded\n";
  }
  for(auto p:pairs)
    protocol.add_pair(p.first, p.second);

  logs << utils::LogLevel::INFO << cnt_jobs << " pairs will be aligned\n";

  auto start = std::chrono::high_resolution_clock::now(); // --- timer starts!
  protocol.run();
  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
  logs << utils::LogLevel::INFO << (size_t ) protocol.n_jobs_completed()
       << " global alignment sequence identities calculated within " << time_span.count() << " [s]\n";

  protocol.print_header(std::cout);
  protocol.print_sequence_identity(std::cout);
}
