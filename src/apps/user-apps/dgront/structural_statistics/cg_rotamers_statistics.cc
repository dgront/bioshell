#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/angles.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <core/protocols/selection_protocols.hh>
#include <core/chemical/ChiAnglesDefinition.hh>

utils::Logger logger("cg_rotamers_statistics");

using namespace core::data::structural;
using namespace core::data::structural::selectors;

using core::calc::structural::transformations::local_coordinates_three_atoms;
using namespace core::calc::structural;

//ResidueHasBBCB has_full_bb_cb;
IsBBCB is_b_cb;
InverseAtomSelector select_sc(is_b_cb);
ResidueHasAllHeavyAtoms is_complete;
SelectChainBreaks at_chainbreak;

bool check_residue(const Residue& ri) {
  if (!is_complete(ri)) return false;
  if (at_chainbreak(ri)) return false;
  if (ri.residue_type()==core::chemical::Monomer::GLY) return false;
  if (ri.residue_type()==core::chemical::Monomer::ALA) return false;
  return true;
}

void get_cen_coordinates(const Residue & ri) {

  PdbAtom_SP n = ri.find_atom(" N  ");
  PdbAtom_SP ca = ri.find_atom(" CA ");
  PdbAtom_SP c = ri.find_atom(" C  ");
  PdbAtom_SP cb = ri.find_atom(" CB ");
  Residue_SP sc = ri.clone(select_sc);
  Vec3 ca_local, n_local, cb_local;
  Vec3 cen = sc->cm();

  auto lcs = local_coordinates_three_atoms(*n, *ca, *c);
  lcs->apply(cen);
  lcs->apply(*n, n_local);
  lcs->apply(*ca, ca_local);
  lcs->apply(*cb, cb_local);

  double r = cen.distance_to(cb_local);
  double a = evaluate_planar_angle(ca_local, cb_local, cen);
  double t = evaluate_dihedral_angle(n_local,ca_local,cb_local,cen);
  double phi = to_degrees(core::calc::structural::evaluate_phi(*ri.previous(), ri));
  double psi = to_degrees(core::calc::structural::evaluate_psi(ri, *ri.next()));
  double omega = to_degrees(core::calc::structural::evaluate_omega(ri, *ri.next()));
  std::string code = ri.owner()->owner()->code();
  std::cout << utils::string_format("%4s %s %4d %3s",
      code.c_str(), ri.owner()->id().c_str(), ri.id(), ri.residue_type().code3.c_str());
  std::cout << utils::string_format("   %c %c %6.3f %6.3f %6.3f   %7.2f %7.2f %7.2f  %6.4f %6.2f %7.4f %7.4f %4s    %7.2f",
      ri.residue_type().code1, ri.ss(), cen.x, cen.y, cen.z, phi, psi, omega, r, to_degrees(a), sin(t), cos(t),
      define_rotamer(ri).c_str(), to_degrees(t));

  for (unsigned short i = 1; i <= core::chemical::ChiAnglesDefinition::count_chi_angles(ri.residue_type()); ++i)
    std::cout << utils::string_format(" %6.1f", to_degrees(core::calc::structural::evaluate_chi(ri, i)));
  std::cout << "\n";
}

const std::string cg_rotamers_statistics_info = R"(
Prints coordinates of CEN atom in local coordinates system.

These local coordinates are used to define coarse-grained rotamers for each amino acid type
)";

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::structural;
  using namespace core::calc::structural;

  utils::options::OptionParser & cmd = OptionParser::get("cg_rotamers_statistics");
  cmd.register_option(utils::options::help, show_examples, verbose, db_path);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures
  cmd.register_option(keep_alternate, include_hydrogens);

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"print rotamer coordinates for each amino acid found in the given chains' set (list of pdb_id+chain_id)",
      "cg_rotamers_statistics -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./all/pdb/path/");
  cmd.program_info(cg_rotamers_statistics_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);
  std::cout << "#pdbid c  res id aa ss  x       y      z       phi     psi     omega     r  planar  sin(t)  cos(t)  rotamer   t       chi1  chi2   chi3  chi4\n";
  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for (auto ci: *s) ci->remove_ligands();   // --- remove ligands from each chain of a structure
//    std::ofstream outf(s->code() + s->get_chain(0)->id() + ".dat");
//    outf.close();
    for(auto c: *s) {
      for(auto r:*c) {
        if(check_residue(*r)) {
          get_cen_coordinates(*r);
        }
      }
    }
  }
}