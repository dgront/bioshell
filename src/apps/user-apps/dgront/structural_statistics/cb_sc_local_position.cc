#include <ostream>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/selectors/SelectPlanarCAGeometry.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/interactions/BackboneHBondMap.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/calc/structural/transformations/CartesianToSpherical.hh>


utils::Logger logger("ca_distances");

const std::string ca_distances_info = R"(
Calculates short range distances between C-alfa atoms
)";

using namespace core::data::structural;
using namespace core::calc::structural;

struct FragData;
typedef std::shared_ptr<FragData> FragData_SP;

struct FragData {
  std::string pdb_id;       ///< PDB_ID of the protein the observation come from
  std::string residue_type; ///< the type of the middle residue
  char res_icode, ss;       ///< the middle residue: its icode and secondary structure
  std::string chain_id;     ///< teh source chain
  int residue_id;           ///< middle residue: index
  double r24, r14, r25;     ///< three distances for the 5-residue long fragment
  double alfa, teta1, teta2;  ///< planar angle (for the middle residue) and two dihedral angles
  Vec3 cb_spherical;          ///< spherical coordinates of CB
  PdbAtom cb_local;         ///< Position of CB in local coordinate system
  PdbAtom sg_local;         ///< Position of SG in local coordinate system

  /// The five CA atoms of the fragment
  std::vector<PdbAtom_SP> ca_atoms;

  FragData() : ca_atoms(5, nullptr) {}

  static FragData_SP create_frag(Residue_SP r0,Residue_SP r1, Residue_SP r2, Residue_SP r3, Residue_SP r4);
};


typedef std::shared_ptr<FragData> FragData_SP;

FragData_SP FragData::create_frag(Residue_SP r0,Residue_SP r1, Residue_SP r2, Residue_SP r3, Residue_SP r4) {

  std::vector<Residue_SP> res_vect{ r0, r1, r2, r3, r4 };
  FragData_SP f = std::make_shared<FragData>();
  for (int i=0;i<5;++i) {
    f->ca_atoms[i] = res_vect[i]->find_atom(" CA ");
    if (f->ca_atoms[i] == nullptr) { return nullptr;}
  }
  for (int i = 1; i < 5; ++i) {
    if (f->ca_atoms[i]->distance_to(*f->ca_atoms[i - 1]) > 4.5)
      return nullptr;
  }
  PdbAtom_SP cb = r2->find_atom(" CB ");
  if (cb == nullptr) { return nullptr;}
  double d = f->ca_atoms[2]->distance_to(*cb);
  if (d < 1.43 || d > 1.63) return nullptr;

  core::data::structural::selectors::ResidueHasBBCB hasBbcb;
  core::data::structural::selectors::SelectPlanarCAGeometry if_flat;
  if (!hasBbcb(*r2)) return nullptr;
  if (if_flat(*r2)) return nullptr;

  f->r24 = f->ca_atoms[1]->distance_to(*f->ca_atoms[3]);
  f->r14 = f->ca_atoms[0]->distance_to(*f->ca_atoms[3]);
  f->r25 = f->ca_atoms[1]->distance_to(*f->ca_atoms[4]);

  f->alfa = core::calc::structural::evaluate_planar_angle(*f->ca_atoms[1], *f->ca_atoms[2], *f->ca_atoms[3]);
  f->teta2 = core::calc::structural::evaluate_dihedral_angle(*f->ca_atoms[1], *f->ca_atoms[2], *f->ca_atoms[3], *f->ca_atoms[4]);
  f->teta1 = core::calc::structural::evaluate_dihedral_angle(*f->ca_atoms[0], *f->ca_atoms[1], *f->ca_atoms[2], *f->ca_atoms[3]);

  f->residue_id = r2->id();
  f->residue_type = r2->residue_type().code3;
  f->pdb_id = r2->owner()->owner()->code();
  f->res_icode = r2->icode();
  f->chain_id = r2->owner()->id();
  f->ss = r2->ss();

  core::calc::structural::transformations::Rototranslation_SP rt =
      core::calc::structural::transformations::local_coordinates_three_atoms(*f->ca_atoms[1], *f->ca_atoms[2], *f->ca_atoms[3]);

  rt->apply(*cb, (f->cb_local));
  core::calc::structural::transformations::CartesianToSpherical to_spherical;
  to_spherical.apply(f->cb_local, f->cb_spherical);

  return f;
}

std::ostream &operator<<(std::ostream &out, FragData &fd) {
  using namespace core::calc::structural;
  // ---------- fragment info
  out << utils::string_format("%4s %2s %3s %4d%c %c  ", fd.pdb_id.c_str(), fd.chain_id.c_str(), fd.residue_type.c_str(),
      fd.residue_id, fd.res_icode, fd.ss);
  // ---------- local distances
//  out << utils::string_format("   %7.3f %7.3f %7.3f", fd.r24, fd.r14, fd.r25);
  // ---------- local angles
  out << utils::string_format("   %8.3f %8.3f %8.3f", fd.alfa, fd.teta1, fd.teta2);
  // ---------- local CB
  out << utils::string_format("   %7.3f %7.3f %7.3f", fd.cb_local.x, fd.cb_local.y, fd.cb_local.z);
  // ---------- local CB - spherical
  out << utils::string_format("   %6.3f %7.3f %7.3f", fd.cb_spherical.x, fd.cb_spherical.y, fd.cb_spherical.z);

  return out;
}

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;

  utils::options::OptionParser & cmd = OptionParser::get("ca_distances");
  cmd.register_option(utils::options::help, show_examples, verbose,db_path);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate short distances between CA",
      "cb_sc_local_position -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./pdb/");
  cmd.program_info(ca_distances_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);

  selectors::SelectChainBreaks chain_broken;
  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for(auto ci:*s) ci->remove_ligands();
    for(auto chain: *s) {
      for(int i=0;i<chain->size()-4;++i) {
        const Chain & c = *chain;
        auto fd = FragData::create_frag(c[i],c[i+1],c[i+2],c[i+3],c[i+4]);
        if (fd != nullptr)
          std::cout << *fd << "\n";
      }
    }
  }
}