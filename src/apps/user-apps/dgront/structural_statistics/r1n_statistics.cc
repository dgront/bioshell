#include <core/data/io/Pdb.hh>
#include <core/data/structural/ResidueSegmentProvider.hh>
#include <core/data/structural/selectors/ResidueSegmentSelector.hh>
#include <core/data/structural/Structure.hh>

#include <core/calc/structural/local_backbone_geometry.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>

utils::Logger logger("r1n_statistics");

const std::string r1n_statistics_info = R"(
Calculates R13, R14 and R15 distances to derive for CABS-like bonded force field
)";

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::structural;
  using namespace core::calc::structural;

  utils::options::Option frag_len_opt("-n", "-frag_length", "the length of each considered fragment");

  utils::options::OptionParser & cmd = OptionParser::get("r1n_statistics");
  cmd.register_option(utils::options::help, show_examples, verbose,db_path, frag_len_opt);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate R15 distances from PISCES data set; a list from PISCES server must be provided together with the path where PDB files are located",
      "r1n_statistics -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./pdb/ -n=5");
  cmd.add_example(id,"calculate R14 distances from a single PDB file",
      "r1n_statistics -in:pdb=1aba.pdb -n=5");
  cmd.program_info(r1n_statistics_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);

  core::index2 frag_len = option_value<core::index2>(frag_len_opt);

  core::calc::structural::ResidueSegmentGeometry_SP property;
  core::index2 ipos, jpos;
  switch (frag_len) {
    case 3:
      property = std::make_shared<class R13>(0);
      ipos = 0; jpos = 2;
      break;
    case 4:
      property = std::make_shared<class R14x>(0);
      ipos = 1; jpos = 2;
      break;
    case 5:
      property = std::make_shared<class R15>(0);
      ipos = 1; jpos = 3;
      break;
    default:
      std::cerr << "Fragment length must be either 3, 4 or 5\n";
      return 0;
  }

  selectors::HasProperlyConnectedCA is_connected;
  selectors::IsSSType is_helix(is_connected, 'H');
  selectors::IsSSType is_strand(is_connected, 'E');
  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    ResidueSegmentProvider rsp(s, frag_len);
    while(rsp.has_next()) {
      const ResidueSegment_SP  seg = rsp.next();
      if (is_connected(*seg)) {
//        auto ss = seg->sequence();
        const auto & ires = *(*seg)[ipos];
        const auto & jres = *(*seg)[jpos];

        std::cout << utils::string_format("%c%c %c%c %f | ", ires.residue_type().code1, jres.residue_type().code1,
            ires.ss(), jres.ss(), (*property)(*seg))<< *seg <<"\n";
      }
    }
  }

}