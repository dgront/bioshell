#include <ostream>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/interactions/BackboneHBondMap.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>


utils::Logger logger("ca_distances");

const std::string ca_distances_info = R"(
Calculates short range distances between C-alfa atoms
)";

using namespace core::data::structural;
using namespace core::calc::structural;


int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;

  utils::options::OptionParser & cmd = OptionParser::get("ca_distances");
  cmd.register_option(utils::options::help, show_examples, verbose,db_path);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate short distances between CA",
      "r1n_statistics -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./pdb/ -n=5");
  cmd.program_info(ca_distances_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);

  selectors::SelectChainBreaks chain_broken;
  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for(auto ci:*s) ci->remove_ligands();
    for (auto ri_it = s->first_const_residue();ri_it!=s->last_const_residue();++ri_it) {
      const Residue & ri = **ri_it;
      char ssi = ri.ss();
      if ( chain_broken(ri)) continue;
      auto ca_i = ri.find_atom(" CA ");
      if(ca_i== nullptr) continue;
      for (auto rj_it = s->first_const_residue();rj_it!=s->last_const_residue();++rj_it) {
        const Residue & rj = **rj_it;
        char ssj = rj.ss();
        if ( chain_broken(rj)) continue;
        if(abs(ri.id()-rj.id())<5) continue;
        auto ca_j = rj.find_atom(" CA ");
        if(ca_j== nullptr) continue;
        if(ca_i->distance_square_to(*ca_j)<25)
          std::cout << utils::string_format("%s %c %c %4d %c %2s %4d %c %2s %5d %f\n",
              s->code().c_str(), ssi, ssj, ri.id(), ri.residue_type().code1, ri.owner()->id().c_str(), rj.id(),
              rj.residue_type().code1, rj.owner()->id().c_str(), abs(ri.id() - rj.id()), ca_i->distance_to(*ca_j));
      }
    }
  }
}