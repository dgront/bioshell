#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/angles.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <core/protocols/selection_protocols.hh>

utils::Logger logger("gang_frag_input");

using namespace core::data::structural;

bool check_bb_fragment(const std::vector<PdbAtom_SP> &ca, core::index2 start, core::index2 len) {
  for (int i = 1; i < len; ++i) {
    double d = ca[i - 1 + start]->distance_to(*ca[i + start]);
    if (d > 4.0) return false;
  }
  return true;
}

struct FragData {
  core::index1 frag_len;
  std::string pdb_id;
  std::string chain_id;
  int first_res_id;
  char first_res_icode;
  std::string first_res_aa;
  std::string sequence;
  std::string secondary;
  std::vector<Vec3> atoms;
  std::vector<double> planars;
  std::vector<double> dihedrals;

  void calculate_angles() {
    using namespace core::calc::structural;

    planars.clear();
    dihedrals.clear();
    // ---------- the loop calculates planars and dihedrals over a fragment
    for (int i = 3; i < frag_len + 3; ++i) {
      double a = evaluate_planar_angle(atoms[i - 2], atoms[i - 1], atoms[i]);
      double t = evaluate_dihedral_angle(atoms[i - 3], atoms[i - 2], atoms[i - 1], atoms[i]);
      planars.push_back(a);
      dihedrals.push_back(t);
    }
  }

  double refold_error() {
    using namespace core::calc::structural;
    std::vector<Vec3> at;
    for (int i = 0; i < 3; ++i) at.push_back(atoms[i]);
    for (int i = 3; i < frag_len + 3; ++i) {
      Vec3 p;
      z_matrix_to_cartesian(at[i - 3], at[i - 2], at[i - 1], 3.8, planars[i - 3], dihedrals[i - 3], p);
      at.push_back(p);
    }
    double d = at.back().distance_to(atoms[frag_len + 3]);
    return d;
  }
};

std::ostream &operator<<(std::ostream &out, FragData &fd) {
  using namespace core::calc::structural;
  // ---------- fragment info
  out << utils::string_format("%4s %s %3s %4d%c %s %s ", fd.pdb_id.c_str(), fd.chain_id.c_str(), fd.first_res_aa.c_str(),
      fd.first_res_id, fd.first_res_icode, fd.sequence.c_str(), fd.secondary.c_str());

  // ---------- spanning vector
  out << utils::string_format("   %7.3f %7.3f %7.3f", fd.atoms[fd.frag_len + 2].x - fd.atoms[3].x,
      fd.atoms[fd.frag_len + 2].y - fd.atoms[3].y, fd.atoms[fd.frag_len + 2].z - fd.atoms[3].z);

  // ---------- the loop over planars and dihedrals for a fragment
  for (int i = 0; i < fd.frag_len; ++i)
    out << utils::string_format("   %7.3f %8.3f", to_degrees(fd.planars[i]), to_degrees(fd.dihedrals[i]));

  // ---------- print coordinates for sticky tails atoms (overlaps)
  std::vector<Vec3>& a = fd.atoms;
  out << utils::string_format("  | %8.3f %8.3f %8.3f  %8.3f %8.3f %8.3f  %8.3f %8.3f %8.3f", a[0].x,a[0].y,a[0].z,
      a[1].x,a[1].y,a[1].z, a[2].x,a[2].y,a[2].z);
  core::index1 n = fd.frag_len - 1;
  out << utils::string_format(" | %8.3f %8.3f %8.3f  %8.3f %8.3f %8.3f  %8.3f %8.3f %8.3f", a[n].x,a[n].y,a[n].z,
      a[n-1].x,a[n-1].y,a[n-1].z, a[n-2].x,a[n-2].y,a[n-2].z);

  return out;
}

bool compute_gangfrag_features(const Structure &source, const core::index2 frag_len, std::ostream & out) {

  using namespace core::calc::structural;

  bool result = true;

  // ---------- clone the whole structure as CA-only
  ::selectors::IsCA is_ca;
  Structure_SP ca_only = source.clone();
  core::protocols::keep_selected_atoms(is_ca, *ca_only);

  // ---------- Process each CA-only chain separately
  for(Chain_SP chain_sp: *ca_only) {
    std::string code = ca_only->code();
    // ---------- If a clone has fewer residues than the source, skip it (some residues miss their CA atom)
    if (chain_sp->count_residues() != source.get_chain(chain_sp->id())->count_residues()) {
      logger << utils::LogLevel::WARNING << code << " chain " << chain_sp->id() << " must have missing CA atoms, skipped\n";
      return false;
    }
    // ---------- Copy CA atoms to a separate vector that will be sent to a function
    std::vector<PdbAtom_SP> ca;
    for (auto a_it = chain_sp->first_const_atom(); a_it != chain_sp->last_const_atom(); ++a_it)
      ca.push_back(*a_it);

    std::string sequence = chain_sp->create_sequence()->sequence;  // --- sequence of the current fragment
    std::string secondary = chain_sp->create_sequence()->str();    // --- its secondary structure
    core::index2 len = ca.size();

    out << "#PDB c  aa res_id frag_seq     frag_ss        dx      dy      dz     angles-planar_dihedral-planar_dihedral\n";
    for (core::index2 which_pos = 3; which_pos < len - frag_len - 3; ++which_pos) {
      if (!check_bb_fragment(ca, which_pos - 3, frag_len + 6)) {
        logger << utils::LogLevel::WARNING << "Chain break at pos " << which_pos << " in " << code << "\n";
        continue;
      }
      // ---------- create output data frame and start filling it up
      const Residue & r = *ca[which_pos]->owner();
      FragData current_frag_data;
      current_frag_data.frag_len = frag_len;
      current_frag_data.pdb_id = code;
      current_frag_data.chain_id = chain_sp->id();
      current_frag_data.first_res_id = r.id();
      current_frag_data.first_res_icode = r.icode();
      current_frag_data.first_res_aa = r.residue_type().code3.c_str();
      // ---------- sequence of a fragment, including 3 aa overhang at each end
      current_frag_data.sequence = sequence.substr(which_pos - 3, frag_len + 6);
      // ---------- secondary structure of a fragment, including 3 aa overhang at each end
      current_frag_data.secondary = secondary.substr(which_pos - 3, frag_len + 6);

      // ---------- copy CA positions, including tails
      for (int i = -3; i < frag_len + 3; ++i) {
        current_frag_data.atoms.push_back(*ca[i+which_pos]);
      }
      current_frag_data.calculate_angles();
      double err =  current_frag_data.refold_error();
      if (fabs(err-3.8) > 0.2) {
        logger << utils::LogLevel::WARNING
               << utils::string_format("Refolding error too high for pos %d in %s %s: %f\n", which_pos, code.c_str(),
                   current_frag_data.chain_id.c_str(), fabs(err - 3.8));
        continue;
      }
      out << current_frag_data<<"\n";
    }
  }

  return result;
}

const std::string gang_frag_input_info = R"(
Generates input features for GAN for generating fragments
)";

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::structural;
  using namespace core::calc::structural;

  utils::options::Option frag_len_opt("-n", "-frag_length", "the length of a considered fragment (the number of moved residues)");
  utils::options::OptionParser & cmd = OptionParser::get("gang_frag_input");
  cmd.register_option(utils::options::help, show_examples, verbose, db_path, frag_len_opt);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate all features for fragments of 5 amino acids extracted from the given set (list of pdb_id+chain_id)",
      "gang_frag_input -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./all/pdb/path/ -n=5");
  cmd.add_example(id,"calculate only local distances from a single PDB file, for fragments of 11 residues",
      "gang_frag_input -in:pdb=1aba.pdb -r -n=11");
  cmd.program_info(gang_frag_input_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);
  ::selectors::IsCA is_ca;
  core::index2 frag_len = utils::options::option_value<core::index2>(frag_len_opt);
  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for (auto ci: *s) ci->remove_ligands();   // --- remove ligands from each chain of a structure
    std::ofstream outf(s->code() + s->get_chain(0)->id() + ".dat");
    compute_gangfrag_features(*s, frag_len, outf);
    outf.close();
  }
}