#include <ostream>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/selectors/SelectPlanarCAGeometry.hh>
#include <core/data/structural/Structure.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/select_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>
#include <core/calc/structural/protein_angles.hh>


utils::Logger logger("dpack_input");

const std::string ca_distances_info = R"(
Calculates input data to train the deep-packer model
)";

using namespace core::data::structural;
using namespace core::calc::structural;
using core::chemical::Monomer;

struct DPData;
typedef std::shared_ptr<DPData> DPData_SP;


class OneHotEncoder {
public:
  OneHotEncoder& add_to_map(char c, core::index1 pos) {
    mapping[c] = pos;
    return *this;
  }

  std::string encode(char c) {
    std::string out = "";
    for (int i = 0; i < mapping.size(); ++i) out += " 0";
    out[mapping[c] * 2 + 1] = '1';

    return out;
  }

private:
  std::map<char, core::index1> mapping;
};

OneHotEncoder encode_aa;
OneHotEncoder encode_ss;


struct DPData {
  std::string pdb_id;       ///< PDB_ID of the protein the observation come from
  std::string residue_type; ///< the type of the middle residue
  char res_icode, ss;       ///< the middle residue: its icode and secondary structure
  std::string chain_id;     ///< the source chain
  int residue_id;           ///< middle residue: index
  double phi, psi;          ///< bb dihedrals
  int if_cis;               ///< 1 if cis-peptide bond, 0 if trans
  std::vector<double> chi;  ///< planar angle (for the middle residue) and two dihedral angles

  static DPData_SP create(Residue_SP prev_res,Residue_SP the_res, Residue_SP next_res);
  static bool check(Residue_SP prev_res,Residue_SP the_res, Residue_SP next_res);
};

bool DPData::check(Residue_SP prev_res, Residue_SP the_res, Residue_SP next_res) {
  core::data::structural::selectors::ResidueHasAllHeavyAtoms check_atoms;
  core::data::structural::selectors::SelectPlanarCAGeometry if_flat;
  core::data::structural::selectors::SelectChainBreaks is_broken;

  std::vector<Residue_SP> resids = {prev_res, the_res, next_res};
  for(const auto r: resids) {
    if (is_broken(r)) return false;
  }
  for (const auto r: resids) {
    if (r->residue_type().id != core::chemical::Monomer::GLY.id && if_flat(*r)) return false;
    if (is_broken(r)) return false;
  }

  return true;
}



DPData_SP DPData::create(Residue_SP prev_res,Residue_SP the_res, Residue_SP next_res) {

  DPData_SP f = std::make_shared<DPData>();
  f->phi = core::calc::structural::evaluate_phi(*prev_res, *the_res);
  f->psi = core::calc::structural::evaluate_psi(*the_res, *next_res);
  double omega = core::calc::structural::evaluate_omega(*the_res, *next_res);
  f->if_cis = int(fabs(omega) < 1.59);
  f->residue_id = the_res->id();
  f->residue_type = the_res->residue_type().code3;
  f->pdb_id = the_res->owner()->owner()->code();
  f->res_icode = the_res->icode();
  f->chain_id = the_res->owner()->id();
  f->ss = the_res->ss();
  core::calc::structural::evaluate_chi(*the_res, f->chi);
  while (f->chi.size() < 4) f->chi.push_back(0.0);

  return f;
}

std::ostream &operator<<(std::ostream &out, DPData &fd) {

  std::string aa_str = encode_aa.encode(Monomer::get(fd.residue_type).code1);
  std::string ss_str = encode_ss.encode(fd.ss);
  // ---------- fragment info
  out << utils::string_format("%4s %2s %3s %4d%c %c  ", fd.pdb_id.c_str(), fd.chain_id.c_str(), fd.residue_type.c_str(),
      fd.residue_id, fd.res_icode, fd.ss);
  // ---------- backbone dihedral angles
  out << utils::string_format("   %8.3f %8.3f %1d %s %s  ", fd.phi, fd.psi, fd.if_cis, aa_str.c_str(), ss_str.c_str());
  // ---------- sidechain dihedral angles
  out << utils::string_format("   %8.3f %8.3f %8.3f %8.3f", fd.chi[0], fd.chi[1], fd.chi[2], fd.chi[3]);

  return out;
}


int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;

  utils::options::OptionParser & cmd = OptionParser::get("dpack_input");
  cmd.register_option(utils::options::help, show_examples, verbose,db_path);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures
  cmd.register_option(select_chains);

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate deep-packer training data from the whole PISCES set",
      "dpack_input -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./pdb/");
  cmd.add_example(id,"calculate deep-packer training data from a single PDB file in a local folder",
      "dpack_input -in:pdb=2gb1.pdb");
  cmd.program_info(ca_distances_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  encode_ss.add_to_map('H', 0).add_to_map('E', 1).add_to_map('C', 2);
  for (core::index2 i = 0; i < 20; ++i) encode_aa.add_to_map(Monomer::get(i).code1, i);

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);

  selectors::SelectChainBreaks chain_broken;
  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for(auto ci:*s) ci->remove_ligands();
    for(auto chain: *s) {
      for (int i = 0; i < chain->size() - 2; ++i) {
        const Chain &c = *chain;
        if (!DPData::check(c[i], c[i + 1], c[i + 2])) continue;
        std::cout << *DPData::create(c[i], c[i + 1], c[i + 2]) << "\n";
      }
    }
  }
}