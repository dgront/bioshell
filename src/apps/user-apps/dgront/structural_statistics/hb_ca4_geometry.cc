#include <ostream>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/interactions/BackboneHBondMap.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>


utils::Logger logger("hb_ca4_geometry");

const std::string r1n_statistics_info = R"(
Calculates local geometry of a H-bond in a CA-only model
)";

using namespace core::data::structural;
using namespace core::calc::structural;

struct HBType {
  char type;
  double avg_y, avg_z;
};

struct HBGeometry {
  double d_aa, d_bb, d_ab, d_ba;
  char type;
  std::string chain;
  int i_residue_id;
  char i_i_code;
  int j_residue_id;
  char j_i_code;
};



std::ostream & operator<<(std::ostream & out, const HBGeometry & hb) {
  out << utils::string_format("%s %4d%c %4d%c %c %7.4f %7.4f %7.4f %7.4f",
      hb.chain.c_str(), hb.i_residue_id, hb.i_i_code, hb.j_residue_id, hb.j_i_code,
      hb.type, hb.d_aa, hb.d_bb, hb.d_ab, hb.d_ba);

  return out;
}
std::string header = "#type   dAA       dBB       dAB       dBA\n";

HBGeometry hb_geometry(PdbAtom &i, PdbAtom &i_next, PdbAtom &j, PdbAtom &j_next, const char hb_type) {

  HBGeometry hb;
  hb.type = hb_type;
  hb.d_aa = i.distance_to(j);
  hb.d_bb= i_next.distance_to(j_next);
  hb.d_ab = i.distance_to(j_next);
  hb.d_ba= i_next.distance_to(j);
  hb.i_residue_id = i.owner()->id();
  hb.i_i_code = i.owner()->icode();
  hb.j_residue_id = j.owner()->id();
  hb.j_i_code = j.owner()->icode();
  hb.chain = j.owner()->owner()->id();

  return hb;
}

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;

  utils::options::OptionParser & cmd = OptionParser::get("hb_ca4_geometry");
  cmd.register_option(utils::options::help, show_examples, verbose,db_path);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate H-bond geometry for a PISCES data set; a list from PISCES server must be provided together with the path where PDB files are located",
      "ca_hb_geometry -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./pdb/");
  cmd.add_example(id,"calculate H-bond geometry from a single PDB file", "ca_hb_geometry -in:pdb=2gb1.pdb");
  cmd.program_info(r1n_statistics_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);


  selectors::SelectChainBreaks chain_broken;
  std::cout << header;
  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for(auto ci:*s) ci->remove_ligands();
    // ---------- create a hydrogen bond map
    interactions::BackboneHBondMap hbmap(*s);
    core::index2 max_pos = hbmap.length();
    std::vector<HBGeometry> obs;

    // ---------- This block detects all H-bonds in sheets (E)
    for (core::index2 i_pos = 0; i_pos < max_pos; ++i_pos) {
      const Residue_SP ri = hbmap.residue(i_pos);
      if ((ri->ss() != 'E') || (chain_broken(ri))) continue;
      for (core::index2 j_pos = 0; j_pos < max_pos; ++j_pos) {
        if(i_pos==j_pos) continue;
        const Residue_SP rj = hbmap.residue(j_pos);
        if ((rj->ss() != 'E') || (chain_broken(rj))) continue;
        if(hbmap.is_antiparallel_bridge(i_pos, j_pos)) {
          try{
            PdbAtom_SP inca=ri->next()->find_atom(" CA ");
            PdbAtom_SP ica=ri->find_atom(" CA ");
            PdbAtom_SP jpca=rj->previous()->find_atom(" CA ");
            PdbAtom_SP jca=rj->find_atom(" CA ");
            obs.push_back(hb_geometry(*ica, *inca, *jca, *jpca, 'A'));
          } catch (...) {
            std::cerr <<"ERROR 1\n";
          }
        } else if(hbmap.is_parallel_bridge(i_pos, j_pos)) {
          try {
            PdbAtom_SP inca=ri->next()->find_atom(" CA ");
            PdbAtom_SP ica=ri->find_atom(" CA ");
            PdbAtom_SP jnca=rj->next()->find_atom(" CA ");
            PdbAtom_SP jca=rj->find_atom(" CA ");
            obs.push_back(hb_geometry(*ica, *inca, *jca, *jnca, 'P'));
          } catch (...) {
            std::cerr <<"ERROR 2\n";
          }
        }
      }
    }
    // ---------- This block detects all H-bonds in helices (H)
    for (core::index2 i_pos = 4; i_pos < max_pos; ++i_pos) {
      const Residue_SP ri = hbmap.residue(i_pos);
      if ((ri->ss() != 'H') || (chain_broken(ri))) continue;
      const Residue_SP rj = hbmap.residue(i_pos-3);             // H bond in alha helix is 1-5, but in terms of CA it's 1-4 !!!
      if ((rj->ss() != 'H') || (chain_broken(rj))) continue;

      if (hbmap.are_H_bonded(i_pos, i_pos - 4)) {               // again, we check 1-5 bonding but measure 1-4 CA geometry
        try {
          PdbAtom_SP inca=ri->next()->find_atom(" CA ");
          PdbAtom_SP ica=ri->find_atom(" CA ");
          PdbAtom_SP jnca=rj->next()->find_atom(" CA ");
          PdbAtom_SP jca=rj->find_atom(" CA ");
          core::index2 in_pos = hbmap.residue_index(*ri->next());
          if (hbmap.are_H_bonded(in_pos, in_pos - 4))
            obs.push_back(hb_geometry(*ica, *inca, *jca, *jnca, 'H'));
        } catch (...) {
          std::cerr <<"ERROR H\n";
        }
      }
    }
    // ---------- print observations
    for(const auto & hb:obs)
      std::cout << s->code() << " " << hb << "\n";
  }

}