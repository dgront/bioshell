#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/angles.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <core/protocols/selection_protocols.hh>

utils::Logger logger("frag_generator_training_set");

using namespace core::data::structural;
core::data::structural::selectors::ResidueHasBB has_bb;

std::string check_bb_fragment(const std::vector<Residue_SP> &resids, core::index2 start, core::index2 len) {
  std::ostringstream err;
  for (int i = 0; i < len; ++i) {
    if (!has_bb(resids[i + start])) {
      err << "missing backbone atom at pos " << (i+start) <<"\n";
    }
  }
  for (int i = 1; i < len; ++i) {
    auto prev_ca = resids[i - 1 + start]->find_atom(" CA ");
    auto the_ca = resids[i + start]->find_atom(" CA ");
    if ((prev_ca == nullptr) || (the_ca == nullptr)) {
      return err.str();
    }
    double d = the_ca->distance_to(*prev_ca);
    if (d > 4.0) {
      err << "bad distance between pos " << (i+start-1) << " and " << (i+start) <<": " << d <<"\n";
    }
  }
  return err.str();
}

struct FragData {
  core::index1 frag_len;
  std::string pdb_id;
  std::string chain_id;
  std::string sequence;
  std::string secondary;
  std::vector<Residue_SP> residues; // --- frag_len + 2 residues
};

std::ostream &operator<<(std::ostream &out, FragData &fd) {
  using namespace core::calc::structural;
  // ---------- fragment info
  out << utils::string_format("%4s %s %s %s ", fd.pdb_id.c_str(), fd.chain_id.c_str(),
      fd.sequence.c_str(), fd.secondary.c_str());

  // ---------- N-terminal reference frame:   c(i-1), n(i),   ca(i)
  auto prev_C = *fd.residues[0]->find_atom_safe(" C  ");
  out << utils::string_format("   %8.3f %8.3f %8.3f", prev_C.x, prev_C.y, prev_C.z);
  auto the_N = *fd.residues[1]->find_atom_safe(" N  ");
  out << utils::string_format("   %8.3f %8.3f %8.3f", the_N.x, the_N.y, the_N.z);
  auto the_CA = *fd.residues[1]->find_atom_safe(" CA ");
  out << utils::string_format("   %8.3f %8.3f %8.3f", the_CA.x, the_CA.y, the_CA.z);

  // ---------- C-terminal reference frame:   c(i),   n(i+1), ca(i+1)
  auto the_C = *fd.residues[fd.frag_len]->find_atom_safe(" C  ");
  out << utils::string_format("   %8.3f %8.3f %8.3f", the_C.x, the_C.y, the_C.z);
  auto next_N = *fd.residues[fd.frag_len + 1]->find_atom_safe(" N  ");
  out << utils::string_format("   %8.3f %8.3f %8.3f", next_N.x, next_N.y, next_N.z);
  auto next_CA = *fd.residues[fd.frag_len + 1]->find_atom_safe(" CA ");
  out << utils::string_format("   %8.3f %8.3f %8.3f", next_CA.x, next_CA.y, next_CA.z);


  // ---------- the loop over planars and dihedrals for a fragment
  for (int i = 1; i <= fd.frag_len; ++i) {
    double phi = core::calc::structural::evaluate_phi(*fd.residues[i-1], *fd.residues[i]);
    double psi = core::calc::structural::evaluate_psi(*fd.residues[i], *fd.residues[i+1]);
    double omega = core::calc::structural::evaluate_omega(*fd.residues[i], *fd.residues[i+1]);
    out << utils::string_format("   %8.3f %8.3f %8.3f", to_degrees(phi), to_degrees(psi), to_degrees(omega));
  }

  return out;
}

bool compute_gangfrag_features(const Structure &source, const core::index2 frag_len, std::ostream & out) {

  using namespace core::calc::structural;

  bool result = true;

  // ---------- clone the whole structure as CA-only
  ::selectors::IsBB is_bb;
  Structure_SP bb_only = source.clone();
  core::protocols::keep_selected_atoms(is_bb, *bb_only);

  // ---------- Process each CA-only chain separately
  for(Chain_SP chain_sp: *bb_only) {
    std::string code = bb_only->code();
    // ---------- If a clone has fewer residues than the source, skip it (some residues miss their CA atom)
    if (chain_sp->count_residues() != source.get_chain(chain_sp->id())->count_residues()) {
      logger << utils::LogLevel::WARNING << code << " chain " << chain_sp->id() << " must have missing CA atoms, skipped\n";
      return false;
    }
    // ---------- Copy CA atoms to a separate vector that will be sent to a function
    std::vector<Residue_SP> resids;
    for (auto r_it: *chain_sp)
      resids.push_back(r_it);

    std::string sequence = chain_sp->create_sequence()->sequence;  // --- sequence of the current fragment
    std::string secondary = chain_sp->create_sequence()->str();    // --- its secondary structure
    core::index2 len = resids.size();
           //     20.532  -20.299   -5.054     20.901  -20.444   -6.326     20.632  -19.393   -7.295     19.498  -24.585  -12.754     19.463  -23.475  -13.486     19.215  -23.514  -14.925   -134.196  150.184 -177.492    -56.452  -47.661 -178.358    -65.063  -35.132  179.673    -62.300  -47.195  179.279    -62.328  -40.995  179.628

    std::ostringstream header_format;
    header_format << "#PDB c  %" << frag_len << "s %" << frag_len << "s  ";
    out << utils::string_format(header_format.str().c_str(),"frag_seq","frag_ss");
    //     " 12345678 12345678 12345678 12345678 12345678 12345678 12345678 12345678 12345678"
    out << "  C_x(-1)  C_y(-1)  C_z(-1)    N_x(0)   N_y(0)   N_z(0)     CA_x(0)  CA_y(0)  CA_z(0)";
    out << "  C_x(n-1) C_y(n-1) C_z(n-1)   N_x(n)   N_y(n)   N_z(n)     CA_x(n)  CA_y(n)  CA_z(n)";
    for (int i=0;i<frag_len;++i) out << "     phi      psi     omega  ";
    out << "\n";
    for (core::index2 which_pos = 1; which_pos < len - frag_len - 1; ++which_pos) {
      auto err = check_bb_fragment(resids, which_pos - 1, frag_len + 2);
      if (err.length() > 0) {
        logger << utils::LogLevel::WARNING << "Bad fragment at pos " << which_pos << " in " << code << ":\n" << err;
        continue;
      }
      // ---------- create output data frame and start filling it up
      FragData current_frag_data;
      current_frag_data.frag_len = frag_len;
      current_frag_data.pdb_id = code;
      current_frag_data.chain_id = chain_sp->id();
      // ---------- sequence of a fragment, including 3 aa overhang at each end
      current_frag_data.sequence = sequence.substr(which_pos - 1, frag_len + 2);
      // ---------- secondary structure of a fragment, including 3 aa overhang at each end
      current_frag_data.secondary = secondary.substr(which_pos - 1, frag_len + 2);

      // ---------- copy CA positions, including tails
      for (int i = -1; i < frag_len + 1; ++i) {
        current_frag_data.residues.push_back(resids[i+which_pos]);
      }

      out << current_frag_data<<"\n";
    }
  }

  return result;
}

const std::string gang_frag_input_info = R"(
Generates input features for GAN for generating fragments
)";

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::structural;
  using namespace core::calc::structural;

  utils::options::Option frag_len_opt("-n", "-frag_length", "the length of a considered fragment (the number of moved residues)");
  utils::options::OptionParser & cmd = OptionParser::get("frag_generator_training_set");
  cmd.register_option(utils::options::help, show_examples, verbose, db_path, frag_len_opt);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate all features for fragments of 5 amino acids extracted from the given set (list of pdb_id+chain_id)",
      "frag_generator_training_set -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./all/pdb/path/ -n=5");
  cmd.add_example(id,"calculate only local distances from a single PDB file, for fragments of 11 residues",
      "frag_generator_training_set -in:pdb=1aba.pdb -r -n=11");
  cmd.program_info(gang_frag_input_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);
  core::index2 frag_len = utils::options::option_value<core::index2>(frag_len_opt);
  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for (auto ci: *s) ci->remove_ligands();   // --- remove ligands from each chain of a structure
    std::ofstream outf(s->code() + s->get_chain(0)->id() + ".dat");
    compute_gangfrag_features(*s, frag_len, outf);
    outf.close();
  }
}