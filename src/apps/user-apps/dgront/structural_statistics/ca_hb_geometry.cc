#include <ostream>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/interactions/BackboneHBondMap.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>


utils::Logger logger("ca_hb_geometry");

const std::string r1n_statistics_info = R"(
Calculates local geometry of a H-bond in a CA-only model
)";

using namespace core::data::structural;
using namespace core::calc::structural;

struct HBType {
  char type;
  double avg_y, avg_z;
};

struct HBGeometry {
  double x, y, z;
  double r, a;
  double d_prev, d, d_next;
  HBType type;
};


HBType PARALLEL {'P', 0.0554903, -0.364022};        // --- for parallel paired strands
HBType ANTIPARALLEL {'A', -0.204107, -0.63542};     // --- for antiparallel paired strands
HBType HELICAL{'H', -0.4, -1.3};                         // --- for helices
HBType helical{'h', -0, 0};                         // --- for helices - oposite direction

std::ostream & operator<<(std::ostream & out, const HBGeometry & hb) {
  out << hb.type.type;
  out << utils::string_format(" %7.4f %7.4f %7.4f %7.4f %7.4f   %5.3f %5.3f %5.3f",
      hb.x, hb.y, hb.z, hb.r, hb.a, hb.d_prev, hb.d, hb.d_next);
  return out;
}
std::string header = "#type   x       y       z       r       a  d_prev     d  d_nrxt\n";

HBGeometry hb_geometry(PdbAtom &i_prev, PdbAtom &i, PdbAtom &i_next, PdbAtom &j_prev, PdbAtom &j, PdbAtom &j_next,
    const HBType & hb_type) {

  double avg_y = hb_type.avg_y; // --- for parallel, then antiparallel
  double avg_z = hb_type.avg_z;
  auto i_rt_sp = transformations::local_coordinates_three_atoms(i_prev, i, i_next);
  auto j_rt_sp = transformations::local_coordinates_three_atoms(j_prev, j, j_next);
  Vec3 copy;
  i_rt_sp->apply(j,copy);
//  j_rt_sp->apply(i,copy);

  HBGeometry hb;
  hb.x = copy.x;
  hb.y = copy.y;
  hb.z = copy.z;
  hb.type = hb_type;
  hb.d_prev = i_prev.distance_to(j_prev);
  hb.d_next = i_next.distance_to(j_next);
  hb.d = i.distance_to(j);
  copy.y-=avg_y;
  copy.z-=avg_z;
  hb.r = sqrt(copy.y * copy.y + copy.z * copy.z);
  hb.a = atan2(copy.z, copy.y);
  if (hb.a < 0) hb.a += 2 * M_PI;
  if (hb.a < M_PI / 2.0) hb.a += 2 * M_PI;

  return hb;
}

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;

  utils::options::OptionParser & cmd = OptionParser::get("ca_hb_geometry");
  cmd.register_option(utils::options::help, show_examples, verbose,db_path);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate H-bond geometry for a PISCES data set; a list from PISCES server must be provided together with the path where PDB files are located",
      "ca_hb_geometry -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./pdb/");
  cmd.add_example(id,"calculate H-bond geometry from a single PDB file", "ca_hb_geometry -in:pdb=2gb1.pdb");
  cmd.program_info(r1n_statistics_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);


  std::vector<HBGeometry> obs;
  selectors::SelectChainBreaks chain_broken;
  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for(auto ci:*s) ci->remove_ligands();
    // ---------- create a hydrogen bond map
    interactions::BackboneHBondMap hbmap(*s);
    core::index2 max_pos = hbmap.length();

    // ---------- This block detects all H-bonds in sheets (E)
    for (core::index2 i_pos = 0; i_pos < max_pos; ++i_pos) {
      const Residue_SP ri = hbmap.residue(i_pos);
      if ((ri->ss() != 'E') || (chain_broken(ri))) continue;
      for (core::index2 j_pos = 0; j_pos < max_pos; ++j_pos) {
        if(i_pos==j_pos) continue;
        const Residue_SP rj = hbmap.residue(j_pos);
        if ((rj->ss() != 'E') || (chain_broken(rj))) continue;
        if(hbmap.is_antiparallel_bridge(i_pos, j_pos)) {
          try{
            PdbAtom_SP ipca=ri->previous()->find_atom(" CA ");
            PdbAtom_SP inca=ri->next()->find_atom(" CA ");
            PdbAtom_SP ica=ri->find_atom(" CA ");
            PdbAtom_SP jnca=rj->previous()->find_atom(" CA ");
            PdbAtom_SP jpca=rj->next()->find_atom(" CA ");
            PdbAtom_SP jca=rj->find_atom(" CA ");
            obs.push_back(hb_geometry(*ipca, *ica, *inca, *jpca, *jca, *jnca, ANTIPARALLEL));
          } catch (...) {
            std::cerr <<"ERROR 1\n";
          }
        } else if(hbmap.is_parallel_bridge(i_pos, j_pos)) {
          try {
            PdbAtom_SP ipca=ri->previous()->find_atom(" CA ");
            PdbAtom_SP inca=ri->next()->find_atom(" CA ");
            PdbAtom_SP ica=ri->find_atom(" CA ");
            PdbAtom_SP jpca=rj->previous()->find_atom(" CA ");
            PdbAtom_SP jnca=rj->next()->find_atom(" CA ");
            PdbAtom_SP jca=rj->find_atom(" CA ");
            obs.push_back(hb_geometry(*ipca, *ica, *inca, *jpca, *jca, *jnca, PARALLEL));
          } catch (...) {
            std::cerr <<"ERROR 2\n";
          }
        }
      }
    }
    // ---------- This block detects all H-bonds in helices (H)
    for (core::index2 i_pos = 4; i_pos < max_pos; ++i_pos) {
      const Residue_SP ri = hbmap.residue(i_pos);
      if ((ri->ss() != 'H') || (chain_broken(ri))) continue;
      const Residue_SP rj = hbmap.residue(i_pos-3);             // H bond in alha helix is 1-5, but in terms of CA it's 1-4 !!!
      if ((rj->ss() != 'H') || (chain_broken(rj))) continue;

      if (hbmap.are_H_bonded(i_pos, i_pos - 4)) {               // again, we check 1-5 bonding but measure 1-4 CA geometry
        try {
          PdbAtom_SP ipca=ri->previous()->find_atom(" CA ");
          PdbAtom_SP inca=ri->next()->find_atom(" CA ");
          PdbAtom_SP ica=ri->find_atom(" CA ");
          PdbAtom_SP jpca=rj->previous()->find_atom(" CA ");
          PdbAtom_SP jnca=rj->next()->find_atom(" CA ");
          PdbAtom_SP jca=rj->find_atom(" CA ");
          obs.push_back(hb_geometry(*ipca, *ica, *inca, *jpca, *jca, *jnca, HELICAL));
          obs.push_back(hb_geometry(*jpca, *jca, *jnca, *ipca, *ica, *inca,  helical));
        } catch (...) {
          std::cerr <<"ERROR H\n";
        }
      }
    }
  }

  // ---------- print observations
  std::cout << header;
  for(const auto & hb:obs)
    std::cout << hb << "\n";
}