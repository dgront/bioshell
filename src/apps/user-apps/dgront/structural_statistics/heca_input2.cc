#include <core/data/io/Pdb.hh>
#include <core/data/structural/ResidueSegment.hh>
#include <core/data/structural/ResidueSegmentProvider.hh>
#include <core/data/structural/selectors/ResidueSegmentSelector.hh>
#include <core/data/structural/Structure.hh>

#include <core/calc/structural/local_backbone_geometry.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <simulations/forcefields/cartesian/CAHydrogenBond.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>
#include <core/protocols/selection_protocols.hh>

utils::Logger logger("heca_input2");

class CAHBondEnergy : public core::calc::structural::ResidueSegmentGeometry {
public:

  CAHBondEnergy(core::index2 which_res, double en_cutoff)  : ResidueSegmentGeometry("%7.2f") {
    ires_ = which_res;
    en_cutoff_ = en_cutoff;
    name_ = "CAHBondEnergy";
  }

  double operator()(const core::data::structural::ResidueSegment &rs)  const {

    int en_cnt = 0;
    std::string key;
    try {
      key = rs[ires_]->residue_id()+":"+rs[ires_]->owner()->id();
      core::index4 j = residue_id_to_index_.at(key);

      for (int i = 0; i < system_->n_atoms; ++i) {
        double e = energy_->evaluate_raw_energy(*system_, j, i);
          if (e < en_cutoff_) en_cnt += 1;
      }
    } catch (...) {
      logger << utils::LogLevel::CRITICAL << "Can't find residue object for a hash:" << key << "\n";
    }
    return en_cnt;
  }

  virtual ~CAHBondEnergy() = default;

  void set_strctr(const core::data::structural::Structure_SP strcr) {

    residue_id_to_index_.clear();
    sequence = ""; secondary = "";
    core::index4 cnt = 0;
    for (auto chain: *strcr) {      // --- combine sequence from all chains if input fasta_ss data was given
      auto sec_str = chain->create_sequence();
      sequence += sec_str->sequence;
      secondary += sec_str->str();
      for(auto ri:*chain) {
        std::string key = ri->residue_id()+":"+ri->owner()->id();
        residue_id_to_index_.insert(std::pair<std::string,core::index4>(key,cnt));
        ++cnt;
      }
    }
    system_ = std::make_shared<simulations::systems::surpass::SurpassAlfaChains>(*strcr);
    energy_ = std::make_shared<simulations::forcefields::cartesian::CAHydrogenBond>(*system_, secondary);
    logger<<utils::LogLevel::INFO<< "New structure set: " << strcr->code() << "\n";
    logger<<utils::LogLevel::INFO<< cnt<< " amino acid residues hashed\n";
  }

private:
  core::index2 ires_;
  double en_cutoff_;
  std::map<std::string,core::index4> residue_id_to_index_;
  std::string sequence, secondary;
  std::shared_ptr<simulations::systems::surpass::SurpassAlfaChains> system_;
  std::shared_ptr<simulations::forcefields::cartesian::CAHydrogenBond> energy_;
};

class Observations  {
public:
  core::data::structural::ResidueSegment_SP segment;
  std::vector<std::string> observations;

  Observations(core::data::structural::ResidueSegment_SP seg) {
    segment = seg;
  }
};

std::ostream & operator<<(std::ostream & out, Observations & o) {

  const core::data::structural::Structure_SP s = o.segment->structure();
  core::data::structural::ResidueSegment_SP seg = o.segment;
  int center_pos = seg->size() / 2;
  const core::data::structural::Residue & center_res = *(*seg)[center_pos];
  out << utils::string_format("%4s %2s %4d %3s %c %s",s->code().c_str(), center_res.owner()->id().c_str(),
      center_res.id(), center_res.residue_type().code3.c_str(), center_res.ss(), seg->sequence()->str().c_str());
  for (const std::string & oi: o.observations) out << oi;

  return out;
}


const std::string r1n_statistics_info = R"(
Calculates input features for HECA DL model
)";

int count_ca_neighbors(std::vector<core::data::structural::PdbAtom_SP> cas, int pos, double cutoff, int separation=4) {
  int count = 0;
  auto the_ca = *cas[pos];
  int n_ca = cas.size();
  for (int i = 0; i <= std::max(0, pos - separation); ++i) {
    if(cas[i]->distance_to(the_ca)<cutoff) ++count;
  }
  for (int i = std::max(n_ca, pos + separation); i < n_ca; ++i) {
    if(cas[i]->distance_to(the_ca)<cutoff) ++count;
  }

  return count;
}

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::structural;
  using namespace core::calc::structural;

  utils::options::OptionParser & cmd = OptionParser::get("heca_input2");
  cmd.register_option(utils::options::help, show_examples, verbose,db_path);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate all features extracted from the given set (list of pdb_id+chain_id)",
      "heca_input2 -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./pdb/");
  cmd.add_example(id,"calculate only local distances from a single PDB file",
      "heca_input2 -in:pdb=1aba.pdb -r");
  cmd.program_info(r1n_statistics_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);

  selectors::HasProperlyConnectedCA is_connected;
  core::data::structural::selectors::IsCA is_ca;
  selectors::AllResiduesSelected correct_bb(selectors::HasProperlyConnectedCA(),
      std::make_shared<selectors::ResidueHasBB>());

  for (auto s : structures) {
    std::vector<PdbAtom_SP> cas;
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for (auto ci: *s) ci->remove_ligands();   // --- remove ligands from each chain of a structure
    Structure_SP ca_only = s->clone();
    core::protocols::keep_selected_atoms(is_ca, *ca_only);

    auto ca_iter = ca_only->first_atom();
    cas.push_back(*ca_iter);
    ++ca_iter;
    bool is_chain_broken = false;
    for (auto it = ca_iter; it != ca_only->last_atom(); ++it) {
      auto cai = *it;
      if (cai->distance_to(*cas.back()) > 4.2) {
        is_chain_broken = true;
        break;
      }
      cas.push_back(cai);
    }
    if(is_chain_broken) continue;

    int n_ca = cas.size();
    // --- R13
    std::vector<double> r13(n_ca, 0.0);
    for (int i = 1; i < cas.size() - 1; ++i)
      r13[i] = cas[i - 1]->distance_to(*cas[i + 1]);
    // --- R14
    std::vector<double> r14(n_ca, 0.0);
    for (int i = 1; i < cas.size() - 2; ++i)
      r14[i] = cas[i - 1]->distance_to(*cas[i + 2]);
    // --- R15
    std::vector<double> r15(n_ca, 0.0);
    for (int i = 2; i < cas.size() - 2; ++i)
      r15[i] = cas[i - 2]->distance_to(*cas[i + 2]);
    // --- ca_nb_count for 4.0A
    std::vector<int> nb_cnts_40(n_ca, 0.0);
    for (int i = 0; i < cas.size(); ++i)
      nb_cnts_40[i] = count_ca_neighbors(cas, i, 4.0);
    // --- ca_nb_count for 4.5A
    std::vector<int> nb_cnts_45(n_ca, 0.0);
    for (int i = 0; i < cas.size(); ++i)
      nb_cnts_45[i] = count_ca_neighbors(cas, i, 4.5);
    // --- ca_nb_count for 5.0A
    std::vector<int> nb_cnts_50(n_ca, 0.0);
    for (int i = 0; i < cas.size(); ++i)
      nb_cnts_50[i] = count_ca_neighbors(cas, i, 5.0);
    // --- ca_nb_count for 6.0A
    std::vector<int> nb_cnts_60(n_ca, 0.0);
    for (int i = 0; i < cas.size(); ++i)
      nb_cnts_60[i] = count_ca_neighbors(cas, i, 6.0);
    // --- ca_nb_count for 8.0A
    std::vector<int> nb_cnts_80(n_ca, 0.0);
    for (int i = 0; i < cas.size(); ++i)
      nb_cnts_80[i] = count_ca_neighbors(cas, i, 8.0);

    // --- H-bond energy
    auto seq_sec = (*ca_only)[0]->create_sequence();
    simulations::systems::surpass::SurpassAlfaChains system(*ca_only);
    simulations::forcefields::cartesian::CAHydrogenBond energy(system, seq_sec->str());

    std::vector<int> hb_counts(n_ca, 0.0);

    std::ofstream out(s->code()+".dat");
    logger << utils::LogLevel::INFO << "Writing " << (s->code()+".dat") << "\n";
    auto chain = (*(*ca_only)[0]);
    for(int i=0;i<n_ca;++i) {
      out << utils::string_format("%4d %3s %c  %5.3f %7.3f %6.3f %1d %1d %1d %1d %2d  %1d\n",
          i, chain[i]->residue_type().code3.c_str(), chain[i]->ss(), r13[i], r14[i], r15[i],
          nb_cnts_40[i], nb_cnts_45[i], nb_cnts_50[i], nb_cnts_60[i], nb_cnts_80[i], hb_counts[i]);
    }
    out.close();
  }

}