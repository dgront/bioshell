#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/angles.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/select_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <core/protocols/selection_protocols.hh>
#include <core/chemical/ChiAnglesDefinition.hh>

utils::Logger logger("sc_contact_distances");

using namespace core::data::structural;
using namespace core::data::structural::selectors;

using core::calc::structural::transformations::local_coordinates_three_atoms;
using namespace core::calc::structural;

//ResidueHasBBCB has_full_bb_cb;
IsBBCB is_b_cb;
InverseAtomSelector select_sc(is_b_cb);
ResidueHasAllHeavyAtoms is_complete;
SelectChainBreaks at_chainbreak;

bool check_residue(const Residue& ri) {
  if (!is_complete(ri)) return false;
  if (at_chainbreak(ri)) return false;
  if (ri.residue_type()==core::chemical::Monomer::GLY) return false;
  if (ri.residue_type()==core::chemical::Monomer::ALA) return false;
  return true;
}


void print_distances(core::data::structural::Structure_SP s, double aa_cutoff) {

  std::vector<PdbAtom_SP> sc_positions;
  std::vector<Residue_SP> residues;
  std::vector<std::string> chain_ids;

  for (auto c: *s) {
    int atom_cnt = 0;
    for (auto r: *c) {
      ++atom_cnt;
      if (check_residue(*r)) {
        Residue_SP sc = (*r).clone(select_sc);
        Vec3 cen = sc->cm();
        PdbAtom_SP cen_atom = std::make_shared<PdbAtom>(atom_cnt, " SC ", cen.x, cen.y, cen.z);
        sc_positions.push_back(cen_atom);
        residues.push_back(sc);
        chain_ids.push_back(r->owner()->id());
      }
    }
  }

  for(core::index4 i=0;i<sc_positions.size();++i) {
    for (core::index4 j = 0; j < i; ++j) {
      int d_ij = abs(int(sc_positions[j]->id()) - int(sc_positions[i]->id()));
      if (chain_ids[i] != chain_ids[j]) d_ij = -1;
      double d_aa = residues[i]->min_distance(residues[j]);
      double d_cg = sc_positions[i]->distance_to(*sc_positions[j]);
      if (d_aa < aa_cutoff) {
        std::cout << utils::string_format("%c %c %4d %7.3f %7.3f\n", residues[i]->residue_type().code1, residues[j]->residue_type().code1, d_ij, d_cg, d_aa);
      }
    }
  }

}

const std::string sc_contact_distances_info = R"(
Prints coordinates of CEN atom in local coordinates system.

These local coordinates are used to define coarse-grained rotamers for each amino acid type
)";

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::structural;
  using namespace core::calc::structural;

  utils::options::OptionParser & cmd = OptionParser::get("sc_contact_distances");
  cmd.register_option(utils::options::help, show_examples, verbose, db_path);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures
  cmd.register_option(keep_alternate, include_hydrogens);
  cmd.register_option(select_chains);

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"print contact distances for each protein found in the given chains' set (list of pdb_id+chain_id)",
      "sc_contact_distances -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./all/pdb/path/");
  cmd.program_info(sc_contact_distances_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);
  std::cout << "#aaij dij    dcg    d_aa\n";
  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for (auto ci: *s) ci->remove_ligands();   // --- remove ligands from each chain of a structure
//    std::ofstream outf(s->code() + s->get_chain(0)->id() + ".dat");
//    outf.close();
    print_distances(s, 10.0);
  }
}