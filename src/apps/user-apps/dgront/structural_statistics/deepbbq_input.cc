#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/protein_angles.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>

#include <simulations/forcefields/cartesian/CAHydrogenBond.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>
#include <core/protocols/selection_protocols.hh>

utils::Logger logger("deepbbq_input");

using namespace core::data::structural;

void copy_ca(const Structure &source, std::vector<PdbAtom_SP> & dest) {
  ::selectors::IsCA is_ca;
  for (auto a_it = source.first_const_atom(); a_it != source.last_const_atom(); ++a_it)
    if (is_ca(**a_it)) dest.push_back(*a_it);
}

int count_hbonds(core::index2 which_pos, simulations::systems::surpass::SurpassAlfaChains &system,
    simulations::forcefields::cartesian::CAHydrogenBond & hb_energy) {

  int n_hb = 0;
  for (int i = 1; i < system.n_atoms - 1; ++i) {
    double e = hb_energy.evaluate_raw_energy(system, which_pos, i);
    if (e < hb_energy.hbond_energy_cutoff) n_hb += 1;
  }

  return n_hb;
}

void count_neighbors(std::vector<PdbAtom_SP> ca, int which_pos, int & n4, int & n45, int & n5, int & n6) {

  PdbAtom &a = *ca[which_pos];
  for (int i = 0; i < ca.size(); ++i) {
    if(abs(which_pos-i)<3) continue;
    double d = a.distance_to(*ca[i]);
    if (d > 6) continue;
    ++n6;
    if (d < 5) {
      ++n5;
      if (d < 4.5) {
        ++n45;
        if (d < 4) ++n4;
      }
    }
  }
}

bool compute_bbq_features(const Structure &source, std::ostream & out) {

  bool result = true;

  ::selectors::IsCA is_ca;
  Structure_SP ca_only = source.clone();
  core::protocols::keep_selected_atoms(is_ca, *ca_only);

  std::vector<PdbAtom_SP> ca;
  for (auto a_it = source.first_const_atom(); a_it != source.last_const_atom(); ++a_it)
    if (is_ca(**a_it)) ca.push_back(*a_it);
  if(source.count_residues()!=ca_only->count_residues())
      return false;
  std::string secondary = source[0]->create_sequence()->str();  // --- secondary structure
  std::string code = source.code() + source[0]->id();
  simulations::systems::surpass::SurpassAlfaChains system(*ca_only);
  simulations::forcefields::cartesian::CAHydrogenBond hb_energy_E(system, secondary);
  simulations::forcefields::cartesian::CAHydrogenBond hb_energy_H(system, secondary, "H");
  core::index2 len = ca.size();

  out << "#PDB   pos omega  lambda  aa ss cis  r13b  r13f       r14b     r14f    r15b  r15f   hbonds  neighbors\n";
  for (core::index2 which_pos = 1; which_pos < len-1; ++which_pos) {

    try {
      double omega = core::calc::structural::evaluate_omega(*ca[which_pos]->owner(), *ca[which_pos + 1]->owner());
      double lambda = core::calc::structural::evaluate_lambda(*ca[which_pos - 1]->owner(), *ca[which_pos]->owner(),
          *ca[which_pos + 1]->owner());
      double r13_f = (which_pos >= len - 2) ? 0 : ca[which_pos]->distance_to(*ca[which_pos + 2]);
      double r13_b = (which_pos < 2) ? 0 : ca[which_pos]->distance_to(*ca[which_pos - 2]);
      double r14_f = (which_pos >= len - 3) ? 0 :
          core::data::basic::r14x(*ca[which_pos], *ca[which_pos + 1], *ca[which_pos + 2], *ca[which_pos + 3]);
      double r14_b = (which_pos < 3) ? 0 :
          core::data::basic::r14x(*ca[which_pos], *ca[which_pos - 1], *ca[which_pos - 2], *ca[which_pos - 3]);
      double r15_f = (which_pos >= len - 4) ? 0 : ca[which_pos]->distance_to(*ca[which_pos + 4]);
      double r15_b = (which_pos < 4) ? 0 : ca[which_pos]->distance_to(*ca[which_pos - 4]);

      int n_hb_H = count_hbonds(which_pos, system, hb_energy_H);
      int n_hb_E = count_hbonds(which_pos, system, hb_energy_E);

      int n4 = 0, n45 = 0, n5 = 0, n6 = 0;
      count_neighbors(ca, which_pos, n4, n45, n5, n6);
      core::data::structural::Residue &r = *(ca[which_pos]->owner());

      out << utils::string_format(
          "%s %4d  %6.3f  %6.3f  %c %c  %d  %5.3f %5.3f    %7.3f %7.3f   %6.3f %6.3f    %1d %1d   %1d %1d %1d %1d\n",
          code.c_str(), r.id(), omega, lambda, r.residue_type().code1, r.ss(), (fabs(omega)<1.59),
          r13_b, r13_f, r14_b, r14_f, r15_b, r15_f, n_hb_E, n_hb_H, n4, n45, n5, n6);
    } catch (utils::exceptions::AtomNotFound & e) {
      logger << utils::LogLevel::WARNING << code << ": can't find atom " << e.missing_atom_name << " at pos "
             << ca[which_pos]->owner()->id() << "\n";
      result = false;
    }
  }
  return result;
}

const std::string deepbbq_input_info = R"(
Calculates input features for deep-BBQ DL model
)";

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::structural;
  using namespace core::calc::structural;


  utils::options::OptionParser & cmd = OptionParser::get("deepbbq_input");
  cmd.register_option(utils::options::help, show_examples, verbose,db_path);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate all features for all proteins in the given set (list of pdb_id+chain_id)",
      "deepbbq_input -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./pdb/ ");
  cmd.add_example(id,"calculate deep-BBQ features from a single PDB file",
      "deepbbq_input -in:pdb=1aba.pdb");
  cmd.program_info(deepbbq_input_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);
  ::selectors::IsCA is_ca;
  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for (auto ci: *s) ci->remove_ligands();   // --- remove ligands from each chain of a structure
    std::ofstream outf(s->code() + s->get_chain(0)->id() + ".dat");
    compute_bbq_features(*s, outf);
    outf.close();
  }
}