
#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/angles.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/interactions/ContactMap.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>

#include <simulations/forcefields/cartesian/CAHydrogenBond.hh>
#include <core/protocols/selection_protocols.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>

utils::Logger logger("plaque7_input");
const std::string program_info = R"(
Reads a PDB file and produces 7x7 plaques extracted from the protein's contact map
)";

using namespace core::data::structural;
using namespace core::data::structural::selectors;
using namespace core::calc::structural::interactions;

struct ContactData {
  unsigned int i;
  unsigned int j;
  double d;
  double alfa;
};

std::ostream& operator<<(std::ostream& out, ContactData& c) {
  out << c.i << " "<<c.j << " "<<c.d << " "<<c.alfa;
  return out;
}

class ProteinContacts;
typedef std::shared_ptr<ProteinContacts> ProteinContacts_SP;

class ProteinContacts {
public:
  std::string sequence;
  std::string secondary;
  std::vector<double> phi;
  std::vector<double> psi;
  std::vector<double> omega;
  std::vector<ContactData> contacts;

  static ProteinContacts_SP from_structure(const Structure &s) {

    if (!is_protein_ok(s)) {
      logger << utils::LogLevel::WARNING << "Structure " << s.code() << " is incomplete!\n";
      return nullptr;
    }

    ProteinContacts_SP out = std::make_shared<ProteinContacts>();
    for (Chain_SP c_sp: s) {
      auto ss = c_sp->create_sequence(true);
      out->sequence += ss->sequence;
      out->secondary += ss->str();
    }
    out->compute_dihedrals(s);

    ContactMap cmap(s, 5.0);
    const std::vector<std::pair<core::index2, core::index2>> &contacts = cmap.contacts_list();

    IsBBCB is_b_cb;
    InverseAtomSelector select_sc(is_b_cb);

    for(auto cti: contacts) {
      if (abs(cti.first - cti.second) < 5) continue;
      Residue_SP ri = cmap.residue(cti.first);
      Residue_SP rj = cmap.residue(cti.second);
      if((ri->residue_type()==core::chemical::Monomer::GLY)||(rj->residue_type()==core::chemical::Monomer::GLY)) continue;
      Residue_SP sc_i = ri->clone(select_sc);
      Residue_SP sc_j = rj->clone(select_sc);
      Vec3 cen_i = sc_i->cm();
      Vec3 cen_j = sc_j->cm();
      double d = cen_i.distance_to(cen_j);
      cen_i -= *ri->find_atom(" CA ");
      cen_j -= *rj->find_atom(" CA ");
      double alfa =  core::calc::structural::evaluate_planar_angle(cen_i,cen_j);
      ContactData cd{cti.first, cti.second, d, alfa};
      std::cout << cd << "\n";
      out->contacts.push_back(cd);
    }

    return out;
  }

private:

  static bool is_protein_ok(const Structure &s) {

    ResidueHasAllHeavyAtoms is_complete;
    SelectChainBreaks at_chainbreak;

    for (Chain_SP c_sp: s) {
      const Chain &c = *c_sp;
      if (!is_complete(c[0])) return false;
      for (int i = 1; i < c.size() - 1; ++i) {
        if (at_chainbreak(c[i])) return false;
      }
      if (!is_complete(c[c.size() - 1])) return false;
    }

    return true;
  }

  void compute_dihedrals(const Structure &s) {

    for(Chain_SP c_sp:s) {
      const Chain &c = *c_sp;
      phi.push_back(0.0);
      for (int ir = 1; ir < c.size()-1; ++ir) {
        phi.push_back(core::calc::structural::evaluate_phi(*c[ir - 1], *c[ir]));
        psi.push_back(core::calc::structural::evaluate_psi(*c[ir], *c[ir + 1]));
        psi.push_back(core::calc::structural::evaluate_omega(*c[ir], *c[ir + 1]));
      }
      psi.push_back(0.0);
      omega.push_back(0.0);
    }
  }

};


int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::structural;
  using namespace core::calc::structural;


  utils::options::OptionParser & cmd = OptionParser::get("plaque7_input");
  cmd.register_option(utils::options::help, show_examples, verbose,db_path);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate all features for fragments of 5 amino acids extracted from the given set (list of pdb_id+chain_id)",
      "heca_input -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./pdb/ -n=5");
  cmd.add_example(id,"calculate only local distances from a single PDB file, for fragments of 11 residues",
      "heca_input -in:pdb=1aba.pdb -r -n=11");
  cmd.program_info(program_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);
  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for (auto ci: *s) ci->remove_ligands();   // --- remove ligands from each chain of a structure
    std::ofstream outf(s->code() + s->get_chain(0)->id() + ".dat");
    ProteinContacts_SP cd = ProteinContacts::from_structure(*s);
    outf.close();
  }
}