#include <core/data/io/Pdb.hh>
#include <core/data/structural/ResidueSegment.hh>
#include <core/data/structural/ResidueSegmentProvider.hh>
#include <core/data/structural/selectors/ResidueSegmentSelector.hh>
#include <core/data/structural/Structure.hh>

#include <core/calc/structural/local_backbone_geometry.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <simulations/forcefields/cartesian/CAHydrogenBond.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>
#include <core/protocols/selection_protocols.hh>

utils::Logger logger("heca_input");

class CAHBondEnergy : public core::calc::structural::ResidueSegmentGeometry {
public:

  CAHBondEnergy(core::index2 which_res, double en_cutoff)  : ResidueSegmentGeometry("%7.2f") {
    ires_ = which_res;
    en_cutoff_ = en_cutoff;
    name_ = "CAHBondEnergy";
  }

  double operator()(const core::data::structural::ResidueSegment &rs)  const {

    int en_cnt = 0;
    std::string key;
    try {
      key = rs[ires_]->residue_id()+":"+rs[ires_]->owner()->id();
      core::index4 j = residue_id_to_index_.at(key);

      for (int i = 0; i < system_->n_atoms; ++i) {
        double e = energy_->evaluate_raw_energy(*system_, j, i);
          if (e < en_cutoff_) en_cnt += 1;
      }
    } catch (...) {
      logger << utils::LogLevel::CRITICAL << "Can't find residue object for a hash:" << key << "\n";
    }
    return en_cnt;
  }

  virtual ~CAHBondEnergy() = default;

  void set_strctr(const core::data::structural::Structure_SP strcr) {

    residue_id_to_index_.clear();
    sequence = ""; secondary = "";
    core::index4 cnt = 0;
    for (auto chain: *strcr) {      // --- combine sequence from all chains if input fasta_ss data was given
      auto sec_str = chain->create_sequence();
      sequence += sec_str->sequence;
      secondary += sec_str->str();
      for(auto ri:*chain) {
        std::string key = ri->residue_id()+":"+ri->owner()->id();
        residue_id_to_index_.insert(std::pair<std::string,core::index4>(key,cnt));
        ++cnt;
      }
    }
    system_ = std::make_shared<simulations::systems::surpass::SurpassAlfaChains>(*strcr);
    energy_ = std::make_shared<simulations::forcefields::cartesian::CAHydrogenBond>(*system_, secondary);
    logger<<utils::LogLevel::INFO<< "New structure set: " << strcr->code() << "\n";
    logger<<utils::LogLevel::INFO<< cnt<< " amino acid residues hashed\n";
  }

private:
  core::index2 ires_;
  double en_cutoff_;
  std::map<std::string,core::index4> residue_id_to_index_;
  std::string sequence, secondary;
  std::shared_ptr<simulations::systems::surpass::SurpassAlfaChains> system_;
  std::shared_ptr<simulations::forcefields::cartesian::CAHydrogenBond> energy_;
};

class Observations  {
public:
  core::data::structural::ResidueSegment_SP segment;
  std::vector<std::string> observations;

  Observations(core::data::structural::ResidueSegment_SP seg) {
    segment = seg;
  }
};

std::ostream & operator<<(std::ostream & out, Observations & o) {

  const core::data::structural::Structure_SP s = o.segment->structure();
  core::data::structural::ResidueSegment_SP seg = o.segment;
  int center_pos = seg->size() / 2;
  const core::data::structural::Residue & center_res = *(*seg)[center_pos];
  out << utils::string_format("%4s %2s %4d %3s %c %s",s->code().c_str(), center_res.owner()->id().c_str(),
      center_res.id(), center_res.residue_type().code3.c_str(), center_res.ss(), seg->sequence()->str().c_str());
  for (const std::string & oi: o.observations) out << oi;

  return out;
}


const std::string r1n_statistics_info = R"(
Calculates input features for HECA DL model
)";

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::structural;
  using namespace core::calc::structural;

  utils::options::Option frag_len_opt("-n", "-frag_length", "the length of each considered fragment");
  Option local_opt("-r", "-r1n", "list local R1N distances along a chain fragment");
  Option neighb_opt("-s", "-neighbors", "count spatial neighbors for each CA in a fragment");
  Option hbonds_opt("-b", "-hbonds", "print hydrogen bond energy in the 3CA definition");
  Option lambda_opt("-l", "-lambda", "print lambda angle for the middle peptide plate (input for BBQ training)");
  Option all_opt("-a", "-all", "turns on -r, -b and -s; this is the default if neither of the three is given");

  utils::options::OptionParser & cmd = OptionParser::get("heca_input");
  cmd.register_option(utils::options::help, show_examples, verbose,db_path, frag_len_opt);
  cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures
  cmd.register_option(local_opt, neighb_opt, hbonds_opt, all_opt, lambda_opt);

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate all features for fragments of 5 amino acids extracted from the given set (list of pdb_id+chain_id)",
      "heca_input -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./pdb/ -n=5");
  cmd.add_example(id,"calculate only local distances from a single PDB file, for fragments of 11 residues",
      "heca_input -in:pdb=1aba.pdb -r -n=11");
  cmd.program_info(r1n_statistics_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  cmd.inject_option(input_pdb_header_ss, "true");
  cmd.inject_option(input_pdb_header, "true");
  if((!cmd.was_used(local_opt))&&(!cmd.was_used(neighb_opt))&&(!cmd.was_used(hbonds_opt)))
    cmd.inject_option(all_opt, "true");

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);

  core::index2 frag_len = option_value<core::index2>(frag_len_opt, 5);

  std::vector<ResidueSegmentGeometry_SP> properties;

  if(cmd.was_used(lambda_opt)) {
    properties.push_back(std::make_shared<LambdaDihedral>(frag_len/2));
    properties.back()->format(" %8.3f   ");
  }
  if (cmd.was_used(local_opt) || cmd.was_used(all_opt)) {
    for (int i = 0; i < frag_len - 2; ++i) {
      properties.push_back(std::make_shared<R13>(i)); properties.back()->format(" %5.3f");
    }
    for (int i = 0; i < frag_len - 3; ++i) {
      properties.push_back(std::make_shared<R14x>(i)); properties.back()->format(" %7.3f");
    }
    for (int i = 0; i < frag_len - 4; ++i) {
      properties.push_back(std::make_shared<R15>(i)); properties.back()->format(" %6.3f");
    }
  }
  if (cmd.was_used(neighb_opt) || cmd.was_used(all_opt)) {
    for (int i = 0; i < frag_len; ++i) {
      properties.push_back(std::make_shared<CaNeighborsCount>(i, 4.0, 4));
      if (i==0) properties.back()->format("     %1.0f");
      else properties.back()->format(" %1.0f");
      properties.push_back(std::make_shared<CaNeighborsCount>(i, 4.5, 4)); properties.back()->format(" %1.0f");
      properties.push_back(std::make_shared<CaNeighborsCount>(i, 5.0, 4)); properties.back()->format(" %1.0f");
      properties.push_back(std::make_shared<CaNeighborsCount>(i, 6.0, 4)); properties.back()->format(" %1.0f");
    }
  }
  std::vector<std::shared_ptr<CAHBondEnergy>> hbonds;
  if (cmd.was_used(hbonds_opt) || cmd.was_used(all_opt)) {
    for (int i = 0; i < frag_len; ++i) {
      auto hb_prop = std::make_shared<CAHBondEnergy>(i, -0.1);
      properties.push_back(hb_prop);
      hbonds.push_back(hb_prop);
      if(i==0)
        hb_prop->format("     %1.0f");
      else
        hb_prop->format(" %1.0f");
    }
  }
  selectors::HasProperlyConnectedCA is_connected;
  core::data::structural::selectors::IsCA is_ca;
  selectors::AllResiduesSelected correct_bb(selectors::HasProperlyConnectedCA(),
      std::make_shared<selectors::ResidueHasBB>());

  for (auto s : structures) {
    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for (auto ci: *s) ci->remove_ligands();   // --- remove ligands from each chain of a structure
    Structure_SP ca_only = s->clone();
    core::protocols::keep_selected_atoms(is_ca, *ca_only);
    ResidueSegmentProvider rsp(s, frag_len);
    for(auto hbonds_prop:hbonds) hbonds_prop->set_strctr(ca_only);
    while(rsp.has_next()) {
      const ResidueSegment_SP  seg = rsp.next();
      if (is_connected(*seg)) {
        Observations o(seg);
        for(const auto &p:properties) {
          if(correct_bb(*seg))
            o.observations.push_back(utils::string_format(p->format(), (*p)(*seg)));
        }
        if(o.observations.size()>0)
          std::cout << o << "\n";
      }
    }
  }

}