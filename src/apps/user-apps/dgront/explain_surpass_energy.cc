#include <ostream>

#include <core/data/io/Pdb.hh>
#include <core/data/io/ss2_io.hh>

#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/protocols/selection_protocols.hh>
#include <core/data/structural/Structure.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/select_options.hh>
#include <utils/options/structures_from_cmdline.hh>

#include <core/calc/structural/interactions/PairwiseResidueMap.hh>
#include <core/calc/structural/interactions/BackboneHBondMap.hh>

#include <simulations/forcefields/cartesian/CAHydrogenBond.hh>
#include <simulations/forcefields/surpass/SurpassContactEnergy.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>

#include <utils/io_utils.hh>
#include <utils/Logger.hh>
#include <core/data/io/fasta_io.hh>
#include <core/calc/structural/interactions/ContactMap.hh>


utils::Logger l("explain_surpass_energy");

const std::string program_info = R"(
      Calculates the H-bond and contact energy terms in the SURPASS-Alfa model for a given PDB file.)";

using namespace core::calc::structural::interactions;

void print_maps(core::index2 size, std::shared_ptr<PairwiseResidueMap<double>> & map1,
    std::shared_ptr<PairwiseResidueMap<double>> & map2) {

  std::map<std::string,std::pair<double,double>> print_map;
  for (core::index2 i = 0; i < size; ++i) {
    for (core::index2 j = 0; j < size; ++j) {
      std::string key = utils::string_format("%4d %4d", i, j);

      if (map1!= nullptr && map1->has_element(i, j)) print_map[key] = std::make_pair(map1->at(i, j, 0), 0);
      if (map2!= nullptr && map2->has_element(i, j)) {
        if(print_map.find(key)==print_map.cend())
          print_map[key] = std::make_pair(0, map2->at(i, j, 0));
        else
          print_map[key] = std::make_pair(print_map[key].first, map2->at(i, j, 0));
      }
    }
  }
  for(const auto & p: print_map) {
    std::cout << p.first<<utils::string_format(" %7.4f %7.4f\n",p.second.first, p.second.second);
  }
}


double print_maps(core::index2 size, std::shared_ptr<PairwiseResidueMap<double>> map1,
    BackboneHBondMap & map2, const std::string & secondary, bool if_print) {

  // --- a map of all-atom H-bonds indexed by interacting residues
  std::vector<std::pair<core::index2,core::index2>> aa_hb_list;
  // --- a map of H-bonds in the 3CA definition
  std::vector<std::pair<core::index2,core::index2>> cg_hb_list;
  // --- a map of all-atom H-bonds but shifted to sideways CA
  std::vector<std::pair<core::index2,core::index2>> aa_ca_hb_list;

//  BackboneHBondInteraction_SP hb = nullptr, hb2 = nullptr;
  // --- firstly we correct all-atom H-bond map from DSSP to the CA-only map
  for (core::index2 i = 0; i < size; ++i) {
    if (secondary[i] == 'C') continue;
    for (core::index2 j = 0; j < size; ++j) {
      if ( secondary[j] == 'C') continue;
      if (i == j) continue;
      // ---------- record CA-pairs corresponding to an all-atom H-bond -> convert beta-bridges to CG H-bonds
      if (map2.is_antiparallel_bridge(i, j)) {
        aa_ca_hb_list.push_back({i, j});
      }
      if (map2.is_parallel_bridge(i, j)) {
        aa_ca_hb_list.push_back({i, j});
      }
    }
    // ---------- record CA-pairs corresponding to an all-atom H-bond -> convert alfa turns to CG H-bonds
    if (map2.accepts_N_turn(i, 4)) {
      if (secondary[i + 3] == 'C') continue;
      aa_ca_hb_list.push_back({i, i + 3});
      aa_ca_hb_list.push_back({i+3, i});
    }
  }
  if (if_print) {
    std::cout << "#  i    j  en_3ca   if_aa if_cg\n";
  }
  int sum_aa = 0, sum_cg = 0;
  for (core::index2 i = 0; i < size; ++i) {
    for (core::index2 j = 0; j < size; ++j) {
      auto key = std::make_pair(i,j);
      bool aa_found = false, cg_found = false;
      if (std::find(aa_ca_hb_list.cbegin(), aa_ca_hb_list.cend(), key) != aa_ca_hb_list.cend()) aa_found = true;
      if (map1->has_element(i,j)) {
        cg_found = true;
        cg_hb_list.push_back(key);
      }
      if ((aa_found || cg_found) && if_print) {
        std::cout << utils::string_format("%4d %4d  %c%c %6.3f     %d    %d\n", i, j, secondary[i], secondary[j],
            map1->at(i, j, 0), aa_found, cg_found);
      }
      sum_aa += aa_found;
      sum_cg += cg_found;
    }
  }

  return jaccard_overlap_coefficient(aa_ca_hb_list, cg_hb_list);
}


/** @brief Returns true if the structure is OK for calculations.
 *
 * Structure is not OK, if any residue lacks a CA atom
 * @return true if H-bonds in the 3CA definition can be evaluated for the whole structure
 */
bool is_structure_ok(core::data::structural::Structure_SP strctr) {

  for (const auto chain_sp: *strctr) {
    for (const auto res_sp: *chain_sp) {
      if (res_sp->find_atom(" CA ") == nullptr) return false;
    }
  }
  return true;
}

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace core::data::structural;
  using namespace core::calc::structural::interactions;
  using namespace utils::options;
  using namespace simulations::forcefields::cartesian;

  Option explain_hbonds("-hb", "-hbonds", "explain hydrogen bonding energy for each input structure");
  Option explain_contacts("-cnt", "-contacts", "explain surpass contacts for each input structure");
  Option energy_cutoff("-c", "-cutoff", "cut-off for energy values");
  Option hbond_overlap("-overlap", "-overlap", "list side-by-side all the all-atom and SURPASS H-bonds");
  Option rescore("-rescore", "-rescore", "just rescore the input structures, without printing details");
  Option details("-details", "-details", "print detailed list of all interactions");

  utils::options::OptionParser & cmd = OptionParser::get("explain_surpass_energy");
  cmd.register_option(utils::options::help, show_examples, verbose,db_path, input_fasta_ss, select_chains);
  cmd.register_option(input_pdb,input_ss2, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header); // input PDB structures
  cmd.register_option(explain_hbonds, explain_contacts, hbond_overlap, energy_cutoff, details, rescore);
  cmd.register_option(select_models,all_models);

  core::index2 id = cmd.add_examples_group("examples:");
  cmd.add_example(id,"calculate energy for a given PDB file (native):", "explain_surpass_energy -in:pdb=2gb1.pdb");
  cmd.add_example(id,"calculate energy for a given PDB model:", "explain_surpass_energy  -ip=1693.pdb -in:fasta:secondary=2gb1A.fasta_ss");
  cmd.add_example(id,"compare 3CA and all-atom H-bond maps for a native PDB file (native):",
      "explain_surpass_energy -in:pdb=2gb1.pdb -overlap");
  cmd.add_example(id,"calculate energy for a selected chain of a PDB file :",
      "explain_surpass_energy -in:pdb=2gb1.pdb -select:chains=A");
  cmd.add_example(id,"exmplain in details H-bond energy for a given PDB file:",
      "-in:pdb=2gb1.pdb -in:pdb:path=./ -rescore -details -hbonds");
  cmd.program_info(program_info);


  // ---------- parse the command line
  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // ---------- if neither explain_contacts nor explain_hbonds was used - explain both of these terms
  if ((!cmd.was_used(explain_contacts)) && (!cmd.was_used(explain_hbonds))) {
    cmd.inject_option(explain_contacts, "true");
    cmd.inject_option(explain_hbonds, "true");
  }
  // ---------- if no action requested - just rescore
  if ((!cmd.was_used(details)) && (!cmd.was_used(rescore))&& (!cmd.was_used(hbond_overlap))) {
    cmd.inject_option(rescore, "true");
  }
  cmd.inject_option(input_pdb_header, "true");    // --- enforce reading in PDB file header - it may hold secondary structure

  // ---------- The lines below check all the relevant cmdline flags and read all requested PDB files
  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);

  core::data::structural::selectors::IsCA is_ca;

  // ---------- Process input sequence with secondary structure, annotate structures
  std::string sequence, secondary;
  if (cmd.was_used(input_fasta_ss)) { // ---------- Read SS fasta format
    const std::string input_ss_file = option_value<std::string>(input_fasta_ss);
    auto sec_str = core::data::io::read_fasta_ss_file(input_ss_file);
    secondary = sec_str->str();
    sequence = (sec_str->sequence.size() == 0) ? std::string('A', sec_str->str().size()) : sec_str->sequence;
  } else if(cmd.was_used(input_ss2)) { // ---------- Read SS2 format
      std::string input_ss2_file = option_value<std::string>(input_ss2);
      auto sec_str = core::data::io::read_ss2(input_ss2_file);
      secondary = sec_str->str();
      sequence = (sec_str->sequence.size() == 0) ? std::string('A', sec_str->str().size()) : sec_str->sequence;
  }

  // ---------- Prepare the header for the energy table
  if (cmd.was_used(rescore)) {
    std::string en_header("#");
    if (cmd.was_used(explain_contacts)) en_header += " SurpassContactEnergy";
    if (cmd.was_used(explain_hbonds)) en_header += " CAHydrogenBond";
    std::cout << en_header << "\n";
  }
int cnt_str = 0;
  // ---------- Process input structures
  for (auto s : structures) {

    l << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for (auto ci: *s) ci->remove_ligands();

    if(!is_structure_ok(s)) {
      l << utils::LogLevel::WARNING << "Structure incomplete: " << s->code() << "\n";
      continue;
    }
    if (!cmd.was_used(input_fasta_ss) && !cmd.was_used(input_ss2)) {
      sequence = ""; secondary = "";
      for (auto chain: *s) {      // --- combine sequence from all chains if input fasta_ss data was given
        auto sec_str = chain->create_sequence();
        sequence += sec_str->sequence;
        secondary += sec_str->str();
      }
    }
    else (*s)[0]->annotate_ss(secondary);
      Structure_SP all_atom = s->clone();                           // --- clone the structure for reference
    core::protocols::keep_selected_atoms(is_ca, *s);
    simulations::systems::surpass::SurpassAlfaChains chains(*s);  // --- SURPASS system to be scored
    simulations::forcefields::surpass::SurpassContactEnergy cnt_energy(chains, 100, -0.1);
    simulations::forcefields::cartesian::CAHydrogenBond hb_energy(chains, secondary, "EH");

    hb_energy.hbond_energy_cutoff = option_value(energy_cutoff, -0.1);

    std::shared_ptr<PairwiseResidueMap<double>> hb3ca_map = nullptr;
    std::shared_ptr<PairwiseResidueMap<double>> sur_cnt_map = nullptr;

    if(cmd.was_used(explain_hbonds))
      hb3ca_map = std::make_shared<PairwiseResidueMap<double>>(*s);
    else
      hb3ca_map = nullptr;
    if(cmd.was_used(explain_contacts))
      sur_cnt_map = std::make_shared<PairwiseResidueMap<double>>(*s);
    else
      sur_cnt_map = nullptr;

    for (core::index4 i = 0; i < chains.n_atoms; ++i) {
      for (core::index4 j = 0; j < chains.n_atoms; ++j) {
        if(hb3ca_map != nullptr) {
          double e_hb = hb_energy.evaluate_raw_energy(chains, i, j, false);
          if (e_hb < hb_energy.hbond_energy_cutoff) {
            hb3ca_map->add(i, j, e_hb);
          }
        }
        if(sur_cnt_map != nullptr) {
          double e_cn = cnt_energy.evaluate_energy(chains, i, j);
          if (e_cn < 0.0) sur_cnt_map->add(i, j, e_cn);
        }
      }
    }

    if(cmd.was_used(details)) {           // ---------- print surpass interactions: H-bond and contacts for a current structure
      print_maps(chains.n_atoms, hb3ca_map, sur_cnt_map);
    }
    if (cmd.was_used(rescore)) {          // ---------- just rescore models
        std::cout<<structure_ids[cnt_str];
        cnt_str+=1;
      if (cmd.was_used(explain_contacts)) std::cout << " " << cnt_energy.energy(chains);
      if (cmd.was_used(explain_hbonds)) std::cout << " " << hb_energy.energy(chains);
      std::cout << "\n";
    }
    if (cmd.was_used(hbond_overlap)) {    // ---------- compare surpass and all-atom H-bonds
      BackboneHBondMap hb_allatom(*all_atom);
      double overlap = print_maps(chains.n_atoms, hb3ca_map, hb_allatom, secondary, true);
      std::cout << "# "<< s->code()<<" overlap on CA: " << overlap << " with " << hb3ca_map->size() << " CG bonds\n";
    }
  }
}
