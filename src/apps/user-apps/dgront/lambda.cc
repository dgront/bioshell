#include <iostream>
#include <iomanip>

#include <core/calc/structural/angles.hh>
#include <core/calc/structural/protein_angles.hh>

#include <core/data/io/Pdb.hh>

int main(const int argc, const char* argv[]) {

  using namespace core::data::io;

  core::data::io::Pdb reader(argv[1], is_not_alternative); // file name (PDB format, may be gzip-ped)
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

  // Iterate over all chains
  for (auto it_chain = strctr->begin(); it_chain != strctr->end(); ++it_chain) {
    // Iterate over all residues
    const core::data::structural::Chain & c = **it_chain;
    for (size_t i = 1; i < c.size() - 2; ++i) {
      std::cout
          << utils::string_format("%s %c %4d %3s %8.3f %8.3f\n", strctr->code().c_str(), c.id(), c[i]->id(),
              c[i]->residue_type().code3.c_str(), core::calc::structural::evaluate_lambda(*c[i - 1], *c[i], *c[i + 1]),
              core::calc::structural::evaluate_dihedral_angle(
                  *std::static_pointer_cast<core::data::basic::Vec3>(c[i - 1]->find_atom(" CA ")),
                  *std::static_pointer_cast<core::data::basic::Vec3>(c[i]->find_atom(" CA ")),
                  *std::static_pointer_cast<core::data::basic::Vec3>(c[i + 1]->find_atom(" CA ")),
                  *std::static_pointer_cast<core::data::basic::Vec3>(c[i + 2]->find_atom(" CA "))));
    }
  }
}
