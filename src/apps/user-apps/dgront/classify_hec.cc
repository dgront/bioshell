#include <cstdio>
#include <ctime>
#include <cmath>
#include <iostream>
#include <sstream>
#include <vector>
#include <memory>
#include <chrono>
#include <chrono>
#include <utility> // for std::pair
#include <algorithm>

#include <core/index.hh>

#include <core/data/basic/Vec3.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/basic/Array2D.hh>
#include <core/data/io/load_decoys.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <core/calc/structural/classify_hec.hh>

#include <core/alignment/scoring/LocalStructure5.hh>
#include <core/alignment/scoring/LocalStructureMatch.hh>

#include <utils/io_utils.hh>
#include <utils/string_utils.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>

#include <utils/exit.hh>
#include <utils/LogManager.hh>
#include <utils/Logger.hh>
#include <core/calc/structural/interactions/BackboneHBondMap.hh>
#include <core/calc/structural/ProteinArchitecture.hh>

utils::Logger l("classify_hec");

// ---------- Data loaded from files by load_trainig_data() method
std::vector<core::data::basic::Coordinates_SP> structures;
std::vector<std::string> structure_ids;
std::vector<core::index2> structure_for_fragment;
std::vector<core::index2> residue_id_for_fragment;
std::vector<char> chain_id_for_fragment;

// ---------- Results from greedy clustering done by greedy_clustering() method
core::data::basic::Coordinates_SP all_coordinates;
std::vector<core::index4> cluster_ids;
std::vector<std::vector<core::index4>> cluster_members;

// --- selectors used by hbond-related methods to validate the structure
core::data::structural::ResidueIsTerminal terminal_residue;
core::data::structural::ProperlyConnectedCA check_R12;
core::data::structural::ResidueHasBB has_bb;


using namespace core;
typedef core::alignment::scoring::LocalStructureMatch<core::alignment::scoring::LocalStructure5,4> Matcher5;

//typedef double (core::alignment::scoring::local_structure_match<std::vector<real>::const_iterator,
//    real*, 4>) (q_it, (real*)) matching_function;

class MedoidCentroid {
public:
  core::index4 cluster_id;
  core::index4 size;
  core::index2 medoid_position;
  char chain_id;
  std::vector<double> centroid;
  std::string medoid_protein;

  MedoidCentroid(const core::index4 cluster_id,const core::index4 size, const std::vector<double> centroid_data,const core::index4 medoid_index) : cluster_id(cluster_id), size(size), centroid(8,0.0) {

    std::copy(centroid_data.cbegin(), centroid_data.cend(), centroid.begin());
    medoid_protein = structure_ids[structure_for_fragment[medoid_index]];
    medoid_position = residue_id_for_fragment[medoid_index];
    chain_id = chain_id_for_fragment[medoid_index];
  }

  inline double distance_to(const MedoidCentroid & mc) {

    double s = 0.0;
    for(core::index2 i = 0;i<4;++i) {
      double t = centroid[i] - mc.centroid[i];
      s += t*t;
    }

    return s;
  }

  void write_pdb(const std::string & outfname) {

    using namespace core::data::structural;

    core::data::io::Pdb reader(medoid_protein);
    core::data::structural::Structure_SP s = reader.create_structure(0);
    Structure::residue_iterator r_it = s->first_residue();
    l << utils::LogLevel::FINER << "Reaching "<<medoid_protein<<" "<<medoid_position<<" "<<chain_id<<"\n";
    while(((*r_it)->id()!=medoid_position) || ((*r_it)->owner()->id()!=chain_id)) {
      ++r_it;
    }
    std::shared_ptr<std::ostream> out = utils::out_stream(outfname);
    core::data::basic::Vec3 cm(0.0);
    double n = 0.0;
    Structure::residue_iterator r_it_copy = r_it; // tmp copy of the iterator to calculate CM
    for(core::index2 i=0;i<5;++i) {
      for (auto it = (*r_it_copy)->begin(); it != (*r_it_copy)->end(); it++) {
        cm += *(*it);
        n+=1.0;
      }
      ++r_it_copy;
    }
    cm /= n;
    for(core::index2 i=0;i<5;++i) {
      for (auto it = (*r_it)->begin(); it != (*r_it)->end(); it++) {
        (*(*it))-=cm;
        *out<< (*it)->to_pdb_line() << "\n";
      }
      ++r_it;
    }
    out->flush();
    (std::dynamic_pointer_cast<std::ofstream>(out))->close();
    std::cerr << outfname<<" saved\n";
  }
};

std::ostream& operator<<(std::ostream &out, const MedoidCentroid &mc) {

  out << utils::string_format("id: %5d size: %7d distances: %5.2f %5.2f %5.2f %5.2f  sdev: %5.2f %5.2f %5.2f %5.2f %s %d",
          mc.cluster_id, mc.size, mc.centroid[0], mc.centroid[1], mc.centroid[2], mc.centroid[3], mc.centroid[4], mc.centroid[5], mc.centroid[6], mc.centroid[7],mc.medoid_protein.c_str(),mc.medoid_position);

  return out;
}

core::index4 get_medoid_centroid(const std::vector<core::index4> cluster_elements,const Matcher5 & lm,std::vector<double> & medoid) {

  std::vector<double> avg(4,0);
  std::vector<double> sdv(4,0);
  for(core::index4 id : cluster_elements) {
    std::vector<double>::const_iterator it = lm.get_query_distances(id);
    for(int i=0;i<4;++i) {
      avg[i] += (*it);
      sdv[i] += (*it)*(*it);
      ++it;
    }
  }
  for(int i=0;i<4;++i) {
    avg[i] /= double(cluster_elements.size());
    sdv[i] /= double(cluster_elements.size());
    sdv[i] = sqrt(sdv[i] -  avg[i]*avg[i]);
  }

  for (int i = 0; i < 4; ++i) {
    medoid[i] = avg[i];
    medoid[i + 4] = sdv[i];
  }
  std::vector<double>::const_iterator it = lm.get_query_distances(cluster_elements[0]);

//  double min_dist = core::alignment::scoring::local_structure_match<std::vector<real>::const_iterator, real*, 4> (q_it, (real*))

  double min_dist = core::alignment::scoring::local_structure_match<std::vector<real>::const_iterator,std::vector<real>::const_iterator,4>(avg.cbegin(),it);
  core::index4 min_id = 0;
  for(core::index4 id : cluster_elements) {
    it = lm.get_query_distances(id);
    double d = core::alignment::scoring::local_structure_match<std::vector<real>::const_iterator,std::vector<real>::const_iterator,4>(avg.cbegin(),it);
    if(d<min_dist) {
      min_dist = d;
      min_id = id;
    }
  }

  return min_id;
}

void print_medoid_distance_matrix(const std::vector<std::shared_ptr<MedoidCentroid>> & medoids,std::ostream & where) {

  core::data::basic::Array2D<double> mm(medoids.size(),medoids.size());
  for(core::index2 i=1;i<medoids.size();++i) {
    for(core::index2 j=0;j<i;++j) {
      double d = medoids[i]->distance_to(*medoids[j]);
      mm.set(i,j,d);
      mm.set(j,i,d);
    }
  }

  mm.print(" %6.2f",where);
}

void print_medoid_distances(const std::vector<std::shared_ptr<MedoidCentroid>> & medoids,std::ostream & where) {

  for(core::index2 i=1;i<medoids.size();++i) {
    for(core::index2 j=0;j<i;++j) {
      double d = medoids[i]->distance_to(*medoids[j]);
      where << i<<" "<<j<<" "<<d<<"\n";
    }
  }
}
// ---------- Extracts distances for all possible 5-aa fragments from the loaded structures
void greedy_clustering(const double cutoff,const core::index4 min_size,const core::index4 n_clusters_max,std::vector<std::shared_ptr<MedoidCentroid>> & resulting_medoids) {

  unsigned long long cnt = 0;
  l << utils::LogLevel::INFO << "Greedy clustering of " << all_coordinates->size() << " possible fragments \n";
  auto start = std::chrono::high_resolution_clock::now();

  Matcher5 lm(all_coordinates,all_coordinates);
  cluster_ids.push_back(0);
  std::vector<core::index4> cluster_0;
  cluster_0.push_back(0);
  cluster_members.push_back(cluster_0);
  for(core::index4 i=1;i<all_coordinates->size()-4;++i) {
    if(structure_for_fragment[i]!=structure_for_fragment[i+4]) {
      i+=3;
      continue;
    }
    bool flag = true;
    core::index4 best_cluster = 0;
    double best_distance = lm(i, 0);
    for (core::index4 which_cluster = 0; which_cluster < cluster_ids.size(); ++which_cluster) {
      core::index4 j = cluster_ids[which_cluster];

      runtime_assert(j == cluster_members[which_cluster][0], "clusters inconsistency!")

      ++cnt;
      const double d = lm(i, j);
      if (d < cutoff) {
	if (best_distance > d) {
          best_cluster = which_cluster;
          best_distance = d;
          flag = false;
        }
      }
    }
    if (flag == true) {
      std::vector<core::index4> cluster_i;
      cluster_members.push_back(cluster_i);
      cluster_members.back().push_back(i);
      cluster_ids.push_back(i);
    } else {
      cluster_members[best_cluster].push_back(i);
    }
  }

  //---------- show timer stats
  auto end = std::chrono::high_resolution_clock::now();
  l << utils::LogLevel::INFO << "Clustered after "
      << (double) (std::chrono::duration_cast<std::chrono::milliseconds>(
          end - start).count()) << " [ms], " << (unsigned long long) cnt<<" comparisons performed\n";
  l << utils::LogLevel::INFO << cluster_ids.size()<<" clusters created at the greedy stage\n";

  //---------- select clusters larger than the cutoff size
  core::index4 largest_size = 0;
  std::vector<std::pair<core::index4,core::index4>> results;
  for (core::index4 which_cluster = 0; which_cluster < cluster_ids.size(); ++which_cluster) {
    if (largest_size < cluster_members[which_cluster].size()) largest_size = cluster_members[which_cluster].size();
    if (cluster_members[which_cluster].size() >= min_size)
      results.push_back(std::make_pair(which_cluster, cluster_members[which_cluster].size()));
  }

  //---------- sort them by size and print
  auto sort_by_size = [] (std::pair<unsigned int, unsigned int>  v1, std::pair<unsigned int, unsigned int>  v2) { return (v1.second > v2.second); };
  std::sort(results.begin(),results.end(),sort_by_size);

  l << utils::LogLevel::INFO << "Largest cluster size: " << results.size() << "\n";
  l << utils::LogLevel::INFO << results.size()<< " clustered ale larger than the cutoff size : " << int(min_size)<<"\n";

  for (core::index2 ic = 0; ic < std::min(core::index4(results.size()),n_clusters_max); ++ic) {
    std::vector<double> medoid_data(8, 0.0);
    core::index4 medoid_pos = get_medoid_centroid(cluster_members[results[ic].first], lm, medoid_data);
    std::shared_ptr<MedoidCentroid> mc = std::make_shared<MedoidCentroid>(ic,results[ic].second,medoid_data,medoid_pos);
    resulting_medoids.push_back(mc);
  }
}



void load_trainig_data() {

  using namespace utils::options;
  using namespace core::data::structural;

  // ---------- Load the PDB files as structures
  std::vector<std::string> names;
  std::vector<core::data::structural::Structure_SP> tmp_strct;
  structures_from_cmdline(names, tmp_strct);

  all_coordinates = std::make_shared<core::data::basic::Coordinates>();

  ResidueHasCA has_ca;
  // ---------- Note which structure a position comes from
  for (core::index2 i = 0; i < names.size(); ++i) {
    Structure_SP si = tmp_strct[i];
    if (si->count_residues() < 10) {
      l << utils::LogLevel::WARNING << names[i] << " contains only " << si->count_residues() << " residues, rejected\n";
      continue;
    }
    const core::index2 l = structure_ids.size();
    structure_ids.push_back(names[i]);

    for (Chain_SP ci : *si) {
      ci->erase(std::remove_if(ci->begin(), ci->end(), [&] (Residue_SP ri) { return !has_ca(*ri); }), ci->end());
      for(Residue_SP ri : *ci) {
        PdbAtom_SP ai = ri->find_atom(" CA ");
        all_coordinates->push_back(*ai);
        structure_for_fragment.push_back(l);
        residue_id_for_fragment.push_back(ri->id());
        chain_id_for_fragment.push_back(ri->owner()->id());
      }
    }
  }
}

bool hbond_distances(char ss_type, const core::data::structural::Residue &donor,
      const core::data::structural::Residue &acceptor, double &d1, double &d2, char &sheet_type) {

  using namespace core::data::structural;

  // --- test selectors: check if R12 are not too long and if the residues are not at termini
  if ((!check_R12(acceptor)) || (!check_R12(donor))) return false;
  if (terminal_residue(acceptor) || terminal_residue(donor)) return false;
  if ((!has_bb(acceptor)) || (!has_bb(donor))) return false;

  PdbAtom_SP atom_d = donor.find_atom(" CA ");
  PdbAtom_SP atom_a = acceptor.find_atom(" CA ");
  PdbAtom_SP atom_before_d = donor.previous()->find_atom(" CA ");
  PdbAtom_SP atom_before_a = acceptor.previous()->find_atom(" CA ");
  PdbAtom_SP atom_after_d = donor.next()->find_atom(" CA ");
  PdbAtom_SP atom_after_a = acceptor.next()->find_atom(" CA ");
  if (atom_a == nullptr || atom_after_a == nullptr || atom_before_a == nullptr || atom_d == nullptr || atom_after_d == nullptr || atom_before_d == nullptr)
    return false;

  Vec3 p1(*atom_d);
  p1 += *atom_before_d;
  p1 /= 2.0;
  Vec3 p2(*atom_d);
  p2 += *atom_after_d;
  p2 /= 2.0;

  Vec3 p3(*atom_a);
  p3 += *atom_before_a;
  p3 /= 2.0;
  Vec3 p4(*atom_a);
  p4 += *atom_after_a;
  p4 /= 2.0;
  double d13 = p1.distance_to(p3);
  double d14 = p1.distance_to(p4);
  double d23 = p2.distance_to(p3);
  double d24 = p2.distance_to(p4);
  if (fabs(d13 - d24) < fabs(d14 - d23)) { // p1 close to p3, p2 close to p4
    d1 = d13;
    d2 = d24;
    sheet_type = 'P';
  } else {
    d1 = d14;
    d2 = d23;
    sheet_type = 'A';
  }
  return true;
}

void observe_CA_hbonds() {

  using namespace utils::options;
  using namespace core::data::io;
  using namespace core::data::structural;

  // ---------- Load the list of PDB files to be processed
  std::vector<std::string> fnames;
  if (input_pdb_list.was_used()) utils::read_listfile(option_value<std::string>(input_pdb_list), fnames);

  // ---------- Iterate over structures on the list
  for(core::index2 i=0;i<fnames.size();++i) {
    // ---------- Create a reader and create a structure from the first model in each PDB file
    Pdb reader(fnames[i],is_standard_atom);
    auto strctr = reader.create_structure(0);
    std::string strctr_code = strctr->code();

    // ---------- Create a map of all hydrogen bonds from that structure, annotate the structure with SS information
    core::calc::structural::BackboneHBondMap hbmap(*strctr);
    core::calc::structural::ProteinArchitecture pa(*strctr);
    pa.annotate(*strctr);
//    hbmap.ss_by_dssp(*strctr);

    std::tuple<real, real, real> o; // --- helical geometry description is stored here
    std::tuple<real, real, real, char> out_beta; // --- helical geometry description is stored here

    // --- now generate list of non-bonded cases (negative distribution)
    char sheet_type = 'P'; // --- A or P
    for (auto donor_res_it = strctr->first_const_residue(); donor_res_it != strctr->last_const_residue(); ++donor_res_it) {
      auto donor_ca = (*donor_res_it)->find_atom(" CA ");
      const Residue &donor = (**donor_res_it);
      if (donor_ca == nullptr) continue;
      if (!check_R12(donor)) continue;
      if (terminal_residue(donor)) continue;

      int donor_index = hbmap.residue_index(donor);
      char ss_d = donor.ss();
      for (auto accpt_res_it = strctr->first_const_residue();
           accpt_res_it != strctr->last_const_residue(); ++accpt_res_it) {
        auto accpt_ca = (*accpt_res_it)->find_atom(" CA ");
        if (accpt_ca == nullptr) continue;
        if ((accpt_ca->distance_to(*donor_ca) > 10.0)) continue;
        const Residue &acceptor = (**accpt_res_it);
        if (!check_R12(acceptor)) continue;
        if (terminal_residue(acceptor)) continue;
        char ss_a = acceptor.ss();
        int acceptor_index = hbmap.residue_index(acceptor);

        int offset = donor_index - acceptor_index;
        if (offset == 4) { // --- dispatch alpha-helix
          core::calc::structural::describe_helical_ca_geometry(donor, acceptor, o);
          if (hbmap.are_H_bonded(donor, acceptor))
            std::cout << utils::string_format("%c %c %6.3f %6.3f %7.3f %4d %4d %4d %c YES H4 %s\n",
              ss_d, ss_a, std::get<0>(o), std::get<1>(o), std::get<2>(o), donor.id(), acceptor.id(),
              offset, sheet_type, strctr_code.c_str());
          else
            std::cout << utils::string_format("%c %c %6.3f %6.3f %7.3f %4d %4d %4d %c NO  H4 %s\n",
              ss_d, ss_a, std::get<0>(o), std::get<1>(o), std::get<2>(o), donor.id(), acceptor.id(),
              offset, sheet_type, strctr_code.c_str());
        }
        if (offset > 2) { // --- dispatch strands
          core::calc::structural::describe_beta_ca_geometry(donor, acceptor, out_beta);
          if (std::get<3>(out_beta) == 'P') { // --- parallel sheet
            if (hbmap.is_parallel_bridge(donor_index,acceptor_index))
              std::cout << utils::string_format("%c %c %6.3f %6.3f %7.3f %4d %4d %4d %c YES E %s\n",
                ss_d, ss_a, std::get<0>(out_beta), std::get<1>(out_beta), std::get<2>(out_beta), donor.id(),
                acceptor.id(), offset, std::get<3>(out_beta), strctr_code.c_str());
            else
              std::cout << utils::string_format("%c %c %6.3f %6.3f %7.3f %4d %4d %4d %c NO  E %s\n",
                ss_d, ss_a, std::get<0>(out_beta), std::get<1>(out_beta), std::get<2>(out_beta), donor.id(),
                acceptor.id(), offset, std::get<3>(out_beta), strctr_code.c_str());
          } else { // --- antiparallel sheet
            if (hbmap.is_antiparallel_bridge(donor_index,acceptor_index))
              std::cout << utils::string_format("%c %c %6.3f %6.3f %7.3f %4d %4d %4d %c YES E %s\n",
                ss_d, ss_a, std::get<0>(out_beta), std::get<1>(out_beta), std::get<2>(out_beta), donor.id(),
                acceptor.id(), offset, std::get<3>(out_beta), strctr_code.c_str());
            else
              std::cout << utils::string_format("%c %c %6.3f %6.3f %7.3f %4d %4d %4d %c NO  E %s\n",
                ss_d, ss_a, std::get<0>(out_beta), std::get<1>(out_beta), std::get<2>(out_beta), donor.id(),
                acceptor.id(), offset, std::get<3>(out_beta), strctr_code.c_str());
          }
        }
      }
    }
  }
}

void classify_12(const core::data::basic::Coordinates_SP ca_trace,std::vector<unsigned char> & assignment) {

  core::alignment::scoring::LocalStructure5 s(ca_trace);
  if(assignment.size()<ca_trace->size())
    assignment.resize(ca_trace->size());
  core::calc::structural::classify_12(s,assignment);
}

int main(const int argc, const char* argv[]) {

  utils::options::Option train("-train", "-classify_hec:train", "run the 5-mers clustering to get centroids");
  utils::options::Option train_hbonds("-train_hbonds", "-classify_hec:train::hbonds", "get distribution of CA-based hbond distances");
  utils::options::Option cutoff("-c", "-classify_hec:clustering_cutoff", "cutoff distance value to stop the greedy clustering of 5-aa fragments");
  utils::options::Option min_size("-min_size", "-classify_hec:min_size", "minimum size of a cluster to be included in results");
  utils::options::Option n_clust("-nc", "-classify_hec:n_clusters", "maximum number of clusters to print (ordered by size)");
  utils::options::Option n_clust_structures("-nc_struct", "-classify_hec:n_clusters_pdb", "maximum number of cluster structures to write in the PDB format (ordered by size)");

  using namespace utils::options;
  utils::options::OptionParser & cmd = utils::options::OptionParser::get();
  cmd.register_option(utils::options::db_path, utils::options::help, utils::options::verbose);
  cmd.register_option(input_pdb, input_pdb_native, input_pdb_list, input_pdb_modelslist, input_pdb_path); // input PDB structures
  cmd.register_option(all_models,ca_only,bb_only);
  cmd.register_option(cutoff, min_size, n_clust, n_clust_structures, train, train_hbonds);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  const double clustering_cutoff = utils::options::option_value<double>(cutoff, 0.5);
  const core::index4 min_cluster_size = utils::options::option_value<core::index4>(min_size, 1000);
  const core::index2 n_clusters = utils::options::option_value<core::index2>(n_clust, 50);
  const core::index2 n_clusters_pdb = utils::options::option_value<core::index2>(n_clust_structures, 20);


  // ---------- Training mode
  if(train_hbonds.was_used()) {
    observe_CA_hbonds();
    return 0;
  }

  if(train.was_used()) {
    if(!input_pdb_list.was_used()) {
      l << utils::LogLevel::SEVERE << "A list of input structures must be provided with -in:pdb:listfile option to run the clustering\n";
      return 0;
    }
    load_trainig_data();
    std::vector<std::shared_ptr<MedoidCentroid>> medoids;
    greedy_clustering(clustering_cutoff,min_cluster_size,n_clusters,medoids);
    if(medoids.size()>n_clusters)
      medoids.resize(n_clusters);
    std::ofstream out("cluster-distances");
    print_medoid_distances(medoids,out);

  for(std::shared_ptr<MedoidCentroid> mc : medoids)
      std::cout << *mc << "\n";

    core::index2 mi = 0;
    for (std::shared_ptr<MedoidCentroid> mc : medoids) {
      if (mi < n_clusters_pdb) mc->write_pdb(utils::string_format("cluster%d.pdb", mi));
      else break;
      ++mi;
    }

    return 0;
  }

  // ---------- Classification mode
  if (!cmd.was_used(ca_only)) cmd.inject_option(ca_only, "");
  std::vector<std::string> structure_names;
  std::vector<core::data::structural::Structure_SP> structures;
  structures_from_cmdline(structure_names, structures);
  for (const core::data::structural::Structure_SP s : structures) {
    core::data::basic::Coordinates_SP ca_trace = std::make_shared<std::vector<core::data::basic::Vec3>>();
    core::data::structural::structure_to_coordinates(s,*ca_trace,core::data::structural::IsCA());
    std::cerr << ca_trace->size() << "\n";
    core::alignment::scoring::LocalStructure5 l5(ca_trace);
    std::string out = core::calc::structural::classify_hec(l5);
    std::cout << out << "\n";
  }
}

