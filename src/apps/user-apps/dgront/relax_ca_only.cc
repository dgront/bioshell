#include <vector>
#include <string>

#include <core/data/io/Pdb.hh>
#include <utils/options/OptionParser.hh>
#include <core/BioShellVersion.hh>
#include <utils/options/input_options.hh>
#include <utils/options/sampling_options.hh>
#include <utils/options/output_options.hh>
#include <simulations/SimulationSettings.hh>
#include <core/data/io/ss2_io.hh>
#include <core/data/io/fasta_io.hh>
#include <utils/LogManager.hh>

// ---- energy
#include <simulations/forcefields/TotalEnergyByAtom.hh>
#include <simulations/forcefields/cabs/CabsR13.hh>
#include <simulations/forcefields/cabs/CabsR14.hh>
#include <simulations/forcefields/cabs/CabsR15.hh>
#include <simulations/systems/SimpleAtomTyping.hh>
#include <simulations/forcefields/mf/R12.hh>
#include <simulations/forcefields/cartesian/CAHydrogenBond.hh>
#include <simulations/forcefields/cartesian/CartesianConstraints.hh>

// ---- movers & sampler
#include <simulations/movers/TranslateAtom.hh>
#include <simulations/movers/MoversSetSweep.hh>
#include <simulations/sampling/SimulatedAnnealing.hh>

// --- observations
#include <simulations/observers/ObserveEvaluators.hh>
#include <simulations/observers/AdjustMoversAcceptance.hh>
#include <simulations/evaluators/CallEvaluator.hh>
#include <simulations/evaluators/Timer.hh>
#include <simulations/observers/cartesian/PdbObserver.hh>
#include <simulations/observers/ObserveEnergyComponents.hh>
#include <simulations/evaluators/cartesian/CrmsdEvaluator.hh>
#include <simulations/observers/cartesian/ExplicitPdbFormatter.hh>
#include <simulations/movers/PerturbChainFragment.hh>

using namespace utils::options;

using namespace simulations::forcefields;
using namespace simulations::forcefields::cartesian;
using namespace simulations::movers;
using namespace simulations::systems;
using namespace simulations::evaluators;
using namespace simulations::observers;

utils::Logger logs("relax_ca_only");

core::data::sequence::SecondaryStructure_SP get_ss_from_cmd_line(const simulations::SimulationSettings &settings) {

  if (settings.has_key(input_ss2)) { // ---------- Read SS2 format
    std::string input_ss2_file = settings.get<std::string>(input_ss2);
    return core::data::io::read_ss2(input_ss2_file);
  } else if (settings.has_key(input_fasta_ss)) { // ---------- Read SS fasta format
    const std::string input_ss_file = settings.get<std::string>(input_fasta_ss);
    return core::data::io::read_fasta_ss_file(input_ss_file);
  } else {
    logs << utils::LogLevel::SEVERE << "No input file. Provide pdb, SS2 or fasta_ss file.\n";
    exit(1);
  }
}

int main(int cnt, const char* argv[]) {
  using namespace utils::options; // --- All the options are in this namespace

  utils::LogManager::INFO();
  logs << utils::LogLevel::INFO << "BioShell version:\n" << core::BioShellVersion().to_string() << "\n";

  utils::options::Option dmax("-dmax", "-displacement", "maximum displacement allowed for each CA atom");
  utils::options::Option erep("-erep", "-energy_report", "write per-residue energy report after relax");

  utils::options::OptionParser &cmd = utils::options::OptionParser::get();
  cmd.register_option(utils::options::help, verbose, db_path, rnd_seed);
  cmd.register_option(mc_outer_cycles(100), mc_inner_cycles(10), mc_cycle_factor(1));
  cmd.register_option(random_jump_range(0.3));
  cmd.register_option(input_pdb_native, input_ss2, input_fasta_ss);  // Input options
  cmd.register_option(input_pdb, input_fasta_ss, input_pdb_native);  // More input options
  cmd.register_option(output_pdb); // output options
  cmd.register_option(begin_temperature(0.1), end_temperature(0.01), temp_steps(10), dmax(1.0));
  cmd.register_option(dmax(1.0), erep);

  core::index2 id = cmd.add_examples_group("Relax a PDB file :");

  cmd.add_example(id, "relax a PDB file - default settings",
      "relax_ca_only -in:pdb=2gb1.pdb -in:ss2=2gb1A.ss2");

  cmd.add_example(id, "relax a PDB file - extensive sampling",
      "relax_ca_only -in:pdb=2gb1.pdb -in:ss2=2gb1A.ss2  -sample:mc_outer_cycles=100 -sample:mc_inner_cycles=100");

  cmd.add_example(id, "relax a PDB file - extensive sampling with larger cutoff",
      "relax_ca_only -in:pdb=2gb1.pdb -in:ss2=2gb1A.ss2  -sample:mc_outer_cycles=100 -sample:mc_inner_cycles=100 -dmax=2.0");

  // ---------- Parse the command line flags, set up simulation settings
  if (!cmd.parse_cmdline(cnt, argv)) return 1;
  simulations::SimulationSettings settings;
  settings.insert_or_assign(cmd, true);

  // ---------- Read the input PDB to be relaxed
  core::data::io::Pdb reader(option_value<std::string>(input_pdb), core::data::io::is_ca, core::data::io::keep_all, true);
  core::data::structural::Structure_SP strctr = reader.create_structure(0);

  // ---------- Create the system to be relaxed
  std::vector<std::string> ca_vec( {" CA "});
  std::vector<std::string> aa_vec( {"ALA", "ARG", "ASN", "ASP", "CYS", "GLU", "GLN", "GLY", "HIS", "ILE", "LEU", "LYS",
                                    "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"});
  AtomTypingInterface_SP ca_type = std::make_shared<simulations::systems::SimpleAtomTyping>(aa_vec, ca_vec, ca_vec);  // atom typing is used to assign FF parameters to each atom
  CartesianChains chains(ca_type, *strctr);
  int i = -1;
  for (auto a_it=strctr->first_const_atom(); a_it != strctr->last_const_atom(); ++a_it) {
    chains[++i].set(**a_it);
  }
  CartesianChains chains_backup(chains);           // --- make a backup system

  // ---------- Read the input sequence and secondary structure
  core::data::sequence::SecondaryStructure_SP secondary_str = get_ss_from_cmd_line(settings);

  // ---------- Energy function: container for the total
  auto total_en = std::make_shared<simulations::forcefields::TotalEnergyByAtom>();
  // ---------- R12 i.e. pseudobonds
  ByAtomEnergy_SP r12 = std::static_pointer_cast<ByAtomEnergy>(std::make_shared<mf::R12>(chains));
  total_en->add_component(r12, 10.0);
  // ---------- R13
  ByAtomEnergy_SP r13 = std::static_pointer_cast<ByAtomEnergy>(std::make_shared<cabs::CabsR13>(chains, secondary_str->sequence, secondary_str->str()));
  total_en->add_component(r13, 1.0);
  // ---------- R14
  ByAtomEnergy_SP r14 = std::static_pointer_cast<ByAtomEnergy>(std::make_shared<cabs::CabsR14>(chains, secondary_str->sequence, secondary_str->str()));
  total_en->add_component(r14, 0.2);
  // ---------- R15
  ByAtomEnergy_SP r15 = std::static_pointer_cast<ByAtomEnergy>(std::make_shared<cabs::CabsR15>(chains, secondary_str->sequence, secondary_str->str()));
  total_en->add_component(r15, 1.0);
  // ---------- CartesianConstraints
  double d_max = option_value<double>(dmax);
  ByAtomEnergy_SP cc = std::static_pointer_cast<ByAtomEnergy>(std::make_shared<CartesianConstraints>(chains, d_max, 100.0));
  total_en->add_component(cc, 1.0);

  // ---------- CA-only H-bonds
  ByAtomEnergy_SP ca_hb =
      std::static_pointer_cast<ByAtomEnergy>(std::make_shared<simulations::forcefields::cartesian::CAHydrogenBond>(chains, secondary_str->str(), "HE"));
  total_en->add_component(ca_hb, 10.0);

  logs << utils::LogLevel::INFO << "starting energy: " << total_en->energy(chains) << "\n";

  // ---------- Movers
  auto moves = std::make_shared<MoversSetSweep>();
  std::shared_ptr<TranslateAtom> translate = std::make_shared<TranslateAtom>(chains, chains_backup, *total_en);
  translate->max_move_range(0.25);
  moves->add_mover(translate, chains.n_atoms);
  auto perturb_frag = std::make_shared<PerturbChainFragment>(chains, chains_backup, 6, *total_en);
  perturb_frag->max_move_range(0.25);
  moves->add_mover(perturb_frag, chains.n_atoms);

  core::calc::statistics::Random::seed(712345);
  // ---------- Sampler
  simulations::sampling::SimulatedAnnealing sa(moves);

  // ---------- Observers
  typedef typename std::function<double(void)> ObservedFunctor;
  ObservedFunctor recent_energy = ([total_en, &chains]() { return total_en->energy(chains); });
  ObservedFunctor recent_temp = ([&sa]() { return sa.temperature(); });

  // ---------- Observerve evaluators: energy, r-end, cm
  auto obs_evaluators = std::make_shared<simulations::observers::ObserveEvaluators>("evaluators.dat");
  auto evaluate_energy_sp = std::make_shared<CallEvaluator<ObservedFunctor>>(recent_energy, "energy");
  auto evaluate_temp_sp = std::make_shared<CallEvaluator<ObservedFunctor>>(recent_temp, "temperature");
  auto rms = std::make_shared<simulations::evaluators::cartesian::CrmsdEvaluator>(strctr, chains);
  auto timer = std::make_shared<Timer>();
  obs_evaluators->add_evaluator(evaluate_energy_sp);
  obs_evaluators->add_evaluator(evaluate_temp_sp);
  obs_evaluators->add_evaluator(timer);
  obs_evaluators->add_evaluator(rms);
  sa.outer_cycle_observer(obs_evaluators);
  std::shared_ptr<simulations::observers::cartesian::AbstractPdbFormatter> fmt =
      std::make_shared<simulations::observers::cartesian::ExplicitPdbFormatter>(*strctr);
  auto trajectory = std::make_shared<simulations::observers::cartesian::PdbObserver>(chains, fmt, "trajectory.pdb");
  sa.outer_cycle_observer(trajectory);


  // --- Create observer for energy components and movers
  auto obs_en = std::make_shared<ObserveEnergyComponents>(*total_en, chains, "energy.dat", true);
  sa.outer_cycle_observer(obs_en);

  std::shared_ptr<simulations::observers::AdjustMoversAcceptance> observe_moves
      = std::make_shared<simulations::observers::AdjustMoversAcceptance>(*moves,"movers.dat", 0.4);

  sa.outer_cycle_observer(observe_moves);

  // ---------- Make observations for the input structure (e.g. the native) for comparison & analysis
  trajectory->observe();
  obs_evaluators->observe();
  obs_en->observe();

  // --- RUN !
  sa.reset(settings);
  sa.run();
  obs_evaluators->finalize();

  simulations::observers::cartesian::PdbObserver final(chains, fmt, "final.pdb");
  final.observe();
  if(erep.was_used()) {
    for(int i=0;i<chains.n_atoms;++i) {
      std::cout << utils::string_format(" %3d", i);
      for(int e =0;e<total_en->count_components();++e) {
        std::cout << utils::string_format(" %7.3f", total_en->get_component(e)->energy(chains, i));
      }
      std::cout << "\n";
    }
  }
}

