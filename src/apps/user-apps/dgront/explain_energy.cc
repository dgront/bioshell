#include <iostream>
#include <map>
#include <cstdio>
#include <ctime>
#include <iomanip>
#include <memory>
#include <algorithm>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/transformations/Crmsd.hh>
#include <core/calc/structural/transformations/PairwiseCrmsd.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>

#include <simulations/systems/ResidueChain_OBSOLETE.hh>
#include <simulations/forcefields/cabs/CabsA13.hh>
#include <simulations/forcefields/cabs/CabsR13.hh>
#include <simulations/forcefields/cabs/CabsR15.hh>
#include <simulations/forcefields/cabs/CabsT14.hh>
#include <simulations/forcefields/LongRangeByResidues.hh>
#include <simulations/forcefields/cartesian/RamachandranFF.hh>
#include <simulations/forcefields/TotalEnergyByResidue.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <utils/options/select_options.hh>
#include <utils/options/scoring_options.hh>
#include <utils/options/calc_options.hh>
#include <utils/options/selector_from_cmdline.hh>
#include <utils/exceptions/NoSuchFile.hh>
#include <utils/Logger.hh>
#include <utils/io_utils.hh>
#include <ui/www/web_utils.hh>

#include <ui/www/WebServer.hh>
#include <simulations/systems/cabs/CabsBbAtomTyping.hh>
#include <simulations/forcefields/cartesian/cartesian_force_field_factory.hh>

using namespace core::data::sequence;
using namespace core::data::structural;

utils::Logger logger("explain_energy");

std::string pdb_path;
std::string app_html = R"(
<html>
<head>
</head>
<body>
  <form name='nn' action="superimpose" method="POST">
    <input type="text" id="pdbid" name="pdbid" value="2kmk" />
    all models?
    <input type="checkbox" name="all_models" id="all_models" checked><br />
    <textarea rows="10" cols="10" name="match_atoms" id="match_atoms">
      *:*:_CA_
    </textarea>
    <textarea rows="10" cols="10" name="rot_atoms" id="rot_atoms">
      *:*:_CA_
    </textarea>
    <br />
    <input type="submit" value="calculate" />
  </form>
</body>
</html)";


void show_empty_form(ui::www::Response* res) {

  res->send(app_html);
}

std::string cabs_scfx =
  R"(
CabsA13                 2.0 -  ${INPUT_SS2} 0.01
CabsR13                 2.0 -  ${INPUT_SS2} 0.01
CabsT14                 2.0 -  ${INPUT_SS2} 0.01
CabsR15                 2.0 -  ${INPUT_SS2} 0.01
)";
void tabulate_ramachandran(const simulations::systems::ResidueChain<core::data::basic::Vec3> & system, core::index2 pos, double step) {

  double factor = 3.14159 / 180.0;
  simulations::forcefields::cartesian::RamachandranFF<core::data::basic::Vec3> rama(system, "-");
  for (core::index2 i = 1; i < system.count_residues() - 1; ++i) {
    std::cerr<<i<<"\n";
    for (double phi = -180.0; phi < 180.0; phi += step) {
      for (double psi = -180.0; psi < 180.0; psi += step) {
        double r = rama.calculate_by_residue(i, phi * factor, psi * factor);
        if (r < -18) std::cout << i << " " << phi << " " << psi << " " << r << "\n";
      }
    }
  }
}

std::vector<double> &evaluate_R15(simulations::systems::ResidueChain<core::data::basic::Vec3> model, SecondaryStructure_SP secondary,
                                      double pseudocounts,
                                      std::vector<double> &out) {

  simulations::forcefields::cabs::CabsR15<core::data::basic::Vec3> r15(model, "-", secondary, pseudocounts);

  if (model.count_residues() < r15.property_span) return out;

  for (unsigned int i = 0; i < unsigned(model.count_residues() - 4); ++i) {
    out[i] = r15.calculate_by_residue(i);
  }

  return out;
}

void evaluate_T14(simulations::systems::ResidueChain<core::data::basic::Vec3> model, SecondaryStructure_SP secondary, double pseudocounts, std::vector<double> & out_scores) {

  simulations::forcefields::cabs::CabsT14<core::data::basic::Vec3> t14(model, "-", secondary, pseudocounts);

  if(model.count_residues()<4) return;

  for (unsigned int i = 0; i < unsigned(model.count_residues() - 3); ++i)
    out_scores[i] = t14.calculate_by_residue(i);
}

void rescore(const std::string & scfx_cfg,const std::vector<core::data::structural::Structure_SP> & models,
    std::ostream & out) {

  using namespace utils::options;

  // ---------- Atom mapping & scored system
  simulations::systems::AtomTypingInterface_SP mapping = std::make_shared<simulations::systems::cabs::CABSAtomTyping>();
  simulations::systems::ResidueChain<core::data::basic::Vec3> rc(mapping,*models[0]);

  // ---------- Energy function
  std::shared_ptr<simulations::forcefields::TotalEnergyByResidue> en = nullptr;
  if (!scfx_config.was_used()) {
    utils::replace_substring(cabs_scfx, "${INPUT_PDB}", option_value<std::string>(input_pdb)); // Once again, read the input_pdb to get the input structure -> will be used as the reference
    utils::replace_substring(cabs_scfx, "${INPUT_SS2}", option_value<std::string>(input_ss2)); // input_ss2 option provides SS2 data for A13 energy
    logger << utils::LogLevel::INFO << "The energy cfg is:\n" << cabs_scfx << "\n";
    std::stringstream ss(cabs_scfx);
    en = simulations::forcefields::cartesian::from_cfg_file<core::data::basic::Vec3>(rc, ss);
  } else {
    en = simulations::forcefields::cartesian::from_cfg_file<core::data::basic::Vec3>(rc,
      option_value<std::string>(scfx_config));
  }
  std::vector<double> en_by_res(rc.count_residues());

  for(const core::data::structural::Structure_SP & s_sp : models) {
    core::index2 i = 0;
    for (auto i_at = s_sp->first_atom(); i_at != s_sp->last_atom(); ++i_at) {
      rc[i].set(**i_at);
      ++i;
    }
    for(core::index2 ires=0;ires<rc.count_residues();++ires) {
      out <<utils::string_format("%4d ",ires);
      for (core::index2 ie = 0; ie < en->count_components(); ++ie) {
        auto en_sp = en->get_component(ie);
        out << utils::string_format("%6f ",en_sp->calculate_by_residue(ires));
      }
      out << "\n";
    }
  }
}

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::basic;
  using namespace core::data::structural;
  using core::calc::structural::transformations::PairwiseCrmsd;

  // ********** INPUT SECTION **********
  // ---------- Set up command line parser and parse the given options ----------
  utils::options::OptionParser & cmd = OptionParser::get();
  cmd.register_option(utils::options::help, show_examples, verbose, db_path);
  cmd.register_option(input_pdb, input_pdb_list,input_pdb_path); // input PDB structures
  cmd.register_option(all_models, ca_only, bb_only, include_hydrogens);
  cmd.register_option(scfx_config, input_ss2);
  cmd.parse_cmdline(argc, argv, true);

  // ---------- Read the PDB input files from cmdline ----------
  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);

  // ---------- Read the secondary structure - it is necessary to define local potentials ----------
  SecondaryStructure_SP secondary_sp = nullptr;
  if (input_ss2.was_used()) secondary_sp = core::data::io::read_ss2(option_value<std::string>(input_ss2));
  else {
    logger << utils::LogLevel::CRITICAL<<"Secondary structure must be provided for the structure for scoring\n";
    return 0;
  }

    rescore(option_value<std::string>(scfx_config),structures,std::cout);


  return EXIT_SUCCESS;
}
