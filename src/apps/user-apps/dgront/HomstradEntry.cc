#include <iostream>
#include <memory>
#include <vector>

#include <core/index.hh>

#include <core/algorithms/predicates.hh>
#include <core/data/io/pir_io.hh>
#include <core/data/io/Pdb.hh>
#include <core/alignment/MultipleSequenceAlignment.hh>
#include <core/alignment/on_alignment_computations.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/transformations/Crmsd.hh>

#include <utils/Logger.hh>

#include <apps/user-apps/dgront/HomstradEntry.hh>

namespace apps {
namespace user_apps {
namespace dgront {

using namespace core::data::sequence;
using namespace core::data::structural;
using namespace core::alignment;
using namespace core::data::io;

HomstradPairData::HomstradPairData(HomstradEntry &entry, const std::string &query_id, const std::string &tmplt_id)
  : query_name(query_id), tmplt_name(tmplt_id), entry_(entry) {

  core::data::structural::Chain_SP q, t;
  std::vector<PdbAtom> qv, tv;

  PairwiseAlignment ali(0, entry_.get_row(query_id), entry_.get_row(tmplt_id));

  std::string aligned_q_seq = ali.get_aligned_query(entry_.get_sequence(query_name)->sequence,'-');
  std::string aligned_t_seq = ali.get_aligned_template(entry_.get_sequence(tmplt_name)->sequence,'-');
  n_identical_ = sum_identical(aligned_q_seq,aligned_t_seq);
  n_query_gaps_ = std::count(aligned_q_seq.begin(), aligned_q_seq.end(), '-');
  n_tmplt_gaps_ = std::count(aligned_t_seq.begin(), aligned_t_seq.end(), '-');
  ali.get_aligned_query_template(*entry_.structures[query_id], *entry_.structures[tmplt_id], *q, *t);
  q_len_ = entry_.get_sequence(query_name)->length();
  t_len_ = entry_.get_sequence(tmplt_name)->length();

//  entry_.aligned_coordinates(3, 0, q, t);
  for (const Residue_SP resi : *q) qv.push_back(*resi->find_atom(" CA "));
  for (const Residue_SP resi : *t) tv.push_back(*resi->find_atom(" CA "));
  core::calc::structural::transformations::Crmsd<std::vector<PdbAtom>, std::vector<PdbAtom>> rms;
  crmsd_ = rms.crmsd(qv, tv, qv.size());

}

HomstradEntry::HomstradEntry(const std::string &fname) : l("HomstradEntry") {

  std::vector<Sequence_SP> database;  // --- stores all database sequences
  read_pir_file(fname, database);
  for (Sequence_SP si:database) {
    std::shared_ptr<PirEntry> p = std::dynamic_pointer_cast<PirEntry>(si);
    add_row(p->code(), p);
  }
  load_structures();

  for (auto qi = cbegin(); qi != cend(); ++qi) {
    for (auto ti = cbegin(); ti != qi; ++ti) {
      pairwise_results.emplace_back(*this, *qi, *ti);
      std::cerr << pairwise_results.back()<<"\n";
    }
  }

}


void HomstradEntry::load_structures() {

  for (auto it = cbegin(); it != cend(); ++it) {
    PirEntry_SP ee = std::dynamic_pointer_cast<PirEntry>(get_sequence(*it));
    Pdb reader(find_pdb_file_name(ee->code(), "./"), all_true(is_not_hydrogen, is_not_water), true);
    auto s = reader.create_structure(0);
    Chain_SP chain = s->get_chain(ee->first_chain_id());
    SelectResidueRange sel(utils::string_format("%d-%d", ee->first_residue_id(), ee->last_residue_id()));
    core::algorithms::Not<SelectResidueRange> reverted_selector(sel);
    l << utils::LogLevel::INFO << "selecting: " << sel.selector_string() << "\n";
    chain->erase(std::remove_if(chain->begin(), chain->end(), reverted_selector), chain->end());
    structures[ee->code()] = chain;
    l << utils::LogLevel::INFO << s->code() << " structure loaded of " << structures[ee->code()]->size() <<
    " residues\n";
  }
}

void HomstradEntry::aligned_coordinates(core::index2 which_query, core::index2 which_tmplt,
    std::vector<Residue_SP> &aligned_query, std::vector<Residue_SP> &aligned_tmplt) {

  const std::string &q_name = get_key(which_query);
  const std::string &t_name = get_key(which_tmplt);
  PairwiseAlignment ali(0, get_row(q_name), get_row(t_name));
  ali.get_aligned_query_template(*structures[q_name], *structures[t_name], aligned_query, aligned_tmplt);
}

std::ostream & operator<<(std::ostream & out, const HomstradPairData & e) {

  out << utils::string_format("%s %4d %3d   %s %4d %3d  %4d %5.2f",
    e.query_name.c_str(),e.query_length(),e.count_query_gaps(),
    e.tmplt_name.c_str(),e.tmplt_length(),e.count_tmplt_gaps(),
    e.count_identical(),e.crmsd());

  return out;
}

}
}
}