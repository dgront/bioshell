#include <cstdio>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <memory>
#include <vector>
#include <iostream>

#include <core/index.hh>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>

#include <utils/io_utils.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/select_options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/align_options.hh>

#include <core/alignment/SemiglobalAligner.hh>
#include <core/alignment/scoring/SimilarityMatrix.hh>
#include <core/alignment/scoring/SimilarityMatrixScore.hh>
#include <core/alignment/scoring/NcbiSimilarityMatrixFactory.hh>

#include <core/calc/structural/transformations/Crmsd.hh>

#include <utils/Logger.hh>

utils::Logger l("superimpose_by_sequence");
typedef core::data::structural::Structure::atom_iterator atom_iterator;

/// Calculates structural alignment of two protein chains based on their sequence alignment.
/**
 *
 */
int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::structural;
  using namespace core::alignment::scoring;

  utils::options::OptionParser & cmd = OptionParser::get();
  cmd.register_option(help,verbose, db_path);

  // ---------- input options
  cmd.register_option(query_pdb,template_pdb,substitution_matrix); // input PDB structures
  cmd.register_option(include_hydrogens,select_residues_by_code3);
  cmd.register_option(verbose,help);

  // ---------- output options
  cmd.register_option(output_pdb);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // ********** INPUT SECTION **********
  Pdb q_reader(option_value<std::string>(query_pdb));
  Structure_SP query = q_reader.create_structure(0);
  std::cerr << query->count_atoms()<<"\n";
  Chain & query_chain = *(*query)[0];
  query_chain.erase(std::remove_if(query_chain.begin(),query_chain.end(),[](Residue_SP r){ return r->find_atom(" CA ")==nullptr;}),query_chain.end());
  core::data::sequence::Sequence_SP query_seq = query_chain.create_sequence();

  Pdb t_reader(option_value<std::string>(template_pdb));
  Structure_SP tmplt = t_reader.create_structure(0);
  Chain & tmplt_chain = *(*tmplt)[0];
  core::data::sequence::Sequence_SP tmplt_seq = tmplt_chain.create_sequence();
  std::remove_if(tmplt_chain.begin(),tmplt_chain.end(),[](Residue_SP r){ return r->find_atom(" CA ")==nullptr;});

  // ********** SEQUENCE ALIGNMENT SECTION **********
  core::alignment::SemiglobalAligner<short,SimilarityMatrixScore<short>> aligner(std::max(tmplt_seq->length(),query_seq->length()));
  std::string matrix_name = option_value<std::string>(substitution_matrix);
  std::shared_ptr<SimilarityMatrix<short>> sim_m = NcbiSimilarityMatrixFactory::get().load_matrix(matrix_name);

  SimilarityMatrixScore<short> score(query_seq->sequence, tmplt_seq->sequence, *sim_m);
  aligner.align(-10,-1,score);
  core::alignment::PairwiseAlignment_SP ali = aligner.backtrace();

  // --- Copy CA atom (if found) from each query residue
  std::vector<Vec3> query_ca, tmplt_ca;
  int j=0;
  for(unsigned short i=0;i<ali->query_length();++i)
    if((j=ali->which_template_for_query(i))>-1)
        tmplt_ca.push_back(*tmplt_chain[j]->find_atom(" CA "));

  // --- Copy CA atom (if found) from each template residue
  for(unsigned short i=0;i<ali->template_length();++i)
    if((j=ali->which_query_for_template(i))>-1)
        query_ca.push_back(*query_chain[j]->find_atom(" CA "));

  // --- superimpose the query CA atoms on the template CA atoms
  core::calc::structural::transformations::Crmsd<std::vector<Vec3>,std::vector<Vec3>> crmsd;
  double crmsd_val = crmsd.crmsd(query_ca,tmplt_ca,query_ca.size());

  // --- Find pairs with very high distance
  Vec3 tmp;
  std::vector<core::index2> too_far;
  for (core::index2 i = 0; i < query_ca.size(); ++i) {
    crmsd.apply(query_ca[i], tmp);
    if (tmp.distance_to(tmplt_ca[i]) > crmsd_val) too_far.push_back(i);
//    std::cout <<i<<" "<<tmp.distance_to(tmplt_ca[i])<<"\n";
  }
  // --- Remove the pairs that are distant from the superposition set
  std::sort(too_far.rbegin(), too_far.rend());
  for(core::index2 i_far: too_far) {
    query_ca.erase(query_ca.begin() + i_far);
    tmplt_ca.erase(tmplt_ca.begin() + i_far);
  }
  // --- Superimpose once again
  crmsd_val = crmsd.crmsd(query_ca,tmplt_ca,query_ca.size());
  for (core::index2 i = 0; i < query_ca.size(); ++i) {
    crmsd.apply(query_ca[i], tmp);
//    std::cout <<i<<" "<<tmp.distance_to(tmplt_ca[i])<<"\n";
  }

  std::string out_fname = (output_pdb.was_used()) ? option_value<std::string>(output_pdb) : "out.pdb";
  std::shared_ptr<std::ostream> out = utils::out_stream(out_fname);
  query = q_reader.create_structure(0);
  for(auto atom_it = query->first_atom();atom_it!=query->last_atom();++atom_it) {
    crmsd.apply(**atom_it);
    *out << (*atom_it)->to_pdb_line()<<"\n";
  }

}
