#include <iostream>

#include <core/data/io/clustalw_io.hh>
#include <core/data/sequence/Sequence.hh>
#include <core/calc/statistics/simple_statistics.hh>
#include <core/data/basic/Array2D.hh>

#include <utils/io_utils.hh>


utils::Logger l("msa_column_correlation");

const std::string cg_rotamers_info = R"(
      Reads a MSA (clustalW format) and evaluates correlation between any pair of columns.)";

int main(const int argc, const char* argv[]) {

  std::vector<core::data::sequence::Sequence_SP> msa;   // --- Sequence_SP is just a shorter name for std::shared_ptr<Sequence>
  core::data::io::read_clustalw_file(argv[1],msa);

  core::data::basic::Array2D<core::index4 > m(22,22);

  core::index2 id_i = 0, id_j = 0;
  for(core::index4 i=1;i<msa[0]->length();++i) {
    for(core::index4 j=0;j<i;++j) {
      m.clear(0);
      for(core::index2 k=0;k<msa.size();++k) {
        if ((id_i = msa[k]->get_monomer(i).id) > 19) continue;
        if ((id_j = msa[k]->get_monomer(j).id) > 19) continue;
        m.get(id_i,id_j)++;
      }
      double crit_chi = core::calc::statistics::chi2_independence_test(m);
      double crit_gk  = core::calc::statistics::goodman_kruskal_rank_correlation(m);
      std::cout << i << " " << j <<" "<< crit_chi << " " << crit_gk << "\n";
    }
  }
}

