#include <cstdio>
#include <ctime>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <utility>

static const int MAX_NAME_LENGTH = 50;
static const int MAX_GAPS = 50;
static const int MAX_TARGETS = 1040;

int max_gap_id = 0;
int max_protein_id = 0;
std::string proteins[MAX_TARGETS];
std::string proteins_hitnames[MAX_TARGETS];
std::map<std::string, int> protein_mapping;

std::vector<std::pair<int, float> > results[MAX_TARGETS][MAX_GAPS];

bool cmpscore(std::pair<int, float> i, std::pair<int, float> j) {
	return i.second > j.second;
}
std::vector<std::string> & split(const std::string &s, const char delim,
		std::vector<std::string> &tokens) {

	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim))
		tokens.push_back(item);

	return tokens;
}

void read_mapping(const std::string fname) {

	std::ifstream infile;
	infile.open(fname);
	std::string line;
	char name[MAX_NAME_LENGTH];
	int template_id;
	while (std::getline(infile, line)) {
		sscanf(line.c_str(), "%d %s", &template_id, name);
		if (template_id > max_protein_id)
			max_protein_id = template_id;
		std::string s(name);
		proteins[template_id] = s;
		std::vector<std::string> tokens;
		split(s, '.', tokens);
		proteins_hitnames[template_id] = tokens[0];
		if (protein_mapping.find(s) == protein_mapping.end())
			protein_mapping[s] = template_id;
	}
	std::cerr << protein_mapping.size() << " targets mapped." << std::endl;
}

std::string to_string(std::pair<int, float> r) {

	std::ostringstream ss;
	ss << "(" << r.first << "," << r.second << ")";
	return ss.str();
}

int best_hit_rank(int my_id,
		std::vector<std::pair<int, float> > sorted_results) {

	int id = 0;
	const char* my_name = proteins_hitnames[my_id].c_str();
	for (const std::pair<int, float>&r : sorted_results) {
		if ((my_id != id)
				&& (strcmp(proteins_hitnames[r.first].c_str(), my_name) == 0))
			return id;
		else
			id++;
	}
	return id;
}

void read_file(const std::string fname, const int my_id) {

	std::ifstream infile;
	infile.open(fname);
	std::string line;
	float score;
	int template_id, gap_id;
	int n_targets = -1;
	while (std::getline(infile, line)) {
		sscanf(line.c_str(), "%d %*s %d %*f %*f %f", &template_id, &gap_id,
				&score);
		if (gap_id > max_gap_id)
			max_gap_id = gap_id;

		results[my_id][gap_id].push_back(std::make_pair(template_id, score));
//		std::cerr << "pushing " << to_string(results[my_id][gap_id].back())
//				<< " into " << my_id << " " << gap_id << std::endl;
	}

	for (int ig = 0; ig <= max_gap_id; ig++)
		std::sort(results[my_id][ig].begin(), results[my_id][ig].end(),
				cmpscore);

//	for (const std::pair<int, float>&r : results[my_id][0])
//		std::cout << to_string(r) << std::endl;
//	std::cout << std::endl;
//	for (const std::pair<int, float>&r : results[my_id][1])
//		std::cout << to_string(r) << std::endl;

	for (int ig = 0; ig <= max_gap_id; ig++) {
//		std::cout << std::endl;
//		for (int i = 0; i < 5; i++)
//			std::cout << to_string(results[my_id][ig][i]) << std::endl;
		std::cout << best_hit_rank(0, results[my_id][ig]) << std::endl;
	}
}

int main(int cnt, char* argv[]) {

	read_mapping(argv[1]);
	read_file(argv[3], atoi(argv[2]));
}
