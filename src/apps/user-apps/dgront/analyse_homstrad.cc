#include <iostream>
#include <memory>
#include <vector>

#include <apps/user-apps/dgront/HomstradEntry.hh>


#include <utils/Logger.hh>

using namespace core::data::sequence;
using namespace core::data::structural;
using namespace core::alignment;
using namespace core::data::io;

utils::Logger l("analyse_homstrad");

int main(const int argc, const char* argv[]) {

  using namespace core::data::io;

  apps::user_apps::dgront::HomstradEntry e(argv[1]);

//  e.load_structures();
//  std::vector<Residue_SP> q, t;
//  std::vector<PdbAtom> qv, tv;
//
//  e.aligned_coordinates(3, 0, q, t);
//  for (const Residue_SP resi : q) qv.push_back(*resi->find_atom(" CA "));
//  for (const Residue_SP resi : t) tv.push_back(*resi->find_atom(" CA "));
//  core::calc::structural::transformations::Crmsd<std::vector<PdbAtom>, std::vector<PdbAtom>> rms;
//  std::cerr <<int(3)<<" "<<int(0)<<" "<< e.get_key(3) << " " << e.get_key(0) << " " << rms.crmsd(qv, tv, qv.size(), true) << "\n";
//  q[0]->owner()->id('B');
//
//  for(auto ri:q) {
//    for(auto ai: *ri) {
//      rms.apply(*ai);
//      std::cout << ai->to_pdb_line()<<"\n";
//    }
//  }
//  for (auto ri:t)
//    for (auto ai: *ri)
//      std::cout << ai->to_pdb_line() << "\n";

}

