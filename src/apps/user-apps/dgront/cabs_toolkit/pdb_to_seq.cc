#include <core/data/io/Pdb.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/io/seq_io.hh>

utils::Logger logger("pdb_to_seq");

const std::string traf_to_pdb_info = R"(
Reads a PDB file and writes its sequence in the SEQ format

USAGE:
    pdb_to_seq protein.pdb
)";


int main(int argc, const char *argv[]) {

  using namespace core::data::io;
  using namespace core::data::structural;

  if (argc < 2) {
    std::cerr << traf_to_pdb_info;
    return 0;
  }

  Pdb reader(argv[1], is_ca, keep_all, true);
  Structure_SP strctr = reader.create_structure(0);
  for(auto c:*strctr) {
    std::cout << create_seq_string((*c->create_sequence()));
  }
}