#include <core/data/io/Pdb.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/io/fasta_io.hh>

utils::Logger logger("traf_to_pdb");

const std::string traf_to_pdb_info = R"(
Reads a TRAF file produced by CABS and writes a trajectory in the PDB format.

USAGE:
    traf_to_pdb sequence.fasta TRAF
)";

bool read_frame(std::ifstream &in, std::vector<core::data::structural::PdbAtom_SP> &frame) {
  int i_frame, n_res;
  double en1, en2, en3;
  int x, y, z;

  try {
    in >> i_frame >> n_res >> en1 >> en2 >> en3;
    if(n_res!=frame.size()+2)
      return false;
    in >> x >> y >> z;
    for (int i = 0; i < n_res - 2; ++i) {
      in >> x >> y >> z;
      frame[i]->x = x * 0.61;
      frame[i]->y = y * 0.61;
      frame[i]->z = z * 0.61;
    }
    in >> x >> y >> z;
  } catch (...) {
    return false;
  }

  return true;
}

int main(int argc, const char *argv[]) {

  using namespace core::data::io;
  using namespace core::data::structural;

  if (argc < 3) {
    std::cerr << traf_to_pdb_info;
    return 0;
  }

  Pdb reader(argv[1], is_ca);
  Structure_SP strctr = reader.create_structure(0);
  std::vector<PdbAtom_SP> frame;
  for(auto it_atom=strctr->first_atom();it_atom!=strctr->last_atom();++it_atom)
    frame.push_back(*it_atom);

  std::ifstream inp(argv[2]);
  int i_frame = 0;
  while (read_frame(inp, frame)) {
    std::cout << "MODEL     " << (++i_frame) << "\n";
    for (auto a: frame) std::cout << a->to_pdb_line() << "\n";
    std::cout << "ENDMDL\n";
  }
}