#include <core/data/io/Pdb.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/io/seq_io.hh>

utils::Logger logger("pdb_to_fchains");

const std::string traf_to_pdb_info = R"(
Reads a PDB file and writes FCHAINS file

USAGE:
    pdb_to_fchains protein.pdb [n_chains]
)";


struct ipos {
  int ix, iy, iz;
  
};

int main(int argc, const char *argv[]) {

  using namespace core::data::io;
  using namespace core::data::structural;

  if (argc < 2) {
    std::cerr << traf_to_pdb_info;
    return 0;
  }
  int n_chains = (argc>2) ? atoi(argv[2]) : 1;

  Pdb reader(argv[1], is_ca, keep_all, true);
  Structure_SP strctr = reader.create_structure(0);
  auto ait=strctr->first_const_atom();
  std::vector<ipos> fchains;
  while (ait != strctr->last_const_atom()) {
    Vec3 pos = (**ait);
    pos /= 0.61;
    fchains.push_back({(int) std::round(pos.x), (int) std::round(pos.y), (int) std::round(pos.z)});
    ++ait;
  }
  int dx = 2 * fchains[0].ix - fchains[1].ix;
  int dy = (2 * fchains[0].iy - fchains[1].iy);
  int dz = 2 * fchains[0].iz - fchains[1].iz;
  fchains.push_back({dx, dy, dz});
  std::rotate(fchains.rbegin(), fchains.rbegin() + 1, fchains.rend());
  int k = fchains.size() - 1;
  dx = 2 * fchains[k].ix - fchains[k - 1].ix;
  dy = (2 * fchains[k].iy - fchains[k - 1].iy);
  dz = 2 * fchains[k].iz - fchains[k - 1].iz;
  fchains.push_back({dx, dy, dz});
  //below part of code tries to keep distance between atoms between 29 and 49; it moves all following atoms closer if the distance is to big and then checks if it's not to small
  for (int i=0;i<fchains.size()-1;i++){
    auto v1 = fchains[i];
    auto v2 = fchains[i+1];
    if ((v2.ix-v1.ix)*(v2.ix-v1.ix)+(v2.iy-v1.iy)*(v2.iy-v1.iy)+(v2.iz-v1.iz)*(v2.iz-v1.iz)>49)
        for (int j=i+1;j<fchains.size();j++){
            if (v2.iy-v1.iy >0 ) fchains[j].iy-=1;
            else fchains[j].iy+=1;
            if (v2.iz-v1.iz >0 )fchains[j].iz-=1;
            else fchains[j].iz+=1;
        }
    v2 = fchains[i+1];
    if ((v2.ix-v1.ix)*(v2.ix-v1.ix)+(v2.iy-v1.iy)*(v2.iy-v1.iy)+(v2.iz-v1.iz)*(v2.iz-v1.iz)<29)
        for (int j=i+1;j<fchains.size();j++){
            if (v2.iz-v1.iz >0 )fchains[j].iz+=1;
            else fchains[j].iz-=1;
        }
    v2 = fchains[i+1];

    if ((v2.ix-v1.ix)*(v2.ix-v1.ix)+(v2.iy-v1.iy)*(v2.iy-v1.iy)+(v2.iz-v1.iz)*(v2.iz-v1.iz)>49 || (v2.ix-v1.ix)*(v2.ix-v1.ix)+(v2.iy-v1.iy)*(v2.iy-v1.iy)+(v2.iz-v1.iz)*(v2.iz-v1.iz)<29)

        std::cerr<<i<<" "<<i+1<<" "<<(v2.ix-v1.ix)*(v2.ix-v1.ix)+(v2.iy-v1.iy)*(v2.iy-v1.iy)+(v2.iz-v1.iz)*(v2.iz-v1.iz)<<" "<<v1.ix<<" "<<v1.iy<<" "<<v1.iz<<"\n";
  }
 //end of checking
  for (int i = 0;i < n_chains; ++i) {
    std::cout << " "<<fchains.size() << "\n";
    for (ipos p: fchains)
      std::cout << utils::string_format("%6d%5d%5d\n", p.ix, p.iy, p.iz);
  }
  
}

