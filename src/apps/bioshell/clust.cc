#include <algorithm>
#include <vector>
#include <iostream>
#include <core/index.hh>

#include <core/calc/clustering/DistanceByValues.hh>
#include <core/calc/clustering/Medoid.hh>
#include <core/calc/clustering/HierarchicalCluster.hh>
#include <core/calc/clustering/HierarchicalClustering.hh>
#include <core/calc/clustering/analyse_clustering.hh>
#include <core/algorithms/trees/algorithms.hh>
#include <core/algorithms/trees/trees_io.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/Logger.hh>
#include <utils/io_utils.hh>

#include <utils/Profiler.hh>
#include <utils/ProfilingManager.hh>

using namespace core::calc::clustering;

static const std::string empty("");

const std::string clust_info = R"(
clust program performs hierarchical clustering of objects based on a matrix of their mutual distances.

The distance matrix is a mandatory parameter of this program. clust can do single_link, average_link,
complete_link, median_link and Ward's method clustering.
)";

utils::options::Option n_data("-n", "-clustering:n_data", "the number of data to cluster");
utils::options::Option max_dist("-dmax", "-clustering:max_distance", "distance cutoff used to stop the clustering process");
utils::options::Option unknown_dist("-unk_dist", "-clustering:missing_distance",
  "value used for unknown distances, i.e. to fill the missing elements of the distance matrix");
utils::options::Option output_tree("-ot", "-clustering:out:tree", "write a clustering tree into a file");
utils::options::Option output_tree_data("-otd", "-clustering:out:tree_data",
    "write a clustering tree along with elements of each cluster into a file");
utils::options::Option output_tree_medoids("-otm", "-clustering:out:tree_medoid",
    "write a clustering tree with medoids of each cluster into a file");
utils::options::Option input_tree("-it", "-clustering:in:tree", "read a clustering tree from a file");
utils::options::Option output_clusters("-oc", "-clustering:out:clusters",
    "write clusters into separate files; see other options to set distance cutoff or ratio");
utils::options::Option output_fraction("-clustering:out:fraction", "-clustering:out:fraction",
    "clusters will be written for this particular stage of clustering; implicitly switches -clustering:out:clusters on");
utils::options::Option output_distance("-clustering:out:distance", "-clustering:out:distance",
    "clusters will be written for this particular merging distance value; implicitly switches -clustering:out:clusters on");
utils::options::Option output_min_size("-clustering:out:min_size", "-clustering:out:min_size",
    "minimum cluster size to be written");
utils::options::Option output_tree_xml("-clustering:out:xml", "-clustering:out:xml",
    "writes the clustering tree in the XML format that may be visualized by TreeVis");
utils::options::Option output_tree_size_growth("-clustering:out:size_growth", "-clustering:out:size_growth",
    "writes the histogram of clusters' size at each step");
utils::options::Option output_tree_size_growth_n("-clustering:out:size_growth:step",
    "-clustering:out:size_growth::step", "clusters' growth statistics will be written every n-steps");

utils::options::Option cluster_single("-single", "-clustering::single",
    "use single-link clustering strategy [default]");
utils::options::Option cluster_complete("-complete", "-clustering:complete", "use complete-link clustering strategy");
utils::options::Option cluster_average("-average", "-clustering:average", "use average-link clustering strategy");
utils::options::Option cluster_centroid("-centroid", "-clustering:centroid", "use centroid-link clustering strategy");
utils::options::Option cluster_median("-median", "-clustering:median", "use median-link clustering strategy");
utils::options::Option cluster_ward("-ward", "-clustering:ward",
    "use Ward's method to merge clusters (minimum variance approach)");

int main(int argc, const char* argv[]) {

  using utils::options::option_value;

  utils::Logger l("clust");
  std::shared_ptr<utils::Profiler> profiler = utils::ProfilingManager::get().get_profiler("clust");
  profiler->start();

  utils::options::OptionParser & cmd = utils::options::OptionParser::get("clust");
  cmd.register_option(utils::options::help, utils::options::markdown_help, utils::options::verbose,utils::options::show_examples); // basic options
  cmd.register_option(utils::options::input_file, input_tree);			// input options
  cmd.register_option(n_data, max_dist, unknown_dist);					// clustering params
  cmd.must_have_value(n_data,true);
  cmd.register_option(output_tree, output_tree_data, output_tree_medoids, output_clusters);	// output options
  cmd.register_option(output_fraction, output_distance, output_min_size, output_tree_xml);	// output options
  cmd.register_option(output_tree_size_growth, output_tree_size_growth_n);
  cmd.register_option(cluster_single, cluster_complete, cluster_average, cluster_ward, cluster_centroid,
      cluster_median);

  cmd.add_examples_group("Example usage of clust:");
  cmd.add_example(0,"Read in a file with pairwise distances and write clusters for distance cutoff = 0.4",
      "clust -i=tm_dist -n=140 -clustering:out:distance=0.4");
  cmd.program_info(clust_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  std::string infname = utils::options::option_value<std::string>(utils::options::input_file);
  std::shared_ptr<DistanceByValues<float> > d;
  l << utils::LogLevel::INFO << "Reading distances from " << infname << "\n";
  float missing_distance = option_value<float>(max_dist, std::numeric_limits<float>::max());
  float unknown_distance = option_value<float>(unknown_dist, std::numeric_limits<float>::max());
  std::shared_ptr<HierarchicalClustering<float, std::string> > hac;

  if (!n_data.was_used()) {
      l << utils::LogLevel::CRITICAL
          << "You must specify the number of items to be clustered by -clustering:n_data option !!!" << "\n";
      return 0;
  }
  size_t n_points = option_value<size_t>(n_data);
      
  if (input_tree.was_used()) {
    std::ifstream in;
    utils::in_stream(option_value<std::string>(input_tree),in);
    hac = HierarchicalClustering<float, std::string>::from_stream(in, empty);
  } else {
    std::shared_ptr<ClusterMergingRule<float>> strategy = std::static_pointer_cast<ClusterMergingRule<float>>(
        std::make_shared<SingleLink<float>>());
    if (cluster_average.was_used())
      strategy = std::static_pointer_cast<ClusterMergingRule<float>>(std::make_shared<AverageLink<float>>());
    if (cluster_complete.was_used())
      strategy = std::static_pointer_cast<ClusterMergingRule<float>>(std::make_shared<CompleteLink<float>>());
    if (cluster_median.was_used())
      strategy = std::static_pointer_cast<ClusterMergingRule<float>>(std::make_shared<MedianLink<float>>());
    if (cluster_centroid.was_used())
      strategy = std::static_pointer_cast<ClusterMergingRule<float>>(std::make_shared<CentroidLink<float>>());
    if (cluster_ward.was_used())
      strategy = std::static_pointer_cast<ClusterMergingRule<float>>(std::make_shared<WardsMethod<float>>());

    n_points = option_value<size_t>(n_data);
    d = std::make_shared<DistanceByValues<float> >(infname, n_points, unknown_distance, missing_distance);
    hac = std::make_shared<HierarchicalClustering<float, std::string> >(d->labels(), empty);
    hac->run_clustering(*d, *strategy);
  }

  if (output_tree.was_used()) hac->write_merging_steps(*utils::out_stream(option_value<std::string>(output_tree)));
  if (output_tree_data.was_used())
    hac->write_merging_steps_data(*utils::out_stream(option_value<std::string>(output_tree_data)));
  if (output_tree_medoids.was_used()) {
    hac->write_merging_steps_medoids(*utils::out_stream(option_value<std::string>(output_tree_medoids)), *d);
  }

  if (output_tree_xml.was_used()) {
    std::ofstream of(option_value<std::string>(output_tree_xml));
    std::shared_ptr<BinaryTreeNode<std::string> > ptr = std::static_pointer_cast<BinaryTreeNode<std::string> >(
        hac->get_root());
    core::algorithms::trees::XMLFormatters<std::shared_ptr<BinaryTreeNode<std::string>>>xml(of);
    core::algorithms::trees::write_tree(ptr, xml.start, xml.leaf, xml.stop);
  }

  if (output_tree_size_growth.was_used()) {
    core::index2 n = (output_tree_size_growth_n.was_used()) ? option_value<core::index2>(output_tree_size_growth_n) : 1;
    growing_clusters_size<float, std::string>(hac, *utils::out_stream(option_value<std::string>(output_tree_size_growth)),
        n);
  }

  const bool write_clusters = output_clusters.was_used() || output_fraction.was_used() || output_distance.was_used();
  if (write_clusters) {
    // ---------- We must read the distance again since the previous content of this matrix had changed during the clustering process
    DistanceByValues<float> d2(infname, n_points, unknown_distance, missing_distance);
    // ---------- The distance cutoff value for which we retrieve clusters
    double dist = (output_distance.was_used()) ? option_value<double>(output_distance) : missing_distance;
    if (output_fraction.was_used()) {
      const double f = option_value<double>(output_fraction);
      const core::index4 which_step = (core::index4) ((hac->count_steps() - hac->count_objects()) * f
          + hac->count_objects());
      dist = hac->clustering_step(which_step)->merging_distance;
    }
    // ---------- The minimum size of a cluster to be written
    const core::index2 min_size = (output_min_size.was_used()) ? option_value<core::index2>(output_min_size) : 1;
    // ---------- and finally the clusters themselves
    auto clusters = hac->get_clusters(dist, min_size);
    l << utils::LogLevel::INFO << "Writing " << clusters.size() << " clusters for clustering distance: " << dist
        << ", min size:" << min_size << "\n";
    std::vector<std::string> elements; // --- elements (data) extracted from cluster members
    std::vector<std::shared_ptr<BinaryTreeNode<std::string> > >nodes; // --- nodes of a cluster - ie. its members
    for (const std::shared_ptr<HierarchicalCluster<float, std::string> > & c : clusters) {
      elements.clear();
      collect_leaf_elements(std::static_pointer_cast<BinaryTreeNode<std::string>>(c), elements);
      nodes.clear();
      collect_leaf_nodes(std::static_pointer_cast<BinaryTreeNode<std::string>>(c), nodes);
      if (elements.size() < min_size) continue;
      std::string fname = utils::string_format("c%d_%d", c->id, elements.size());
      std::ofstream of(fname);
      auto medoid = medoid_by_average_distance<float, std::string, DistanceByValues<float> >(c, d2);
      of << "# medoid distance max, average, stdev: " << medoid.max_distance << " " << medoid.average_distance << " "
          << medoid.stdev_distance << "\n";
      of << "# medoid element " << medoid.medoid << "\n";
      for(core::index4 i=0;i<elements.size();++i) {
        if (medoid.id == nodes[i]->id)
          of << elements[i] << " " << 0 << "\n";
        else
          of << elements[i] << " " << d2(medoid.id, nodes[i]->id) << "\n";
      }
      of.close();
    }
  }
  profiler->stop();
  utils::ProfilingManager::get().show(std::cerr);
}
