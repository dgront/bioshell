#include <apps/bioshell/bbf_utils.cc>

int main(const int argc, const char *argv[]) {

    // |||||||||||||||||||||||||||||| HOW TO USE ||||||||||||||||||||||||||||||
    // || 	./bbf -bbf:db_dist=<path_to_file> -bbf:db_full=<path_to_file> 	 ||
    // ||			-bbf:input=<path_to_file> -bbf:output=<path_to_file>	 ||
    // ||		 [-bbf:number_of_fragments=100] [-bbf:tm1=1] [-bbf:tm2=1]	 ||
    // ||		 [-bbf:nothing_stay/-bbf:everything_stay] [-bbf:only_ca]	 ||
    // |||||||||||||||||||||||||||||| HOW TO USE ||||||||||||||||||||||||||||||

    // OPTIONS PARSING
    Option db_dist("-bbf:db_dist", "-bbf:db_dist", "data base file with distances");
    Option db_full("-bbf:db_full", "-bbf:db_full", "data base file with full fragments");
    Option number_of_fragments("-bbf:number_of_fragments", "-bbf:number_of_fragments", "number of fragments");
    // is any better way to do this?
    Option input("-bbf:input", "-bbf:input", "input file in pdb format (may be gzipped)");
    // is any better way to do this?
    Option output("-bbf:output", "-bbf:output", "output file in pdb format");
    Option tm1("-bbf:tm1", "-bbf:tm1", "first trimming mode argument");
    Option tm2("-bbf:tm2", "-bbf:tm2", "second trimming mode argument");
    // can't be used with everything_stay, only ca stay by default
    Option nothing_stay("-bbf:nothing_stay", "-bbf:nothing_stay", "stay nothing from query structure");
    // can't be used with nothing_stay, only ca stay by default
    Option everything_stay("-bbf:everything_stay", "-bbf:everything_stay", "stay everything from query structure");
    // so, whole bb participate by default
    Option only_ca("-bbf:only_ca", "-bbf:only_ca", "when selected only ca atoms participate in fragments alinging");

    OptionParser &cmd = OptionParser::get();

    cmd.register_option(help);
    cmd.register_option(verbose, db_path);
    cmd.register_option(db_dist, db_full, number_of_fragments);
    cmd.register_option(input, output);
    cmd.register_option(tm1, tm2, nothing_stay, everything_stay, only_ca);

    if (!cmd.parse_cmdline(argc, argv)) return 1;

    std::string db_dist_file = option_value<std::string>(db_dist);
    std::string db_full_file = option_value<std::string>(db_full);
    int n_of_fragments = 100;
    if (number_of_fragments.was_used()) {
        n_of_fragments = option_value<int>(number_of_fragments);
    }
    std::string input_file = option_value<std::string>(input);
    std::string output_file = option_value<std::string>(output);
    int t1 = 1;
    if (tm1.was_used()) {
        t1 = option_value<int>(tm1);
    }
    int t2 = 1;
    if (tm2.was_used()) {
        t2 = option_value<int>(tm2);
    }
    int what_stay = 0;
    if (nothing_stay.was_used() && !everything_stay.was_used()) {
        what_stay = -1;
    }
    else if (!nothing_stay.was_used() && everything_stay.was_used()) {
        what_stay = 1;
    }
    else if (nothing_stay.was_used() && everything_stay.was_used()) {
        logger << utils::LogLevel::CRITICAL << "Nothing_stay can't be combined with everything_stay";
        return -1;
    }
    // DATABASE LOADING
    std::shared_ptr<DatabaseKeeper> database = std::make_shared<DatabaseKeeper>(db_dist_file, db_full_file);
    if (t1 + t2 >= (*database).get_length() || t1 + t2 < 0) {
        logger << utils::LogLevel::CRITICAL << "Invalid trimming_mode arguments\n";
        return -1;
    }
    logger << utils::LogLevel::INFO << "DATABASE loaded successfully\n";
    try {
        // QUERY LOADING
        // is_bb filter may be changed in future (especially with fixing-missing-1/2/3-residues feature)
        core::data::io::Pdb reader(input_file,
                                   core::data::io::all_true(core::data::io::is_standard_atom, core::data::io::is_bb),core::data::io::keep_all,
                                   true); // header disappear...
        Structure_SP query = reader.create_structure(0);
        if (!bb_is_complete(query)) {
            logger << utils::LogLevel::CRITICAL << "Query is invalid\n";
            return -1;
        }
        logger << utils::LogLevel::INFO << "QUERY loaded successfully\n";
        // BUILDING
        build_whole_structure(database, query, n_of_fragments, t1, t2, what_stay, only_ca.was_used());
        logger << utils::LogLevel::INFO << "REBUILD success!\n";
        // WRITING OUTPUT STRUCTURE TO FILE
        std::ofstream myfile;
        myfile.open(output_file);
        core::data::io::write_pdb(query, myfile);
        myfile.close();
        logger << utils::LogLevel::FILE << "File saved\n";
        logger << utils::LogLevel::FINEST << "Done\n";
    }
    catch (...) {
        logger << utils::LogLevel::CRITICAL << "Query is invalid\n";
        return -1;
    }
    return 0;
}
