#include <cstdio>
#include <ctime>
#include <iostream>
#include <memory>

#include <core/BioShellEnvironment.hh>

#include <core/data/io/Pdb.hh>
#include <core/data/io/pir_io.hh>
#include <core/data/io/fasta_io.hh>
#include <core/data/io/alignment_io.hh>
#include <core/data/io/clustalw_io.hh>
#include <core/data/sequence/SequenceProfile.hh>

#include <core/alignment/Target.hh>
#include <core/alignment/Template.hh>
#include <core/alignment/on_alignment_computations.hh>
#include <core/alignment/PairwiseAlignment.hh>
#include <core/alignment/PairwiseSequenceAlignment.hh>
#include <core/alignment/NWAligner.hh>
#include <core/alignment/AlignmentRow.hh>
#include <core/alignment/AlignmentBlock.hh>
#include <core/alignment/scoring/SimilarityMatrix.hh>
#include <core/alignment/scoring/Picasso3.hh>
#include <core/alignment/scoring/AlignmentConstraints.hh>
#include <core/alignment/scoring/AlignmentScoreBase.hh>
#include <core/alignment/scoring/SecondarySimilarity.hh>
#include <core/alignment/scoring/CombinedScore.hh>
#include <core/alignment/scoring/SimilarityMatrixScore.hh>
#include <core/alignment/scoring/NcbiSimilarityMatrixFactory.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/align_options.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/calc_options.hh>
#include <utils/Logger.hh>
#include <utils/io_utils.hh>

utils::Logger seq_align_logger("seq_align");

short int gap_open_fasta = -10;
short int gap_extn_fasta = -2;
double gap_open_profile = -1.3;
double gap_extn_profile = -0.1;
double gap_open_t1d = -1.5;
double gap_extn_t1d = -0.3;

const std::string alignc_info = R"(
      Calculates an alignment between two given sequences or sequence profiles. The program can also
convert between sequence alignment formats. Finally, it may be used to prepare .target and .tmplt files
used as an input by BioShell threading programs.)";

using namespace core::data::sequence;

void align_all_pairs(const std::vector<Sequence_SP> & input_sequences) {

  using namespace core::alignment::scoring;

  short int gap_open = utils::options::option_value("-align::gap_open",-10);
  short int gap_extn = utils::options::option_value("-align::gap_extend",-2);
  NcbiSimilarityMatrix_SP sim_m =
    NcbiSimilarityMatrixFactory::get().get_matrix(utils::options::option_value<std::string>("-align::matrix"));

  core::index4 max_len = (*std::max_element(input_sequences.begin(), input_sequences.end(),
    [](Sequence_SP si, Sequence_SP sj) { return si->length() < sj->length(); }))->length();
  seq_align_logger << utils::LogLevel::FILE << "Longest sequence: " << int(max_len) << "\n";

  // --- create aligner object
  core::alignment::NWAligner<short, SimilarityMatrixScore<short>> aligner(max_len);

}

int main(const int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace core::alignment;
  using namespace utils::options; // --- for option_value<>()
  using namespace core::alignment::scoring;

  utils::options::OptionParser & cmd = OptionParser::get("alignc");
  cmd.register_option(utils::options::help, utils::options::markdown_help, verbose, utils::options::db_path);
  cmd.register_option(output_fasta, output_blocks, output_seq_width,output_gaps,output_alipath);
  cmd.register_option(query_infile, template_infile, query_fasta, template_fasta);

  // ---------- Alignment-related options
  cmd.register_option(align_global, align_local, align_semiglobal, gap_extend, gap_open, substitution_matrix);
  cmd.register_option(query_fasta, query_chk, query_fasta_string);
  cmd.register_option(template_fasta, template_chk, template_fasta_string);

  core::index2 id = cmd.add_examples_group("Calculate sequence alignment:");
  cmd.add_example(id,"Align a target - template pair (aka threading 1D); template and target files may be prepared with seqc command",
    "alignc -in:template=2kmk.tmplt -in::query=2aza.query");
  cmd.add_example(id,"Align sequences in FASTA format (two FASTA files): ",
    "alignc -in:template:fasta:string=GYHFCGGSLINSQWVVSAAHCY -in::query:fasta:string=QRHLCGGSIIGNWILTAAHCF -out::fasta");
  cmd.program_info(alignc_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // ********** DATA PLACEHOLDERS **********

  // --- the query sequence - FASTA
  std::vector<std::shared_ptr<Sequence>> query_sequences;  ///< Input query sequences read from a FASTA or a PIR file
  if(cmd.was_used(query_fasta)) read_fasta_file(option_value<std::string>(query_fasta), query_sequences);
  // --- the query sequence - PIR

  // --- container for the sequence database; load FASTA
  std::vector<std::shared_ptr<Sequence>> db_sequences; ///< Input template sequences read from a FASTA or a PIR file
  if(cmd.was_used(template_fasta)) read_fasta_file(option_value<std::string>(template_fasta), db_sequences);
  // --- templates in PIR

  if (db_sequences.size() == 0)
    align_all_pairs(query_sequences);

  // ********** OUTPUT SECTION **********
//  core::index2 width = (output_seq_width.was_used()) ? get_option<core::index2>(output_seq_width) : 80;

//  // ---------- Write blocks
//  if (output_blocks.was_used()) {
//    for (const PairwiseAlignment & ali : alignments) {
//      const auto blocks = ali.aligned_blocks();
//      for (const AlignmentBlock & b : blocks) std::cout << b<<"\n";
//      std::cout << "\n";
//    }
//  }
//
//  // ---------- Write gaps
//  if (output_gaps.was_used()) {
//    /// TODO implement "Write gaps" option
//  }
//
//  // ---------- Write path
//  if (output_alipath.was_used()) {
//    std::shared_ptr<std::ostream> out = utils::out_stream(get_option<std::string>(output_alipath));
//    for (const PairwiseAlignment & a : alignments) *out << a.to_path() << "\n";
//  }
//
//  // ---------- Write FASTA
//  if (output_fasta.was_used()) {
//    std::shared_ptr<std::ostream> out = utils::out_stream(get_option<std::string>(output_fasta,"stdout"));
//    int i=-1;
//    for (const PairwiseAlignment & ali : alignments) {
//      const Sequence_SP s1 = input_sequences[++i]->create_ungapped_sequence();
//      const Sequence_SP s2 = input_sequences[++i]->create_ungapped_sequence();
//      (*out) << core::data::io::create_fasta_string(ali,*s1,*s2);
//    }
//  }
}

