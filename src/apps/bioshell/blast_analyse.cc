#include <iostream>
#include <iterator>
#include <regex>

#include <core/index.hh>
#include <core/data/io/Hsp.hh>


#include <utils/io_utils.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>

#include <utils/Logger.hh>
#include <core/data/io/fasta_io.hh>

utils::Logger l("blast_analyse");
const std::string blast_analyse_info = R"(
      Reads output file produced by (psi)blast program, extracts hits and formats nice summary output. Basic filtering is also
available.)";

using namespace core::data::io;

void emplace_hit(std::vector<Hsp> &hits, const std::smatch & data){

  using namespace core;

  const std::regex just_scores_regex("Score =\\s+?(\\d+).+Expect = (.+?), Method: (?:.|\\n)+?Identities =\\s+?(\\d+).+Positives =\\s+(\\d+).+Gaps =\\s+?(\\d+)(?:.|\\n)+?%\\)");
  const std::regex gi_species_regex("\\[(.+?)\\]");
  const std::regex sw_species_regex("Tax\\s*?=\\s*?([\\n\\w ]*?)\\s*?RepID");
  const std::regex alignment_regex("Query\\s+?(\\d+?)\\s+?([-_[:alnum:]]+?)\\s+?(\\d+?)\\n.+?\\nSbjct\\s+?(\\d+?)\\s+?([-_[:alnum:]]+?)\\s+?(\\d+?)\\n");

  index2 length;
  std::string description;
  std::string species;

  index2 score;
  double expect;
  index2 identities;
  index2 positives;
  index2 gaps;
  index2 query_start;
  index2 query_end;
  index2 sbjct_start;
  index2 sbjct_end;
  std::string query;
  std::string sbjct;

  core::index1 hit_index = 0;
  description = data[1];
  length = utils::from_string<index2>(data[3]);
  std::smatch species_match;
  std::regex_search(description.cbegin(), description.cend(), species_match, gi_species_regex);
  if (!species_match.empty()) species = species_match[1];
  else {
    std::regex_search(description.cbegin(), description.cend(), species_match, sw_species_regex);
    if (!species_match.empty()) species = species_match[1];
    std::replace( species.begin(), species.end(), '\n', ' ');
  }

  score = utils::from_string<index2>(data[4]);
  expect = utils::from_string<double>(data[5]);
  identities = utils::from_string<index2>(data[6]);
  positives = utils::from_string<index2>(data[7]);
  gaps = utils::from_string<index2>(data[8]);

  std::string ali = data[9];
  auto pos2 = ali.cbegin();
  auto end2 = ali.cend();
  std::smatch alignment_block_match,just_scores_match;
  query = "";
  sbjct = "";
  query_end = 0;
  sbjct_end = 0;
  query_start = 65534;
  sbjct_start = 65534;
  for (; std::regex_search(pos2, end2, alignment_block_match, alignment_regex); pos2 = alignment_block_match.suffix().first - 1) {

    if(std::regex_search(pos2, end2, just_scores_match, just_scores_regex)) {
      if(just_scores_match.position() < alignment_block_match.position()) {

        score = utils::from_string<index2>(data[1]);
        expect = utils::from_string<double>(data[2]);
        identities = utils::from_string<index2>(data[3]);
        positives = utils::from_string<index2>(data[4]);
        gaps = utils::from_string<index2>(data[5]);

        query = "";
        sbjct = "";
        query_start = 65534;
        sbjct_start = 65534;
        ++hit_index;
      }
    }

    query_start =  std::min(query_start,utils::from_string<index2>(alignment_block_match[1]));
    sbjct_start =  std::min(sbjct_start,utils::from_string<index2>(alignment_block_match[4]));
    query_end =  std::max(query_end,utils::from_string<index2>(alignment_block_match[3]));
    sbjct_end =  std::max(sbjct_end,utils::from_string<index2>(alignment_block_match[6]));
    query += alignment_block_match[2];
    sbjct += alignment_block_match[5];
  }

  hits.emplace_back(length, description, species, score, expect, identities, positives, gaps, query_start,query_end,sbjct_start,sbjct_end,query,sbjct);

}


int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;

  utils::options::OptionParser &cmd = OptionParser::get("blast_analyse");
  cmd.register_option(utils::options::help, utils::options::markdown_help);
  cmd.register_option(verbose, db_path);

  // ---------- input options
  cmd.register_option(input_blast_output, input_blast_iteration, input_blast_last_iteration);

  // ---------- filtering options

  // ---------- output-related options
  cmd.register_option(output_blast_nhits, output_fasta, output_seq_width);

  cmd.program_info(blast_analyse_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // ********** INPUT SECTION **********
  if (!input_blast_output.was_used()) {
    l << utils::LogLevel::SEVERE << "\n";
    return 0;
  }
  const std::string data = utils::load_text_file(option_value<std::string>(input_blast_output));
  std::vector<size_t> chunk_markers;
  chunk_markers.push_back(0);
  size_t iter_pos = data.find("Results");
  std::cout<<"iter "<<iter_pos<<"\n";
  int len=0;
  while (iter_pos != std::string::npos) {
    len+=1;
    l << utils::LogLevel::FINE << "Buffer position: " << iter_pos << "\n";
    std::cout << "Buffer position: " << iter_pos << " ,len "<<len<<"\n";
    chunk_markers.push_back(iter_pos);
    iter_pos = data.find("Results", iter_pos+1);
  }
  chunk_markers.push_back(data.length());

  
  std::vector<Hsp> hits;
  core::index2 from = 0;
  core::index2 to = chunk_markers.size() - 2;
  std::cout<<"blast_last" <<option_value<core::index2>(input_blast_iteration)<<"\n";
 
  if (input_blast_iteration.was_used())
    from = to = option_value<core::index2>(input_blast_iteration, chunk_markers.size() - 2);
  if (input_blast_last_iteration.was_used())
    from = to = chunk_markers.size() - 2;

  std::cout << from<< " - " <<to<<"\n";
  for (core::index2 iter = from; iter <= to; ++iter) {
    l << utils::LogLevel::INFO << "processing iteration " << int(iter) << "\n";

  std::cout << "from "<< chunk_markers[iter] << " - " <<"to " << chunk_markers[iter+1]<<"\n";
    std::string sub_data = data.substr(chunk_markers[int(iter)], chunk_markers[int(iter+1)]);

    auto pos = sub_data.cbegin();
    auto end = sub_data.cend();
    std::smatch mm;

    const std::string hsp_regex_string = ">((.|\\n)+?)\\nLength=(\\d+)(?:.|\\n)+?Score =\\s+?(\\d+).+Expect = (.+?), Method: (?:.|\\n)+?Identities =\\s+?(\\d+).+Positives =\\s+(\\d+).+Gaps =\\s+?(\\d+)(?:.|\\n)+?%\\)((.|\\n)+?)>";
    std::regex rx(hsp_regex_string);
    for (; regex_search(pos, end, mm, rx); pos = mm.suffix().first - 1) {
      emplace_hit(hits, mm);
      if ((hits.size() % 1000 == 0) && (hits.size() > 0))
        l << utils::LogLevel::FINE << hits.size() << " hits processed\n";
    }
    l << utils::LogLevel::INFO << hits.size() << " hits loaded\n";
  }
  const std::string output_header = "         species                len score gaps  gap% ident ident%    evalue  qpos tpos sequence";
  std::cout << output_header << "\n";
  core::index4 nhits = (utils::options::output_blast_nhits.was_used()) ? utils::options::option_value<core::index4>(
    utils::options::output_blast_nhits) : hits.size();
  //for (core::index2 i = 0; i < nhits; ++i) std::cout << hits[i];

  if (output_fasta.was_used()) {
    std::shared_ptr<std::ostream> outf = utils::out_stream(option_value<std::string>(output_fasta));
    (*outf) << output_header << "\n";
    for (core::index2 i = 0; i < nhits; ++i) (*outf) << hits[i]<<"\n";
  }

  return 0;
}
