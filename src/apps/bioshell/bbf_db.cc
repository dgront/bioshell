#include <apps/bioshell/bbf_utils.cc>

int main(const int argc, const char *argv[]) {

    // |||||||||||||||||||||||||||||| HOW TO USE ||||||||||||||||||||||||||||||
    // || 	../bbf_db [-bbf_db:input_dir=<path>] -bbf_db:output_dir=<path>   ||
    // ||		   	  [-bbf_db:length_of_fragment=5]						 ||
    // ||					[-bbf_db:subset]								 ||
    // ||																	 ||
    // |||||||||||||||||||||||||||||| HOW TO USE ||||||||||||||||||||||||||||||

    // options parsing
    Option input_dir("-bbf_db:input_dir", "-bbf_db:input_dir",
                     "directory with input files in pdb format (may be gzipped)");    // is any better way to do this?
    Option output_dir("-bbf_db:output_dir", "-bbf_db:output_dir",
                      "output directory");                    // is any better way to do this?
    Option length_of_fragment("-bbf_db:length_of_fragment", "-bbf_db:length_of_fragment", "length of fragments");
    Option subset("-bbf_db:subset", "-bbf_db:subset", "file with adresses files to build database");

    OptionParser &cmd = OptionParser::get();

    cmd.register_option(help);
    cmd.register_option(verbose, db_path);
    cmd.register_option(input_dir, output_dir, length_of_fragment, subset);
    if (!cmd.parse_cmdline((const core::index2) argc, argv)) return 1;

    int fragment_len = 5;
    if (length_of_fragment.was_used()) {
        fragment_len = option_value<int>(length_of_fragment);
    }
    bool input_subset;
    std::string input;
    if (subset.was_used()) {
        input_subset = true;
        input = option_value<std::string>(subset);
    }
    else if (input_dir.was_used()) {
        input_subset = false;
        input = option_value<std::string>(input_dir);
    }
    else { return 1; }
    build_database(input, option_value<std::string>(output_dir), fragment_len, input_subset);
    return 0;
}
