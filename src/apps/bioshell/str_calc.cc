#include <cstdio>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <memory>
#include <algorithm>

#include <core/algorithms/predicates.hh>

#include <core/chemical/AtomicElement.hh>
#include <core/chemical/ChiAnglesDefinition.hh>

#include <core/data/io/Pdb.hh>
#include <core/data/io/DataTable.hh>
#include <core/data/io/load_decoys.hh>
#include <core/data/basic/Array2D.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/selectors/ResidueSegmentSelector.hh>

#include <core/calc/structural/angles.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/local_backbone_geometry.hh>
#include <core/calc/structural/TorsionBinType.hh>
#include <core/calc/structural/interactions/BackboneHBondInteraction.hh>
#include <core/calc/structural/ProteinArchitecture.hh>
#include <core/calc/structural/transformations/Crmsd.hh>

#include <core/protocols/PairwiseCrmsd.hh>
#include <core/protocols/PairwiseTMScore.hh>
#include <core/protocols/selection_protocols.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <utils/options/select_options.hh>
#include <utils/options/selector_from_cmdline.hh>
#include <utils/options/calc_options.hh>
#include <utils/Logger.hh>
#include <utils/io_utils.hh>
#include <core/data/structural/ResidueSegmentProvider.hh>

const std::string str_calc_info = R"(
Performs various calculations on a biomolecular structure, primarily
 various distances and angles, distance maps, etc.)";

utils::Logger l("str_calc");
typedef core::data::structural::Structure::atom_iterator atom_iterator;

void distance_map(atom_iterator first_atom, atom_iterator last_atom,
    core::data::basic::Array2D<double> destination) {

  core::index2 i = 0;
  for (auto ai = first_atom; ai != last_atom; ++ai) {
    core::index2 j = 0;
    for (auto aj = first_atom; aj != last_atom; ++aj) {
      double d = (*ai)->distance_to(*(*aj));
      destination(i, j) = d;
      destination(j, i) = d;
      j++;
    }
    i++;
  }
}

bool check_input(const size_t n_structures, const size_t n_decoys) {

  if ((n_structures > 0) && (n_decoys > 0)) {
    l << utils::LogLevel::SEVERE << "str_calc can process structures or decoys but not both at the same time\n"
        << "Try once again, providing only one of the two data sets\n";
    return false;
  }
  if ((n_structures == 0) && (n_decoys == 0)) {
    l << utils::LogLevel::SEVERE << "No input structures for crmsd calculations! Provide them with -in:pdb flag\n";
    return false;
  }
  return true;
}

int main(const int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::basic;
  using namespace core::data::structural;
  using namespace core::data::structural::selectors;
  using namespace core::calc::structural;
  using namespace core::calc::structural::transformations;
  using namespace core::protocols;

  utils::options::OptionParser & cmd = OptionParser::get("str_calc");
  cmd.register_option(utils::options::help, show_examples, utils::options::markdown_help, verbose,db_path);
  cmd.register_option(input_pdb, input_pdb_native, input_pdb_list, input_pdb_path, input_pdb_header); // input PDB structures
  cmd.register_option(input_pdb_modelslist, input_models, input_chainid_list); // input models - also in PDB format, but simplified reading
  cmd.register_option(all_models, input_pdb_sort);
  cmd.register_option(ca_only, bb_only, include_hydrogens, keep_alternate, keep_water);
  // ---------- crmsd options
  cmd.register_option(calc_crmsd,calc_crmsd_matching_atoms,calc_crmsd_matching_atoms_file,calc_crmsd_rotated_atoms,calc_crmsd_rotated_atoms_file);
  // ---------- tm-score options
  cmd.register_option(calc_tmscore,tmscore_ref_length);
  // ---------- selecting options
  cmd.register_option(select_ca,select_bb,select_bb_cb,select_cb,select_atoms_by_name, select_aa);
  cmd.register_option(select_substructure, select_models,select_chains);
  // ---------- include options for computing structural properties
  cmd.register_option(calc_dssp, calc_a13, calc_r13, calc_t14, calc_r14, calc_r15);
  cmd.register_option(calc_lambda);
  cmd.register_option(calc_chi, calc_abego, calc_hbonds_bb,calc_phi_psi,calc_tau, calc_omega);
  cmd.register_option(calc_distmap_min,calc_distmap_ca,calc_distmap_described,calc_distmap_allatom,distmap_seq_separation,longest_distance_shown);
  // ---------- output options
  cmd.register_option(output_generic, output_pdb, out_sse);

  core::index2 id = cmd.add_examples_group("Calculate crmsd:");
  cmd.add_example(id,"find all pairwise all-atom crmsd distances between all the models in a given PDB",
      "str_calc -in:pdb=2kmk-1.pdb -calc::crmsd -in:pdb::all_models -in:pdb:native=2KMK.pdb.gz");
  cmd.add_example(id,"read in only CA atoms; find all pairwise crmsd distances between all the models in a given PDB",
    "str_calc -select::ca -in:pdb=2kmk-1.pdb -calc::crmsd -in:pdb::all_models -in:pdb:native=2KMK.pdb.gz");
  cmd.add_example(id,"find all-atom crmsd distances between all models in a single PDB and the reference native structure",
      "str_calc -in:pdb=2kmk-1.pdb -calc::crmsd -in:pdb::all_models -in:pdb:native=2KMK.pdb.gz");
  cmd.add_example(id,"read in only CA atoms; find crmsd distances between all models in a single PDB and the reference native structure",
    "str_calc -select::ca -in:pdb=2kmk-1.pdb -calc::crmsd -in:pdb::all_models -in:pdb:native=2KMK.pdb.gz");
  cmd.add_example(id,"as in the example (3), but after superimposing alpha-carbons of chain A, calculate crmsd on all the atoms of chain A",
      "str_calc -in:pdb=2kmk-1.pdb -calc::crmsd -in:pdb::all_models -in:pdb:native=2KMK.pdb.gz \\\n\t-calc::crmsd::matching_atoms=A:*:_CA_ -calc::crmsd::rotated_atoms=A:*:*");
  cmd.add_example(id,"read a multi-model PDB file and superimpose each pair based on a atom subset \\\n\t(defined in an external file); then compute crmsd on residue no. 13 of chain A",
      "str_calc -in:pdb=2kmk-1.pdb -calc::crmsd -in:pdb::all_models -in:pdb:native=2KMK.pdb.gz  \\\n\t-calc::crmsd::matching_atoms::file=match_set -calc::crmsd::rotated_atoms=A:13:*");

  id = cmd.add_examples_group("Calculate structural properties of a polypeptide chain:");
  cmd.add_example(id,"find all Chi angles and rotamer types: ",
    "str_calc -in:pdb=2gb1.pdb -calc::chi -select::aa");

  id = cmd.add_examples_group("Contact and hydrogen bond maps:");
  cmd.add_example(id,"Calculate minimal distance between residues: ",
    "str_calc -in::pdb=./test_inputs/2gb1.pdb -calc::distmap::min -calc::distmap::describe");
  cmd.add_example(id,"Calculate a map of backbone hydrogen bonds in a given protein structure: ",
    "str_calc -in::pdb=./test_inputs/2gb1.pdb -calc:hbonds:bb");

  cmd.program_info(str_calc_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // ********** INPUT SECTION **********
  std::vector<core::data::basic::Coordinates_SP> decoys;
  std::vector<std::string> decoy_ids;
  utils::options::models_from_cmdline(decoy_ids, decoys);

  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;

  std::vector<core::calc::structural::ProteinArchitecture_SP> sse;

  utils::options::structures_from_cmdline(structure_ids, structures);

  // ********** SELECTIONS **********
  std::vector<std::shared_ptr<AtomSelector>> atom_selectors = atoms_selection_from_cmdline();
  if (!atom_selectors.empty()) {
    LogicalANDSelector select_all(atom_selectors);
    for (core::data::structural::Structure_SP m : structures)
      core::protocols::keep_selected_atoms(select_all, *m);
  }

  // ********** CALCULATE SECTION **********
  std::shared_ptr<std::ostream> out = utils::out_stream(option_value<std::string>(output_generic));

  // ---------- FIRST ASSIGN SSE IF REQUESTED ----------
  if (calc_dssp.was_used()) {
    for (auto s : structures) {
      sse.push_back(std::make_shared<core::calc::structural::ProteinArchitecture>(*s));
      sse.back()->annotate(*s);
    }
  }

  // ---------- CALCULATE CRMSD ----------
  if (calc_crmsd.was_used()) {
    if(!check_input(structures.size(),decoys.size())) return 0;

    AtomSelector_SP sup_at = std::make_shared<LogicalANDSelector>(atoms_selection_from_cmdline());
    AtomSelector_SP rot_at = nullptr;
    std::shared_ptr<PairwiseCrmsd> crmsd = nullptr;
    if (calc_crmsd_matching_atoms.was_used()) sup_at = atom_selection_by_locator(calc_crmsd_matching_atoms);
    if (calc_crmsd_matching_atoms_file.was_used()) sup_at = atom_selection_by_locator_file(calc_crmsd_matching_atoms_file);

    crmsd = std::make_shared<PairwiseCrmsd>(structures, sup_at, rot_at, structure_ids);
    
    std::shared_ptr<std::ostream> pdb_output = nullptr;
    if(output_pdb.was_used())
      pdb_output = utils::out_stream(option_value<std::string>(output_pdb));

    if (decoys.size() > 0) {
      if (input_pdb_native.was_used()) {
        Structure_SP native = native_from_cmdline();
        crmsd->calculate(native,pdb_output);
      } else {
        crmsd->calculate();
      }
    }
    if (structures.size() > 0) {
      if (input_pdb_native.was_used()) {
        Structure_SP native = native_from_cmdline();
        crmsd->calculate(native,pdb_output);
      } else {
        crmsd->calculate();
      }
    }

    return 0; // Don't have to do anything else once crmsd has been done
  }

  // ---------- CALCULATE TMSCORE ----------
  if (calc_tmscore.was_used()) {
    if(!check_input(structures.size(),decoys.size())) return 0;

    core::index2 ref_length = (tmscore_ref_length.was_used()) ? option_value<core::index2>(tmscore_ref_length) : 0;
    std::shared_ptr<PairwiseTMScore> tmscore = std::make_shared<PairwiseTMScore>(structures, structure_ids,ref_length);

    if (decoys.size() > 0) {
      if (input_pdb_native.was_used()) {
        Structure_SP native = native_from_cmdline();
        tmscore->calculate(native,nullptr);
      } else {
        tmscore->calculate();
      }
    }
    if (structures.size() > 0) {
      if (input_pdb_native.was_used()) {
        Structure_SP native = native_from_cmdline();
        tmscore->calculate(native,nullptr);
      } else {
        tmscore->calculate();
      }
    }

    return 0; // Don't have to do anything else once tmscore has been done
  }

  // ---------- *** CALCULATE DISTANCE map_in_contact ***  ----------
  double short_distance =
      (longest_distance_shown.was_used()) ? option_value<double>(longest_distance_shown) : 100000000.0;

  // ---------- CA DISTANCE map_in_contact  ----------
  int seq_sep = (distmap_seq_separation.was_used()) ? option_value<int>(distmap_seq_separation) : 1;
  if (calc_distmap_ca.was_used()) {
    std::vector<std::string> atom_tags;
    core::data::structural::selectors::IsCA ca_test;
    for (auto s : structures) {
      atom_tags.clear();
      std::vector<PdbAtom_SP> atoms;
      if (!calc_distmap_described.was_used()) {
        int i_atom = -1;
        for (auto ai = s->first_atom(); ai != s->last_atom(); ++ai) {
          if (!ca_test(**ai)) continue;
          atoms.push_back(*ai);
          atom_tags.push_back(utils::string_format("%5d ", ++i_atom));
        }
      } else {
        for (auto ai = s->first_atom(); ai != s->last_atom(); ++ai) {
          if (!ca_test(**ai)) continue;
          atoms.push_back(*ai);
          atom_tags.push_back(utils::string_format("%4s %s %4d%c ", (*ai)->owner()->owner()->id().c_str(),
              (*ai)->owner()->residue_type().code3.c_str(), (*ai)->owner()->id(), (*ai)->owner()->icode()));
        }
      }

      for (core::index4 i=0;i<atoms.size();++i) {
        for (core::index4 j=0;j<i;++j) {
          if (atoms[i]->owner()->id() - atoms[j]->owner()->id() < seq_sep) continue;
          const double d = atoms[i]->distance_to(*(atoms[j]));
          if (d <= short_distance) std::cout << atom_tags[i] << atom_tags[j] << utils::string_format("%7.3f\n", d);
        }
      }
    }
    return 0; // Don't have to do anything else once a CA distance map has been computed
  }

  // ********** SELECT ATOMS **********
  std::shared_ptr<AtomSelector> atom_selector
    = std::make_shared<LogicalANDSelector>(atoms_selection_from_cmdline());
  if (atom_selector != nullptr) {
    for (core::data::structural::Structure_SP m : structures) {
      for (auto c_it = m->begin(); c_it < m->end(); ++c_it) {
        auto r_it = (*c_it)->begin();
        while(r_it!=(*c_it)->end()) {
          Residue_SP r = *r_it;
          r->erase(std::remove_if(r->begin(), r->end(), core::algorithms::DereferencedNot<AtomSelector>(*atom_selector)), r->end());
          if (r->count_atoms() == 0) r_it = (*c_it)->erase(r_it);
          else ++r_it;
        }
      }
    }
  }

  if (calc_distmap_min.was_used()) {
    for (auto s : structures) {
      if (calc_distmap_described.was_used()) {
        for (auto ri = s->first_residue(); ri != s->last_residue(); ++ri) {
          for (auto rj = s->first_residue(); rj != ri; ++rj) {
            if ((*ri)->id() - (*rj)->id() < seq_sep) continue;
            const double d = (*ri)->min_distance(*rj);
            if (d <= short_distance)
              std::cout << utils::string_format("%4s %s %4d%c  %4s %s %4d%c  %7.3f\n", (*ri)->owner()->id().c_str(),
                                          (*ri)->residue_type().code3.c_str(), (*ri)->id(), (*ri)->icode(),
                                          (*rj)->owner()->id().c_str(), (*rj)->residue_type().code3.c_str(),
                                          (*rj)->id(), (*rj)->icode(), d);
          }
        }
      } else {
        core::index2 i=0;
        for (auto ri = s->first_residue(); ri != s->last_residue(); ++ri) {
          core::index2 j = 0;
          for (auto rj = s->first_residue(); rj != ri; ++rj) {
            if ((*ri)->id() - (*rj)->id() < seq_sep) continue;
            const double d = (*ri)->min_distance(*rj);
            if (d <= short_distance) std::cout << utils::string_format("%4d %4d %7.3f\n", i, j, d);
            ++j;
          }
          ++i;
        }
      }
    }
    return 0; // Don't have to do anything else once a map of minimal inter-residue distances has been computed
  }

  if (calc_distmap_allatom.was_used()) {
    for (auto s : structures) {
      if (calc_distmap_described.was_used()) {
        for (auto ai = s->first_atom(); ai != s->last_atom(); ++ai) {
          auto ri = (*ai)->owner();
          for (auto aj = s->first_atom(); aj != ai; ++aj) {
            auto rj = (*aj)->owner();
            if (ri->id() - rj->id() < seq_sep) continue;
            const double d = (*ai)->distance_to(*(*aj));
            if (d <= short_distance)
              std::cout << utils::string_format("%4s %s %4d%c %4s %4d  %4s %s %4d%c %4s %4d  %7.3f\n",
                                                ri->owner()->id().c_str(),
                                                ri->residue_type().code3.c_str(),
                                                ri->id(), ri->icode(), (*ai)->atom_name().c_str(), (*ai)->id(), rj->owner()->id().c_str(),
                                                rj->residue_type().code3.c_str(), rj->id(), rj->icode(),
                                                (*aj)->atom_name().c_str(), (*aj)->id(), d);
          }
        }
      } else {
        core::index2 i=0;
        for (auto ri = s->first_atom(); ri != s->last_atom(); ++ri) {
          core::index2 j = 0;
          for (auto rj = s->first_atom(); rj != ri; ++rj) {
            if ((*ri)->owner()->id() - (*rj)->owner()->id() < seq_sep) continue;
            const double d = (*ri)->distance_to(**rj);
            if (d <= short_distance) std::cout << utils::string_format("%4d %4d %7.3f\n", i, j, d);
            ++j;
          }
          ++i;
        }
      }
    }
    return 0; // Don't have to do anything else once a map of minimal inter-residue distances has been computed
  }

  // ---------- Detect hydrogen bonds
  if (calc_hbonds_bb.was_used()) {
    for (auto s : structures) {
      interactions::BackboneHBondCollector collect_hb;
      collect_hb.dssp_energy_cutoff(-0.2);
      std::vector<ResiduePair_SP> hbonds;
      collect_hb.collect(*s, hbonds);
      for (const auto ri : hbonds) {
        interactions::BackboneHBondInteraction_SP hb = std::static_pointer_cast<interactions::BackboneHBondInteraction>(ri);
        std::cout << *hb << "\n";
      }
    }
  }

  // ********** SHOW SECONDARY STRUCTURE BLOCKS **********
  if (out_sse.was_used()) {
    if (structures.size() > 0) {
      core::index4 i_struct = 0;
      for (auto s : structures)
        for (auto i_sse: sse[i_struct]->sse_vector()) { std::cout << i_sse->info() << std::endl; }
    }
  }

  // ********** CALCULATE LOCAL PROPERTIES OF THE CHAIN **********
  std::vector<ResidueSegmentGeometry_SP> what_to_calc;
//  if(calc_abego.was_used())
//    what_to_calc.push_back(LocalBackbonePropertyDef::property_by_name("ABEGO"));
  int frag_len = 0;
  if(calc_lambda.was_used()){
    what_to_calc.push_back(std::make_shared<LambdaDihedral>(1));
    frag_len = std::max(frag_len,3);
  }
  if(calc_r13.was_used())  {
    what_to_calc.push_back(std::make_shared<R13>(0));
    frag_len = std::max(frag_len,3);
  }
  if(calc_r14.was_used())  {
    what_to_calc.push_back(std::make_shared<R14x>(0));
    frag_len = std::max(frag_len,4);
  }
  if(calc_r15.was_used())  {
    what_to_calc.push_back(std::make_shared<R15>(0));
    frag_len = std::max(frag_len,5);
  }
  if(calc_a13.was_used()) {
    what_to_calc.push_back(std::make_shared<A13>(0));
    frag_len = std::max(frag_len,3);
  }
  if(calc_t14.was_used()) {
    what_to_calc.push_back(std::make_shared<T14>(0));
    frag_len = std::max(frag_len,4);

  }
  if(calc_phi_psi.was_used()) {
    what_to_calc.push_back(std::make_shared<Phi>(1));
    what_to_calc.push_back(std::make_shared<Psi>(1));
    frag_len = std::max(frag_len,3);
  }
  if(calc_omega.was_used()) {
    what_to_calc.push_back(std::make_shared<Omega>(1));
    frag_len = std::max(frag_len,3);
  }

  if (what_to_calc.size() > 0) {

    selectors::HasProperlyConnectedCA is_connected;
    for (auto s: structures) {
      ResidueSegmentProvider rsp(s, frag_len);
      while (rsp.has_next()) {
        const ResidueSegment_SP seg = rsp.next();
        if (is_connected(*seg)) {
          auto ss = seg->sequence();
          std::cout << ss->sequence << " " << ss->str() << " ";
          for (auto prop: what_to_calc)
            std::cout << utils::string_format(prop->format(), (*prop)(*seg)) << " ";
          std::cout  << "\n";
        }
      }
    }
  }

  ResidueHasAllHeavyAtoms has_full_sc;
  // ********** CALCULATE CHI ANGLES FOR SIDE CHAINS **********
  if (structures.size() > 0) {
    core::index4 i_struct = 0;
    for (auto s : structures) {
      l << utils::LogLevel::FINE << "Processing " << s->code() << "\n";
      for (Chain_SP c : *s) {
        // ---------- Compute sidechain dihedrals
        if (calc_chi.was_used()) {
          for(core::index2 i_resid=0;i_resid<c->count_residues();++i_resid) {
            const Residue_SP r = (*c)[i_resid];
            const core::chemical::Monomer & m = r->residue_type();
            std::cout << utils::string_format("%4d %4s %3s", r->id(), r->owner()->id().c_str(), m.code3.c_str());
            if (!has_full_sc(r)) {
              std::cout <<"\n";
              continue;
            }
            if ((m.id != core::chemical::Monomer::ALA.id) && (m.id != core::chemical::Monomer::GLY.id)) {
              std::cout << std::setw(5) << core::calc::structural::define_rotamer(*r);
              for(core::index2 i=1;i<=core::chemical::ChiAnglesDefinition::count_chi_angles(m);++i)
                std::cout
                    << utils::string_format(" %7.2f", core::calc::structural::evaluate_chi(*r, i) * 180.0 / 3.14159);
            }
            std::cout <<"\n";
          }
        }
      }
      ++i_struct;
    }
  }
}
