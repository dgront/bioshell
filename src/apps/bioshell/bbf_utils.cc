#ifndef BBF_UTILS_HH
#define BBF_UTILS_HH

#include <memory>
#include <vector>
#include <set>
#include <sys/stat.h>
#include <dirent.h>
#include <math.h>

#include <core/data/structural/Structure.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/calc/structural/transformations/Crmsd.hh>
#include <utils/Logger.hh>
#include <utils/LogManager.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <utils/files.hh>
#include <core/algorithms/basic_algorithms.hh>
#include <core/calc/structural/protein_angles.hh>

using namespace core::data::basic;
using namespace core::data::structural;
using namespace utils::options;

utils::Logger logger = utils::Logger("BBF");

/** @brief Tell if structure is complete
 *
 * @param strctr - structure to check
 * @param min_dist - minimum accepted distance beteween CA (in Angstremos).
 * if You don't want to check this condition, use 0
 * @param max_dist - maximum accepted distance between CA (in Angstremos).
 * if You don't want to check this condition, use 0.
 * @param too_small_distances - place to save name of structure if distance is to small.
 * Useful with checking large set set of proteins.
 * If You don't need this, leave it default.
 * @param to_big_distances - place to save name of structure if distance is to big.
 * Useful with checking large set set of proteins.
 * If You don't need this, leave it default.
 * @param missing_atoms - place to save name of structure if some atom missing.
 * Useful with checking large set set of proteins.
 * If You don't need this, leave it default.
 */
bool bb_is_complete(
        Structure_SP strctr,
        float min_dist = 2.8,
        float max_dist = 4.8,
        std::shared_ptr<std::vector<std::string>> too_big_distances = std::make_shared<std::vector<std::string>>(),
        std::shared_ptr<std::vector<std::string>> too_small_distances = std::make_shared<std::vector<std::string>>(),
        std::shared_ptr<std::vector<std::string>> missing_atoms = std::make_shared<std::vector<std::string>>()) {
    // Iterate over all residues in the structure
    for (auto it_chain = strctr->begin(); it_chain != strctr->end(); ++it_chain) {
        int previous_residue = 0;
        std::shared_ptr<PdbAtom> previous_CA;
        for (auto it_resid = (*it_chain)->begin(); it_resid != (*it_chain)->end(); ++it_resid) {
            const core::chemical::Monomer &m = (*it_resid)->residue_type();
            PdbAtom_SP atom_sp;
            if (m.type == 'P') {
                atom_sp = (*it_resid)->find_atom(" N  ");
                if (atom_sp == nullptr) {
                    logger << utils::LogLevel::WARNING << "Missing backbone atom N  \n";
                    missing_atoms->push_back(strctr->code());
                    return false;
                }
                atom_sp = (*it_resid)->find_atom(" CA ");
                if (atom_sp == nullptr) {
                    logger << utils::LogLevel::WARNING << "Missing backbone atom CA \n";
                    missing_atoms->push_back(strctr->code());
                    return false;
                }
                atom_sp = (*it_resid)->find_atom(" C  ");
                if (atom_sp == nullptr) {
                    logger << utils::LogLevel::WARNING << "Missing backbone atom C  \n";
                    missing_atoms->push_back(strctr->code());
                    return false;
                }
                atom_sp = (*it_resid)->find_atom(" O  ");
                if (atom_sp == nullptr) {
                    logger << utils::LogLevel::WARNING << "Missing backbone atom O  \n";
                    missing_atoms->push_back(strctr->code());
                    return false;
                }
                if (previous_residue == 0) {
                    previous_residue = (*it_resid)->id();
                    previous_CA = (*it_resid)->find_atom(" CA ");
                }
                else {
                    previous_residue = (*it_resid)->id();
                    float distance = previous_CA->distance_to(*((*it_resid)->find_atom(" CA ")));
                    if (min_dist != 0 and distance <= min_dist) {
                        logger << utils::LogLevel::WARNING << "Distance between CA is smaller than " << min_dist <<
                        "A!\n";
                        too_small_distances->push_back(strctr->code());
                        return false;
                    }
                    else if (max_dist != 0 and distance >= max_dist) {
                        logger << utils::LogLevel::WARNING << "Distance between CA is grather than " << max_dist <<
                        "A!\n";
                        too_big_distances->push_back(strctr->code());
                    }
                    previous_CA = (*it_resid)->find_atom(" CA ");
                }
            }
        }
    }
    logger << utils::LogLevel::FINER << "Backbone complete!\n";
    return true;
}

void build_database(std::string input, std::string output_dir, int fragment_len = 5, bool input_subset = false,
                    bool recursive = true) {
    // contains addresses to rejected files
    // name of vector is reason why we not use
    std::shared_ptr<std::vector<std::string>> too_big_distances = std::make_shared<std::vector<std::string>>();
    std::shared_ptr<std::vector<std::string>> too_small_distances = std::make_shared<std::vector<std::string>>();
    std::shared_ptr<std::vector<std::string>> missing_atoms = std::make_shared<std::vector<std::string>>();
    std::shared_ptr<std::set<std::string>> files = std::make_shared<std::set<std::string>>();
    if (input_subset) { subsetFiles(input, files); }
    else { listFiles(input, files); }
    std::shared_ptr<std::fstream> db_file(new std::fstream);
    std::string output_directory = output_dir;
    db_file->open(output_directory + std::to_string(fragment_len) + "_full_database", std::ios::out);
    if (!db_file->good()) {
        db_file->open(std::to_string(fragment_len) + "_full_database", std::ios::out);
        logger << utils::LogLevel::SEVERE << "No such directory! Using actual working directory!\n";
    }
    float line_ref = 0; //reference to actual line in "full" database file
    // ## MUST be increased with adding new RESIDUE to "full" database
    std::vector<std::string> damaged_pdb;
    std::vector<std::string> incomplete_pdb;
    std::set<std::pair<float, std::vector<float>>> set_db_dist;
    for (const std::string &file_name : *files) {
        try {
            Structure_SP strctr;
            core::data::io::Pdb reader(file_name, core::data::io::keep_all,core::data::io::keep_all, true);
            strctr = reader.create_structure(0);
            if (bb_is_complete(strctr, 2.7, 0, too_small_distances, too_big_distances, missing_atoms)) {
                for (auto it_chain = strctr->begin(); it_chain != strctr->end(); ++it_chain) {
                    //for each chain in structure
                    (*db_file) << ">" << strctr->code() << ".";
                    (*db_file) << (*it_chain)->id() << std::endl;    //>ID.chain
                    std::vector<PdbAtom> fragments_vector(fragment_len);
                    int begin_counter = 0;
                    PdbAtom previous_CA_atom;
                    for (auto it_resid = (*it_chain)->begin(); it_resid != (*it_chain)->end(); ++it_resid) {
                        //for each residue in chain
                        //if last iteration
                        const core::chemical::Monomer &m = (*it_resid)->residue_type();
                        PdbAtom_SP atom_sp;
                        if (m.type == 'P') {
                            atom_sp = (*it_resid)->find_atom(" N  ");
                            (*db_file) << atom_sp->to_pdb_line().substr(30, 24);
                            atom_sp = (*it_resid)->find_atom(" CA ");
                            (*db_file) << atom_sp->to_pdb_line().substr(30, 24);


                            /////////////////////////// saving distance ///////////////////////////////////////////////
                            if ((begin_counter > 0) and (previous_CA_atom.distance_to(*atom_sp) > 4.7)) {
                                // gap in chain
                                begin_counter = 0;
                                fragments_vector[begin_counter] = *atom_sp;
                            }
                            else if (begin_counter >= fragment_len - 1) {
                                // fragment
                                std::vector<float> tmp_lengths;
                                fragments_vector[fragment_len - 1] = *atom_sp;
                                for (int i = 2; i < fragment_len; ++i) {
                                    tmp_lengths.emplace_back(fragments_vector[0].distance_to(fragments_vector[i]));
                                }
                                set_db_dist.emplace(line_ref - (fragment_len - 1), tmp_lengths);
                                std::vector<PdbAtom> tmp(fragments_vector.begin() + 1, fragments_vector.end());
                                fragments_vector.erase(fragments_vector.begin(), fragments_vector.end() - 1);
                                fragments_vector.insert(fragments_vector.begin(), tmp.begin(), tmp.end());
                            }
                            else {
                                // fragment not yet loaded
                                fragments_vector[begin_counter] = *atom_sp;
                            }
                            /////////////////////////// end of saving distance ////////////////////////////////////////


                            previous_CA_atom = *atom_sp;
                            atom_sp = (*it_resid)->find_atom(" C  ");
                            (*db_file) << atom_sp->to_pdb_line().substr(30, 24);
                            atom_sp = (*it_resid)->find_atom(" O  ");
                            (*db_file) << atom_sp->to_pdb_line().substr(30, 24);
                            (*db_file) << std::endl;
                            ++line_ref;
                            ++begin_counter;
                        }
                    }
                    // adding last distances between CA
                    for (auto it = fragments_vector.begin(); it != fragments_vector.end(); ++it) {
                        std::cout << "(" << (*it) << ")";
                    }
                    std::cout << "\n";
                    for (int i = 0; i != fragment_len - 3; ++i) {
                        std::vector<float> tmp_lengths;
                        for (int j = i + 2; j < fragment_len - 1; ++j) {
                            std::cout << i << ":" << j << " " << fragments_vector[i] << ":" << fragments_vector[j] <<
                            std::endl;
                            tmp_lengths.emplace_back(fragments_vector[i].distance_to(fragments_vector[j]));
                        }
                        for (int j = tmp_lengths.size(); j != fragment_len - 2; ++j) {
                            tmp_lengths.emplace_back(0);
                        }
                        if (tmp_lengths.size() != 0) {
                            set_db_dist.emplace((line_ref - fragment_len) + 0.1 * (i + 1), tmp_lengths);
                        }
                    }
                }
            }
            else {
                incomplete_pdb.push_back(file_name);
            }

        }
        catch (const std::out_of_range &oor) {
            damaged_pdb.push_back(file_name);
        }
    }
    db_file->close();

    //saving distance file
    std::shared_ptr<std::fstream> db_dist_file(new std::fstream);
    db_dist_file->open(output_directory + std::to_string(fragment_len) + "_db_dist", std::ios::out);
    if (!db_dist_file->good()) {
        logger << utils::LogLevel::SEVERE << "No such directory! Using actual working directory!\n";
        db_dist_file->open(std::to_string(fragment_len) + "_db_dist", std::ios::out);
    }
    //save fragment length to distances file
    *db_dist_file << fragment_len << std::endl;
    //simply save distance file
    //constance first column width but not use this feature later
    //8 characters to distance, space, reference(not limited length)
    for (std::set<std::pair<float, std::vector<float>>>::iterator it = set_db_dist.begin();
         it != set_db_dist.end(); ++it) {
        std::string str_reference = utils::to_string((*it).first);
        str_reference = str_reference + std::string(16, ' ');
        str_reference = str_reference.substr(0, 16);
        *db_dist_file << str_reference << ' ';
        for (std::vector<float>::const_iterator v_it = ((*it).second).begin(); v_it != ((*it).second).end(); ++v_it) {
            std::string str_dist = utils::to_string(*v_it);
            str_dist = str_dist + std::string(8, ' ');
            str_dist = str_dist.substr(0, 8);
            *db_dist_file << str_dist << ' ';
        }
        *db_dist_file << std::endl;
    }
    db_dist_file->close();

//	DEBUG INFO (for users)


// 	READERS FAILS
    if (damaged_pdb.size() != 0) {
        core::algorithms::uniquify(damaged_pdb.begin(), damaged_pdb.end());
        logger << utils::LogLevel::WARNING << "Damaged PDB files (reader crashed): " << damaged_pdb.size() << "\n";
        std::copy(damaged_pdb.begin(), damaged_pdb.end(), std::ostream_iterator<std::string>(std::cout, ", "));
        std::cout << std::endl << std::endl;
    }

// 	TOO BIG DISTANCES
    if (too_big_distances->size() != 0) {
        core::algorithms::uniquify(too_big_distances->begin(), too_big_distances->end());
        logger << utils::LogLevel::WARNING << "Too big distances in: " << too_big_distances->size() << "\n";
        std::copy(too_big_distances->begin(), too_big_distances->end(),
                  std::ostream_iterator<std::string>(std::cout, ", "));
        std::cout << std::endl;
    }

// 	TOO SMALL DISTANCES
    if (too_small_distances->size() != 0) {
        core::algorithms::uniquify(too_small_distances->begin(), too_small_distances->end());
        logger << utils::LogLevel::WARNING << "Too small distances in: " << too_small_distances->size() << "\n";
        std::copy(too_small_distances->begin(), too_small_distances->end(),
                  std::ostream_iterator<std::string>(std::cout, ", "));
        std::cout << std::endl;
    }

// 	MISSING BACKBONE ATOMS
    if (missing_atoms->size() != 0) {
        core::algorithms::uniquify(missing_atoms->begin(), missing_atoms->end());
        logger << utils::LogLevel::WARNING << "Missing backbone atoms in: " << missing_atoms->size() << "\n";
        std::copy(missing_atoms->begin(), missing_atoms->end(), std::ostream_iterator<std::string>(std::cout, ", "));
        std::cout << std::endl;
    }


    logger << utils::LogLevel::FINEST << "Database ready to use!\n";
    std::cout << std::endl << "Database ready to use!\n";
}

float std_dev(std::vector<float> v, float ave) { // It should be somewhere else
    float E = 0;
    float inverse = 1.0 / static_cast<float>(v.size());
    for (unsigned i = 0; i < v.size(); i++) {
        E += pow(static_cast<float>(v[i]) - ave, 2);
    }
    return sqrt(inverse * E);
}


class DatabaseKeeper {
    int fragment_len;
    int number_of_subsets;
    std::shared_ptr<std::vector<std::shared_ptr<std::set<std::pair<float, int>>>>> all_dist;
    std::shared_ptr<std::vector<std::shared_ptr<Residue>>> db_res;

//    void wys(
//            std::shared_ptr<std::vector<std::shared_ptr<std::pair<float, std::shared_ptr<std::vector<float>>>>>> tmp_distance_matrix) {
//        for (auto it = tmp_distance_matrix->begin(); it != tmp_distance_matrix->end(); ++it) {
//            std::cout << ((*it)->first) << ":[";
//            for (auto it2 = (*it)->second->begin(); it2 != (*it)->second->end(); ++it2) {
//                std::cout << (*it2) << ",";
//            }
//            std::cout << "]\n";
//        }
//        std::cout << std::endl;
//    }

    int load_databases(std::string db_dist_file_string, std::string db_full_file_string) {
        //load database from given files to given structure
        //returns length of fragments loaded
        //on errors return -1
        int fragment_len = -1;
        std::ifstream db_dist_file;
        db_dist_file.open(db_dist_file_string);
        if (!db_dist_file.good()) {
            logger << utils::LogLevel::CRITICAL << "Distance file broken/missing\n";
            return -1;
        }
        db_dist_file >> fragment_len;
        int distances_count = ((fragment_len - 1) * (fragment_len - 2)) / 2;
        all_dist = std::make_shared<std::vector<std::shared_ptr<std::set<std::pair<float, int>>>>>(distances_count);

        bool brk = false;
        // tmp matrix
        std::shared_ptr<std::vector<std::shared_ptr<std::pair<float, std::shared_ptr<std::vector<float>>>>>> tmp_distance_matrix;
        tmp_distance_matrix = std::make_shared<std::vector<std::shared_ptr<std::pair<float, std::shared_ptr<std::vector<float>>>>>>();
        float reference;

        while (!brk) {
            if (tmp_distance_matrix->size() < fragment_len - 2) {
                // prepare tmp matrix
                float tmp;
                db_dist_file >> reference;
                if (!db_dist_file.good()) {
                    brk = true;
                    break;
                }
                std::shared_ptr<std::pair<float, std::shared_ptr<std::vector<float>>>> pair =
                        std::make_shared<std::pair<float, std::shared_ptr<std::vector<float>>>>();
                pair->first = reference;
                pair->second = std::make_shared<std::vector<float>>();
                tmp_distance_matrix->emplace_back(pair);
                for (int i = 0; i < fragment_len - 2; ++i) {
                    db_dist_file >> tmp;
                    tmp_distance_matrix->back()->second->emplace_back(tmp);
                }
                int actual_size = tmp_distance_matrix->size();
                for (int j = 1; j < actual_size; ++j) {
                    for (int k = 0; k < fragment_len - (2 + j); k++) {
                        (*tmp_distance_matrix)[actual_size - (1 + j)]->second->emplace_back(
                                (*(*tmp_distance_matrix)[actual_size - 1]->second)[k]);
                    }
                }
            }
            if (tmp_distance_matrix->size() == fragment_len - 2) {
                float tmp;
                double intpart;
                std::shared_ptr<std::pair<float, std::shared_ptr<std::vector<float>>>> first = (*tmp_distance_matrix)[0];
                for (int i = 0; i < distances_count; ++i) {
                    if (((*all_dist)[i]) == nullptr) {
                        (*all_dist)[i] = std::make_shared<std::set<std::pair<float, int>>>();
                    }
                    (*all_dist)[i]->emplace((*(first->second))[i], first->first);

                }
                db_dist_file >> reference;
                std::vector<std::shared_ptr<std::pair<float, std::shared_ptr<std::vector<float>>>>> tmp_distance_matrix_copy(
                        tmp_distance_matrix->begin() + 1, tmp_distance_matrix->end());
                *tmp_distance_matrix = tmp_distance_matrix_copy;


                std::shared_ptr<std::pair<float, std::shared_ptr<std::vector<float>>>> pair =
                        std::make_shared<std::pair<float, std::shared_ptr<std::vector<float>>>>();
                pair->first = reference;
                pair->second = std::make_shared<std::vector<float>>();
                tmp_distance_matrix->emplace_back(pair);
                for (int i = 0; i < fragment_len - 2; ++i) {
                    db_dist_file >> tmp;
                    tmp_distance_matrix->back()->second->emplace_back(tmp);
                }
                for (int j = 1; j < fragment_len - 2; ++j) {
                    for (int k = 0; k < fragment_len - (2 + j); k++) {
                        (*tmp_distance_matrix)[(fragment_len - 3) - j]->second->emplace_back(
                                (*((*tmp_distance_matrix)[fragment_len - 3]->second))[k]);
                    }
                }
                std::shared_ptr<std::pair<float, std::shared_ptr<std::vector<float>>>> second = (*tmp_distance_matrix)[1];
                if (modf(second->first, &intpart) > 0.05) {
                    first = (*tmp_distance_matrix)[0];
                    for (int i = 0; i < distances_count; ++i) {
                        if (((*all_dist)[i]) == nullptr) {
                            (*all_dist)[i] = std::make_shared<std::set<std::pair<float, int>>>();
                        }
                        (*all_dist)[i]->emplace((*(first->second))[i], first->first);

                    }
                    tmp_distance_matrix = std::make_shared<std::vector<std::shared_ptr<std::pair<float, std::shared_ptr<std::vector<float>>>>>>();
                }
            }
        }
        db_dist_file.close();
        for (int i = 0; i < distances_count; ++i) {
            std::cout << "Distances " << i << "[" << (*all_dist)[i]->size() << "]" << std::endl;
            for (auto it = (*all_dist)[i]->begin();
                 it != (*all_dist)[i]->end(); ++it) {
                std::cout << it->second << " : " << it->first << ", ";
            }
            std::cout << "\n\n";
        }


        std::ifstream db_file;
        db_file.open(db_full_file_string);
        std::string tmp;

        if (!db_file.good()) {
            logger << utils::LogLevel::CRITICAL << "Database file broken/missing\n";
            return -1;
        }
        int res_id = 0;
        while (db_file.good()) {
            getline(db_file, tmp);
            if (tmp.length() == 0) { break; }
            if (tmp[0] != '>') {
                db_res->push_back(
                        std::make_shared<Residue>(res_id, 'G')
                );
                ++res_id;
                (*(db_res->end() - 1))->push_back(
                        std::make_shared<PdbAtom>(1, " N  ", stof(tmp.substr(0, 8)), stof(tmp.substr(8, 8)),
                                                  stof(tmp.substr(16, 8))));
                (*(db_res->end() - 1))->push_back(
                        PdbAtom_SP(
                                new PdbAtom(2, " CA ", stof(tmp.substr(24, 8)), stof(tmp.substr(32, 8)),
                                            stof(tmp.substr(40, 8)))));
                (*(db_res->end() - 1))->push_back(
                        PdbAtom_SP(
                                new PdbAtom(3, " C  ", stof(tmp.substr(48, 8)), stof(tmp.substr(56, 8)),
                                            stof(tmp.substr(64, 8)))));
                (*(db_res->end() - 1))->push_back(
                        PdbAtom_SP(
                                new PdbAtom(4, " O  ", stof(tmp.substr(72, 8)), stof(tmp.substr(80, 8)),
                                            stof(tmp.substr(88, 8)))));
            }
            else { res_id = 0; }

        }
        db_file.close();
        return fragment_len;
    }

    std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<Residue>>>>> give_best_fragments(
            std::shared_ptr<std::vector<std::shared_ptr<Residue>>> query_fragment,
            unsigned int n_of_fragments = 100) {

        class SubDatabaseKeeper {
            std::shared_ptr<std::set<std::pair<float, int>>> dist;
            float query_dist;
            std::set<std::pair<float, int>>::iterator it_middle;
            bool last_up;
            std::set<std::pair<float, int>>::iterator it_lower;
            bool low_end;
            std::set<std::pair<float, int>>::iterator it_upper;
            bool up_end;

        public:
            int give_next() {
                std::pair<float, int> i;
                if (last_up) {
                    last_up = false;
                    if (!up_end) {
                        if (it_upper == dist->end()) {
                            up_end = true;
                            i = *it_lower;
                            --it_lower;
                        }
                        else {
                            i = *it_upper;
                            ++it_upper;
                        }
                    }
                    else {
                        i = *it_lower;
                        --it_lower;
                    }
                }
                else {
                    last_up = true;
                    if (!low_end) {
                        if (it_lower == dist->begin()) {
                            low_end = true;
                            i = *it_lower;
                        }
                        else {
                            i = *it_lower;
                            --it_lower;
                        }
                    }
                    else {
                        i = *it_upper;
                        ++it_upper;
                    }
                }
                return i.second;
            }

            SubDatabaseKeeper(std::shared_ptr<std::set<std::pair<float, int>>> dista, float query_dista) {
                dist = dista;
                query_dist = query_dista;
                it_middle = (*dist).lower_bound(std::make_pair(query_dist, 0));
                last_up = false;
                it_lower = (*dist).lower_bound(std::make_pair(query_dist, 0));
                low_end = false;
                it_upper = (*dist).lower_bound(std::make_pair(query_dist, 0));
                up_end = false;
            }
        };

        std::vector<float> query_dists;
        std::vector<PdbAtom_SP> query_fragment_ca;
        for (auto it_res = query_fragment->begin(); it_res != query_fragment->end(); ++it_res) {
            PdbAtom_SP atom_sp = (*it_res)->find_atom(" CA ");
            query_fragment_ca.push_back(atom_sp);
        }
        for (int i = 0; i < fragment_len; ++i) {
            for (int j = i + 2; j < fragment_len; ++j) {
                query_dists.push_back(query_fragment_ca[i]->distance_to(*query_fragment_ca[j]));
            }
        }
        std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<Residue>>>>> best_fragments
                = std::make_shared<std::vector<std::shared_ptr<std::vector<std::shared_ptr<Residue>>>>>(0);
        if (n_of_fragments > ((*all_dist)[0])->size()) {
            //logger<<utils::LogLevel::WARNING<<"No enough fragments in database! full database\n";
            n_of_fragments = ((*all_dist)[0])->size();
        }
        std::vector<std::shared_ptr<SubDatabaseKeeper>> vec_of_sub;
        //std::unordered_map<int,unsigned short> candidates; //reference number, how many subsets includes this one
        unsigned size_of_cand = 2 * (((*all_dist)[0])->size());
        unsigned short candidates[size_of_cand];  // DANGER !!!
        std::fill_n(candidates, size_of_cand, 0);
        for (int i = 0; i < number_of_subsets; ++i) {
            std::shared_ptr<SubDatabaseKeeper> sub = std::make_shared<SubDatabaseKeeper>(((*all_dist)[i]),
                                                                                         query_dists[i]);
            vec_of_sub.push_back(sub);
        }
        for (int i = 0; i < number_of_subsets; ++i) {
            vec_of_sub[i]->give_next();
        }
        while (best_fragments->size() < n_of_fragments) {
            for (int i = 0; i < number_of_subsets; ++i) {
                int next = vec_of_sub[i]->give_next();
                candidates[next]++;
                if (candidates[next] == number_of_subsets) {
                    std::shared_ptr<std::vector<std::shared_ptr<Residue>>> tmp_fragment =
                            std::make_shared<std::vector<std::shared_ptr<Residue>>>(0);
                    for (int k = 0; k != fragment_len; ++k) {
                        // Getting next residues becouse we have reference number to first Residue only.
                        tmp_fragment->push_back((*db_res)[next + k]);
                    }
                    best_fragments->push_back(tmp_fragment);
                    if ((best_fragments->size() >= n_of_fragments)) { break; }
                }
            }
        }
        return best_fragments;
    }

    // function give best fragment from vector of fragments
    std::shared_ptr<std::vector<std::shared_ptr<Residue>>> give_best_from_vector(
            std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<Residue>>>>> best_n_fragments,
            std::shared_ptr<std::vector<std::shared_ptr<Residue>>> query_fragment,
            bool only_ca) {
        std::shared_ptr<std::vector<std::shared_ptr<Residue>>> best_fragment = nullptr; //output
        double best_value = std::numeric_limits<double>::max();
        std::vector<PdbAtom> vec_query_fragment;
        std::vector<PdbAtom> best_vec_fragment;
        bool n, c, o;
        n = c = o = false; // true if atom exist in query
        int additional_atoms = 0;
        for (auto residue = (*query_fragment).begin(); residue != (*query_fragment).end(); ++residue) {
            if (!only_ca && (*residue)->find_atom(" N  ")) {
                vec_query_fragment.push_back((*(*residue)->find_atom(" N  ")));
                n = true;
                ++additional_atoms;
            }
            vec_query_fragment.push_back((*(*residue)->find_atom(" CA ")));
            if (!only_ca && (*residue)->find_atom(" C  ")) {
                vec_query_fragment.push_back((*(*residue)->find_atom(" C  ")));
                c = true;
                ++additional_atoms;
            }
            if (!only_ca && (*residue)->find_atom(" O  ")) {
                vec_query_fragment.push_back((*(*residue)->find_atom(" O  ")));
                o = true;
                ++additional_atoms;
            }
        }
        int licznik = 0;
        for (auto fragment = best_n_fragments->begin(); fragment != best_n_fragments->end(); ++fragment) {
            ++licznik;
            std::vector<PdbAtom> vec_fragment;
            for (auto residue = (*(*fragment)).begin(); residue != (*(*fragment)).end(); ++residue) {
                if (n) vec_fragment.push_back((*(*residue)->find_atom(" N  ")));
                vec_fragment.push_back((*(*residue)->find_atom(" CA ")));
                if (c) vec_fragment.push_back((*(*residue)->find_atom(" C  ")));
                if (o) vec_fragment.push_back((*(*residue)->find_atom(" O  ")));
            }
            core::calc::structural::transformations::Crmsd<std::vector<PdbAtom>, std::vector<PdbAtom>> crmsd_o;
            double tmp_value = crmsd_o.crmsd(vec_fragment, vec_query_fragment, fragment_len + additional_atoms, false);
            if (tmp_value < best_value) {
                best_value = tmp_value;
                best_fragment = (*fragment);
                best_vec_fragment = vec_fragment;
            }
        }
        core::calc::structural::transformations::Crmsd<std::vector<PdbAtom>, std::vector<PdbAtom>> crmsd_o;
        crmsd_o.crmsd(best_vec_fragment, vec_query_fragment, fragment_len + additional_atoms, true);
        std::shared_ptr<std::vector<std::shared_ptr<Residue>>> bf = std::make_shared<std::vector<std::shared_ptr<Residue>>>(
                0);
        for (auto residue = (*best_fragment).begin(); residue != (*best_fragment).end(); ++residue) {
            std::shared_ptr<Residue> tmp_residue = std::make_shared<Residue>((*residue)->id(),
                                                                             (*residue)->residue_type());
            for (auto atom = (*residue)->begin(); atom != (*residue)->end(); ++atom) {
                PdbAtom tmp_atom_object = *(*atom);
                std::shared_ptr<PdbAtom> tmp_atom = std::make_shared<PdbAtom>(tmp_atom_object);
                crmsd_o.apply(*tmp_atom);
                tmp_residue->push_back(tmp_atom);
            }
            bf->push_back(tmp_residue);
        }
        return bf;
    }


public:
    int get_length() { return fragment_len; }

    DatabaseKeeper(std::string db_dist_file, std::string db_full_file) {
        db_res = std::make_shared<std::vector<std::shared_ptr<Residue>>>();
        fragment_len = load_databases(db_dist_file, db_full_file);
        number_of_subsets = ((fragment_len - 2) * (fragment_len - 1)) / 2;
    }

    std::shared_ptr<std::vector<std::shared_ptr<Residue>>> give_best(
            std::shared_ptr<std::vector<std::shared_ptr<Residue>>> query_fragment, int n_of_fragments = 100,
            bool only_ca = true) {
        return give_best_from_vector(give_best_fragments(query_fragment, n_of_fragments), query_fragment, only_ca);
    }
};


void paste_fragment(    //function paste given fragment, more general-purpose version is needed
        std::shared_ptr<Chain> final_chain,
        std::shared_ptr<std::vector<std::shared_ptr<Residue>>> fragment,
        int fragment_out, int last_in, // last_in from main function (bbf.cc)
        int what_stay
) {
    int from_begin = 0;
    if (last_in != -1) {
        from_begin += fragment_out;
        last_in += fragment_out;
    }
    for (auto it_residue = (*fragment).begin() += from_begin; it_residue != (*fragment).end(); ++it_residue) {
        PdbAtom_SP n = ((*final_chain)[last_in + 1])->find_atom(" N  ");
        PdbAtom_SP alpha = ((*final_chain)[last_in + 1])->find_atom(" CA ");
        PdbAtom_SP c = ((*final_chain)[last_in + 1])->find_atom(" C  ");
        PdbAtom_SP o = ((*final_chain)[last_in + 1])->find_atom(" O  ");
        ((*final_chain)[last_in + 1])->clear(); // numeration? should another atoms be maintain in residue?
        if (what_stay == 1 && n != NULL) ((*final_chain)[last_in + 1])->push_back(n);
        else ((*final_chain)[last_in + 1])->push_back((*it_residue)->find_atom(" N  "));
        if (what_stay != -1) ((*final_chain)[last_in + 1])->push_back(alpha);
        else ((*final_chain)[last_in + 1])->push_back((*it_residue)->find_atom(" CA "));
        if (what_stay == 1 && n != NULL) ((*final_chain)[last_in + 1])->push_back(c);
        else ((*final_chain)[last_in + 1])->push_back((*it_residue)->find_atom(" C  "));
        if (what_stay == 1 && n != NULL) ((*final_chain)[last_in + 1])->push_back(o);
        else ((*final_chain)[last_in + 1])->push_back((*it_residue)->find_atom(" O  "));
        ++last_in;
    }
}

void build_chain(std::shared_ptr<DatabaseKeeper> database, Chain_SP chain, int n_of_fragments = 100,
                 int first = 1, int last = -1, int t1 = 1, int t2 = 1, int what_stay = 0, bool only_ca = false,
                 bool fix_index = true) {
    logger << utils::LogLevel::INFO << "Rebuilding chain\n";
    std::shared_ptr<std::vector<std::shared_ptr<Residue>>> ref = std::make_shared<std::vector<std::shared_ptr<Residue>>>();
    for (auto it_res = chain->begin(); it_res != chain->end(); ++it_res) {
        std::shared_ptr<Residue> res = std::make_shared<Residue>((*it_res)->id(), (*it_res)->residue_type());
        for (auto it_atom = (*it_res)->begin(); it_atom != (*it_res)->end(); ++it_atom) {
            std::shared_ptr<PdbAtom> at =
                    std::make_shared<PdbAtom>((*it_atom)->id(), (*it_atom)->atom_name(), (*it_atom)->x, (*it_atom)->y,
                                              (*it_atom)->z);
            res->push_back(at);
        }
        ref->push_back(res);
    }
    std::shared_ptr<std::vector<std::shared_ptr<Residue>>> query_fragment =
            std::make_shared<std::vector<std::shared_ptr<Residue>>>();
    // number of last residue in chain from output structure, which is not participating in trimming
    int last_in = first - 2;
    if (last_in - (int) t2 >= -1) { last_in -= t2; }
    int length_of_fragments = database->get_length();
    if (last == -1) { last = (int) (chain->size()); }
    core::index4 tmp_atom_number = (*((*((*chain)[0]))[0])).id();
    while (last_in + length_of_fragments < last) {
        for (int residue = last_in + 1; residue <= last_in + length_of_fragments; ++residue) {
            query_fragment->push_back((*ref)[residue]);
        }
        std::shared_ptr<std::vector<std::shared_ptr<Residue>>> best_fragment =
                database->give_best(query_fragment, only_ca);
        paste_fragment(chain, best_fragment, t2, last_in, what_stay);
        last_in += (length_of_fragments - (t1 + t2));
        query_fragment->clear();
    }
    //missing residues are added without dependency on trimming mode from fragment with lenght=length_of_fragments
    for (int residue = last - length_of_fragments; residue != last; ++residue) {
        query_fragment->push_back((*chain)[residue]);
    }
    std::shared_ptr<std::vector<std::shared_ptr<Residue>>> best_fragment =
            database->give_best(query_fragment, n_of_fragments, only_ca);
    paste_fragment(chain, best_fragment, 0, last - length_of_fragments - 1, what_stay);
    // atoms indexes fixing
    if (fix_index) {
        for (auto it_res = chain->begin(); it_res != chain->end(); ++it_res) {
            for (auto it_atom = (*it_res)->begin(); it_atom != (*it_res)->end(); ++it_atom) {
                (*(*it_atom)).id(tmp_atom_number);
                ++tmp_atom_number;
            }
        }
    }
}

void build_whole_structure(std::shared_ptr<DatabaseKeeper> database, Structure_SP query, int n_of_fragments = 100,
                           int t1 = 1, int t2 = 1, int what_stay = 0, bool only_ca = false) {
    logger << utils::LogLevel::INFO << "Rebuilding whole structure\n";
    for (auto it_chain = (*query).begin(); it_chain != (*query).end(); ++it_chain) {
        build_chain(database, *it_chain, n_of_fragments, 1, -1, t1, t2, what_stay, only_ca, false);
    }
    // atoms indexes fixing
    core::index4 tmp_atom_number = 1;
    for (auto it_chain = query->begin(); it_chain != query->end(); ++it_chain) {
        for (auto it_res = (*it_chain)->begin(); it_res != (*it_chain)->end(); ++it_res) {
            for (auto it_atom = (*it_res)->begin(); it_atom != (*it_res)->end(); ++it_atom) {
                (*(*it_atom)).id(tmp_atom_number);
                ++tmp_atom_number;
            }
        }
    }
}

// container holding backbone CRMSD and phi, psi differences
struct bbf_test_results {
    float crmsd;
    std::vector<float> phis;
    std::vector<float> psis;
};

// function works in same way as build_whole_structure, but calculates CRMSD and phi, psi angles
// one additional argument is essential: reference <--- must have all (and only) backbone atoms
bbf_test_results build_whole_structure_test(std::shared_ptr<DatabaseKeeper> database, Structure_SP reference,
                                            Structure_SP query,
                                            int number_of_fragments = 100, unsigned t1 = 1, unsigned t2 = 1,
                                            int what_stay = 0, bool only_ca = false) {
    float crmsd = 0;
    std::vector<float> phis;
    std::vector<float> psis;
    bbf_test_results results = {crmsd, phis, psis};
    build_whole_structure(database, query, number_of_fragments, t1, t2, what_stay, only_ca);
    std::vector<PdbAtom> ref_atoms;
    std::vector<PdbAtom> query_atoms;
    std::vector<double> ref_phis;
    std::vector<double> query_phis;
    std::vector<double> ref_psis;
    std::vector<double> query_psis;
    for (auto it_chain = reference->begin(); it_chain != reference->end(); ++it_chain) {
        unsigned i = 0;
        for (auto it_res = (*it_chain)->begin(); it_res != (*it_chain)->end(); ++it_res) {
            const Residue &r = *(*(*it_chain))[i];
            if (i != 0) {
                const Residue &r_prev = *(*(*it_chain))[i - 1];
                ref_phis.push_back(core::calc::structural::evaluate_phi(r_prev, r));
            }
            if ((*(*it_chain)).size() > i + 1) {
                const Residue &r_next = *(*(*it_chain))[i + 1];
                ref_psis.push_back(core::calc::structural::evaluate_psi(r, r_next));
            }
            ++i;
            for (auto it_atom = (*it_res)->begin(); it_atom != (*it_res)->end(); ++it_atom) {
                ref_atoms.push_back(*(*it_atom));
            }
        }
    }
    for (auto it_chain = query->begin(); it_chain != query->end(); ++it_chain) {
        unsigned i = 0;
        for (auto it_res = (*it_chain)->begin(); it_res != (*it_chain)->end(); ++it_res) {
            const Residue &r = *(*(*it_chain))[i];
            if (i != 0) {
                const Residue &r_prev = *(*(*it_chain))[i - 1];
                query_phis.push_back(core::calc::structural::evaluate_phi(r_prev, r));
            }
            if ((*(*it_chain)).size() > i + 1) {
                const Residue &r_next = *(*(*it_chain))[i + 1];
                query_psis.push_back(core::calc::structural::evaluate_psi(r, r_next));
            }
            ++i;
            for (auto it_atom = (*it_res)->begin(); it_atom != (*it_res)->end(); ++it_atom) {
                query_atoms.push_back(*(*it_atom));
            }
        }
    }
    core::calc::structural::transformations::Crmsd<std::vector<PdbAtom>, std::vector<PdbAtom>> crmsd_o;
    results.crmsd = crmsd_o.crmsd(ref_atoms, query_atoms, ref_atoms.size(), false);
    for (unsigned int i = 0; i < ref_phis.size(); i++) {
        results.phis.push_back(std::min(std::abs(ref_phis[i] - query_phis[i]), std::abs(ref_phis[i] + query_phis[i])));
        results.psis.push_back(std::min(std::abs(ref_psis[i] - query_psis[i]), std::abs(ref_psis[i] + query_psis[i])));
    }
    return results;
}

#endif