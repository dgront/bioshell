#include <cstdio>
#include <ctime>
#include <algorithm>     //for erase and remove_if
#include <iostream>
#include <memory>
#include <functional>

#include <core/data/io/Pdb.hh>
#include <core/data/io/fasta_io.hh>
#include <core/data/io/pir_io.hh>
#include <core/data/io/ss2_io.hh>
#include <core/data/io/clustalw_io.hh>
#include <core/data/sequence/SequenceFilter.hh>
#include <core/data/sequence/SequenceProfile.hh>
#include <core/data/sequence/ReduceSequenceAlphabet.hh>

#include <core/alignment/on_alignment_computations.hh>
#include <core/alignment/Target.hh>
#include <core/alignment/Template.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/selector_from_cmdline.hh>
#include <utils/options/align_options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/calc_options.hh>
#include <utils/options/select_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <utils/Logger.hh>
#include <utils/io_utils.hh>

utils::Logger l("seqc");

const std::string seqc_info = R"(
Converts between different protein (or nucleic) sequence file formats. Also, it can perform some basic calculations on sequences.)";


int main(const int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::sequence;

  Option reduce_seq("-reduce", "-seqc:reduce", "write input sequence(s) in a reduced alphabet; "
    "alphabet name must be provided with this option, otherwise the program just print the list of valid alphabets");
  Option sort_seq("-sort", "-seqc:sort", "sort the sequences before they are printed");

  utils::options::OptionParser & cmd = OptionParser::get("seqc");
  cmd.register_option(utils::options::help, utils::options::markdown_help, verbose, db_path);
  cmd.register_option(input_fasta, input_clustalw, input_pir, input_ss2, input_asn1, input_chk);
  cmd.register_option(template_outfile, template_chk, template_asn1, template_ss2, template_pdb, template_name);
  cmd.register_option(query_outfile, query_chk, query_ss2, query_asn1, query_name);
  cmd.register_option(output_fasta, output_seq_width,output_fasta_secondary,out_profile_txt, sort_seq, output_ss2, out_profile_columns);
  cmd.register_option(sequence_identity, reduce_seq);
  cmd.register_option(select_sequence_by_name_from_file, select_sequence_by_name, select_sequence_by_length,
    select_chains, select_sequence_by_complexity, select_nucleic_sequence, select_protein_sequence, select_by_known_res_fraction);
  cmd.register_option(input_pdb, input_chainid_list, input_pdb_native, input_pdb_list, input_pdb_modelslist, input_pdb_path, input_pdb_header); // input PDB structures
  cmd.register_option(all_models,ca_only,bb_only,include_hydrogens,select_models,input_pdb_sort);

  core::index2 id = cmd.add_examples_group("Extract sequence (or secondary structure) from PDB format:");
  cmd.add_example(id,"read secondary structure information from a PDB file header and write in FASTA",
    "seqc -in:pdb=2gb1.pdb -out:fasta:secondary -out:fasta");

  cmd.add_example(id,"from PDB to SS2:",
    "seqc -in:pdb=2gb1.pdb -in::pdb::header -out:ss2=2gb1.ss2");

  id = cmd.add_examples_group("Transform FASTA file:");
  cmd.add_example(id,"Write FASTA file with only one line per sequence (don't wrap sequences)",
    "seqc -in:fasta=in.fasta -out:sequence:width=0 -out:fasta");

  cmd.add_example(id,"Sort sequences from the longest to the shortest",
    "seqc -in:fasta=in.fasta -seqc:sort -out:fasta");

  cmd.add_example(id,"Filter out sequences that are not proteins or are too short",
    "./seqc -if=5edw.fasta -select::sequence::long_at_least=10 -select::sequence::protein -out::fasta");

  cmd.program_info(seqc_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;
  if (output_fasta_secondary.was_used()) { // --- option hooks
    cmd.inject_option(input_pdb_header, "true");
    cmd.inject_option(output_fasta, "");
  }

  std::vector<std::string> strings_buffer;    // To hold strings after parsing

  // ********** PREPARE SEQUENCE FILTERS **********
  std::vector<std::shared_ptr<SequenceFilter>> filters = create_sequence_filters();

  // ********** INPUT SECTION **********
  std::vector<std::shared_ptr<Sequence>> input_sequences;
  std::vector<SecondaryStructure_SP> ss2;
  SequenceProfile_SP profile = nullptr;

  std::string reduced_alphabet_name = reduce_seq.was_used() ? option_value<std::string>(reduce_seq) : "";

  if(reduce_seq.was_used() && (!ReduceSequenceAlphabet::has_alphabet(reduced_alphabet_name))) {
    std::cerr << "Provide a valid name of reduced alphabets. The known alphabets are:\n";
    for(auto it = ReduceSequenceAlphabet::cbegin();it!=ReduceSequenceAlphabet::cend();++it) {
      std::cout << it->first<<"\n";
    }

    return 0;
  }

  // ---------- Read FASTA
  if (input_fasta.was_used()) read_fasta_file(option_value<std::string>(input_fasta), input_sequences);
  // ---------- Read PIR
  if (input_pir.was_used()) read_pir_file(option_value<std::string>(input_pir), input_sequences);
  // ---------- Read ClustalW
  if (input_clustalw.was_used()) read_clustalw_file(option_value<std::string>(input_clustalw), input_sequences);
  // ---------- Read SS2
  if (input_ss2.was_used()) {
    std::string fnames = option_value<std::string>(input_ss2);
    utils::split(fnames, strings_buffer, ',');
    for (const std::string & fname : strings_buffer)
      ss2.push_back(read_ss2(fname));
  }
  // ---------- Read CHK
  if (input_chk.was_used()) {
    profile = core::data::sequence::read_binary_checkpoint(option_value<std::string>(input_chk));
    input_sequences.push_back(std::static_pointer_cast<Sequence>(profile));
  }

  // ---------- Read ASN1
  if (input_asn1.was_used()) {
    profile = core::data::sequence::read_ASN1_checkpoint(option_value<std::string>(input_asn1));
    input_sequences.push_back(std::static_pointer_cast<Sequence>(profile));
  }

  // ---------- Load also PDB files, if requested
  std::vector<std::string> structure_names;
  std::vector<core::data::structural::Structure_SP> structures;
  utils::options::structures_from_cmdline(structure_names, structures);
  std::shared_ptr<core::data::structural::selectors::ChainSelector> requested_chain = nullptr;
  if (select_chains.was_used())
    requested_chain = std::make_shared<core::data::structural::selectors::ChainSelector>(option_value<std::string>(select_chains)[0]);
  for (auto str_it : structures)
    for (auto chain_it : *str_it) {
      if ((requested_chain == nullptr) || ((requested_chain != nullptr) && (*requested_chain)(*chain_it))) {
        if (input_pdb_header.was_used())
          ss2.push_back(chain_it->create_sequence());
        else
          input_sequences.push_back(chain_it->create_sequence());
      }
    }

  // ********** FILTER THE SEQUENCES IF REQUESTED **********
  if (filters.size() > 0) {
    for (const std::shared_ptr<SequenceFilter> filter_sp : filters) {
      input_sequences.erase(std::remove_if(input_sequences.begin(), input_sequences.end(),std::not1(std::ref(*filter_sp))),
          input_sequences.end());
    }
  }
  l << utils::LogLevel::FILE << input_sequences.size() << " sequences left after the filters were applied\n";

  // ********** Convert sequences to a reduced alphabet (of 8 or of 16 amino acid types) **********
  if (reduce_seq.was_used()) {
    const ReduceSequenceAlphabet &conv = *(ReduceSequenceAlphabet::get_alphabet(reduced_alphabet_name));
    for (core::index4 i=0;i<input_sequences.size();++i) {
      const Sequence & si = *(input_sequences[i]);
      std::string s = conv.encode_sequence(si.sequence);
      input_sequences[i] = std::make_shared<Sequence>(si.header(),s,si.first_pos());
    }
    for (core::index4 i=0;i<ss2.size();++i) {
      const SecondaryStructure & si = *(ss2[i]);
      std::string s = conv.encode_sequence(si.sequence);
      ss2[i] = std::make_shared<SecondaryStructure>(si.header(),s,si.first_pos(),si.str());
    }
  }

  // ********** OUTPUT SECTION **********
  core::index2 width = (output_seq_width.was_used()) ? option_value<core::index2>(output_seq_width) : 80;
  width = (width > 0) ? width : std::numeric_limits<core::index2>::max();

  if (sort_seq.was_used())
    std::sort(input_sequences.begin(), input_sequences.end(), [](Sequence_SP s1, Sequence_SP s2) { return s2->length() < s1->length(); });

  // ---------- Write sequence profile table
  if(out_profile_txt.was_used()) {
    if(profile!=nullptr) {
      if(out_profile_columns.was_used())
        profile = profile->create_reordered(option_value<std::string>(out_profile_columns));
      std::shared_ptr<std::ostream> out = utils::out_stream(option_value<std::string>(out_profile_txt));
      profile->write_table(*out);
    }
    else
      l<<utils::LogLevel::WARNING<<"Provide input sequence profile to use "<<out_profile_txt.name()<<" option!\n";
  }

  // ---------- Write FASTA
  if (output_fasta.was_used()) {
    std::shared_ptr<std::ostream> out = utils::out_stream(option_value<std::string>(output_fasta));
    for (const std::shared_ptr<Sequence> & s : input_sequences)
      *out << create_fasta_string(*s, width);
    for (SecondaryStructure_SP ss_i : ss2) {
      *out << create_fasta_string(*ss_i, width);
      if (output_fasta_secondary.was_used()) *out << create_fasta_secondary_string(*ss_i, width);
    }
    out->flush();
  }

  // ---------- Write SS2
  if (output_ss2.was_used()) {
    std::shared_ptr<std::ostream> out = utils::out_stream(option_value<std::string>(output_ss2));
    for (SecondaryStructure_SP ss_i : ss2) core::data::io::write_ss2(*ss_i,*out);
    out->flush();
  }

  // ---------- Create and write Target object
  if(query_outfile.was_used()) {

    core::data::sequence::SequenceProfile_SP prof = nullptr;
    std::pair<std::string, std::string> basename_ext;
    if(query_chk.was_used()) {
      prof = core::data::sequence::read_binary_checkpoint(option_value<std::string>(query_chk));
        basename_ext = utils::root_extension(utils::basename(option_value<std::string>(template_chk)));
    }
    if(query_asn1.was_used()) {
      prof = core::data::sequence::read_ASN1_checkpoint(option_value<std::string>(query_asn1));
        basename_ext = utils::root_extension(utils::basename(option_value<std::string>(template_chk)));
    }

    if ((prof!=nullptr) && (query_ss2.was_used())) {
      core::alignment::Target_SP q = std::make_shared<core::alignment::Target>(basename_ext.first,
        option_value<std::string>(query_chk),
        option_value<std::string>(query_ss2));
      std::shared_ptr<std::ostream> o_sp = utils::out_stream(option_value<std::string>(query_outfile));
      if(query_name.was_used())
        q->name = option_value<std::string>(query_name);

      *o_sp << *q;
    } else {
      l<<utils::LogLevel::SEVERE<<"Sequence profile and secondary structure profile are required to create a target object!\n";
    }
  }

  // ---------- Create and write Template object
  if(template_outfile.was_used()) {
    if (template_ss2.was_used() && template_pdb.was_used()) {
      core::data::sequence::SequenceProfile_SP prof = nullptr;

      std::pair<std::string, std::string> basename_ext;
      if(template_chk.was_used()) {
        prof = core::data::sequence::read_binary_checkpoint(option_value<std::string>(template_chk));
          basename_ext = utils::root_extension(utils::basename(option_value<std::string>(template_chk)));
      }
      if(template_asn1.was_used()) {
        prof = core::data::sequence::read_ASN1_checkpoint(option_value<std::string>(template_asn1));
          basename_ext = utils::root_extension(utils::basename(option_value<std::string>(template_chk)));
      }
      if(prof==nullptr)
        l<<utils::LogLevel::SEVERE<<"Sequence profile is required to create a template object!\n";
      else {
        core::data::io::Pdb reader(utils::options::option_value<std::string>(template_pdb));
        core::data::structural::Structure_SP structure = reader.create_structure(0);
        core::data::structural::Chain_SP resids;
        std::copy(structure->first_residue(), structure->last_residue(), std::back_inserter(*resids));
        core::alignment::Template_SP t = std::make_shared<core::alignment::Template>(basename_ext.first,
            prof, read_ss2(option_value<std::string>(template_ss2)),resids);
        if(template_name.was_used())
          t->name = option_value<std::string>(template_name);
        std::shared_ptr<std::ostream> o_sp = utils::out_stream(option_value<std::string>(template_outfile));
        *o_sp << *t;
      }
    }
  }

  // ********** CALCULATE SECTION **********
  if (sequence_identity.was_used()) {
    std::vector<core::index2> len;
    for (const std::shared_ptr<Sequence> & si : input_sequences)
      len.push_back(si->length_without_gaps());

    for (core::index4 i = 1; i < input_sequences.size(); ++i) {
      const std::shared_ptr<Sequence> & si = input_sequences[i];
      const double ni = (double) len[i];
      for (core::index4 j = 0; j < i; ++j) {
        const std::shared_ptr<Sequence> & sj = input_sequences[j];
        const double nj = (double) len[j];
        std::cout << si->header() << " " << sj->header() << " "
            << core::alignment::sum_identical(*si, *sj) * 2.0 / (ni + nj) << "\n";
      }
      l << utils::LogLevel::FINE << "computed sequence identity for " << si->header() << "\n";
    }
  }
}
