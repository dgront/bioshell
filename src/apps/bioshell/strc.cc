#include <cstdio>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <memory>

#include <core/index.hh>

#include <core/algorithms/predicates.hh>

#include <core/data/io/Pdb.hh>
#include <core/data/io/PdbField.hh>
#include <core/data/io/DataTable.hh>
#include <core/data/basic/Array2D.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <utils/io_utils.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <utils/options/select_options.hh>
#include <utils/options/selector_from_cmdline.hh>
#include <utils/options/calc_options.hh>

#include <utils/Logger.hh>
#include <utils/LogManager.hh>

#include <core/protocols/selection_protocols.hh>

const std::string strc_info = R"(
      Utility for handling PDB files. Can extract a given fragment, reorder residues, etc.)";

utils::Logger l("strc");
typedef core::data::structural::Structure::atom_iterator atom_iterator;

int main(int argc, const char* argv[]) {
    //utils::LogManager::FINEST();

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::structural;

  utils::options::Option split_chains("-strc:split_chains", "-strc::split_chains",
      "save each chain of a PDB file into a separate PDB file");

  utils::options::Option remove_ligands("-strc:remove_ligands", "-strc::remove_ligands",
                                      "remove all ligands from a PDB deposit");

  utils::options::Option renumber_pdb("-strc:renumber", "-strc:renumber", "renumber atoms and residues in each output structure");

  utils::options::OptionParser & cmd = OptionParser::get("strc");
  cmd.register_option(utils::options::help, utils::options::markdown_help);
  cmd.register_option(verbose, db_path);
  // ---------- input options
  cmd.register_option(input_pdb, input_pdb_list, input_pdb_path, input_pdb_header); // input PDB structures
  cmd.register_option(all_models, ca_only, bb_only, include_hydrogens, input_pdb_sort, keep_alternate);

  // ---------- selecting options
  cmd.register_option(select_models, select_substructure, select_aa, select_nt, select_chains);
  cmd.register_option(select_ca, select_bb, select_bb_cb, select_cb, select_atoms_by_name);

  // ---------- do some stuff
  cmd.register_option(orient_structure, remove_ligands);
  // ---------- output options
  cmd.register_option(output_pdb, output_pdb_header, split_chains, renumber_pdb, output_name_prefix);
  cmd.program_info(strc_info);

  core::index2 id = cmd.add_examples_group("Write selected chains / residues / atoms");
  cmd.add_example(id,"write only chain A",
    "strc -in:pdb=5edw.pdb -select::chains=A -out:pdb=5edwA.pdb");
  cmd.add_example(id,"write only aminoacids of chain A (ligands, water etc will be removed",
    "strc -in:pdb=5edw.pdb -select::chains=A -out:pdb=5edwA.pdb -select::aa");

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // ********** INPUT SECTION **********
  std::vector<core::data::structural::Structure_SP> structures;
  std::vector<std::string> structure_ids;
  utils::options::structures_from_cmdline(structure_ids, structures);

  // ********** SELECTIONS **********
  std::vector<std::shared_ptr<core::data::structural::selectors::AtomSelector>> atom_selectors
      = atoms_selection_from_cmdline();
  if (!atom_selectors.empty()) {
    selectors::LogicalANDSelector select_all(atom_selectors);
    for (core::data::structural::Structure_SP m : structures)
      core::protocols::keep_selected_atoms(select_all, *m);
  }

  // ---------- re-orient structures
  if (orient_structure.was_used()) {
    for (core::data::structural::Structure_SP m : structures) {
      core::data::basic::Vec3 cm;
      double n=0;
      for (auto it = m->first_atom(); it != m->last_atom(); it++) {
        cm += *(*it);
        ++n;
      }
      cm /= n;
      for (auto it = m->first_atom(); it != m->last_atom(); it++)
        (*(*it))-=cm;
    }
  }

  // ---------- remove ligands
  if (remove_ligands.was_used()) {
    for (core::data::structural::Structure_SP m : structures) {
      for(Chain_SP ci : *m) ci->remove_ligands();
    }
  }

  // ********** OUTPUT SECTION **********
  // ---------- Renumber atoms and residues
  if (renumber_pdb.was_used()) {
    core::index2 i_resid = 0;
    for (core::data::structural::Structure_SP m : structures) {
      for (Chain_SP ci : *m)
        std::for_each(ci->begin(), ci->end(), [&](Residue_SP e) {(e)->id(++i_resid);});
      core::index4 i_atom = 0;
      std::for_each(m->first_atom(), m->last_atom(), [&](PdbAtom_SP e) {(e)->id(++i_atom);});
    }
  }

// ---------- Write PDB
  if (split_chains.was_used()) {
    std::string out_fname;
    std::shared_ptr<std::ostream> out;
    if (utils::options::output_name_prefix.was_used())
      out_fname = utils::options::option_value<std::string>(utils::options::output_name_prefix);

    size_t iname = 0;
    for (core::data::structural::Structure_SP m : structures) {
      std::string fn = out_fname + structure_ids[iname];
      std::pair<std::string, std::string> re = utils::root_extension(fn);
      for (Chain_SP ci : *m) {
        out = utils::out_stream(re.first + ci->id() + "." + re.second);
        for (auto it = ci->first_atom(); it != ci->last_atom(); it++)
          *out << (*it)->to_pdb_line() << "\n";
      }
      ++iname;
    }
    return 0;
  }

  if (output_pdb.was_used()) {
    std::shared_ptr<std::ostream> out;
    if (utils::options::output_name_prefix.was_used()) {
      std::string outfname = utils::options::option_value<std::string>(utils::options::output_name_prefix)
          + utils::options::option_value<std::string>(output_pdb);
      out = utils::out_stream(outfname);
    } else out = utils::out_stream(utils::options::option_value<std::string>(output_pdb));
    if(output_pdb_header.was_used()) {
      for(const auto & fi:structures[0]->pdb_header) {
          if (fi.first!="CONECT") {
              *out << fi.second->to_pdb_line() << "\n";
          }
      }
    }
    if (structures.size() > 1) {
      for (core::index4 i = 0; i < structures.size(); ++i) {
        *out << utils::string_format("MODEL   %6d\n", i + 1);
        for (auto it = structures[i]->first_atom(); it != structures[i]->last_atom(); it++)
          *out << (*it)->to_pdb_line() << "\n";
        *out << "ENDMDL\n";
      }
    } else
      for (auto it = structures[0]->first_atom(); it != structures[0]->last_atom(); it++)
        *out << (*it)->to_pdb_line() << "\n";

  }
}
