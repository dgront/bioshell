#include <numeric>
#include <apps/bioshell/bbf_utils.cc>

int main(const int argc, const char* argv[]){

	// |||||||||||||||||||||||||||||| HOW TO USE ||||||||||||||||||||||||||||||
	// || 	./bbf  -bbf_test:db_in_folder=<path>/(-bbf_test:db_dist=<path>	 ||
	// ||									   -bbf_test:db_full=<path>)	 ||
	// ||		   [-bbf_test:subset]	[-bbf_test:fragment_len=5]			 ||
	// ||		   [-bbf_test:query_in_folder=<path>]/subset				 ||
	// ||			-bbf_test:results_folder=<path>							 ||
	// ||			-bbf_test:tmp_folder=<path> 							 ||
	// ||	[-bbf_test:dont_delete_tmp_db] [-bbf_test:write_str]			 ||
	// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
	// |||||| parameters for build_whole_structure_test function: |||||||||||||
	// ||	[-bbf_test:number_of_fragments=100] [-bbf_test:tm1=1]			 ||
	// ||	[-bbf_test:tm2=1] [-bbf_test:nothing_stay]						 ||
	// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

	// OPTIONS PARSING
	Option db_in_folder("-bbf_test:db_in_folder", "-bbf_test:db_in_folder", "folder with structures destined for fragments database"); // is any better way to do this? can't be combined with db_dist/db_full
	Option db_dist("-bbf_test:db_dist", "-bbf_test:db_dist", "data base file with distances"); // is any better way to do this? can't be combined with db_in_folder
	Option db_full("-bbf_test:db_full", "-bbf_test:db_full", "data base file with full fragments"); // is any better way to do this? can't be combined with db_in_folder
	Option subset("-bbf_test:subset", "-bbf_test:subset", "file with adresses files to build database");
	Option length_of_fragment("-bbf_test:length_of_fragment", "-bbf_test:length_of_fragment", "length of fragments");
	Option query_in_folder("-bbf_test:query_in_folder", "-bbf_test:query_in_folder", "folder with structures destined for being queries"); // is any better way to do this?
	Option results_folder("-bbf_test:results_folder", "-bbf_test:results_folder", "folder for results");
	Option tmp_folder("-bbf_test:tmp_folder", "-bbf_test:tmp_folder", "folder for temporary files (Few GB of space is needed (it depends on test magnitude))");
	Option dont_delete_tmp_db("-bbf_test:dont_delete_tmp_db", "-bbf_test:dont_delete_tmp_db", "If selected database files from tmp_out folder will be maintained");
	Option write_str("-bbf_test:write_str", "-bbf_test:write_str", "If selected builded structures will be writed in included path");

	Option number_of_fragments("-bbf_test:number_of_fragments", "-bbf_test:number_of_fragments", "number of fragments");
	Option tm1("-bbf_test:tm1", "-bbf_test:tm1", "first trimming mode argument");
	Option tm2("-bbf_test:tm2", "-bbf_test:tm2", "second trimming mode argument");
	Option nothing_stay("-bbf_test:nothing_stay", "-bbf_test:nothing_stay", "stay nothing from query structure");

	OptionParser & cmd = OptionParser::get();

	cmd.register_option(help);
	cmd.register_option(verbose, db_path);
	cmd.register_option(db_in_folder, db_dist, db_full, length_of_fragment);
	cmd.register_option(query_in_folder, results_folder);
	cmd.register_option(tmp_folder, dont_delete_tmp_db, write_str);
	cmd.register_option(number_of_fragments, tm1, tm2, nothing_stay);
	cmd.register_option(subset);

	if (!cmd.parse_cmdline(argc, argv))	return 1;

	if ( db_in_folder.was_used() && (db_dist.was_used()||db_full.was_used()) ) {
		logger<<utils::LogLevel::CRITICAL<<"db_in_folder option can't be combined with db_dist/db_full\n";
		return -1;
	}
	std::string db_in;
	std::string db_dist_file;
	std::string db_full_file;
	if ( db_in_folder.was_used() ){db_in = option_value<std::string>(db_in_folder);}
	else{
		if (!( db_dist.was_used() && db_full.was_used() )){
			logger<<utils::LogLevel::CRITICAL<<"db_dist must be combined with db_full\n";
			return -1;
		}
		db_dist_file = option_value<std::string>(db_dist);
		db_full_file = option_value<std::string>(db_full);
	}
	std::string wrt_str;
	if ( write_str.was_used() ){wrt_str = option_value<std::string>(write_str);}
	int fragment_len;
	if (db_in_folder.was_used() ){
		if (length_of_fragment.was_used()){
			fragment_len = option_value<int>(length_of_fragment);
		}
		else{fragment_len=5;}
	}
	std::string results_out = option_value<std::string>(results_folder);
	std::string tmp_out = option_value<std::string>(tmp_folder); // dont_delete_tmp_db.was_used()
	unsigned n_of_fragments = 100;
	if (number_of_fragments.was_used()){
		n_of_fragments = option_value<unsigned>(number_of_fragments);
	}
	unsigned t1 = 1;
	if (tm1.was_used()){
		t1 = option_value<unsigned>(tm1);
	}
	unsigned t2 = 1;
	if (tm2.was_used()){
		t2 = option_value<unsigned>(tm2);
	}
	int what_stay;
	nothing_stay.was_used() ? what_stay = -1 : what_stay = 0;

	// DATABASE (BUILDING AND) LOADING
	std::shared_ptr<DatabaseKeeper> database;
	if ( db_in_folder.was_used() ){
		build_database(db_in, tmp_out, fragment_len);
		database = std::make_shared<DatabaseKeeper>(tmp_out+std::to_string(fragment_len)+"_db_dist", tmp_out+std::to_string(fragment_len)+"_full_database");
	}
	else{
		database = std::make_shared<DatabaseKeeper>(db_dist_file, db_full_file);
		fragment_len = (*database).get_length();
	}
	if ((int)(t1+t2)>=fragment_len || (int)(t1+t2)<0) {
		logger<<utils::LogLevel::CRITICAL<<"Invalid trimming_mode arguments\n";
		return -1;
	}
	std::vector<bbf_test_results> all_results;
	std::vector<std::string> all_names;

	// QUERIES & REFERENCES LOADING AND BUILDING WITH TEST
	std::shared_ptr<std::set<std::string>> files = std::make_shared<std::set<std::string>>();
	if (subset.was_used()){subsetFiles(option_value<std::string>(subset),files);}
	else if (query_in_folder.was_used()){listFiles(option_value<std::string>(query_in_folder),files);}
	else {return -1;}

	for (const std::string & file_name : *files){
		try{
			core::data::io::Pdb reader_ref(file_name, core::data::io::all_true(core::data::io::is_standard_atom, core::data::io::is_bb));
			Structure_SP strctr_ref = reader_ref.create_structure(0);
			if(! bb_is_complete(strctr_ref)){
				logger<<utils::LogLevel::WARNING<<"Invalid query "<<file_name<<"\n";
				continue;
			}
			core::data::io::Pdb reader(file_name, core::data::io::all_true(core::data::io::is_standard_atom, core::data::io::is_bb));
			Structure_SP strctr = reader.create_structure(0);
			bbf_test_results results = build_whole_structure_test(database, strctr_ref, strctr, n_of_fragments, t1, t2, what_stay, true);
			all_results.push_back(results);
			all_names.push_back(file_name);
			if (write_str.was_used()){
				std::ofstream myfile;
				std::string name_ex = (file_name.substr(file_name.rfind("/"))); 	// this can be done better
				name_ex = name_ex.substr(1,name_ex.find(".")); 						// this can be done better
				myfile.open(wrt_str + "_" + name_ex + ".pdb"); // good???
				core::data::io::write_pdb(strctr, myfile);
				myfile.close();
			}
		}
		catch(...){
			logger<<utils::LogLevel::WARNING<<"Invalid query "<<file_name<<"\n";
			return -1;
		}
	}

	// ERASING TEMPORARY FILES
	if (!dont_delete_tmp_db.was_used() && db_in_folder.was_used()){
		const char *dbdist = db_dist_file.c_str();
		std::remove(dbdist);
		const char *dbfull = db_full_file.c_str();
		std::remove(dbfull);
	}

	// WRITING CSV WITH RESULTS <--- CHANGE IT, IF YOU NEED ANOTHER CALCULATIONS !!! <--- CHANGE IT, IF YOU NEED ANOTHER CALCULATIONS !!! <--- CHANGE IT, IF YOU NEED ANOTHER CALCULATIONS !!!
	std::ofstream results_file(results_out+"results_"+std::to_string(fragment_len)+"_"+std::to_string(n_of_fragments)+"_"+
							   std::to_string(t1)+"_"+std::to_string(t2)+"_"+std::to_string(what_stay)+".csv");
	for (unsigned i=0; i<all_results.size(); i++){
		// calculating mean and std dev - is any better way to do this??
		float mean_phis;
		mean_phis = std::accumulate(all_results[i].phis.begin(),all_results[i].phis.end(),0.0);
		mean_phis /= all_results[i].phis.size();
		float mean_psis;
		mean_psis = std::accumulate(all_results[i].psis.begin(),all_results[i].psis.end(),0.0);
		mean_psis /= all_results[i].psis.size();
		float std_dev_phis;
		std_dev_phis = std_dev(all_results[i].phis, mean_phis);
		float std_dev_psis;
		std_dev_psis = std_dev(all_results[i].psis, mean_psis);
		// end of calculations
		results_file<<all_names[i]<<";"<<std::to_string(all_results[i].phis.size()+1)<<";"<<std::to_string(all_results[i].crmsd)<<";"
								   <<std::to_string(mean_phis)<<";"<<std::to_string(mean_psis)<<";"<<std::to_string(std_dev_phis)<<";"<<std::to_string(std_dev_psis)<<std::endl;
	}
	results_file.close();

	logger<<utils::LogLevel::FINEST<<"Done\n";

	return 0;
}
