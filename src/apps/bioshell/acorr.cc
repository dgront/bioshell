#include <numeric>
#include <iostream>

#include <core/index.hh>

#include <core/data/io/DataTable.hh>
#include <core/calc/statistics/simple_statistics.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>

#include <utils/Logger.hh>
#include <core/calc/statistics/OnlineStatistics.hh>

utils::Logger l("acorr");

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;

  const std::string acorr_info = R"(
Calculates autocorrelation from a time-series data.)";

  utils::options::Option displacement("-max", "-acorr:displacement", "calculates average displacement as a function of lag time");
  utils::options::Option lag_max("-max", "-acorr:t_max", "set maximum range for the autocorrelation time (lag time)");
  utils::options::Option real_time("-t", "-acorr:real_time", "provide real time (wall-clock time) so autocorrelation time will be expressed int in seconds rather than MC-steps");

  utils::options::OptionParser & cmd = OptionParser::get("acorr");
  cmd.register_option(utils::options::help, utils::options::markdown_help);
  cmd.register_option(verbose, db_path);
  // ---------- input options
  cmd.register_option(input_file, data_column, data_columns);

  // ---------- output-related options
  cmd.register_option(lag_max, real_time, displacement);

  // ---------- output options
  cmd.program_info(acorr_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // ********** INPUT SECTION **********
  DataTable dt;
  dt.load(option_value<std::string>(input_file));
  std::vector<double> out;
  core::index2 x_max;
  std::vector<core::index2> icols;

  if(displacement.was_used()) {
    option_value<core::index2>(displacement, icols);
    l << "Average displacement computed for x,y,z columns: " << icols[0] << " " << icols[1] << " " << icols[2] << "\n";
    for (core::index2 i = 0; i < icols.size(); ++i) --icols[i];

    std::vector<core::data::basic::Vec3> cm;
    for (const TableRow & tr:dt)
      cm.emplace_back(tr.get<double>(icols[0]),tr.get<double>(icols[1]),tr.get<double>(icols[2]));

    core::calc::statistics::average_displacement(cm, cm.size() / 10, out);
  }

  if(data_columns.was_used()) {
    option_value<core::index2>(data_columns, icols);
    for (core::index2 i = 0; i < icols.size(); ++i) --icols[i];

    std::vector<std::vector<double>> data;
    for (const TableRow & tr:dt) {
      std::vector<double> row;
      for (core::index2 i_col:icols) row.push_back(tr.get<double>(i_col));
      data.push_back(row);
    }
    x_max = option_value<core::index2>(lag_max, data.size() / 10);
    core::calc::statistics::autocorrelate(data, x_max, out);
  }
  if(data_column.was_used()) {
    core::index2 icol = option_value<core::index2>(data_column) - 1;
    std::vector<double> data;
    std::for_each(dt.begin(), dt.end(), [&data, icol](const TableRow &row) { data.push_back(row.get<double>(icol)); });
    x_max = option_value<core::index2>(lag_max, data.size() / 10);
    core::calc::statistics::autocorrelate(data.begin(), data.end(), x_max, out);
  }

  std::vector<double> x(out.size());
  if(real_time.was_used()) {
    DataTable dt;
    dt.load(option_value<std::string>(real_time));
    std::vector<double> time;
    dt.column(0,time);

    for (core::index2 t_lag = 0; t_lag < x.size(); ++t_lag) {
      core::calc::statistics::OnlineStatistics stats;
      core::index4 i_first=0;
      for (core::index4 i_last = i_first + t_lag; i_last < time.size(); ++i_last) {
        stats(time[i_last]-time[i_first]);
        ++i_first;
      }
      x[t_lag] = stats.avg();
    }
  } else {
    for (core::index4 k = 0; k < x.size(); k++) x[k] = k;
  }

  std::vector<double> y(out.size());
  for (core::index4 k = 0; k < x.size(); k++) y[k] = log(out[k]);

  y[0] = 0.0;

  std::pair<double, double> a_b = core::calc::statistics::regression_line(x, y, 0, x.size() / 2);
  std::cout << "# Linear regression log(y) = a*x+b : " << std::get<0>(a_b) << " " << std::get<1>(a_b) << "\n";
  std::cout << "# autocorrelation time : " << -1.0 / std::get<0>(a_b) << "\n";

  for (core::index4 i = 0; i < out.size(); ++i) std::cout <<x[i] << " " << out[i] << " " << log(out[i]) << "\n";

  return 0;
}
