#include <iostream>
#include <algorithm>

#include <core/index.hh>

#include <core/data/io/DataTable.hh>
#include <core/calc/statistics/Histogram.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>

#include <utils/Logger.hh>

utils::Logger l("hist");

const std::string hist_info = R"(
      Calculates a histogram of one-dimensional. Application reads a file with multiple columns, the relevant column
must be selected by a command line option.)";

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;

  utils::options::Option bin_width("-w", "-hist:bin_width", "set bin width","0.1");
  utils::options::Option hist_min("-min", "-hist:x_min", "set minimum range for the histogram");
  utils::options::Option hist_max("-max", "-hist:x_max", "set maximum range for the histogram");

  utils::options::OptionParser & cmd = OptionParser::get("hist");
  cmd.register_option(utils::options::help, utils::options::markdown_help);
  cmd.register_option(verbose, db_path);
  // ---------- input options
  cmd.register_option(input_file, data_column);

  // ---------- histogram-related options
  cmd.register_option(hist_min, hist_max, bin_width);

  // ---------- output options
//  cmd.register_option();
  cmd.program_info(hist_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // ********** INPUT SECTION **********
  DataTable dt;
  dt.load(option_value<std::string>(input_file));

  core::index2 icol = option_value<core::index2>(data_column);
  icol = (icol <= 0) ? 0 : icol - 1;
  std::vector<double> data;
  std::for_each(dt.begin(), dt.end(), [&data,icol](const TableRow & row) {data.push_back(row.get<double>(icol));});
  double x_min = option_value<double>(hist_min, *std::min_element(data.begin(), data.end()));
  double x_max = option_value<double>(hist_max, *std::max_element(data.begin(), data.end()));
  double width = option_value<double>(bin_width);

  core::calc::statistics::Histogram<double, core::index4> h(width, x_min, x_max);
  h.output_counts_format("%d");
  h.insert(data);
  std::cout << h;

  return 0;
}
