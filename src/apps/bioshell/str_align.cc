#include <cstdio>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <memory>
#include <algorithm>
#include <type_traits>

#include <core/algorithms/predicates.hh>

#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <core/alignment/TMAlign.hh>

#include <core/protocols/StructureAlignmentProtocol.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <utils/options/select_options.hh>
#include <utils/options/selector_from_cmdline.hh>

#include <utils/Logger.hh>
#include <utils/ThreadPool.hh>
#include <utils/io_utils.hh>

#include <core/alignment/scoring/AlignmentTMScoring.hh>

const std::string str_align_info = R"(Structure alignment tool.)";

utils::Logger l("str_align");

int main(int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace utils::options;
  using namespace core::data::basic;
  using namespace core::data::structural;
  using namespace core::calc::structural::transformations;

  utils::options::Option tabular_output("-str_align:out:tabular", "-str_align:out:tabular", "turn on tabular output instead of the default tm-score style");
//  utils::options::Option alignment_output("-str_align:out:alignment", "-str_align:out:alignment", "output alignment in tm_score - like format");

  utils::options::OptionParser & cmd = OptionParser::get("str_align");
  cmd.register_option(utils::options::help, utils::options::markdown_help, show_examples, verbose, db_path, n_threads);
  cmd.register_option(input_pdb, input_pdb_native, input_pdb_list, input_pdb_path, input_pdb_header); // input PDB structures
  cmd.register_option(input_alipath);
  cmd.register_option(all_models, ca_only, bb_only, include_hydrogens, select_models, input_pdb_sort);
  cmd.register_option(select_ca, select_models, keep_alternate, bb_only);
  // ---------- output options
  cmd.register_option(output_pdb, tabular_output, output_fasta);

  core::index2 id = cmd.add_examples_group("Structure alignment examples:");
  cmd.add_example(id,"find all pairwise crmsd distances between all the models in a given PDB",
      "str_calc -in:pdb=2KMK.pdb.gz -calc::crmsd -in:pdb::all_models -in:pdb:native=2kmk-1.pdb");

  cmd.program_info(str_align_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  l << utils::LogLevel::INFO << "The command line was:\n\t" << cmd.build_cmdline()<<"\n\n";
  selectors::AtomSelector_SP select_ca_only = std::make_shared<core::data::structural::selectors::IsCA>();

  // ********** INPUT SECTION **********
  // ---------- Query coordinates - data structures
  std::vector<core::data::structural::Structure_SP> structures; // --- full structures, as they are in PDB
  std::vector<std::string> structure_ids; // --- string ID for each structure
  // --- read query structures
  utils::options::structures_from_cmdline(structure_ids, structures);

  // ---------- Native coordinates
  Structure_SP native = nullptr;
  std::string native_fn("");
  int which_native = -1;
  if (input_pdb_native.was_used()) {
    native = utils::options::native_from_cmdline();
    native_fn = option_value<std::string>(input_pdb_native);
    structure_ids.push_back(native_fn);
    which_native = structures.size();
    structures.push_back(native);
  }

  // ---------- Input alignment as a path
  core::alignment::PairwiseAlignment_SP in_ali = nullptr;
  if(input_alipath.was_used()) {
    in_ali = std::make_shared<core::alignment::PairwiseAlignment>(option_value<std::string>(input_alipath));
  }

  // ********** CALCULATE SECTION **********
  if (in_ali != nullptr) {
    if(native==nullptr) {
      l<<utils::LogLevel::WARNING<<"To evaluate a user-provided alignment, both a single query structure and the reference structure must be provided!\n";
      return 0;
    }
    if(structures.size()>1)
    l<<utils::LogLevel::WARNING<<"Only the first user-provided query structure is used to assess the user-provided alignment\n";

    return 0;
  }

  // ---------- CALCULATE STRUCTURAL ALIGNMENTS ----------
  core::index2 n_thread = option_value<core::index2>(n_threads);
  core::protocols::StructureAlignmentProtocol aligner(n_thread);

  for (core::index2 i = 0; i < structure_ids.size(); ++i) aligner.add_input_structure(structures[i], structure_ids[i]);
  if (which_native < 0) aligner.align();
  else aligner.align(core::index2(which_native));

  if (tabular_output.was_used()) {
    aligner.print_alignment_report_tabular(std::cout);
  }
  else aligner.print_alignment_report(std::cout);

  // ---------- PDB OUTPUT ----------
  if(output_pdb.was_used()) aligner.write_aligned_structures();

  // ---------- ALIGNMENT OUTPUT AS SEQUENCES ----------
  if(output_fasta.was_used()) {
    auto out = utils::out_stream(utils::options::option_value<std::string>(output_fasta, "stdout"));
    aligner.print_alignment_fasta(*out);
  }
}
