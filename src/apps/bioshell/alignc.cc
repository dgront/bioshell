#include <cstdio>
#include <ctime>
#include <iostream>
#include <memory>

#include <core/BioShellEnvironment.hh>

#include <core/data/io/Pdb.hh>
#include <core/data/io/pir_io.hh>
#include <core/data/io/fasta_io.hh>
#include <core/data/io/alignment_io.hh>
#include <core/data/io/clustalw_io.hh>
#include <core/data/sequence/SequenceProfile.hh>

#include <core/alignment/Target.hh>
#include <core/alignment/Template.hh>
#include <core/alignment/on_alignment_computations.hh>
#include <core/alignment/PairwiseAlignment.hh>
#include <core/alignment/PairwiseSequenceAlignment.hh>
#include <core/alignment/NWAligner.hh>
#include <core/alignment/AlignmentRow.hh>
#include <core/alignment/AlignmentBlock.hh>
#include <core/alignment/scoring/SimilarityMatrix.hh>
#include <core/alignment/scoring/Picasso3.hh>
#include <core/alignment/scoring/AlignmentConstraints.hh>
#include <core/alignment/scoring/AlignmentScoreBase.hh>
#include <core/alignment/scoring/SecondarySimilarity.hh>
#include <core/alignment/scoring/CombinedScore.hh>
#include <core/alignment/scoring/SimilarityMatrixScore.hh>
#include <core/alignment/scoring/NcbiSimilarityMatrixFactory.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/align_options.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/calc_options.hh>
#include <utils/Logger.hh>
#include <utils/io_utils.hh>

utils::Logger alignc_logger("alignc");

short int gap_open_fasta = -10;
short int gap_extn_fasta = -2;
double gap_open_profile = -1.3;
double gap_extn_profile = -0.1;
double gap_open_t1d = -1.5;
double gap_extn_t1d = -0.3;

const std::string alignc_info = R"(
      Calculates an alignment between two given sequences or sequence profiles. The program can also
convert between sequence alignment formats. Finally, it may be used to prepare .target and .tmplt files
used as an input by BioShell threading programs.)";


int main(const int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace core::alignment;
  using namespace utils::options;
  using namespace core::alignment::scoring;

  typedef core::alignment::scoring::SimilarityMatrixScore<short int> SS_MatrixScore;

  utils::options::OptionParser & cmd = OptionParser::get("alignc");
  cmd.register_option(utils::options::help, utils::options::markdown_help, verbose, utils::options::db_path);
  cmd.register_option(input_alignment_fasta, input_alignment_pir, input_native_fasta,input_alipath,input_chk,input_alignment_edinburgh);
  cmd.register_option(output_fasta, output_blocks, output_seq_width,output_gaps,output_alipath);
  cmd.register_option(calc_aln_k,sequence_identity, substitution_matrix);
  cmd.register_option(query_infile, template_infile);

  // ---------- Alignment-related options
  cmd.register_option(align_global,align_local,align_semiglobal,gap_extend,gap_open);
  cmd.register_option(query_fasta,query_chk, query_fasta_string);
  cmd.register_option(template_fasta, template_chk, template_fasta_string);

  core::index2 id = cmd.add_examples_group("Calculate sequence alignment:");
  cmd.add_example(id,"Align a target - template pair (aka threading 1D); template and target files may be prepared with seqc command",
      "alignc -in:template=2kmk.tmplt -in::query=2aza.query");
  cmd.add_example(id,"Align sequences in FASTA format (two FASTA files): ",
    "alignc -in:template:fasta:string=GYHFCGGSLINSQWVVSAAHCY -in::query:fasta:string=QRHLCGGSIIGNWILTAAHCF -out::fasta");
  cmd.program_info(alignc_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // ********** DATA PLACEHOLDERS **********
  std::vector<std::shared_ptr<Sequence>> input_sequences;   ///< Input sequences read from a FASTA file
  std::shared_ptr<PairwiseAlignment> reference_alignment;   ///< Alignment used as a reference in calculations (e.g. when comparing the agreement between two alignments)
  std::vector<PairwiseAlignment> alignments;                ///< All the input alignments
  std::vector<PairwiseSequenceAlignment_SP> seq_alignments;    ///< Sequence alignments

  // ********** INPUT ALIGNMENT SECTION **********
  // ---------- Read FASTA
  if (input_alignment_fasta.was_used()) read_fasta_file(option_value<std::string>(input_alignment_fasta), input_sequences);

  // ---------- Read PIR
  if (input_alignment_pir.was_used()) read_pir_file(option_value<std::string>(input_alignment_pir), input_sequences);

  if(input_alignment_edinburgh.was_used()) {
    PairwiseSequenceAlignment_SP ali = read_edinburgh(option_value<std::string>(input_alignment_edinburgh));
    seq_alignments.push_back(ali);
    alignments.push_back(*seq_alignments.back()->alignment);
  }

  // ---------- Input alignment as a path
  if(input_alipath.was_used()) {
    std::string alignment_path = option_value<std::string>(input_alipath);
    alignments.push_back( PairwiseAlignment(alignment_path) );
  }

  // ---------- Read the reference alignment in FASTA format - if given
  if (input_native_fasta.was_used()) {
	std::vector<std::shared_ptr<Sequence>> ref_sequences;
    read_fasta_file(option_value<std::string>(input_native_fasta), ref_sequences);
    if (ref_sequences.size() <2) {
        alignc_logger << utils::LogLevel::WARNING
                  << "The reference FASTA file holds only a single sequence; reference alignment cannot be created!\n";
    } else {
    	reference_alignment = std::make_shared<PairwiseAlignment>(
    		ref_sequences[0]->sequence, ref_sequences[0]->first_pos(),
    		ref_sequences[1]->sequence, ref_sequences[1]->first_pos(),0.0);
    }
  }

  // ---------- There must be even number of sequences to merge them into pairwise sequence alignments
  if (input_sequences.size() % 2 == 1) {
    alignc_logger << utils::LogLevel::WARNING
                  << "odd number of sequences obtained from an input file; the last one will be neglected\n";
    input_sequences.pop_back();
  }
  for (size_t i = 0; i < input_sequences.size(); i += 2) {
    const std::shared_ptr<Sequence> & s1 = input_sequences[i];
    const std::shared_ptr<Sequence> & s2 = input_sequences[i + 1];
    alignments.push_back(
      PairwiseAlignment(s1->sequence, s1->first_pos(), s2->sequence, s2->first_pos(),0.0));
  }

  // ********** CALCULATE ALIGNMENT SECTION **********
  core::data::sequence::Sequence_SP q_seq,t_seq;
  if (query_fasta.was_used() && template_fasta.was_used()) {
    read_fasta_file(option_value<std::string>(query_fasta), input_sequences);
    q_seq = input_sequences[0];
    input_sequences.clear();
    read_fasta_file(option_value<std::string>(template_fasta), input_sequences);
    t_seq = input_sequences[0];
    input_sequences.clear();
    input_sequences.push_back(q_seq);
    input_sequences.push_back(t_seq);
  }

  if (query_fasta_string.was_used() && template_fasta_string.was_used()) {
    q_seq = std::make_shared<Sequence>("query", option_value<std::string>(query_fasta_string), 1);
    t_seq = std::make_shared<Sequence>("template", option_value<std::string>(template_fasta_string), 1);
    input_sequences.push_back(q_seq);
    input_sequences.push_back(t_seq);
  }

  if (q_seq != nullptr && t_seq != nullptr) {
    short int open = (gap_open.was_used()) ? option_value<short int>(gap_open) : gap_open_fasta;
    short int cont = (gap_extend.was_used()) ? option_value<short int>(gap_extend) : gap_extn_fasta;

    std::string matrix_name = option_value<std::string>(substitution_matrix);
    std::shared_ptr<SimilarityMatrix<short>> b62_matrix = NcbiSimilarityMatrixFactory::get().load_matrix(matrix_name);

    const SS_MatrixScore b62_score(q_seq->sequence, t_seq->sequence, *b62_matrix);
    alignc_logger << utils::LogLevel::INFO<<"global alignment of two FASTA sequences with gap parameters: "<<open<<", "<<cont<<"\n";
    core::alignment::NWAligner<short int,SS_MatrixScore> global(std::max(q_seq->length(), t_seq->length()));
    global.align(open, cont, b62_score);
    PairwiseAlignment ali(*global.backtrace());
    alignments.push_back(ali);
  }

  if (query_chk.was_used() && template_chk.was_used()) {
    double open = (gap_open.was_used()) ? option_value<double>(gap_open) : gap_open_profile;
    double cont = (gap_extend.was_used()) ? option_value<double>(gap_extend) : gap_extn_profile;
    core::data::sequence::SequenceProfile_SP q_prof = core::data::sequence::read_binary_checkpoint(
      option_value<std::string>(query_chk));
    core::data::sequence::SequenceProfile_SP t_prof = core::data::sequence::read_binary_checkpoint(
      option_value<std::string>(template_chk));
    input_sequences.push_back(std::static_pointer_cast<core::data::sequence::Sequence>(q_prof));
    input_sequences.push_back(std::static_pointer_cast<core::data::sequence::Sequence>(t_prof));

    const core::alignment::scoring::Picasso3 picasso(q_prof,t_prof);
    alignc_logger << utils::LogLevel::INFO<<"global alignment of two blast profiles with gap parameters: "<<open<<", "<<cont<<"\n";
    core::alignment::NWAligner<double,Picasso3> global(std::max(q_prof->length(), t_prof->length()));
    global.align(open, cont, picasso);
    PairwiseAlignment ali(*global.backtrace());
    alignments.push_back(ali);
  }

  if (query_infile.was_used() && template_infile.was_used()) {
    double open = (gap_open.was_used()) ? option_value<double>(gap_open) : gap_open_t1d;
    double cont = (gap_extend.was_used()) ? option_value<double>(gap_extend) : gap_extn_t1d;
    core::alignment::Target_SP q = core::alignment::Target::from_file(option_value<std::string>(query_infile));

    if (q == nullptr) {
      alignc_logger << utils::LogLevel::SEVERE << "Can't load target (query) data - file "
          << option_value<std::string>(query_infile) << "does not exist or it is empty!";
      return 0;
    }
    core::alignment::Template_SP t = core::alignment::Template::from_file(option_value<std::string>(template_infile));
    if (t == nullptr) {
      alignc_logger << utils::LogLevel::SEVERE << "Can't load template data - file "
          << option_value<std::string>(template_infile) << " does not exist or it is empty!";
      return 0;
    }
    std::shared_ptr<AlignmentScoreBase<float>> prof_score = std::make_shared<Picasso3>(q->profile, t->profile);
    std::shared_ptr<AlignmentScoreBase<float>> ss_score = std::make_shared<SecondarySimilarity>(q->ss, t->ss);
    core::alignment::scoring::CombinedScore<float> multiscore;
    multiscore.add_score(prof_score, 1.0);
    multiscore.add_score(ss_score, 5.0);

    alignc_logger << utils::LogLevel::INFO << "global T1D with gap parameters: " << open << ", " << cont << "\n";
    core::alignment::NWAligner<float, CombinedScore<float>> global(std::max(q->length(), t->length()));
    global.align(open, cont, multiscore);

    const PairwiseAlignment_SP  ali = global.backtrace();
    alignments.push_back(*ali);

    PairwiseSequenceAlignment ss_ali(ali,q->ss,t->ss);
    core::index2 n_id = core::alignment::sum_identical(ss_ali);
    std::cout
        << utils::string_format("# %9.2f %4d %6.2f", ali->alignment_score, n_id,
                                double(n_id) * 2.0 / (ali->template_length() + ali->query_length())) << "\n";
    core::data::io::write_edinburgh(ss_ali, std::cout, 80);
  }

  // ********** CALCULATE SECTION **********
  if (calc_aln_k.was_used()) {
    core::index2 k = option_value<core::index2>(calc_aln_k);
    if (reference_alignment == 0)
      alignc_logger << utils::LogLevel::WARNING
                    << "you must provide a reference alignment to compute Aln_N measure; use '-in:native:fasta' option for this purpose!\n";
    else {
    	std::cout <<"#Aln_k nQ  nTr nTi nAln-ref\n";
      for (PairwiseAlignment & ali : alignments)
    	  std::cout <<utils::string_format("  %3d  %3d %3d %3d %3d\n",compute_aln_n(*reference_alignment, k, ali),ali.query_length(),
    			  reference_alignment->template_length(),ali.template_length(),reference_alignment->n_aligned());
      }
  }
  if (sequence_identity.was_used()) {
    int i = -1;
    for (PairwiseAlignment & ali : alignments) {
      // the alignment might have been changed by this program. So here we:
      // - remove gaps from the sequences used to create an alignment
      // - re-create the aligned sequences according to the actual state of the alignment object
      const std::string tq = ali.get_aligned_query(input_sequences[++i]->create_ungapped_sequence()->sequence,'-');
      const std::string & tmp_query_header = input_sequences[i]->header();
      const std::string tt = ali.get_aligned_template(input_sequences[++i]->create_ungapped_sequence()->sequence,'-');
      const std::string & tmp_tmplt_header = input_sequences[i]->header();
      std::cout << tmp_query_header << " : " << tmp_tmplt_header << " "
                << core::alignment::sum_identical(tq, tt) * 2.0
        / (ali.template_length() + ali.query_length()) << "\n";
    }
  }

  // ********** OUTPUT SECTION **********
//  core::index2 width = (output_seq_width.was_used()) ? option_value<core::index2>(output_seq_width) : 80;

  // ---------- Write blocks
  if (output_blocks.was_used()) {
    for (const PairwiseAlignment & ali : alignments) {
      const auto blocks = ali.aligned_blocks();
      for (const AlignmentBlock & b : blocks) std::cout << b<<"\n";
      std::cout << "\n";
    }
  }

  // ---------- Write gaps
  if (output_gaps.was_used()) {
    /// TODO implement "Write gaps" option
  }

  // ---------- Write path
  if (output_alipath.was_used()) {
    std::shared_ptr<std::ostream> out = utils::out_stream(option_value<std::string>(output_alipath));
    for (const PairwiseAlignment & a : alignments) *out << a.to_path() << "\n";
  }

  // ---------- Write FASTA
  if (output_fasta.was_used()) {
    std::shared_ptr<std::ostream> out = utils::out_stream(option_value<std::string>(output_fasta));
    int i=-1;
    for (const PairwiseAlignment & ali : alignments) {
      const Sequence_SP s1 = input_sequences[++i]->create_ungapped_sequence();
      const Sequence_SP s2 = input_sequences[++i]->create_ungapped_sequence();
      (*out) << core::data::io::create_fasta_string(ali,*s1,*s2);
    }
  }
}

