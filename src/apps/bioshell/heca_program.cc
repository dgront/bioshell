// ---------- Command line options imports
#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/select_options.hh>
#include <utils/options/output_options.hh>
#include <utils/options/structures_from_cmdline.hh>

// ---------- computations
#include <core/data/structural/ResidueSegment.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/HECA.hh>
#include <core/calc/structural/HECA.cc>

// ---------- output
#include <core/data/io/fasta_io.hh>
#include <core/data/io/ss2_io.hh>

utils::Logger logger("heca");

const std::string heca_info = R"(
Predicts secondary structure in the three-letter (i.e. HEC) code for a given PDB
)";

int main(int argc, const char* argv[]) {

    utils::LogManager::INFO();

    using namespace core::data::io;
    using namespace utils::options;
    using namespace core::data::structural;
    using namespace core::calc::structural;
    using namespace core::data::structural::selectors;

    utils::options::Option frag_len_opt("-n", "-frag_length", "the length of each considered fragment");
    utils::options::Option deep_model_opt("-dm", "-deep_model", "the neural network model");

    utils::options::OptionParser & cmd = OptionParser::get("heca");
    cmd.register_option(utils::options::help, show_examples, verbose,db_path, frag_len_opt,deep_model_opt);
    cmd.register_option(utils::options::output_ss2, utils::options::output_fasta_secondary);
    cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures

    core::index2 id = cmd.add_examples_group("examples:");
    cmd.add_example(id,"calculate all features for fragments of 5 amino acids extracted from the given set (list of pdb_id+chain_id)",
                    "heca -in:pdb:chainid::list=cullpdb-list -in:pdb:path=./pdb/ -n=5");
    cmd.program_info(heca_info);

    if (!cmd.parse_cmdline(argc, argv)) return 1;
    cmd.inject_option(input_pdb_header_ss, "true");
    cmd.inject_option(input_pdb_header, "true");

    std::vector<core::data::structural::Structure_SP> structures;
    std::vector<std::string> structure_ids;
    utils::options::structures_from_cmdline(structure_ids, structures);

    core::index2 frag_len = option_value<core::index2>(frag_len_opt, 13);
    std::string model_file = option_value<std::string>(deep_model_opt, core::BioShellEnvironment::bioshell_db_path()+"/heca/heca_model_13.json");

    std::string predicted_secondary = "";
    std::string secondary;

    for (auto s : structures) {
        for (auto chain:s->chain_codes()) {
            logger << utils::LogLevel::INFO << "Processing chain: " << chain << "\n";
            auto sel = ChainSelector(chain);
            auto schain = s->clone(sel);
            auto heca = HECA(model_file, frag_len);
            predicted_secondary = heca.assign(schain);
            secondary = (*schain)[0]->create_sequence()->str();
            double rate=0;
            for (int pos = 0; pos < predicted_secondary.size(); pos++) {
              if (predicted_secondary[pos] != '-' && secondary[pos] == predicted_secondary[pos])
                rate += 1;
            }
            rate = rate/(predicted_secondary.size() - std::count(predicted_secondary.cbegin(), predicted_secondary.cend(), '-'));
            logger << utils::LogLevel::INFO << "Success rate for chain " << chain << ": " << rate << "\n";

//            std::cout << rate << "\n";
//            std::cout<<predicted_secondary<<"\n";
//            std::cout<<secondary<<"\n";
            std::string header = utils::string_format("%s:%s",s->code().c_str(), chain.c_str());
            core::data::sequence::SecondaryStructure sec_str(header, s->get_chain(chain)->create_sequence()->sequence, 1,
                predicted_secondary);
            if (output_fasta_secondary.was_used()) {
              std::cout << core::data::io::create_fasta_string(sec_str)<<"\n";
              std::cout << core::data::io::create_fasta_secondary_string(sec_str)<<"\n";
            }
            if(output_ss2.was_used()) {
              std::shared_ptr<std::basic_ostream<char>> out = utils::out_stream(option_value<std::string>(output_ss2));
              core::data::io::write_ss2(sec_str, *out);
            }
        }
    }
}