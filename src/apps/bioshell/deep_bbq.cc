#include <numeric>
#include <iostream>

#include <core/index.hh>
#include <core/BioShellEnvironment.hh>

#include <core/calc/numeric/basic_math.hh>
#include <core/calc/structural/DeepBBQPredictor.hh>
#include <core/data/io/Pdb.hh>
#include <core/calc/structural/calculate_from_structure.hh>
#include <core/calc/structural/Bbq.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <core/protocols/selection_protocols.hh>

#include <utils/Logger.hh>
#include <utils/io_utils.hh>
#include <core/data/io/ss2_io.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/select_options.hh>
#include <utils/options/scoring_options.hh>
#include <utils/options/sampling_options.hh>
#include <utils/options/structures_from_cmdline.hh>


#include <simulations/systems/PdbAtomTyping.hh>
#include <simulations/forcefields/cartesian/DSSPEnergy.hh>
#include <simulations/forcefields/cartesian/PhiPsiEnergy.hh>
#include <simulations/movers/LambdaMover.hh>
#include <simulations/movers/MoversSetSweep.hh>
#include <simulations/forcefields/TotalEnergyByAtom.hh>
#include <simulations/sampling/SimulatedAnnealing.hh>
#include <simulations/observers/cartesian/PdbObserver.hh>
#include <simulations/observers/cartesian/ExplicitPdbFormatter.hh>
#include <simulations/observers/ObserveEnergyComponents.hh>
#include <simulations/sampling/MetropolisAcceptanceCriterion.hh>
#include <core/calc/structural/HECA.hh>


using namespace core::data::structural;

utils::Logger logger("deep_bbq");


int main(int argc, const char* argv[]) {

    using namespace core::data::structural;
  using namespace core::calc::structural;
    using namespace simulations::forcefields::cartesian;

  using namespace utils::options;
    utils::options::Option deep_model_opt("-dm", "-deep_model", "the neural network model");

  std::string model_name = "deepBBQ_12_model01.json";
    utils::options::OptionParser & cmd = OptionParser::get("deep_bbq");
    cmd.register_option(utils::options::help, show_examples, verbose,db_path);
    cmd.register_option(input_pdb, input_pdb_list, input_chainid_list, input_pdb_path, input_pdb_header, input_pdb_header_ss); // input PDB structures
    cmd.register_option(mc_outer_cycles(100), mc_inner_cycles(10), mc_cycle_factor(1), box_size(100.0));
    cmd.register_option(random_jump_range(0.1));
    cmd.register_option(input_ss2);
    cmd.register_option(begin_temperature(0.5), end_temperature(0.1), temp_steps(10));

    cmd.register_option(ca_only,deep_model_opt,select_chains,select_bb,select_aa);
    utils::options::Option optimize("-optimize", "-optimize", "optimize the structure with PhiPsi and DSSP energy");
    cmd.register_option(optimize);
    utils::options::Option real_lambda("-real_lambda", "-real_lambda", "rebuilt from calculated lambda, not predicted");
    cmd.register_option(real_lambda);

    if (!cmd.parse_cmdline(argc, argv)) return 1;
    cmd.inject_option(input_pdb_header_ss, "true");
    cmd.inject_option(input_pdb_header, "true");
    if (!cmd.was_used(real_lambda)) cmd.inject_option(ca_only,"true");

    std::string model_path = option_value<std::string>(deep_model_opt, core::BioShellEnvironment::bioshell_db_path()+"/bbq/deepBBQ_12_model01.json");

    auto bbq = Bbq();

    // ---------- start the predictor - load the DL model from a JSON file
    DeepBBQPredictor predictor(model_path, model_name);
    DeepBBQPredictor::initialize();

  std::vector<core::data::structural::Structure_SP> structures;
    std::vector<std::string> structure_ids;
    utils::options::structures_from_cmdline(structure_ids, structures);
    core::data::structural::selectors::ResidueHasCA has_ca;
    for (auto s : structures) {
        // --- remove ligands before doing anything (otherwise it segfaults after removing residues)
        for (auto chain: *s) chain->remove_ligands();
        // --- remove residues without CA
        core::protocols::keep_selected_residues(has_ca, *s);
        // --- creating new structure and chain to store rebuilt coordinates
      Structure_SP new_strc = std::make_shared<Structure>(s->code());
        logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
        auto out = utils::out_stream(s->code() + "_rebuilt.pdb");
        for (auto chain: *s) {
            logger << utils::LogLevel::FINE << "Processing chain " << chain->id() << "\n";
            if (chain->count_residues() < 5) {
              logger << utils::LogLevel::WARNING << "Chain " << chain->id() << " is too short for reconstruction!\n";
              continue;
            }
            Structure_SP strctr = std::make_shared<Structure>(s->code());
            Chain_SP out_chain = std::make_shared<Chain>(chain->id());
            //adding dummy residue befor the chain - to be able to rebuild all the atoms in first residue
            Vec3 v1 = *(*chain)[0]->find_atom(" CA ");
            Vec3 v2 = *(*chain)[1]->find_atom(" CA ");
            Vec3 v3 = *(*chain)[2]->find_atom(" CA ");
            auto ca_befor_N = dummy_ca_N(v1, v2, v3);
            Residue_SP rN = std::make_shared<Residue>(-1, "ALA");
            rN->ss('C');
            rN->push_back(ca_befor_N);
            out_chain->push_back(rN);
            //coping only CA to the rebuilt structure
            core::data::structural::selectors::IsCA isca;
            for (const Residue_SP ri : *chain) {
                Residue_SP r = ri->clone(isca); // --- deep copy of each residue
                out_chain->push_back(r);
            }
            //adding dummy residue after chain to rebuilt atoms in the last residue
            Residue_SP rC = std::make_shared<Residue>(-1, "ALA");
            rC->ss('C');
            core::index4 nres = chain->count_residues();
            //std::cout << nres << "\n";
            v1.set(*((*chain)[nres - 3]->find_atom(" CA ")));
            v2.set(*((*chain)[nres - 2]->find_atom(" CA ")));
            v3.set(*((*chain)[nres - 1]->find_atom(" CA ")));
            auto ca_after_C = dummy_ca_C(v1, v2, v3);
            rC->push_back(ca_after_C);
            out_chain->push_back(rC);
          out_chain->terminal_residue(out_chain->back());
            strctr->push_back(out_chain);
            std::stringstream out_stream;

            core::data::sequence::SecondaryStructure_SP secondary;
            if (input_ss2.was_used()) {
                 secondary = core::data::io::read_ss2(option_value<std::string>(input_ss2));
              core::index4 r_i=0;
              for (const Residue_SP ri : *out_chain) {
                ri->ss((*secondary)[r_i++]);
              }
            }


            compute_bbq_features(*strctr, out_stream, false);
            out_stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //ignore first line
            std::vector<double> lambdas;
            if (cmd.was_used(real_lambda)){
                lambdas.push_back(0);
                compute_lambda(*s,lambdas);
                lambdas.push_back(0);
            }
            else {
              // --- set a timer to check how long doest it take to reconstruct a given chain
              std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
                lambdas = predictor.predict(out_stream); //works with any istream, e.g. stringstream
              std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
              std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(now - start);
              logger << utils::LogLevel::INFO << "deep-BBQ model run for " << lambdas.size() << " residues in " << time_span.count() << " sec.\n";
            }
            auto reconstructed = bbq.rebuild(*out_chain, lambdas);

            //printing rebuilt structure to file
          reconstructed->terminal_residue(*(reconstructed->end()-1));
            for (auto it = reconstructed->first_atom(); it != reconstructed->last_atom(); it++)
                *out << (*it)->to_pdb_line() << "\n";
            new_strc->push_back(reconstructed);
        }
        //optimizing a structure if flag was used
        if (cmd.was_used(optimize)){

            AtomTypingInterface_SP typing = std::make_shared<PdbAtomTyping>("data/forcefield/pdb_atom_types.dat");
            CartesianChains chains(typing,*new_strc);
            CartesianChains backup(typing,*new_strc);

           //creating energy
            auto dssp = std::make_shared<DSSPEnergy>(chains);
            auto phi_psi = std::make_shared<PhiPsiEnergy>(chains,"-");
            auto total_en = std::make_shared<simulations::forcefields::TotalEnergyByAtom>();
            total_en->add_component(dssp, 1.0);
            total_en->add_component(phi_psi, 0.3);
            //creating movers
            auto lmover=std::make_shared<simulations::movers::LambdaMover>(chains,*new_strc,backup,*total_en);
            auto moves = std::make_shared<simulations::movers::MoversSetSweep>();
            moves->add_mover(lmover,new_strc->count_residues());

          // ---- prepare a sampler: simulated annealing
          core::index2 n_inner = cmd.was_used(mc_inner_cycles) ? option_value<core::index2>(mc_inner_cycles) : 100;
          core::index2 n_outer = cmd.was_used(mc_outer_cycles) ? option_value<core::index2>(mc_outer_cycles) : 100;
          double t_from = cmd.was_used(mc_inner_cycles) ? option_value<double>(begin_temperature) : 0.5;
          double t_to = cmd.was_used(mc_outer_cycles) ? option_value<double>(end_temperature) : 0.1;
          core::index2 t_steps = cmd.was_used(mc_outer_cycles) ? option_value<core::index2>(temp_steps) : 5;
          auto temps = core::calc::numeric::equally_spaced_floats(t_from, t_to, t_steps);
            simulations::sampling::SimulatedAnnealing sa(moves, temps);
          sa.cycles(n_inner,n_outer);

            double de = dssp->energy(chains);
            double ppe = phi_psi->energy(chains);
            // ---- Observe trajectory
            std::shared_ptr<simulations::observers::cartesian::AbstractPdbFormatter> fmt = std::make_shared<simulations::observers::cartesian::ExplicitPdbFormatter>(*new_strc);
            auto trajectory = std::make_shared<simulations::observers::cartesian::PdbObserver>(chains, fmt, "trajectory.pdb");
            sa.outer_cycle_observer(trajectory);
            // --- Create observer for energy components
            auto obs_en = std::make_shared<simulations::observers::ObserveEnergyComponents>(*total_en, chains, "energy.dat", true);
          sa.outer_cycle_observer(obs_en);

            sa.run();
            de = dssp->energy(chains);
            ppe = phi_psi->energy(chains);
        }
    }

  return 0;
}
