#include <iostream>

#include <core/chemical/monomer_io.hh>
#include <core/chemical/Monomer.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>

const std::string program_info = R"(
Prepares monomers file from Components-pub.cif file.)";

int main(const int argc, const char* argv[]) {

  utils::options::Option input_components("-icif", "-in:cif", "provides input file in the CIF format");
  utils::options::Option output_monomers("-o", "-out", "provides output file in the BioShell internal formet");

  utils::options::OptionParser & cmd = utils::options::OptionParser::get("convert_monomers");
  cmd.register_option(utils::options::help, utils::options::markdown_help);
  cmd.register_option(utils::options::verbose);
  cmd.register_option(input_components, output_monomers);
  cmd.program_info(program_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  core::chemical::read_monomers_cif(utils::options::option_value<std::string>(input_components));
  core::chemical::write_monomers_txt(utils::options::option_value<std::string>(output_monomers, "monomers.txt"));
}
