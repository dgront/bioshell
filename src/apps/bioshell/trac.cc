#include <cstdio>
#include <ctime>
#include <iostream>

#include <core/chemical/AtomicElement.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/io/DataTable.hh>
#include <core/data/io/load_decoys.hh>
#include <core/data/basic/Array2D.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <core/calc/structural/angles.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/TorsionBinType.hh>
#include <core/calc/structural/interactions/BackboneHBondInteraction.hh>
#include <core/calc/structural/ProteinArchitecture.hh>
#include <core/calc/structural/transformations/Crmsd.hh>
#include <core/protocols/PairwiseCrmsd.hh>
#include <core/protocols/PairwiseTMScore.hh>
#include <core/chemical/ChiAnglesDefinition.hh>

#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/output_options.hh>
#include <utils/options/input_options.hh>
#include <utils/options/structures_from_cmdline.hh>
#include <utils/options/select_options.hh>
#include <utils/options/selector_from_cmdline.hh>
#include <utils/io_utils.hh>

const std::string trac_info = R"(
Reorient biomolecular trajectory given in PDB format)";

utils::Logger l("trac");
typedef core::data::structural::Structure::atom_iterator atom_iterator;

int main(const int argc, const char* argv[]) {

  using namespace core::data::io;
  using namespace core::data::basic;
  using namespace utils::options;
  using namespace core::data::structural;

  utils::options::Option to_origin("-center", "-trac::to_origin", "move CM of each frame to [0,0,0]");

  utils::options::OptionParser & cmd = OptionParser::get("trac");
  cmd.register_option(utils::options::help, show_examples, utils::options::markdown_help, verbose,db_path);
  cmd.register_option(input_pdb_native); // input PDB structures
  cmd.register_option(input_pdb_modelslist, input_models); // input models - also in PDB format, but simplified reading
  cmd.register_option(all_models, ca_only, bb_only, include_hydrogens);
  // ---------- selecting options
  cmd.register_option(select_ca,select_bb,select_bb_cb,select_cb,select_atoms_by_name);
  cmd.register_option(select_substructure, select_models);
  // ---------- affect models
  cmd.register_option(to_origin);
  // ---------- output options
  cmd.register_option(output_pdb);

  core::index2 id = cmd.add_examples_group("Reposition frames:");
  cmd.add_example(id,"move each frame so its CM is at [0,0,0]; keep all hydrogens",
    "trac -in:pdb:models=traj.pdb -trac::to_origin -in:pdb:with_hydrogens -out:pdb=cen.pdb");
  cmd.program_info(trac_info);

  if (!cmd.parse_cmdline(argc, argv)) return 1;

  // ********** INPUT SECTION **********
  std::vector<core::data::basic::Coordinates_SP> decoys;
  std::vector<std::string> decoy_ids;
  Structure_SP first_strctr = utils::options::models_from_cmdline(decoy_ids, decoys);

  // ********** REQUESTED TRANSFORMATIONS **********
  if (to_origin.was_used()) {
    for (core::data::basic::Coordinates_SP m : decoys) {
      Vec3 cm(0.0);
      for (size_t i = 0; i < (*m).size(); ++i) cm += (*m)[i];
      cm /= double((*m).size());
      for (size_t i = 0; i < (*m).size(); ++i)  (*m)[i] -= cm;
    }
  }

  // ********** OUTPUT **********
  if(output_pdb.was_used()) {
    std::shared_ptr<std::ostream> stream = utils::out_stream(utils::options::option_value<std::string>(output_pdb));
    core::index4 imodel = 0;
    for (core::data::basic::Coordinates_SP m : decoys) {
      (*stream) << utils::string_format("MODEL   %6d\n",++imodel);
      coordinates_to_structure(*m, *first_strctr);
      for (auto ai = first_strctr->first_const_atom(); ai != first_strctr->last_const_atom(); ++ai)
        (*stream) << (*ai)->to_pdb_line() << "\n";
      (*stream) << "ENDMDL\n";
    }
  }
}
