/*
------------------------------------------------------------------------------
          Licensing information can be found at the end of the file.
------------------------------------------------------------------------------
http.hpp - v1.0 - Basic HTTP protocol implementation over sockets (no https).
*/

/** @file web_client.hh
 * @brief Provides functions to call a remote server by HTTP protocol, e.g. to download a HTML document
 *
 */
#ifndef UI_WWW_WebClient_HH
#define UI_WWW_WebClient_HH

#include <utils/io_utils.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <cstddef>

namespace ui {
namespace www {

#define _CRT_NONSTDC_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS

#include <cstddef> // for size_t


/// Defines status of a HTTP call
typedef enum HttpStatus {
  HTTP_STATUS_PENDING,
  HTTP_STATUS_COMPLETED,
  HTTP_STATUS_FAILED,
} http_status_t;

/// Data structure for a single HTTP connection
typedef struct http_t {
  HttpStatus status;
  int status_code;
  char const *reason_phrase;
  char const *content_type;
  std::size_t response_size;
  void *response_data;
} http_t;

/** @brief Sends a GET request to a server
 *
 * @param url - URL to be accessed
 * @param memctx - buffer for data
 * @return a structure describing the result
 */
http_t *http_get(const std::string & url, void *memctx);

/** @brief Sends a POST request to a server
 *
 * @param url - URL to be accessed
 * @param memctx - buffer for data
 * @return a structure describing the result
 */
http_t *http_post(const std::string & url, void const *data, std::size_t size, void *memctx);

/** @brief Process a HTTP connection
 *
 * @param http - HTTP structure
 */
HttpStatus http_process(http_t *http);

/** @brief Releases a HTTP connection
 *
 * @param http - HTTP structure
 */
void http_release(http_t *http);

}
}
#endif


/*
-----------------------------------------------------------------------------

ALTERNATIVE B - Public Domain (www.unlicense.org)

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or distribute this
software, either in source code form or as a compiled binary, for any purpose,
commercial or non-commercial, and by any means.

In jurisdictions that recognize copyright laws, the author or authors of this
software dedicate any and all copyright interest in the software to the public
domain. We make this dedication for the benefit of the public at large and to
the detriment of our heirs and successors. We intend this dedication to be an
overt act of relinquishment in perpetuity of all present and future rights to
this software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

------------------------------------------------------------------------------
*/
