/** \file web_utils.hh
 * @brief Provides utilities for  web apps
 */

#ifndef UI_WWW_web_utils_HH
#define UI_WWW_web_utils_HH

#include <string>

#include <utils/Logger.hh>
#include <utils/string_utils.hh>
#include <utils/options/Option.hh>

namespace ui {
namespace www {

unsigned long resolve_name(const std::string &name);

void split_url(const std::string &url, std::string &first, std::string &second);

std::string load_url(const std::string &url);

/** \brief Decodes a URL string that has been sent in an encoded form
 *
 * @param url - encoded URL
 * @return decoded URL string
 */
std::string decode_url(const std::string &url);

/** \brief Encodes a URL string by replacing non-ASCI characters with their codes.
 *
 * @param url - a URL to be encoded
 * @return encoded URL string
 */
std::string encode_url(const std::string &url);

}
}
#endif
