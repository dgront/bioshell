#ifndef UI_TASKS_ForEach_HH
#define UI_TASKS_ForEach_HH

#include <string>
#include <memory>

#include <core/data/io/XMLElement.hh>
#include <ui/tasks/Task.hh>
#include <ui/tasks/Protocol.hh>

namespace ui {
namespace tasks {

/** @brief This task will iterate over a range of values.
 *
 * All the internal tasks (i.e. tasks defined inside this task) will be repeated for each value
 * of the loop variable. The values themselves may be either defined as a string (comma separated)
 * or read from a file (one value per line). The given order of values is preserved.
 * The example given below will produce an input file for:
 * @code
<for_each loop_variable_name='{TARGET}'>
</file_from_template>
 * @endcode
 */
class ForEach : public Protocol {
public:

  /** @name XML elements that <code>for_each</code> task can recognize
   */
  ///@{
  static const std::string LoopVariableAttribute; ///< keyword to provide the loop variable
  static const std::string ValuesFileAttribute; ///<  keyword to provide an input files with the values the variable may take
  static const std::string ValuesAttributeOrTag; ///< keyword to provide all the values the variable may take (separated with a comma)
  ///@}

  /** @brief Creates a new ForEach task based on its definition stored in an XML file.
   *
   * @param xml_cfg_file - task config file name
   */
  ForEach(const std::string & xml_cfg_file);

  /** @brief Creates a new ForEach task based on the XML data structure.
   *
   * @param task_cfg - root element of a XML structure describing this task
   */
  ForEach(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);

  /// Actually runs the task i.e. executes the external program
  virtual  void run();

private:
  utils::Logger logger;
  std::string loop_variable_name_;
  std::vector<std::string> values;
  std::shared_ptr<core::data::io::XMLElement> xml_cfg_;
  /// Called by the constructor to load the data from XML structure
  bool load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);
};

}
}

#endif
