#include <string>
#include <memory>

#include <ui/tasks/Task.hh>
#include <ui/tasks/Variables.hh>
#include <ui/tasks/Declare.hh>
#include <core/data/io/XML.hh>
#include <core/data/io/XMLElement.hh>

namespace ui {
namespace tasks {

Declare::Declare(const std::string & xml_cfg_file) : logger("Declare") {

  core::data::io::XML parser(xml_cfg_file);
  load_task(parser.document_root());
  Task::task_name = "Declare";
}

Declare::Declare(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) : logger("Declare") {
  load_task(task_cfg);
  Task::task_name = "Declare";
}

bool Declare::load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) {

  if (task_cfg->name().compare(DeclareTaskTag) == 0) {
    // ---------- Extract the variable name and its associated value
    std::string variable_name = task_cfg->find_value(VariableAttribute);
    std::string variable_value = task_cfg->find_value(ValueAttribute);
    if ((variable_name.size() == 0) || (variable_value.size() == 0)) {
      logger << utils::LogLevel::SEVERE << "The Declare task must use " << VariableAttribute << " and "
          << ValueAttribute << " keywords to declare a variable with its value\n";
      // throw incorect argument
    }
    Variables::get().set(variable_name,variable_value);
    logger << utils::LogLevel::INFO << "variable \"" << variable_name << "\" initialized with \"" << variable_value << "\" value\n";

  }

  return false;
}


}
}


