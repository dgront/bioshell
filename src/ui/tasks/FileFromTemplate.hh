#ifndef UI_TASKS_FileFromTemplate_HH
#define UI_TASKS_FileFromTemplate_HH

#include <string>
#include <memory>
#include <map>

#include <core/data/io/XMLElement.hh>
#include <ui/tasks/Task.hh>

namespace ui {
namespace tasks {

/** @brief This task will produce a text from a predefined template.
 *
 * The example given below will produce an input file from a template:
 * @code
<file_from_template output_file='result'>
     <template_text>
     QUERY_SEQUENCE={QUERY_NAME}.fasta
     </template_text>
     <substitute variable="{QUERY_NAME}" value="T0666" />
</file_from_template>
 * @endcode
 */
class FileFromTemplate : public Task {
public:

  /** @brief Creates a new FileFromTemplate based on its definition stored in an XML file.
   *
   * @param xml_cfg_file - task config file name
   */
  FileFromTemplate(const std::string & xml_cfg_file);

  /** @brief Creates a new FileFromTemplate based on the XML data structure.
   *
   * @param task_cfg - root element of a XML structure describing this task
   */
  FileFromTemplate(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);

  /// Actually runs the task i.e. create the files on disc accorging to the template provided by a user
  virtual void run();

private:
  utils::Logger logger;
  std::string template_text;
  std::string out_fname;
  std::map<std::string,std::string> substitutions;

  /// Called by the constructor to load the data from XML structure
  bool load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);
};

}
}

#endif
