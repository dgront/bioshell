#include <string>
#include <memory>
#include <stdexcept>

#include <core/data/io/XML.hh>
#include <core/data/io/XMLElement.hh>
#include <utils/io_utils.hh>
#include <ui/tasks/ForEach.hh>
#include <ui/tasks/Variables.hh>
#include <ui/tasks/Protocol.hh>

namespace ui {
namespace tasks {

const std::string ForEach::LoopVariableAttribute = "loop_variable_name"; //
const std::string ForEach::ValuesFileAttribute = "loop_values_from_file"; //
const std::string ForEach::ValuesAttributeOrTag = "loop_values"; //


ForEach::ForEach(const std::string & protocol_cfg) : Protocol(protocol_cfg,ForEachTaskTag), logger("ForEach") {

  Task::task_name = "ForEach";
  core::data::io::XML parser(protocol_cfg);
  xml_cfg_ = parser.document_root();
}

ForEach::ForEach(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) : Protocol(task_cfg,ForEachTaskTag), logger("ForEach") {

  Task::task_name = "ForEach";
  // ---------- Just create the copy of the XML text; it must be parsed and loaded at the execution stage (otherwise some files may not exist yet)
  xml_cfg_ = task_cfg;
}

void ForEach::run() {

  // ---------- Extract the ID string, if given
  if (xml_cfg_->has_attribute(LoopVariableAttribute)) loop_variable_name_ = xml_cfg_->attribute(LoopVariableAttribute);
  else
    throw std::invalid_argument("for_each task must provide a list of values valid for the loop index variable!");

  // ---------- Loop values from a file, separated either by a comma or a new line
  std::string in_file_name = xml_cfg_->find_value(ValuesFileAttribute);
  if(in_file_name.size()>0) {
    std::string f = Variables::subst(in_file_name);  // ---------- Substitute variables within the file name
    std::string file = utils::load_text_file(f);
    std::replace(file.begin(),file.end(),',','\n');
    utils::split(file,values,'\n');
  }
  // ---------- Loop values from a string, separated either by a comma or a new line
  std::string variables_as_text = xml_cfg_->find_value(ValuesAttributeOrTag);
  if (variables_as_text.size()>0) {
    std::string v = Variables::subst(variables_as_text); // ---------- Substitute variables within the variables
    std::replace(v.begin(), v.end(), ',', '\n');
    utils::split(v, values, '\n');
  }
  // ---------- Elaborated logging, if desired
  if (values.size() > 0) {
    logger << utils::LogLevel::INFO  << " values declared for the loop variable "
        << loop_variable_name_;
    if (logger.is_logable(utils::LogLevel::FINE)) {
      logger << " :\n";
      for (const std::string & v : values)
        logger << "\t\t" << v << "\n";
      logger << "\n";
    }
  } else {
    logger << utils::LogLevel::SEVERE << "No values declared for the loop variable: " << loop_variable_name_
        << ", the loop will not be executed at all!\n";
  }

  // ---------- Now iterate through the loop
  for (const std::string & value : values) {
    Variables::get().set(loop_variable_name_, value);
    if (!if_dry_run) {
      for (auto & t : tasks) {
        logger << utils::LogLevel::FINE << "launching " << t->name() << " within a for_each loop\n";
        t->run();
      }
    }
  }
}

}
}

