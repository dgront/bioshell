#include <cstdlib>
#include <string>
#include <memory>
#include <stdexcept>

#include <core/data/io/XML.hh>
#include <core/data/io/XMLElement.hh>

#include <ui/tasks/Variables.hh>
#include <ui/tasks/ExternalTask.hh>

namespace ui {
namespace tasks {

ExternalTask::ExternalTask(const std::string & xml_cfg_file) : l("ExternalTask") {

  Task::task_name = "ExternalTask";
  core::data::io::XML parser(xml_cfg_file);
  load_task(parser.document_root());
}

ExternalTask::ExternalTask(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) : l("ExternalTask") {

  Task::task_name = "ExternalTask";
  load_task(task_cfg);
}

bool ExternalTask::load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) {

  using namespace core::data::io;

  if (task_cfg->name().compare(ExternalTaskTag) == 0) {
    // ---------- Extract the name of the executable we gona run
    executable = task_cfg->find_value(CommandAttribute);

    // ---------- Extract the parameter names and their associated values
    for (core::index2 i = 0; i < task_cfg->count_branches(); ++i) {
      std::shared_ptr<XMLElement> bi = std::static_pointer_cast<XMLElement>(task_cfg->get_branch(i));
      if (bi->name().compare(ParameterTag) == 0) {
        insertion_order.push_back( bi->find_value(ParameterNameAttributeOrTag) );
	if (bi->has_attribute(ParamValueNameAttributeOrTag)) parameters[bi->find_value(
            ParameterNameAttributeOrTag)] = bi->find_value(ParamValueNameAttributeOrTag);
        else parameters[bi->find_value(ParameterNameAttributeOrTag)] = "";
      }
    }

    // ---------- Extract the dry-run flag value
    if (task_cfg->has_attribute(DryRunAttribute)) {
      std::istringstream(task_cfg->attribute(DryRunAttribute)) >> std::boolalpha >> if_dry_run;
      if (if_dry_run) l << utils::LogLevel::INFO << "running in dry-run mode" << "\n";
      else l << utils::LogLevel::INFO << "dry-run mode switched OFF" << "\n";
    }

    // ---------- Extract the output file name, if given
    if (task_cfg->has_attribute(OutputFileAttributeOrTag)) out_fname = task_cfg->attribute(OutputFileAttributeOrTag);

    // ---------- Extract the separator character, if given
    if (task_cfg->has_attribute(FlagValueSeparatorAttribute))
      flag_value_separator = task_cfg->attribute(FlagValueSeparatorAttribute)[0];

    // ---------- Extract the path string, if given
    if (task_cfg->has_attribute(PathAttribute)) path = task_cfg->attribute(PathAttribute);

    // ---------- Extract the ID string, if given
    if (task_cfg->has_attribute(UniqueIDAttribute)) unique_id_ = task_cfg->attribute(UniqueIDAttribute);
    else unique_id_ = utils::to_string(task_cfg->id);

    return true;
  }

  return false;
}

void ExternalTask::run() {

  std::stringstream ss;
  if (path.size() > 0) ss << utils::join_paths(path, executable);
  else ss << executable;
  for (const std::string & pi : insertion_order) {
    std::string pv = Variables::get().subst(pi);
    ss << " " << pv << " " << parameters[pv];
  }

  std::string cmd = Variables::get().subst(ss.str());
  l << utils::LogLevel::INFO << "Executing an external command: " << cmd << "\n";

  if (if_dry_run) {
    std::cerr << cmd << "\n";
  } else {
    FILE *in;
    char buff[512];
    if (!(in = popen(cmd.c_str(), "r"))) {
      throw std::runtime_error("Error while executing: " + cmd);
    }

    if (out_fname.size() > 0) {
      std::string out = Variables::subst(out_fname);
      l << utils::LogLevel::INFO << "Writing stdout stream to a file: " << out << "\n";
      std::shared_ptr<std::ostream> output = utils::out_stream(out);
      while (fgets(buff, sizeof(buff), in) != NULL) {
        *output << buff;
      }
      output->flush();
    }
    pclose(in);
  }
}

}
}
