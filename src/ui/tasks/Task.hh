#ifndef UI_TASKS_Task_HH
#define UI_TASKS_Task_HH

#include <string>

namespace ui {
namespace tasks {

/** @brief Base class for a BioShell's task
 *
 */
class Task {
public:

  /// Returns the name (string id) of this task
  const std::string & name() const { return task_name; }

  /** @brief Returns the unique identifier string assigned to this task.
   *
   * The identifying string for each task may be assigned by setting the <code>unique_id</code>
   * attribute within the task element; in the case of ExternalTask this would look like:
   * @code
<external unique_id="run_hostname" cmd="hostname" />
   * @endcode
   *
   * By default, the id is set to a string representing an integer index assigned the task XML element by the XML reader
   */
  const std::string & unique_id() const { return unique_id_; }

  /// @brief Bare virtual destructor
  virtual ~Task() {};

  /** @brief Virtual method to be called when this task is executed
   * Actual implementation must be provided by a derived class
   */
  virtual void run() = 0;

  /** @name XML tags that define BioShell's tasks.
   *
   * Each variable holds a name of a BioShell task. It should be used as a name of an XML tag
   * to declare a relevant task.
   */
  ///@{
  static const std::string ProtocolTag;
  static const std::string ProtocolTaskTag;
  static const std::string ForEachTaskTag;
  static const std::string ExternalTaskTag;
  static const std::string FileFromTemplateTag; //
  static const std::string DeclareTaskTag; //
  static const std::string MakeDirTag;
  static const std::string ChangeDirTag;
  ///@}

  /** @name XML tags that define tasks' inner elements.
   *
   * <strong>Note</strong> that some of them may also be used as attributes!
   */
  ///@{
  static const std::string ParameterTag;//
  static const std::string ParameterNameAttributeOrTag;//
  static const std::string ParamValueNameAttributeOrTag;//
  static const std::string TemplateFileAttributeOrTag; //
  static const std::string TemplateTextTag; //
  static const std::string SubstitutionTag;//
  static const std::string OutputFileAttributeOrTag;//
  static const std::string XMLConfigAttribute;//
  ///@}

  /** @name XML tags that define tasks' inner elements
   */
  ///@{
  static const std::string UniqueIDAttribute; ///< Provides the unique id (aka name) for a task or o protocol
  static const std::string CommandAttribute; ///< The command to be executed by ExternalTask
  static const std::string DryRunAttribute; ///< dry-run mode for a protocol or a task
  static const std::string PathAttribute; ///< path to an executable or a file
  static const std::string FlagValueSeparatorAttribute;///< char separating a flag from its value at the command line
  static const std::string VariableAttribute;///< a string (word) used to denote a variable to be substituted in a template text
  static const std::string ValueAttribute;///< the value to be used as a replacement for a variable
  ///@}
protected:
  std::string unique_id_; ///< unique ID assigned to this task (task name may be noy unique)
  std::string task_name; ///< the name of this task
};

}
}

#endif
