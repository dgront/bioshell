#include <stdio.h>
#include <sys/stat.h>
#include <cstdlib>
#include <string>
#include <memory>
//#include <stdexcept>
#include <iostream>
//#include <iomanip>

#include <ui/tasks/Task.hh>
#include <ui/tasks/MakeDir.hh>
#include <ui/tasks/Variables.hh>
#include <core/data/io/XML.hh>
#include <core/data/io/XMLElement.hh>
#include <utils/io_utils.hh>
#include <utils/string_utils.hh>

namespace ui {
namespace tasks {

const std::string MakeDir::DirectoryNameAttribute = "directory_name";
const std::string MakeDir::DirectoryPermissionAttribute = "directory_permissions";

MakeDir::MakeDir(const std::string & xml_cfg_file) : logger("MakeDir") {

  Task::task_name = "MakeDir";
  core::data::io::XML parser(xml_cfg_file);
  load_task(parser.document_root());
}

MakeDir::MakeDir(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) :  logger("MakeDir") {

  Task::task_name = "MakeDir";
  load_task(task_cfg);
}

bool MakeDir::load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) {

  if (task_cfg->name().compare(MakeDirTag) == 0) {
    // ---------- Extract the name of the directory to be created
    dir_name_ = task_cfg->find_value(DirectoryNameAttribute);
    if (dir_name_.size() ==0)     throw std::invalid_argument("make_dir task must provide the name of the new directory; use directory_name attribute for this purpose!");

    // ---------- Extract the permissions
    permissions = task_cfg->find_value(DirectoryPermissionAttribute);
    return true;
  }

  return false;
}

void MakeDir::run() {

  std::string dir_name = Variables::subst(dir_name_);
  size_t p = 0777;
  if(permissions.size()>1) {
    std::stringstream ss(permissions);
    ss >> std::oct >> p;
  }
  logger << utils::LogLevel::FINE << "Making a new directory: " << dir_name << "\n";
#ifndef _WIN64
  mkdir(dir_name.c_str(), p);
#else
    mkdir(dir_name.c_str());
#endif

}

}
}
