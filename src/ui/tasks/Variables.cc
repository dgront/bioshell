#include <string>

#include <ui/tasks/Variables.hh>
#include <utils/Logger.hh>
#include <utils/LogManager.hh>

namespace ui {
namespace tasks {

void Variables::set(std::string & name_value) {

  utils::trim(name_value, "\n");
  std::vector<std::string> tokens = utils::split(name_value, {','});
  for (const std::string & token : tokens) {
    std::vector<std::string> key_val = utils::split(token, {':'});
    if (key_val.size() != 2) {
      logger << utils::LogLevel::WARNING << "Skipping incorrect variable declaration: " << token << "\n";
    } else {
      set(key_val[0], key_val[1]);
      logger << utils::LogLevel::INFO << "Initializing variable: " << token << "\n";
    }
  }
}

utils::Logger &operator <<(utils::Logger &logger, const Variables v) {

  std::stringstream ss;
  for (auto p : v.map)
    ss << '\t' << p.first << " = " << p.second << "\n";

  logger << ss.str();

  return logger;
}

}
}
