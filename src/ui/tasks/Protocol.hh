#ifndef UI_TASKS_Protocol_HH
#define UI_TASKS_Protocol_HH

#include <string>
#include <memory>

#include <core/data/io/XMLElement.hh>
#include <ui/tasks/Task.hh>
#include <utils/Logger.hh>

namespace ui {
namespace tasks {

/** @brief A Protocol is a set of mutually-dependent Task objects executed in a predefined order.
 *
 * Note, that a Protocol is a Task.
 */
class Protocol : public Task {
public:
  bool if_dry_run = false;  ///< If set to true, this task will only print on the stderr all the command that would be executed

  /** @brief Creates a new Protocol based on its definition stored in an XML file.
   *
   * The constructor will read the given XML file and create all the tasks defined in it.
   * @param xml_cfg - protocol config file name
   * @param my_tag - a name tag assigned to this protocol
   */
  Protocol(const std::string & xml_cfg, const std::string & my_tag = ProtocolTag);

  /** @brief Creates a new Protocol based on the data stored in the XML document
   *
   * @param task_cfg - root of the XML document's tree that defines the protocol
   * @param my_tag - a name tag assigned to this protocol
   */
  Protocol(const std::shared_ptr<core::data::io::XMLElement> & task_cfg, const std::string & my_tag = ProtocolTag) : l("Protocol") {
    load_protocol(task_cfg,my_tag,tasks);
  }

  /// @brief Bare virtual destructor
  virtual ~Protocol() {};

  virtual void run();

protected:
  std::vector<std::shared_ptr<Task>> tasks; ///< All the tasks used by this protocol stored in the order they should be executed

  /** @brief Loads a protocol task from a XML config file
   *
   * @param protocol_cfg - definition of this task (XML format parsed)
   * @param my_tag - a name tag assigned to this protocol
   * @param tasks_vector - vector where the sub-tasks will be inserted to
   * @return true when successfully loaded
   */
  bool load_protocol(const std::shared_ptr<core::data::io::XMLElement> & protocol_cfg, const std::string & my_tag,
      std::vector<std::shared_ptr<Task>> & tasks_vector);

private:
  utils::Logger l;
};

}
}

#endif
