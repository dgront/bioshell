#ifndef UI_TASKS_ProtocolTask_HH
#define UI_TASKS_ProtocolTask_HH

#include <string>
#include <memory>

#include <core/data/io/XMLElement.hh>
#include <ui/tasks/Protocol.hh>
#include <ui/tasks/Task.hh>
#include <utils/Logger.hh>

namespace ui {
namespace tasks {

/** @brief This task will run a whole protocol as a task
 *
 * The example task definition will run PsiBlast program by executing
 * a protocol stored in <code>run_psiblast.xml</code> file
 * @code
<protocol_task xml_config="run_psiblast.xml" />
 * @endcode
 */
class ProtocolTask : public Task {
public:

  /** @brief Creates a new ExternalTask based on its definition stored in an XML file.
   *
   * @param xml_cfg_file - task config file name
   */
  ProtocolTask(const std::string & xml_cfg_file);

  /** @brief Creates a new ExternalTask based on the XML data structure.
   *
   * @param task_cfg - root element of a XML structure describing this task
   */
  ProtocolTask(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);

  /// Actually runs the underlying protocol i.e. executes all its tasks
  void run();

private:
  utils::Logger l;
  std::shared_ptr<Protocol> protocol_task;
  /// Called by the constructor to load the data from XML structure
  bool load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);
};

}
}

#endif
