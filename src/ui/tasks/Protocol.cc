#include <cstdlib>
#include <string>
#include <memory>
#include <exception>

#include <ui/tasks/Protocol.hh>
#include <ui/tasks/Declare.hh>
#include <ui/tasks/ProtocolTask.hh>
#include <ui/tasks/ExternalTask.hh>
#include <ui/tasks/ForEach.hh>
#include <ui/tasks/FileFromTemplate.hh>
#include <ui/tasks/MakeDir.hh>
#include <ui/tasks/ChangeDir.hh>
#include <core/data/io/XML.hh>
#include <core/data/io/XMLElement.hh>
#include <utils/Logger.hh>

#include <core/alignment/tasks/alignment_utility_tasks.hh>

namespace ui {
namespace tasks {

Protocol::Protocol(const std::string & protocol_cfg,const std::string & my_tag) : l("Protocol") {

  core::data::io::XML parser(protocol_cfg);
  Task::task_name = "Protocol";
  load_protocol(parser.document_root(),my_tag,tasks);
}

bool Protocol::load_protocol(const std::shared_ptr<core::data::io::XMLElement> & task_cfg, const std::string & my_tag,
    std::vector<std::shared_ptr<Task>> & tasks_vector) {

  using namespace core::data::io;

  if (task_cfg->name().compare(my_tag) == 0) {

    // ---------- Extract all the tasks defined in the XML file
    for (core::index2 i = 0; i < task_cfg->count_branches(); ++i) {
      std::shared_ptr<XMLElement> bi = std::static_pointer_cast<XMLElement>(task_cfg->get_branch(i));

      l << utils::LogLevel::INFO << "Creating a task instance: " << bi->name() << "\n";
      // ---------- Here is the BIG dispatch for all the Task types defined in BioShell
      if (bi->name().compare(ExternalTaskTag) == 0) tasks_vector.push_back(std::make_shared<ExternalTask>(bi));
      if (bi->name().compare(FileFromTemplateTag) == 0) tasks_vector.push_back(std::make_shared<FileFromTemplate>(bi));
      if (bi->name().compare(ProtocolTaskTag) == 0) tasks_vector.push_back(std::make_shared<ProtocolTask>(bi));
      if (bi->name().compare(ForEachTaskTag) == 0) tasks_vector.push_back(std::make_shared<ForEach>(bi));
      if (bi->name().compare(DeclareTaskTag) == 0) tasks_vector.push_back(std::make_shared<Declare>(bi));
      if (bi->name().compare(MakeDirTag) == 0) tasks_vector.push_back(std::make_shared<MakeDir>(bi));
      if (bi->name().compare(ChangeDirTag) == 0) tasks_vector.push_back(std::make_shared<ChangeDir>(bi));
      if (bi->name().compare(core::alignment::tasks::TrimStructureByAlignment::MyTagName) == 0)
        tasks_vector.push_back(std::make_shared<core::alignment::tasks::TrimStructureByAlignment>(bi));
    }

    // ---------- Extract the dry-run flag value
    if (task_cfg->has_attribute(DryRunAttribute))
      std::istringstream(task_cfg->attribute(DryRunAttribute)) >> std::boolalpha >> if_dry_run;

    // ---------- Extract the ID string, if given
    if (task_cfg->has_attribute(UniqueIDAttribute)) unique_id_ = task_cfg->attribute(UniqueIDAttribute);
    else unique_id_ = utils::to_string(task_cfg->id);

    l<<utils::LogLevel::INFO<<tasks_vector.size()<<" tasks defined within the "<<unique_id_<<" protocol\n";
    return true;
  }

  return false;
}

void Protocol::run() {

  if (!if_dry_run) {
    for (auto & t : tasks) {
      l << utils::LogLevel::INFO << "launching " << t->name() << "\n";
      t->run();
    }
  }
}

}
}
