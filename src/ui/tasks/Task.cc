#include <string>

#include <ui/tasks/Task.hh>

namespace ui {
namespace tasks {

const std::string Task::ProtocolTag = "protocol";
const std::string Task::ProtocolTaskTag = "protocol_task";
const std::string Task::ExternalTaskTag = "external";
const std::string Task::ForEachTaskTag = "for_each";
const std::string Task::DeclareTaskTag = "declare";
const std::string Task::MakeDirTag = "make_dir";
const std::string Task::ChangeDirTag = "change_dir";

const std::string Task::UniqueIDAttribute = "unique_id";
const std::string Task::CommandAttribute = "cmd";
const std::string Task::DryRunAttribute = "dry_run";
const std::string Task::ParameterTag = "param";
const std::string Task::ParameterNameAttributeOrTag = "flag";
const std::string Task::ParamValueNameAttributeOrTag = "value";
const std::string Task::PathAttribute = "path";
const std::string Task::FlagValueSeparatorAttribute = "flag_value_separator";
const std::string Task::XMLConfigAttribute = "xml_config";

const std::string Task::FileFromTemplateTag = "file_from_template";
const std::string Task::TemplateFileAttributeOrTag = "template_file";
const std::string Task::OutputFileAttributeOrTag = "output_file";
const std::string Task::TemplateTextTag = "template_text";
const std::string Task::SubstitutionTag = "substitute";
const std::string Task::VariableAttribute = "variable";
const std::string Task::ValueAttribute = "value";

}
}
