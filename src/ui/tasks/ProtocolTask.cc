#include <cstdlib>
#include <string>
#include <memory>
#include <exception>

#include <ui/tasks/ProtocolTask.hh>
#include <ui/tasks/Protocol.hh>
#include <core/data/io/XML.hh>
#include <core/data/io/XMLElement.hh>

namespace ui {
namespace tasks {

ProtocolTask::ProtocolTask(const std::string & xml_cfg_file) : l("ProtocolTask") {

  Task::task_name = "ProtocolTask";
  core::data::io::XML parser(xml_cfg_file);
  load_task(parser.document_root());
}

ProtocolTask::ProtocolTask(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) : l("ProtocolTask") {

  Task::task_name = "ProtocolTask";
  load_task(task_cfg);
}

bool ProtocolTask::load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) {

  if (task_cfg->name().compare(ProtocolTaskTag) == 0) {
    // ---------- Extract the name of the XML file with the protocol definition
    protocol_task = std::make_shared<Protocol>(task_cfg->find_value(XMLConfigAttribute));

    // ---------- Extract the ID string, if given
    if (task_cfg->has_attribute(UniqueIDAttribute)) unique_id_ = task_cfg->attribute(UniqueIDAttribute);
    else unique_id_ = utils::to_string(task_cfg->id);

    return true;
  }

  return false;
}

void ProtocolTask::run() {

  protocol_task->run();
}

}
}
