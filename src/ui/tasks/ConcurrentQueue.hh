#ifndef UI_TASKS_ConcurrentQueue_HH
#define UI_TASKS_ConcurrentQueue_HH

#include <string>
#include <deque>
#include <condition_variable>

namespace ui {
namespace tasks {

/** @brief Generic thread-safe queue
 *
 */
template<typename T>
class ConcurrentQueue {
public:

  /// Notifies when the object has been updated: either new data tossed in or the stream was closed
  std::condition_variable was_updated;

  void push_back(const T data) {

      std::unique_lock<std::mutex> l(lock);
      buffer.push_back(data);
      was_updated.notify_one();
  }

  /// Returns the from element from the queue
  T pop_front() {
    std::unique_lock<std::mutex> l(lock);
    T t = buffer.front();
    buffer.pop_front();
    return t;
  }

  /// Returns true if the stream has been closed i.e. there is be no more data to expect
  inline bool is_closed() const { return   all_data_sent; }

  /// Closes the stream has been closed i.e. there is be no more data to expect
  void close_stream() {
    std::unique_lock<std::mutex> l(lock);
    was_updated.notify_one();
    all_data_sent = true;
  }

  /// Returns the size of this queue
  size_t size() {
    std::unique_lock<std::mutex> l(lock);
    return buffer.size();
  }

private:
  bool all_data_sent = false;
  std::mutex lock;
  std::deque<T> buffer;
};

}
}

#endif
