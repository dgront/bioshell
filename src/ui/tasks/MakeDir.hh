#ifndef UI_TASKS_MakeDir_HH
#define UI_TASKS_MakeDir_HH

#include <string>
#include <memory>

#include <core/data/io/XMLElement.hh>
#include <ui/tasks/Task.hh>

namespace ui {
namespace tasks {

/** @brief This task will create a directory
 *
 * The example given below will create a new directory
 * @code
<make_dir directory_name="results" />
 * @endcode
 */
class MakeDir : public Task {
public:

  /** @name XML elements that <code>make_dir</code> task can recognize
   */
  ///@{
  static const std::string DirectoryNameAttribute; ///< keyword to provide the name of the directory to be created (mandatory parameter)
  static const std::string DirectoryPermissionAttribute; ///<  keyword to provide permissions for the new directory
  ///@}

  /** @brief Creates a new directory.
   *
   * @param xml_cfg_file - task config file name
   */
  MakeDir(const std::string & xml_cfg_file);

  /** @brief Creates a new directory.
   *
   * @param task_cfg - root element of a XML structure describing this task
   */
  MakeDir(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);

  /// Actually runs the task i.e. executes the external program
  virtual void run();

private:
  utils::Logger logger;
  std::string dir_name_;
  std::string permissions;

  /// Called by the constructor to load the data from XML structure
  bool load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);
};

}
}

#endif
