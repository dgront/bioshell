#ifndef UI_TASKS_Declare_HH
#define UI_TASKS_Declare_HH

#include <string>
#include <memory>

#include <core/data/io/XMLElement.hh>
#include <ui/tasks/Task.hh>

namespace ui {
namespace tasks {

/** @brief This task will declare a variable and initializes it with a value
 *
 * This task uses Task::VariableAttribute and Task::ValueAttribute keywords to declare the variable:value pair
 * The example given below will produce an input file for:
 * @code
<for_each loop_variable_name='{TARGET}'>
</file_from_template>
 * @endcode
 */
class Declare : public Task {
public:

  /** @brief Creates a new Declare task based on its definition stored in an XML file.
   *
   * @param xml_cfg_file - task config file name
   */
  Declare(const std::string & xml_cfg_file);

  /** @brief Creates a new Declare task based on the XML data structure.
   *
   * @param task_cfg - root element of a XML structure describing this task
   */
  Declare(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);

  /// Does nothing, because the variables are declared at startup
  virtual void run() {};

private:
  utils::Logger logger;
  bool load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);
};

}
}

#endif
