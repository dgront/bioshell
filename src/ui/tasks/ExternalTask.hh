#ifndef UI_TASKS_ExternalTask_HH
#define UI_TASKS_ExternalTask_HH

#include <string>
#include <memory>
#include <map>

#include <core/data/io/XMLElement.hh>
#include <ui/tasks/Task.hh>
#include <utils/Logger.hh>

namespace ui {
namespace tasks {

/** @brief This task will run an external program.
 *
 * The example task definition will run PsiBlast program from the current directory:
 * @code
<external cmd="blastpgp" path="./" dry-run="true">
     <param flag="-d" value="nr"/>
     <param flag="-i" value="target.fasta"/>
     <param flag="-I" value=""/>
</external>
 * @endcode
 */
class ExternalTask : public Task {
public:
  bool if_dry_run = false;  ///< If set to true, this task will only print on the stderr the command that would be executed

  /** @brief Character used to separate a cmdline flag from its value.
   *
   * In most cases it's a space, but BioShell uses '=' character
   */
  char flag_value_separator = ' ';

  /** @brief Creates a new ExternalTask based on its definition stored in an XML file.
   *
   * @param xml_cfg_file - task config file name
   */
  ExternalTask(const std::string & xml_cfg_file);

  /** @brief Creates a new ExternalTask based on the XML data structure.
   *
   * @param task_cfg - root element of a XML structure describing this task
   */
  ExternalTask(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);

  /// Actually runs the task i.e. executes the external program
  void run();

private:
  std::string executable;
  std::string path;
  std::string out_fname;
  std::map<std::string,std::string> parameters;
  std::vector<std::string> insertion_order;
  utils::Logger l;

  /// Called by the constructor to load the data from XML structure
  bool load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);
};

}
}

#endif
