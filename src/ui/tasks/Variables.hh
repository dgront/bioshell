#ifndef UI_TASKS_Variables_HH
#define UI_TASKS_Variables_HH

#include <string>
#include <map>

#include <utils/Logger.hh>
#include <utils/string_utils.hh>

namespace ui {
namespace tasks {

/** @brief Holds variables accessible from tasks, configurable from cmdline.
 *
 * This class is a singleton container that stores key:variable map
 */
class Variables {
public:

  /// Returns a singleton reference to the container
  static Variables & get() {
    static Variables variables;
    return variables;
  }

  inline static std::string subst(const std::string & a_string)  {

    std::string s(a_string);
    for(auto a=Variables::get().map.cbegin();a!=Variables::get().map.cend();++a) {
      utils::replace_substring(s, a->first, a->second);
    }

    return s;
  }

  /** @brief Sets a value assigned to a given key
   *
   * @param name - name of the key
   * @param value  - the value assigned to it
   */
  inline void set(const std::string & name, const std::string & value) { map[name] = value; }

  /** @brief Sets a value assigned to a  key
   *
   * @param name_value - name of the key and its value as a single string separated by a colon ':'
   * More than one value may be specified in a single call; in that case the key:value pairs must be separated with a comma, e.g.
   * "radius:12.0,cutoff=10.5"
   */
  void set(std::string & name_value);

  /** @brief Returns a value assigned to a given string
   * @param variable_name - variable name (i.e. the key)
   * @return the respective value, as a string
   */
  inline std::string &  get(std::string & variable_name) { return map[variable_name]; }

  /** @brief Returns a value assigned to a given string
   * @param variable_name - variable name (i.e. the key)
   * @return the respective value, as a string
   */
  inline const std::string & operator()(const std::string & name) { return map[name]; }

  /** @brief Returns a const-iterator that begins iteration over all variables stored in this map
   * @return const-iterator pointing to the beginning of this container
   */
  std::map<std::string,std::string>::const_iterator cbegin() const { return map.cbegin(); }

  /** @brief Returns a const-iterator that end iteration over all variables stored in this map
   * @return const-iterator pointing behind the end of this container
   */
  std::map<std::string,std::string>::const_iterator cend() const { return map.cend(); }

  /// Returns the number of variables stored here
  inline size_t size() const { return map.size(); }

  friend utils::Logger &operator <<(utils::Logger &logger, const Variables v);

private:
  Variables() : logger("Variables") {}
  utils::Logger logger;
  std::map<std::string,std::string> map;
};

/** @brief Prints a set of variables in a nice format into a Logger stream
 *
 * @param logger - output stream
 * @param v - Variables container
 * @return reference to the logger
 */
utils::Logger &operator <<(utils::Logger &logger, const Variables v);
}
}


#endif
