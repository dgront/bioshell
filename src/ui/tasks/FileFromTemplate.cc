#include <cstdlib>
#include <string>
#include <memory>
#include <exception>

#include <ui/tasks/Task.hh>
#include <ui/tasks/FileFromTemplate.hh>
#include <ui/tasks/Variables.hh>
#include <core/data/io/XML.hh>
#include <core/data/io/XMLElement.hh>
#include <utils/io_utils.hh>
#include <utils/string_utils.hh>

namespace ui {
namespace tasks {

FileFromTemplate::FileFromTemplate(const std::string & xml_cfg_file) : logger("FileFromTemplate") {

  Task::task_name = "FileFromTemplate";
  core::data::io::XML parser(xml_cfg_file);
  load_task(parser.document_root());
}

FileFromTemplate::FileFromTemplate(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) :  logger("FileFromTemplate") {

  Task::task_name = "FileFromTemplate";
  load_task(task_cfg);
}

bool FileFromTemplate::load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) {

  using namespace core::data::io;

  if (task_cfg->name().compare(FileFromTemplateTag) == 0) {

    // ---------- Extract the name of the executable we gona run
    std::string fname = task_cfg->find_value(TemplateFileAttributeOrTag);
    if (fname.size() > 0) { // --- load the file with the template
      utils::load_text_file(fname, template_text);
    } else {
      template_text = task_cfg->find_value(TemplateTextTag);
      utils::trim(template_text,"\n");
    }
    // ---------- Extract the parameter names and their associated values
    for (core::index2 i = 0; i < task_cfg->count_branches(); ++i) {
      std::shared_ptr<XMLElement> bi = std::static_pointer_cast<XMLElement>(task_cfg->get_branch(i));
      if (bi->name().compare(SubstitutionTag) == 0) {
        substitutions[bi->find_value(VariableAttribute)] = bi->find_value(ValueAttribute);
      }
    }

    // ---------- Extract the output file name, if given
    if (task_cfg->has_attribute(OutputFileAttributeOrTag)) out_fname = task_cfg->attribute(OutputFileAttributeOrTag);

    // ---------- Extract the ID string, if given
    if (task_cfg->has_attribute(UniqueIDAttribute)) unique_id_ = task_cfg->attribute(UniqueIDAttribute);
    else unique_id_ = utils::to_string(task_cfg->id);

    return true;
  }

  return false;
}

void FileFromTemplate::run() {

  Variables & v = Variables::get();

  for (auto ip = substitutions.begin(); ip != substitutions.end(); ++ip)
    utils::replace_substring(template_text, ip->first, ip->second);

  for(auto a=v.cbegin();a!=v.cend();++a)
    utils::replace_substring(template_text, a->first, a->second);

  if(out_fname.size()>0) {
    std::string f = Variables::get().subst(out_fname);
    logger << utils::LogLevel::FILE<<"Writing a cfg file : "<<f<<"\n";
    std::ofstream out(f);
    out << template_text;
    out.close();
  }
}

}
}
