#include <stdio.h>
#include <unistd.h>
#include <cstdlib>
#include <string>
#include <iostream>

#include <ui/tasks/Task.hh>
#include <ui/tasks/ChangeDir.hh>
#include <ui/tasks/Variables.hh>
#include <core/data/io/XML.hh>
#include <core/data/io/XMLElement.hh>
#include <utils/io_utils.hh>
#include <utils/string_utils.hh>

namespace ui {
namespace tasks {

const std::string ChangeDir::DirectoryNameAttribute = "directory_name";

ChangeDir::ChangeDir(const std::string & xml_cfg_file) : logger("ChangeDir") {

  Task::task_name = "MakeDir";
  core::data::io::XML parser(xml_cfg_file);
  load_task(parser.document_root());
}

ChangeDir::ChangeDir(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) :  logger("ChangeDir") {

  Task::task_name = "ChangeDir";
  load_task(task_cfg);
}

bool ChangeDir::load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) {

  if (task_cfg->name().compare(ChangeDirTag) == 0) {
    // ---------- Extract the name of the directory to be created
    dir_name_ = task_cfg->find_value(DirectoryNameAttribute);
    if (dir_name_.size() ==0)     throw std::invalid_argument("change_dir task must provide the name of the new directory; use directory_name attribute for this purpose!");

    return true;
  }

  return false;
}

void ChangeDir::run() {

  std::string dir_name = Variables::subst(dir_name_);
  logger << utils::LogLevel::FINE << "Changing working directory: " << dir_name << "\n";
  int chdir_status = chdir(dir_name.c_str());
  if (chdir_status==-1){
    logger<< utils::LogLevel::WARNING << "Error while changing directory \n";
  }
}

}
}
