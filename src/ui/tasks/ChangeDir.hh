#ifndef UI_TASKS_ChangeDir_HH
#define UI_TASKS_ChangeDir_HH

#include <string>
#include <memory>

#include <core/data/io/XMLElement.hh>
#include <ui/tasks/Task.hh>

namespace ui {
namespace tasks {

/** @brief This task will change a working directory
 *
 * Example of use:
 * @code
<change_dir directory_name="./results" />
 * @endcode
 */
class ChangeDir : public Task {
public:

  /** @name XML elements that <code>change_dir</code> task can recognize
   */
  ///@{
  static const std::string DirectoryNameAttribute; ///< keyword to provide the name of the directory to go to
  ///@}

  /** @brief Creates a task than changes current working directory
   *
   * @param xml_cfg_file - task config file name
   */
  ChangeDir(const std::string & xml_cfg_file);

  /** @brief Creates a task than changes current working directory
   *
   * @param task_cfg - root element of a XML structure describing this task
   */
  ChangeDir(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);

  /// Actually runs the task i.e. executes the external program
  virtual void run();

private:
  utils::Logger logger;
  std::string dir_name_;

  /// Called by the constructor to load the data from XML structure
  bool load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);
};

}
}

#endif
