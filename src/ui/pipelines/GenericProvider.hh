#ifndef UI_PROVIDERS_GenericProvider_HH
#define UI_PROVIDERS_GenericProvider_HH

namespace ui {
namespace pipelines {

/** @brief Base class for any data provider
 */
template <typename D>
class GenericProvider {
public:

  /// Returns true if there are any element left
  virtual bool has_next() const = 0;

  /// returns the next data element
  virtual D next() = 0;
};

}
}

#endif
