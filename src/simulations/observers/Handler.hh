/** @file Handler.hh
 *  @brief Base class to represent observers handler as file or SQL database
 */
#ifndef SIMULATIONS_OBSERVERS_Handler_HH
#define SIMULATIONS_OBSERVERS_Handler_HH

#include <memory>
#include <ostream>
#include <vector>
#include <utils/Logger.hh>

namespace simulations {
namespace observers {


/** @brief Base class to represent observers handler as file or SQL database
 *
 * This can be a base class for handlers as file or SQL database to save observations from simulation.
 */
class Handler {
public:
    /// Construtor
    Handler(const std::string & out_fname): out_fname_(out_fname), logger_("Handler"){};

  /// Virtual destructor (empty)
  virtual ~Handler() {};

  /// Sets header line for observations
   void set_header(const std::vector<std::string>& v){
      for (auto i=v.begin();i!=v.end();++i){
          header_.push_back(*i);
      }
  }

   virtual void handle_header(const std::vector<std::string>& v)=0;

  ///Writes observed values to handler
  virtual void write(const std::vector<std::string>&)=0;

protected:
    std::string out_fname_;
    std::vector<std::string> header_;
    utils::Logger logger_;

};
/// Declares a shared pointer to Handler type
typedef std::shared_ptr<Handler> Handler_SP;

}
}

#endif
