#include <sstream>

#include <simulations/observers/ToHistogramObserver.hh>
#include <simulations/evaluators/Evaluator.hh>


namespace simulations {
namespace observers {

void ToHistogramObserver::finalize() {
  std::stringstream ss;
  ss << h_;
  std::vector<std::string> lines = utils::split(ss.str(), {'\n'}, false, false);
  for (auto &l: lines) l += "\n";
  handler_->write(lines);
};

bool ToHistogramObserver::observe() {
  if (!trigger_->operator()()) return false;
  for (double d: e_->evaluate())
    h_.insert(d);

  return true;
}


}
}


