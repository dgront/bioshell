#include<iostream>
#include <fstream>

#include <simulations/observers/cartesian/PdbObserver.hh>
#include <core/data/basic/Vec3I.hh>

namespace simulations {
namespace observers {
namespace cartesian {

bool PdbObserver::observe() {

  ++cnt;
  if (!ObserverInterface::trigger_->operator()()) return false;
  std::vector<std::string> observed_data;

  observed_data.push_back(utils::string_format("MODEL   %6d\n", cnt));
  std::vector<std::string> lines;
  pdb_formatter_->format(observed_object_, lines);
  for (std::string l:lines) observed_data[0] += l + "\n";
  observed_data[0] += "ENDMDL";
  handler_->write(observed_data);
  return true;
}

bool PdbObserver::observe(const simulations::systems::CartesianAtoms &system) {

  ++cnt;
  std::vector<std::string> observed_data;
  observed_data.push_back(utils::string_format("MODEL   %6d\n", cnt));
  std::vector<std::string> lines;
  pdb_formatter_->format(system, lines);
  for (std::string l:lines) observed_data[0] += l + "\n";
  observed_data[0] += "ENDMDL";
  handler_->write(observed_data);
  return true;
}

void write_pdb_conformation(const systems::CartesianAtoms &observed_object,
                            std::shared_ptr<AbstractPdbFormatter> pdb_formatter, const std::string & out_fname) {

  PdbObserver o{observed_object, pdb_formatter, out_fname};
  o.observe();
}

}
}
}
