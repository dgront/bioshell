#include<iostream>
#include<arpa/inet.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <iostream>
#include <fstream>

#include <simulations/observers/cartesian/PymolHandler.hh>

namespace simulations {
namespace observers {
namespace cartesian {

void PymolHandler::send_line(const std::string &line) {

  sendto(sockfd, line.c_str(), line.length(), 0, (struct sockaddr *) &serv, m);
  if (line.back() != '\n') sendto(sockfd, "\n", 1, 0, (struct sockaddr *) &serv, m);
}

bool PymolHandler::handle(const std::vector<std::string> &v) {

  ++cnt;
  for (const std::string &line: v) {
    send_line(line);
  }
  sendto(sockfd, "Q", 1, 0, (struct sockaddr *) &serv, m);
  return true;

}

}
}
}
