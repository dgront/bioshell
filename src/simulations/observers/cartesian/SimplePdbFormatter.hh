#ifndef SIMULATIONS_OBSERVERS_SimplePdbFormatter_HH
#define SIMULATIONS_OBSERVERS_SimplePdbFormatter_HH

#include <simulations/observers/cartesian/AbstractPdbFormatter.hh>

namespace simulations {
namespace observers {
namespace cartesian {

/** @brief Creates PDB-formatted strings from Vec3 coordinates with same atom names, one atom per residue
 *
 */
class SimplePdbFormatter : public AbstractPdbFormatter {
public:

  SimplePdbFormatter(const std::string & atom_name, const std::string & residue_name, const std::string & element_symbol);

  virtual ~SimplePdbFormatter() {}

  virtual void format(const simulations::systems::CartesianAtoms & system, std::vector<std::string> & sink) const;

  virtual void format(const simulations::systems::CartesianAtoms & system, std::ostream & sink) const;

private:
  std::string atom_name_;
  std::string residue_name_;
  std::string element_symbol_;
};


}
}
}
#endif
