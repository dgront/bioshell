#include <core/data/basic/Vec3I.hh>
#include <core/data/io/Pdb.hh>
#include <simulations/observers/cartesian/SimplePdbFormatter.hh>

namespace simulations {
namespace observers {
namespace cartesian {


SimplePdbFormatter::SimplePdbFormatter(const std::string & atom_name,const std::string & residue_name, const std::string & element_symbol) :
    atom_name_(atom_name), residue_name_(residue_name), element_symbol_(element_symbol) {}


void SimplePdbFormatter::format(const simulations::systems::CartesianAtoms & system,
    std::vector<std::string> & sink) const {

  for (core::index4 i = 0; i < system.n_atoms; ++i)
    sink.push_back(utils::string_format(core::data::io::Atom::atom_format_uncharged,
                                 i + 1, atom_name_.c_str(), ' ', residue_name_.c_str(), 'A', i + 1, ' ', system[i].x(),
                                 system[i].y(), system[i].z(), 1.0, 1.0, element_symbol_.c_str()));

}

void SimplePdbFormatter::format(const simulations::systems::CartesianAtoms & system,
    std::ostream & sink) const {

  for (core::index4 i = 0; i < system.n_atoms; ++i)
    sink << utils::string_format(core::data::io::Atom::atom_format_uncharged,
                                 i + 1, atom_name_.c_str(), ' ', residue_name_.c_str(), 'A', i + 1, ' ', system[i].x(),
                                 system[i].y(), system[i].z(), 1.0, 1.0, element_symbol_.c_str()) << "\n";
}

}
}
}
