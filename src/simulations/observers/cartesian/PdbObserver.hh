#ifndef SIMULATIONS_OBSERVERS_PdbObserver_HH
#define SIMULATIONS_OBSERVERS_PdbObserver_HH

#include<iostream>
#include <fstream>

#include <core/data/basic/Vec3.hh>
#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/observers/ToStreamObserver.hh>
#include <simulations/observers/cartesian/AbstractPdbFormatter.hh>

namespace simulations {
namespace observers {
namespace cartesian {

/** @brief Writes a PDB trajectory for an observed system.
 *
 */
class PdbObserver : public ToStreamObserver {
public:

  /** @brief Creates an observer that writes a conformation to a file in PDB format
   * @param observed_object - system whose coordinates will be stored in the file
   * @param pdb_format_source - biomolecular structure that corresponds to the system. PDB format data:
   *   - atom names
   *   - residue names
   *   - atom and residue numbering
   *   - chain ID
   * will be extracted from this structure. Coordinates will be taken from <code>observed_object</code>
   * @param out_fname - name of the output file
   */
  PdbObserver(const simulations::systems::CartesianAtoms & observed_object,
              AbstractPdbFormatter_SP pdb_formatter, const std::string & out_fname) :
              ToStreamObserver(out_fname), observed_object_(observed_object), pdb_formatter_(pdb_formatter) { observe_header(); }

  PdbObserver(const simulations::systems::CartesianAtoms & observed_object,
              AbstractPdbFormatter_SP pdb_formatter, Handler_SP handler) :
              ToStreamObserver(handler), observed_object_(observed_object), pdb_formatter_(pdb_formatter) {observe_header();}

  /// Default virtual destructor
  virtual ~PdbObserver() final { finalize(); }

  /// Append PDB - formatted data to the file
  bool observe() final;

  /** @brief Observe another set of coordinates to this stream.
   *
   * The given system should be a conformation of the same system! Otherwise the PDB format will be totally wrong.
   * This method has been introduced to save replica conformations to a single file.
   *
   * @param system - a conformation corresponding to the observed system
   * @return true
   */
  bool observe(const simulations::systems::CartesianAtoms & system);

  /// Append PDB - formatted data to the file
  void observe_header() {
    std::vector<std::string> h{"HEADER", "THEORETICAL_MODEL"};
    handler_->handle_header(h);
  }

  /** @brief Closes the file with the observations.
   */
  void finalize() final { }

  virtual core::index4 count_observe_calls() const { return cnt; }

private:
  core::index4  cnt = 0;
  const simulations::systems::CartesianAtoms & observed_object_;
  AbstractPdbFormatter_SP pdb_formatter_;
};


/** @brief A simple utility method that create a PdbObserver just to write a single conformation into a PDB file
 * @param observed_object - system whose coordinates will be stored in the file
 * @param pdb_format_source - biomolecular structure that corresponds to the system. PDB format data:
 * @param out_fname - name of the output file
 */
void write_pdb_conformation(const systems::CartesianAtoms &observed_object,
    std::shared_ptr<AbstractPdbFormatter> pdb_formatter, const std::string & out_fname);


}
}
}
#endif
