#ifndef SIMULATIONS_OBSERVERS_AbstractPdbFormatter_HH
#define SIMULATIONS_OBSERVERS_AbstractPdbFormatter_HH

#include <memory>
#include <ostream>
#include <simulations/systems/CartesianAtoms.hh>

namespace simulations {
namespace observers {
namespace cartesian {

/** @brief Abstract interface to create PDB-formatted strings from Vec3 coordinates
 *
 */
class AbstractPdbFormatter {
public:

  virtual ~AbstractPdbFormatter() {}

  virtual void format(const simulations::systems::CartesianAtoms & system, std::vector<std::string> & sink) const = 0;
  virtual void format(const simulations::systems::CartesianAtoms & system, std::ostream & sink) const = 0;
};

typedef typename std::shared_ptr<AbstractPdbFormatter> AbstractPdbFormatter_SP;

}
}
}
#endif
