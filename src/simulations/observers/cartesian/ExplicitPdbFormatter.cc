#include <core/data/io/Pdb.hh>
#include <simulations/observers/cartesian/ExplicitPdbFormatter.hh>

namespace simulations {
namespace observers {
namespace cartesian {


ExplicitPdbFormatter::ExplicitPdbFormatter(const core::data::structural::Structure &pdb_format_source) :
  logs("ExplicitPdbFormatter") {

  std::string pdb_atom_fmter = "ATOM  %5d %s %s %c%4d    %%8.3f%%8.3f%%8.3f  1.00 99.99";
  for (auto atom_it = pdb_format_source.first_const_atom(); atom_it != pdb_format_source.last_const_atom(); ++atom_it) {
    const auto a = **atom_it;
    const auto r = *(a.owner());
    format_lines.emplace_back(
        utils::string_format(pdb_atom_fmter, a.id(), a.atom_name().c_str(), r.residue_type().code3.c_str(),
                             r.owner()->char_id(), r.id()));
  }
}


void ExplicitPdbFormatter::format(const simulations::systems::CartesianAtoms & system,
                                              std::vector<std::string> & sink) const {

  for(core::index4 i=0;i<system.n_atoms;++i)
    sink.push_back(utils::string_format(format_lines[i], system[i].x(), system[i].y(), system[i].z()));
}


void ExplicitPdbFormatter::format(const simulations::systems::CartesianAtoms & system, std::ostream & sink) const {

  for (core::index4 i = 0; i < system.n_atoms; ++i)
    sink << utils::string_format(format_lines[i], system[i].x(), system[i].y(), system[i].z()) << "\n";

}


void ExplicitPdbFormatter::format(const std::unique_ptr<core::data::basic::Vec3I[]> & system, std::ostream & sink) const {

    for (core::index4 i = 0; i< format_lines.size();++i)
        sink << utils::string_format(format_lines[i], system[i].x(), system[i].y(), system[i].z()) << "\n";

}

void ExplicitPdbFormatter::format(const std::unique_ptr<core::data::basic::Vec3I[]> & system, std::vector<std::string> & sink) const {

    for(core::index4 i=0; i< format_lines.size();++i)
        sink.push_back(utils::string_format(format_lines[i], system[i].x(), system[i].y(), system[i].z()));
}


}
}
}
