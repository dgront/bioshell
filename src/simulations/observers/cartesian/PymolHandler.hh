#ifndef SIMULATIONS_OBSERVERS_PymolObserver_HH
#define SIMULATIONS_OBSERVERS_PymolObserver_HH

#include<iostream>
#include<arpa/inet.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include <iostream>
#include <fstream>

#include <core/data/basic/Vec3.hh>
#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/observers/Handler.hh>

namespace simulations {
namespace observers {
namespace cartesian {

/** @brief Sends a conformation to PyMol program.
 *
 * Inside PyMol you must start a UDP server that listens to a port where the data is sent.
 * @param observed_object - system whose coordinates will be stored in the file
 * @param pdb_format_source - biomolecular structure that corresponds to the system.
 * @param address - address of the UDP server that listens to the PDB data (usually this is "127.0.0.1")
 * @param port - port to listen to , by default 65000
 * @see AbstractPdbObserver<C>
 */
 // @todo Refactor this class into fully working handler
class PymolHandler : public Handler {
public:

  PymolHandler(const systems::CartesianAtoms &observed_object,
                const core::data::structural::Structure &pdb_format_source, const std::string address =
  "127.0.0.1", const size_t port = 65000) : Handler("") {

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    serv.sin_family = AF_INET;
    serv.sin_port = htons(65000);
    serv.sin_addr.s_addr = inet_addr(address.c_str());
    m = sizeof(serv);
  }

  /// Default virtual destructor
  ~PymolHandler() override = default;

  ///Writes observed values to handler
  virtual void write(const std::vector<std::string>&) {

  }

  void bond(const core::index4 i_atom, const core::index4 j_atom) {
    send_line(utils::string_format("BOND bond id#%d, id#%d", i_atom, j_atom));
    sendto(sockfd, "Q", 1, 0, (struct sockaddr *) &serv, m);
  }

  /// The virtual method does nothing in this class
  virtual void finalize() {}

  virtual std::shared_ptr<std::ostream> output_stream() { return nullptr; }

  virtual void output_stream(std::shared_ptr<std::ostream> out) {}

    bool handle(const std::vector<std::string> &v);

private:
  int sockfd;
  struct sockaddr_in serv;
  socklen_t m = 0;
  core::index4  cnt = 0;

  void send_line(const std::string &line) ;

};



}
}
}

#endif
