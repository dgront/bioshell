#ifndef SIMULATIONS_OBSERVERS_ExplicitPdbFormatter_HH
#define SIMULATIONS_OBSERVERS_ExplicitPdbFormatter_HH

#include <core/data/structural/Structure.hh>
#include <core/data/basic/Vec3I.hh>

#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/observers/cartesian/AbstractPdbFormatter.hh>

namespace simulations {
namespace observers {
namespace cartesian {

/** @brief Creates PDB-formatted strings from Vec3 coordinates using explicitly given PDB line formats
 *
 */
class ExplicitPdbFormatter : public AbstractPdbFormatter {
public:

  ExplicitPdbFormatter(const core::data::structural::Structure &pdb_format_source);

  virtual ~ExplicitPdbFormatter() {}

  virtual void format(const simulations::systems::CartesianAtoms & system,
                      std::vector<std::string> & sink) const;

  virtual void format(const simulations::systems::CartesianAtoms& system, std::ostream & sink) const;

  void format(const std::unique_ptr<core::data::basic::Vec3I[]> & system, std::vector<std::string> & sink) const;

  void format(const std::unique_ptr<core::data::basic::Vec3I[]> & system, std::ostream & sink) const;
private:
  std::vector<std::string> format_lines; ///< formatting string for every atom in the structure
  utils::Logger logs;
};


}
}
}
#endif
