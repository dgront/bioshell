#include <iostream>
#include <iomanip>

#include <simulations/observers/ObserveEnergyComponents.hh>

namespace simulations {
namespace observers {


ObserveEnergyComponents::ObserveEnergyComponents(forcefields::TotalEnergy &total_energy,
    systems::CartesianAtoms &scored_system, const std::string &fname, const bool observe_header_line) :
    ToStreamObserver(fname), logger("ObserveEnergyComponents"), total_energy_(total_energy),
    scored_system_(scored_system) {
  if (observe_header_line) observe_header();
}

ObserveEnergyComponents::ObserveEnergyComponents(forcefields::TotalEnergy &total_energy,
    systems::CartesianAtoms &scored_system, Handler_SP handler, const bool observe_header_line) :
    ToStreamObserver(handler), logger("ObserveEnergyComponents"), total_energy_(total_energy),
    scored_system_(scored_system) {
  if (observe_header_line) observe_header();
}


bool ObserveEnergyComponents::observe() {

  ++cnt;
  if (!ObserverInterface::trigger_->operator()()) return false;
  std::vector<std::string> observed_data;

  observed_data.push_back(utils::string_format("%5d", cnt));

  double en = 0.0;
  const std::vector<double> &factors = total_energy_.get_factors();

  std::stringstream str;
  str << std::fixed;
  for (core::index2 i = 0; i < total_energy_.count_components(); ++i) {
    double ee = total_energy_.get_component(i)->energy(scored_system_);
    observed_data.push_back(utils::string_format('%' + utils::to_string(total_energy_.get_sw()[i]) + ".2f", ee));
    en += ee * factors[i];
  }
  observed_data.insert(observed_data.begin() + 1,
      utils::string_format("%" + utils::to_string(total_energy_.name().size()) + ".2f", en));
  observed_data.push_back(total_energy_.get_forcefield_tag());
  handler_->write(observed_data);
  return true;
}

void ObserveEnergyComponents::finalize() {

}

std::string ObserveEnergyComponents::header_string() const {

  return total_energy_.header_string();
}

void ObserveEnergyComponents::observe_header(const std::string &prefix) {
  std::vector<std::string> header_vector = utils::split(header_string(), {' '}, false, false);
  header_vector.insert(header_vector.begin(), prefix);
  handler_->handle_header(header_vector);
}

} // ~ simulations
} // ~ observers
