#ifndef SIMULATIONS_OBSERVERS_ObserveEnergyComponents_HH
#define SIMULATIONS_OBSERVERS_ObserveEnergyComponents_HH

#include <vector>
#include <memory>
#include <iostream>

#include <utils/Logger.hh>

#include <simulations/evaluators/Evaluator.hh>
#include <simulations/observers/ToStreamObserver.hh>
#include <simulations/forcefields/TotalEnergy.hh>

namespace simulations {
namespace observers {

using simulations::evaluators::Evaluator_SP;

/** @brief Observes all energy components of a given TotalEnergyByResidue instance.
 *
 * Each observation makes a row of energy values in the output stream.
 */
class ObserveEnergyComponents : public virtual ToStreamObserver, public std::enable_shared_from_this<ObserverInterface>{
public:

  /** @brief Creates an observer that evaluates and writes energy components as a nice table into a file
   * @param total_energy - total energy of a system; energy components contained in that object will be evaluated and printed
   * in a single row at each <code>observe()</code> call
   * @param fname - name of the output file
   * @param observe_header_line - if true (and this is the default), the constructor will call
   *    <code>observe_header()</code> method which writes header line
   */
  ObserveEnergyComponents(forcefields::TotalEnergy & total_energy, systems::CartesianAtoms & scored_system, const std::string & fname, const bool observe_header_line = true);

  /** @brief Creates an observer that evaluates and writes energy components as a nice table into a file
  * @param total_energy - total energy of a system; energy components contained in that object will be evaluated and printed
  * in a single row at each <code>observe()</code> call
  * @param handler - handler object to handle output
  * @param observe_header_line - if true (and this is the default), the constructor will call
  *    <code>observe_header()</code> method which writes header line
  */
  ObserveEnergyComponents(forcefields::TotalEnergy & total_energy, systems::CartesianAtoms & scored_system, Handler_SP handler, const bool observe_header_line = true);

  /// Virtual destructor
  virtual ~ObserveEnergyComponents() {}

  virtual bool observe();

  virtual void finalize();

  /// Returns the header of an energy table as a string
  std::string header_string() const;

  /** @brief Writes the energy table header line to the stream.
   *
   * @param prefix - a string to be printed just ahead of the header line. By default a "#     " string is used
   * so the header line is interpreted as a comment e.g. by gnuplot. Also, <code>observe()</code> method prints
   * observation counter in the first column and the '#' character works as an id for this column
   */
  void observe_header(const std::string & prefix = "#    ");

private:
  utils::Logger logger;
  forcefields::TotalEnergy & total_energy_;
  systems::CartesianAtoms & scored_system_;
  core::index4 cnt = 0;
};

} // ~ simulations
} // ~ observers
#endif
