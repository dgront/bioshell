/** @file ScreenHandler.hh
 *  @brief A class to represent observers handler to the screen
 */
#ifndef SIMULATIONS_OBSERVERS_ScreenHandler_HH
#define SIMULATIONS_OBSERVERS_ScreenHandler_HH

#include <memory>
#include <ostream>
#include <iostream>
#include <vector>
#include <fstream>

#include <simulations/observers/Handler.hh>

namespace simulations {
namespace observers {

/** @brief Base class to represent observers handler to the screen
 *
 * A class for handlers writing to the screen
 */
class ScreenHandler: public Handler {
public:
  ///Construtor
  ScreenHandler(): Handler(""){};

  /// destructor (empty)
  ~ScreenHandler() {};

  void handle_header(const std::vector<std::string>& v){
      set_header(v);
      write(v);
  }

  void write(const std::vector<std::string>& v){
      for (auto i=v.cbegin();i!=v.cend();++i)
      std::cout<<(*i)<<" ";
      std::cout<<"\n";
    }

  void finalize(){};

};
/// Declares a shared pointer to ScreenHandler type
typedef std::shared_ptr<ScreenHandler> ScreenHandler_SP;

}
}

#endif
