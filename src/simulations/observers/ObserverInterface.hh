/** @file ObserverInterface.hh
 *  @brief Provides ObserverInterface and ObserverInterface_SP types
 */
#ifndef SIMULATIONS_OBSERVERS_ObserverInterface_HH
#define SIMULATIONS_OBSERVERS_ObserverInterface_HH

#include <memory>

#include <simulations/observers/ObserverTrigger.hh>
#include <simulations/observers/FileHandler.hh>
#include <simulations/observers/ScreenHandler.hh>


namespace simulations {
namespace observers {

/** @brief Observer interface provides <code>bool observe()</code> method.
 *
 * The purpose of an observer is to record a value or values during sampling. The observed value may be written to
 * a stream, stored in a histogram, etc.
 */
class ObserverInterface {
public:

  /// Create an observer which records every observations
  ObserverInterface(Handler_SP handler,ObserverTrigger_SP trigger = std::make_shared<ObserverTrigger>()) : trigger_(trigger),handler_(handler) {}

  /// Create an observer with FileHanlder from string filename
  ObserverInterface(const std::string & filename,ObserverTrigger_SP trigger = std::make_shared<ObserverTrigger>()) : trigger_(trigger) {
      if ((filename=="") || (filename=="stdout")||(filename=="cout"))
          handler_ =std::dynamic_pointer_cast<Handler>(std::make_shared<ScreenHandler>());
      else
          handler_ =std::dynamic_pointer_cast<Handler>(std::make_shared<FileHandler>(filename));
  }

  /// Virtual destructor (empty)
  virtual ~ObserverInterface() {}

  /// This method is called to take observations (to be implemented by a derived class)
  virtual bool observe() = 0;

  /// This method will be called before the program shuts down, e.g. to close open files
  virtual void finalize() = 0;

  /** @brief Replaces the trigger for this observer with a new one.
   *
   * This call will change the way how often observations are taken
   *
   * @param new_trigger - instance of a new trigger object
   */
  void trigger(ObserverTrigger_SP new_trigger) { trigger_ = new_trigger; }

  /**@brief Provides a const-access to the trigger used by this observer.
   * @return an observation trigger object
   */
  const ObserverTrigger_SP trigger() const { return trigger_; }

  /// Sets the new handler to this observer
  void handler(Handler_SP handler){ handler_ = handler; }

  /// Gives a pointer to a observer's handler
  Handler_SP handler(){ return handler_; }

protected:
  ObserverTrigger_SP trigger_;
  Handler_SP handler_;
};

/// Declares a shared pointer to ObserverInterface type
typedef std::shared_ptr<ObserverInterface> ObserverInterface_SP;
}
}

#endif
