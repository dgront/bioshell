/** @file SQLiteHandler.hh
 *  @brief A class to represent observers handler as a SQLite database
 */
#ifdef SQLITE3
#ifndef SIMULATIONS_OBSERVERS_SQLiteHandler_HH
#define SIMULATIONS_OBSERVERS_SQLiteHandler_HH

#include <memory>
#include <ostream>
#include <iostream>
#include <vector>
#include <fstream>
#include <sqlite3.h>

#include <simulations/observers/Handler.hh>
#include <utils/string_utils.hh>

namespace simulations {
namespace observers {


/** @brief Base class to represent observers handler as a SQLite database
 *
 * A class for handlers represented as a SQLite database
 */
class SQLiteHandler: public Handler {
public:

  ///Construtor
  SQLiteHandler( const std::string & table_name,const std::string & out_fname="simulation.db"): Handler(out_fname),table_name_(table_name){
      int exit=0;
      exit = sqlite3_open(out_fname_.c_str(), &database_ptr);
      if (exit) {
          logger_<< utils::LogLevel::WARNING<< "Error open DB " << sqlite3_errmsg(database_ptr) ;
      }
      else
          logger_<< utils::LogLevel::FINE << "Opened Database Successfully!" ;
  };

  /// destructor (empty)
  ~SQLiteHandler() {};

  void handle_header(const std::vector<std::string>& v) {
      set_header(v);
  }
  void write_header(const std::vector<std::string>& v) {
      std::string sql = "CREATE TABLE IF NOT EXISTS "+table_name_+"(\n";

      for (auto i=0;i<header_.size();++i) {
          if (std::find(header_[i].begin(), header_[i].end(), '#') != header_[i].end())
              sql += "n_observation INT,\n";
          else
              {
              std::string a(v[i]);
              utils::trim(a," ");
              if (utils::is_integer(utils::trim(a," "))==true)  sql += utils::trim(header_[i]," ") + " INT,\n";
              else if (utils::is_floating_point(utils::trim(a," "))==true) sql +=  utils::trim(header_[i]," ") + " REAL,\n";
              else sql +=  utils::trim(header_[i]," ") + " TEXT,\n";
              }
      }
      sql.pop_back();
      sql.pop_back();
      sql+=");";
      int exit = 0;
      char* messaggeError;
      exit = sqlite3_exec(database_ptr, sql.c_str(), NULL, 0, &messaggeError);
      if (exit != SQLITE_OK) {
          logger_<< utils::LogLevel::WARNING << "Error Create Table" << messaggeError<<"\n"<<sql<<"\n";
          sqlite3_free(messaggeError);
      }
      else
          logger_<< utils::LogLevel::FINE << "Table created Successfully";

  }

  void write(const std::vector<std::string>& v) {
      std::string sql("INSERT INTO "+table_name_+" VALUES(");
      ++cnt_;
      if (v.size()!=header_.size()) {
          if (v.size() == header_.size() - 1)
              sql += utils::to_string(cnt_) + ",";
          else
              throw std::runtime_error("SQLiteHandler:  different number of columns in header and observations! Check if every observer has its own Handler");
      }
      if (!if_table_created) {
          write_header(v);
          if_table_created=true;
      }

      for (auto i=0;i<v.size();++i)
      {
          std::string a(v[i]);
          if (utils::is_floating_point(v[i])|utils::is_integer(v[i])) sql +=  v[i] + ",";
          else sql +=  "'"+utils::trim(a," ") + "',";
      }

      sql.pop_back();
      sql+=");";
      char* messaggeError;
      int exit = sqlite3_exec(database_ptr, sql.c_str(), NULL, 0, &messaggeError);
      if (exit != SQLITE_OK) {
          logger_<< utils::LogLevel::WARNING << "Error Insert ";
          sqlite3_free(messaggeError);
      }
      else
          logger_<< utils::LogLevel::FINE  << "Records created Successfully!";

  }

  void finalize(){sqlite3_close(database_ptr);};

private:
    bool if_table_created=false;
    sqlite3* database_ptr;
    std::string table_name_;
    core::index4 cnt_=0;
};
    /// Declares a shared pointer to SQLiteHandler type
typedef std::shared_ptr<SQLiteHandler> SQLiteHandler_SP;

}
}

#endif
#endif

