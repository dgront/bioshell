#ifndef BIOSHELL_HANDLETOVECTOR_HH
#define BIOSHELL_HANDLETOVECTOR_HH

#include <simulations/observers/Handler.hh>

/// @brief Custom handler to keep output strings in a vector
class HandleToVector : public simulations::observers::Handler {
public:
  HandleToVector() : Handler("") {}

  void write(const std::vector<std::string>& v) override {
    for(const std::string & l:v)
      lines.push_back(l);
  }

  void handle_header(const std::vector<std::string>& v) override {}

  std::vector<std::string> lines;
};

#endif //BIOSHELL_HANDLETOVECTOR_HH
