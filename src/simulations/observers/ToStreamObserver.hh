/** @file ToStreamObserver.hh
 *  @brief Observer that can change a stream used to write observations.
 */
#ifndef SIMULATIONS_OBSERVERS_ToStreamObserver_HH
#define SIMULATIONS_OBSERVERS_ToStreamObserver_HH

#include <memory>
#include <ostream>

#include <simulations/observers/ObserverInterface.hh>

namespace simulations {
namespace observers {

/** @brief Observer which can expose the pointer to its output stream.
 *
 * This can be a base class for observers that write observations to a file. It provides a method to expose
 * the pointer to the output stream and ability to substitute stream pointer with another one,
 * which effectively changes the destination where observations are written.
 */
class ToStreamObserver : public ObserverInterface {
public:
    ///Creates object from handler
    ToStreamObserver(Handler_SP handler):ObserverInterface(handler){}

    ///Creates object with FileHandler from string filename
    ToStreamObserver(const std::string & filename):ObserverInterface(filename){}

  /// Virtual destructor (empty)
  virtual ~ToStreamObserver() {}

};

}
}

#endif
