#ifndef SIMULATIONS_OBSERVERS_AdjustMoversAcceptance_HH
#define SIMULATIONS_OBSERVERS_AdjustMoversAcceptance_HH

#include <simulations/observers/ObserveMoversAcceptance.hh>

namespace simulations {
namespace observers {

using simulations::evaluators::Evaluator_SP;

/** @brief At every <code>observe()</code> call adjusts movers so acceptance rate matches the assumed target value
 *
 * Each observation makes a row of acceptance rate values in the output stream.
 */
class AdjustMoversAcceptance : public virtual ObserveMoversAcceptance {
public:


  /** @brief Creates an observer that evaluates and writes energy components as a file
   * @param movers_set - MoversSet object
   * @param fname - name of the output file
   */
  AdjustMoversAcceptance(simulations::movers::MoversSetSweep &movers_set, const std::string &fname,
                         double target_acceptance_rate = 0.4) : ToStreamObserver(fname), ObserveMoversAcceptance(movers_set, fname),
    logger("AdjustMoversAcceptance"), target_rate_(target_acceptance_rate) {}

    AdjustMoversAcceptance(simulations::movers::MoversSetSweep &movers_set, Handler_SP handler,
                           double target_acceptance_rate = 0.4) : ToStreamObserver(handler), ObserveMoversAcceptance(movers_set, handler),
                                                                  logger("AdjustMoversAcceptance"), target_rate_(target_acceptance_rate) {}

  /// Virtual destructor
  virtual ~AdjustMoversAcceptance() {}

  virtual bool observe();

  void set_target_acceptance_rate(double target_acceptance_rate) { target_rate_ = target_acceptance_rate; }

private:
  utils::Logger logger;
  double target_rate_;
};

/// Declare a type of a shared pointer to AdjustMoversAcceptance
typedef std::shared_ptr<AdjustMoversAcceptance> AdjustMoversAcceptance_SP;

} // ~ simulations
} // ~ observers
#endif
