#ifndef SIMULATIONS_OBSERVERS_ObserveWLSampling_HH
#define SIMULATIONS_OBSERVERS_ObserveWLSampling_HH

#include <simulations/observers/ObserverInterface.hh>
#include <simulations/sampling/WangLandauSampler.hh>
#include <simulations/evaluators/Timer.hh>

namespace simulations {
namespace observers {

/** @brief Observe Wang-Landau sampling process.
 *
 * This observer prints the current state of the two histograms that governs a Wang-Landau sampling process:
 * the H histogram (the one that must be flat) and the S histogram, which estimates the microcanonical entropy
 * of a system
 */
class ObserveWLSampling : public ObserverInterface {
public:

  /** @brief Create a new observer that writes histograms to a single file
   * @param sampler - sampler to be observed
   * @param file_name - output file name
   */
  ObserveWLSampling(const sampling::WangLandauSampler & sampler, const std::string & file_name);

    /** @brief Create a new observer that writes histograms to a single file
     * @param sampler - sampler to be observed
     * @param handler - output handler
     */
    ObserveWLSampling(const sampling::WangLandauSampler & sampler, Handler_SP handler);

  /// Default virtual destructor
  virtual ~ObserveWLSampling() = default;

  bool observe() override;

  void observe_header(const std::string prefix="#    ");

  /** @brief Does nothing, the file is always kept closed.
   */
  void finalize() override {}

private:
  const sampling::WangLandauSampler & sampler_;
  evaluators::Timer timer;
};

} // ~ sampling
} // ~ simulations

#endif
