#include <vector>
#include <iostream>
#include <iomanip>

#include <utils/Logger.hh>

#include <simulations/evaluators/Evaluator.hh>
#include <simulations/observers/ObserveEvaluators.hh>

namespace simulations {
namespace observers {


void ObserveEvaluators::add_evaluator(evaluators::Evaluator_SP evaluator) {

  evaluators.push_back(evaluator);
  sw.push_back(evaluator->width());
  logger << utils::LogLevel::INFO << "observing new evaluator: " << evaluator->header() << "\n";
}

std::string ObserveEvaluators::header_string() const {

  std::stringstream ss;
  for (const auto &e : evaluators) ss << " " << e->header() << " ";

  return ss.str();
}

void ObserveEvaluators::finalize() {

}

bool ObserveEvaluators::observe() {

  ++cnt;
  if (!trigger_->operator()()) return false;
  std::vector<std::string> observed_data;
  observed_data.push_back(utils::string_format("%5d", cnt));
  for (size_t i = 0; i < evaluators.size(); ++i) {
    std::string s = evaluators[i]->to_string();
    observed_data.push_back(s);
  }
  handler_->write(observed_data);
  return true;
}

} // ~ simulations
} // ~ observers
