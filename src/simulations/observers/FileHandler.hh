/** @file FileHandler.hh
 *  @brief A class to represent observers handler file
 */
#ifndef SIMULATIONS_OBSERVERS_FileHandler_HH
#define SIMULATIONS_OBSERVERS_FileHandler_HH

#include <memory>
#include <ostream>
#include <iostream>
#include <vector>
#include <fstream>

#include <simulations/observers/Handler.hh>

namespace simulations {
namespace observers {

/** @brief Base class to represent observers handler as file
 *
 * A class for handlers represented as file
 */
class FileHandler : public Handler {
public:
  /**
   * @brief Construtor cerates a handler that write observations into a file
   * @param out_fname - output file name
   */
  FileHandler(const std::string &out_fname) : Handler(out_fname) {
    std::ofstream out(out_fname_, std::fstream::out);
    out.close();
  };

  /// destructor (empty)
  ~FileHandler() {};

  /** @brief Set a header for a file
   * @param v - vector of strings (e.g column titles)
   */
  void handle_header(const std::vector<std::string> &v) {
    set_header(v);
    write(v);
  }

  /** @brief Write observations  into a file
   * @param v - vector of observations as strings
   */
  void write(const std::vector<std::string> &lines) {
    std::ofstream out(out_fname_, std::fstream::out| std::fstream::app);
    for (const auto & line: lines) out << line << " ";
    out << "\n";
    out.close();
  }

  void finalize() {};

};

/// Declares a shared pointer to FileHandler type
typedef std::shared_ptr<FileHandler> FileHandler_SP;

}
}

#endif
