/** @file TriggerLowEnergy.hh
 *  @brief Provides TriggerLowEnergy object that triggers observations when energy is low enough
 */
#ifndef SIMULATIONS_OBSERVERS_TriggerLowEnergy_HH
#define SIMULATIONS_OBSERVERS_TriggerLowEnergy_HH

#include <memory>
#include <utils/Logger.hh>

#include <simulations/forcefields/Energy.hh>
#include <simulations/observers/ObserverTrigger.hh>

namespace simulations {
namespace observers {

/** @brief Triggers observation when energy is low enough.
 */
class TriggerLowEnergy : public ObserverTrigger {
public:

  /** @brief Creates a trigger that observes only low-energy events
   * All other will be neglected.
   *
   * @param n - energy function used to guide this trigger. The energy function is evaluated at every
   * <code>observe()</code> call
   */
  TriggerLowEnergy(simulations::forcefields::Energy & energy, systems::CartesianAtoms & conformation,
	const double low_energy_value = 0, const double fraction = 0.1) : energy_(energy), conformation_(conformation),
	low_energy_value_(low_energy_value), fraction_(fraction), logs("TriggerLowEnergy") {}

  /// Default virtual destructor
  virtual ~TriggerLowEnergy() = default;

  /** @brief Accepts only low-energy observations
   *
   * @return true if the current energy value is low enough
   */
  virtual bool operator()() {

    ++count_observe_calls_;
    double en = energy_.energy(conformation_);
    double cutoff = (low_energy_value_ < 0) ? (1.0 - fraction_) * low_energy_value_ : (1.0 + fraction_) *
                                                                                          low_energy_value_;
    if (en < cutoff) {
      if (en < low_energy_value_) {
        low_energy_value_ = en;
        logs << utils::LogLevel::FINE << "trigger min-energy set to " << en << "\n";
      }
      ++count_observed_;
      return true;
    }
    return false;
  }

  /// Says how many times this trigger actually forced an observation
  core::index4 count_observed() const override { return count_observed_; }

private:
  simulations::forcefields::Energy & energy_;
  systems::CartesianAtoms & conformation_;
  double low_energy_value_;
  double fraction_;
  core::index4 count_observed_;
  utils::Logger logs;
};

}
}

#endif
