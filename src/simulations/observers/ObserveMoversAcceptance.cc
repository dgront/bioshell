#include <iostream>
#include <iomanip>

#include <simulations/observers/ObserveMoversAcceptance.hh>

namespace simulations {
namespace observers {

bool ObserveMoversAcceptance::observe() {

  ++cnt;
  if(!ObserverInterface::trigger_->operator()()) return false;
  std::vector<std::string> observed_data=utils::split(utils::to_string(ms_), {','},false,false);
  observed_data.insert(observed_data.begin(),utils::string_format("%5d",cnt));
  handler_->write(observed_data);
  return true;
}

void ObserveMoversAcceptance::finalize() {

}

} // ~ simulations
} // ~ observers
