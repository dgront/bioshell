#include <iostream>
#include <iomanip>

#include <simulations/observers/AdjustMoversAcceptance.hh>

namespace simulations {
namespace observers {

bool AdjustMoversAcceptance::observe() {


  if(!ObserverInterface::trigger_->operator()()) return false;

  for (core::index2 im = 0; im < ms_.count_movers(); ++im) {
    const auto m = ms_.get_mover(im);
    double rate = m->get_success_rate();
    if (target_rate_ - rate > 0.02) m->max_move_range(m->max_move_range() * 0.9);
    if (m->max_move_range_allowed() > m->max_move_range()) {
      if (rate - target_rate_ > 0.02) m->max_move_range(m->max_move_range() * 1.1);
    }
  }
    ObserveMoversAcceptance::observe();


  return true;
}

} // ~ simulations
} // ~ observers
