#ifndef SIMULATIONS_OBSERVERS_ToHistogramObserver_HH
#define SIMULATIONS_OBSERVERS_ToHistogramObserver_HH

#include <memory>
#include <ostream>

#include <core/calc/statistics/Histogram.hh>

#include <simulations/observers/ObserverInterface.hh>
#include <simulations/evaluators/Evaluator.hh>


namespace simulations {
namespace observers {

/** @brief Observer which can expose the pointer to its output stream.
 *
 * This can be a base class for observers that write observations to a file. It provides a method to expose
 * the pointer to the output stream and ability to substitute stream pointer with another one,
 * which effectively changes the destination where observations are written.
 */
class ToHistogramObserver : public ObserverInterface {
public:
  /// Creates object from handler
  ToHistogramObserver(evaluators::Evaluator_SP e, double min_e, double max_e, double step, Handler_SP handler)
      : ObserverInterface(handler), h_(step, min_e, max_e) { e_ = e;}

  /// Creates object with FileHandler from string filename
  ToHistogramObserver(evaluators::Evaluator_SP e, double min_e, double max_e, double step, const std::string &filename)
      : ObserverInterface(filename), h_(step, min_e, max_e) { e_ = e;}

  /// Virtual destructor (empty)
  ~ToHistogramObserver() override = default;

  /// Writes the histogram to an array of strings and then send these strings to the respective handler
  void finalize() override;

  /// Makes an observation by storing the observed value in a histogram
  bool observe() override;

  /// Provides read-only access to the histogram
  const core::calc::statistics::HistogramD4 & histogram() const { return h_; }

private:
  evaluators::Evaluator_SP e_;
  core::calc::statistics::HistogramD4 h_;   // Histogram<double, index4>
};

}
}

#endif
