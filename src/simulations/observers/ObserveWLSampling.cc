#include <iomanip>
#include <fstream>

#include <simulations/observers/ObserveWLSampling.hh>

namespace simulations {
namespace observers {

ObserveWLSampling::ObserveWLSampling(const sampling::WangLandauSampler & sampler, const std::string & file_name) :ObserverInterface(file_name), sampler_(sampler) {
observe_header();}

ObserveWLSampling::ObserveWLSampling(const sampling::WangLandauSampler & sampler, Handler_SP handler) :ObserverInterface(handler), sampler_(sampler) {
    observe_header();}

/// This method is called to take observations
bool ObserveWLSampling::observe() {

  if(!ObserverInterface::trigger_->operator()()) return false;

  const std::map<int, int> & flat = sampler_.get_histogram_H();
  const std::map<int, double> & dos = sampler_.get_histogram_S();
  const std::map<int, double> & E = sampler_.get_histogram_E();
  const std::map<int, unsigned long int > & C = sampler_.get_histogram_C();

  double mean = 0.0;
  for (const auto &it : flat) mean += it.second;
  mean /= (double) flat.size();

  const auto minimal = std::min_element(dos.begin(), dos.end(),
                                        [](std::pair<int, double> lhs, std::pair<int, double> rhs) {
                                            return lhs.second < rhs.second;
                                        });
  std::vector<std::string> observed_data;
  long double sum_of_states = 0;
  double min_dos = (*minimal).second;
  core::index4 n_obs = trigger()->count_observed();
  for (const auto &i : dos) {
    int bin = i.first;
    std::string a= utils::string_format("%6d,%6d,%10.2f,%10d,%7.5f,%10.2f,%10d",
                                n_obs, bin, i.second - min_dos, flat.at(bin), flat.at(bin) / mean,
                                E.at(bin) / C.at(bin), C.at(bin));
    sum_of_states += std::exp(i.second - min_dos);
    observed_data = utils::split(a, {','},false,false);
    handler_->write(observed_data);
  }

  return true;
}
void ObserveWLSampling::observe_header(const std::string prefix){
    std::vector<std::string> a{prefix," en_bin", "       DoS", "      H"," H_relative"," en_mean", " total_counts"};
    handler_->handle_header(a);
}


} // ~ observers
} // ~ simulations
