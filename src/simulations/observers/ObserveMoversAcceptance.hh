#ifndef SIMULATIONS_OBSERVERS_ObserveMoversAcceptance_HH
#define SIMULATIONS_OBSERVERS_ObserveMoversAcceptance_HH

#include <vector>
#include <memory>
#include <iostream>

#include <utils/Logger.hh>

#include <simulations/evaluators/Evaluator.hh>
#include <simulations/observers/ToStreamObserver.hh>
#include <simulations/movers/MoversSetSweep.hh>

namespace simulations {
namespace observers {

using simulations::evaluators::Evaluator_SP;

/** @brief Observes acceptance rate for every mover contained in the given MoversSet.
 *
 * Each observation makes a row of acceptance rate values in the output stream.
 */
class ObserveMoversAcceptance : public virtual ToStreamObserver {
public:

  /** @brief Creates an observer that evaluates and writes energy components as a file
   * @param movers_set - MoversSet object
   * @param fname - name of the output file
   */
  ObserveMoversAcceptance(simulations::movers::MoversSetSweep &movers_set, const std::string &fname) : ToStreamObserver(fname),
    logger("ObserveMoversAcceptance"), ms_(movers_set) {
      observe_header();
  }
/** @brief Creates an observer that evaluates and writes energy components as a file
   * @param movers_set - MoversSet object
   * @param handler -  output handler
   */
    ObserveMoversAcceptance(simulations::movers::MoversSetSweep &movers_set, Handler_SP handler) : ToStreamObserver(handler),
                                                                                                         logger("ObserveMoversAcceptance"), ms_(movers_set) {
        observe_header();
    }

  /// Virtual destructor
  virtual ~ObserveMoversAcceptance() {}

  virtual bool observe();

  virtual void finalize();

  /// Returns the header of an output table as a string
  std::string header_string() const { return ms_.header_string(); }

  /** @brief Writes the movers acceptance ratios table header line to the stream.
   *
   * @param prefix - a string to be printed just ahead of the header line. By default a '#' character is used
   * so the header line is interpreted as a comment e.g. by gnuplot
   */
  void observe_header(const std::string &prefix = "#    ") {
      std::vector<std::string> header_vector = utils::split(header_string(), {','},false,false);
      header_vector.insert(header_vector.begin(),prefix);
      handler_->handle_header(header_vector);}

  virtual core::index4 count_observe_calls() const { return cnt; }

private:
  utils::Logger logger;

protected:
  simulations::movers::MoversSetSweep &ms_;
  core::index4 cnt = 0;
};

/// Declare a type of a shared pointer to ObserveMoversAcceptance
typedef std::shared_ptr<ObserveMoversAcceptance> ObserveMoversAcceptance_SP;

} // ~ simulations
} // ~ observers
#endif
