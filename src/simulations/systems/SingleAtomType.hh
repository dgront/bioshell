#ifndef SIMULATIONS_SYSTEMS_SingleAtomType_HH
#define SIMULATIONS_SYSTEMS_SingleAtomType_HH

#include <core/index.hh>

#include <simulations/systems/AtomTypingInterface.hh>

namespace simulations {
namespace systems {

/** @brief There is only one atom type in this atom typing.
 *
 * By default every atom is identified by this typing as an alpha carbon (named <code>" CA "</code>),
 * but the name may ba changed. Its code is always 0
 */
class SingleAtomType : public AtomTypingInterface {
public:

  explicit SingleAtomType(const std::string & atom_type_name = " CA ", const std::string & residue_type_name = "ALA")
  : ca_name(atom_type_name), res_name(residue_type_name) {}

  /// @brief Virtual destructor to satisfy a compiler
  ~SingleAtomType() override = default;

  /// @brief Always returns 0, because there is only one atom type according to this atom typing
  core::index1 atom_type(const std::string & atom_internal_name) const override { return 0; }

  /// @brief Always returns 0, no matter what the atom is <code>a</code>
  core::index1 atom_type(const core::data::structural::PdbAtom & a) const override { return 0; }

  /// @brief Always returns 0, because there is only one atom type according to this atom typing
  core::index1 atom_type(const std::string & atom_name, const std::string & residue_name, const AtomTypingVariants variant) const override { return 0; }

  /// @brief Always returns the atom type used to construct this object
  const std::string & atom_internal_name(const core::index1 atom_id) const override { return ca_name; }

  /// @brief Always returns the atom type used to construct this object
  const std::string & atom_internal_name(const core::data::basic::Vec3I & atom) const { return ca_name; }

  /// @brief Always returns 0the atom type used to construct this object
  virtual core::index2 atom_id(const std::string & atom_internal_name) const { return 0; }

  /// @brief Just return 1
  core::index1 count_atom_types() const override { return 1; }

  /** @brief Returns true if a given string has been registered as an atom type
   * @param atom_name - name of the requested atom, as written in the PDB file used to start a simulation
   * @param residue_name - name of a residue
   * @param variant - variant of the residue; typically AtomTypingVariants::STANDARD should be used
   *    unless its a special case for this residue, e.g. it is the N-terminal or C-terminal one
   * @return true if the @code atom_name @endcode equals the name declared for this object; false otherwise
   */
  bool is_known_atom_type(const std::string & atom_name, const std::string & residue_name,
                          const AtomTypingVariants variant) const override { return atom_name == ca_name; }

  /** @brief Returns true if a given string has been registered as an atom type
   * @param a - PdbAtom object
   * @return true if the name of a give atom equals the name declared for this object; false otherwise
   */
  bool is_known_atom_type(const core::data::structural::PdbAtom &a) const override { return a.atom_name() == ca_name; }

  virtual bool is_known_residue_type(const std::string & residue_name,
            const systems::AtomTypingVariants variant) const override { return residue_name == res_name; }

  /// @brief Always returns 0, because there is only one residue type according to this atom typing
  core::index1 residue_type( const std::string & residue_name, const AtomTypingVariants variant) const override { return 0; }

  const std::string & residue_internal_name(const core::index1 residue_id) const override { return res_name; }

private:
  const std::string ca_name;
  const std::string res_name;
};

}
}
#endif
