#include <simulations/systems/cabs/CabsAtomTyping.hh>

namespace simulations {
namespace systems {
namespace cabs {

const std::vector<std::string> CabsAtomTyping::cabs_atom_names = {
    " CA ",
    " bb  ",
    " CB ",
    " SC "
};

const std::vector<std::string> CabsAtomTyping::cabs_residue_types = {
    "ALA", "ARG", "ASN", "ASP",
    "CYS", "GLN", "GLU", "GLY", "HIS", "ILE", "LEU", "LYS",
    "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"
};




}
}
}
