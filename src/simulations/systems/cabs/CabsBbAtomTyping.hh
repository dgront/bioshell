#ifndef SIMULATIONS_SYSTEMS_CABS_CabsBbAtomTyping_HH
#define SIMULATIONS_SYSTEMS_CABS_CabsBbAtomTyping_HH

#include <vector>
#include <string>

#include <core/index.hh>

#include <simulations/systems/SimpleAtomTyping.hh>

namespace simulations {
namespace systems {
namespace cabs {

/** @brief Atom typing for <code>cabs-bb</code> representation.
 *
 */
class CabsBbAtomTyping : public SimpleAtomTyping {
public:
  /// Names of all distinct atoms defined in the CABS++ model
  static const std::vector<std::string> cabsbb_atom_names;
  /// Names of all residue types CABS force field can handle
  static const std::vector<std::string> cabsbb_residue_types;

  /// Constructor will initialize the object based on pre-defined atom names for the CABS++ representation
  CabsBbAtomTyping() : SimpleAtomTyping(cabsbb_residue_types, cabsbb_atom_names, cabsbb_atom_names) {}

  /// Virtual destructor to satisfy a compiler
  ~CabsBbAtomTyping() final = default;
};

}
}
}

#endif

