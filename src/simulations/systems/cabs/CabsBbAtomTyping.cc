#include <simulations/systems/cabs/CabsBbAtomTyping.hh>

namespace simulations {
namespace systems {
namespace cabs {

const std::vector<std::string> CabsBbAtomTyping::cabsbb_atom_names = {
  " N  ",
  " CA ",
  " C  ",
  " O  ",
  " CB ",
  " SC1",
  " SD1",
  " SE1",
  " SF1",
  " SH1",
  " SI1",
  " SK1",
  " SL1",
  " SM1",
  " SN1",
  " SP1",
  " SQ1",
  " SR1",
  " SS1",
  " ST1",
  " SV1",
  " SW1",
  " SY1"
};

const std::vector<std::string> CabsBbAtomTyping::cabsbb_residue_types = {
    "ALA", "ARG", "ASN", "ASP",
    "CYS", "GLN", "GLU", "GLY", "HIS", "ILE", "LEU", "LYS",
    "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"
};

}
}
}
