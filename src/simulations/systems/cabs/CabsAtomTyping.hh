#ifndef SIMULATIONS_SYSTEMS_CABS_CabsAtomTyping_HH
#define SIMULATIONS_SYSTEMS_CABS_CabsAtomTyping_HH

#include <vector>
#include <string>

#include <core/index.hh>

#include <simulations/systems/SimpleAtomTyping.hh>

namespace simulations {
namespace systems {
namespace cabs {

/** @brief Atom typing for the original <code>cabs</code> representation.
 */
class CabsAtomTyping : public SimpleAtomTyping {
public:
  /// Names of all distinct atoms defined in the CABS model
  static const std::vector<std::string> cabs_atom_names;
  static const std::vector<std::string> cabs_residue_types;

  /// Constructor will initialize the object based on pre-defined atom names for the original CABS representation
  CabsAtomTyping() : SimpleAtomTyping(cabs_residue_types, cabs_atom_names, cabs_atom_names) {}

  /// Virtual destructor to satisfy a compiler
  virtual ~CabsAtomTyping() {}
};

}
}
}

#endif

