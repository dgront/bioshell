#ifndef SIMULATIONS_SYSTEMS_CartesianChains_HH
#define SIMULATIONS_SYSTEMS_CartesianChains_HH

#include <string>
#include <memory>

#include <core/index.hh>

#include <simulations/systems/AtomRange.hh>
#include <simulations/systems/AtomTypingInterface.hh>
#include <simulations/systems/AtomTypingVariants.hh>

#include <simulations/systems/BuildFluidSystem.fwd.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/systems/CartesianAtoms.hh>

namespace simulations {
namespace systems {

/** @brief Atomic system in the Cartesian space composed of atoms, residues and chains.
 *
 * @tparam C - the type used to express coordinates; Vec3 for simple models, Vec3Cubic for modeling in periodic boundary conditions
 */
class CartesianChains : public CartesianAtoms {
  friend BuildFluidSystem;
public:

  /** @brief Creates a new object based on a biomolecular structure.
   *
   * @param typing - atom typing object that is used to convert between "biochemical" atom types (e.g. aromatic carbon)
   *   into integers indexing atom types in a desired force field
   * @param s - a Structure object, typically read from a PDB file
   */
  CartesianChains(const AtomTypingInterface_SP typing, const core::data::structural::Structure & s);

  /** @brief Creates a system of of <code>n_atoms</code> residues, each of them comprising one atom, placed in a single chain
   *
   * Every atom is assigned to chain 'A', its atom-type is set according to the given PDB-style atom name and residue name
   *
   * @param typing - object used to translate between PDB atom names and force field atom types
   * @param n_atoms - the total number of atoms in this system
   * @param atom_name - string representing a PDB-style atom name (the same for all atoms)
   * @param residue_name - string representing a PDB-style residue name (the same for all atoms)
   */
  CartesianChains(const AtomTypingInterface_SP typing, const core::index4 n_atoms, const std::string & atom_name = " CA ", const std::string & residue_name = "ALA");

  /** @brief Creates a system of of multiple chains.
   *
   * @param typing - object used to translate between PDB atom names and force field atom types
   * @param n_atoms_in_chains - the number of residues (one atom each) in each chain
   * @param atom_name - string representing a PDB-style atom name (the same for all atoms)
   */
  CartesianChains(const AtomTypingInterface_SP typing, const std::vector<core::index2> & n_atoms_in_chains,
                  const std::string & atom_name = " CA ");

  /// Necessary virtual destructor
  ~CartesianChains() override {};

  /** @brief Returns the number of atoms in a given residue.
   *
   * The total number of atoms in this system may be checked by calling <code>CartesianAtoms::count_atoms();</code>
   * @param residue_index - which residue?
   */
  inline core::index2 count_atoms(const core::index2 residue_index) const { return atoms_for_residue_[residue_index].last_atom - atoms_for_residue_[residue_index].first_atom + 1; }

  /** @brief look for an atom by its internal name.
   *
   * Attempts to find an atom called <code>atom_name</code> in a residue <code>i_res</code>. Throws an exception
   * if nothing was found.
   *
   * @param i_res - residue index
   * @param internal_atom_name - name of an atom to look for
   * @return a reference to the atom of interest
   * @throw std::range_error if the requested atom cannot be found
   */
  core::data::basic::Vec3I & find_atom(const core::index2 i_res, const std::string & internal_atom_name) {

    using base = CartesianAtoms;
    // --- we get the first atom from the residue just to read the type of that residue
    core::index2 residue_type = base::operator[](atoms_for_residue_[i_res].first_atom).residue_type;
    // --- here we obtain the type ot the atom, according to the force field (i.e. the relevant AtomTyping)
    core::index2 type = CartesianAtoms::atom_typing->atom_type(
      internal_atom_name,core::chemical::Monomer::get(residue_type).code3,AtomTypingVariants::STANDARD);

    for (core::index4 i = atoms_for_residue_[i_res].first_atom; i <= atoms_for_residue_[i_res].last_atom; ++i)
      if (base::operator[](i).atom_type == type) return base::operator[](i);

    throw std::range_error(utils::string_format("Can't find atom %s  in residue %d",internal_atom_name.c_str(),i_res));
  }

  /** @brief look for an atom by its internal name.
   *
   * Attempts to find an atom called <code>internal_atom_name</code> in a residue <code>i_res</code>.
   *
   * @param i_res - residue index
   * @param internal_atom_name - name of an atom to look for. The name should follow internal atom names used
   * by the force field (i.e. AtomTyping instance) rather than just PDB codes
   * @return index of a requested atoms or <code>std::numeric_limits<core::index4>::max()</code> when not found
   */
  core::index4 find_atom_index(const core::index2 i_res, const std::string & internal_atom_name) const {

    using base = CartesianAtoms;

    // --- we get the first atom from the residue just to read the type of that residue
    core::index2 residue_type = base::operator[](atoms_for_residue_[i_res].first_atom).residue_type;
    // --- here we obtain the type ot the atom, according to the force field (i.e. the relevant AtomTyping)
    core::index2 type = CartesianAtoms::atom_typing->atom_type(
      internal_atom_name, core::chemical::Monomer::get(residue_type).code3,AtomTypingVariants::STANDARD);
    // --- and finally we iterate over all atoms from the residue and look for the one of the requested type.
    for (core::index4 i = atoms_for_residue_[i_res].first_atom; i <= atoms_for_residue_[i_res].last_atom; ++i) {
      if (base::operator[](i).atom_type == type) return i;
    }

    return std::numeric_limits<core::index4>::max();
  }

  /** @brief look for an atom by the order number of a residue this atom belongs to and its atom_type.
   *
   * Attempts to find an atom called <code>atom_type</code> in a residue <code>i_res</code>.
   *
   * @param i_res - residue index
   * @param atom_type - atom typing of an atom to look for; that integer must match atom types defined by
   *    @code AtomTypingInterface @endcode used to create this object
   * @return index of a requested atoms or <code>std::numeric_limits<core::index4>::max()</code> when not found
   */
  core::index4 find_atom_index(const core::index2 i_res, const core::index2 & type) const {

    using base = CartesianAtoms;

    for (core::index4 i = atoms_for_residue_[i_res].first_atom; i <= atoms_for_residue_[i_res].last_atom; ++i) {
      if (base::operator[](i).atom_type == type) return i;
    }

    return std::numeric_limits<core::index4>::max();
  }

  /** @brief Returns the type of the residue the given atom belongs to.
   *
   * This method simply returns Vec3::residue_type value for the relevant atom.
   * @param atom_index index of the atom of interest
   */
  inline core::index1 residue_type(const core::index4 atom_index) const { return CartesianAtoms::operator[](atom_index).residue_type; }

  /// Counts residues of this system
  inline core::index2 count_residues() const { return atoms_for_residue_.size(); }

  /// Counts chains of this system
  inline core::index2 count_chains() const { return atoms_for_chain_.size(); }

  /// Returns an atom range for a given residue
  inline const AtomRange & atoms_for_residue(const core::index2 residue_index) const {
    return atoms_for_residue_[residue_index];
  }

  /// Returns the index of the residue a given atom belongs to
  inline core::index2 residue_for_atom(const core::index4 atom_index) const {

    core::index4 from = 0;
    core::index4 to = atoms_for_residue_.size() - 1;
    core::index4 i_res = (to + from)/2;
    while(!atoms_for_residue_[i_res].is_my_atom(atom_index)) {
      if(atoms_for_residue_[i_res].first_atom > atom_index) {
        to = i_res - 1;
        i_res = (to + from)/2;
      } else {
        from = i_res + 1;
        i_res = (to + from)/2;
      }
    }

    return i_res;
  }

  /// Returns an atom range for a given chain
  inline const AtomRange & atoms_for_chain(const core::index2 chain_index) const {
    return atoms_for_chain_[chain_index];
  }

  /// Returns a chain index a given atom index
  inline core::index2 chain_for_atom(const core::index4 which_atom) const {

    for (core::index2 i = 0; i < count_chains(); ++i) {
      if ((which_atom >= atoms_for_chain_[i].first_atom) && (which_atom <= atoms_for_chain_[i].last_atom)) return i;
    }
    throw std::out_of_range(utils::string_format(
      "Can't find chain index for atom %d. The system has only %d atom in %d residues, %d chains\n",
      which_atom, CartesianAtoms::n_atoms, count_residues(), count_chains()));
  }

  /// Returns integer ID of this system
  inline core::index2 system_id() const { return system_id_; }

  /// Sets a new integer ID for this system
  inline void system_id(core::index2 id) { system_id_ = id; }

  /** @brief Returns a string ID of a chain
   * @param chain_index - index (order number) of a chain
   * @return string ID of that chain
   */
  const std::string& chain_id(const core::index2 chain_index) const override { return chain_ids_[chain_index]; }

  /** @brief Returns an index of a chain
   * @param chain_id - ID string of a chain
   * @return index of a chain for the given ID or -1 if that ID can't be found in this system
   */
  int chain_index(const std::string & chain_id) const override;

  /** @brief the number of atoms of the largest residue of this system
   * @return the maximum number of atoms a molecule / residue of this system has
   */
  core::index4 longest_residue() const;

protected:
  std::vector<AtomRange> atoms_for_residue_;
  std::vector<AtomRange> atoms_for_chain_;
  std::vector<std::string> chain_ids_;

private:
  core::index2 system_id_;
  utils::Logger logger;
};


} // ~ simulations
} // ~ cartesian

#endif
