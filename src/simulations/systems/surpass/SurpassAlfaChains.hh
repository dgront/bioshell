#ifndef SIMULATIONS_CARTESIAN_SURPASS_SurpassAlfaChains_HH
#define SIMULATIONS_CARTESIAN_SURPASS_SurpassAlfaChains_HH

#include <string>

#include <utils/Logger.hh>

#include <core/data/basic/Vec3I.hh>
#include <simulations/systems/surpass/SurpassAlfaAtomTyping.hh>
#include <simulations/systems/CartesianChains.hh>
//#include <simulations/representations/SurpassSecondaryStructure.hh>


namespace simulations {
namespace systems {
namespace surpass {

/** @brief System consisting of SURPASS chains
 */
class SurpassAlfaChains : public CartesianChains {
public:

  /** @brief Creates a new SurpassModel object based on a biomolecular structure
   *
   * @param s - a Structure object - can be in SURPASS representation or all-atom. In the latter case the constructor
   * will convert all-atom model into the SURPASS representation
   */
  explicit SurpassAlfaChains(const core::data::structural::Structure &s);

    /** @brief Creates a new SurpassModel object based on a biomolecular structure
   *
   * @param s - a Structure object - can be in SURPASS representation or all-atom. In the latter case the constructor
   * will convert all-atom model into the SURPASS representation
   */
    SurpassAlfaChains(SurpassAlfaChains &s);


  /// Necessary virtual destructor
  virtual ~SurpassAlfaChains() {}

  /// Update coordinates of all SURPASS atoms
  void update_surpass_atoms(core::index4 first,core::index4 last);

  const std::string & surpass_ss2() const {return surpass_ss2_;}

  core::index4 n_surpass_atoms;

    /** @brief Provide a read-only access to SURPASS atoms.
   */
  const std::vector<core::data::basic::Vec3I> & surpass_atoms() const { return surpass_atoms_; }

    /** @brief Prepares system for scoring
    */
    virtual void prepare_for_scoring() { update_surpass_atoms(0,n_atoms-1); }

    /** @brief Clears system after scoring
     */
    virtual void clear_after_scoring() { update_surpass_atoms(0,n_atoms-1); }

    /** @brief Prepares system for scoring
    */
    virtual void prepare_for_scoring(core::index4 which_atom) { update_surpass_atoms(which_atom,which_atom); }

    /** @brief Clears system after scoring
     */
    virtual void clear_after_scoring(core::index4 which_atom) { update_surpass_atoms(which_atom,which_atom); }

    /** @brief Prepares system for scoring
  */
    virtual void prepare_for_scoring(core::index4 atom_from,core::index4 atom_to) { update_surpass_atoms(atom_from,atom_to); }

    /** @brief Clears system after scoring
     */
    virtual void clear_after_scoring(core::index4 atom_from,core::index4 atom_to) { update_surpass_atoms(atom_from,atom_to); }

private:
  utils::Logger logger;
  std::string surpass_ss2_;
  std::vector<core::data::basic::Vec3I> surpass_atoms_;

  /// Creates secondary structure string of SURPASS atoms representation
  void create_surpass_ss2(const core::data::structural::Structure &s);
};

}
} // ~ simulations
} // ~ cartesian

#endif
