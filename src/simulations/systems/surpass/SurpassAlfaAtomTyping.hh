#ifndef SIMULATIONS_CARTESIAN_SurpassAlfaAtomTyping_HH
#define SIMULATIONS_CARTESIAN_SurpassAlfaAtomTyping_HH

#include <vector>
#include <string>

#include <core/index.hh>

#include <simulations/systems/SimpleAtomTyping.hh>

namespace simulations {
namespace systems {
namespace surpass {

/** @brief Atom typing for <code>SURPASS-Alfa</code> model.
 */
class SurpassAlfaAtomTyping : public SimpleAtomTyping {
public:

  /// Constructor will initialize the base class object with a list of SURPASS-Alfa atom types
  SurpassAlfaAtomTyping();

  /// Virtual destructor to satisfy a compiler
  ~SurpassAlfaAtomTyping() override = default;
};

}
}
}

#endif

