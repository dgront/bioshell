#include <simulations/representations/surpass_utils.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>
#include <core/data/basic/Vec3I.hh>


namespace simulations {
namespace systems {
namespace surpass {

/** @brief Creates a new SurpassModel object based on a biomolecular structure
 *
 * @param s - a Structure object - can be in SURPASS representation or all-atom. In the latter case the constructor
 * will convert all-atom model into the SURPASS representation
 */
SurpassAlfaChains::SurpassAlfaChains(const core::data::structural::Structure &s)
  : CartesianChains(std::make_shared<SurpassAlfaAtomTyping>(), s), logger("SurpassAlfaChains"),
    n_surpass_atoms(n_atoms-3), surpass_atoms_(n_atoms-3) {
    logger << utils::LogLevel::INFO << "Creating a system based on " << s.code() << "\n";
    update_surpass_atoms(0,n_atoms-1);
    create_surpass_ss2(s);
}

SurpassAlfaChains::SurpassAlfaChains(SurpassAlfaChains &s)
  : CartesianChains(s), logger("SurpassAlfaChains"),n_surpass_atoms(n_atoms-3), surpass_atoms_(n_atoms-3),
  surpass_ss2_(s.surpass_ss2_) {
    logger << utils::LogLevel::INFO << "Creating a system as a deep copy\n";
    for (core::index4 i = 0; i < n_surpass_atoms; ++i) {
        surpass_atoms_[i].set(s.surpass_atoms_[i]);
        surpass_atoms_[i].chain_id = s.surpass_atoms_[i].chain_id;
        surpass_atoms_[i].atom_type = s.surpass_atoms_[i].atom_type;
        surpass_atoms_[i].residue_type = s.surpass_atoms_[i].residue_type;
    }
}

void SurpassAlfaChains::update_surpass_atoms(core::index4 first,core::index4 last){
    core::index4 start = (int(first)-3 > 0) ? int(first)-3: 0;
    core::index4 stop = (last <= int(n_atoms-1)-3) ? last : int(n_atoms-1)-3; // indexes are till n_atoms-1 and -3 is to make ai+3 still in atom range
    Vec3 ai_0,ai_1, ai_2, ai_3;
    for (auto ai=start;ai<=stop;++ai){
        ai_0.set(operator[](ai));
        operator[](ai+1).get_closest(operator[](ai),ai_1);
        operator[](ai+2).get_closest(operator[](ai),ai_2);
        operator[](ai+3).get_closest(operator[](ai),ai_3);
        ai_0+=ai_1;
        ai_0+=ai_2;
        ai_0+=ai_3;
        surpass_atoms_[ai].set(ai_0/=4.0);
     //   std::cout<<ai<<" "<<surpass_atoms_[ai].x()<<" "<<surpass_atoms_[ai].y()<<" "<<surpass_atoms_[ai].z()<<"\n";
    }
}
void SurpassAlfaChains::create_surpass_ss2(const core::data::structural::Structure &s) {
    //atom_type in Vec3I stores info about ss2 type H=0, E=1, C=2
    // residue_type stores info which element in order it is - coils are 0 and rest are numbered from 1
    core::index4 ss2_cnt=1;
    std::string ss4(4, 'C');
    char ss;
    core::index2 chain_cnt=0;
 //   for (auto res=s.first_const_residue();res!=s.last_const_residue();res++)
    for (auto &chain:s) {
        for (auto i = 0; i < chain->count_residues() - 3; ++i) {
            ss4[0] = (*chain)[i]->ss();
            ss4[1] = (*chain)[i + 1]->ss();
            ss4[2] = (*chain)[i + 2]->ss();
            ss4[3] = (*chain)[i + 3]->ss();
            ss = simulations::representations::surpass_ss(ss4);
            if (i != 0) {
                if (ss != surpass_ss2_[surpass_ss2_.size() - 1]) {
                    if (ss != 'C') ss2_cnt += 1;
                }
            } else if (ss == 'C') ss2_cnt = 0;
            surpass_ss2_ += ss;
            if (ss == 'H') {
                surpass_atoms_[chain_cnt+i].atom_type = 0;
                surpass_atoms_[chain_cnt+i].residue_type = ss2_cnt;
            } else if (ss == 'E') {
                surpass_atoms_[chain_cnt+i].atom_type = 1;
                surpass_atoms_[chain_cnt+i].residue_type = ss2_cnt;
            } else if (ss == 'C') {
                surpass_atoms_[chain_cnt+i].atom_type = 2;
                surpass_atoms_[chain_cnt+i].residue_type = 0;
            }
        }
        chain_cnt+=1;
    }
}

}
} // ~ simulations
} // ~ cartesian

