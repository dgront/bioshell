#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/systems/AtomTypingInterface.hh>


namespace simulations {
namespace systems {


std::string CartesianAtoms::chain_code_ = "A";

CartesianAtoms::CartesianAtoms(const AtomTypingInterface_SP typing, const size_t n_atoms) :
    atom_typing(typing), n_atoms(n_atoms), coordinates_(n_atoms),
    logger("CartesianAtoms") {

  logger << utils::LogLevel::INFO << "A new system of " << CartesianAtoms::n_atoms << " atoms created\n";

  for (core::index2 i = 0; i < CartesianAtoms::n_atoms; i++) {
    coordinates_[i].residue_type = 0;
    coordinates_[i].atom_type = 0;
    coordinates_[i].chain_id = 0;
  }
}

CartesianAtoms::CartesianAtoms(const CartesianAtoms &source) :
    atom_typing(source.atom_typing), n_atoms(source.n_atoms), coordinates_(n_atoms),
    logger("CartesianAtoms") {

  logger << utils::LogLevel::FINE << "A new system of " << CartesianAtoms::n_atoms << " atoms copied\n";

  for (core::index4 i = 0; i < n_atoms; ++i) {
    coordinates_[i].set(source.coordinates_[i]);
    coordinates_[i].chain_id = source.coordinates_[i].chain_id;
    coordinates_[i].atom_type = source.coordinates_[i].atom_type;
    coordinates_[i].residue_type = source.coordinates_[i].residue_type;
  }
}

CartesianAtoms::CartesianAtoms(core::data::structural::Chain &c) : n_atoms(c.count_atoms()), coordinates_(n_atoms), logger("CartesianAtoms")  {

  core::index4 i = 0;
  for (core::data::structural::Chain::atom_iterator ai = c.first_atom(); ai != c.last_atom(); ai++) {
    coordinates_[i].set(*(*ai));
    i++;
  }
}


void calculate_cm(const CartesianAtoms & system, core::data::basic::Vec3I & cm_pos) {

  cm_pos.set(0.0);
  for (core::index4 i = 0; i < system.n_atoms; i++)
    cm_pos += (system[i]);
  cm_pos /= ((double) system.n_atoms);
}


void calculate_cm(const CartesianAtoms & system, core::data::basic::Vec3 & cm_pos) {

  cm_pos.set(0.0);
  for (core::index4 i = 0; i < system.n_atoms; i++) {
    cm_pos.x += system[i].x();
    cm_pos.y += system[i].y();
    cm_pos.z += system[i].z();
  }
  cm_pos /= ((double) system.n_atoms);
}

void CartesianAtoms::center_at(const double cx, const double cy, const double cz) {

  core::data::basic::Vec3I c;
  calculate_cm(*this, c);
  c *= -1.0;
  c.add(cx, cy, cz);
  for (core::index4 i = 0; i < n_atoms; i++)
    coordinates_[i] += c;
}

double calculate_rg_square(const CartesianAtoms & system) {

  core::data::basic::Vec3 cm;
  calculate_cm(system, cm);

  double s = 0, n = 0, cc = 0;
  for (core::index4 i = 0; i < system.n_atoms; i++) {
    ++n;
    const auto &c = system[i];
    cc = c.x() - cm.x;
    s += cc * cc;
    cc = c.y() - cm.y;
    s += cc * cc;
    cc = c.z() - cm.z;
    s += cc * cc;
  }

  return s / n;
}

} // ~ simulations
} // ~ cartesian


