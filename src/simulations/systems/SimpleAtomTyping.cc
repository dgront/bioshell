#include <core/index.hh>

#include <simulations/systems/SimpleAtomTyping.hh>

namespace simulations {
namespace systems {

SimpleAtomTyping::SimpleAtomTyping(const std::vector<std::string> & allowed_residue_names,
          const std::vector<std::string> & internal_atom_names,
          const std::vector<std::string> & pdb_atom_names)
          : AtomTypingInterface(internal_atom_names), residue_names_(allowed_residue_names) {}

core::index1 SimpleAtomTyping::atom_type(const std::string & atom_name, const std::string & residue_name,
                               const AtomTypingVariants variant) const {
  return AtomTypingInterface::atom_type(atom_name);
}

core::index1 SimpleAtomTyping::atom_type(const core::data::structural::PdbAtom & a) const {

  if(!AtomTypingInterface::is_known_atom_type(a.atom_name()))
    return 255;
  else
    return AtomTypes::atom_type(a.atom_name());
}

bool SimpleAtomTyping::is_known_atom_type(const std::string &atom_name, const std::string &residue_name,
                                          const AtomTypingVariants variant) const {
  return AtomTypingInterface::is_known_atom_type(atom_name);
}

bool SimpleAtomTyping::is_known_atom_type(const core::data::structural::PdbAtom &a) const {
  return AtomTypingInterface::is_known_atom_type(a.atom_name());
}

bool SimpleAtomTyping::is_known_residue_type(const std::string & residue_name,
                           const systems::AtomTypingVariants variant) const {
  return std::find(residue_names_.begin(), residue_names_.end(), residue_name) != residue_names_.end();
}

core::index1 SimpleAtomTyping::residue_type(const std::string & residue_name,
                          const systems::AtomTypingVariants variant) const {
  return std::distance(residue_names_.begin(), std::find(residue_names_.begin(), residue_names_.end(), residue_name));
}

const std::string & SimpleAtomTyping::residue_internal_name(const core::index1 residue_id) const {
  return residue_names_[residue_id];
}

}
}

