#ifndef SIMULATIONS_CARTESIAN_SimpleAtomTyping_HH
#define SIMULATIONS_CARTESIAN_SimpleAtomTyping_HH

#include <vector>
#include <string>

#include <core/index.hh>

#include <simulations/systems/AtomTypingInterface.hh>

namespace simulations {
namespace systems {

/** @brief Atom typing where each residue may have the same atom types.
 *
 * This atom typing does not support different atom typing variants, i.e. every residue
 * is treated as AtomTypingVariants::STANDARD
 * This is a rather basic atom typing to used in simple (e.g. coarse grained) models such as CABS or SURPASS.
 */
class SimpleAtomTyping : public AtomTypingInterface {
public:

  /// Constructor will initialize the object based on its static data
  SimpleAtomTyping(const std::vector<std::string> & allowed_residue_names,
                   const std::vector<std::string> & internal_atom_names, const std::vector<std::string> & pdb_atom_names);

  /// Virtual destructor to satisfy a compiler
  ~SimpleAtomTyping() override = default;

  core::index1 atom_type(const std::string & atom_name, const std::string & residue_name,
                                 const AtomTypingVariants variant) const override;

  /** @brief Returns an atom type index for a PdbAtom object.
   * @param a - PDB atom object
   * @return index1 index for that atom
   */
  core::index1 atom_type(const core::data::structural::PdbAtom & a) const override;

  bool is_known_atom_type(const std::string & atom_name, const std::string & residue_name,
                          const AtomTypingVariants variant) const override;

  bool is_known_atom_type(const core::data::structural::PdbAtom &a) const override;

  bool is_known_residue_type(const std::string & residue_name,
                                     const systems::AtomTypingVariants variant) const override;

  core::index1 residue_type(const std::string & residue_name,
                                    const systems::AtomTypingVariants variant) const override;

  const std::string & residue_internal_name(const core::index1 residue_id) const override;

private:
  const std::vector<std::string> residue_names_;
};

}
}

#endif

