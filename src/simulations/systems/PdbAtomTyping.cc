#include <algorithm>
#include <set>
#include <iomanip>
#include <core/data/structural/Structure.hh>
#include <simulations/systems/PdbAtomTyping.hh>
#include <core/data/sequence/SequenceProfile.hh>

namespace simulations {
namespace systems {

std::vector<std::string> list_3codes_aminoacids() {
    std::vector<std::string> out;
    for(const auto & m: core::data::sequence::SequenceProfile::aaOrderByNCBI()) out.push_back(m.code3);
    return out;
}

utils::Logger PdbAtomTyping::l("PdbAtomTyping");

PdbAtomTyping::PdbAtomTyping(const std::string& filename) :AtomTypingInterface(){
    std::string line;
    std::shared_ptr<std::istream> infile;
    infile = std::static_pointer_cast<std::istream>(std::make_shared<std::ifstream>(filename));
    if (!*infile) l<< utils::LogLevel::SEVERE << "Can't open file: " << filename << "\n";
    l<< utils::LogLevel::FILE << "Reading PDB from: " << filename << "\n";
    while (std::getline(*infile, line)) {
        register_atom_type(line.substr(0,4));
    }

}

bool PdbAtomTyping::is_known_residue_type(const std::string & residue_name,
                                   const systems::AtomTypingVariants variant) const{
return std::find(residues_names.begin(),residues_names.end(),residue_name)!=residues_names.end();
}

core::index1 PdbAtomTyping::residue_type(const std::string & residue_name,
                                  const systems::AtomTypingVariants variant) const{
    return std::distance(residues_names.begin(), std::find(residues_names.begin(),residues_names.end(),residue_name));
}

const std::string & PdbAtomTyping::residue_internal_name(const core::index1 residue_id) const{
return residues_names[residue_id];
}

bool PdbAtomTyping::is_known_atom_type(const std::string & atom_name, const std::string & residue_name,
                                const AtomTypingVariants variant) const {
    return AtomTypes::is_known_atom_type(atom_name);

}

bool PdbAtomTyping::is_known_atom_type(const core::data::structural::PdbAtom &a) const {
    return AtomTypes::is_known_atom_type(a.atom_name());
}

core::index1 PdbAtomTyping::atom_type(const std::string & atom_name, const std::string & residue_name,
                               const AtomTypingVariants variant) const{
    return AtomTypes::atom_type(atom_name);
}

core::index1 PdbAtomTyping::atom_type(const core::data::structural::PdbAtom & a) const {
    return AtomTypes::atom_type(a.atom_name());
}


}
}