#ifndef SIMULATIONS_SYSTEMS_CartesianAtoms_HH
#define SIMULATIONS_SYSTEMS_CartesianAtoms_HH

#include <string>
#include <memory>

#include <core/data/basic/Vec3I.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/systems/CartesianAtoms.fwd.hh>
#include <simulations/systems/AtomTypingInterface.hh>


namespace simulations {
namespace systems {

/** @brief A simple system of atoms defined in the Cartesian space.
 *
 * This object does not carry information on residues nor chains the atoms belong to. If you need to
 * keep atoms in higher-level object, use CartesianChains derived class
 *
 */
class CartesianAtoms {
public:
  /// AtomTyping object converts PDB-style atom names into internal numbering used by a force field
  const AtomTypingInterface_SP atom_typing;
  const core::index4 n_atoms; ///< The total number of atoms in this system

  /** @brief Create a new system on <code>n_atoms</code> atoms.
   *
   * Residue and atom types are set to 0 for every atom.
   * @param typing - object used to convert internal atom type indexes to PDB atom names and vice versa
   * @param n_atoms - the number of atoms in this system
   */
  CartesianAtoms(const AtomTypingInterface_SP typing, const size_t n_atoms);

  /** @brief Copying constructor.
   *
   * @param source - a system to be duplicated
   */
  explicit CartesianAtoms(const CartesianAtoms& source);

  /** @brief Creates a system based on atoms from a given chain.
   *
   * The resulting system will have as many atoms as the source chain. The information about residues
   * the atoms belong to is lost.
   * @param c - all atoms of this Chain object will be copied to the newly created system.
   */
  explicit CartesianAtoms(core::data::structural::Chain & c);

  /// Necessary virtual destructor
  virtual ~CartesianAtoms() = default;

  /** @brief Moves the center of this system to a new location in 3D
   * It might be useful when placing a single chain in the middle of a box
   * @param center - the new center of mass (CM) location
   */
  inline void center_at(const Vec3 & center) { center_at(center.x, center.y, center.z); }

  /** @brief Moves the center of this system to a new location in 3D
   * It might be useful when placing a single chain in the middle of a box
   * @param cx - x coordinate of the new center of mass (CM) location
   * @param cy - y coordinate of the new CM location
   * @param cz - z coordinate of the new CM location
   */
  void center_at(const double cx, const double cy, const double cz);

  /** @brief Index-operator returns coordinates of a requested atom
   * @param index - index of an atom; starts from 0 of course
   */
  inline core::data::basic::Vec3I& operator[](const core::index4 & index) { return coordinates_[index]; }

  /** @brief Index-operator returns coordinates of a requested atom
   * @param index - index of an atom; starts from 0 of course
   */
  inline const core::data::basic::Vec3I& operator[](const core::index4 & index) const { return coordinates_[index]; }

  /// Provides const access to the coordinates pointer
  inline const std::vector<core::data::basic::Vec3I>& coordinates() const { return coordinates_; }

  /** @brief Returns the box size
   * @return side length of a simulation box the system lives in
   */
  inline double box_side() const { return coordinates_[0].get_box_len(); }

  /** @brief Returns a string ID of a chain
   * Even if a modelled system consists of single atoms, chain ID is required to save conformations in PDB format
   * @param chain_index - index (order number) of a chain
   * @return string ID of that chain
   */
  virtual const std::string& chain_id(const core::index2 chain_index) const { return chain_code_; }

  /** @brief Sets a new value for the string identifying this system instance.
   *
   * That string will show up in simulation's output
   * @param new_tag - a new tag string (no gaps, please!)
   */
  void set_system_tag(const std::string &new_tag) { system_tag = new_tag; }

  /** @brief Returns an index of a chain
   * @param chain_id - ID string of a chain
   * @return always 0 because CartesianAtomsSimple always contains a single chain
   */
  virtual int chain_index(const std::string & chain_id) const { return 0; }

  /** @brief Returns a string that identifies this system.
   *
   * @return a string ID
   */
  const std::string &get_system_tag() const { return system_tag; }

  /** @brief Prepares system for scoring
  */
  virtual void prepare_for_scoring() {}

  /** @brief Clears system after scoring
   */
  virtual void clear_after_scoring() {}

  /** @brief Prepares system for scoring
  */
    virtual void prepare_for_scoring(core::index4 which_atom) {}

    /** @brief Clears system after scoring
     */
    virtual void clear_after_scoring(core::index4 which_atom) {}

    /** @brief Prepares system for scoring
  */
    virtual void prepare_for_scoring(core::index4 atom_from,core::index4 atom_to) {}

    /** @brief Clears system after scoring
     */
    virtual void clear_after_scoring(core::index4 atom_from,core::index4 atom_to) {}
private:
   std::vector<core::data::basic::Vec3I> coordinates_; ///< Atomic coordinates
  utils::Logger logger;
  std::string system_tag;
  static std::string chain_code_;
};

/// Computes the center of mass of the whole system
void calculate_cm(const CartesianAtoms & system, core::data::basic::Vec3 & cm_pos);

/// Computes the center of mass of the whole system
void calculate_cm(const CartesianAtoms & system, core::data::basic::Vec3I & cm_pos);

/// Computes the squared gyration radius for a given system
double calculate_rg_square(const CartesianAtoms & system);

/** @brief Copies given coordinates into this chain.
 */
template<typename It>
void set_conformation(It begin, It end, CartesianAtoms & destination) {

  core::index4 i = 0;
  for (It it = begin; it != end; ++it) {
    destination[i].set(**it);
    ++i;
  }
}

typedef typename std::shared_ptr<CartesianAtoms> CartesianAtoms_SP;

} // ~ simulations
} // ~ cartesian

#endif
