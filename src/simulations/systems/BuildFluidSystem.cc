#include <memory>

#include <core/data/basic/Vec3Cubic.hh>
#include <core/data/basic/Vec3I.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <simulations/systems/BuildFluidSystem.hh>

namespace simulations {
namespace systems {

void PointGridGenerator::center(double x, double y, double z) {

  cx_ = x - box_width_ / 2.0;
  cy_ = y - box_width_ / 2.0;
  cz_ = z - box_width_ / 2.0;
}

SimpleCubicGrid::SimpleCubicGrid(double box_length, core::index4 n_points) : PointGridGenerator(box_length, n_points) {

  points_one_side_ = std::ceil(std::pow(n_points, 1.0 / 3));
  w_ = box_width_ / ((double) points_one_side_);
  cell_margin_ = w_ / 2.0;

}

core::data::basic::Vec3 SimpleCubicGrid::coordinates(core::index4 which_point) const {

  return coordinates(which_point % points_one_side_ ,
                     (which_point / points_one_side_) % points_one_side_,
                     (which_point / (points_one_side_ * points_one_side_)) % points_one_side_);
}

core::data::basic::Vec3 SimpleCubicGrid::coordinates(core::index2 i, core::index2 j, core::index2 k) const {

  return core::data::basic::Vec3{float(w_ * i + cell_margin_ + cx_), float(w_ * j + cell_margin_ + cy_), float(w_ * k + cell_margin_ + cz_)};
}


void BuildFluidSystem::generate(CartesianAtoms & system, const core::data::structural::PdbAtom & a,
                                const PointGridGenerator_SP & grid) {

  if (grid->n_points() != system.n_atoms)
    throw std::invalid_argument("incorrect size of the given chain!");

  for (core::index4 i_atom = 0; i_atom < system.n_atoms; i_atom++) {
    system[i_atom].set(grid->coordinates(i_atom));
  }
}


void BuildFluidSystem::generate(CartesianChains &system,
          const core::data::structural::Residue &r, const PointGridGenerator_SP & grid) {

  if (r.count_atoms() * grid->n_points() != system.n_atoms)
    throw std::invalid_argument("incorrect size of the given chain!");

  system.atoms_for_residue_.clear(); // --- remove definitions of all residues to make them new

  core::data::basic::Vec3I cm {r.cm()};
  core::index4 i_atom = 0;
  for (core::index4 i_mol = 0; i_mol < grid->n_points(); i_mol++) {
    system.atoms_for_residue_.emplace_back(i_atom, i_atom + r.count_atoms() - 1);
    core::data::basic::Vec3I c{grid->coordinates(i_mol)};
    c -= cm;
    for (const core::data::structural::PdbAtom_SP & a:r) {
      system[i_atom].set(*a);
      system[i_atom] += c;
      ++i_atom;
    }
  }
}

core::data::structural::Structure_SP BuildFluidSystem::build_structure(
    const core::data::structural::Residue & r, const PointGridGenerator_SP & grid,
    const std::string &structure_id, const std::string & chain_id) {

  core::data::structural::Structure_SP s = std::make_shared<core::data::structural::Structure>(structure_id);
  core::data::structural::Chain_SP c = std::make_shared<core::data::structural::Chain>(chain_id);

  // ---------- move the center of mass of the original residue to 0,0,0
  Vec3 cm = r.cm();
  for (const core::data::structural::PdbAtom_SP& ai:r) (*ai) -= cm;

  core::data::structural::selectors::SelectAllAtoms select_all;
  core::index4 i_atom = 0;
  for (core::index4 i_mol = 0; i_mol < grid->n_points(); i_mol++) {
    core::data::structural::Residue_SP r_new = r.clone(select_all);
    c->push_back(r_new);
    r_new->id(i_mol + 1);
    for (const core::data::structural::PdbAtom_SP& ai:*(r_new)) {
      *(ai) += grid->coordinates(i_mol);
      ai->id(++i_atom);
    }
  }
  s->push_back(c);

  return s;
}

}
}
