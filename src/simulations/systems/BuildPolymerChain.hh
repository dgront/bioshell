#ifndef SIMULATIONS_SYSTEMS_BuildPolymerChain_HH
#define SIMULATIONS_SYSTEMS_BuildPolymerChain_HH

#include <memory>
#include <core/index.hh>
#include <core/calc/statistics/Random.hh>
#include <simulations/systems/CartesianChains.hh>

namespace simulations {
namespace systems {

/** @brief Generates a random conformation of a simple polymer in the Cartesian space
 *
 * @tparam C - object used to represent a single atom, bead, particle, etc.
 */
class BuildPolymerChain {
public:
  /// Number of atoms in each chain to be generated
  const core::index4 n_atoms_;

  /** @brief Constructs the chain generator for a fixed length of polymers.
   *
   * Generated coordinates will be stored in the given CartesianChains object. The new chain will start
   * where <code>system[0]</code> is placed. The first generated atom is placed at index 1. This builder will therefore
   * generate as many coordinates as <code>CartesianChains::n_atoms - 1</code>. <strong>Note</strong> that the
   * <code>system</code> object must be properly created i.e. the atoms must already be initialised. This builder
   * only affects existing coordinates.
   *
   * @param system - a unique pointer to an array where resulting coordinates will be written
   * @param last_test - atom placed at index <code>N<code> must not clash with atoms at most <code>N-last_test<code>;
   *    by default <code>last_test = 1<code>
   */
  BuildPolymerChain(CartesianChains & system, const int last_test = 1);

  /** @brief Generates a new polymer conformation.
   *
   * This method places randomly each atom <code>bond_length</code> apart from the previous atom and tests excluded
   * volume test of the new atom with the upstream part of a chain. The procedure is repeated until
   * the newly created atom does not violate the excluded volume test. This might be repeated up to <code>n_bead_attempts</code>
   * times. If still failing, a new chain is started over. After <code>n_chain_attempts</code> unsuccessful chain attempts,
   * <code>false</code> is returned
   * @param bond_length - length of each bond between beads, i.e. the distance between subsequent atoms.
   * @param cutoff - excluded volume distance; any two atoms cannot get closer than this value.
   * @param n_bead_attempts - how many times placement of each bead is repeated. After that, this method starts a new chain from scratch.
   * @param n_chain_attempts - how many times a chain generation procedure is repeated. After that, false is returned.
   */
  bool generate(const float bond_length, const float cutoff,
                const core::index2 n_bead_attempts = 1000, const core::index2 n_chain_attempts = 1000);

  /** @brief Generates a new polymer conformation.
   *
   * This method places randomly each atom <code>bond_length</code> apart from the previous atom and tests excluded
   * volume test of the new atom with the upstream part of a chain. The procedure is repeated until
   * the newly created atom does not violate the excluded volume test. This might be repeated up to <code>n_bead_attempts</code>
   * times. If still failing, a new chain is started over. After <code>n_chain_attempts</code> unsuccessful chain attempts,
   * <code>false</code> is returned
   * @param bond_lengths - length of each bond between beads, i.e. the distance between subsequent atoms; If there is fewer than
   *    <code>n_atoms-1</code> values, the bond lengths will be cycled
   * @param cutoff - excluded volume distance; any two atoms cannot get closer than this value.
   * @param n_bead_attempts - how many times placement of each bead is repeated. After that, this method starts a new chain from scratch.
   * @param n_chain_attempts - how many times a chain generation procedure is repeated. After that, false is returned.
   */
  bool generate(const std::vector<float> & bond_lengths, const float cutoff,
                const core::index2 n_bead_attempts = 1000, const core::index2 n_chain_attempts = 1000);


private:
  core::data::basic::Vec3I tmp;
  CartesianChains  & system;
  std::uniform_real_distribution<float> rand_coordinate;
  core::calc::statistics::Random & generator;
  int last_test_;

  bool try_chain(const float bond_length, const core::index2 n_attempts, const float cutoff);
  bool try_chain(const std::vector<float> & bond_lengths, const core::index2 n_attempts, const float cutoff);
  bool is_good_point(const core::index4 n_atoms_so_far, const core::data::basic::Vec3I & candidate, const float min_distance_square);
};

}
}

#endif
