/** \file AtomRange.hh
 * @brief Provide AtomRange structure and a related ostream printing operator
 */
#ifndef SIMULATIONS_SYSTEMS_AtomRange_HH
#define SIMULATIONS_SYSTEMS_AtomRange_HH

#include <iostream>

#include <core/index.hh>

namespace simulations {
namespace systems {

/** @brief A range of atoms (both side inclusive) that belong to a residue or a chain.
 *
 * Because this structure provides iterators, it is possible to use this range in STL algorithms or to iterate over it, as in the following example:
 */
struct AtomRange {
  core::index4 first_atom; ///< First atom of this range
  core::index4 last_atom; ///< Last atom of this range

  /** @brief  Constructor creates a new range.
   *
   * @param ref_system - system where the atoms belong to
   * @param first_atom - the first atom in this range
   * @param last_atom - the last atom in this range
   */
  AtomRange(const core::index4 first_atom, const core::index4 last_atom) :
      first_atom(first_atom), last_atom(last_atom) {}

  /// Counts atoms that belong to this range
  inline core::index4 size() const { return last_atom + 1 - first_atom; }

  /// returns true if a given atom belong to this AtomRange
  inline bool is_my_atom(const core::index4 atom_index) const { return atom_index >= first_atom && atom_index <= last_atom; }
};

/** @brief ostream operator for AtomRange type.
 *
 * @tparam C - the type used to express coordinates
 * @param stream - output stream
 * @param range - object to be written
 * @return reference to the output stream
 */
std::ostream &operator<<(std::ostream &stream, const AtomRange &range);

} // ~ systems
} // ~ simulations

#endif
