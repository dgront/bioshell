#ifndef SIMULATIONS_SYSTEMS_ATOMTYPES_H
#define SIMULATIONS_SYSTEMS_ATOMTYPES_H

#include <string>
#include <vector>
#include <map>

#include <core/index.hh>

namespace simulations {
namespace systems {

/** @brief Stores all atom types allowed in a given force field
 *
 * An atom type has a name (string), but in data structures is represented by an integer index.
 * This class provides translation between the two
 */
class AtomTypes {
public:

  /** @brief Creates an empty set of atom types.
   *
   * Use @code register_atom_type() @endcode to add new types
   */
  AtomTypes() {}

  /** @brief Creates a set of atom types based on a given list.
   */
  AtomTypes(const std::vector<std::string> & atom_names);

  virtual /** @brief Returns internal index of an atom type given its name
   * @param internal_atom_name - atom name in a force field, e.g. "CT" for tetrahedral carbon in AMBER ff
   * @return integer index for that atom type
   */
  core::index1 atom_type(const std::string& internal_atom_name) const { return name_to_index_.at(internal_atom_name); }

  /** @brief Returns true if a given string has been registered as an atom type
   * @param internal_atom_name - atom name in a force field definition
   * @return true if this atom name has been already registered in this atom typing
   */
  bool is_known_atom_type(const std::string& internal_atom_name) const;

  /** @brief Returns name of an atom type
   * @param atom_type_index - atom type index
   * @return atom index
   */
  const std::string& atom_name(const core::index1 atom_type_index) const { return index_to_name_[atom_type_index]; }

  /** @brief Register a new atom type.
   * @param internal_atom_name - the name of the new atom type defined for a given force field
   * @return integer index assigned to the new type
   */
  core::index1 register_atom_type(const std::string& internal_atom_name);

  /// @brief Count how many different atom types are defined by this typing
  virtual core::index1 count_atom_types() const {  return index_to_name_.size(); }

private:
  std::vector<std::string> index_to_name_;            ///< index_to_name_[i] -> name of i-th atom in a given force field
  std::map<std::string, core::index1> name_to_index_; ///< the opposite to index_to_name_[i]
};

}
}

#endif // ATOMTYPES_H