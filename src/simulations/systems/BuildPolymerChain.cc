#include <core/calc/statistics/Random.hh>
#include <simulations/systems/BuildPolymerChain.hh>

namespace simulations {
namespace systems {

BuildPolymerChain::BuildPolymerChain(CartesianChains & system, const int last_test) :
    n_atoms_(system.n_atoms), tmp(0), system(system), rand_coordinate(-1.0, 1.0),
  generator(core::calc::statistics::Random::get()) {
  last_test_ = last_test;
}

bool BuildPolymerChain::generate(const float bond_length, const float cutoff,
                                             const core::index2 n_bead_attempts, const core::index2 n_chain_attempts) {

  system[0].set(0.0);

  for (core::index2 n_tries = 0; n_tries < n_chain_attempts; ++n_tries)
    if (try_chain(bond_length, n_bead_attempts, cutoff)) return true;

  return false;
}

bool BuildPolymerChain::generate(const std::vector<float> & bond_lengths, const float cutoff,
                                             const core::index2 n_bead_attempts, const core::index2 n_chain_attempts) {

  system[0].set(0.0);

  for (core::index2 n_tries = 0; n_tries < n_chain_attempts; ++n_tries)
    if (try_chain(bond_lengths, n_bead_attempts, cutoff)) return true;

  return false;
}

bool  BuildPolymerChain::try_chain(const float bond_length, const core::index2 n_attempts, const float cutoff) {

  const float cutoff2 = cutoff * cutoff;
  for (core::index4 ai = 1; ai < n_atoms_; ++ai) {
    core::index2 n_attmpt = 0;
    do {
      tmp.set(rand_coordinate(generator), rand_coordinate(generator), rand_coordinate(generator));
      tmp.norm(bond_length);
      tmp += system[ai - 1];
      n_attmpt++;
    } while ((!is_good_point(ai, tmp, cutoff2)) && (n_attmpt < n_attempts));
    if (n_attmpt == n_attempts) return false;
    system[ai].set(tmp);
  }
  return true;
}

bool  BuildPolymerChain::try_chain(const std::vector<float> & bond_lengths, const core::index2 n_attempts, const float cutoff) {

  const float cutoff2 = cutoff * cutoff;
  for (core::index4 ai = 1; ai < n_atoms_; ++ai) {
    core::index2 n_attmpt = 0;
    do {
      tmp.set(rand_coordinate(generator), rand_coordinate(generator), rand_coordinate(generator));
      tmp.norm(bond_lengths[ai % bond_lengths.size()]);
      tmp += system[ai - 1];
      n_attmpt++;
    } while ((!is_good_point(ai, tmp, cutoff2)) && (n_attmpt < n_attempts));
    if (n_attmpt == n_attempts) return false;
      system[ai].set(tmp);
  }
  return true;
}

bool BuildPolymerChain::is_good_point(const core::index4 n_atoms_so_far, const core::data::basic::Vec3I &candidate,
                                                  const float min_distance_square) {

  for (int i = 0; i < int(int(n_atoms_so_far) - last_test_); ++i) {
    if (candidate.closest_distance_square_to(system[i]) < min_distance_square) return false;
  }

  return true;
}

}
}
