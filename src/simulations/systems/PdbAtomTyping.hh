#ifndef SIMULATIONS_SYSTEMS_PdbAtomTyping_HH
#define SIMULATIONS_SYSTEMS_PdbAtomTyping_HH

#include <core/index.hh>
#include <core/data/structural/Structure.hh>
#include <simulations/systems/AtomTypingInterface.hh>
#include <utils/Logger.hh>

namespace simulations {
namespace systems {

std::vector<std::string> list_3codes_aminoacids();

/** @brief Atom typing based on a PDB file
*
* Here each unique atom name found in a given PDB structure gets its own ID. There is no particular
* order of the IDs enforced. Internal (typed) atom names are set to element names
*/
class PdbAtomTyping : public AtomTypingInterface {
public:

    /** @brief Creates atom typing based on a given biomolecular structure.
     *
     * Resulting atom typing will assign a distinct type to each unique atom name. Atoms of the same name coming from
     * different residue types will share the same atom type, e.g. " CA " atom from GLY and from ALA will be of the same type.
     * Atom type indexes are assigned in the order atoms appear in the PDB file
     * @param structure - a structure
     */
    PdbAtomTyping(const std::string& filename="forcefield/pdb_atom_types.dat");

    /// Virtual destructor to satisfy a compiler
    virtual ~PdbAtomTyping() {}

    /** @brief Returns true if a given residue type has been registered
   * @param residue_name - residue name in the three-letter code
   * @param variant - request a special variant of the residue, e.g. C-terminal
   * @return true if such a residue name has been already registered in this atom typing
   */
    virtual bool is_known_residue_type(const std::string & residue_name,
                                       const systems::AtomTypingVariants variant) const;

    /** @brief Returns index of a given residue type
     * Throws exception if a given residue type has not been registered
     * @param residue_name - residue name in the three-letter code
     * @param variant - request a special variant of the residue, e.g. C-terminal
     * @return residue type code
     */
    virtual core::index1 residue_type(const std::string & residue_name,
                                      const systems::AtomTypingVariants variant) const;

    /** @brief Returns an internal  name for a given internal residue type.
     *  @param residue_id - index assigned to a given atom type according to this  typing
     *  @return internal name of a given residue, including a possible patch
     */
    virtual const std::string & residue_internal_name(const core::index1 residue_id) const;

    /** @brief Returns true if a given string has been registered as an atom type
     * @param atom_name - name of the requested atom, as written in the PDB file used to start a simulation
     * @param residue_name - name of a residue
     * @param variant - variant of the residue; typically AtomTypingVariants::STANDARD should be used
     *    unless its a special case for this residue, e.g. it is the N-terminal or C-terminal one
     * @return true if this atom name has been already registered in this atom typing
     */
    virtual bool is_known_atom_type(const std::string & atom_name, const std::string & residue_name,
                                    const AtomTypingVariants variant) const;

    /** @brief Returns true if a given string has been registered as an atom type
     * @param a - PdbAtom object
     * @return true if this atom name has been already registered in this atom typing
     */
    virtual bool is_known_atom_type(const core::data::structural::PdbAtom &a) const ;

    /** @brief Returns an internal index for an atom identified by its PDB name, its residue name and a requested variant.
     * @param atom_name - name of the requested atom, as written in the PDB file used to start a simulation
     * @param residue_name - name of a residue
     * @param variant - variant of the residue; typically AtomTypingVariants::STANDARD should be used
     *    unless its a special case for this residue, e.g. it is the N-terminal or C-terminal one
     */
    virtual core::index1 atom_type(const std::string & atom_name, const std::string & residue_name,
                                   const AtomTypingVariants variant) const;

    /// Returns an internal index for an atom identified by its PDB name and its residue name
    virtual core::index1 atom_type(const core::data::structural::PdbAtom & a) const ;

private:
    static utils::Logger l;
    std::vector<std::string> residues_names=list_3codes_aminoacids();
};

}
}
#endif