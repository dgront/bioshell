#ifndef SIMULATIONS_ISING_Ising2D_H
#define SIMULATIONS_ISING_Ising2D_H

#include <core/index.hh>
#include <core/calc/statistics/Random.hh>
#include <core/data/basic/Array2D.hh>
#include <simulations/evaluators/Evaluator.hh>
#include <simulations/forcefields/TotalEnergy.hh>

namespace simulations {
namespace systems {
namespace ising {


using core::index2;
using core::index4;
using core::data::basic::Array2D;

/** @brief Represents a 2D system of spins (Ising model)
 * @tparam S - data type representing a single spin; typically use core::index1
 * @tparam I - data type used to index spins; use core::index2 for a moderate-size systems
 *
 */
template<typename S, typename I>
class Ising2D : public Array2D<S>, public forcefields::TotalEnergy, public evaluators::Evaluator {

  using Array2D<S>::n_columns;
  using Array2D<S>::size;

public:
  Ising2D(const index2 n_rows, const index2 n_columns) : Array2D<S>(n_rows, n_columns),
      evaluators::Evaluator({"Ising2DEnergy"}) {

    if ((n_columns == 0) || (n_rows == 0)) throw std::invalid_argument("Invalid array size");
    in_cluster_flags.resize(n_rows * n_columns);
    top_offset = (n_rows - 1) * n_columns;
    sw.push_back(10);
    factors.push_back(1.0);

    TotalEnergy::name("Ising2DEnergy");
  }

  /** @brief Returns index of a spin that is located just above a given spin.
   * The method applies periodic boundary conditions
   * @param spin_index - index of the referred spin
   * @return index ot the top spin, including periodic boundary conditions
   */
  I top(const I spin_index) const {
    return (spin_index < n_columns) ? spin_index + top_offset : spin_index - n_columns;
  }

  /** @brief Returns index of a spin that is located just below a given spin
   * The method applies periodic boundary conditions
   * @param spin_index - index of the referred spin
   * @return index ot the spin below, including periodic boundary conditions
   */
  I bottom(const I spin_index) const {
    return (spin_index >= size - n_columns) ? spin_index - top_offset : spin_index + n_columns;
  }

  /** @brief Returns index of a spin that is located on the left of a given spin
   * The method applies periodic boundary conditions
   * @param spin_index - index of the referred spin
   * @return index ot the left spin, including periodic boundary conditions
   */
  I left(const I spin_index) const { return (spin_index % n_columns != 0) ? I(spin_index - 1) : spin_index - 1 + n_columns; }

  /** @brief Returns index of a spin that is located on the right of a given spin
   * The method applies periodic boundary conditions
   * @param spin_index - index of the referred spin
   * @return index ot the right spin, including periodic boundary conditions
   */
  I right(const I spin_index) const { return (spin_index % n_columns != I(n_columns - 1)) ? spin_index + 1 : spin_index + 1 - n_columns; }

  /// Returns the total number of spins of this system
  inline I count_spins() const { return size; }

  /// Assigns spins randomly
  void initialize();

  /// Flips a given sping to the opposite configuration
  inline void flip_spin(I point) { Ising2D<S, I>::the_data[point] = Ising2D<S, I>::the_data[point] == 0 ? 1 : 0; }

  /** @brief evaluates energy related to a given spin
   *
   * @param point - which spin
   * @return energy of that spin calculated with all its neighbors
   */
  inline double point_energy(I point) const {

    double en = 0.;
    S spin = Ising2D<S, I>::the_data[point];

    en += (Ising2D<S, I>::the_data[top(point)] == spin) ? -1 : 1;
    en += (Ising2D<S, I>::the_data[bottom(point)] == spin) ? -1 : 1;
    en += (Ising2D<S, I>::the_data[left(point)] == spin) ? -1 : 1;
    en += (Ising2D<S, I>::the_data[right(point)] == spin) ? -1 : 1;

    return en;
  }

  /// Calculates the total energy of the system
  double calculate();

  /// This method, inherited from Evaluator interface, evaluates the total energy of the system
  virtual std::vector<double> evaluate() { return {calculate()}; }

  /// Returns the name of the energy column, which is <code>"TotalEnergy"</code>
  virtual std::string header_string() const { return "TotalEnergy"; }

  /// Returns the width (in characters) of energy values printed
  virtual core::index2 width() const { return 8; }

  virtual core::index2 min_width() const { return 8; }

  /** @brief Writes the current conformation of this Ising system
   *
   * The resulting string is Base64 encoded
   * @param out - output stream
   */
  void write(std::ostream & out);

  /** @brief Stores the current conformation of this Ising system as a string.
   *
   * The string is Base64 encoded
   * @return a base64 encoded string
   */
  std::string write();

  /** @brief Loads a conformation of an Ising system from a base-64 encoded string.
   *
   * @param src_base64 - a base64 encoded string
   */
  void load(std::string& src_base64);

private:
  std::vector<I> in_cluster_flags;
  I top_offset;
  const std::string name_;
};

class Ising2DBB: public Ising2D<core::index1,core::index2>{};
}
}
}

#endif




