#include <core/calc/statistics/Random.hh>
#include <core/data/basic/Array2D.hh>

#include <simulations/systems/ising/Ising2D.hh>

#include <utils/base64_encoding.hh>

namespace simulations {
namespace systems {
namespace ising {

template<typename S, typename I>
void Ising2D<S, I>::initialize() {
  /* Populate the array with random spins */
  core::calc::statistics::Random &gen = core::calc::statistics::Random::get();
  std::uniform_int_distribution<S> generate(0, 1);
  S spin;

  for (I i = 0; i < Ising2D<S, I>::size; ++i) {
    spin = generate(gen);
    Ising2D<S, I>::the_data[i] = spin;
  }
}

template<typename S, typename I>
double Ising2D<S, I>::calculate() {
  double en = 0.;
  for (I i = 0; i < Ising2D<S, I>::size; ++i) {
    en += point_energy(i);
  }
  return en / 2.0;
}

template<typename S, typename I>
void Ising2D<S, I>::write(std::ostream & out) { out << write(); }

template<typename S, typename I>
std::string Ising2D<S, I>::write() {

  return utils::base64_encode(Ising2D<S, I>::the_data);
}

template<typename S, typename I>
void Ising2D<S, I>::load(std::string& src_base64) {

  std::vector<core::index1> dst = utils::base64_decode(src_base64);
  for (I i = 0; i < Ising2D<S, I>::size; ++i) Ising2D<S, I>::the_data[i] = dst[i];
}

template
class Ising2D<core::index1, core::index2>;

template
class Ising2D<core::index1, core::index4>;

}
}
}





