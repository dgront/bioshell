#ifndef CORE_DATA_BASIC_IsingArray3D_H
#define CORE_DATA_BASIC_IsingArray3D_H

#include <core/calc/statistics/Random.hh>
#include <core/data/basic/Array3D.hh>


namespace simulations {
namespace systems {
namespace ising {

/** @brief Ising 3D system implemened as an Array3D derivative
 *
 * @tparam T type of data used to represent spins, typically unsigned char
 */
template<typename T>
class IsingArray3D : public core::data::basic::Array3D<T> {
public:
  IsingArray3D(const index2 n_rows, const index2 n_columns, const index2 n_layers) :
    core::data::basic::Array3D<T>(n_rows, n_columns, n_layers) {

    if ((n_columns == 0) || (n_rows == 0) || (n_layers == 0)) throw std::invalid_argument("Invalid array size");
    tree.resize(n_rows * n_columns * n_layers);
  }


  /** @brief sets a spin to a new state
   *
   * @param point - which spin
   * @param value  - its new state
   */
  void put_point(size_t &point, T &value) {
    /* This is used for calculating the cluster connectivity, each point has to be defined again */
    this->the_data[point] = value;
    std::vector<T> neighbors = this->get_cubic_neighbors(point);
    tree[point] = -1;
    T r1 = point;
    T r2;
    for (auto &neighbor: neighbors) {
      if (this->the_data[neighbor] == value) {
        r2 = find_size(neighbor);
        if (r1 != r2) {
          if (tree[r1] > tree[r2]) {
            tree[r2] += tree[r1];
            tree[r1] = r2;
            r1 = r2;
          }
          else {
            tree[r1] += tree[r2];
            tree[r2] = r1;
          }
        }
      }
    }

  }

  /// Populate the array with random spins
  void initialize() {
    core::calc::statistics::Random &gen = core::calc::statistics::Random::get();
    std::uniform_int_distribution<int> generate(0, 1);
    T spin;

    for (int i = 0; i < this->size; ++i) {
      spin = generate(gen);
      this->the_data[i] = spin;
    }
  }

  /// calculates the information for cluster connections
  void calc_tree() {
    /*
     * calculates the information for cluster connections
     * only then can the cluster count and size methods work properly
     */
    std::vector<T> dummy;
    for (int i = 0; i < this->size; ++i) {
      dummy.push_back(this->the_data[i]);
      this->the_data[i] = 13; //dummy value
    }

    for (int i = 0; i < this->size; ++i) {
      put_point(i, dummy[i]);
    }
  }

  template<typename D>
  void set(const D &data) {
    index4 i = 0;
    for (auto it = data.begin(); it != data.end(); ++it) {
      this->the_data[i] = data[i];
      ++i;
    }
  }

  template<typename D>
  void set_tree(const D &data) {
    index4 i = 0;
    for (auto it = data.begin(); it != data.end(); ++it) {
      tree[i] = data[i];
      ++i;
    }
  }

  void flip_spin(size_t &point) {
    T new_spin = this->the_data[point] == 0 ? 1 : 0;
    this->the_data[point] = new_spin;
  }

  void find_cluster(std::vector<T> &result, T &point, T &spin) {
    /* finds a cluster with no previous info */
    std::vector<T> neighbors = this->get_cubic_neighbors(point);
    for (auto &neighbor: neighbors) {
      if (this->the_data[neighbor] == spin &&
          std::find(result.begin(), result.end(), neighbor) == result.end()) {
        result.push_back(neighbor);
        find_cluster(result, neighbor, spin);
      }

    }
    return;
  }

  double calc_mean_cluster_size() {
    double sizes = 0;
    double count = 0;
    for (int i = 0; i < this->size; ++i) {
      if (tree[i] < 0) {
        sizes -= tree[i];
        count += 1;
      }
    }
    return sizes / count;
  }


  double calc_cluster_count() {
    double count = 0;
    for (int i = 0; i < this->size; ++i) {
      if (tree[i] < 0) count += 1;
    }
    return count;
  }

  const std::vector<T> &expose_vector() const { return this->the_data; }

  const std::vector<T> &expose_tree() const { return tree; }

  const std::vector<T> expose_find() {
    /* this could be used to take a snapshot of clusters in the system */
    std::vector<T> find;
    for (int i = 0; i < this->size; ++i) {
      find.push_back(find_size(i));
    }
    return find;
  }


  T find_size(T node) {
    /*
    this is the core algorithm for cluster connectivity calculations
    returns the size of calculated_node's cluster 
    while making the path to its root shorter
    */
    T node_1 = node;
    T node_2 = node;
    while (tree[node_1] >= 0) {
      tree[node_2] = tree[node_1];
      node_2 = node_1;
      node_1 = tree[node_1];
    }
    return node_1;
  }

  /// Calculates the energy of a single spin
  double point_energy(T point) {
    double en = 0.;
    std::vector<T> neighbors = this->get_cubic_neighbors(point);
    int spin = this->the_data[point];
    for (auto &neighbor: neighbors) {
      if (spin == this->the_data[neighbor]) en += 1;
      else en -= 1;
    }
    return en;
  }

  /// Calculates the energy of this system
  double calc_energy() {
    double en = 0.;
    for (T i = 0; i < this->size; ++i) {
      en += this->point_energy(i);
    }
    return en / 2.0;
  }

protected:
  std::vector<T> tree;        // cluster connectivity info

};

}
}
}

#endif




