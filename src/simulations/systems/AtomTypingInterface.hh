#ifndef SIMULATIONS_SYSTEMS_AtomTypingInterface_HH
#define SIMULATIONS_SYSTEMS_AtomTypingInterface_HH

#include <memory>

#include <core/index.hh>
#include <core/data/basic/Vec3I.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>

#include <simulations/systems/AtomTypingVariants.hh>
#include <simulations/systems/AtomTypes.hh>

namespace simulations {
namespace systems {

/** @brief AtomTyping converts atoms (identified by their names as they are stored in a PDB file) into internal numbering used by force fields.
 *
 * This interface declares ability to convert from atom name into an internal index and back. Each atom
 * has two names assigned:
 *   - one according to PDB naming conventions, e.g. alpha carbon in proteins in " CA ", and
 *   - name that is specific for a force field.
 */
class AtomTypingInterface : protected AtomTypes {
public:

  /// Empty constructor
  AtomTypingInterface() : AtomTypes() {}

  /// empty destructor
  virtual ~AtomTypingInterface() = default;

  /** @brief Detect residue patch for a given residue object.
   *
   * This method recognizes terminal variants of amino acid residues
   * @param r - a residue in a chain
   * @return AtomTypingVariants enum value
   */
  static AtomTypingVariants residue_patch(const core::data::structural::Residue &r) {

    AtomTypingVariants termini = AtomTypingVariants::STANDARD;
    if (r.residue_type().type == 'P') {
      if (r.next() == nullptr) termini = systems::AtomTypingVariants::C_TERMINAL;
      if (r.previous() == nullptr) termini = systems::AtomTypingVariants::N_TERMINAL;
    }
    return termini;
  }

  /** @brief Returns true if a given residue type has been registered
   * @param residue_name - residue name in the three-letter code
   * @param variant - request a special variant of the residue, e.g. C-terminal
   * @return true if such a residue name has been already registered in this atom typing
   */
  virtual bool is_known_residue_type(const std::string & residue_name,
                             const systems::AtomTypingVariants variant) const = 0;

  /** @brief Returns index of a given residue type
   * Throws exception if a given residue type has not been registered
   * @param residue_name - residue name in the three-letter code
   * @param variant - request a special variant of the residue, e.g. C-terminal
   * @return residue type code
   */
  virtual core::index1 residue_type(const std::string & residue_name,
                            const systems::AtomTypingVariants variant) const = 0;

  /** @brief Returns index of a given residue type.
   *
   * This method simply calls the virtual method @code residue_type(name, variant) @endcode
   * with the three letter code of a residue; the variant is detected by @code residue_patch() @endcode
   * @param r - a residue
   * @return residue type code
   */
  core::index1 residue_type(const core::data::structural::Residue &r) const {
    return residue_type(r.residue_type().code3, residue_patch(r));
  }

  /** @brief Returns an internal  name for a given internal residue type.
   *  @param residue_id - index assigned to a given atom type according to this  typing
   *  @return internal name of a given residue, including a possible patch
   */
  virtual const std::string & residue_internal_name(const core::index1 residue_id) const = 0;

  /** @brief Returns true if a given string has been registered as an atom type
   * @param internal_atom_name - atom name in a force field definition
   * @return true if this atom name has been already registered in this atom typing
   */
  bool is_known_atom_type(const std::string& internal_atom_name) const { return AtomTypes::is_known_atom_type(internal_atom_name); }

  /** @brief Returns true if a given string has been registered as an atom type
   * @param atom_name - name of the requested atom, as written in the PDB file used to start a simulation
   * @param residue_name - name of a residue
   * @param variant - variant of the residue; typically AtomTypingVariants::STANDARD should be used
   *    unless its a special case for this residue, e.g. it is the N-terminal or C-terminal one
   * @return true if this atom name has been already registered in this atom typing
   */
  virtual bool is_known_atom_type(const std::string & atom_name, const std::string & residue_name,
                          const AtomTypingVariants variant) const = 0;

  /** @brief Returns true if a given string has been registered as an atom type
   * @param a - PdbAtom object
   * @return true if this atom name has been already registered in this atom typing
   */
  virtual bool is_known_atom_type(const core::data::structural::PdbAtom &a) const = 0;

  /** @brief Returns atom type index for a given internal atom name.
   * @param atom_internal_name - name of this atom type, as it is defined in this atom typing. This could not be
   * the PDB atom name, because in general more than one atom type may be assigned to a given PDB atom name
   * (e.g. CG1 from VAL may be a different atom than  CG1 in TRP)
   */
  core::index1 atom_type(const std::string & atom_internal_name) const override {  return AtomTypes::atom_type(atom_internal_name); }

  /** @brief Returns an internal index for an atom identified by its PDB name, its residue name and a requested variant.
   * @param atom_name - name of the requested atom, as written in the PDB file used to start a simulation
   * @param residue_name - name of a residue
   * @param variant - variant of the residue; typically AtomTypingVariants::STANDARD should be used
   *    unless its a special case for this residue, e.g. it is the N-terminal or C-terminal one
   */
  virtual core::index1 atom_type(const std::string & atom_name, const std::string & residue_name,
                                 const AtomTypingVariants variant) const = 0;

  /** @brief Returns an internal index for an atom identified by its PDB name and its residue name.
   *
   * This method uses STANDARD typing variant
   * @param atom_name - name of the requested atom, as written in the PDB file used to start a simulation
   * @param residue_name - name of a residue
   */
  core::index1 atom_type(const std::string & atom_name, const std::string & residue_name) const {
    return atom_type(atom_name, residue_name, AtomTypingVariants::STANDARD);
  }

  /// Returns an internal index for an atom identified by its PDB name and its residue name
  virtual core::index1 atom_type(const core::data::structural::PdbAtom & a) const = 0;

  /// @brief Count how many different atom types are defined by this typing
  core::index1 count_atom_types() const override {  return AtomTypes::count_atom_types(); }

  /** @brief Returns an internal atom name for a given internal atom type.
   *  @param atom_id - index assigned to a given atom type according to this atom typing
   *  @return name for a given atom as it is assigned by this atom typing
   */
  virtual const std::string & atom_internal_name(const core::index1 atom_id) const { return atom_name(atom_id); }

  /** @brief Returns an internal atom name based on <code>atom_type</code> field that is stored in Vec3I type
   *  @param atom - atom data
   *  @return name for a given atom as it is assigned by this atom typing
   */
  const std::string & atom_internal_name(const core::data::basic::Vec3I & atom) const { return atom_internal_name(atom.atom_type); }

protected:
  /// constructor that initializes AtomTypes base class with allowed atom types
  explicit AtomTypingInterface(const std::vector<std::string> & atom_names) : AtomTypes(atom_names) {}
};

/// Define a shared pointer type
typedef std::shared_ptr<AtomTypingInterface> AtomTypingInterface_SP;

}
}
#endif
