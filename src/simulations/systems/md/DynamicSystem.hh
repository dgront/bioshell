#ifndef SIMULATIONS_SYSTEMS_MD_DynamicSystem_HH
#define SIMULATIONS_SYSTEMS_MD_DynamicSystem_HH

#include <random>
#include <memory>

#include <core/index.hh>
#include <core/data/basic/Vec3.hh>
#include <core/calc/statistics/Random.hh>

#include <simulations/systems/AtomTypingInterface.hh>
#include <simulations/systems/SingleAtomType.hh>

namespace simulations {
namespace systems {
namespace md {

/** @brief Decorates a Cartesian system with a vector of velocities.
 * This facilitates Molecular Dynamics simulation for a Cartesian system.
 * The following example shows how to create a DynamicSystem and initialize velocities:
 * \include ex_DynamicSystem.cc
 *
 * The next example runs a simple DMD simulatin of argon gas:
 * \include ex_DMD_Ar.cc
 *
 * @tparam T a type used to represent the system, e.g. CartesianAtomsSimple or ResidueChain
 */
template<typename T>
class DynamicSystem : public T {
public:
  std::unique_ptr<core::data::basic::Vec3[]> velocities; ///< Atomic velocities, implemented in the very same ways as coordinates in CartesianAtomsSimple

  /** @brief Constructor creates an instance for a given number of atoms.
   * @param typing - atom typing, required by a constructor of a base class <code>T</code>
   * @param n_atoms - the number of atoms in this system
   */
  DynamicSystem(const AtomTypingInterface_SP typing, const size_t n_atoms) : T(typing, n_atoms), velocities(
    new core::data::basic::Vec3[n_atoms]) {}

  /** @brief Assign velocities randomly from Maxwell distributions.
   *
   * After this call is ended, every atom of this <code>DynamicSystem</code> object will have a newly assigned
   * random velocity vector, according to Maxwell distributions. The velocity is in \f$ \left[\frac{m}{s}\right] \f$
   * @param t - temperature (in Kelvins)
   */
  void random_velocities(const double t) {

    core::calc::statistics::Random &generator = core::calc::statistics::Random::get();
    std::uniform_real_distribution<double> rand_vector;
    std::normal_distribution<double> rand_velocity;
    core::data::basic::Vec3 cm; // --- CM velocity
    double total_mass = 0;
    for (core::index4 i = 0; i < T::n_atoms; ++i) {
      const double c = sqrt(8.3146 * t / get_mass(core::index4(i)) *
                            1000.0); // [J/(mol K)] * [K] / [kg/mol] = [m*m/(s*s)]; factor 1000 makes [kg/mol] from [g/mol]
      velocities[i].x = (1.0 - 2 * rand_velocity(generator)) * c * rand_velocity(generator);
      velocities[i].y = (1.0 - 2 * rand_velocity(generator)) * c * rand_velocity(generator);
      velocities[i].z = (1.0 - 2 * rand_velocity(generator)) * c * rand_velocity(generator);
      cm.x += velocities[i].x * velocities[i].register_;
      cm.y += velocities[i].y * velocities[i].register_;
      cm.z += velocities[i].z * velocities[i].register_;
      total_mass += velocities[i].register_;
    }

    cm /= total_mass;
    for (core::index4 i = 0; i < T::n_atoms; ++i)
      velocities[i] -= cm;
  }

  /** @brief Returns the mass of <code>which_atom</code> atom.
   *
   * @param which_atom - index of the atom.
   * @returns mass in AU
   */
  double get_mass(const core::index4 which_atom) const { return velocities[which_atom].register_ * 0.01; }

  /** @brief Set the mass of an atom.
   *
   * The mass is stored in the <code>Vec3::register_</code> field as an integer value with accuracy of two decimal points
   * @param which_atom - index of the atom whose mass is to be set
   * @param m - the value of mass in AU (atomic units)
   */
  void set_mass(const core::index4 which_atom, const double m) { velocities[which_atom].register_ = m * 100; }

  /** @brief Set the mass of all atoms for to given value.
   *
   * The mass is stored in the <code>Vec3::register_</code> field as an integer value with accuracy of two decimal points
   * @param m - the value of mass in AU (atomic units)
   */
  void set_mass(const double m) {

    for (core::index4 i = 0; i < T::n_atoms; ++i) {
      velocities[i].register_ = m * 100;
    }
  }

  /// Calculates kinetic temperature of the system
  double temperature(void) const {

    double t = 0;
    for (core::index4 i = 0; i < T::n_atoms; ++i)
      t += get_mass(i) * 0.001 * velocities[i].length_squared() / 8.3146; // --- 0.001 coverts from [g/mol] to [kg/mol]

    return t / (3.0 * double(T::n_atoms - 1));
  }

private:
};

}
}
}

#endif // SIMULATIONS_CARTESIAN_DynamicSystem_HH

/**
 * \example ex_DynamicSystem.cc
 */
