#include <simulations/systems/AtomTypes.hh>
#include <algorithm>

namespace simulations {
namespace systems {

AtomTypes::AtomTypes(const std::vector<std::string> & atom_names) {

  for(const std::string & name:atom_names) register_atom_type(name);
}

core::index1 AtomTypes::register_atom_type(const std::string& internal_atom_name) {
  // --- first check if we already have that atom type registered
  const auto pos = std::find(index_to_name_.begin(), index_to_name_.end(), internal_atom_name);
  if (pos != index_to_name_.end())
    return std::distance(index_to_name_.begin(), pos);

  // --- if not, add it to the data structures
  index_to_name_.push_back(internal_atom_name);
  name_to_index_[internal_atom_name] = index_to_name_.size() - 1;

  return name_to_index_[internal_atom_name];
}

bool AtomTypes::is_known_atom_type(const std::string& internal_atom_name) const {
  return (std::find(index_to_name_.begin(), index_to_name_.end(), internal_atom_name)!=index_to_name_.end());
}

}
}