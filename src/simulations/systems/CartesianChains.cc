
#include <string>
#include <fstream>
#include <memory>
#include <random>
#include <stdexcept>
#include <type_traits>

#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/systems/CartesianChains.hh>
#include <simulations/systems/AtomTypingInterface.hh>
#include <simulations/systems/AtomTypingVariants.hh>

namespace simulations {
namespace systems {

std::ostream &operator<<(std::ostream &stream, const AtomRange &range) {

  stream << range.first_atom << " : " << range.last_atom;
  return stream;
}

CartesianChains::CartesianChains(const AtomTypingInterface_SP typing, const core::index4 n_atoms,
                                 const std::string &atom_name, const std::string &residue_name) :
    CartesianAtoms(typing, n_atoms), system_id_(0), logger("CartesianChains") {

  using base = CartesianAtoms;

  for (core::index4 i = 0; i < base::n_atoms; i++) {
    atoms_for_residue_.emplace_back(i, i);
    core::data::basic::Vec3I &a = base::operator[](i);
    a.chain_id = 0;
    try {
      a.atom_type = typing->atom_type(atom_name, residue_name);
    } catch (std::out_of_range &e) {
      logger << utils::LogLevel::SEVERE << "Can't detect the  atom type for >" << atom_name << "<\n";
      throw e;
    }
  }
  atoms_for_chain_.emplace_back(0, base::n_atoms - 1);
}

int CartesianChains::chain_index(const std::string &chain_id) const {
  for (int i = 0; i < chain_ids_.size(); ++i)
    if (chain_id == chain_ids_[i]) return i;
  return -1;
}

CartesianChains::CartesianChains(const AtomTypingInterface_SP typing, const core::data::structural::Structure &s) :
    CartesianChains(typing, s.count_atoms(), (*(s.first_const_atom()))->atom_name(),
                    (*s.first_const_residue())->residue_type().code3) {

  using base = CartesianAtoms;
  core::index4 first_atom_in_next_residue = 0;
  core::index4 first_atom_in_next_chain = 0;
  core::index4 i_atom = 0;
  core::index2 i_resid = 0;
  atoms_for_chain_.clear();
  atoms_for_residue_.clear();

  for (const auto &cp : s) {
    logger << utils::LogLevel::FINE << "processing chain " << (*cp).id() << "\n";
    chain_ids_.push_back(cp->id());
    for (const auto &rp : *cp) {
      core::index1 residue_type = typing->residue_type(*rp);
      for (const auto &ap : *rp) {
        core::data::basic::Vec3I &a = base::operator[](i_atom);
        a.set(*ap);
        const std::string &id = cp->id();
        int chain_idx = chain_index(id);
        if (chain_idx < 0) {
          a.chain_id = chain_ids_.size();
          chain_ids_.push_back(id);
          logger << utils::LogLevel::WARNING << "Unknown chain " << id << " " << " at residue" << *rp << "\n";
        } else
          a.chain_id = chain_idx;
        try {
          a.atom_type = typing->atom_type(*ap);
        } catch (std::out_of_range &e) {
          logger << utils::LogLevel::SEVERE << "Unknown atom >" << ap->atom_name() << "< found in residue "
                 << rp->residue_type().code3 << "\n";
        }
        a.residue_type = residue_type;
        ++i_atom;
      }
      ++i_resid;
      atoms_for_residue_.emplace_back(first_atom_in_next_residue, i_atom - 1);

      // --- The IF below is just to print detailed message what atoms go to the current residue and what's their typing
      if (logger.is_logable(utils::LogLevel::FINER)) {
        core::data::basic::Vec3I &a = base::operator[](atoms_for_residue_.back().last_atom);
        logger << utils::LogLevel::FINER << "Atoms for residue " << size_t(i_resid - 1) << " "
               << atom_typing->residue_internal_name(a.residue_type) << " : " <<
               size_t(atoms_for_residue_.back().first_atom) << " - " << size_t(atoms_for_residue_.back().last_atom)
               << "\n";
        for (core::index4 ai = atoms_for_residue_.back().first_atom; ai <= atoms_for_residue_.back().last_atom; ++ai) {
          core::data::basic::Vec3I &aa = base::operator[](ai);
          logger << utils::LogLevel::FINER << "\t" << size_t(ai) << " is typed " << int(aa.atom_type)
                 << " " << base::atom_typing->atom_internal_name(aa.atom_type) << "\n";
        }
      }
      first_atom_in_next_residue = i_atom;
    }
    atoms_for_chain_.emplace_back(first_atom_in_next_chain, i_atom - 1);
    first_atom_in_next_chain = i_atom;
  }

  for (core::index2 ichain = 0; ichain < count_chains(); ++ichain) {
    for (core::index4 ia = atoms_for_chain_[ichain].first_atom; ia <= atoms_for_chain_[ichain].last_atom; ++ia)
      if (chain_for_atom(ia) != ichain)
        throw std::invalid_argument(
            utils::string_format("Inconsistent chain indexing for atom %d\n\tChain cached: %d chain assumed: %d\n", ia,
                                 chain_for_atom(ia), ichain));
  }
}

CartesianChains::CartesianChains(const AtomTypingInterface_SP typing,
                                 const std::vector<core::index2> &n_atoms_in_chains,
                                 const std::string &atom_name) : CartesianChains(typing, std::accumulate(
    n_atoms_in_chains.begin(),
    n_atoms_in_chains.end(), 0), atom_name) {

  using base = CartesianAtoms;

  core::index4 i_atom = 0;
  core::index2 i_chain = 0;
  atoms_for_chain_.clear();
  atoms_for_residue_.clear();
  for (core::index2 n : n_atoms_in_chains) { // --- loop over chains
    core::index2 i_res = 0;
    atoms_for_chain_.emplace_back(i_atom, i_atom + n - 1);
    for (core::index4 i = i_atom; i < i_atom + n; ++i) { // --- loop over atoms in n-th chain
      base::operator[](i).chain_id = i_chain;
      chain_ids_.push_back(std::string{utils::letters_digits[i_chain % utils::letters_digits.size()]});
      ++i_res;
    }
    i_atom += n;
    ++i_chain;
  }

  for (core::index2 ichain = 0; ichain < count_chains(); ++ichain) {
    for (core::index4 ia = atoms_for_chain_[ichain].first_atom; ia <= atoms_for_chain_[ichain].last_atom; ++ia)
      if (chain_for_atom(ia) != ichain)
        throw std::invalid_argument(utils::string_format("Inconsistent chain indexing for atom %d\n", ia));
  }
}

core::index4 CartesianChains::longest_residue() const {
  core::index4 longest_molecule = 0;
  for (core::index4 i = 0; i < count_residues(); ++i) {
    longest_molecule = std::max(longest_molecule, atoms_for_residue(i).size());
  }

  return longest_molecule;
}


}
}