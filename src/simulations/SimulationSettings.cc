#include <string>

#include <simulations/SimulationSettings.hh>
#include <utils/options/OptionParser.hh>
#include <utils/string_utils.hh>

namespace simulations {

utils::Logger logs("SimulationSettings");

void SimulationSettings::insert_or_assign(const utils::options::OptionParser &option_parser, bool insert_all) {

  settings_name_ = option_parser.program_name();

  const std::vector<utils::options::Option> & options = option_parser.registered_options();
  for(const utils::options::Option & o : options) {

    if(insert_all || o.was_used()) {
      std::string val(option_parser.value_string(o));
      core::data::basic::ThreadSafeMap<std::string, std::string>::insert_or_assign(o.get_element_name(), val);
    } else {
      std::string default_val = o.default_value_string();
      if (default_val.size()>0)
        core::data::basic::ThreadSafeMap<std::string, std::string>::insert_or_assign(o.get_element_name(), default_val);
    }
  }
}

const std::string& SimulationSettings::get(const std::string& key) const {
  try {
    return core::data::basic::ThreadSafeMap<std::string,std::string>::at(key);
  } catch (std::out_of_range & e) {
    logs << "unknown key: " << key << "\n";
    throw e;
  }
}

} // ~ simulations

