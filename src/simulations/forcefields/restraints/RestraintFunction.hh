#ifndef SIMULATIONS_FORCEFIELDS_RESTRAINTS_RestraintFunction_HH
#define SIMULATIONS_FORCEFIELDS_RESTRAINTS_RestraintFunction_HH

#include <string>
#include <vector>
#include <memory>
#include <limits>

#include <core/calc/numeric/DerivableFunction1D.hh>
#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/Interpolate1D.hh>
#include <core/index.hh>

namespace simulations {
namespace forcefields {
namespace restraints {

/** @brief RestraintFunction is used to convert restrained property (e.g. distance) into an energy value.
 *
 * For example, a HarmonicRestraintFunction takes a value <code>d</code> (say, a distance between two atoms)
 * and applies harmonic formula <code>k*d*d/2</code> which is returned as an energy of that restraint. Various
 * classes derived from RestraintFunction allows different transformations, suitable for particular modelling scenarios.
 */
class RestraintFunction : public core::calc::numeric::DerivableFunction1D<double> {
public:
  /** \brief Evaluates the function value and its derivative at x.
 * @param x - function argument, on which a restraint is based, e.g. interatomic distance
 * @param derivative - one-element array, used to store derivative's result
 * @return function value at x
 */
  virtual double operator()(const double x, double &derivative) const = 0;

  /** \brief Evaluates the function value  at x.
   * @param x - function argument, on which a restraint is based, e.g. interatomic distance
   * @return function value at x
   */
  virtual double operator()(const double x) const = 0;
};

/// A shared pointer to RestraintFunction defined as a type for convenience
typedef std::shared_ptr<RestraintFunction> RestraintFunction_SP;

/** \brief Applies a linear penalty to a violation from a mean value of a restrained parameter
*/
class LinearPenaltyFunction : public RestraintFunction {
public:

  LinearPenaltyFunction(const std::vector<double> & parameters): minimum(parameters[0]), slope(parameters[1]) {};

  virtual double operator()(const double x, double &derivative) const;

  virtual double operator()(const double x) const;

private:
  double minimum;
  double slope;
};

/** \brief Applies a linear penalty to a violation that exceeds a threshold.
 *
 *  The penalty is evaluated as:
 *    - \f$ f(x) = y_0 \f$ when \f$ | x - x_0 | < h \f$
 *    - \f$ f(x) = y_0 + a \times | x - x_0 | \f$ in other cases
 *  \f$ h \f$ is the half width of the range that is allowed without penalty and \f$ a \f$ is the slope
 */
class FlatBottomLinearPenalty : public RestraintFunction {
public:

  /** @brief Creates a FlatBottomLinearPenalty object for a given minimum location and a slope
   * @param parameters - four values: \f$ x_0\f$,  slope \f$ a\f$, half_width \f$ h\f$ and
   *    minimum penalty \f$ y_0\f$
   */
  FlatBottomLinearPenalty(const std::vector<double> & parameters);

  double operator()(const double x, double &derivative) const;

  double operator()(const double x) const;

private:
  double minimum_x;
  double slope;
  double half_width;
  double minimumPenalty;
};

/** \brief Evaluates a restraint according to a harmonic potential.
 *
 * Restraint value is defined as:
 * \f$ k \fract{(x-x_0)^2}{2}  \f$
 *
 */
class HarmonicRestraintFunction : public RestraintFunction {
public:

  /** @brief Creates a HarmonicRestraintFunction object for a given minimum location and a force constant
   * @param parameters - two values: location of the function's minimum location \f$ x_0 \f$
   *        and a force constant \f$ k \f$
   */
  HarmonicRestraintFunction(const std::vector<double> & parameters): mean(parameters[0]), force(parameters[1]) {};

  virtual double operator()(const double x, double &derivative) const;

  virtual double operator()(const double x) const;

private:
  double mean;
  double force;
};


/** \brief Evaluates a restraint according to a number of harmonic potentials.
 *
 * The lowest (minimum) of the harmonic penalties is reported as the final function value.
 */
class MinMultiHarmonicFunction : public RestraintFunction {
public:

  /** @brief Creates a MinMultiHarmonicFunction object
   * @param parameters - two values: location of the function's minimum location \f$ x_0 \f$
   *        and a force constant \f$ k \f$
   */
  MinMultiHarmonicFunction(const std::vector<double> & parameters);

  virtual double operator()(const double x, double &derivative) const;

  virtual double operator()(const double x) const;

private:
  std::vector<double> mean;
  std::vector<double> force;
};

/** \brief Evaluates a restraint according to a Lorentzian function.
 *
 * Restraint value is defined as:
 * \f$  \fract{1}{2\pi} \fract{\gamma}{(x-x_0)^2+(1/2 \gamma)^2} -1 \f$
 *
 * Energy at the minimum is -1, far from that point is 0.0
 */
class LorentzianRestraint : public RestraintFunction {
public:

  /** @brief Creates a HarmonicRestraintFunction object for a given minimum location and a force constant
   * @param parameters - two values: location of the function's minimum location \f$ x_0 \f$
   *        and a force constant \f$ k \f$
   */
  LorentzianRestraint(const std::vector<double> & parameters):
    mean(parameters[0]), gamma(parameters[1]) {};

  virtual double operator()(const double x, double &derivative) const;

  virtual double operator()(const double x) const;

private:
  double mean;
  double gamma;
};

class SplineRestraint : public RestraintFunction {
public:

    /** @brief Creates a SplineRestraintFunction object for a given x and y vectors
     * @param parameters - values: first half is considered as x values and second is considered as y values
     */
    SplineRestraint(const std::vector<double> & parameters);

    virtual double operator()(const double x, double &derivative) const;

    virtual double operator()(const double x) const;

private:
    core::calc::numeric::CatmullRomInterpolator<double> cri;
    std::vector<double> vx{};
    std::vector<double> vy{};

};

/** @brief Creates an energy function based on restraints.
 *
 * The input file must provide the following columns:
 * i_atom  j_atom weight FUNC_NAME func_param1  func_param2 ..
 * where i_atom  and j_atom are indexes of the two constrained atoms and weight is used to scale energy
 * of this constraint. The remaining parameters define a function used to transform a distance value
 * into an energy value. FUNC_NAME can be: HARMONIC (or HarmonicRestraintFunction),
 * LORENTZIAN (or LorentzianRestraint), LINEAR (or LinearPenaltyFunction) and FLATBOTTOM (or FlatBottomLinearPenalty).
 * Each of them requires some numeric parameters - see definition of particular classes for details
 *
 * @param function_name - name of a restraint function to be created
 * @param parameters - parameters for the function, e.g. equilibrium distance and force constant for a harmonic function
 * @return a shared pointer to a RestraintFunction object
 */
RestraintFunction_SP
restraint_function_factory(const std::string &function_name, const std::vector<double> &parameters);

}
}
}

#endif
