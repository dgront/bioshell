#ifndef SIMULATIONS_FORCEFIELDS_RESTRAINTS_Restraint_HH
#define SIMULATIONS_FORCEFIELDS_RESTRAINTS_Restraint_HH

#include <string>
#include <vector>
#include <cmath>

#include <core/index.hh>
#include <core/data/basic/Vec3I.hh>
#include <simulations/forcefields/restraints/RestraintFunction.hh>

namespace simulations {
namespace forcefields {
namespace restraints {

/** @brief Abstract restraint.
 */
class Restraint {
public:

  /// Empty virtual constructor to satisfy the compiler
  virtual ~Restraint() {}

  /// Returns the name of this restraint, e.g. "DistanceRestraint"
  virtual const std::string & name() const = 0;

  /// Evaluates energy of this restraint
  virtual double evaluate() const = 0;

protected:
  RestraintFunction_SP f_;
};

typedef std::shared_ptr<Restraint> Restraint_SP;

/** @brief Restraints a distance between two atoms.
 */
class DistanceRestraint : public Restraint {
public:

  DistanceRestraint(const core::data::basic::Vec3I& vi, const core::data::basic::Vec3I& vj, RestraintFunction_SP f)
    : name_("DistanceRestraint"), vi_(vi), vj_(vj) {
    f_ = f;
  }

  /// Empty virtual constructor to satisfy the compiler
  virtual ~DistanceRestraint() {}

  /// Returns the name of this restraint, e.g. "DistanceRestraint"
  virtual const std::string & name() const { return name_; }

  /** @brief Evaluates energy of a distance restraint.
   *
   * Distance between atoms will be converted into an energy value with a given RestraintFunction
   * @return energy value
   */
  virtual double evaluate() const { return (*f_)(sqrt(vi_.closest_distance_square_to(vj_))); }

private:
  std::string name_;
  const core::data::basic::Vec3I & vi_;
  const core::data::basic::Vec3I & vj_;
};

}
}
}

#endif
