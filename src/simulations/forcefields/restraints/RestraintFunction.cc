#include <simulations/forcefields/restraints/RestraintFunction.hh>
#include <math.h>

namespace simulations {
namespace forcefields {
namespace restraints {


/** \brief Evaluates the function value and its derivative at x.
 * @param x - function argument, on which a restraint is based, e.g. interatomic distance
 * @param derivative - one-element array, used to store derivative's result
 * @return function value at x
 */
double LinearPenaltyFunction::operator()(const double x, double &derivative) const {

  double z = (x - minimum);
  if (z > 0) {
    derivative = slope;
    return z * slope;
  } else {
    if (z == 0) {
      derivative = 0;
      return 0.0;
    }
  }
  derivative = -slope;
  return -z * slope;
}

double LinearPenaltyFunction::operator()(const double x) const { return fabs(x - minimum) * slope; }

FlatBottomLinearPenalty::FlatBottomLinearPenalty(const std::vector<double> & parameters) {
  minimum_x = parameters[0];
  slope = parameters[1];
  half_width = parameters[2];
  minimumPenalty = parameters[3];
}

double FlatBottomLinearPenalty::operator()(double x) const {

  double dev = abs(x - minimum_x);
  if (dev < half_width) {
    return minimumPenalty;
  }
  return minimumPenalty + slope * dev;

}

double FlatBottomLinearPenalty::operator()(const double x, double & derivative) const {

  double z = (x - minimum_x);
  if (z > 0) {
    derivative = slope;
    return z * slope;
  } else {
    if (z == 0) {
      derivative = 0;
      return 0.0;
    }
  }
  derivative = -slope;
  return -z * slope;
}

double HarmonicRestraintFunction::operator()(const double x, double & derivative) const {

  double z = (x - mean);
  derivative = -force * z;

  return 0.5 * z * z * force;
}

double HarmonicRestraintFunction::operator()(double x) const {

  double z = (x - mean);
  return 0.5 * z * z * force;
}


double MinMultiHarmonicFunction::operator()(const double x, double & derivative) const {

  double min = -std::numeric_limits<double>::max();
  for (core::index4 i = 0; i < mean.size(); i++) {
    double z = (x - mean[i]);
    double val = 0.5 * z * z * force[i];
    if (val < min) {
      derivative = -force[i] * z;
      min = val;
    }
  }

  return min;
}

double MinMultiHarmonicFunction::operator()(double x) const {

  double min = -std::numeric_limits<double>::max();
  for (core::index4 i = 0; i < mean.size(); i++) {
    double z = (x - mean[i]);
    double val = 0.5 * z * z * force[i];
    if (val < min) min = val;
  }

  return min;
}


double LorentzianRestraint::operator()(const double x, double & derivative) const {

  double z = (x - mean);
  double zz = z * z;
  double s2 = gamma*gamma;

  double div = zz+s2;
  derivative = (2*z)/(zz+s2)-(2*z*zz)/(div*div);

  return zz / (zz  + s2);
}

double LorentzianRestraint::operator()(double x) const {

  double z = (x - mean);
  z = z*z;
  return z / (z  + gamma * gamma) - 1;
}

MinMultiHarmonicFunction::MinMultiHarmonicFunction(const std::vector<double> &parameters) {
  for (core::index2 i = 0; i < parameters.size(); i += 2) {
    mean.push_back(parameters[i]);
    force.push_back(parameters[i + 1]);
  }
}

SplineRestraint::SplineRestraint(const std::vector<double> &parameters) {
    for (core::index2 i = 0; i < parameters.size()/2;++i) {
        vx.push_back(parameters[i]);
    }
    for (core::index2 i = parameters.size()/2; i < parameters.size();++i) {
        vy.push_back(parameters[i]);
    }
}

double SplineRestraint::operator()(const double x, double &derivative) const {
    core::calc::numeric::Interpolate1D<std::vector<double>, double, core::calc::numeric::CatmullRomInterpolator<double> > interpolator(vx,vy, cri);
    return interpolator(x);
}

double SplineRestraint::operator()(const double x) const{
    core::calc::numeric::Interpolate1D<std::vector<double>, double, core::calc::numeric::CatmullRomInterpolator<double> > interpolator(vx,vy, cri);
    return interpolator(x);
}

RestraintFunction_SP restraint_function_factory(const std::string & function_name, const std::vector<double> & parameters) {

  if ((function_name.compare("HARMONIC") == 0) || (function_name.compare("HarmonicRestraintFunction") == 0))
    return std::static_pointer_cast<RestraintFunction>(std::make_shared<HarmonicRestraintFunction>(parameters));

  if ((function_name.compare("MULTIHARMONIC") == 0) || (function_name.compare("MinMultiHarmonicFunction") == 0))
    return std::static_pointer_cast<RestraintFunction>(std::make_shared<MinMultiHarmonicFunction>(parameters));

  if ((function_name.compare("LINEAR") == 0) || (function_name.compare("LinearPenaltyFunction") == 0))
    return std::static_pointer_cast<RestraintFunction>(std::make_shared<LinearPenaltyFunction>(parameters));

  if ((function_name.compare("FLATBOTTOM") == 0) || (function_name.compare("FlatBottomLinearPenalty") == 0))
    return std::static_pointer_cast<RestraintFunction>(std::make_shared<FlatBottomLinearPenalty>(parameters));

  if ((function_name.compare("LORENTZIAN") == 0) || (function_name.compare("LorentzianRestraint") == 0))
    return std::static_pointer_cast<RestraintFunction>(std::make_shared<LorentzianRestraint>(parameters));

  if ((function_name.compare("SPLINE") == 0) || (function_name.compare("SplineRestraint") == 0))
      return std::static_pointer_cast<RestraintFunction>(std::make_shared<SplineRestraint>(parameters));

  return nullptr;
};

}
}
}
