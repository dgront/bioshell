#ifndef SIMULATIONS_FORCEFIELDS_CABS_CabsR13_HH
#define SIMULATIONS_FORCEFIELDS_CABS_CabsR13_HH

#include <memory>
#include <vector>

#include <core/data/sequence/SecondaryStructure.hh>
#include <core/data/io/ss2_io.hh>
#include <utils/string_utils.hh>

#include <simulations/forcefields/mf/ShortRangeMFBase.hh>

namespace simulations {
namespace forcefields {
namespace cabs {

using namespace core::data::sequence;

/** @brief Knowledge based potential that evaluates energy of a \f$R_{13}\f$ distance.
 *
 * \todo_code change the implementation so atom types are used rather than atom names.
 * \todo_code introduce a global map that binds global keywords to database file names.
 * The \f$R_{13}\f$ distance is defined between <code>i</code> and <code>i+2</code> atoms; by default alpha carbons (CA atoms) are used.
 *
 * @see the other 1D local potentials are: R15, T14, R14x, A13 and R12
 */
class CabsR13 : public mf::ShortRangeMFBase {
public:

  /** \brief Creates R13 energy function based on string parameters.
   *
   * @param system - the system whose energy will be evaluated
   * @param parameters - a vector of string parameters. Three  parameters are required by this constructor:
   *     - energy parameters file; use '-' (dash) character to use the default file that is stored in the bioshell parameters set
   *     - secondary structure data in SS2 file format
   *     - pseudocounts, e.g "0.001"
   */
  CabsR13(const systems::CartesianAtoms & system, const std::vector<std::string> & parameters) :
    CabsR13(system, core::data::io::read_ss2(parameters[1]), parameters[0], utils::from_string<double>(parameters[2])) {}

  /** \brief Creates R13 energy function.
   *
   * @param system - the system whose energy will be evaluated
   * @param scored_sequence - amino acid sequence as a string
   * @param scored_secondary - secondary structure as a string
   * @param ff_file - energy parameters file; use '-' (dash) character to use the default file that is stored in the bioshell parameters set
   * @param pseudocounts - pseudocounts value, e.g "0.001"
   */
  CabsR13(const systems::CartesianAtoms & system, const std::string &scored_sequence, const std::string &scored_secondary,
      const std::string & ff_file = "forcefield/cabs/R13_cabs.dat", const double pseudocounts = 0.001)
      : mf::ShortRangeMFBase(system, (ff_file != "-") ? ff_file : "forcefield/cabs/R13_cabs.dat",
        scored_sequence, scored_secondary, 0, 2, 3, "CabsR13", pseudocounts), logger("CabsR13") {}

  /** \brief Creates R13 energy function.
   *
   * @param system - the system whose energy will be evaluated
   * @param scored_sequence - secondary structure object provides the sequence and secondary structure data for energy evaluation
   * @param ff_file - energy parameters file; use '-' (dash) character to use the default file that is stored in the bioshell parameters set
   * @param pseudocounts - pseudocounts value, e.g "0.001"
   */
  CabsR13(const systems::CartesianAtoms & system, const SecondaryStructure_SP & scored_sequence,
      const std::string & ff_file = "forcefield/cabs/R13_cabs.dat", const double pseudocounts = 0.001)
      : mf::ShortRangeMFBase(system, (ff_file != "-") ? ff_file : "forcefield/cabs/R13_cabs.dat",
      scored_sequence->sequence, scored_sequence->str(), 0, 2, 3, "CabsR13", pseudocounts), logger("CabsR13") {}

  /// Empty virtual constructor to satisfy the compiler
  ~CabsR13() override = default;

private:
  utils::Logger logger;
};

}
}
}

#endif

