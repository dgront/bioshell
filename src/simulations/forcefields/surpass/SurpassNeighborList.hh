#ifndef SIMULATIONS_FORCEFIELDS_SURPASS_SurpassNeighborList_HH
#define SIMULATIONS_FORCEFIELDS_SURPASS_SurpassNeighborList_HH

#include <core/index.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>
#include <simulations/forcefields/NeighborList.hh>

namespace simulations {
namespace forcefields {
namespace surpass {

class SurpassContactExclusions : public ExcludeSequenceNeighbors {
public:
  SurpassContactExclusions(const NeighborList & enclosing_list, const std::string & secondary) :
      ExcludeSequenceNeighbors(4, enclosing_list) {
    secondary_ = secondary;
  }

  ~SurpassContactExclusions() override = default;

  /** @brief Returns true if a given atom  should be excluded from a list of neighbors.
   *
   * Since loops do not interact in SURPASS, this method returns true for C atom type
   * @param i - index of the first atom in a pair
   * @return true if a given atom should be excluded from non-bonded interactions
   */
  bool if_atom_excluded(core::index4 i) override;

  /** @brief Returns true if this pair of atoms should be excluded from a list of neighbors.
   *
   * @param i - index of the first atom in a pair
   * @param j  - index of the second atom in a pair
   * @return true if this pair of atoms (i,j) should be excluded from non-bonded interactions
   */
  bool if_pair_excluded(core::index4 i, core::index4 j) override;

private:
  std::string secondary_;
};


}
}
}

#endif
