#include <core/index.hh>
#include <core/data/basic/Vec3.hh>

#include <simulations/forcefields/surpass/SurpassLocalRepulsionEnergy.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>

namespace simulations {
namespace forcefields {
namespace surpass {

using namespace systems::surpass;

double SurpassLocalRepulsionEnergy::calculate_one_residue(systems::CartesianAtoms &the_system, core::index4 which_atom){
    SurpassAlfaChains& surpass=dynamic_cast<SurpassAlfaChains&>(the_system);

    const static double R2 = SURPASS_LOCAL_REPULSION_CUTOFF * SURPASS_LOCAL_REPULSION_CUTOFF;
    const static char counts[] = {2,6,4};

    // --- N is the maximum number of neighbours in the sphere
    core::index2 N = counts[surpass.surpass_atoms()[which_atom].atom_type];
    unsigned int n = 0;   // --- actual number of neighbours in the sphere
    nb_list_.update(which_atom);
    for (auto i_it = nb_list_.cbegin(which_atom); i_it != nb_list_.cend(which_atom); ++i_it) {
        double r2 = surpass.surpass_atoms()[*i_it].closest_distance_square_to(surpass.surpass_atoms()[which_atom]);
        if (r2 < R2) ++n;
    }
    if (n > N){
        return (n - N)*(n - N);}
    else return 0.0;
}


double SurpassLocalRepulsionEnergy::energy(systems::CartesianAtoms & conformation){
    SurpassAlfaChains& surpass=dynamic_cast<SurpassAlfaChains&>(conformation);

    double en = 0.0;
    nb_list_.update_all();
    for (core::index4 k = 0; k < surpass.n_surpass_atoms; ++k) {
        double e = calculate_one_residue(conformation,k);

        en += e;
    }
    return en;
}

double SurpassLocalRepulsionEnergy::energy(systems::CartesianAtoms & conformation, const core::index4 which_atom){
    SurpassAlfaChains& surpass=dynamic_cast<SurpassAlfaChains&>(conformation);

    core::index4 start = (int(which_atom)-3 > 0) ? int(which_atom)-3: 0;
    core::index4 stop = (which_atom <= int(surpass.n_surpass_atoms-1)) ? which_atom : int(surpass.n_surpass_atoms-1); // indexes are till n_atoms-1
    double en=0.0;

    by_chunk_evaluation_.clear();
    for (size_t wr=start;wr<=stop;wr++){
        nb_list_.update(wr);
        by_chunk_evaluation_.insert(wr);
        for (auto i_it = nb_list_.cbegin(wr); i_it != nb_list_.cend(wr); ++i_it)
            by_chunk_evaluation_.insert(*i_it);
    }

    for (auto atm=by_chunk_evaluation_.cbegin();atm!=by_chunk_evaluation_.cend();++atm){
        en += calculate_one_residue(conformation,*atm);

    }
    return en;
}


double SurpassLocalRepulsionEnergy::delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 which_atom,
                                systems::CartesianAtoms & new_conformation){
    return delta_energy(reference_conformation,which_atom,which_atom,new_conformation);
}

double SurpassLocalRepulsionEnergy::delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 atom_from,
                                const core::index4 atom_to, systems::CartesianAtoms & new_conformation){
    double en=0.0;
    SurpassAlfaChains& ref_surpass=dynamic_cast<SurpassAlfaChains&>(reference_conformation);

    core::index4 start = (int(atom_from)-3 > 0) ? int(atom_from)-3: 0;
    core::index4 stop = (atom_to <= int(ref_surpass.n_surpass_atoms-1)) ? atom_to : int(ref_surpass.n_surpass_atoms-1); // indexes are till n_atoms-1

    by_chunk_evaluation_.clear();
    for (size_t wr=start;wr<=stop;wr++){
        nb_list_.update(wr);
        by_chunk_evaluation_.insert(wr);
        for (auto i_it = nb_list_.cbegin(wr); i_it != nb_list_.cend(wr); ++i_it)
            by_chunk_evaluation_.insert(*i_it);
    }

    for (auto atm=by_chunk_evaluation_.cbegin();atm!=by_chunk_evaluation_.cend();++atm){
        en -= calculate_one_residue(reference_conformation,*atm);
        en += calculate_one_residue(new_conformation,*atm);

    }
    return en;
}



const double SurpassLocalRepulsionEnergy::SURPASS_LOCAL_REPULSION_CUTOFF = 6.0;

} // ~ simulations
} // ~ generic
} // ~ ff
