#ifndef SIMULATIONS_GENERIC_FF_SurpassLocalRepulsionEnergy_HH
#define SIMULATIONS_GENERIC_FF_SurpassLocalRepulsionEnergy_HH

#include <memory>
#include <cmath>
#include <set>

#include <core/index.hh>
#include <utils/Logger.hh>

#include <simulations/systems/surpass/SurpassAlfaChains.hh>
//#include <simulations/forcefields/LongRangeByResidues.hh>
#include <simulations/forcefields/surpass/SurpassNeighborList.hh>
#include <simulations/forcefields/ByAtomEnergy.hh>

namespace simulations {
namespace forcefields {
namespace surpass {

/** @brief Local repulsion energy for SURPASS model
 * @tparam C - type used to represent coordinates
 */
class SurpassLocalRepulsionEnergy : public ByAtomEnergy {
public:

  /// radius of 'small ball' - the sphere around which_residue
  const static double SURPASS_LOCAL_REPULSION_CUTOFF;

  explicit SurpassLocalRepulsionEnergy(const systems::surpass::SurpassAlfaChains &system) : ByAtomEnergy("SurpassLocalRepulsionEnergy"),
    nb_list_(system.surpass_atoms(),system.n_surpass_atoms ,6.0, 6.0), logger("SurpassLocalRepulsionEnergy"), name_("SurpassLocalRepulsionEnergy") {
      auto ptr = std::make_shared<ExcludeSequenceNeighbors>(4,nb_list_);
      nb_list_.neighbor_rules(std::dynamic_pointer_cast<NeighborListRules>(ptr));
  }

  SurpassLocalRepulsionEnergy(const systems::surpass::SurpassAlfaChains &system,
                              const std::vector<std::string> &parameters) : SurpassLocalRepulsionEnergy(system) {}

  const std::string &name() const { return name_; }

    /** @brief Calculates the total energy of a given conformation.
     * @param conformation - conformation to assess
     * @return energy value
     */
    virtual double energy(systems::CartesianAtoms & conformation);

    /** @brief Calculate energy contribution due to a single atom.
   * @param conformation - conformation to assess
   * @param which_atom - index of an atom
   * @return energy value
   */
    virtual double energy(systems::CartesianAtoms & conformation, const core::index4 which_atom);

    /** @brief Calculate the energy difference due to a single atom.
     * The returned energy difference will be evaluated based on the assumption that only @code which_atom @endcode
     * differs between the two conformations
     * @param reference_conformation - initial conformation to assess
     * @param which_atom - index of an atom
     * @param new_conformation - the new conformation after a MC move to assess
     * @return energy difference evaluated as @code E(new) - E(reference) @endcode
     */
    virtual double delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 which_atom,
                                systems::CartesianAtoms & new_conformation);

    /** @brief Calculate the energy difference due to a contiguous range of atoms.
     * @param reference_conformation - initial conformation to assess
     * @param atom_from - index of the first atom of the moved chunk
     * @param atom_to  - index of the last atom of the moved chunk
     * @param new_conformation - the new conformation after a MC move to assess
     * @return energy difference evaluated as @code E(new) - E(reference) @endcode
     */
    virtual double delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 atom_from,
                                const core::index4 atom_to, systems::CartesianAtoms & new_conformation);

private:
  simulations::forcefields::NeighborList nb_list_;
  utils::Logger logger;
  const std::string name_;
  std::set<core::index4> by_chunk_evaluation_;
  double calculate_one_residue(systems::CartesianAtoms &,core::index4 which_atom);
};


} // ~ simulations
} // ~ generic
} // ~ ff
#endif
