#include <core/data/basic/Vec3Cubic.hh>

#include <simulations/forcefields/surpass/SurpassContactEnergy.hh>
#include <simulations/forcefields/surpass/SurpassNeighborList.hh>
#include <simulations/forcefields/surpass/SurpassContactParameters.hh>
#include <set>

namespace simulations {
namespace forcefields {
namespace surpass {

using namespace systems::surpass;


void SurpassContactEnergy::set_up_constants_nbl(const systems::surpass::SurpassAlfaChains &system) {

  load_surpass_cutoffs();
  // --- we keep the maximum cutoff distance in contact_longest_distance_ field inherited from ContactEnergy
  for (int i = 0; i < 12; ++i)
    contact_longest_distance_ = std::max(contact_longest_distance_, contact_max_distance_[i] * 0.1);
  contact_longest_distance2 = contact_longest_distance_ * contact_longest_distance_;
  // --- set up neighbor list: cutoff and rules
  nbl.cutoff(contact_longest_distance_);
  auto ptr = std::make_shared<SurpassContactExclusions>(nbl, system.surpass_ss2());
  nbl.neighbor_rules(std::dynamic_pointer_cast<NeighborListRules>(ptr));
}

SurpassContactEnergy::SurpassContactEnergy(const systems::surpass::SurpassAlfaChains &system,
    const std::vector<std::string> &parameters) : ContactEnergy(system, 0, 0, 0, 0, 0),
    nbl(system.surpass_atoms(), system.n_surpass_atoms, 0, 6), logger("SurpassContactEnergy") {

  if (parameters.size() < 2)
    std::runtime_error( "SurpassContactEnergy requires exactly two parameters: high_energy_level, low_energy_level!");
  init(utils::to_double(parameters[0].c_str()),utils::to_double(parameters[1].c_str()),0,0,0);
  set_up_constants_nbl(system);
  name("SurpassContactEnergy");
}


SurpassContactEnergy::SurpassContactEnergy(const systems::surpass::SurpassAlfaChains &system,
    const double high_energy_level, const double low_energy_level)
    : ContactEnergy(system, high_energy_level, low_energy_level, 0, 0, 0),
    nbl(system.surpass_atoms(), system.n_surpass_atoms, 0, 6), logger("SurpassContactEnergy") {

  set_up_constants_nbl(system);
  name("SurpassContactEnergy");
}

void SurpassContactEnergy::load_surpass_cutoffs() {

  SurpassContactParameters &cutoffs = SurpassContactParameters::get();
  auto min = cutoffs.contact_rep_distance();
  auto avg = cutoffs.contact_min_distance();
  auto max = cutoffs.contact_max_distance();
  for (int i = 0; i < 12; ++i) {
    contact_rep_distance_[i] = min[i];
    contact_min_distance_[i] = avg[i];
    contact_max_distance_[i] = max[i];
  }
}

double SurpassContactEnergy::energy_from_distance(double distance_square, core::index1 pair_id) {

  double max_d = contact_max_distance_[pair_id] * 0.1;

  if (distance_square < max_d * max_d) {
    double min_d = contact_min_distance_[pair_id] * 0.1;
    if (distance_square > min_d * min_d) {
      return low_energy_level_;
    }
    double excl_d = contact_rep_distance_[pair_id] * 0.1;
    if (distance_square < excl_d * excl_d)
      return high_energy_level_;
  }
  return 0.0;
}

double SurpassContactEnergy::evaluate_energy(systems::surpass::SurpassAlfaChains &conformation,
    core::index4 i_at, core::index4 j_at) {

  if (nbl.if_pair_excluded(j_at, i_at)) return 0.0;

  // --- index to FF parameters for a given atom type combination
  core::index1 pair_id = (conformation[i_at].atom_type << 2) + conformation[j_at].atom_type;

  // --- Maximum distance to score any contact
  double r2 = conformation.surpass_atoms()[i_at].closest_distance_square_to(conformation.surpass_atoms()[j_at],
      contact_longest_distance2);

  return energy_from_distance(r2, pair_id);
}

double SurpassContactEnergy::energy(systems::CartesianAtoms &conformation) {

  double energy = 0.0;

  nbl.update_all();

  SurpassAlfaChains &surpass = dynamic_cast<SurpassAlfaChains &>(conformation);
  for (core::index4 which_atom = 0; which_atom < surpass.n_surpass_atoms; which_atom++) {
    const core::data::basic::Vec3I &o = surpass.surpass_atoms()[which_atom];
    core::index1 o_type = o.atom_type << 2;
    for (core::index4 i = 0; i < which_atom; i++) {
      if (!nbl.if_pair_excluded(which_atom, i)) {

        double r2 = o.closest_distance_square_to(surpass.surpass_atoms()[i], contact_longest_distance2);
        core::index1 pair_id = o_type + surpass.surpass_atoms()[i].atom_type;
        energy += energy_from_distance(r2, pair_id);
      }
    }
  }

  return energy;
}

double SurpassContactEnergy::delta_energy(systems::CartesianAtoms &reference_conformation,
    const core::index4 which_atom, systems::CartesianAtoms &new_conformation) {

    SurpassAlfaChains &ref_surpass = dynamic_cast<SurpassAlfaChains &>(reference_conformation);
    SurpassAlfaChains &new_surpass = dynamic_cast<SurpassAlfaChains &>(new_conformation);
    double en =0.0;
    core::index4 start = (int(which_atom) - 3 > 0) ? int(which_atom) - 3 : 0;
    core::index4 stop = (which_atom <= int(ref_surpass.n_surpass_atoms - 1)) ? which_atom : int(
            ref_surpass.n_surpass_atoms - 1); // indexes are till n_atoms-1
    for (auto i = start; i <= stop; ++i) {
        nbl.update(i);

        for (auto j = nbl.cbegin(i); j != nbl.cend(i); ++j) {
            const core::data::basic::Vec3I &o_ref = ref_surpass.surpass_atoms()[i];
            const core::data::basic::Vec3I &o_new = new_surpass.surpass_atoms()[i];
            core::index1 ref_type = ref_surpass.surpass_atoms()[i].atom_type << 2;
            double r2a = o_ref.closest_distance_square_to(ref_surpass.surpass_atoms()[*j],
                                                          contact_longest_distance2);
            double r2 = o_new.closest_distance_square_to(new_surpass.surpass_atoms()[*j],
                                                         contact_longest_distance2);
            if (r2 == r2a) continue;
            core::index1 pair_id_old = (ref_type) + ref_surpass.surpass_atoms()[*j].atom_type;
            core::index1 pair_id_new = (ref_type) + new_surpass.surpass_atoms()[*j].atom_type;

            en += energy_from_distance(r2, pair_id_new) - energy_from_distance(r2a, pair_id_old);
        }

    }
    return en;
    }


double SurpassContactEnergy::delta_energy(systems::CartesianAtoms &reference_conformation, const core::index4 atom_from,
    const core::index4 atom_to, systems::CartesianAtoms &new_conformation) {

  SurpassAlfaChains &ref_surpass = dynamic_cast<SurpassAlfaChains &>(reference_conformation);
  SurpassAlfaChains &new_surpass = dynamic_cast<SurpassAlfaChains &>(new_conformation);

  core::index4 start = (int(atom_from) - 3 > 0) ? int(atom_from) - 3 : 0;
  core::index4 stop = (atom_to <= int(ref_surpass.n_surpass_atoms - 1)) ? atom_to : int(
      ref_surpass.n_surpass_atoms - 1); // indexes are till n_atoms-1

  std::set<core::index4> to_calc;
  double en = 0.0;
  for (auto i = start; i <= stop; ++i) {
    to_calc.insert(i);
    nbl.update(i);
    for (auto it = nbl.cbegin(i); it != nbl.cend(i); ++it) {
      to_calc.insert(*it);
    }
  }
  for (auto i = to_calc.cbegin(); i != to_calc.cend(); ++i) {
    const core::data::basic::Vec3I &o_ref = ref_surpass.surpass_atoms()[*i];
    const core::data::basic::Vec3I &o_new = new_surpass.surpass_atoms()[*i];
    core::index1 ref_type = ref_surpass.surpass_atoms()[*i].atom_type << 2;
    for (auto j = i; j != to_calc.cend(); ++j) {
      if (!nbl.if_pair_excluded(*j, *i)) {
        double r2a = o_ref.closest_distance_square_to(ref_surpass.surpass_atoms()[*j], contact_longest_distance2);
        double r2 = o_new.closest_distance_square_to(new_surpass.surpass_atoms()[*j], contact_longest_distance2);
        if (r2 == r2a) continue;
        core::index1 pair_id_old = (ref_type) + ref_surpass.surpass_atoms()[*j].atom_type;
        core::index1 pair_id_new = (ref_type) + new_surpass.surpass_atoms()[*j].atom_type;

        en += energy_from_distance(r2,pair_id_new) - energy_from_distance(r2a,pair_id_old);
      }
    }
  }

  return en;
}

}
}
}
