#include <stdlib.h>       // --- for abs()
#include <core/index.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/basic/Vec3Cubic.hh>

#include <simulations/forcefields/surpass/SurpassNeighborList.hh>
#include <simulations/forcefields/surpass/SurpassContactParameters.hh>

namespace simulations {
namespace forcefields {
namespace surpass {

bool SurpassContactExclusions::if_atom_excluded(core::index4 i) {
    return secondary_[i]=='C';
}

bool SurpassContactExclusions::if_pair_excluded(core::index4 i, core::index4 j) {
    // --- A Secondary Structure Elements (H and E) can't interact with itself, but loop has repulsion with the same loop
    if ((enclosing_list_.the_system[i].residue_type == enclosing_list_.the_system[j].residue_type) &&
        (enclosing_list_.the_system[i].atom_type != 2)) {
        return true;

    }
    if (if_atom_excluded(i) || if_atom_excluded(j)) return true;
    // E elements can't have contacts - they have only Hbonds - doesn't work for now
 //   if (enclosing_list_.the_system[i].atom_type == 1 && enclosing_list_.the_system[j].atom_type == 1) return true;
    // --- contacts with short sequence separation are not scored  when they belong to the same chain
    return (enclosing_list_.the_system[i].chain_id == enclosing_list_.the_system[j].chain_id)  &&
           (abs(int(i) - int(j)) <= sequence_separation_);

}

//    double SurpassNeighborList::square_cutoff_with_border(core::index4 i, core::index4 j) const {
//        core::index2 atom_type_i = surpass_system.surpass_atoms()[i].atom_type;
//        double c2 = SurpassContactParameters::get().max_contact_distance(atom_type_i, surpass_system.surpass_atoms()[j].atom_type) +
//                    NeighborList::border_width();
//        if (atom_type_i == 2 && surpass_system.surpass_atoms()[j].atom_type == 2)
//            c2 = SurpassContactParameters::get().repulsion_distance(2, 2) + NeighborList::border_width();
//
//        return c2 * c2;
//    }
}
}
}
