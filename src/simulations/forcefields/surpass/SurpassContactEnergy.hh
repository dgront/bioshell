#ifndef SIMULATIONS_CARTESIAN_FF_SurpassContactEnergy_HH
#define SIMULATIONS_CARTESIAN_FF_SurpassContactEnergy_HH

#include <simulations/systems/surpass/SurpassAlfaChains.hh>
#include <simulations/systems/CartesianAtoms.hh>

#include <simulations/forcefields/cartesian/ContactEnergy.hh>

namespace simulations {
namespace forcefields {
namespace surpass {


/** @brief Contact potential for SURPASS chains
 */
class SurpassContactEnergy : public cartesian::ContactEnergy {
public:

  /** @brief Define a square well contact potential
   *
   * @param system - system to be scored
   * @param parameters - parameters necessary to create this energy function given as a vector of strings.
   *    The vector must provide <code>high_energy_level</code>, <code>low_energy_level</code> and <code>contact_shift</code>
   *    exactly in this order
   */
  SurpassContactEnergy(const systems::surpass::SurpassAlfaChains &system, const std::vector<std::string> &parameters);

  /** @brief Define a square well contact potential
   *
   * @param system - system to be scored
   * @param high_energy_level - a finite-value excluded volume penalty : energy value assigned
   *    when two interacting centers are closer to each other than hard core repulsion distance.
   * @param low_energy_level - energy rewarded for each contact
   * @param contact_shift - a correction to ideal distance values listed in SURPASS force field
   */
  SurpassContactEnergy(const systems::surpass::SurpassAlfaChains &system, const double high_energy_level, const double low_energy_level);

  virtual ~SurpassContactEnergy() {}

    double energy(systems::CartesianAtoms &conformation) override;

    /** @brief Calculates SURPASS contact energy after moving a given residue.
   *
   * In the current implementation this method returns actually the total contact energy of a modelled system;
   * this call actually calls LongRangeByResiduesNBL<C>::calculate()
   * @param which_residue - residue that had been moved
   * @return SURPASS contact energy
   */
  double delta_energy(systems::CartesianAtoms &reference_conformation, const core::index4 which_atom,
      systems::CartesianAtoms &new_conformation) override ;

  /** @brief Calculates SURPASS contact energy after moving a given chunk of atoms.
   *
   * In the current implementation this method returns actually the total contact energy of a modelled system;
   * this call actually calls LongRangeByResiduesNBL<C>::calculate()
   * @param first_res - first atom of the moved chunk
   * @param last_res - last atom of the moved chunk
   * @return SURPASS contact energy
   */
  double delta_energy(systems::CartesianAtoms &reference_conformation,const core::index4 atom_from,
        const core::index4 atom_to, systems::CartesianAtoms &new_conformation) override;

  /// Evaluate energy between two atoms
  double evaluate_energy(systems::surpass::SurpassAlfaChains &conformation, core::index4 i_at, core::index4 j_at);

  double energy_from_distance(double distance_square, core::index1 index);

protected:
  core::index1 contact_rep_distance_[12]; ///< holds excluded volume distances between H,E,C centres
  core::index1 contact_min_distance_[12]; ///< holds distances where a contact reward starts
  core::index1 contact_max_distance_[12]; ///< holds maximum distance where contact is still rewarded
  NeighborList nbl;

private:
  utils::Logger logger;

  void load_surpass_cutoffs();
  /** @brief sets up contact parameters and NBL object
   * The code has been extracted into a method to avoid code duplication in constructors
   */
  void set_up_constants_nbl(const systems::surpass::SurpassAlfaChains &system);
};

}
}
}
#endif
