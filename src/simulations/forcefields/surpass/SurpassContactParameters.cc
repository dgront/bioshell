#include <simulations/forcefields/surpass/SurpassContactParameters.hh>

#include <core/BioShellEnvironment.hh>
#include <core/data/io/DataTable.hh>

namespace simulations {
namespace forcefields {
namespace surpass {

SurpassContactParameters::SurpassContactParameters(const std::string &fname) : logger("SurpassContactParameters") {

  core::data::io::DataTable dt;
  dt.load(core::BioShellEnvironment::from_file_or_db(fname));
  for (const auto &row : dt) {
    core::index2 i = row.get<core::index2>(0);
    core::index2 j = row.get<core::index2>(1);
    core::index1 v = core::index1(row.get<double>(2) * 10);
    contact_min_distance_[(i << 2) + j] = v;
    v = core::index1(row.get<double>(3) * 10);
    contact_ave_distance_[(i << 2) + j] = v;
    v = core::index1(row.get<double>(4) * 10);
    contact_max_distance_[(i << 2) + j] = v;
  }
  if (logger.is_logable(utils::LogLevel::FINE)) {
    logger << utils::LogLevel::FINE << "Contact excluded volume parameters:\n"
           << "\t     H    E    C\n" << utils::string_format("\tH %6.2f%6.2f%6.2f\n", contact_min_distance_[0] * 0.1,
                                                             contact_min_distance_[1] * 0.1, contact_min_distance_[2] * 0.1)
           << utils::string_format("\tE %6.2f%6.2f%6.2f\n", contact_min_distance_[4] * 0.1,
                                   contact_min_distance_[5] * 0.1, contact_min_distance_[6] * 0.1)
           << utils::string_format("\tC %6.2f%6.2f%6.2f\n", contact_min_distance_[8] * 0.1,
                                   contact_min_distance_[9] * 0.1, contact_min_distance_[10] * 0.1);
  }

  logger << utils::LogLevel::FINE << "Contact energy well starts at:\n"
         << "\t     H    E    C\n" << utils::string_format("\tH %6.2f%6.2f%6.2f\n", contact_ave_distance_[0] * 0.1,
                                                           contact_ave_distance_[1] * 0.1, contact_ave_distance_[2] * 0.1)
         << utils::string_format("\tE %6.2f%6.2f%6.2f\n", contact_ave_distance_[4] * 0.1,
                                 contact_ave_distance_[5] * 0.1, contact_ave_distance_[6] * 0.1)
         << utils::string_format("\tC %6.2f%6.2f%6.2f\n", contact_ave_distance_[8] * 0.1,
                                 contact_ave_distance_[9] * 0.1, contact_ave_distance_[10] * 0.1);

  logger << utils::LogLevel::FINE << "Contact energy well ends at:\n"
  << "\t     H    E    C\n" << utils::string_format("\tH %6.2f%6.2f%6.2f\n", contact_max_distance_[0] * 0.1,
  contact_max_distance_[1] * 0.1, contact_max_distance_[2] * 0.1)
  << utils::string_format("\tE %6.2f%6.2f%6.2f\n", contact_max_distance_[4] * 0.1,
  contact_max_distance_[5] * 0.1, contact_max_distance_[6] * 0.1)
  << utils::string_format("\tC %6.2f%6.2f%6.2f\n", contact_max_distance_[8] * 0.1,
  contact_max_distance_[9] * 0.1, contact_max_distance_[10] * 0.1);
}


}
}
}
