#ifndef SIMULATIONS_FORCEFIELD_SURPASS_SurpassContactEnergy_HH
#define SIMULATIONS_FORCEFIELD_SURPASS_SurpassContactEnergy_HH

#include <string>

#include <core/index.hh>
#include <utils/Logger.hh>

namespace simulations {
namespace forcefields {
namespace surpass {

class SurpassContactParameters {
public:

  typedef core::index1 SurpassCutoffDefinitionArray[12];

  /// Returns the reference to the singleton instance of the parameter's set
  static SurpassContactParameters &get() {
    static thread_local SurpassContactParameters instance;
    return instance;
  }

  /// Returns excluded volume distances
  inline double repulsion_distance(core::index1 atom_type_i, core::index1 atom_type_j) {
    core::index1 id = (atom_type_i << 2) + atom_type_j; // index to the array that holds contact distances
    return contact_min_distance_[id] * 0.1;
  }

  /// Returns the shortest distance at which a contact will be rewarded
  inline double min_contact_distance(core::index1 atom_type_i, core::index1 atom_type_j) {
    core::index1 id = (atom_type_i << 2) + atom_type_j; // index to the array that holds contact distances
    return contact_ave_distance_[id] * 0.1;
  }

  /// Returns the longest distance at which a contact will be rewarded
  inline double max_contact_distance(core::index1 atom_type_i, core::index1 atom_type_j) {
    core::index1 id = (atom_type_i << 2) + atom_type_j; // index to the array that holds contact distances
    return contact_max_distance_[id] * 0.1;
  }

  const SurpassCutoffDefinitionArray & contact_rep_distance() const { return contact_min_distance_; }
  const SurpassCutoffDefinitionArray & contact_min_distance() const { return contact_ave_distance_; }
  const SurpassCutoffDefinitionArray & contact_max_distance() const { return contact_max_distance_; }

private:
  utils::Logger logger;
  core::index1 contact_min_distance_[12]; ///< holds excluded volume distances between H,E,C centres
  core::index1 contact_ave_distance_[12]; ///< holds distances where a contact reward starts
  core::index1 contact_max_distance_[12]; ///< holds maximum distance where contact is still rewarded

  /** @brief Loads SURPASS contact parameters from an input file.
   *
   * This class is a singleton to assure the data has been loaded only once.
   * @param fname - contact potential definition, typically located in data/forcefield subdirectory
   */
  SurpassContactParameters(const std::string &fname = "forcefield/surpass_contact.dat");
};

}
}
}
#endif // SIMULATIONS_FORCEFIELD_SURPASS_SurpassContactEnergy_HH
