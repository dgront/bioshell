#include <simulations/forcefields/NeighborList.hh>

namespace simulations {
namespace forcefields {

NeighborList::NeighborList(const std::vector<core::data::basic::Vec3I>  &system, core::index4 n_atoms,
    const double cutoff, const double border_zone_radius) :
    the_system(system), coordinates_last_update(n_atoms), logs("NeighborList") {

  excl_rules_ = std::make_shared<NeighborListRules>();  // --- defines exclusions from this NeighborList
  cutoff_ = cutoff;                                     // --- interaction cutoff
  n_atoms_ = n_atoms;
  neighbors.resize(n_atoms_);
  border_width(border_zone_radius);           // --- set thickness of the buffer zone; this will call the global update of the NBL  update_all();
  update_all();
}

double NeighborList::updates_ratio(bool clear_counters) {
  double r = n_updated / double(n_called);
  if (clear_counters) {
    n_updated = 0;
    n_called = 0;
  }
  return r;
}

void NeighborList::border_width(const double border) {
  border_width_ = border;
  cutoff_with_border_sq_ = (cutoff_ + border_width_) * (cutoff_ + border_width_);
  border_width_third_sq_ = border_width_ * border_width_ / (2.1 * 2.1);
}

bool NeighborList::update(core::index4 which_atom) {

  if(excl_rules_->if_atom_excluded(which_atom)) return false;

  ++n_called;
  // --- compute the distance which_atom has travelled since the last update
  double d2 = the_system[which_atom].closest_distance_square_to(coordinates_last_update[which_atom]);
  // --- if it moved by more than 1/3 of the buffer zone, its neighbors will be recalculated
  if (d2 < border_width_third_sq_) return false;

  ++n_updated;
  logs << utils::LogLevel::FINE << "update at pos " << which_atom << "\n";
  // ---------- Recomputing neighbors of atom which_atom
  // 1) Remove which_atom from neighbors
  for (core::index4 ii: neighbors[which_atom]) {
    auto it = std::find(neighbors[ii].begin(), neighbors[ii].end(), which_atom);
    if (std::distance(it, neighbors[ii].end()) == 1) // --- if the element being removed is at the end, just pop it
      neighbors[ii].pop_back();
    else {
      *it = neighbors[ii].back(); // --- otherwise replace it with the last element on the list
      neighbors[ii].pop_back();
    }
  }

  neighbors[which_atom].clear();
  // 2) Now find neighbors of which_atom by checking all vs. the which_atom
  const core::data::basic::Vec3I &a = the_system[which_atom];
  for (core::index4 j_atom = 0; j_atom < n_atoms_; ++j_atom) {
    if(excl_rules_->if_atom_excluded(j_atom)) continue;
    if (excl_rules_->if_pair_excluded(which_atom, j_atom)) continue;
    double c2 = square_cutoff_with_border(which_atom, j_atom);
    d2 = a.closest_distance_square_to(the_system[j_atom], c2);
    if (d2 >= c2) continue;
    neighbors[which_atom].push_back(j_atom);
    neighbors[j_atom].push_back(which_atom);
  }
  coordinates_last_update[which_atom].set(the_system[which_atom]);

  return true;
}


void NeighborList::update_all() {

  logs << utils::LogLevel::FINER << "global update\n";

  for (auto &v: neighbors) v.clear();

  for (core::index4 i_atom = 0; i_atom < n_atoms_; ++i_atom) { // --- for every atom of the system
    if(excl_rules_->if_atom_excluded(i_atom)) continue;

    const core::data::basic::Vec3I &a = the_system[i_atom];
    // ----- with every atom with index higher then i_atom (excluding self)
    for (core::index4 j_atom = i_atom + 1; j_atom < n_atoms_; ++j_atom) {
      if(excl_rules_->if_atom_excluded(j_atom)) continue;
      if (excl_rules_->if_pair_excluded(i_atom, j_atom)) continue;
      double c2 = square_cutoff_with_border(i_atom, j_atom);
      double d2 = a.closest_distance_square_to(the_system[j_atom], c2);
      if (d2 >= c2) continue;
      neighbors[i_atom].push_back(j_atom);
      neighbors[j_atom].push_back(i_atom);
    }
    coordinates_last_update[i_atom].set(the_system[i_atom]);
  }
}

std::ostream &operator<<(std::ostream &out, const NeighborList &nbl) {

  for (core::index4 i = 0; i < nbl.n_atoms(); ++i) {
    out << utils::string_format("%5d : ", i);
    for (auto i_it = nbl.cbegin(i); i_it != nbl.cend(i); ++i_it) {
      out << utils::string_format("%d ", *i_it);
    }
    out << "\n";
  }

  return out;
}

}
}

