#ifndef SIMULATIONS_FORCEFIELDS_ConstByAtomEnergy_HH
#define SIMULATIONS_FORCEFIELDS_ConstByAtomEnergy_HH

#include <core/index.hh>
#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/systems/CartesianAtoms.hh>

namespace simulations {
namespace forcefields {

/** @brief Dummy energy function which returns always the same value.
 *
 * To be used by tests where energy class must be provided but the values are irrelevant. Using this energy function
 * in Monte Carlo simulations makes all the moves always accepted
 *
 * @tparam C - type of system, e.g. CartesianAtoms, Ising2D or CartesianChains
 */

class ConstByAtomEnergy : public ByAtomEnergy {
public:
  /**
   * @brief Creates the object with a given constant energy value;
   * @param atom_en - by atom energy
   * @param total_en - total energy of the whole system
   */
  ConstByAtomEnergy(const double atom_en, const double total_en) : ByAtomEnergy("ConstByAtomEnergy"), e(atom_en), E(total_en), name_("ConstByAtomEnergy") {}

  const std::string & name() const { return name_; }

  double delta_energy(systems::CartesianAtoms & system, const core::index4 which_atom, systems::CartesianAtoms & new_system) final { return 0.0; }

  double delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 atom_from,
      const core::index4 atom_to, systems::CartesianAtoms & new_conformation) final  { return 0.0; }

  double energy(systems::CartesianAtoms & system, const core::index4 which_atom) final { return e; }

  double energy(systems::CartesianAtoms & system) final { return E; }

private:
  double e;
  double E;
  const std::string name_;
};

} // ~ simulations
} // ~ ff
#endif
