#include <core/index.hh>

#include <simulations/forcefields/TotalEnergyByAtom.hh>
#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/systems/CartesianChains.hh>

namespace simulations {
namespace forcefields {


double TotalEnergyByAtom::energy(systems::CartesianAtoms & conformation) {
    double en = 0.0;
    for (core::index2 i = 0; i < TotalEnergy::components.size(); ++i)
        en += TotalEnergy::components[i]->energy(conformation) * TotalEnergy::factors[i];
    return en;
}


double TotalEnergyByAtom::energy(systems::CartesianAtoms & conformation, const core::index4 which_atom) {
    double en = 0.0;
    for (core::index2 i = 0; i < TotalEnergy::components.size(); ++i)
        en += TotalEnergy::components[i]->energy(conformation,which_atom) * TotalEnergy::factors[i];
    return en;
}


 double TotalEnergyByAtom::delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 which_atom,systems::CartesianAtoms & new_conformation) {
     double en = 0.0;
     for (core::index2 i = 0; i < TotalEnergy::components.size(); ++i)
         en += TotalEnergy::components[i]->delta_energy(reference_conformation, which_atom, new_conformation)
             * TotalEnergy::factors[i];
     return en;
}


double TotalEnergyByAtom::delta_energy(systems::CartesianAtoms &reference_conformation, const core::index4 atom_from,
    const core::index4 atom_to, systems::CartesianAtoms &new_conformation) {
  double en = 0.0;
  for (core::index2 i = 0; i < TotalEnergy::components.size(); ++i)
    en += TotalEnergy::components[i]->delta_energy(reference_conformation, atom_from, atom_to, new_conformation) *
          TotalEnergy::factors[i];
  return en;
}



std::string TotalEnergyByAtom::header_string() const {

  std::stringstream ss;
  ss << std::setw(ByAtomEnergy::name().size()) << ByAtomEnergy::name() << ' ' <<std::setw(TotalEnergy::sw[0])  << TotalEnergy::components[0]->name();
  for (size_t i = 1; i < TotalEnergy::components.size(); ++i)
    ss << ' ' << std::setw(TotalEnergy::sw[i]) << TotalEnergy::components[i]->name();
  ss << " force_field_tag";

  return ss.str();
}

} // ~ generic
} // ~ ff
