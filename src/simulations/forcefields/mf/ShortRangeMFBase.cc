
#include <cmath>

#include <iostream>

#include <core/index.hh>
#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/Interpolate1D.hh>
#include <core/data/sequence/SecondaryStructure.hh>
#include <utils/string_utils.hh>

#include <simulations/forcefields/mf/ShortRangeMFBase.hh>

namespace simulations {
namespace forcefields {
namespace mf {

ShortRangeMFBase::ShortRangeMFBase(const systems::CartesianAtoms &system, const std::string &ff_file,
    const std::string &scored_sequence, const std::string &scored_secondary, const core::index1 first_aa_pos,
    const core::index1 secnd_aa_pos, const core::index1 property_span, const std::string & energy_name,
    const double pseudocounts) : ByAtomEnergy(energy_name), system_(system) {

  first_aa_pos_ = first_aa_pos;
  second_aa_pos_ = secnd_aa_pos;
  property_span_less_one_ = property_span - 1;

  if (scored_secondary.size() != system.n_atoms)
    utils::exit_with_error("ShortRangeMFBase", 28,
        utils::string_format("number of CA atoms (%d) must equal the number of residues of the given sequence (%d) !",
            system.n_atoms, scored_secondary.size()));

  prepare_ff(ff_file,scored_sequence, scored_secondary, pseudocounts);
}


double ShortRangeMFBase::energy(systems::CartesianAtoms &conformation, const core::index4 which_atom) {

  double en = 0;
  if (which_atom >= property_span_less_one_) {
    double d = conformation[which_atom].distance_to(conformation[which_atom - property_span_less_one_]);
    en += (*ff_for_sequence_[which_atom - property_span_less_one_])(d);
  }
  if (conformation.n_atoms - which_atom >= property_span_less_one_) {
    double d = conformation[which_atom].distance_to(conformation[which_atom + property_span_less_one_]);
    en += (*ff_for_sequence_[which_atom])(d);
  }

  return en;
}

double ShortRangeMFBase::energy(systems::CartesianAtoms &conformation) {

  double en = 0;
  auto ff_it = ff_for_sequence_.cbegin();
  for (auto i = 0; i + property_span_less_one_ < conformation.n_atoms; ++i) {
    double d = conformation[i].distance_to(conformation[i + property_span_less_one_]);
    en += (**ff_it)(d);
    ++ff_it;      // --- advance by 1 force field component
  }

  return en;
}

double ShortRangeMFBase::delta_energy(systems::CartesianAtoms &reference_conformation, const core::index4 which_atom,
    systems::CartesianAtoms &new_conformation) {

  double en = 0;
  if (which_atom >= property_span_less_one_) {
    double d = reference_conformation[which_atom].distance_to(reference_conformation[which_atom - property_span_less_one_]);
    en -= (*ff_for_sequence_[which_atom - property_span_less_one_])(d);
    d = new_conformation[which_atom].distance_to(new_conformation[which_atom - property_span_less_one_]);
    en += (*ff_for_sequence_[which_atom - property_span_less_one_])(d);
  }
  if (new_conformation.n_atoms - which_atom >= property_span_less_one_) {
    double d = reference_conformation[which_atom].distance_to( reference_conformation[which_atom + property_span_less_one_]);
    en -= (*ff_for_sequence_[which_atom])(d);
    d = new_conformation[which_atom].distance_to(new_conformation[which_atom + property_span_less_one_]);
    en += (*ff_for_sequence_[which_atom])(d);
  }

  return en;
}

double ShortRangeMFBase::delta_energy(systems::CartesianAtoms &reference_conformation, const core::index4 atom_from,
    const core::index4 atom_to, systems:: CartesianAtoms &new_conformation) {

    core::index4 from=(int(atom_from-property_span_less_one_) > 0) ? atom_from-property_span_less_one_ : 0;
    double en = 0;
    for (core::index4 which_atom=from;which_atom<=atom_to;++which_atom) {
        if(new_conformation.n_atoms - which_atom >= property_span_less_one_) {
            double d = reference_conformation[which_atom].distance_to(reference_conformation[which_atom + property_span_less_one_]);
            en -= (*ff_for_sequence_[which_atom])(d);
            d = new_conformation[which_atom].distance_to(new_conformation[which_atom + property_span_less_one_]);
            en += (*ff_for_sequence_[which_atom])(d);
       //     std::cerr<<which_atom<<" "<<which_atom + property_span_less_one_<<" "<<en<<"\n";
        }
    }
    return en;
}

void ShortRangeMFBase::prepare_ff(const std::string &ff_file, const std::string &scored_sequence,
                                     const std::string &scored_secondary, const double pseudocounts) {

  std::shared_ptr<MeanFieldDistributions> mf = load_1D_distributions(ff_file, pseudocounts);
  ff_for_sequence_.resize(scored_secondary.size(), zero_energy);
  std::string key("__.__");

  for (auto i = 0; i < system_.n_atoms - property_span_less_one_; ++i){
    if (system_[i + property_span_less_one_].chain_id == system_[i].chain_id) {
      key[0] = char(scored_sequence[i + first_aa_pos_]);
      key[1] = char(scored_sequence[i + second_aa_pos_]);
      key[3] = char(scored_secondary[i + first_aa_pos_]);
      key[4] = char(scored_secondary[i + second_aa_pos_]);
      if (!mf->contains_distribution(key))
        throw std::runtime_error(mf->missing_error_msg(key));
      ff_for_sequence_[i] = mf->at(key);
    } else
      ff_for_sequence_[i] = zero_energy;
}

}

}
}
}

