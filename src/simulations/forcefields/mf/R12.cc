#include <core/index.hh>

#include <simulations/forcefields/mf/R12.hh>

namespace simulations {
namespace forcefields {
namespace mf {

double R12::energy(CartesianAtoms &conformation, const core::index4 which_atom) {

  int other = which_atom - 1.0;
  double en = 0;
  if (other >= 0 && conformation[other].chain_id == conformation[which_atom].chain_id) {
    en += calc_one_bond_from_towards_C(conformation, other);
  }
  if (which_atom + 1 < conformation.n_atoms &&
      conformation[which_atom + 1].chain_id == conformation[which_atom].chain_id) {
    en += calc_one_bond_from_towards_C(conformation, which_atom);
  }
  return en * k0_;
}

double R12::energy(CartesianAtoms &conformation) {
  double en = 0;
  for (auto i = 0; i < conformation.n_atoms - 1; ++i)
    if (conformation[i + 1].chain_id == conformation[i].chain_id)
      en += calc_one_bond_from_towards_C(conformation, i);

  return en * k0_;
}

double R12::delta_energy(CartesianAtoms & reference_conformation, const core::index4 which_atom,
    CartesianAtoms & new_conformation) {
    return energy(new_conformation,which_atom)-energy(reference_conformation,which_atom);
}

double R12::delta_energy(CartesianAtoms & reference_conformation, const core::index4 atom_from,
    const core::index4 atom_to,CartesianAtoms & new_conformation) {

    double en = 0.0;
    core::index4 from=atom_from;
    core::index4 to=atom_to;
    if (atom_from!=0) from-=1;
    if (atom_to==reference_conformation.n_atoms-1) to-=1;
    for (auto i=from;i<=to;++i){
        en += calc_one_bond_from_towards_C(new_conformation,i);
        en -= calc_one_bond_from_towards_C(reference_conformation,i);
    }
    return en;
}

}
}
}

