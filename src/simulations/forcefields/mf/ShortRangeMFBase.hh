#ifndef SIMULATIONS_CARTESIAN_FF_MF_ShortRangeMFBase_HH
#define SIMULATIONS_CARTESIAN_FF_MF_ShortRangeMFBase_HH

#include <cmath>

#include <iostream>
#include <memory>
#include <vector>
#include <map>

#include <core/index.hh>
#include <core/data/sequence/SecondaryStructure.hh>

#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/systems/AtomTypingInterface.hh>
#include <simulations/forcefields/mf/MeanFieldDistributions.hh>
#include <simulations/forcefields/ByAtomEnergy.hh>

namespace simulations {
namespace forcefields {
namespace mf {

/** @brief A base class for secondary structure dependent mean-field short range energy types.
 *
 * This is CABS-like local (along amino acid sequence) energy function that depends both
 * on an amino acid sequence and a secondary structure. This base class however does not take into account the probabilities for
 * each secondary structure type. If you need this feature, inherit from SSWeightedShortRangeMFBase class instead
 */
class ShortRangeMFBase : public ByAtomEnergy {
public:

  /** @brief Constructor loads energy terms (spline function parameters) and repacks them into vectors indexes by residue index.
   *
   * The re-packing of these parameters takes care both of the sequence and secondary structure of the scored protein chain.
   * @param system - the system whose energy will be evaluated
   * @param ff_file - config file used by MeanFieldDistribution1D base class to load the distributions
   * @param scored_sequence - the sequence of the scored chain
   * @param scored_secondary - the secondary structure of the scored chain
   * @param first_aa_pos - relative index of the first residue the scoring function depends on
   * @param secnd_aa_pos - relative index of the first residue the scoring function depends on
   * @param property_span - the number of residues involved in a single structural property measurement, e.g. three in the case of planar angle or 5 for \f$R_{15}\f$ distance
   * @param pseudocounts - pseudocounts value used by MeanFieldDistribution1D base class to convert probabilities into energy
   *
   * @see MeanFieldDistributions
   */
  ShortRangeMFBase(const systems::CartesianAtoms &system, const std::string &ff_file,
                   const std::string &scored_sequence, const std::string &scored_secondary, const core::index1 first_aa_pos,
                   const core::index1 secnd_aa_pos, const core::index1 property_span, const std::string & energy_name,
                   const double pseudocounts = -1);

  /// Bare virtual destructor to obey the rules
  ~ShortRangeMFBase() override = default;


  /** @brief Evaluates this Mean Field energy by calling a respective spline interpolator for a given value
   * @param which_residue - at which position of the chain the energy is evaluated
   * @param value - scored property value, e.g. distance, angle, dihedral, etc
   * @return energy value
   */
  double score_property(core::index2 which_residue, double value) const {return (*ff_for_sequence_[which_residue])(value);}

  double energy(systems::CartesianAtoms & conformation, const core::index4 which_atom) override;

  double energy(systems::CartesianAtoms &conformation) override;

  double delta_energy(systems::CartesianAtoms &reference_conformation, const core::index4 which_atom,
      systems::CartesianAtoms &new_conformation) override;

  double delta_energy(systems::CartesianAtoms &reference_conformation, const core::index4 atom_from, const core::index4 atom_to,
      systems:: CartesianAtoms &new_conformation) override;

protected:
  core::index1 first_aa_pos_;
  core::index1 second_aa_pos_;
  core::index1 property_span_less_one_;
  const systems::CartesianAtoms &system_;

private:
  std::vector<EnergyComponent_SP> ff_for_sequence_; // 1 element per position

  /** \brief Prepares local energy functions for each position in the sequence.
   *
   * To each residue in the simulated chain, exactly three functions: for H, C and E secondary structure type
   * are assigned to score the local property such as a distance or an angle by a derived class. The property
   * scoring function assigned to the index <code>i</code> will depend on amino acid types at positions
   * <code>i+first_aa_pos</code> and <code>i+secnd_aa_pos</code>.
   */
  void prepare_ff(const std::string & ff_file, const std::string &scored_sequence, const std::string &scored_secondary,
      const double pseudocounts);
};

}
}
}

#endif

