#ifndef SIMULATIONS_CARTESIAN_FF_MF_R12_HH
#define SIMULATIONS_CARTESIAN_FF_MF_R12_HH

#include <core/index.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/forcefields/NeighborList.hh>
#include <simulations/systems/CartesianChains.hh>

namespace simulations {
namespace forcefields {
namespace mf {

    using namespace simulations::systems;
/** @brief Harmonic energy based on \f$R+{12}\f$ distance.
 *
 * Energy of a single bond is evaluated according to the formula:
 * \[$
 * E(d) = k_0  (d-d_0)^2
 * \]$
 */
class R12 : public ByAtomEnergy {
public:

  /** @brief Constructor loads energy terms (spline function parameters) and repacks them into vectors indexes by residue index.
   *
   * The re-packing of these parameters takes care both of the sequence and secondary structure of the scored protein chain.
   * @param system - the system whose energy will be evaluated
   * @param d0 - bond length i.e. the distance for minimum energy
   * @param k0 - bond strength i.e bond energy scaling factor
   */
  R12(const systems::CartesianAtoms &system, const double d0 = 3.8, const double k0 = 1) :
      ByAtomEnergy("R12"), d0_(d0), k0_(k0), logs("R12") {}

  /// Bare virtual destructor to obey the rules
  virtual ~R12() {}

  double energy(CartesianAtoms & conformation, const core::index4 which_atom) override;

  double energy(CartesianAtoms &conformation) override;

  double delta_energy(CartesianAtoms &reference_conformation, const core::index4 which_atom,
      CartesianAtoms &new_conformation) override;

  double delta_energy(CartesianAtoms &reference_conformation, const core::index4 atom_from, const core::index4 atom_to,
      CartesianAtoms &new_conformation) override;

private:
  double d0_;
  double k0_;
  utils::Logger logs;

  /** @brief Inline function that extract calculation of the energy of a single pseudo-bond.
   *
   * Returned energy is not multiplied by k0_ !!!
   * This low-level method doesn't check anything, e.g. whether which_atom + 1 exists
   * @param conformation - conformation of a system
   * @param which_atom - first atom
   * @return energy of a pseudo-bond that connects which_atom with (which_atom+1)
   */
  inline double calc_one_bond_from_towards_C(CartesianAtoms &conformation, const core::index4 which_atom) {
    double r = sqrt(conformation[which_atom].closest_distance_square_to(conformation[which_atom + 1]));
    if (r<3.5) return 100;
    return (r - d0_) * (r - d0_);
  }
};

}
}
}

#endif

