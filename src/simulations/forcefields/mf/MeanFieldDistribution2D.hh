/** @file MeanFieldDistribution2D.hh
 *  @brief Defines a set of 2D knowledge-based potentials (defined by a spline interpolation of probability),
 *  e.g. based on a Ramachandran map
 */
#ifndef SIMULATIONS_CARTESIAN_FF_MF_MeanFieldDistribution2D_HH
#define SIMULATIONS_CARTESIAN_FF_MF_MeanFieldDistribution2D_HH

#include <cmath>

#include <iostream>
#include <memory>
#include <vector>
#include <map>

#include <core/index.hh>
#include <core/BioShellEnvironment.hh>
#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/InterpolatePeriodic2D.hh>
#include <core/data/sequence/Sequence.hh>

#include <utils/Logger.hh>

namespace simulations {
namespace forcefields {
namespace mf {

using namespace core::calc::numeric;
using core::data::sequence::Sequence;

/** @brief Shorter name for the type of each interpolated mean-field distribution.
 *
 * Each energy component is an instance of InterpolatePeriodic2D for double values with CatmullRomInterpolator algorithm
 */
typedef InterpolatePeriodic2D<double, CatmullRomInterpolator<double>> EnergyComponent2D;
/// Shared pointer for EnergyComponent type
typedef std::shared_ptr<InterpolatePeriodic2D<double, CatmullRomInterpolator<double>>> EnergyComponent2D_SP;

/** @brief The base class for energy functions that assess the geometry of bonded atom.
 *
 * This class reads and holds 2D functions to be used to evaluate knowledge-based energy
 */
class MeanFieldDistribution2D {
public:

  MeanFieldDistribution2D(const std::string & ff_file, const bool if_convert_from_degrees = true,const double pseudocounts = -1) :
      logger("MeanFieldDistribution2D") {
    load(ff_file, if_convert_from_degrees,pseudocounts);
  }

  /// const-iterator points to the beginning of energy components stored in this energy function object
  std::map<std::string, EnergyComponent2D_SP>::const_iterator components_begin() const { return ff.cbegin(); }

  /// const-iterator points to the end of energy components stored in this energy function object
  std::map<std::string, EnergyComponent2D_SP>::const_iterator components_end() const { return ff.cend(); }

  /// Name of this energy function, as stored in the header of the input file
  const std::string & name() const { return name_; }

  /** @brief Returns a requested energy component.
   *
   * If the component has not been registered in this container, an exception is thrown
   * @param component_key - a string identifier pointing to the energy component
   * @return a requested energy component
   */
  EnergyComponent2D_SP at(const std::string & component_key) {

#ifdef DEBUG
    if(ff.find(component_key)==ff.end())
    std::cerr<<" Can't find FF: "<<component_key<<"\n";
#endif
    return ff.at(component_key);
  }

  /** @brief Returns true if a certain energy component was registered in this container.
   *
   * @param component_key - a string identifier pointing to the energy component
   * @return true if it is stored in this container; false otherwise
   */
  bool contains_distribution(const std::string & component_key) const { return ff.find(component_key) != ff.end(); }

  /** @brief Returns a vector of all keys denoting energy components (distributions) known to this container
   *
   * @return vector of all keys mapping to distributions
   */
  const std::vector<std::string> known_distributions() const;

private:
  std::string name_; ///< Name of this energy function, as stored in the header of the input file
  CatmullRomInterpolator<double> cri;
  std::map<std::string, EnergyComponent2D_SP> ff;
  utils::Logger logger;
  /** @brief Loads a single component from a file.
   *
   * This method can also convert probabilities to energies. The conversion is done always when the given
   * pseudocounts fraction is non-negative.  By default it <strong>is</strong> set to <code>-1</code>, so no conversion is applied
   */
  EnergyComponent2D_SP load_component(const std::string & fname, const bool if_convert_from_degrees,
      const double pseudocounts_fraction = -1.0) {

    std::ifstream in(core::BioShellEnvironment::from_file_or_db(fname));
    EnergyComponent2D_SP en = load_component(in, if_convert_from_degrees, pseudocounts_fraction);
    in.close();

    return en;
  }

  /** @brief Loads a single component from a file.
   *
   * This method can also convert probabilities to energies. The conversion is done always when the given
   * pseudocounts fraction is non-negative.
   */
  EnergyComponent2D_SP load_component(std::istream & in, const bool if_convert_from_degrees,
      const double pseudocounts_fraction);

  /** @brief Loads the components from a file.
   *
   * This method can also convert probabilities to energies. The conversion is done always when the given
   * pseudocounts fraction is non-negative.
   */
  void load(const std::string & fname,const bool if_convert_from_degrees, const double pseudocounts_fraction = -1.0);
};

}
}
}

#endif

