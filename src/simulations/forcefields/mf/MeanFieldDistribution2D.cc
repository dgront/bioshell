#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <memory>
#include <map>

#include <core/index.hh>
#include <core/BioShellEnvironment.hh>
#include <core/calc/structural/angles.hh>
#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/InterpolatePeriodic2D.hh>
#include <core/data/io/DataTable.hh>
#include <core/data/basic/Array2D.hh>
#include <utils/string_utils.hh>
#include <utils/io_utils.hh>
#include <utils/Logger.hh>
#include <utils/options/input_options.hh>
#include <core/algorithms/basic_algorithms.hh>

#include <simulations/forcefields/mf/MeanFieldDistribution2D.hh>

namespace simulations {
namespace forcefields {
namespace mf {

using namespace core::calc::numeric;

EnergyComponent2D_SP MeanFieldDistribution2D::load_component(std::istream & source, const bool if_convert_from_degrees,
    const double pseudocounts_fraction) {

  core::data::io::DataTable dt;
  dt.load(source);

  double xmin = dt[0].get<double>(0);
  double xstep = dt[0].get<double>(1);
  double xn = dt[0].get<double>(2);
  double ymin = dt[1].get<double>(0);
  double ystep = dt[1].get<double>(1);
  double yn = dt[1].get<double>(2);
  if (if_convert_from_degrees) {
    xmin = core::calc::structural::to_radians(xmin);
    xstep = core::calc::structural::to_radians(xstep);
    ymin = core::calc::structural::to_radians(ymin);
    ystep = core::calc::structural::to_radians(ystep);
  }

  std::shared_ptr<core::data::basic::Array2D<double>> z = std::make_shared<core::data::basic::Array2D<double>>(xn, yn);
  double sum = 0;
  for (core::index4 i = 2; i < dt.size(); ++i) {
    const core::index2 ii = dt[i].get<core::index2>(0);
    const core::index2 jj = dt[i].get<core::index2>(1);
    if((ii>=xn)||(jj>=yn))
			logger << utils::LogLevel::WARNING << "x or y values higher than allowed!\n";
    else {
    const double v = dt[i].get<double>(2);
    sum += v;
    z->set(ii, jj, v);
    }
  }
  for (size_t k = 0; k < xn; k++) {
    for (size_t l = 0; l < yn; l++) {
      double v = (*z)(k, l);
      z->set(k, l, -log((v + pseudocounts_fraction) / sum) + log(pseudocounts_fraction / sum));
    }
  }

  EnergyComponent2D_SP en = std::make_shared<EnergyComponent2D>(xmin, xstep, xn, ymin, ystep, yn, z, cri);

  return en;
}
/**
 * @TODO Each FF component should have its own pseudocount fraction, stored as the last entry in the listfile
 */
void MeanFieldDistribution2D::load(const std::string & fname, const bool if_convert_from_degrees,
    const double pseudocounts_fraction) {

  logger << utils::LogLevel::FILE << "Reading a file: " << fname
      << " with the list of 2D FF components; pseudocounts_fraction = " << pseudocounts_fraction << "\n";
  std::ifstream in(core::BioShellEnvironment::from_file_or_db(fname));
  std::string line, ff_file, key;

  while (std::getline(in, line)) {
    if (line.length() < 2) continue;
    if (line[0] == '#') continue;
    std::istringstream ss(line);
    ss >> key >> ff_file;
    EnergyComponent2D_SP en = load_component(ff_file, if_convert_from_degrees, pseudocounts_fraction);
    ff.insert(std::pair<const std::string, EnergyComponent2D_SP>(key, en));
    logger << utils::LogLevel::FINE << "Registering ff component: >" << key << " " << en->n_points_x() << " x "
        << en->n_points_y() << "<\n";
  }
  logger << utils::LogLevel::INFO << ff.size() << " energy components loaded\n";
}

const std::vector<std::string> MeanFieldDistribution2D::known_distributions() const {

  std::vector<std::string> valid_keys;
  std::transform(ff.begin(), ff.end(), std::back_inserter(valid_keys),
      [](const std::map<std::string, EnergyComponent2D_SP>::value_type& val) {return val.first;});
  return valid_keys;
}

}
}
}

