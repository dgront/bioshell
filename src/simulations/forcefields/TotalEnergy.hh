#ifndef SIMULATIONS_FORCEFIELDS_TotalEnergy_HH
#define SIMULATIONS_FORCEFIELDS_TotalEnergy_HH

#include <vector>
#include <memory>
#include <algorithm>
#include <iomanip>

#include <core/data/basic/Array2D.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/evaluators/Evaluator.hh>

namespace simulations {
namespace forcefields {

/** @brief Calculates energy as a weighted combination of components.
 *
 * @tparam E - the type of energy components. If want to use <code>ByResidueEnergy</code> here,
 * take just <code>TotalEnergyByResidue</code> which is the concrete instantiation with some useful extra stuff
 */
class TotalEnergy : public Energy {
public:

  /// Bare constructor
  TotalEnergy() : Energy("TotalEnergy"), logger("TotalEnergy") {
    precision_ = 2;
    min_width_ = 7;
  }

  /// Virtual destructor
  virtual ~TotalEnergy() {}

  /** @brief Adds a new energy component that will be evaluated every time this total energy is evaluated.
   *
   * Energy returned by the new term will be multiplied by the given factor.
   * @param term - energy term
   * @param factor - factor used to scale the energy value
   */
  void add_component(ByAtomEnergy_SP term, const double factor) {

    factors.push_back(factor);
    components.push_back(term);
    sw.push_back(std::max(min_width(), core::index2(term->name().size())));
    logger << utils::LogLevel::INFO << "added  new energy component with weight = " << factor << "\n";
  }

  virtual core::index2 precision() const { return precision_; }

  virtual core::index2 min_width() const { return min_width_; }

  /** @brief Calculates the total energy of a system.
   *
   * The energy is calculated as a weighted sum of energy terms stored in this container
   */
  double energy(systems::CartesianAtoms &conformation) override {
    double en = 0.0;
    for (core::index2 i = 0; i < components.size(); ++i)
      en += components[i]->energy(conformation) * factors[i];
    return en;
  }

  /** @brief Calculates energy of a given component only
   *
   * @return <strong>unweighted</strong> value of a given energy component that participates to this total energy
   */
  virtual double energy_component(systems::CartesianAtoms & conformation,const core::index2 which_component) const { return components[which_component]->energy(conformation); }

  /// This method has been inherited from Evaluator class is just a synonym for <code>calculate()</code>
 // virtual double evaluate() { return energy(); }

  /// Returns the number of energy terms stored in this container
  core::index2 count_components() const { return components.size(); }

  /** @brief Returns a const-pointer to a requested energy term
   * @param id - index of an energy term (from 0)
   * @return a const pointer to an energy term
   */
  const ByAtomEnergy_SP get_component(core::index2 id) const { return components[id]; }

  /** @brief Returns a const-pointer to a requested energy term
   * @param energy_name - name of an energy term; should exactly match a string returned by
   *    a CalculateEnergyBase::name() method
   * @return a const pointer to an energy term
   */
  const ByAtomEnergy_SP get_component(const std::string & energy_name) const {
    for (ByAtomEnergy_SP e: components)
      if (e->name()==energy_name) return e;

    return nullptr;
  }

  /** @brief Returns a pointer to a requested energy term
   * @param id - index of an energy term (from 0)
   * @return a pointer to an energy term
   */
  ByAtomEnergy_SP get_component(core::index2 id) { return components[id]; }

  /** @brief Returns a pointer to a requested energy term
   * @param energy_name - name of an energy term; should exactly match a string returned by
   *    a CalculateEnergyBase::name() method
   * @return a pointer to an energy term
   */
  ByAtomEnergy_SP get_component(const std::string & energy_name) {
    for (ByAtomEnergy_SP e: components)
      if (e->name()==energy_name) return e;

    return nullptr;
  }

  /// The returned string is the header line for energy components table (printed by ostream operator)
  virtual std::string header_string() const {

    std::stringstream ss;
    ss << std::setw(name().size()) << name() << ' ' << std::setw(sw[0]) << components[0]->name();
    for (size_t i = 1; i < components.size(); ++i)
      ss << ' ' << std::setw(sw[i]) << components[i]->name();
    ss << " force_field_tag";

    return ss.str();
  }

  /// Returns the weights used to scale energy components
  const std::vector<double> & get_factors() const { return TotalEnergy::factors; }

  /** @brief Provides the text field width for each energy component.
   * i-th element of the returned vector may be used to print nicely i-th energy component; just say:
   * @code
   *  std::cout <<  std::setw(get_sw()[i]) << calculate_component(i);
   * @endcode
   * @return vector of field widths
   */
  const std::vector<core::index2> & get_sw() const { return TotalEnergy::sw; }


protected:
  std::vector<double> factors; ///< Weights used to scale energy components
    std::vector<ByAtomEnergy_SP> components; ///< Objects that evaluates particular energy types
    std::vector<core::index2> sw; ///< Width of each energy value when converted to string (used for printing energy table)

private:
  utils::Logger logger;
  core::index2 min_width_;
  core::index2 precision_;
};

typedef typename std::shared_ptr<TotalEnergy> TotalEnergy_SP;
}
}

#endif //SIMULATIONS_GENERIC_FF_TotalEnergy_HH
