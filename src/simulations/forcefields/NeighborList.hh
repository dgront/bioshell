/** @file NeighborList.hh
 * @brief Provides Verlet-style list of neighbors and a few related utilities
 */
#ifndef SIMULATIONS_FORCEFIELDS_NeighborList_HH
#define SIMULATIONS_FORCEFIELDS_NeighborList_HH

#include <iterator> // for iterators
#include <vector>

#include <core/index.hh>
#include <core/data/basic/Vec3I.hh>
#include <simulations/systems/CartesianAtoms.hh>
#include <utils/Logger.hh>

namespace simulations {
namespace forcefields {

class NeighborList;

class NeighborListRules {
public:

  virtual ~NeighborListRules() = default;

  /** @brief Returns true if a given atom  should be excluded from a list of neighbors.
   *
   * The base class implementation returns always false.
   *
   * @param i - index of the first atom in a pair
   * @return true if a given atom should be excluded from non-bonded interactions
   */
  virtual bool if_atom_excluded(core::index4 i) { return false; }

  /** @brief Returns true if this pair of atoms should be excluded from a list of neighbors.
   *
   * In general, examples of such exclusion are directly bonded atoms. In this class a pair is excluded
   * when the two conditions are simultaneously satisfied:
   *  - the two atoms are separated in sequence by at most sequence_separation value, and
   *  - when they belong to the same chain
   * Obviously self-contacts are also excluded by this method. The base class implementation
   * of this method returns true if <code>i==j</code>
   *
   * @param i - index of the first atom in a pair
   * @param j  - index of the second atom in a pair
   * @return true if this pair of atoms (i,j) should be excluded from non-bonded interactions
   */
  virtual bool if_pair_excluded(core::index4 i, core::index4 j) { return i==j; }
};

/// Define a shared pointer to NeighborListRules type
typedef typename std::shared_ptr<NeighborListRules> NeighborListRules_SP;


/** @brief Verlet-style list of neighbors.
 *
 */
class NeighborList {
public:
  const std::vector<core::data::basic::Vec3I> &the_system;

  /** @brief Initializes internal data structure and creates list of neighbors for every atom
   *
   * @param system - the system being simulated
   * @param cutoff - interaction cutoff, i.e. the distance at all interactions computed with this NeighborList become 0.0
   * @param border_zone_radius - buffer layer thickness
   */
  NeighborList(const std::vector<core::data::basic::Vec3I> &system, core::index4 n_atoms,
      const double cutoff, const double border_zone_radius);

  /** @brief Copy-constructor makes a new NeighborList for the very same system as the source list
   *
   * @param source - a NeighborList for a given system
   */
  NeighborList(const NeighborList &source) : NeighborList(source.the_system, source.n_atoms(),
      source.cutoff_, source.border_width_) {
    excl_rules_ = source.excl_rules_;
  }

  /// Virtual destructor
  virtual ~NeighborList() {}

  /// Counts atoms covered by this list of neighbors
  core::index4 n_atoms() const { return n_atoms_; }

  /// Returns iterator pointing at the first neighbor of i-th object
  virtual std::vector<core::index4>::const_iterator cbegin(const core::index4 i) const { return neighbors[i].cbegin(); }

  /// Returns iterator pointing behind the last neighbor of i-th object
  virtual std::vector<core::index4>::const_iterator cend(const core::index4 i) const { return neighbors[i].cend(); }

  /// Counts how many neighbors a given atom has
  core::index2 count_neighbors(core::index4 which_atom) const { return neighbors[which_atom].size(); }

  /** @brief Returns the fraction of @code update(core::index4) @endcode calls that were successful
   * @return The fraction of calls that actually triggered NBL update
   */
  double updates_ratio(bool clear_counters = true);

//  __attribute__((noinline))
  /** @brief Updates the list of neighbors after a given atom was moved.
   *
   * If the given atom traveled further than the half of the buffer zone, its neighbors will be recalculated.
   *
   * @param which_atom - which atom was moved
   * @return true if the update was actually triggered; false when the given atom didn't move far enough
   * to cause the update
   */
  virtual bool update(core::index4 which_atom);

  /// Creates list of neighbors for each atom; the previous contents is wiped out
  virtual void update_all();

  /// Returns the interaction cutoff used by this neighbor list
  double cutoff() const { return cutoff_; }

  /** @brief Sets the new NB list cutoff value
   *
   * This call will trigger full update of this neighbor list
   * @param border - the new radius, e.g. 3.0 [A]
   */
  void cutoff(const double cutoff) {
    cutoff_ = cutoff;
    border_width(border_width_);
  }

  /// Returns the buffer zone radius used by this neighbor list
  double border_width() const { return border_width_; }

  /// Returns the square of the contact zone radius including buffer
  virtual double square_cutoff_with_border(core::index4 i, core::index4 j) const { return cutoff_with_border_sq_; }

  /** @brief Sets the new radius of the buffer zone.
   *
   * This call will trigger full update of this neighbor list
   * @param border - the new radius, e.g. 3.0 [A]
   */
  void border_width(const double border);

  /** @brief Defines the minimum separation in atom indexes to consider them neighbors.
   *
   * Sequence separation 2 means that only subsequent atoms are not considered as neighbors. This allows bond exclusion.
   * Calling this method will trigger full update of the list
   *
   * @param delta_ij - sequence separation
   */
  void neighbor_rules(NeighborListRules_SP new_rules) {
    excl_rules_ = new_rules;
    update_all();
  }

  /** @brief Asks NeighborListRules object if a given atom of atoms qualifies for an interaction
   *
   * @param i - index of an atom
   * @return true if a given atom should be excluded from non-bonded interactions
   */
  virtual bool if_atom_excluded(core::index4 i) { return excl_rules_->if_atom_excluded(i); }

  /** @brief Asks NeighborListRules object if a given pair of atoms qualifies for an interaction
   *
   * @param i - index of the first atom in a pair
   * @param j  - index of the second atom in a pair
   * @return true if this pair of atoms (i,j) should be excluded from non-bonded interactions
   */
  virtual bool if_pair_excluded(core::index4 i, core::index4 j) { return excl_rules_->if_pair_excluded(i,j); }

protected:
  double cutoff_;
  double border_width_;
  core::index4 n_called = 0;
  core::index4 n_updated = 0;
  core::index4 n_atoms_;
  double border_width_third_sq_;            ///< square of 1/3 buffer zone thickness, used to decide whether neighbors must be recalculated
  double cutoff_with_border_sq_;            ///< square radius of the interaction sphere extended by the buffer zone
  NeighborListRules_SP excl_rules_;
  std::vector<std::vector<core::index4>> neighbors;
  std::vector<core::data::basic::Vec3I> coordinates_last_update;
  utils::Logger logs;
};

/** @brief Prints all neighbors in a given neighbor list
 * @param out - output stream
 * @param nbl - a reference to a list of neighbors
 */
std::ostream &operator<<(std::ostream &out, const NeighborList &nbl);


class ExcludeSequenceNeighbors : public NeighborListRules {
public:
  ExcludeSequenceNeighbors(int sequence_separation, const NeighborList & enclosing_list) :
      enclosing_list_(enclosing_list) { sequence_separation_ = sequence_separation; }

  ~ExcludeSequenceNeighbors() override = default;

  bool if_pair_excluded(core::index4 i, core::index4 j) override {

    if (std::abs(int(i - j)) < sequence_separation_) {          // --- if they are closer than sequence_separation
      // --- if they belong to the same chain: do not include on a contact list
      if (enclosing_list_.the_system[i].chain_id == enclosing_list_.the_system[j].chain_id) return true;
      if (i == j) return true;    // --- obviously exclude also self-contacts
    }

    return false;
  }

protected:
  /// a pair of atoms i,j is excluded when <code>std::abs(int(i - j)) < sequence_separation_</code>
  int sequence_separation_;
  /** @brief A reference to the NeighborList object that uses these exclusion rules.
   * This NeighborListRules object can access information about the scored system
   * (e.g. atom types, secondary structure etc.); this information is stored in the enclosing list of neighbors
   */
  const NeighborList & enclosing_list_;
};

}
}

#endif
