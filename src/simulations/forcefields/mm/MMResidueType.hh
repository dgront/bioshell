/** @file MMResidueType Implementation of .rtp files parser
 * @brief Force-field files parser utility.
 */

#ifndef BIOSHELL_MMRESIDUETYPE_HH
#define BIOSHELL_MMRESIDUETYPE_HH

#include <vector>
#include <string>
#include <core/index.hh>
#include <map>

#include <simulations/systems/AtomTypingVariants.hh>

namespace simulations {
namespace forcefields {
namespace mm {

struct MMResidueAtom {
  std::string pdb_atom_name; ///< Name of this atom as it appears in the PDB
  std::string internal_atom_name; ///< Name of the type of this atom
  double charge;
  core::index2 index; ///< Number of atom inside residue, starting from 1
};

struct MMResidueBond {
  std::string pdb_atom_name1; ///< Name of this atom as it appears in the PDB
  std::string pdb_atom_name2; ///< Name of this atom as it appears in the PDB
  std::string description; ///< Data as string
};

struct MMResidueRotatableBond {
  std::string pdb_atom_name1; ///< Name of this atom as it appears in the PDB
  std::string pdb_atom_name2; ///< Name of this atom as it appears in the PDB
};


struct MMResidueDihedral {
  std::string pdb_atom_name1; ///< Name of this atom as it appears in the PDB
  std::string pdb_atom_name2; ///< Name of this atom as it appears in the PDB
  std::string pdb_atom_name3; ///< Name of this atom as it appears in the PDB
  std::string pdb_atom_name4; ///< Name of this atom as it appears in the PDB
  std::string description; ///< Data as string
};

struct MMResidueImproper {
  std::string pdb_atom_name1; ///< Name of this atom as it appears in the PDB
  std::string pdb_atom_name2; ///< Name of this atom as it appears in the PDB
  std::string pdb_atom_name3; ///< Name of this atom as it appears in the PDB
  std::string pdb_atom_name4; ///< Name of this atom as it appears in the PDB
  std::string description; ///< Data as string
};

struct MMResidueAngle {
  std::string pdb_atom_name1; ///< Name of this atom as it appears in the PDB
  std::string pdb_atom_name2; ///< Name of this atom as it appears in the PDB
  std::string pdb_atom_name3; ///< Name of this atom as it appears in the PDB
  std::string description; ///< Data as string
};

struct MMResidueType {
  std::string residue_type_name;
  std::vector<MMResidueAtom> atoms;
  std::vector<MMResidueAngle> angles;
  std::vector<MMResidueBond> bonds;
  std::vector<MMResidueRotatableBond> rotatable_bonds;
  std::vector<MMResidueDihedral> dihedrals;
  std::vector<MMResidueImproper> impropers;
};

/** @brief Main function that reads file and converts data inside to map from residue name -> residue data.
 *
 * @param fname - reference to string containing name of file to be read.
 * @param dst - reference to existing map of identical type to returned one. This is to allow to read various groups
 * from different files. Please notice, that if any group is in many files then it's data will be summed up
 * (possibly resulting in redundancy).
 * @return Reference to map given dst argument.
 */
std::map<std::string, MMResidueType> &
read_mm_topology_file(const std::string &fname, std::map<std::string, MMResidueType> &dst);

/** @brief Read a file with PDB atom name translation rules.
 *
 * The rules are used e.g. to translate from old PDB name format to the new one, e.g. "HG11" -> "HG12" in GLU residue
 */
void load_name_translation(const std::string &);

std::string patch_residue_name(const std::string & residue_name,
    const simulations::systems::AtomTypingVariants variant = systems::AtomTypingVariants::STANDARD);

}
}
}

#endif //BIOSHELL_MMRESIDUETYPE_HH
