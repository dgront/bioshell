#include <limits>

#include <core/index.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMAtomTyping.hh>
#include <simulations/forcefields/mm/MMDihedralType.hh>

namespace simulations {
namespace forcefields {
namespace mm {

utils::Logger dihedral_type_logger("MMDihedralType");

MMDihedralType::MMDihedralType(const std::string &cfg_line, const MMAtomTyping_SP atom_typing) {
  std::string name1, name2, name3, name4;
  int func_type, n;

  std::stringstream ss(cfg_line);
  ss >> name1 >> name2 >> name3  >> name4 >> func_type >> theta0_ >> V_ >> n;
  func_type_ = static_cast<core::index1>(func_type);
  n_ = static_cast<core::index1>(n);
  theta0_  *= 3.14159 / 180.0;

  core::index1 max = 255;
  try {
    type_i_ = (name1 == "X") ? max : atom_typing->AtomTypingInterface::atom_type(name1);
  } catch(const std::out_of_range &  e) {
    dihedral_type_logger << "Can't assign atom type index for internal atom type name: "<<name1<<"\n";
    throw(e);
  }
  try {
    type_j_ = (name2=="X") ? max : atom_typing->AtomTypingInterface::atom_type(name2);
  } catch(const std::out_of_range &  e) {
    dihedral_type_logger << "Can't assign atom type index for internal atom type name: "<<name2<<"\n";
    throw(e);
  }
  try {
    type_k_ = (name3=="X") ? max : atom_typing->AtomTypingInterface::atom_type(name3);
  } catch(const std::out_of_range & e) {
    dihedral_type_logger << "Can't assign atom type index for internal atom type name: "<<name3<<"\n";
    throw(e);
  }
  try {
    type_l_ = (name4=="X") ? max : atom_typing->AtomTypingInterface::atom_type(name4);
  } catch(const std::out_of_range &  e) {
    dihedral_type_logger << "Can't assign atom type index for internal atom type name: "<<name4<<"\n";
    throw(e);
  }
}

bool MMDihedralType::is_it_me(const core::index1 atom_type1, const core::index1 atom_type2,
                              const core::index1 atom_type3, const core::index1 atom_type4) const {

  core::index1 max = 255;
  // --- Here we check if the two middle atom types match
  if ((atom_type2 == type_j_) && (atom_type3 == type_k_)) {
    if ((atom_type1 == type_i_) && (atom_type4 == type_l_)) return true; // first and fourth also match
    // Here we check for type1 - type2 - type3 - X
    if ((atom_type1 == type_i_) && (type_l_ == max)) return true;
    // Here we check for X - type2 - type3 - type4
    if ((type_i_ == max) && (atom_type4 == type_l_)) return true;
    // Here we check for X - type2 - type3 - X
    if ((type_i_ == max) && (type_l_ == max)) return true;
  }
  // --- Here we check if the two middle atom types match in the reversed order
  if ((atom_type2 == type_k_) && (atom_type3 == type_j_)) {
    if ((atom_type1 == type_l_) && (atom_type4 == type_i_)) return true; // first and fourth also match
    if ((atom_type1 == type_l_) && (type_l_ == max)) return true;
    if ((type_i_ == max) && (atom_type4 == type_i_)) return true;
    // Here we check for X - type2 - type3 - X
    if ((type_i_ == max) && (type_l_== max)) return true;
  }

  return false;
}

MMDihedralType::MMDihedralType(const MMDihedralType & type) {

  type_i_ = type.type_i_;
  type_j_ = type.type_j_;
  type_k_ = type.type_k_;
  type_l_ = type.type_l_;
  theta0_ = type.theta0_;
  V_ = type.V_;
  n_ = type.n_;
  func_type_ = type.func_type_;
}

/// Returns a string representing this object
std::string MMDihedralType::to_string() const {
  return utils::string_format("%3d %3d %3d %3d     %1d    %7.2f   %10.5f   %1d",
    type_i_, type_j_, type_k_, type_l_, func_type_, theta0_ / 3.14159 * 180.0, V_, n_);
}

}
}
}
