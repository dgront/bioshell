#ifndef SIMULATIONS_CARTESIAN_FF_MM_MMDihedralType_HH
#define SIMULATIONS_CARTESIAN_FF_MM_MMDihedralType_HH


#include <core/index.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMAtomTyping.hh>

namespace simulations {
namespace forcefields {
namespace mm {

/** @brief Provides parameters for one dihedral angle type in molecular mechanic force field.
 *
 * This class provides parameters necessary to calculate energy of a single dihedral angle, according to the following formula:
 * \f[
 * E(\theta) = {V_n \over 2} [1 + {\rm cos}(n\theta - \theta_0)]
 * \f]
 * The parameters are returned by the respective methods:
 *    - <code>n()<\code> returns \f$V_n\f$
 *    - <code>Vn()<\code> returns \f$n\f$
 *    - <code>theta0()<\code> returns \f$\theta_0\f$
 */
class MMDihedralType {
public:

  /** @brief Creates a definition object from a string (Gromos FF definition format)
   *
   * Example dihedral angle definitions:
   * <code><pre>
   * CA  CA  CA  OH       4      180.00     4.60240     2    ; comment after a semicolon
   * H5  O   C   OH       4      180.00     4.60240     2    ; new99
   * H5  O   C   OS       4      180.00     4.60240     2    ; new99
   * CM  CT  CM  HA       4      180.00     4.60240     2    ; new99
   * CA  CA  CA  Br       4      180.00     4.60240     2    ; new99
   * CM  H4  C   O        4      180.00     4.60240     2    ; new99
   * </pre></code>
   */
  MMDihedralType(const std::string &cfg_line, const MMAtomTyping_SP atom_typing);

  /// Copy constructor
  MMDihedralType(const MMDihedralType & type);

  /** @brief returns true if this object defines a dihedral angle between three particular atoms (by atom type).
   *
   * @param atom_type1 - the force field specific type index of the first of the four atoms that form a planar
   * @param atom_type2 - the force field specific type index of the second of the four atoms that form a planar
   * @param atom_type3 - the force field specific type index of the third of the four atoms that form a planar
   * @param atom_type4 - the force field specific type index of the fourth of the four atoms that form a planar
   */
  bool is_it_me(const core::index1 atom_type1, const core::index1 atom_type2,
                const core::index1 atom_type3, const core::index1 atom_type4) const;


  /// Returns value of dihedral angle (in radians!)
  double theta0() const { return theta0_; }

  /// Returns dihedral angle spring constant (energy scaling factor)
  double Vn() const { return V_; }

  /// Returns dihedral angle multiplicity constant
  core::index1 n() const { return n_; }

  /// Returns the atom_type of the first atom in this dihedral angle
  core::index1 type_i() const { return type_i_; }

  /// Returns the atom_type of the second atom in this dihedral angle
  core::index1 type_j() const { return type_j_; }

  /// Returns the atom_type of the third atom in this dihedral angle
  core::index1 type_k() const { return type_k_; }

  /// Returns the atom_type of the fourth atom in this dihedral angle
  core::index1 type_l() const { return type_l_; }

  /// Returns a string representing this object
  std::string to_string() const;

private:
  double theta0_;
  double V_;
  core::index1 n_;
  core::index1 func_type_;
  core::index1 type_i_;
  core::index1 type_j_;
  core::index1 type_k_;
  core::index1 type_l_;
};


}
}
}
#endif
