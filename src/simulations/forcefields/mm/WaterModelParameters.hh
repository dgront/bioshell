#ifndef SIMULATIONS_FORCEFIELDS_MM_WaterModel_HH
#define SIMULATIONS_FORCEFIELDS_MM_WaterModel_HH

#include <core/index.hh>
#include <core/data/structural/Residue.hh>
#include <utils/Logger.hh>

namespace simulations {
namespace forcefields {

/** @brief Stores parametrisation for water models.
 */
class WaterModelParameters {
public:

  std::string name; ///< name of this water model
  double qO;        ///< charge on the oxygen atom
  double qH;        ///< charge on the hydrogen atom
  double rOH;       ///< O-H bond length
  double aHOH;      ///< H-O-H planar angle
  double sO;        ///< LJ sigma parameter for the oxygen atom
  double eO;        ///< LJ epsilon parameter for the oxygen atom
  core::index1 n_atoms; ///< number of atoms per each water moelcule (3, 4, 5 or 6, depensing on a model)

  /// Empty constructor
  WaterModelParameters() {}

  /// Copy constructor maked a deep copy of a given model <code>m</code>
  WaterModelParameters(const WaterModelParameters & m);

  /// Assignment operator
  WaterModelParameters& operator=(const WaterModelParameters & other);

  /** @brief Creates a Residue object that holds a single water molecule.
   *
   * The returned molecule contains also dummy atoms such as l1, l2 or m, as defined in a water model
   * @return a Residue with a single water molecule
   */
  core::data::structural::Residue_SP create_residue() const;

  /** @brief Returns a water model by its name
   * @param model_name - "TIP3P", "SPC" etc.
   * @return a water model parametrisations
   */
  static const WaterModelParameters & get_model(const std::string & model_name) { return known_models.at(model_name); }

  /** @brief Loads a file with water model definitions
   * @param fname file name; by default a file from BioShell distribution is loaded
   */
  static void load_models(const std::string & fname = "forcefield/mm/water_models.txt");

private:
  static std::map<std::string, WaterModelParameters> known_models;
};

std::ostream& operator<<(std::ostream& out, const WaterModelParameters& rhs);

}
}

#endif
