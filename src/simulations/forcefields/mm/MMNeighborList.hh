#ifndef SIMULATIONS_FORCEFIELDS_MM_NeighborList_HH
#define SIMULATIONS_FORCEFIELDS_MM_NeighborList_HH

#include <iterator> // for iterators
#include <vector>

#include <core/index.hh>
#include <core/data/basic/Vec3I.hh>
#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/forcefields/NeighborList.hh>
#include <simulations/forcefields/mm/MMBondEnergy.hh>
#include <simulations/forcefields/mm/ExcludedPairsList.hh>

#include <utils/Logger.hh>

namespace simulations {
namespace forcefields {
namespace mm{

/** @brief Verlet-style list of neighbors for MM field.
 */

class MMNeighborList :public NeighborList {
public:

  /** @brief Initializes internal data structure and creates list of neighbors for every atom
   *
   * @param system - the system being simulated
   * @param cutoff - interaction cutoff, i.e. the distance at all interactions computed with this NeighborList become 0.0
   * @param border_zone_radius - buffer layer thickness
   * @param sequence_separation - nearest neighbors along the sequence of the same chain will be excluded when
   *    sequence_separation is higher than 1.
   */
  MMNeighborList(const systems::CartesianAtoms &system, const double cutoff, const double border_zone_radius,
      core::index2 sequence_separation, const MMBondEnergy &bonded_energy) : NeighborList(system.coordinates(), cutoff,
      border_zone_radius, sequence_separation),
      excluded_pairs(ExcludedPairsList::create_excluded_pair_list(system, bonded_energy)) {}

  /// Virtual destructor
  virtual ~MMNeighborList() {}

  /// Check if a given pair of atoms is excluded from NB interactions
  virtual bool if_pair_excluded(core::index4 i, core::index4 j) {

      return excluded_pairs.are_excluded(i,j) && NeighborList::if_pair_excluded(i,j);
  }

  ExcludedPairsList excluded_pairs;
};

}
}
}

#endif
