#include <core/index.hh>

#include <simulations/forcefields/mm/Water3PointEnergy.hh>

namespace simulations {
namespace forcefields {
namespace mm {

Water3PointEnergy::Water3PointEnergy(const WaterModelParameters &params) : ByAtomEnergy("Water3PointEnergy") {

  q_[0] = params.qO;
  q_[1] = q_[2] = params.qH;
  sO_ = params.sO;
  eO_four_ = params.eO * 4.0;
  sigma2_ = sO_ * sO_;
}


double Water3PointEnergy::calculate_two_hoh(systems::CartesianAtoms & conformation,
                                            const core::index4 oxygen_i, const core::index4 oxygen_j) {

  double en = 0;
  double eq = 0;
  const core::data::basic::Vec3I & o = conformation[oxygen_i];
  double r2 = o.closest_distance_square_to(conformation[oxygen_j], CUTOFF_OFF2);
  if (r2 >= CUTOFF_OFF2) return 0;

  // ---------- O-O LJ energy
  double r2_s = sigma2_ / r2;
  double r6 = r2_s * r2_s * r2_s;
  double r12 = r6 * r6;
  en += eO_four_ * (r12 - r6) * sw(r2);
  if (r2 < 1.0) return en;

    // ---------- Coulomb energy - now it's disappears when O-O distance is larger than the cutoff
  for (core::index4 i = 0; i < 3; ++i) {
    const core::data::basic::Vec3I & o= conformation[oxygen_i + i];
    for (core::index4 j = 0; j < 3; ++j) {
      r2 = o.closest_distance_square_to(conformation[j + oxygen_j]);
      eq += q_[i] * q_[j] / sqrt(r2);
    }
  }

  return en + FACTOR_KJ * eq * kJ_mol_to_K;
}


double Water3PointEnergy::energy(systems::CartesianAtoms & conformation) {

  double en = 0;
  for (core::index4 i = 3; i < conformation.n_atoms; i += 3) {
    for (core::index4 j = 0; j < i; j += 3)
      en += calculate_two_hoh(conformation, i, j);
  }
  return en;
}

double Water3PointEnergy::energy(systems::CartesianAtoms & conformation, const core::index4 oxygen_atom_index) {

  double en = 0;
  for (core::index4 j = 0; j < oxygen_atom_index; j += 3)
    en += calculate_two_hoh(conformation, j, oxygen_atom_index);
  for (core::index4 j = oxygen_atom_index + 3; j < conformation.n_atoms; j += 3)
    en += calculate_two_hoh(conformation, j, oxygen_atom_index);

  return en;
}

double Water3PointEnergy::delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 oxygen_atom_index,
                    systems::CartesianAtoms & new_conformation) {
  double en = 0;

  for (core::index4 j = 0; j < oxygen_atom_index; j += 3) {
    en += calculate_two_hoh(new_conformation, j, oxygen_atom_index) - calculate_two_hoh(reference_conformation, j, oxygen_atom_index);
  }

  for (core::index4 j = oxygen_atom_index + 3; j < new_conformation.n_atoms; j += 3) {
    en += calculate_two_hoh(new_conformation, j, oxygen_atom_index) -
          calculate_two_hoh(reference_conformation, j, oxygen_atom_index);
  }

  return en;
}

}
}
}
