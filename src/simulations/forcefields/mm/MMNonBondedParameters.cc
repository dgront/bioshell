#include <simulations/forcefields/mm/MMNonBondedParameters.hh>
#include <utils/io_utils.hh>

namespace simulations {
namespace forcefields {
namespace mm {


MMNonBondedParameters::MMNonBondedParameters(const core::index1 id, const std::string &cfg_line) {
  std::stringstream ss(cfg_line);
  char ptype;
  int z;
  // --- the order in the file: name      at.num  mass     charge ptype  sigma      epsilon
  ss >>  type_ >> z >> mass_>> charge_ >> ptype >> sigma_ >> epsilon_;
  element_ = z;     // --- to avoid cast to index1 which is char
  sigma_ *= 10.0;   // --- from [nm] to [A]
  id_ = id;
}

MMNonBondedParameters::MMNonBondedParameters(const MMNonBondedParameters &mm) {
  id_ = mm.id();
  type_ = mm.type_;
  charge_ = mm.charge_;
  mass_ = mm.mass_;
  epsilon_ = mm.epsilon_;
  sigma_ = mm.sigma_;
}

std::map<std::string, MMNonBondedParameters> &
read_mm_atom_types(const std::string &fname, std::map<std::string, MMNonBondedParameters> &dst) {

  std::string file_content = utils::load_text_file(fname);
  std::vector<std::string> lines;
  utils::split_into_strings(file_content, lines,'\n');
  core::index1 i_atom = 0;
  int i_line = 0;
  while(lines[i_line].rfind("[ atomtypes ]", 0) == std::string::npos) ++i_line;    // --- rewind until [ atomtypes ] section
  ++i_line;                                                                         // --- pass the header
  while ((i_line < lines.size()) && (lines[i_line].rfind("[ ", 0) == std::string::npos)) {  // --- until the next section
    if(lines[i_line][0]!= ';') {
      MMNonBondedParameters par(i_atom, lines[i_line]);
      ++i_atom;
      dst.emplace(par.internal_name(), par);
    }
    ++i_line;
  };

  return dst;
}

}
}
}
