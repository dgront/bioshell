#ifndef SIMULATIONS_FF_MM_Water3PointEnergy_HH
#define SIMULATIONS_FF_MM_Water3PointEnergy_HH

#include <core/index.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/forcefields/mm/WaterModelParameters.hh>
#include <simulations/systems/CartesianAtoms.hh>

#include <utils/Logger.hh>

namespace simulations {
namespace forcefields {
namespace mm {

/** @brief Provides parameters for one bond type in molecular mechanic force field.

 */
class Water3PointEnergy : public ByAtomEnergy {
public:

  const double CUTOFF_ON = 7.0;
  const double CUTOFF_OFF = 9.0;
  const double CUTOFF_ON2 = CUTOFF_ON * CUTOFF_ON;
  const double CUTOFF_OFF2 = CUTOFF_OFF * CUTOFF_OFF;

  /** @brief Creates a 3-point water energy type, such as TIP3P potential
   *
   */
  Water3PointEnergy(const WaterModelParameters & params);

  const std::string & name() const { return name_; }

  double energy(systems::CartesianAtoms & conformation) final;

  /** @brief Calculate energy between a single water molecule and all other waters
   * @param conformation - system consisting of water molecules
   * @param oxygen_atom_index - index of an oxygen atom that identifies a water molecule
   * @return energy value
   */
  double energy(systems::CartesianAtoms & conformation, const core::index4 oxygen_atom_index) final;

  /** @brief Calculate the energy difference due to a single water molecule.
   * The returned energy difference will be evaluated based on the assumption that only the water molecule pointed by
   * the @code oxygen_atom_index @endcode differs between the two conformations
   * @param reference_conformation - conformation to assess
   * @param oxygen_atom_index - index of an oxygen atom that identifies a water molecule
   * @param new_conformation - conformation to assess
   * @return energy difference evaluated as @code E(new) - E(reference) @endcode
   */
  double delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 oxygen_atom_index,
                              systems::CartesianAtoms & new_conformation) final;

  /** @brief Calculate the energy difference due to a contiguous range of atoms.
 * @param reference_conformation - initial conformation to assess
 * @param atom_from - index of the first atom of the moved chunk
 * @param atom_to  - index of the last atom of the moved chunk
 * @param new_conformation - the new conformation after a MC move to assess
 * @return energy difference evaluated as @code E(new) - E(reference) @endcode
 */
 // TODO implement delta_energy() method here
  double delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 atom_from,
      const core::index4 atom_to, systems::CartesianAtoms & new_conformation) final {

    throw std::logic_error{"Function not yet implemented."};
  }

private:
  double q_[3];
  double sO_;
  double sigma2_;
  double eO_four_;
  const double kJ_mol_to_K = 1000.0 / (6.022E23 * 1.381E-23);
  const double FACTOR_KJ = 1389.0; // --- energy in [kJ / mol]
  std::string name_;

  double calculate_two_hoh(systems::CartesianAtoms & conformation, const core::index4 oxygen_i, const core::index4 oxygen_j);

  inline double sw(const double d2) const {

    if (d2 <= CUTOFF_ON2) {
      return 1.0;
    } else if (d2 > CUTOFF_ON2 && d2 <= CUTOFF_OFF2) {
      double d = (CUTOFF_OFF2 - CUTOFF_ON2);
      double d3 = d * d * d;
      double w = (CUTOFF_OFF2 - d2) * (CUTOFF_OFF2 - d2);
      w *= (CUTOFF_OFF2 + 2 * d2 - 3 * CUTOFF_ON2);
      w /= d3;
      return w;
    } else {
      return 0.0;
    }
  }
};


}
}
}
#endif
