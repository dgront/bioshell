#ifndef SIMULATIONS_FORCEFIELDS_MM_MMBondEnergy_HH
#define SIMULATIONS_FORCEFIELDS_MM_MMBondEnergy_HH

#include <vector>
#include <tuple>

#include <core/index.hh>
#include <core/chemical/Molecule.hh>
#include <core/chemical/molecule_utils.hh> // for core::chemical::bond_length()

#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/forcefields/mm/MMBondType.hh>
#include <simulations/forcefields/mm/MMBondedParameters.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>
#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/systems/CartesianChains.hh>

namespace simulations {
namespace forcefields {
namespace mm {

using namespace systems;

/** @brief Data type used by MMBondEnergy to hold data about each bond to be evaluated.
 * This data type is also used to provide the list of bonds per se, e.g. to create a molecule graph,
 * utilized by all other bonded energy terms constructors
 */
struct MMBondTerm {
  core::index4 i; ///< The first of the two bonded atoms
  core::index4 j; ///< The second of the two bonded atoms
  double k; /// < Force constant
  double d0; /// < Equilibrium bond length
  core::index1 calc_flag = 0; ///< flag to be used in calculation by chunk to not caclulate this Bond two times

  /// Assigning c'tor
  inline MMBondTerm(core::index4 ai, core::index4 aj, double kb, double d) {
    i = ai;
    j = aj;
    k = kb;
    d0 = d;
    calc_flag = 0;
  }
};

/** @brief Calculates energy of covalent bonds according to a MM force field
 * @see MMBondType - for bond energy definition
 */

class MMBondEnergy : public ByAtomEnergy {
private:

  /// Sorts bond definitions so they are ordered according to the order of residues in a chain
  struct CompareMMBondTerms {

    explicit CompareMMBondTerms(const MMBondEnergy & en) : e(en) {}

    bool operator()(const MMBondTerm & left,const MMBondTerm & right) const {
      const CartesianChains & c = static_cast<const systems::CartesianChains&>(e.the_system);
      return (c.residue_for_atom(left.i) < c.residue_for_atom(right.i));
    }

    const MMBondEnergy & e;
  };

public:

  /** @brief Creates object that evaluates energy of all bonds in a biomolecular chain
   *
   * @param system - bonds energy of this system will be evaluated
   * @param bond_manager - provides parameters for bonds energy: bond lengths and their force constants
   */
  MMBondEnergy(const CartesianAtoms & system, const MMBondedParameters &bond_manager);

  /// Virtual destructor
  virtual ~MMBondEnergy() {}

  /** @brief Calculates energy for all bonds connected to atoms of a given residue
   *
   * This method evaluates all bonds attached to any atom of a given residue
   * Every bond energy is evaluated exactly once.
   * @param which_residue - index of the residue to be evaluated
   * @return harmonic energy computed for bonds connected to any atom of <code>which_residue</code> residue
   */
  double energy(CartesianAtoms& conformation, core::index4 which_atom) override;


  /** @brief Calculates total energy for all bonds in this molecule.
   */
  double energy(CartesianAtoms & conformation) override;

  double delta_energy(CartesianAtoms &reference_conformation, core::index4 which_atom,
      CartesianAtoms &new_conformation) override;

  double delta_energy(CartesianAtoms &reference_conformation, core::index4 atom_from, core::index4 atom_to,
      CartesianAtoms &new_conformation) override;

  double delta_energy(CartesianAtoms &reference_conformation, CartesianAtoms & new_conformation,
      const bool log_differences = false);

  /** @brief Evaluate energy of a single bond
   *
   * @param bond - which bond
   * @return energy of that bond calculated for the current conformation
   */
  double calculate(const MMBondTerm & bond);

  /// Provides read-only reference to a matrix of all bonds in the given molecule
  const std::vector<MMBondTerm> &get_bonds() const { return all_bonds_; };

protected:
  const CartesianAtoms & the_system; ///< the system whose energy will be evaluated
  std::vector<MMBondTerm> all_bonds_;
  std::vector<std::vector<core::index4>> bonds_for_atom_;

  /** @brief Creates object that evaluates energy of all bonds in a biomolecular chain.
   *
   * This constructor assumes the derived class will create the list of all bonds of a system
   * @param system - bonds energy of this system will be evaluated
   */
  explicit MMBondEnergy(const CartesianAtoms & system);

private:
  utils::Logger logs;

  void create_bonds(const MMBondedParameters &bond_manager) ;

  std::string nice_atom_info(core::index4 atom_index) const ;

  void print_bond_msg(const MMBondTerm & b) ;
  void print_missing_bond_msg(const core::index4 i, const core::index4 j) ;
};

}
}
}


#endif

