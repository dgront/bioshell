#ifndef SIMULATIONS_FORCEFIELDS_MM_MMPlanarEnergy_HH
#define SIMULATIONS_FORCEFIELDS_MM_MMPlanarEnergy_HH

#include <vector>
#include <tuple>

#include <core/index.hh>
#include <core/calc/structural/angles.hh>
#include <core/chemical/Monomer.hh>
#include <core/chemical/molecule_utils.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMPlanarType.hh>
#include <simulations/forcefields/mm/MMBondedParameters.hh>
#include <simulations/forcefields/mm/MMBondEnergy.hh>
#include <simulations/forcefields/mm/graph_from_residue_chain.hh>

namespace simulations {
namespace forcefields {
namespace mm {

using namespace core::data::basic;

/** @brief Evaluates energy of planar angles.
 *
 * @see MMPlanarType - for planar angle energy definition
 */

class MMPlanarEnergy : public ByAtomEnergy {
private:

  /// Internal holder for a single planar angle term
  struct MMPlanarTerm {
    core::index4 i;
    core::index4 j;
    core::index4 k;
    double Ka;
    double alpha0;
    core::index1 calc_flag=0;


      /// Assigning c'tor
    inline MMPlanarTerm(core::index4 ai,core::index4 aj, core::index4 ak, double ka,double a) {
      i = ai;
      j = aj;
      k = ak;
      Ka = ka;
      alpha0 = a;
    }
  };

  /// Sorts planar angle definitions so they are ordered according to the order of residues in a chain
  struct CompareMMPlanarTerms {

    CompareMMPlanarTerms(const MMPlanarEnergy & en) : e(en) {}

    bool operator()(const MMPlanarTerm & left,const MMPlanarTerm & right) const {
      const systems::CartesianChains & c = static_cast<const systems::CartesianChains&>(e.the_system);

      return (c.residue_for_atom(left.i) < c.residue_for_atom(right.i) || c.residue_for_atom(left.k) < c.residue_for_atom(right.k));
    }

    const MMPlanarEnergy & e; ///< Comparer needs a reference to the energy object to access ResidueChain and residue indexes from there
  };

public:

  /** @brief Creates energy object of all planar angles in the system.
   *
   * @param system - chain whose energy will be evaluated
   * @param bonded_manager - provides parameters for planar angle energy terms
   * @param bond_energy - bond energy object provides a list of all bonds; the bonds are necessary to detect planar angles (a planar angle is defined by two
   * bonds attached to the same atom). <code>core::chemical::find_planar_angles()</code> is used to detect planars
   */
  MMPlanarEnergy(const CartesianAtoms &system, const MMBondedParameters &bonded_manager,
      const MMBondEnergy & bond_energy) : ByAtomEnergy("MMPlanarEnergy"), the_system(system), logs("MMPlanarEnergy") {

    // ---------- check if a given system is actually of CartesianChains type
    try {
      const systems::CartesianChains & c = static_cast<const systems::CartesianChains&>(the_system);
    } catch (const std::exception& e) {
      utils::exit_with_error("MMPlanarEnergy.hh", 86, "Can't cast a given system to CartesianChains type!");
    }

    angles_for_atom_.resize(system.CartesianAtoms::n_atoms);
    create_planar_angles(bond_energy,bonded_manager);
  }

  /// Virtual destructor is empty
  virtual ~MMPlanarEnergy() {}

  double energy(CartesianAtoms &conformation) override;

  double delta_energy(CartesianAtoms &referance_conformation, core::index4 which_atom,
      CartesianAtoms &new_conformation) override;

  /// @todo Find smallest and largest  MMPlanarTerm index and set flag only for these
  double delta_energy(CartesianAtoms &referance_conformation, core::index4 atom_from, core::index4 atom_to,
      CartesianAtoms &new_conformation) override;

  double energy(CartesianAtoms &conformation, core::index4 which_atom) override;

   const std::vector<MMPlanarTerm> &get_planar_angles() const {return planar_terms_;}

private:

  const CartesianAtoms &the_system; ///< the system whose energy will be evaluated
  utils::Logger logs;
  std::vector<MMPlanarTerm> planar_terms_;
  std::vector<std::vector<core::index4>> angles_for_atom_;

  void create_planar_angles(const simulations::forcefields::mm::MMBondEnergy &bond_energy,
                         const simulations::forcefields::mm::MMBondedParameters &bonded_params) ;

  std::string nice_atom_info(core::index4 atom_index) const ;

  void print_planar_msg(const MMPlanarTerm & planar_term) ;
};

}
}
}
#endif // SIMULATIONS_FORCEFIELDS_MM_MMPlanarEnergy_HH

