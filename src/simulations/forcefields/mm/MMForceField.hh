#ifndef SIMULATIONS_CARTESIAN_FF_MM_MMForceField_HH
#define SIMULATIONS_CARTESIAN_FF_MM_MMForceField_HH

#include <core/index.hh>

#include <simulations/forcefields/mm/MMAtomTyping.hh>
#include <simulations/forcefields/mm/MMBondedParameters.hh>

namespace simulations {
namespace forcefields {
namespace mm {

struct MMForceFieldFiles {

  /// FIle that contains all atom types with their LJ parameters
  const std::string non_bonded_itp;
  const std::string bonded_itp;
  const std::string residues_rtp;
  /// File that renames atoms from PDB naming in the FF to the "real" PDB atom naming
  const std::string translation_rules_rtp;
};


/** @brief Defines one atom type in molecular mechanic force field.
 * \todo_code multiply sigma values in amber03_atoms.par and remove sigma_*=10.0; from constructors
 */
class MMForceField {
public:

  explicit MMForceField(const std::string &force_field_name);

  MMForceField(const std::string &nonbonded_itp_file, const std::string &bonded_itp_file,
      const std::string &residues_rtp_file, const std::string &translation_rules_rtp = "");

  /** @brief Set translation rules for atom names.
   *
   * Note: all translation rules must be set before any force field is created!
   * @param translation_file - file with translation rules - GROMACS format, e.g.:
   * <code><pre>[ ILE ]
CD CD1
HG11 HG12
HG12 HG13
<pre><code>
   */
  static void add_translation_rules(const std::string &translation_file);

  /** @brief Topology information for all residue types defined in this force field.
   * This data typically from a @code .rtp @endcode param file such as @code amber03_aminoacids.rtp @endcode
   * @return a list indexed by strings - patched names of residues, that provides  MMResidueType for every residue
   */
  const std::map<std::string, MMResidueType> & resiude_topology() const { return resiude_topology_; }

  /** @brief non-bonded parameters define all atom types of this force field with their LJ parameters
   * @return a map of all atom types of this force field
   */
  const std::map<std::string, MMNonBondedParameters> & nonbonded_parameters() const { return nonbonded_parameters_; }

  /** @brief annotates residue topology with additional information.
   * Most notably, this method can be used to upload definition of rotable bonds
   * @param fname - input file name  (Gromacs format)
   */
  void ammend_nonbonded_parameters(const std::string &fname) { read_mm_topology_file(fname, resiude_topology_); }

  /** @brief Parameters for all bonds, planars and dihedrals defined in this force field
   * @return object that holds parameters for bonded energy terms
   */
  const MMBondedParameters & bonded_parameters() const { return bonded_parameters_; }

  /** @brief Atom typing for this force field
   * @return a shared pointer to a MMAtomTyping object
   */
  MMAtomTyping_SP mm_atom_typing();

  /** @brief Returns a vector of residue type codes registered in this force field.
   * The residues are typically loaded from a @code .rtp @endcode file
   * @return list of residue types parametrised by this force field
   */
  std::vector<std::string> registered_residue_types();

  /** @brief Returns a vector of known force field names, e.g. AMBER03.
   *
   * Names are all capital letters with no spaces
   * @return list of valid force field names, that may be used to create a MMForceField object
   */
  static const std::vector<std::string> & known_force_fields();

  /** @brief A structure that hold fine names that define a particular molecular force field
   * @param ff_name - name of a force field - must be one of the strings returned by known_force_fields()
   * @return a force field definition
   */
  static const MMForceFieldFiles & force_field_files(const std::string& ff_name);

private:
  static std::vector<std::string> known_force_fields_;
  static std::map<std::string, MMForceFieldFiles> force_field_files_;

  std::map<std::string, MMResidueType> resiude_topology_;
  std::map<std::string, MMNonBondedParameters> nonbonded_parameters_;
  MMBondedParameters bonded_parameters_;
  std::vector<std::string> atom_type_names_;

  MMAtomTyping_SP mm_atom_typing_;
};


}
}
}
#endif
