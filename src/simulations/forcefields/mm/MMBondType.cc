#include <core/index.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMAtomTyping.hh>
#include <simulations/forcefields/mm/MMBondType.hh>

namespace simulations {
namespace forcefields {
namespace mm {

utils::Logger bond_type_logger("MMBondType");

MMBondType::MMBondType(const std::string &cfg_line, const MMAtomTyping_SP atom_typing) {

  std::string name1, name2;
  std::stringstream ss(cfg_line);
  core::index1 func_type; // unused so far
  ss >> name1 >> name2 >> func_type >> value_ >> constant_;
  value_ *= 10.0; // To get Angstroms from nanometers
  constant_ *= 0.001; // To make kcal from cal

  try {
    type_i_ = atom_typing->AtomTypingInterface::atom_type(name1);
  } catch(const std::out_of_range & e) {
    bond_type_logger << utils::LogLevel::SEVERE << "Can't assign atom type index for internal atom type name: "<<name1<<"\n";
    throw(e);
  }
  try {
    type_j_ = atom_typing->AtomTypingInterface::atom_type(name2);
  } catch(const std::out_of_range & e) {
    bond_type_logger << utils::LogLevel::SEVERE << "Can't assign atom type index for internal atom type name: "<<name2<<"\n";
    throw(e);
  }
}

MMBondType::MMBondType(const MMBondType & type) {

  type_i_ = type.type_i_;
  type_j_ = type.type_j_;
  value_ = type.value_;
  constant_ = type.constant_;
}

std::string MMBondType::to_string() const { return utils::string_format("%3d %3d    1    %7.5f   %8,1f", type_i_, type_j_, value_,constant_); }

std::ostream & operator<<(std::ostream & out, const MMBondType & mbt) {

  out << utils::string_format("%3d %3d %10.5f %10.1f", mbt.type_i(), mbt.type_j(), mbt.length(), mbt.constant());

  return out;
}

}
}
}
