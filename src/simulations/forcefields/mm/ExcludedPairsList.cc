#include <iostream>
#include <algorithm>

#include <core/index.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <core/chemical/molecule_utils.hh>
#include <simulations/forcefields/mm/MMBondEnergy.hh>
#include <simulations/forcefields/mm/ExcludedPairsList.hh>

#include <simulations/forcefields/mm/graph_from_residue_chain.hh>
#include <iomanip>

namespace simulations {
namespace forcefields {
namespace mm {

ExcludedPairsList::ExcludedPairsList(const core::index4 n_atoms, const core::index1 max_excluded_for_atom) :
  n_atoms(n_atoms), max_excluded_for_atom(max_excluded_for_atom) {

  excluded_pairs.resize(max_excluded_for_atom * n_atoms);
  std::fill(excluded_pairs.begin(),excluded_pairs.end(),std::numeric_limits<core::index4>::max());
  excluded_pairs_counts.resize(n_atoms);
}


void ExcludedPairsList::sort() {

  for (core::index4 i = 0; i < n_atoms; ++i)
    std::sort(excluded_pairs.begin() + i * max_excluded_for_atom, excluded_pairs.begin() + (i + 1) * max_excluded_for_atom);
}

ExcludedPairsList ExcludedPairsList::create_excluded_pair_list(const core::chemical::Molecule<AtomIndexType> &molecule) {

  core::index4 n_atoms = molecule.count_atoms();

  std::vector<std::tuple<AtomIndexType, AtomIndexType, AtomIndexType>> planars;
  core::chemical::find_planar_angles(molecule, planars);
  std::vector<std::tuple<AtomIndexType, AtomIndexType, AtomIndexType, AtomIndexType>> dihedrals;
  core::chemical::find_torsion_angles(molecule, dihedrals);

  // --- Here we find the maximum number of exclusions per atom
  std::vector<core::index2> excluded_each_atom(n_atoms); // --- temporary vector counts how many each atom was in an excluded pair
  for (auto bit = molecule.cbegin_bonds(); bit != molecule.cend_bonds(); ++bit) {
    std::pair<core::index4,core::index4> pair = bit->first;
//    const auto & edge = bit->second;
    excluded_each_atom[pair.first]++;
    excluded_each_atom[pair.second]++;
  }
  for (const auto &pi:planars) {
    excluded_each_atom[std::get<0>(pi).atom_index]++;
    excluded_each_atom[std::get<1>(pi).atom_index]++;
    excluded_each_atom[std::get<2>(pi).atom_index]++;
  }
  for (const auto &di:dihedrals) {
    excluded_each_atom[std::get<0>(di).atom_index]++;
    excluded_each_atom[std::get<1>(di).atom_index]++;
    excluded_each_atom[std::get<2>(di).atom_index]++;
    excluded_each_atom[std::get<3>(di).atom_index]++;
  }

  core::index1 n_excl = *std::max_element(excluded_each_atom.begin(), excluded_each_atom.end());
  ExcludedPairsList excl_list(n_atoms, n_excl+1); //+1 because we need to exlude also "self" atom

  // --- exclude self-atoms
  for (core::index4 ai = 0; ai < n_atoms; ++ai) {
    excl_list.excluded_pairs[ai * excl_list.max_excluded_for_atom + excl_list.excluded_pairs_counts[ai]] = ai;
    excl_list.excluded_pairs_counts[ai]++;
  }

  // --- exclude atoms involved in bonds
  for (auto bit = molecule.cbegin_bonds(); bit != molecule.cend_bonds(); ++bit) {

    std::pair<core::index4,core::index4> pair = bit->first;
    excl_list.exclude_pair(pair.first,pair.second);
  }

  // --- exclude atoms involved in planar angles
  for (const auto &pi:planars)
    excl_list.exclude_pairs(std::get<0>(pi).atom_index,std::get<1>(pi).atom_index,std::get<2>(pi).atom_index);

  // --- exclude atoms involved in dihedral angles
  for (const auto &di:dihedrals)
    excl_list.exclude_pairs(std::get<0>(di).atom_index,std::get<1>(di).atom_index,std::get<2>(di).atom_index,std::get<3>(di).atom_index);

  excl_list.sort();

  return excl_list;
}

void ExcludedPairsList::exclude_pair(core::index4 i, core::index4 j) {

  if (std::find(excluded_pairs.begin() + i * max_excluded_for_atom,
    excluded_pairs.begin() + i * max_excluded_for_atom + excluded_pairs_counts[i], j) ==
      excluded_pairs.begin() + i * max_excluded_for_atom + excluded_pairs_counts[i]) {
    excluded_pairs[i * max_excluded_for_atom + excluded_pairs_counts[i]] = j;
    excluded_pairs_counts[i]++;
  }
  if (std::find(excluded_pairs.begin() + j * max_excluded_for_atom,
    excluded_pairs.begin() + j * max_excluded_for_atom + excluded_pairs_counts[j], i) ==
      excluded_pairs.begin() + j * max_excluded_for_atom + excluded_pairs_counts[j]) {
    excluded_pairs[j * max_excluded_for_atom + excluded_pairs_counts[j]] = i;
    excluded_pairs_counts[j]++;
  }
}

void ExcludedPairsList::exclude_pairs(core::index4 i, core::index4 j,core::index4 k, core::index4 l) {

  exclude_pair(i,j);
  exclude_pair(i,k);
  exclude_pair(i,l);
  exclude_pair(j,k);
  exclude_pair(j,l);
  exclude_pair(k,l);
}

void ExcludedPairsList::exclude_pairs(core::index4 i, core::index4 j,core::index4 k) {

  exclude_pair(i,j);
  exclude_pair(i,k);
  exclude_pair(j,k);
}

void ExcludedPairsList::print(std::ostream & out) const {

  for(core::index4 i=0;i<n_atoms;++i) {
    out << std::setw(4) << i << " : ";
    for (core::index1 j = 0; j < excluded_pairs_counts[i]; ++j)
      out << std::setw(4) << excluded_pairs[i * max_excluded_for_atom + j];
    out <<"\n";
  }
}

}
}
}
