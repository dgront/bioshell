#include <iostream>
#include <string>
#include <stdio.h>
#include <stdexcept>

#include <core/index.hh>

#include <utils/io_utils.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMBondType.hh>
#include <simulations/forcefields/mm/MMBondedParameters.hh>
#include <simulations/forcefields/mm/MMAtomTyping.hh>

namespace simulations {
namespace forcefields {
namespace mm {

utils::Logger MMBondedParameters::manager_logger("MMBondedParameters");

const static std::regex group_regex("\\[\\ *(\\w+)\\ *\\]((?:\\s*[^\\[\\s])*)");

//These are the names of sections recognised by this code
const static std::string bondtypes = "bondtypes";
const static std::string angletypes = "angletypes";
const static std::string dihedraltypes = "dihedraltypes";
const static std::string impropertypes = "impropertypes";

void MMBondedParameters::make_new_bond_type(std::string & line, const MMAtomTyping_SP atom_typing) {

  bond_types_.emplace_back(line, atom_typing);
  manager_logger << utils::LogLevel::FINER << "Registering a bond type: " << bond_types_.back().to_string()<<"\n";
}

void MMBondedParameters::make_new_planar_type( std::string & line, const MMAtomTyping_SP atom_typing) {

  planar_types_.emplace_back(line, atom_typing);
  manager_logger << utils::LogLevel::FINER << "Registering a planar type: " <<  planar_types_.back().to_string()<<"\n";
}

void MMBondedParameters::make_new_dihedral_type(std::string & line, const MMAtomTyping_SP atom_typing) {

  dihedral_types_.emplace_back(line, atom_typing);
  manager_logger << utils::LogLevel::FINER << "Registering a dihedral type: " <<  dihedral_types_.back().to_string()<<"\n";
}


void MMBondedParameters::read_params_file(const std::string &fname, const MMAtomTyping_SP atom_typing) {

  n_atom_types_ = atom_typing->count_atom_types();
  atom_typing_ = atom_typing;

  std::function<void(MMBondedParameters&, std::string & line, const MMAtomTyping_SP atom_typing)> func = 0;

  std::string file_content = utils::load_text_file(fname);

  auto sections_begin = std::sregex_iterator(file_content.begin(), file_content.end(), group_regex);
  auto sections_end = std::sregex_iterator();

  for (auto sections_iter = sections_begin; sections_iter != sections_end; ++sections_iter) { //for every section in the file
    std::string group_name = (*sections_iter)[1];
    std::string group_data = (*sections_iter)[2];
    func = 0;
    if(group_name==bondtypes) func = &MMBondedParameters::make_new_bond_type;
    if(group_name==angletypes) func = &MMBondedParameters::make_new_planar_type;
    if(group_name==dihedraltypes) func = &MMBondedParameters::make_new_dihedral_type;
    if(group_name==impropertypes) func = &MMBondedParameters::make_new_dihedral_type;
    if(func==0) {
      manager_logger << utils::LogLevel::WARNING << "Unknown section found in MM parameters file: "<<group_name<<"\n";
      continue;
    }
    unsigned long i;
    for (std::string &group_line : utils::split(group_data, {'\n'})) {
      i = group_line.find_first_not_of(" \\t");
      if (i != std::string::npos && group_line[i] != ';')
        func(*this,group_line, atom_typing_);
    }
  }
  manager_logger << utils::LogLevel::INFO << fname<<" file parsed; registered types:\n"
    << "bonds:     " << bond_types_.size() << "\n"
    << "planars:   " << planar_types_.size() << "\n"
    << "dihedrals: " << dihedral_types_.size() << "\n";
}

bool MMBondedParameters::is_bond_defined(const core::index1 atom_type1, const core::index1 atom_type2) const {

  for(const MMBondType & bt:bond_types_)
    if(bt.is_it_me(atom_type1,atom_type2)) return true;
  return false;
}

const MMBondType & MMBondedParameters::find_bond(const core::index1 atom_type1, const core::index1 atom_type2) const {

  for(const MMBondType & bt:bond_types_)
    if(bt.is_it_me(atom_type1,atom_type2)) return bt;

  throw std::invalid_argument(
      utils::string_format("Can't find a bond definition between atom types %d %s and %d %s", atom_type1,
          atom_typing_->atom_internal_name(atom_type1).c_str(), atom_type2,
          atom_typing_->atom_internal_name(atom_type2).c_str()));
}

const MMPlanarType & MMBondedParameters::find_planar(const core::index1 atom_type1, const core::index1 atom_type2, const core::index1 atom_type3) const {

  for (const MMPlanarType &bt:planar_types_)
    if (bt.is_it_me(atom_type1, atom_type2, atom_type3)) return bt;

  manager_logger << utils::LogLevel::CRITICAL << "Can't find a planar definition : "
    << atom_typing_->atom_internal_name(atom_type1) <<" "
    << atom_typing_->atom_internal_name(atom_type2) <<" "
    << atom_typing_->atom_internal_name(atom_type3) <<"\n";
  throw std::invalid_argument("Can't find a planar definition");
}

const MMDihedralType & MMBondedParameters::find_dihedral(const core::index1 atom_type1, const core::index1 atom_type2, const core::index1 atom_type3,
                                   const core::index1 atom_type4) const {

  for (const MMDihedralType &bt:dihedral_types_)
    if (bt.is_it_me(atom_type1, atom_type2, atom_type3, atom_type4)) return bt;

  manager_logger << utils::LogLevel::CRITICAL << "Can't find a dihedral definition : "
    << atom_typing_->atom_internal_name(atom_type1) <<" "
    << atom_typing_->atom_internal_name(atom_type2) <<" "
    << atom_typing_->atom_internal_name(atom_type3) <<" "
    << atom_typing_->atom_internal_name(atom_type4) <<"\n";

  throw std::invalid_argument("Can't find a dihedral definition");
}



}
}
}
