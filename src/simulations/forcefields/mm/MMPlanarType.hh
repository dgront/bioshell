#ifndef SIMULATIONS_CARTESIAN_FF_MM_MMPlanarType_HH
#define SIMULATIONS_CARTESIAN_FF_MM_MMPlanarType_HH

#include <core/index.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>
#include <simulations/forcefields/mm/MMAtomTyping.hh>

namespace simulations {
namespace forcefields {
namespace mm {

/** @brief Provides parameters for one planar angle type in molecular mechanic force field.
 *
 * This class provides parameters necessary to calculate energy of a single planar angle, according to the following formula:
 * \f[
 * E(\alpha) = {K_{\alpha}} (\alpha-\alpha_0)^2
 * \f]
 * The parameters are returned by the respective methods:
 *    - <code>alpha0()<\code> returns \f$\alpha_0\f$
 *    - <code>angle()<\code> returns \f$\alpha_0\f$
 *    - <code>constant()<\code> returns \f$K_\alpha\f$
 *    - <code>Ka()<\code> returns \f$K_\alpha\f$
 */
class MMPlanarType {
public:

  /** @brief Creates a definition object from a string (Gromos FF definition format)
   *
   * Example planar angle definitions:
   * <code><pre>
   * H0  CT  H0           1   109.500    292.880 ; comment after a semicolon
   * H0  CT  N            1   109.500    418.400 ; 03GLY AA general  changed based on NMA nmodes
   * C   CT  H0           1   109.500    418.400 ; 03GLY AA general  changed based on NMA nmodes
   * HW  OW  HW           1   104.520    836.800 ; TIP3P water
   * HW  HW  OW           1   127.740      0.000 ; (found in crystallographic water with 3 bonds)
   * C   C   O            1   120.000    669.440 ; new99
   * </pre></code>
   */
  MMPlanarType(const std::string &cfg_line, const MMAtomTyping_SP atom_typing);

  /// Copy c'tor
  MMPlanarType(const MMPlanarType & type);

  /** @brief Property c'tor.
   *
   * @param type_i - index of the type of the first atom involved in the planar angle
   * @param type_j - index of the type of the second atom
   * @param type_k - index of the type of the third atom
   * @param alpha0 - equilibium angle value
   * @param k - spring force constant
   */
  MMPlanarType(const core::index1 type_i,const core::index1 type_j, const core::index1 type_k, const double alpha0, double k);

  /// Returns equilibrium value of planar angle (in radians!)
  double alpha0() const { return alpha0_; }

  /// Returns equilibrium value of planar angle (in radians!)
  double angle() const { return alpha0_; }

  /// Returns planar angle spring constant
  double constant() const { return constant_; }

  /// Returns planar angle spring constant
  double Ka() const { return constant_; }

  /// Returns the atom_type of the first atom in this planar angle
  core::index1 type_i() const { return type_i_; }

  /// Returns the atom_type of the second atom in this planar angle
  core::index1 type_j() const { return type_j_; }

  /// Returns the atom_type of the third atom in this planar angle
  core::index1 type_k() const { return type_k_; }

  /// Less-than operator is used to sort a vector of MMPlanarType objects
  bool operator< (const MMPlanarType & right) const {

    if(type_i_ < right.type_i_) return true;
    if(type_i_ > right.type_i_) return false;
    if(type_j_ < right.type_j_) return true;
    if(type_j_ > right.type_j_) return false;

    return type_k_ < right.type_k_;
  }

  /// is-equal operator
  bool operator== (const MMPlanarType & right) const { return ((type_i_ == right.type_i_) && (type_j_ == right.type_j_) && (type_k_ < right.type_k_)); }

  /** @brief returns true if this object defines a planar angle between three particular atoms (by atom type).
   *
   * @param atom_name1 - the force field specific type index of the first of the three atoms that form a planar
   * @param atom_name2 - the force field specific type index of the second of the three atoms that form a planar
   * @param atom_name3 - the force field specific type index of the third of the three atoms that form a planar
   */
  bool is_it_me(const core::index1 atom_type1, const core::index1 atom_type2, const core::index1 atom_type3) const {
      return ((atom_type1 == type_i_) && (atom_type3 == type_k_)) ||
             ((atom_type1 == type_k_) && (atom_type3 == type_i_));
  }

  /// Returns a string representing this object
  std::string to_string() const;

private:
  double alpha0_;
  double constant_;
  core::index1 type_i_;
  core::index1 type_j_;
  core::index1 type_k_;
};

}
}
}
#endif
