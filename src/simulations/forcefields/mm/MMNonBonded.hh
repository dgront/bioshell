#ifndef SIMULATIONS_CARTESIAN_FF_MM_MMNonBonded_HH
#define SIMULATIONS_CARTESIAN_FF_MM_MMNonBonded_HH

#include <iostream>
#include <stdexcept>

#include <core/index.hh>
#include <core/data/basic/Array2D.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMBondEnergy.hh>
#include <simulations/forcefields/mm/MMNeighborList.hh>

#include <simulations/forcefields/mm/ExcludedPairsList.hh>
#include <simulations/forcefields/mm/graph_from_residue_chain.hh>
#include <set>

namespace simulations {
namespace forcefields {
namespace mm {

/** @brief Lennard-Jones and electrostatic energy terms combined for efficiency.
 * This class evaluates the energy strictly according to the formula, neither a cutoff nor a shift is applied
 */
class MMNonBonded : public ByAtomEnergy {
public:

  const double FACTOR_KCAL = 332.0;
  const double FACTOR_KJ = 1389.3545208;
  const double HARD_SPHERE_REPULSION_SQUARE = 0.5*0.5; ///< Any two atoms cannot be closer than that

  MMNonBonded(const CartesianChains & system, const MMBondEnergy & bonded_energy) : ByAtomEnergy("MMNonBonded"),
     parameters(std::dynamic_pointer_cast<MMAtomTyping> (system.atom_typing)),
    nbl(system,10,4,1,bonded_energy),logger("MMNonBonded") {
  }

  /// \brief Virtual destructor to obey the rules
  ~MMNonBonded() override = default;

  virtual double energy(CartesianAtoms &the_system) {
    double en = 0.0;
    for (core::index4 which_atom = 0; which_atom != the_system.n_atoms; ++which_atom) {
      en += energy(the_system, which_atom);
    }
    return en / 2;
  }

  virtual double energy(CartesianAtoms &the_system, core::index4 which_atom) {

    double ene = 0.0, r = 0.0;
    for (auto j = nbl.cbegin(which_atom); j != nbl.cend(which_atom); ++j) {
      ene += calculate_one_pair(the_system, which_atom, *j, r);
    }
    return ene;
  }

  double delta_energy(CartesianAtoms &reference_conformation, const core::index4 which_atom,
        CartesianAtoms &new_conformation) override {
    double en = 0.0;
    en -= energy(reference_conformation, which_atom);
    en += energy(new_conformation, which_atom);
    return en;
  }

  double delta_energy(CartesianAtoms &reference_conformation, const core::index4 atom_from,
      const core::index4 atom_to, CartesianAtoms &new_conformation) override {

    double ene = 0.0, r = 0.0;
    std::set<std::pair<core::index4, core::index4 >> pair_to_calc;
    for (auto atm = atom_from; atm <= atom_to; ++atm) {
      for (auto j = nbl.cbegin(atm); j != nbl.cend(atm); ++j) {
        if (atm < *j) pair_to_calc.emplace(atm, *j);
        else pair_to_calc.emplace(*j, atm);
      }
    }
    for (auto pair = pair_to_calc.cbegin(); pair != pair_to_calc.cend(); ++pair) {
      ene -= calculate_one_pair(reference_conformation, pair->first, pair->second, r);
      ene += calculate_one_pair(new_conformation, pair->first, pair->second, r);
    }

    return ene;
  }

  double calculate_one_pair(CartesianAtoms &the_system, core::index4 atom1, core::index4 atom2, double &distance_sq) {

    const double sigma_i = parameters->sigma(the_system[atom1].atom_type);
    const double epsilon_i = parameters->epsilon(the_system[atom1].atom_type);
    const double q_i = parameters->charge(the_system[atom1].atom_type, the_system[atom1].residue_type) * 0.0001 - 1.0;
    distance_sq = 0;   // --- distance = 0 means no interaction

    double r2 = the_system[atom1].closest_distance_square_to(the_system[atom2]);
    distance_sq = r2; // --- return the distance as well
    if (r2 < HARD_SPHERE_REPULSION_SQUARE) return 0.0;

    const double sigma_j = parameters->sigma(the_system[atom2].atom_type);
    const double epsilon_j = parameters->epsilon(the_system[atom2].atom_type);
    const double q_j = parameters->charge(the_system[atom2].atom_type, the_system[atom2].residue_type) * 0.0001 - 1.0;
    double r = sqrt(r2);
    double sigma = (sigma_i + sigma_j) * 0.5;
    double r2_s = sigma / r2;
    r2_s = r2_s * r2_s;
    double r6 = r2_s * r2_s * r2_s;
    double r12 = r6 * r6;
    const double epsilon_four = 4.0 * sqrt(epsilon_i * epsilon_j);
    double en = epsilon_four * (r12 - r6);
    en += FACTOR_KCAL * q_i * q_j / r;

    return en;
  }

  const MMNeighborList & get_neighbor_list() const { return nbl; }

private:
  const MMAtomTyping_SP parameters;
  const MMNeighborList nbl;
  static const std::string name_;
  utils::Logger logger;
};


const std::string MMNonBonded::name_ = "MMNonBonded";

}
}
}
#endif
