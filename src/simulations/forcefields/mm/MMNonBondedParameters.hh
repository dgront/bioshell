#ifndef SIMULATIONS_CARTESIAN_FF_MM_MMAtomType_HH
#define SIMULATIONS_CARTESIAN_FF_MM_MMAtomType_HH

#include <string>
#include <map>

#include <core/index.hh>

namespace simulations {
namespace forcefields {
namespace mm {

/** @brief Defines one atom type in molecular mechanic force field.
 * \todo_code multiply sigma values in amber03_atoms.par and remove sigma_*=10.0; from constructors
 */
class MMNonBondedParameters {
public:

  /** @brief Creates an atom definition object from a string (Gromos FF definition format)
   *
   * @param id - integer id assigned to that atom type
   * @param cfg_line - input file name
   * Example file content:
   * <code><pre>
[ atomtypes ]
; name      at.num  mass     charge ptype  sigma      epsilon
H0           1       1.008   0.0000  A   2.47135e-01  6.56888e-02
Br          35      79.90    0.0000  A   0.00000e+00  0.00000e+00
C            6      12.01    0.0000  A   3.39967e-01  3.59824e-01
CA           6      12.01    0.0000  A   3.39967e-01  3.59824e-01
   * </pre></code>
   */
  MMNonBondedParameters(const core::index1 id, const std::string &cfg_line);

  /// Copy constructor
  MMNonBondedParameters(const MMNonBondedParameters& mm) ;

  /// Returns type index, ex. 0, 1, 2...
  core::index1 id() const { return id_; }

  /// Returns the type of the atom in molecular mechanic force field
  const std::string & internal_name() const { return type_; }

  /// Returns the charge for the atom in molecular mechanic force field
  const double & charge() const { return charge_; }

  /// Returns the mass for the atom in molecular mechanic force field
  const double & mass() const { return mass_; }

  /// Returns the value of sigma for the atom in molecular mechanic force field
  const double & sigma() const { return sigma_; }

  /// Returns the value of epsilon for the atom in molecular mechanic force field
  const double & epsilon() const { return epsilon_; }

  /// Returns the atom number representing the element of that atom
  const core::index1 & element() const { return element_; }

private:
  core::index1 id_;
  core::index1 element_;
  double charge_;
  double mass_;
  double sigma_;
  double epsilon_;
  std::string type_;
};

/** @brief Main function that reads a file with atom typing.
 *
 * This function read e.g. a "amber03_ffnonbonded.itp" file.
 *
 * @param fname - reference to string containing name of file to be read.
 * @param dst - reference to existing map of identical type to returned one. This is to allow to append
 * new atom types to the existing ones. Atom tyoe name is used as a key for that map
 * @return Reference to map given dst argument.
 */
std::map<std::string, MMNonBondedParameters> &
read_mm_atom_types(const std::string &fname, std::map<std::string, MMNonBondedParameters> &dst);

}
}
}
#endif
