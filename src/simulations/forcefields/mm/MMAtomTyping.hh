#ifndef SIMULATIONS_CARTESIAN_FF_MM_MMAtomTyping_HH
#define SIMULATIONS_CARTESIAN_FF_MM_MMAtomTyping_HH

#include <map>

#include <core/index.hh>

#include <core/data/basic/Vec3.hh>

#include <simulations/systems/AtomTypingInterface.hh>
#include <simulations/forcefields/mm/MMNonBondedParameters.hh>
#include <simulations/systems/AtomTypingVariants.hh>

#include <utils/Logger.hh>
#include "MMResidueType.hh"

namespace simulations {
namespace forcefields {
namespace mm {

/** @brief Atom typing for molecular mechanics force fields
 */
class MMAtomTyping : public systems::AtomTypingInterface {
public:

  /** @brief Create new atom typing by reading a MM FF param file
   * @param input_file_name - MM_FF param file
   */
  MMAtomTyping(const std::map<std::string, MMNonBondedParameters> & atom_params,
          std::map<std::string, MMResidueType>& residue_topology);

  /// Virtual destructor to satisfy a compiler
  virtual ~MMAtomTyping() = default;

  /** @brief Returns true if a given residue type has been registered
   * @param residue_name - residue name in the three-letter code
   * @param variant - request a special variant of the residue, e.g. C-terminal
   * @return true if such a residue name has been already registered in this atom typing
   */
  bool is_known_residue_type(const std::string & residue_name,
                             const systems::AtomTypingVariants variant) const override;

  /** @brief Returns index of a given residue type
   * Throws exception if a given residue type has not been registered
   * @param residue_name - residue name in the three-letter code
   * @param variant - request a special variant of the residue, e.g. C-terminal
   * @return residue type code
   */
  core::index1 residue_type(const std::string & residue_name,
                         const systems::AtomTypingVariants variant) const override;

  /** @brief Returns an internal  name for a given internal residue type.
   *  @param residue_id - index assigned to a given atom type according to this  typing
   *  @return internal name of a given residue, including a possible patch, e.g. NALA (N-terminal alanine)
   */
  const std::string & residue_internal_name(const core::index1 residue_id) const override { return known_residues_[residue_id]; }

  /// @brief Count how many different residue types (including patched variants) are defined by this typing
  virtual core::index1 count_residue_types() const {  return known_residues_.size(); }

  /** @brief Returns the atom type assigned in this force field to a given PDB atom
   *
   * @param a - PdbAtom object
   * @return atom type index (from 0)
   */
  core::index1 atom_type(const core::data::structural::PdbAtom &a) const override;

  /** @brief Returns an internal index for an atom identified by its PDB name, its residue name and a requested variant.
   *
   * This method should be used to create terminal variants for residues
   * @param pdb_atom_name - name of the atom, as it appears in a PDB file (with spaces, e.g. " CA ")
   * @param residue_name - name of the residue the given atom belongs to (e.g. "GLY")
   * @param variant - request a special variant of the residue, e.g. C-terminal
   * @return atom type index (from 0)
   */
  core::index1 atom_type(const std::string & pdb_atom_name, const std::string & residue_name,
                         const systems::AtomTypingVariants variant) const override;

  /** @brief Returns true if a given string has been registered as an atom type
   * @param pdb_atom_name - name of the atom, as it appears in a PDB file (with spaces, e.g. " CA ")
   * @param residue_name - name of the residue the given atom belongs to (e.g. "GLY")
   * @param variant - request a special variant of the residue, e.g. C-terminal
   * @return true if this atom name has been already registered in this atom typing
   */
  bool is_known_atom_type(const std::string & atom_name, const std::string & residue_name,
                                  const systems::AtomTypingVariants variant) const override;

  /** @brief Returns true if a given string has been registered as an atom type
   * @param a - PdbAtom object
   * @return true if this atom name has been already registered in this atom typing
   */
  bool is_known_atom_type(const core::data::structural::PdbAtom &a) const override;

  /** @brief Returns the sigma parameter value (LJ radius) for an atom of a given type
   * @param atom_type - atom type
   * @return sigma in Angstroms
   */
  double sigma(const core::index1 atom_type) const { return sigma_[atom_type]; }

  /** @brief Returns the sigma parameter value (LJ radius) for an atom of a given type
   * @param atom_type - atom type
   * @return epsilon in energy units
   */
  double epsilon(const core::index1 atom_type) const  { return epsilon_[atom_type]; }

  /** @brief Returns the mass of an atom of a given type
   * @param atom_type - atom type
   * @return atomic mass
   */
  double mass(const core::index1 atom_type) const { return mass_[atom_type]; }

  /** @brief Returns the atomic number for an atom of a given type
   * @param atom_type - atom type
   * @return atomic number, e.g. 6 for carbon, 8 for oxygen etc.
   */
  core::index1 element(const core::index1 atom_type) const { return element_[atom_type]; }

  /** @brief Returns the charge value for an atom of a given type and residue.
   *
   * Note that atoms of the same type may have different charge in different residue types.
   * @param atom_type - atom type
   * @param residue_type - residue type
   * @return charge value
   */
  double charge(const core::index1 atom_type, const core::index1 residue_type) const;

protected:
  /** @brief Returns index of a given residue type
   * Throws exception if a given residue type has not been registered
   * @param residue_name - patched residue name
   * @return residue type code
   */
  core::index1 residue_type(const std::string & residue_name) const;

private:
  // ---------- All residue types registered in this force field including PATCHED residues
  std::vector<std::string> known_residues_;
  // ---------- stores sigma_, epsilon_, mass_ and other parameters for each atom type (its index1)
  std::vector<double> sigma_;
  std::vector<double> epsilon_;
  std::vector<double> mass_;
  std::vector<core::index1> element_;
  // ---------- returns ff extended atom type (2 byte)  from atom name + residue name hash key (i.e. for an extended_atom_name)
  std::map<std::string, core::index2> extended_name_to_ff_index_;
  // ---------- charges are stored by index2 keys (i.e. the ff_index_ above) for each atom type / residue type combination
  std::map<core::index2, double> charge_;
//  // ---------- atom types (1 byte) are stored by a string key for each atom type / residue type combination i.e. the extended_atom_name
  std::map<std::string, core::index1> internal_types_;
  static utils::Logger mm_atom_typing_logs;

  // Produces a string unique for an atom-residue combination, e.g "NMET: CA " for arguments: " CA ", "MET" and "N_TERMINAL"
  std::string extended_atom_name(const std::string & pdb_atom_name, const std::string & residue_name,
                                 const systems::AtomTypingVariants variant) const;

  // Produces a string unique for an atom-residue combination, e.g "NMET: CA " for arguments: " CA ", "MET" and "N_TERMINAL"
  std::string extended_atom_name(const std::string & pdb_atom_name, const std::string & patched_residue_name) const;

  core::index2 charge_index(const core::index1 atom_type, const core::index1 residue_type) const {
    return (core::index2 (atom_type) << 8) + core::index2 (residue_type);
  }
};

/// Define a shared pointer type
typedef std::shared_ptr<MMAtomTyping> MMAtomTyping_SP;

}
}
}
#endif
