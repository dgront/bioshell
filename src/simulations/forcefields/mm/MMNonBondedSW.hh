#ifndef SIMULATIONS_CARTESIAN_FF_MM_MMNonBondedSW_HH
#define SIMULATIONS_CARTESIAN_FF_MM_MMNonBondedSW_HH

#include <iostream>
#include <stdexcept>

#include <core/index.hh>
#include <core/data/basic/Array2D.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMAtomTyping.hh>
#include <simulations/forcefields/mm/MMNonBonded.hh>

namespace simulations {
namespace forcefields {
namespace mm {

/** @brief Lennard-Jones and electrostatic energy with  switching function.
 *
 *   - switching function so the energy function is <code>0.0</code> after <code>CUTOFF_OFF</code> with a smooth transition
 *     over the range [<code>CUTOFF_ON</code>, <code>CUTOFF_OFF</code>)
 */
class MMNonBondedSW : public MMNonBonded {
public:

  const double CUTOFF_ON = 7.0;
  const double CUTOFF_OFF = 9.0;
  const double CUTOFF_ON2 = CUTOFF_ON * CUTOFF_ON;
  const double CUTOFF_OFF2 = CUTOFF_OFF * CUTOFF_OFF;

  MMNonBondedSW(const CartesianChains &system, const MMBondEnergy &bonded_energy) :
        MMNonBonded(system,bonded_energy) {}


  /// \brief Virtual destructor to obey the rules
  ~MMNonBondedSW() override = default;

  double calculate_one_pair(CartesianAtoms &the_system, core::index4 atom1, core::index4 atom2, double & distance_sq) {
    double ene = 0.0;
    ene = MMNonBonded::calculate_one_pair(the_system, atom1, atom2, distance_sq);
    if (distance_sq <= CUTOFF_ON2) {
      return ene;
    } else {
      double d = (CUTOFF_OFF2 - CUTOFF_ON2);
      double d3 = d * d * d;
      double w = (CUTOFF_OFF2 - distance_sq) * (CUTOFF_OFF2 - distance_sq);
      w *= (CUTOFF_OFF2 + 2 * distance_sq - 3 * CUTOFF_ON2);
      w /= d3;
      return w * ene;
    }
  }

};


}
}
}
#endif
