#include <core/index.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMAtomTyping.hh>
#include <simulations/forcefields/mm/MMPlanarType.hh>

namespace simulations {
namespace forcefields {
namespace mm {

utils::Logger planar_type_logger("MMPlanarType");

MMPlanarType::MMPlanarType(const std::string &cfg_line, const MMAtomTyping_SP atom_typing) {

  using systems::AtomTypingInterface;
  core::index2 id;
  std::string name1, name2, name3;
  std::stringstream ss(cfg_line);
  ss >> name1 >> name2 >> name3 >> id >> alpha0_ >> constant_;
  alpha0_ *= 3.14159 / 180.0;
  try {
    type_i_ = atom_typing->AtomTypingInterface::atom_type(name1);
  } catch(const std::out_of_range & e) {
    planar_type_logger << "Can't assign atom type index for internal atom type name: "<<name1<<"\n";
    throw(e);
  }
  try {
    type_j_ = atom_typing->AtomTypingInterface::atom_type(name2);
  } catch(const std::out_of_range & e) {
    planar_type_logger << "Can't assign atom type index for internal atom type name: "<<name2<<"\n";
    throw(e);
  }
  try {
    type_k_ = atom_typing->AtomTypingInterface::atom_type(name3);
  } catch(const std::out_of_range & e) {
    planar_type_logger << "Can't assign atom type index for internal atom type name: "<<name3<<"\n";
    throw(e);
  }
}

MMPlanarType::MMPlanarType(const core::index1 type_i,const core::index1 type_j, const core::index1 type_k, const double alpha0, double k) {
  type_i_ = type_i;
  type_j_ = type_j;
  type_k_ = type_k;
  alpha0_ = alpha0;
  constant_ = k;
}

MMPlanarType::MMPlanarType(const MMPlanarType & type) {

  type_i_ = type.type_i_;
  type_j_ = type.type_j_;
  type_k_ = type.type_k_;
  alpha0_ = type.alpha0_;
  constant_ = type.constant_;
}

std::string MMPlanarType::to_string() const {
  return utils::string_format("%3d %3d %3d    1    %7.3f %10.3f", type_i_, type_j_, type_k_, alpha0_ / 3.14159 * 180.0,constant_);
}

}
}
}
