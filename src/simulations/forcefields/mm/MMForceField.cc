#include <string>

#include <core/BioShellEnvironment.hh>

#include <simulations/forcefields/mm/MMForceField.hh>

namespace simulations {
namespace forcefields {
namespace mm {

MMForceField::MMForceField(const std::string &nonbonded_itp_file, const std::string &bonded_itp_file,
    const std::string &residues_rtp_file, const std::string &translation_rules_rtp) {

  // --- Load atom name translations, if given
  std::string translation_fname = core::BioShellEnvironment::from_file_or_db(translation_rules_rtp);
  if(translation_rules_rtp.size()>0)
    load_name_translation(translation_fname);

  // --- Read MM atom types
  std::string atom_fname = core::BioShellEnvironment::from_file_or_db(nonbonded_itp_file);
  read_mm_atom_types(atom_fname, nonbonded_parameters_);
  atom_type_names_.resize(nonbonded_parameters_.size(), "");
  // --- put the atom types in the correct order; it is assuemed that IDs assigned to MMNonBondedParameters start from 0
  for(const auto & at:nonbonded_parameters_)   // --- copy names by indexes assigned by the atom typing order
    atom_type_names_[at.second.id()] = at.first;

  // --- Read amber topology
  std::string topo_fname = core::BioShellEnvironment::from_file_or_db(residues_rtp_file);
  read_mm_topology_file(topo_fname, resiude_topology_);

  // --- Read bond parameters
  std::string bonded_fname = core::BioShellEnvironment::from_file_or_db(bonded_itp_file);
  bonded_parameters_.read_params_file(bonded_fname, mm_atom_typing());
}

MMAtomTyping_SP MMForceField::mm_atom_typing() {
  // --- Create BioShell's atom typing object if called for the first time
  if(mm_atom_typing_== nullptr)
    mm_atom_typing_ = std::make_shared<MMAtomTyping>(nonbonded_parameters_, resiude_topology_);
  return mm_atom_typing_;
}

void MMForceField::add_translation_rules(const std::string &translation_file) {
  load_name_translation(translation_file);
}

MMForceField::MMForceField(const std::string &force_field_name) : MMForceField(
    force_field_files(force_field_name).non_bonded_itp,
    force_field_files(force_field_name).bonded_itp,
    force_field_files(force_field_name).residues_rtp,
    force_field_files(force_field_name).translation_rules_rtp) {}

std::vector<std::string> MMForceField::known_force_fields_ = {"AMBER03"};

std::map<std::string, MMForceFieldFiles> MMForceField::force_field_files_ = {
    {"AMBER03", {"forcefield/mm/amber03_ffnonbonded.itp", "forcefield/mm/amber03_ffbonded.itp",
                 "forcefield/mm/amber03_aminoacids.rtp", "forcefield/mm/amber03_translation_rules.rtp"}}
};

std::vector<std::string> MMForceField::registered_residue_types() {
  std::vector<std::string> keys;
  for(auto const& imap: resiude_topology_) keys.push_back(imap.first);

  return keys;
}

const std::vector<std::string> & MMForceField::known_force_fields() { return known_force_fields_; }

const MMForceFieldFiles & MMForceField::force_field_files(const std::string & ff_name) {
  return force_field_files_.at(ff_name);
}

}
}
}
