#ifndef SIMULATIONS_CARTESIAN_FF_MM_ExcludedPairsList_HH
#define SIMULATIONS_CARTESIAN_FF_MM_ExcludedPairsList_HH

#include <iostream>
#include <stdexcept>

#include <core/index.hh>
#include <core/chemical/Molecule.hh>
#include <utils/Logger.hh>
#include <utils/string_utils.hh>

#include <simulations/forcefields/mm/graph_from_residue_chain.hh>

namespace simulations {
namespace forcefields {
namespace mm {

/** @brief List of pairs excluded from non-bonded calculations
 */
class ExcludedPairsList {
public:

  const core::index4 n_atoms; ///< The total number of atoms used for energy evaluation
  const core::index1 max_excluded_for_atom; ///< The maximum number of exclusions a single atom participates

  /** @brief Constructor creates a new object
   * @param n_atoms - the total number of atoms used for energy evaluation
   * @param max_excluded_for_atom - the maximum number of exclusions a single atom participates
   */
  ExcludedPairsList(const core::index4 n_atoms, const core::index1 max_excluded_for_atom);

  /** @brief starts iterations over all atoms that are excluded when long range of <code>which_atom</code> is evaluated
   *  @param which_atom - index of an atom whose energy is evaluated
   */
  std::vector<core::index4>::const_iterator cbegin(core::index4 which_atom) const { return excluded_pairs.cbegin() + which_atom * max_excluded_for_atom;}

  /** @brief Ends iterations over all atoms that are excluded when long range of <code>which_atom</code> is evaluated
   *  @param which_atom - index of an atom whose energy is evaluated
   */
  std::vector<core::index4>::const_iterator cend(core::index4 which_atom) const { return excluded_pairs.cbegin() + (which_atom + 1) * max_excluded_for_atom;}

  /** @brief Returns true if the given pair should be excluded from non-bonded energy evaluation
   * @param i - index of the first atom
   * @param j - index of the second atom
   */
  inline bool are_excluded(const core::index4 i,const core::index4 j) const {
    return std::binary_search(excluded_pairs.cbegin() + i * max_excluded_for_atom,excluded_pairs.cbegin() + (i) * max_excluded_for_atom+excluded_pairs_counts[i],j);
  }

  /** @brief Prints a list of exclusions on a given stream
   */
  void print(std::ostream & out) const;

  /** @brief Creates an exclusion list for a given molecule.
   *
   * The list will contain all pairs of atoms that are bonded, form a planar angle or a dihedral angle.
   */
  static ExcludedPairsList create_excluded_pair_list(const core::chemical::Molecule<AtomIndexType> & molecule);


  static ExcludedPairsList create_excluded_pair_list(const CartesianAtoms & system, const MMBondEnergy & bonded_energy) {


    core::chemical::Molecule<AtomIndexType> m = create_molecular_graph(system,bonded_energy.get_bonds());
    return create_excluded_pair_list(m);
  }

private:
  std::vector<core::index4> excluded_pairs;
  std::vector<core::index2> excluded_pairs_counts;
//  utils::Logger logger;
  void sort();
  void exclude_pair(core::index4 i,core::index4 j);
  void exclude_pairs(core::index4 i,core::index4 j,core::index4 k);
  void exclude_pairs(core::index4 i,core::index4 j,core::index4 k,core::index4 l);
};

}
}
}
#endif
