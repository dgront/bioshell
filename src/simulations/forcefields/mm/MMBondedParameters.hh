#ifndef SIMULATIONS_CARTESIAN_FF_MM_MMBondedParameters_HH
#define SIMULATIONS_CARTESIAN_FF_MM_MMBondedParameters_HH

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdexcept>

#include <core/index.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMBondType.hh>
#include <simulations/forcefields/mm/MMDihedralType.hh>
#include <simulations/forcefields/mm/MMPlanarType.hh>
#include <simulations/forcefields/mm/MMAtomTyping.hh>

namespace simulations {
namespace forcefields {
namespace mm {

/** @brief Holds definitions of bonded terms of a MM force field
 */
class MMBondedParameters {
public:

  /// Creates an empty set of bonded parameters
  MMBondedParameters() : atom_typing_(nullptr) { n_atom_types_ = 0; }

  /** @brief Reads definition of bonded interactions from a given file
   * @param fname - input file that provides data on bonded interactions
   * @param atom_typing - atom typing for the force field
   */
  MMBondedParameters(const std::string &fname, const MMAtomTyping_SP atom_typing) {
    read_params_file(fname, atom_typing);
  }

  /** @brief Reads definition of bonded interactions from a given file
   * @param fname - input file that provides data on bonded interactions
   * @param atom_typing - atom typing for the force field
   */
  void read_params_file(const std::string &fname, const MMAtomTyping_SP atom_typing);

  /// Returns bond type object if given atoms are bonded
  bool is_bond_defined(const core::index1 atom_type1, const core::index1 atom_type2) const;

  /// Returns the number of unique atom_types from a loaded atom_typing object
  core::index2 n_atom_types() const { return n_atom_types_; }

//  const MMBondType & longest_bond() const;

  /// Returns bond type object if given atoms are bonded
  const MMBondType &find_bond(const core::index1 atom_type1, const core::index1 atom_type2) const;

  /// Returns bond type object if given atoms are bonded
  const MMPlanarType &
  find_planar(const core::index1 atom_type1, const core::index1 atom_type2, const core::index1 atom_type3) const;

  /// Returns bond type object if given atoms are bonded
  const MMDihedralType &
  find_dihedral(const core::index1 atom_type1, const core::index1 atom_type2, const core::index1 atom_type3,
                const core::index1 atom_type4) const;

private:
  std::vector<MMBondType> bond_types_;
  std::vector<MMPlanarType> planar_types_;
  std::vector<MMDihedralType> dihedral_types_;
  core::index2 n_atom_types_;
  static utils::Logger manager_logger;
  MMAtomTyping_SP atom_typing_;

  void make_new_bond_type(std::string &line, const MMAtomTyping_SP atom_typing);

  void make_new_planar_type(std::string &line, const MMAtomTyping_SP atom_typing);

  void make_new_dihedral_type(std::string &line, const MMAtomTyping_SP atom_typing);
};

}
}
}
#endif
