#include <regex>

#include <core/data/structural/Residue.hh>
#include <utils/io_utils.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMResidueType.hh>

namespace simulations {
namespace forcefields {
namespace mm {

utils::Logger logger("MMResidueType");

//These strings are gathered in one place to be easy to change if .rtp file format has changed.

const static std::string bonds = "bonds";
const static std::string rotatable_bonds = "rotable_dihedrals"; //blad w rotable
const static std::string angles = "angles";
const static std::string atoms = "atoms";
const static std::string dihedrals = "dihedrals";
const static std::string impropers = "impropers";

/**
 * @brief Regex catching residue data in file.
 *
 * Subsequent groups are catching: <br>
 * (keep in mind that group zero means whole catch by (imposed by regex utils)) <br>
 *
 * 0) Group zero means whole catch by (imposed by regex utils) <br>
 * 1) (\S+) -> name of residue <br>
 * X) (?:\;.*) -> optional comment (non capturing group - not visible in iterator) <br>
 * 2) (((\\ \\[.*\\]\\s*\\n[^\\[]+)(?=\\ \\[))*(\\ \\[.*\\]\\s*\\n[^\\[]+)) -> whole data <br>
 * 3) ((\\ \\[.*\\]\\s*\\n[^\\[]+)(?=\\ \\[))* -> one group (bonds/angles/atoms/dihedrals/impropers) that is followed by
 * another group. It is necessary because we don't want to catch fragment of the next group here. <br>
 * 4) (\\ \\[.*\\]\\s*\\n[^\\[]+) -> Matches last group. <br>
 */
const static std::regex residue_regex(
  "\\[\\ *(\\S+)\\ *\\]\\s*(?:\\;.*)?\\n(((\\ \\[.*\\]\\s*\\n[^\\[]+)(?=\\ \\[))*(\\ \\[.*\\]\\s*\\n[^\\[]+))");

/**
 * @brief Regex catching data in group.
 * <br>
 * 0) group zero means whole catch by (imposed by regex utils) <br>
 * 1) (\w+) -> group name <br>
 * 2) ((?:\\s*[^\\[\\s])*) -> all lines <br>
 * X) (?:\\s*[^\\[\\s]) -> one line (catches everything and relies upon std::stringstream conversion (non capturing group)
 */
const static std::regex group_regex("\\[\\ *(\\w+)\\ *\\]((?:\\s*[^\\[\\s])*)");

/**
 * @brief Regex catching line with residue name in file with atoms names translation.
 * <br>
 * 0) group zero means whole catch by (imposed by regex utils) <br>
 * 1) ([^\[\]\s]+) -> residue name <br>
 */
const static std::regex translation_regex("\\[\\s*([^\\[\\]\\s]+)\\s*\\]");

using translation_maps_map = std::map<std::string, std::map<std::string, const std::string>>;

/**
 * @brief Map used to translation old style pdb names to new names. Applies to all residues
 */
static std::map<std::string, const std::string> pdb_name_update_global;
/**
 * @brief Map of maps used to translation old style pdb names to new names.
 * <br>
 * For each residue specified in file with translation there is a map in this map, that contains special translation <br>
 * rules for this residue.
 */
static translation_maps_map pdb_name_update_local;

static void emplace_rule(std::map<std::string, const std::string> &rules_map, const std::string line,
                         size_t line_number) {
    std::vector<std::string> rule = utils::split(line);
    if (rule.size() != 2)
        logger << utils::LogLevel::WARNING << "Line " << line_number << ": " << line << " could not be parsed\n";
    else {
        rules_map.emplace(rule[0], rule[1]);
        logger << utils::LogLevel::FINER << "atom name translation defined: " << rule[0] << " -> " << rule[1] << "\n";
    }
}

void load_name_translation(const std::string &fname) {

  logger << utils::LogLevel::FILE << "Atom name translations loaded from: " << fname << "\n";

  std::string file_content = utils::load_text_file(fname);
  std::vector<std::string> lines = utils::split(file_content, {'\n'});
  std::smatch sm;
  size_t i = 0;
  while (i < lines.size() && !std::regex_match(utils::trim(lines[i]), sm, translation_regex)) {
    emplace_rule(pdb_name_update_global, lines[i], i);
    ++i;
  }
  std::map<std::string, const std::string> *residue_map = &(pdb_name_update_local[sm[1]]);
  for (++i; i < lines.size(); ++i) {
    if (std::regex_match(utils::trim(lines[i]), sm, translation_regex))
      residue_map = &(pdb_name_update_local[sm[1]]);
    else
      emplace_rule(*residue_map, lines[i], i);
  }
}

static void translate_name(std::string &name, translation_maps_map::const_iterator map_iter) {
  if (map_iter != pdb_name_update_local.end()) {
    auto local_it = map_iter->second.find(name);
    if (local_it != map_iter->second.end()) {
      logger << utils::LogLevel::FINER << "Translated " << name << " into " << local_it->second << "\n";
      name = local_it->second;
      return;
    }
  }
  auto global_it = pdb_name_update_global.find(name);
  if (global_it != pdb_name_update_global.end()) {
    name = global_it->second;
  }
}


/**
 * @brief Subroutine to add atom from file line to given residue.
 * @param new_residue Residue to add atom to.
 * @param line String describing atom in rtp file, returned by regex.
 */
static void
add_atom(MMResidueType &new_residue, const std::string &line, translation_maps_map::const_iterator translation_map) {
  std::stringstream ss(line);
  MMResidueAtom atom_struct;
  ss >> atom_struct.pdb_atom_name >> atom_struct.internal_atom_name >> atom_struct.charge >>
     atom_struct.index;
  // --- translate PDB artom name: i.e. fix atom naming issues.
  // --- NOTE: atom names for translation are not badded to 4 characters, fixing names was moved to MMAtomTyping
  translate_name(atom_struct.pdb_atom_name, translation_map);
  new_residue.atoms.push_back(std::move(atom_struct));
}

/**
 * @brief Subroutine to add angle from file line to given residue.
 * @param new_residue Residue to add angle to.
 * @param line String describing angle in rtp file, returned by regex.
 */
static void
add_angle(MMResidueType &new_residue, const std::string &line, translation_maps_map::const_iterator translation_map) {
    std::stringstream ss(line);
    MMResidueAngle angle_struct;
    ss >> angle_struct.pdb_atom_name1 >> angle_struct.pdb_atom_name2 >>
       angle_struct.pdb_atom_name3 >> angle_struct.description;
    if (angle_struct.description.find(';') != std::string::npos)
        angle_struct.description.clear();
    translate_name(angle_struct.pdb_atom_name1, translation_map);
    translate_name(angle_struct.pdb_atom_name2, translation_map);
    translate_name(angle_struct.pdb_atom_name3, translation_map);
    new_residue.angles.push_back(std::move(angle_struct));
}

/**
 * @brief Subroutine to add bond from file line to given residue.
 * @param new_residue Residue to add bond to.
 * @param line String describing bond in rtp file, returned by regex.
 */
static void
add_bond(MMResidueType &new_residue, const std::string &line, translation_maps_map::const_iterator translation_map) {
    std::stringstream ss(line);
    MMResidueBond bond_struct;
    ss >> bond_struct.pdb_atom_name1 >> bond_struct.pdb_atom_name2 >> bond_struct.description;
    if (bond_struct.description.find(';') != std::string::npos)
        bond_struct.description.clear();
    translate_name(bond_struct.pdb_atom_name1, translation_map);
    translate_name(bond_struct.pdb_atom_name2, translation_map);
    new_residue.bonds.push_back(std::move(bond_struct));
}

/**
 * @brief Subroutine to add rotatabe bond from file line to given residue.
 * @param new_residue Residue to add rotatable bond to.
 * @param line String describing rotatable bond in rtp file, returned by regex.
 */
static void add_rotatable_bond(MMResidueType &new_residue, const std::string &line,
                               translation_maps_map::const_iterator translation_map) {
    std::stringstream ss(line);
    MMResidueRotatableBond rotable_bond_struct;
    std::string dummy;
    ss >> dummy >> rotable_bond_struct.pdb_atom_name1 >> rotable_bond_struct.pdb_atom_name2;
    translate_name(rotable_bond_struct.pdb_atom_name1, translation_map);
    translate_name(rotable_bond_struct.pdb_atom_name2, translation_map);
    new_residue.rotatable_bonds.push_back(std::move(rotable_bond_struct));
}

/**
 * @brief Subroutine to add dihedral from file line to given residue.
 * @param new_residue Residue to add dihedral to.
 * @param line String describing dihedral in rtp file, returned by regex.
 */
static void add_dihedral(MMResidueType &new_residue, const std::string &line,
                         translation_maps_map::const_iterator translation_map) {
    std::stringstream ss(line);
    MMResidueDihedral dihedral_struct;
    ss >> dihedral_struct.pdb_atom_name1 >> dihedral_struct.pdb_atom_name2 >>
       dihedral_struct.pdb_atom_name3 >> dihedral_struct.pdb_atom_name4 >> dihedral_struct.description;
    if (dihedral_struct.description.find(';') != std::string::npos)
        dihedral_struct.description.clear();
    translate_name(dihedral_struct.pdb_atom_name1, translation_map);
    translate_name(dihedral_struct.pdb_atom_name2, translation_map);
    translate_name(dihedral_struct.pdb_atom_name3, translation_map);
    translate_name(dihedral_struct.pdb_atom_name4, translation_map);
    new_residue.dihedrals.push_back(std::move(dihedral_struct));
}

/**
 * @brief Subroutine to add improper from file line to given residue.
 * @param new_residue Residue to add improper to.
 * @param line String describing improper in rtp file, returned by regex.
 */
static void add_improper(MMResidueType &new_residue, const std::string &line,
                         translation_maps_map::const_iterator translation_map) {
    std::stringstream ss(line);
    MMResidueImproper improper_struct;
    ss >> improper_struct.pdb_atom_name1 >> improper_struct.pdb_atom_name2 >>
       improper_struct.pdb_atom_name3 >> improper_struct.pdb_atom_name4 >> improper_struct.description;
    if (improper_struct.description.find(';') != std::string::npos)
        improper_struct.description.clear();
    translate_name(improper_struct.pdb_atom_name1, translation_map);
    translate_name(improper_struct.pdb_atom_name2, translation_map);
    translate_name(improper_struct.pdb_atom_name3, translation_map);
    translate_name(improper_struct.pdb_atom_name4, translation_map);
    new_residue.impropers.push_back(std::move(improper_struct));
}

/**
 * @brief Subroutine to clear atoms in given residue.
 * @param residue Residue to clear atoms in.
 */
static void clear_atoms(MMResidueType &residue) {
    residue.atoms.clear();
}

/**
 * @brief Subroutine to clear angles in given residue.
 * @param residue Residue to clear angles in.
 */
static void clear_angles(MMResidueType &residue) {
    residue.angles.clear();
}

/**
 * @brief Subroutine to clear bonds in given residue.
 * @param residue Residue to clear bonds in.
 */
static void clear_bonds(MMResidueType &residue) {
    residue.bonds.clear();
}

/**
 * @brief Subroutine to clear rotatable bonds in given residue.
 * @param residue Residue to clear rotatable bonds in.
 */
static void clear_rotatable_bonds(MMResidueType &residue) {
    residue.rotatable_bonds.clear();
}

/**
 * @brief Subroutine to clear dihedrals in given residue.
 * @param residue Residue to clear dihedrals in.
 */
static void clear_dihedrals(MMResidueType &residue) {
    residue.dihedrals.clear();
}

/**
 * @brief Subroutine to clear impropers in given residue.
 * @param residue Residue to clear impropers in.
 */
static void clear_impropers(MMResidueType &residue) {
    residue.impropers.clear();
}


/** @brief Map from group name (bond/atom/dihedral...) to function that parses single line and puts it into given
 * MMResidueType.
 */
static std::map<std::string, std::pair<std::function<void(MMResidueType &, const std::string &,
                                                                    translation_maps_map::const_iterator)>,
  std::function<void(MMResidueType &)>>> parse_by_name =
  {
    {atoms,           std::make_pair(add_atom, clear_atoms)},
    {angles,          std::make_pair(add_angle, clear_angles)},
    {bonds,           std::make_pair(add_bond, clear_bonds)},
    {rotatable_bonds, std::make_pair(add_rotatable_bond, clear_rotatable_bonds)},
    {dihedrals,       std::make_pair(add_dihedral, clear_dihedrals)},
    {impropers,       std::make_pair(add_improper, clear_impropers)},
  };


//doc in header file.
std::map<std::string, MMResidueType> &
read_mm_topology_file(const std::string &fname, std::map<std::string, MMResidueType> &dst) {
    std::string file_content = utils::load_text_file(fname);

    auto residues_begin = std::sregex_iterator(file_content.begin(), file_content.end(), residue_regex);
    auto residues_end = std::sregex_iterator(); //iterator over whole resiudes

    for (auto residue_iter = residues_begin; residue_iter != residues_end; ++residue_iter) //for every residue in file
    {
        // *** consult any numbers with documentation of regex ***
        std::string residue_name = (*residue_iter)[1];
        MMResidueType &new_residue = dst[residue_name]; //assign proper reference (create  if it does not exist)

        new_residue.residue_type_name = residue_name;
        std::string residue_data = (*residue_iter)[2];

        auto groups_begin = std::sregex_iterator(residue_data.begin(), residue_data.end(), group_regex);
        auto groups_end = std::sregex_iterator();
        for (auto group_iter = groups_begin; group_iter != groups_end; ++group_iter) {
            std::string group_name = (*group_iter)[1];
            std::string group_data = (*group_iter)[2];

            //some IDEs can't handle this pro syntax. Type of parse_line is same as second argument in parse_line map.
            decltype(parse_by_name)::mapped_type::first_type
            parse_line;
            auto function_it = parse_by_name.find(group_name);
            if (function_it == parse_by_name.end()) {
                //function to this property does not exist
                logger << utils::LogLevel::WARNING
                       << "Can't recognize a residue_data property (probably typo like 'diherdals'): \n" << group_name
                       << "\n" << group_data << "\n";
                continue;
            }
            parse_line = function_it->second.first;
            function_it->second.second(new_residue); //Clearing previous content
            auto residue_translation_it = pdb_name_update_local.find(new_residue.residue_type_name);
            unsigned long i;
            for (std::string &group_line : utils::split(group_data, {'\n'})) //for every line of group_data
            {
                i = group_line.find_first_not_of(" \\t");
                if (i != std::string::npos && group_line[i] != ';')
                    parse_line(new_residue, group_line, residue_translation_it);
            }
        }
    }
    return dst;
}

std::string patch_residue_name(const std::string &residue_name, const systems::AtomTypingVariants variant) {

  // --- There are a few hacks to fix inconsistencies between PDB and MM atom naming
  std::string rn = residue_name;
  if (rn.size()==2) rn.insert(0," ");
  if (rn.size()==1) rn.insert(0,"  ");
  if (rn == "HIS") rn = "HID";
  if (variant == systems::AtomTypingVariants::N_TERMINAL) rn = "N" + residue_name;
  if (variant == systems::AtomTypingVariants::C_TERMINAL) rn = "C" + residue_name;

  return rn;
}

}
}
}
