/** @file graph_from_residue_chain.hh
 * @brief Creates a molecular graph from a ResidueChain<C> object which faciliates detection of planar and dihedral angles
 */
#ifndef SIMULATIONS_CARTESIAN_FF_graph_from_residue_chain_HH
#define SIMULATIONS_CARTESIAN_FF_graph_from_residue_chain_HH

#include <iostream>
#include <stdexcept>

#include <core/index.hh>
#include <core/chemical/Molecule.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMBondType.hh>
#include <simulations/forcefields/mm/MMBondEnergy.hh>

namespace simulations {
namespace forcefields {
namespace mm {

/** @brief Data structure used to represent an atom in a molecule that will be created form a ResidueChain<C> object.
 */
struct AtomIndexType {
  core::index4 atom_index; ///< Index number of an atom (from 0)
  core::index2 atom_type; ///< Index referring to the type of this atom
};

/** @brief Returns true if the two structures describe the same atom.
 * Only <code>atom_index</code> field is compared.
 * @param left - the first of the two compared objects
 * @param right - the second of the two compared objects
 */
bool operator==(const AtomIndexType & left, const AtomIndexType & right);

/** @brief Returns true if the left atom has lower index than the right one.
 * Only <code>atom_index</code> field is compared.
 * @param left - the first of the two compared objects
 * @param right - the second of the two compared objects
 */
bool operator<(const AtomIndexType & left, const AtomIndexType & right);

/** @brief Creates a molecule graph representing a given system and its bonds.
 *
 * Atoms of the molecule are represented as structures.
 *
 * @param system - a system of atoms (upper casted to Vec3 objects)
 * @param bonds - an array of all bonds in this molecule
 * @returns a molecule representing a given ResidueChain<C> system
 */

    core::chemical::Molecule<AtomIndexType> create_molecular_graph(const CartesianAtoms & system,
                                                                   const std::vector<MMBondTerm> &bonds) ;

}
}
}

namespace std {
/** @brief Returns a hash for a AtomIndexType object which is just its <code>atom_index</code> field
 */
template<>
struct hash<simulations::forcefields::mm::AtomIndexType> {
  inline std::size_t operator()(const simulations::forcefields::mm::AtomIndexType &k) const { return k.atom_index; }
};
}
#endif
