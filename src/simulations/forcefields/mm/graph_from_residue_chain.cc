#include <simulations/forcefields/mm/graph_from_residue_chain.hh>

namespace simulations {
namespace forcefields {
namespace mm {

bool operator==(const AtomIndexType & left, const AtomIndexType & right) { return (left.atom_index == right.atom_index); }

bool operator<(const AtomIndexType & left, const AtomIndexType & right) { return left.atom_index < right.atom_index; }

core::chemical::Molecule<AtomIndexType> create_molecular_graph(const CartesianAtoms & system,
                                                               const std::vector<MMBondTerm> &bonds) {

    core::chemical::Molecule<AtomIndexType> molecule;
    for (core::index4 i = 0; i < system.n_atoms; ++i) {
        AtomIndexType a{i, system[i].atom_type};
        molecule.add_atom(a);
    }

    for (const MMBondTerm &e : bonds) {
        if (!molecule.are_bonded(e.i, e.j))
            molecule.bind_atoms(e.i, e.j, core::chemical::BondType::SINGLE);    //carbonyl bond is double!
    }

    return molecule;
}

}
}
}
