#ifndef SIMULATIONS_CARTESIAN_FF_MM_MMBondType_HH
#define SIMULATIONS_CARTESIAN_FF_MM_MMBondType_HH

#include <core/index.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMAtomTyping.hh>

namespace simulations {
namespace forcefields {
namespace mm {

/** @brief Provides parameters for one bond type in molecular mechanic force field.
 *
 * This class provides parameters necessary to calculate energy of a single bond, according to the following formula:
 * \f[
 * E(d) = {K_b} (d-d_0)^2
 * \f]
 * The parameters are returned by the respective methods:
 *    - <code>d0()<\code> returns \f$ d_0\f$
 *    - <code>length()<\code> returns \f$ d_0\f$
 *    - <code>constant()<\code> returns \f$K_b\f$
 *    - <code>Kb()<\code> returns \f$K_b\f$
 */
class MMBondType {
public:

  /** @brief Creates a definition object from a string (Gromos FF definition format)
   *
   * Example bond definitions:
   * <code><pre>
   *   C  CB         1    0.14190   374049.6 ;  comment after a semicolon
   *   C  CM         1    0.14440   343088.0
   *   C  CT         1    0.15220   265265.6
   * </pre></code>
   */
  MMBondType(const std::string &cfg_line, const MMAtomTyping_SP atom_typing);

  MMBondType(const MMBondType & type);

  MMBondType() {}

  /** @brief returns true if this object defines a bond between two particular atoms (by atom type).
   *
   * @param atom_name1 - the force field specific type index of the first of the two atoms to be bonded
   * @param atom_name2 - the force field specific type index of the second of the two atoms to be bonded
   */
  bool is_it_me(const core::index1 atom_type1, const core::index1 atom_type2) const {
      return ((atom_type1 == type_i_) && (atom_type2 == type_j_)) ||
             ((atom_type1 == type_j_) && (atom_type2 == type_i_));
  }

  /// Returns the atom_type of the first atom in this bond
  core::index1 type_i() const { return type_i_; }

  /// Returns the atom_type of the second atom in this bond
  core::index1 type_j() const { return type_j_; }

  /// Returns equilibrium bond length (in Angstroms)
  double length() const { return value_; }

  /// Returns equilibrium bond length (in Angstroms)
  double d0() const { return value_; }

  /// Returns bond spring constant
  double constant() const { return constant_; }

  /// Returns bond spring constant
  double Kb() const { return constant_; }

  /// Returns a string representing this object
  std::string to_string() const;

private:
  core::index1 type_i_;
  core::index1 type_j_;
  double value_;
  double constant_;
};

/// Prints some basic information about a bond type
std::ostream & operator<<(std::ostream & out, const MMBondType & mbt);

}
}
}
#endif
