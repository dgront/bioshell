#include <vector>
#include <tuple>

#include <core/index.hh>
#include <core/calc/structural/angles.hh>
#include <core/chemical/Monomer.hh>
#include <core/chemical/molecule_utils.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMPlanarEnergy.hh>


namespace simulations {
namespace forcefields {
namespace mm {

using namespace core::data::basic;

double MMPlanarEnergy::energy(CartesianAtoms & conformation) {
  double angle = 0, en = 0;
  for (const MMPlanarTerm &e : planar_terms_) {
      angle = core::calc::structural::evaluate_planar_angle(conformation[e.i], conformation[e.j], conformation[e.k]);
      angle -= e.alpha0;
      en += angle * angle * e.Ka / 2.0;
  }

  return en;
}
double MMPlanarEnergy::delta_energy(CartesianAtoms& referance_conformation, core::index4 which_atom,CartesianAtoms& new_conformation){
  double pangle = 0,nangle=0, en = 0;

  for (auto i = angles_for_atom_[which_atom].cbegin();i!=angles_for_atom_[which_atom].cbegin(); ++i) {
      const MMPlanarTerm &e = planar_terms_[*i];
      pangle = core::calc::structural::evaluate_planar_angle(referance_conformation[e.i], referance_conformation[e.j], referance_conformation[e.k]);
      nangle = core::calc::structural::evaluate_planar_angle(new_conformation[e.i], new_conformation[e.j], new_conformation[e.k]);
      pangle -= e.alpha0;
      nangle -= e.alpha0;
      en += nangle * nangle * e.Ka / 2.0;
      en -= pangle * pangle * e.Ka / 2.0;

  }
  return en;
}
/// @todo Find smallest and largest  MMPlanarTerm index and set flag only for these
double MMPlanarEnergy::delta_energy(CartesianAtoms& referance_conformation,  core::index4 atom_from,core::index4 atom_to,CartesianAtoms& new_conformation){
    double pangle = 0,nangle=0, en = 0;
    for (auto atm = atom_from;atm<=atom_to;++atm) {
        for (auto i = angles_for_atom_[atm].cbegin(); i != angles_for_atom_[atm].cbegin(); ++i) {
            MMPlanarTerm &e = planar_terms_[*i];
            if (e.calc_flag==0) {
                pangle = core::calc::structural::evaluate_planar_angle(referance_conformation[e.i],
                                                                       referance_conformation[e.j],
                                                                       referance_conformation[e.k]);
                nangle = core::calc::structural::evaluate_planar_angle(new_conformation[e.i], new_conformation[e.j],
                                                                       new_conformation[e.k]);
                pangle -= e.alpha0;
                nangle -= e.alpha0;
                en += nangle * nangle * e.Ka / 2.0;
                en -= pangle * pangle * e.Ka / 2.0;
                e.calc_flag = 1;
            }
        }
    }
    for (auto term:planar_terms_) term.calc_flag=0;

    return en;
}
double MMPlanarEnergy::energy(CartesianAtoms& conformation, core::index4 which_atom){
  double angle = 0, en = 0;

  for (auto i = angles_for_atom_[which_atom].cbegin();i!=angles_for_atom_[which_atom].cbegin(); ++i) {
      const MMPlanarTerm &e = planar_terms_[*i];
      angle = core::calc::structural::evaluate_planar_angle(conformation[e.i], conformation[e.j], conformation[e.k]);
      angle -= e.alpha0;
      en += angle * angle * e.Ka / 2.0;
  }
  return en;
}


void MMPlanarEnergy::create_planar_angles(const simulations::forcefields::mm::MMBondEnergy &bond_energy,
    const simulations::forcefields::mm::MMBondedParameters &bonded_params) {

  // ---------- Constructor check if this cast is allowed!
  const systems::CartesianChains &c = static_cast<const systems::CartesianChains &>(the_system);

  const auto &bonds = bond_energy.get_bonds();
  core::chemical::Molecule<AtomIndexType> molecule = create_molecular_graph(the_system, bonds);

//--Find all planar angles in molecule and prepare properties for MMPlanarType objects
  std::vector<std::tuple<AtomIndexType, AtomIndexType, AtomIndexType>> planars;
  find_planar_angles(molecule, planars);
  for (const auto &p: planars) {
    AtomIndexType ai = std::get<0>(p);
    AtomIndexType aj = std::get<1>(p);
    AtomIndexType ak = std::get<2>(p);
    try {
      const MMPlanarType &ptype = bonded_params.find_planar(ai.atom_type, aj.atom_type, ak.atom_type);
      planar_terms_.emplace_back(ai.atom_index, aj.atom_index, ak.atom_index, ptype.Ka(), ptype.alpha0());
      print_planar_msg(planar_terms_.back());
    } catch (std::invalid_argument e) {
      core::index2 ri = c.residue_for_atom(ai.atom_index);
      core::index2 rk = c.residue_for_atom(ak.atom_index);
      logs << utils::LogLevel::CRITICAL << "Missing planar angle definition for residue(s): "
           << ri << " " << core::chemical::Monomer::get(core::index2(the_system.operator[](ai.atom_index).residue_type)).code3
           << " - "
           << rk << " " << core::chemical::Monomer::get(core::index2(the_system.operator[](ak.atom_index).residue_type)).code3
           << "\n";
      throw e;
    }
  }

CompareMMPlanarTerms comparator(*this);
std::sort(planar_terms_.begin(), planar_terms_.end(), comparator);

for(core::index4 ie=0;ie<planar_terms_.size();++ie) {
  const MMPlanarTerm & e = planar_terms_[ie];
  angles_for_atom_[e.i].push_back(ie);
  angles_for_atom_[e.j].push_back(ie);
  angles_for_atom_[e.k].push_back(ie);

}
if(logs.is_logable(utils::LogLevel::FINER)) {
  for(core::index2 ir=0;ir<angles_for_atom_.size();++ir)
    logs << utils::LogLevel::FINER <<utils::string_format("Atom %4d involved in planars: %5d - %5d\n",ir,angles_for_atom_[ir][0],*(angles_for_atom_[ir].cend()));
}
}

std::string MMPlanarEnergy::nice_atom_info(core::index4 atom_index) const {

  // ---------- Constructor check if this cast is allowed!
  const systems::CartesianChains &c = static_cast<const systems::CartesianChains &>(the_system);

  return utils::string_format("%4d %4s of %3d %3s", atom_index,
      the_system.atom_typing->atom_internal_name(the_system[atom_index]).c_str(), c.residue_for_atom(atom_index),
      core::chemical::Monomer::get(core::index2(the_system[atom_index].residue_type)).code3.c_str());
}

void MMPlanarEnergy::print_planar_msg(const MMPlanarTerm & planar_term) {

const core::index4 i = planar_term.i;
const core::index4 j = planar_term.j;
const core::index4 k = planar_term.k;
double angle = core::calc::structural::evaluate_planar_angle(the_system[i], the_system[j], the_system[k]);
double e = planar_term.Ka*(angle-planar_term.alpha0)*(angle-planar_term.alpha0);
logs << utils::LogLevel::FINE << utils::string_format("New planar : %s and %s and %s a= %6.2f e=%6.2f\n",
  nice_atom_info(i).c_str(), nice_atom_info(j).c_str(), nice_atom_info(k).c_str(),
  core::calc::structural::to_degrees(angle), e);
}


}
}
}

