#ifndef SIMULATIONS_FORCEFIELDS_MM_MMDihedralEnergy_HH
#define SIMULATIONS_FORCEFIELDS_MM_MMDihedralEnergy_HH

#include <vector>
#include <tuple>

#include <core/index.hh>
#include <core/calc/structural/angles.hh>
#include <core/chemical/molecule_utils.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMPlanarType.hh>
#include <simulations/forcefields/mm/MMBondedParameters.hh>
#include <simulations/forcefields/mm/MMBondEnergy.hh>
#include <simulations/forcefields/mm/graph_from_residue_chain.hh>

namespace simulations {
namespace forcefields {
namespace mm {

using namespace core::data::basic;

/** @brief Evaluates energy of dihedral angles.
 * @see MMDihedralType - for dihedral angle energy definition
 */

class MMDihedralEnergy : public ByAtomEnergy {
private:

  /// Internal holder for a single dihedral angle term
  struct MMDihedralTerm {
    core::index4 i;
    core::index4 j;
    core::index4 k;
    core::index4 l;
    double Vn;
    double theta0;
    core::index1 nn;
    core::index1 calc_flag = 0;

    /// Assigning c'tor
    inline MMDihedralTerm(core::index4 ai,core::index4 aj, core::index4 ak, core::index4 al,
                          double vn, double a, core::index1 n) {
      i = ai;
      j = aj;
      k = ak;
      l = al;
      Vn = vn;
      theta0 = a;
      nn = n;
    }
  };

  /// Sorts dihedral angle definitions so they are ordered according to the order of residues in a chain
  struct CompareMMDihedralTerms {

    CompareMMDihedralTerms(const MMDihedralEnergy & en) : e(en) {}

    bool operator()(const MMDihedralTerm & left,const MMDihedralTerm & right) const {
      const CartesianChains & c = static_cast<const systems::CartesianChains&>(e.the_system);
      return (c.residue_for_atom(left.i) < c.residue_for_atom(right.i) ||
        c.residue_for_atom(left.l) < c.residue_for_atom(right.l));
    }

    const MMDihedralEnergy & e;
  };

public:

  /** @brief Creates energy object of all dihedral angles in the system.
   *
   * @param system - chain whose energy will be evaluated
   * @param bonded_manager - provides parameters for planar angle energy terms
   * @param bond_energy - bond energy object provides a list of all bonds; the bonds are necessary to detect dihedral angles;
   * <code>core::chemical::find_torsion_angles()</code> is used to detect dihedrals
   */
  MMDihedralEnergy(const CartesianAtoms &system, const MMBondedParameters &bonded_manager,
      const MMBondEnergy &bond_energy) : ByAtomEnergy("MMDihedralEnergy"), the_system(system), logs("MMDihedralEnergy") {

    // ---------- check if a given system is actually of CartesianChains type
    try {
      const systems::CartesianChains &c = static_cast<const systems::CartesianChains &>(the_system);
    } catch (const std::exception &e) {
      utils::exit_with_error("MMPlanarEnergy.hh", 86, "Can't cast a given system to CartesianChains type!");
    }

    angles_for_atom_.resize(system.CartesianAtoms::n_atoms);
    create_dihedral_angles(bond_energy, bonded_manager);
  }

  virtual ~MMDihedralEnergy() {}

    virtual double energy(CartesianAtoms & conformation);
    virtual double delta_energy(CartesianAtoms& referance_conformation, core::index4 which_atom,CartesianAtoms& new_conformation);

  /// @todo Find smallest and largest  MMDihedralTerm index and set flag only for these
  virtual double delta_energy(CartesianAtoms& referance_conformation, core::index4 atom_from,core::index4 atom_to,CartesianAtoms& new_conformation);
    virtual double energy(CartesianAtoms& conformation, core::index4 which_atom);

  const std::vector<MMDihedralTerm> &get_dihedral_angles() const {return dihedral_terms_;}

private:

  const CartesianAtoms &the_system; ///< the system whose energy will be evaluated
  utils::Logger logs;
  std::vector<MMDihedralTerm> dihedral_terms_;
  std::vector<std::vector<core::index4>> angles_for_atom_;

  void create_dihedral_angles(const MMBondEnergy &bond_energy, const MMBondedParameters &bonded_params) ;

  std::string nice_atom_info(core::index4 atom_index) const ;
  void print_dihedral_msg(const MMDihedralTerm & dihedral_term) ;
  void print_missing_dihedral_msg(const core::index4 i, const core::index4 j, const core::index4 k, const core::index4 l) ;

};


}
}
}
#endif // SIMULATIONS_FORCEFIELDS_MM_MMDihedralEnergy_HH

