#include <memory>
#include <math.h>

#include <core/BioShellEnvironment.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/io/DataTable.hh>

#include <utils/string_utils.hh>

#include <simulations/forcefields/mm/WaterModelParameters.hh>

namespace simulations {
namespace forcefields {

std::map<std::string, WaterModelParameters> WaterModelParameters::known_models;

std::ostream& operator<<(std::ostream& out, const WaterModelParameters& w) {

  out << utils::string_format("%5s : %5.3f %5.2f %6.3f %6.3f", w.name.c_str(), w.rOH, w.aHOH, w.qO, w.qH);
  return out;
}

WaterModelParameters::WaterModelParameters(const WaterModelParameters & m) {
  name = m.name;
  rOH = m.rOH;
  aHOH = m.aHOH;
  qO = m.qO;
  qH = m.qH;
  eO = m.eO;
  sO = m.sO;
}

WaterModelParameters& WaterModelParameters::operator=(const WaterModelParameters & m) {

  name = m.name;
  rOH = m.rOH;
  aHOH = m.aHOH;
  qO = m.qO;
  qH = m.qH;
  eO = m.eO;
  sO = m.sO;

  return *this;
}

core::data::structural::Residue_SP WaterModelParameters::create_residue() const {

  using namespace core::data::structural;

  double alpha = aHOH /2.0 * M_PI / 180.0;

  Residue_SP r = std::make_shared<Residue>(1,"HOH");
  PdbAtom_SP O = std::make_shared<PdbAtom>(1," O  ");
  r->push_back(O);

  PdbAtom_SP H1 = std::make_shared<PdbAtom>(1," H1 ");
  H1->x = rOH * cos(alpha);
  H1->y = rOH * sin(alpha);
  r->push_back(H1);
  PdbAtom_SP H2 = std::make_shared<PdbAtom>(1," H2 ");
  H2->x = rOH * cos(alpha);
  H2->y = -rOH * sin(alpha);
  r->push_back(H2);

  return r;
}

void WaterModelParameters::load_models(const std::string &fname) {

  utils::Logger logger("WaterModel::load_models");

  double kJ_mol_to_K = 1000.0 / (6.022E23 * 1.381E-23);

  logger << utils::LogLevel::FILE << "Reading ff file: " << fname << "\n";
  std::ifstream in(core::BioShellEnvironment::from_file_or_db(fname));
  core::data::io::DataTable dt;
  dt.load(in);

  for(const auto & row :dt) {
    logger << utils::LogLevel::INFO << "loading parameters for: " << row[0] << "\n";

    WaterModelParameters m;
    // name rOH    aHOH      qO     qH   sO         eO
    m.name = row[0];
    m.rOH = row.get<double>(1);
    m.aHOH = row.get<double>(2);
    m.qO = row.get<double>(3);
    m.qH = row.get<double>(4);
    m.sO = row.get<double>(5);
    m.eO = row.get<double>(6) * kJ_mol_to_K;
    if(row.size()==7)
      m.n_atoms = 3;
    known_models[row[0]] = m;
  }
}


}
}

