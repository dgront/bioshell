#include <vector>
#include <tuple>

#include <core/index.hh>
#include <simulations/forcefields/mm/MMBondEnergy.hh>

namespace simulations {
namespace forcefields {
namespace mm {

using namespace systems;


MMBondEnergy::MMBondEnergy(const CartesianAtoms &system, const MMBondedParameters &bond_manager)
    : ByAtomEnergy("MMBondEnergy"), the_system(system), logs("MMBondEnergy") {

  bonds_for_atom_.resize(system.CartesianAtoms::n_atoms);
  create_bonds(bond_manager);
}

double MMBondEnergy::energy(CartesianAtoms &conformation, core::index4 which_atom) {
  double d = 0, en = 0;
  for (auto i = bonds_for_atom_[which_atom].cbegin(); i != bonds_for_atom_[which_atom].cbegin(); ++i) {
    const MMBondTerm &e = all_bonds_[*i];
    d = conformation[e.i].distance_to(conformation[e.j]) - e.d0;
    en += e.k * d * d;
  }

  return en;
}


double MMBondEnergy::energy(CartesianAtoms &conformation) {
  double d = 0, en = 0;
  for (const MMBondTerm &e: all_bonds_) {
    d = conformation[e.i].distance_to(conformation[e.j]) - e.d0;
    en += e.k * d * d;
  }

  return en;
}

double MMBondEnergy::delta_energy(CartesianAtoms &reference_conformation, CartesianAtoms &new_conformation,
    const bool log_differences) {

  double dr = 0, enr = 0, dn = 0, enn = 0;
  for (const MMBondTerm &e: all_bonds_) {
    dr = reference_conformation[e.i].distance_to(reference_conformation[e.j]) - e.d0;
    enr += e.k * dr * dr;
    dn = new_conformation[e.i].distance_to(new_conformation[e.j]) - e.d0;
    enn += e.k * dn * dn;
    if (log_differences && fabs((dr - dn) > 0.0001)) { // logs << utils::LogLevel::FINE
      std::cerr << utils::string_format("%4d - %4d : %5.3f %5.3f : %7.4f %7.4f  %8.4f\n", e.i, e.j,
          reference_conformation[e.i].distance_to(reference_conformation[e.j]),
          new_conformation[e.i].distance_to(new_conformation[e.j]), enr, enn, (dr - dn));
    }
  }

  return enn - enr;
}

double MMBondEnergy::delta_energy(CartesianAtoms &reference_conformation, core::index4 which_atom,
    CartesianAtoms &new_conformation) {
  double dn = 0, dr = 0, en = 0;
  for (auto i = bonds_for_atom_[which_atom].cbegin(); i != bonds_for_atom_[which_atom].cbegin(); ++i) {
    const MMBondTerm &e = all_bonds_[*i];
    dn = new_conformation[e.i].distance_to(new_conformation[e.j]) - e.d0;
    dr = reference_conformation[e.i].distance_to(reference_conformation[e.j]) - e.d0;
    en += e.k * dn * dn;
    en -= e.k * dr * dr;
  }

  return en;
}

/// @todo Find smallest and largest  MMBondEnergy index and set flag only for these
double MMBondEnergy::delta_energy(CartesianAtoms &reference_conformation, core::index4 atom_from, core::index4 atom_to,
    CartesianAtoms &new_conformation) {
  double dn = 0, dr = 0, en = 0;
  for (auto atm = atom_from; atm <= atom_to; ++atm) {
    for (auto i = bonds_for_atom_[atm].cbegin(); i != bonds_for_atom_[atm].cbegin(); ++i) {
      MMBondTerm &e = all_bonds_[*i];
      if (e.calc_flag == 0) {
        dn = new_conformation[e.i].distance_to(new_conformation[e.j]) - e.d0;
        dr = reference_conformation[e.i].distance_to(reference_conformation[e.j]) - e.d0;
        en += e.k * dn * dn;
        en -= e.k * dr * dr;
        e.calc_flag = 1;
      }
    }
  }
  for (auto term: all_bonds_) term.calc_flag = 0;
  return en;
}


double MMBondEnergy::calculate(const MMBondTerm &bond) {

  double d = the_system[bond.i].distance_to(the_system[bond.j]) - bond.d0;
  d *= d * bond.k;
  return d;
}


/** @brief Creates object that evaluates energy of all bonds in a biomolecular chain.
*
* This constructor assumes the derived class will create the list of all bonds of a system
* @param system - bonds energy of this system will be evaluated
*/
MMBondEnergy::MMBondEnergy(const CartesianAtoms &system) : ByAtomEnergy("MMBondEnergy"), the_system(system), logs("MMBondEnergy") {

  bonds_for_atom_.resize(system.CartesianAtoms::n_atoms);
}


void MMBondEnergy::create_bonds(const MMBondedParameters &bond_manager) {

  MMAtomTyping_SP parameters = std::dynamic_pointer_cast<MMAtomTyping>(the_system.atom_typing);

  double tolerance = 0.2;                   // the tolerance parameter defines permissible deviation of bond length
  core::index1 type_i, type_j;              // CHANGE TO INDEX1 AT REFACTORING
  for (core::index2 i = 0; i < the_system.n_atoms; ++i) {
    type_i = the_system[i].atom_type;
    // Here we try to guess the element type for each atom based on the mass of the atom, as stored in the respective MMAtomType structure
    const core::index1 ei = parameters->element(type_i);
    for (core::index2 j = i + 1; j < the_system.n_atoms; ++j) {
      // Now let's find the type of atom j
      type_j = the_system[j].atom_type;
      const core::index1 ej = parameters->element(type_j);
      double R2 = core::chemical::bond_length(ei, ej) +
                  tolerance;  // the maximum distance to create bond between two atoms is based on the actual bond length
      R2 *= R2;
      double r2 = the_system[j].closest_distance_square_to(the_system[i], R2);
      if (r2 < R2) {
        double r = sqrt(r2);
        try {
          const MMBondType &bond_type = bond_manager.find_bond(type_i, type_j);
          if ((r < bond_type.length() - tolerance) || (r > bond_type.length() + tolerance)) continue;
          all_bonds_.emplace_back(i, j, bond_type.constant(), bond_type.d0());
          print_bond_msg(all_bonds_.back());
        } catch (std::invalid_argument &e) {
          print_missing_bond_msg(i, j);
          throw e;
        }
      }
    }
  }

  CompareMMBondTerms comparator(*this);
  std::sort(all_bonds_.begin(), all_bonds_.end(), comparator);

  for (core::index4 ie = 0; ie < all_bonds_.size(); ++ie) {
    const MMBondTerm &e = all_bonds_[ie];
    bonds_for_atom_[e.i].push_back(ie);
    bonds_for_atom_[e.j].push_back(ie);

  }
}

std::string MMBondEnergy::nice_atom_info(core::index4 atom_index) const {

  const systems::CartesianChains &c = static_cast<const systems::CartesianChains &>(the_system);

  return utils::string_format("%4d %4s of %3d %3s", atom_index,
      the_system.atom_typing->atom_internal_name(the_system[atom_index]).c_str(),
      c.residue_for_atom(atom_index),
      the_system.atom_typing->residue_internal_name(the_system[atom_index].residue_type).c_str());
}

void MMBondEnergy::print_bond_msg(const MMBondTerm &b) {

  double r = the_system[b.i].distance_to(the_system[b.j]);
  double e = b.k * (r - b.d0) * (r - b.d0);
  logs << utils::LogLevel::FINER << utils::string_format("New bond : %s and %s d= %7.2f e=%6.2f\n",
      nice_atom_info(b.i).c_str(), nice_atom_info(b.j).c_str(), r, e);
}

void MMBondEnergy::print_missing_bond_msg(const core::index4 i, const core::index4 j) {

  double r = the_system[i].distance_to(the_system[j]);
  logs << utils::LogLevel::SEVERE << utils::string_format("Can't create a bond between atoms : %s and %s d= %5.3f\n",
      nice_atom_info(i).c_str(), nice_atom_info(j).c_str(), r);
}


}
}
}


