#include <core/index.hh>
#include <core/data/structural/Structure.fwd.hh>
#include <core/data/structural/PdbAtom.hh>

#include <utils/io_utils.hh>

#include <simulations/forcefields/mm/MMResidueType.hh>
#include <simulations/forcefields/mm/MMAtomTyping.hh>

namespace simulations {
namespace forcefields {
namespace mm {

utils::Logger MMAtomTyping::mm_atom_typing_logs("MMAtomTyping");

MMAtomTyping::MMAtomTyping(const std::map<std::string, MMNonBondedParameters> & atom_params,
     std::map<std::string, MMResidueType> &residue_topology) {

  core::index1 at_idx = 255;
  // --- register all atom types for that MM force field
  std::vector<std::string> atom_type_names(atom_params.size(), "");
  // --- put the atom types in the correct order; it is assuemed that IDs assigned to MMNonBondedParameters start from 0
  for(const auto & at:atom_params)   // --- copy names by indexes assigned by the atom typing order
    atom_type_names[at.second.id()] = at.first;

  for(const std::string & atm_type_name:atom_type_names) {
    const MMNonBondedParameters & atm_type =  atom_params.at(atm_type_name);

    at_idx = register_atom_type(atm_type_name);
    sigma_.push_back(atm_type.sigma());
    epsilon_.push_back(atm_type.epsilon());
    mass_.push_back(atm_type.mass());
    element_.push_back(core::chemical::AtomicElement::by_mass(atm_type.mass()).z);
    mm_atom_typing_logs << utils::LogLevel::FINE << "Atom type registered: " << atm_type_name << " at index " << int(at_idx) << "\n";
  }
  for( auto & type:residue_topology) {
    // ---------- register a new residue type if needed
     std::string patched_residue_name = type.first;
      bool hetero_flag=false;
      if (patched_residue_name.size()<3){
          hetero_flag=true;
          patched_residue_name.insert(0," ");
          if (patched_residue_name.size()<3) patched_residue_name.insert(0," "); //if residue name was size 1 add 2 spaces
      }

    if(std::find(known_residues_.begin(), known_residues_.end(), patched_residue_name)==known_residues_.end()) {
      mm_atom_typing_logs << utils::LogLevel::FINE << "Residue type registered: " << patched_residue_name
                          << " at index " << int(known_residues_.size()) << "\n";
      known_residues_.push_back(patched_residue_name);
    } else {
      mm_atom_typing_logs << utils::LogLevel::INFO << "Residue type : " << patched_residue_name << " seen again in the topology data\n";
    }
    core::index1 r_idx = residue_type(patched_residue_name);
     std::vector<MMResidueAtom> & atms_in_res = type.second.atoms;
    for(MMResidueAtom & atype:atms_in_res) {
      at_idx = AtomTypes::atom_type(atype.internal_atom_name);
        atype.pdb_atom_name = core::data::structural::format_pdb_atom_name(atype.pdb_atom_name,hetero_flag);
        internal_types_.insert({extended_atom_name(atype.pdb_atom_name, patched_residue_name), at_idx});
      charge_.insert({charge_index(at_idx, r_idx), atype.charge});

      std::string key = extended_atom_name(atype.pdb_atom_name, patched_residue_name);
      extended_name_to_ff_index_.insert({key, at_idx});

      core::index1 atm_idx_check = atom_type(atype.pdb_atom_name, type.first, systems::AtomTypingVariants::STANDARD);
      mm_atom_typing_logs << utils::LogLevel::FINE << utils::string_format("Atom %s from %s is typed %d %s\n",
          atype.pdb_atom_name.c_str(), patched_residue_name.c_str(), atm_idx_check,
          atom_internal_name(atm_idx_check).c_str());
    }
  }
}

bool MMAtomTyping::is_known_residue_type(const std::string & residue_name,
                           const systems::AtomTypingVariants variant) const {

  std::string patched_name = patch_residue_name(residue_name, variant);
  return (std::find(known_residues_.begin(), known_residues_.end(), patched_name)!=known_residues_.end());
}

core::index1 MMAtomTyping::residue_type(const std::string & residue_name, const systems::AtomTypingVariants variant) const {
  return residue_type(patch_residue_name(residue_name, variant));
};

core::index1 MMAtomTyping::residue_type(const std::string & patched_residue_name) const {

  // --- first check if we already have that residue type registered
  const auto pos = std::find(known_residues_.begin(), known_residues_.end(), patched_residue_name);
  if (pos != known_residues_.end())
    return std::distance(known_residues_.begin(), pos);
  else {
    mm_atom_typing_logs << utils::LogLevel::CRITICAL << "Can't assign residue type for: " << patched_residue_name << "\n";
    throw std::out_of_range("unknown residue type: " + patched_residue_name);
  }
}

core::index1 MMAtomTyping::atom_type(const std::string &pdb_atom_name, const std::string &residue_name,
      const systems::AtomTypingVariants variant) const {

  try {
    std::string key = extended_atom_name(pdb_atom_name, residue_name, variant);
    return internal_types_.at(key);
  } catch (const std::out_of_range & e) {
    mm_atom_typing_logs << utils::LogLevel::CRITICAL << "Can't assign atom type for: " << pdb_atom_name << " " << residue_name << " " << variant << "\n";
    throw e;
  }
}

core::index1 MMAtomTyping::atom_type(const core::data::structural::PdbAtom &a) const {

  const core::data::structural::Residue & r = *a.owner();
  systems::AtomTypingVariants termini = residue_patch(*a.owner());

  return atom_type(a.atom_name(), r.residue_type().code3, termini);
}


double MMAtomTyping::charge(const core::index1 atom_type, const core::index1 residue_type) const {
  return charge_.at(charge_index(atom_type, residue_type));
}


std::string MMAtomTyping::extended_atom_name(const std::string & pdb_atom_name, const std::string & residue_name,
                               const systems::AtomTypingVariants variant) const {
  return extended_atom_name(pdb_atom_name, patch_residue_name(residue_name, variant));
}

std::string MMAtomTyping::extended_atom_name(const std::string & pdb_atom_name,
                                             const std::string & patched_residue_name) const {
  return patched_residue_name + ":" + pdb_atom_name;
}


bool MMAtomTyping::is_known_atom_type(const std::string &atom_name, const std::string &residue_name,
                                      const systems::AtomTypingVariants variant) const {
  std::string key = extended_atom_name(atom_name, residue_name, variant);
  return extended_name_to_ff_index_.find(key) != extended_name_to_ff_index_.cend();
}

bool MMAtomTyping::is_known_atom_type(const core::data::structural::PdbAtom &a) const {
  const core::data::structural::Residue & r = *a.owner();
  systems::AtomTypingVariants termini = residue_patch(r);

  return is_known_atom_type(a.atom_name(), r.residue_type().code3, termini);
}


}
}
}
