#include <vector>
#include <tuple>

#include <core/index.hh>
#include <core/calc/structural/angles.hh>
#include <core/chemical/molecule_utils.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/mm/MMDihedralEnergy.hh>


namespace simulations {
namespace forcefields {
namespace mm {

using namespace core::data::basic;


double MMDihedralEnergy::energy(CartesianAtoms & conformation){
    double angle = 0, en = 0;
    for (const MMDihedralTerm &e : dihedral_terms_) {
        angle = core::calc::structural::evaluate_dihedral_angle(conformation[e.i], conformation[e.j], conformation[e.k], conformation[e.l]);
        en += e.Vn * (1.0 + cos(e.nn * angle - e.theta0));
    }

    return en;
}
double MMDihedralEnergy::delta_energy(CartesianAtoms& referance_conformation, core::index4 which_atom,CartesianAtoms& new_conformation){
    double nangle = 0, pangle = 0, en = 0;

    for (auto i = angles_for_atom_[which_atom].cbegin();i!=angles_for_atom_[which_atom].cbegin(); ++i) {
        const MMDihedralTerm &e = dihedral_terms_[*i];
        pangle = core::calc::structural::evaluate_dihedral_angle(referance_conformation[e.i], referance_conformation[e.j], referance_conformation[e.k], referance_conformation[e.l]);
        nangle = core::calc::structural::evaluate_dihedral_angle(new_conformation[e.i], new_conformation[e.j], new_conformation[e.k], new_conformation[e.l]);
        en += e.Vn * (1.0 + cos(e.nn * nangle - e.theta0));
        en -= e.Vn * (1.0 + cos(e.nn * pangle - e.theta0));
    }
    return en;
}

/// @todo Find smallest and largest  MMDihedralTerm index and set flag only for these
double MMDihedralEnergy::delta_energy(CartesianAtoms& referance_conformation, core::index4 atom_from,core::index4 atom_to,CartesianAtoms& new_conformation){
    double nangle = 0, pangle = 0, en = 0;
    for (auto atm = atom_from;atm<=atom_to;++atm) {
        for (auto i = angles_for_atom_[atm].cbegin(); i != angles_for_atom_[atm].cbegin(); ++i) {
            MMDihedralTerm &e = dihedral_terms_[*i];
            if (e.calc_flag==0) {
                pangle = core::calc::structural::evaluate_dihedral_angle(referance_conformation[e.i],
                                                                         referance_conformation[e.j],
                                                                         referance_conformation[e.k],
                                                                         referance_conformation[e.l]);
                nangle = core::calc::structural::evaluate_dihedral_angle(new_conformation[e.i],
                                                                         new_conformation[e.j],
                                                                         new_conformation[e.k],
                                                                         new_conformation[e.l]);

                en += e.Vn * (1.0 + cos(e.nn * nangle - e.theta0));
                en -= e.Vn * (1.0 + cos(e.nn * pangle - e.theta0));
                e.calc_flag = 1;
            }
        }
    }
    for (auto term:dihedral_terms_) term.calc_flag=0;

    return en;
}
double MMDihedralEnergy::energy(CartesianAtoms& conformation, core::index4 which_atom){
    double angle = 0, en = 0;
    for (auto i = angles_for_atom_[which_atom].cbegin();i!=angles_for_atom_[which_atom].cbegin(); ++i) {
        const MMDihedralTerm &e = dihedral_terms_[*i];
        angle = core::calc::structural::evaluate_dihedral_angle(conformation[e.i], conformation[e.j], conformation[e.k], conformation[e.l]);
        en += e.Vn * (1.0 + cos(e.nn * angle - e.theta0));
    }
    return en;
}

void MMDihedralEnergy::create_dihedral_angles(const MMBondEnergy &bond_energy, const MMBondedParameters &bonded_params) {

const auto &bonds = bond_energy.get_bonds();
core::chemical::Molecule<AtomIndexType> molecule = create_molecular_graph(the_system, bonds);

  // ---------- Constructor check if this cast is allowed!
  const systems::CartesianChains &c = static_cast<const systems::CartesianChains &>(the_system);

//--Find all torsion angles in molecule and prepare properties for MMDihedralType objects
std::vector<std::tuple<AtomIndexType, AtomIndexType, AtomIndexType, AtomIndexType>> dihedrals;
find_torsion_angles(molecule, dihedrals);
for (const auto &p : dihedrals) {
  AtomIndexType ai = std::get<0>(p);
  AtomIndexType aj = std::get<1>(p);
  AtomIndexType ak = std::get<2>(p);
  AtomIndexType al = std::get<3>(p);
  try {
    const MMDihedralType &ptype = bonded_params.find_dihedral(ai.atom_type, aj.atom_type, ak.atom_type,
      al.atom_type);
    dihedral_terms_.emplace_back(ai.atom_index, aj.atom_index, ak.atom_index, al.atom_index, ptype.Vn(),
      ptype.theta0(), ptype.n());
    print_dihedral_msg(dihedral_terms_.back());
  } catch(std::invalid_argument e) {
    core::index2 ri = c.residue_for_atom(ai.atom_index);
    core::index2 rl = c.residue_for_atom(al.atom_index);
    logs << utils::LogLevel::CRITICAL<<"Missing planar angle definition for residue(s): "
         <<ri<<" "<<core::chemical::Monomer::get(core::index2(the_system.operator[](ai.atom_index).residue_type)).code3<<" - "
         <<rl<<" "<<core::chemical::Monomer::get(core::index2(the_system.operator[](al.atom_index).residue_type)).code3<<"\n";
    throw e;
  }
}

CompareMMDihedralTerms comparator(*this);
std::sort(dihedral_terms_.begin(), dihedral_terms_.end(), comparator);

for(core::index4 ie=0;ie<dihedral_terms_.size();++ie) {
  const MMDihedralTerm & e = dihedral_terms_[ie];
    angles_for_atom_[e.i].push_back(ie);
    angles_for_atom_[e.j].push_back(ie);
    angles_for_atom_[e.k].push_back(ie);
    angles_for_atom_[e.l].push_back(ie);

}
  if(logs.is_logable(utils::LogLevel::FINER)) {
      for(core::index2 ir=0;ir<angles_for_atom_.size();++ir)
          logs << utils::LogLevel::FINER <<utils::string_format("Atom %4d involved in dihedrals: %5d - %5d\n",ir,angles_for_atom_[ir][0],*(angles_for_atom_[ir].cend()));
  }
}

std::string MMDihedralEnergy::nice_atom_info(core::index4 atom_index) const {

  // ---------- Constructor check if this cast is allowed!
  const systems::CartesianChains &c = static_cast<const systems::CartesianChains &>(the_system);

  return utils::string_format("%4d %4s of %3d %3s", atom_index,
      the_system.atom_typing->atom_internal_name(the_system[atom_index]).c_str(), c.residue_for_atom(atom_index),
      core::chemical::Monomer::get(core::index2(the_system[atom_index].residue_type)).code3.c_str());
}

void MMDihedralEnergy::print_dihedral_msg(const MMDihedralTerm & dihedral_term) {

const core::index4 i = dihedral_term.i;
const core::index4 j = dihedral_term.j;
const core::index4 k = dihedral_term.k;
const core::index4 l = dihedral_term.l;
double angle = core::calc::structural::evaluate_dihedral_angle(the_system[i], the_system[j], the_system[k], the_system[l]);
double e = dihedral_term.Vn * (1.0 + cos(dihedral_term.nn * angle - dihedral_term.theta0));
logs << utils::LogLevel::FINER << utils::string_format("New dihedral : %s and %s and %s and %s t= %7.2f e=%6.2f\n",
  nice_atom_info(i).c_str(), nice_atom_info(j).c_str(), nice_atom_info(k).c_str(), nice_atom_info(l).c_str(),
  core::calc::structural::to_degrees(angle), e);
}

void MMDihedralEnergy::print_missing_dihedral_msg(const core::index4 i, const core::index4 j, const core::index4 k, const core::index4 l) {

logs << utils::LogLevel::SEVERE <<utils::string_format("Can't create a dihedral angle for atoms : %s and %s and %s and %s\n",
  nice_atom_info(i).c_str(), nice_atom_info(j).c_str(), nice_atom_info(k).c_str(), nice_atom_info(l).c_str());
}

}
}
}

