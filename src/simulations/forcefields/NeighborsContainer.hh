#ifndef SIMULATIONS_FORCEFIELDS_NeighborsContainer_HH
#define SIMULATIONS_FORCEFIELDS_NeighborsContainer_HH

#include <memory> // for shared pointers
#include <iterator> // for iterators
#include <vector>

#include <core/index.hh>

namespace simulations {
namespace forcefields {

/** @brief Container to store neighbors.
 *
 */

class NeighborsContainer {
public:

  /** @brief Initializes internal data structure and creates list of neighbors for every atom
   *
   */
  NeighborsContainer() {}

  /// Virtual destructor
  ~NeighborsContainer() {}

  /// Returns iterator pointing at the first neighbor of i-th object
  std::vector<core::index4>::const_iterator cbegin(const core::index4 i) { return neighbors[i].cbegin(); }

  /// Returns iterator pointing behind the last neighbor of i-th object
  std::vector<core::index4>::const_iterator cend(const core::index4 i) { return neighbors[i].cend(); }

  void add_neighbor(core::index4  i, core::index4 j){
      neighbors[i].push_back(j);
      neighbors[j].push_back(i);
  }

private:
  std::vector<std::vector<core::index4>> neighbors;

};
}
}

#endif
