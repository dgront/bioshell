/** @file TotalEnergyByAtom.hh
 * @brief Provides TotalEnergyByAtom type
 */
#ifndef SIMULATIONS_GENERIC_FF_TotalEnergyByAtom_HH
#define SIMULATIONS_GENERIC_FF_TotalEnergyByAtom_HH

#include <core/index.hh>

#include <utils/Logger.hh>

#include <simulations/forcefields/TotalEnergy.hh>
#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/systems/CartesianAtoms.hh>

namespace simulations {
namespace forcefields {

/** @brief Total energy based on <code>ByAtomEnergy</code> terms.
 * This class itself also implements ByAtomEnergy interface, so one can calculate
 * weighted combination of energy terms for a given residue or a chunk of a modelled chain
 */
class TotalEnergyByAtom : public TotalEnergy, public virtual ByAtomEnergy {
public:

  /// Empty c'tor
  TotalEnergyByAtom() : ByAtomEnergy("TotalEnergyByAtom"), logger("TotalEnergyByAtom"){}

  /// Virtual destructor
  ~TotalEnergyByAtom() override = default;

  /// The returned string is the header line for energy components table (printed by ostream operator)
  std::string header_string() const override;

  double energy(systems::CartesianAtoms &conformation) override;

  double energy(systems::CartesianAtoms &conformation, const core::index4 which_atom) override;

   double delta_energy(systems::CartesianAtoms &reference_conformation, const core::index4 which_atom,
      systems::CartesianAtoms &new_conformation) override;

   double delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 atom_from,
      const core::index4 atom_to, systems::CartesianAtoms & new_conformation) override;

private:
  utils::Logger logger;
};

} // ~ simulations
} // ~ ff
#endif
