#ifndef SIMULATIONS_FORCEFIELDS_ByAtomEnergy_HH
#define SIMULATIONS_FORCEFIELDS_ByAtomEnergy_HH

#include <string>
#include <memory>
#include <core/index.hh>
#include <simulations/forcefields/Energy.hh>
#include <simulations/systems/CartesianAtoms.hh>

namespace simulations {
namespace forcefields {

/** @brief Base class for an energy function that may be decomposed into per-atom interactions
 */
class ByAtomEnergy : public Energy {
public:

  using Energy::energy;

  ByAtomEnergy() : Energy() {}

  /** @brief Constructor sets the name of a derived energy term
   * @param name - name of an energy to be printed on screen when reporting energy values
   */
  ByAtomEnergy(const std::string & name) : Energy(name) {}

  /** @brief Calculate energy contribution due to a single atom.
   * @param conformation - conformation to assess
   * @param which_atom - index of an atom
   * @return energy value
   */
  virtual double energy(systems::CartesianAtoms & conformation, const core::index4 which_atom) = 0;

  /** @brief Calculate the energy difference due to a single atom.
   * The returned energy difference will be evaluated based on the assumption that only @code which_atom @endcode
   * differs between the two conformations
   * @param reference_conformation - initial conformation to assess
   * @param which_atom - index of an atom
   * @param new_conformation - the new conformation after a MC move to assess
   * @return energy difference evaluated as @code E(new) - E(reference) @endcode
   */
  virtual double delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 which_atom,
      systems::CartesianAtoms & new_conformation) = 0;

  /** @brief Calculate the energy difference due to a contiguous range of atoms.
   * @param reference_conformation - initial conformation to assess
   * @param atom_from - index of the first atom of the moved chunk
   * @param atom_to  - index of the last atom of the moved chunk
   * @param new_conformation - the new conformation after a MC move to assess
   * @return energy difference evaluated as @code E(new) - E(reference) @endcode
   */
  virtual double delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 atom_from,
      const core::index4 atom_to, systems::CartesianAtoms & new_conformation) = 0;

  /// This virtual destructor is empty
  ~ByAtomEnergy() override = default;
};

typedef typename std::shared_ptr<ByAtomEnergy> ByAtomEnergy_SP;

} // ~ simulations
} // ~ ff
#endif
