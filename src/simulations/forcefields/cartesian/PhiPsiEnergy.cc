#include <simulations/forcefields/cartesian/PhiPsiEnergy.hh>
#include <set>
#include <simulations/systems/CartesianChains.hh>

namespace simulations {
namespace forcefields {
namespace cartesian {

PhiPsiEnergy::PhiPsiEnergy(const systems::CartesianChains & system, const std::vector<std::string> & params) :
        PhiPsiEnergy(system, params[0], (params.size() > 1) ? utils::from_string<double>(params[1]) : 0.01) {
    // check if params[1] contains letters
    if (params.size() > 1) {
        for (size_t i = 0; i < params[1].size(); i++) {
            const char c = params[1][i];
            if (c >= 'A' && c <= 'Z') throw std::invalid_argument("Wrong pseudocounts probability: " + params[1]);
            if (c >= 'a' && c <= 'z') throw std::invalid_argument("Wrong pseudocounts probability: " + params[1]);
        }
    }
}

PhiPsiEnergy::PhiPsiEnergy(const systems::CartesianChains & system, const std::string & listfile, double pseudocounts ) :
        MeanFieldDistribution2D((listfile != "-") ? listfile : "forcefield/ramachandran/ramachandranFF.cfg",true,
                                pseudocounts),  ByAtomEnergy("PhiPsiEnergy"),n_pos(system.count_residues()), index_NCaCO(system.count_residues() * 4), logger("PhiPsiEnergy") {

    using namespace core::data::structural;

    for (core::index2 ri = 0; ri < system.count_residues(); ++ri) {
        try {
            index_NCaCO[ri * 4] = system.find_atom_index(ri, " N  ");
            index_NCaCO[ri * 4 + 1] = system.find_atom_index(ri, " CA ");
            index_NCaCO[ri * 4 + 2] = system.find_atom_index(ri, " C  ");
            index_NCaCO[ri * 4 + 3] = system.find_atom_index(ri, " O  ");
        } catch (std::invalid_argument &e) {
            logger << utils::LogLevel::SEVERE << "Can't find a backbone atom in a residue!";
            throw e;
        }
    }

    ff_for_sequence.resize(n_pos);
    std::string key = "__";
    for (int i = 1; i < n_pos - 1; i++) {
        key[0] = core::chemical::Monomer::get(core::index2(system[i].residue_type)).code1;
        key[1] = core::chemical::Monomer::get(core::index2(system[i+1].residue_type)).code1;
        if (key[1] != 'P') key[1] = 'x';
        if (!contains_distribution(key)) {
            throw std::runtime_error(missing_error_msg(key));
        }
        ff_for_sequence[i] = at(key);
        logger << utils::LogLevel::FINE << "assigning " << key << " component to position " << i << "\n";
    }
}

double PhiPsiEnergy::energy_by_residue(systems::CartesianAtoms& system,core::index4 which_residue){


    if (ff_for_sequence[which_residue] == nullptr) return 0;
        // compute phi
        core::index4 i_ref = which_residue * 4;
        double phi = core::calc::structural::evaluate_dihedral_angle(system[index_NCaCO[i_ref - 2]], system[index_NCaCO[i_ref]],
                                                                     system[index_NCaCO[i_ref + 1]], system[index_NCaCO[i_ref + 2]]);
        // compute psi
        double psi = core::calc::structural::evaluate_dihedral_angle(system[index_NCaCO[i_ref]], system[index_NCaCO[i_ref + 1]],
                                                                     system[index_NCaCO[i_ref + 2]], system[index_NCaCO[i_ref + 4]]);
        // compute energy
 //   logger<<utils::LogLevel::INFO<<"Calculating for residue "<<which_residue<<" : "<<ff_for_sequence[which_residue]->operator ()(phi, psi)<<"\n";

    return ff_for_sequence[which_residue]->operator ()(phi, psi);
}

double PhiPsiEnergy::energy(systems::CartesianAtoms& system){
    auto cartesian_chains = dynamic_cast<systems::CartesianChains &>(system);
    double en=0;

    for (core::index4 resi=0;resi<cartesian_chains.count_residues();++resi)
        en+=energy_by_residue(system,resi);
    return en;
}
double PhiPsiEnergy::energy(systems::CartesianAtoms& system,core::index4 which_atom){

    auto cartesian_chains = dynamic_cast<systems::CartesianChains &>(system);
    core::index4 which_residue = cartesian_chains.residue_for_atom(which_atom);
    return energy_by_residue(system,which_residue);
}


double PhiPsiEnergy::delta_energy(systems::CartesianAtoms& old_conformation ,core::index4 which_atom,systems::CartesianAtoms& new_conformation){
    return energy(new_conformation,which_atom)-energy(old_conformation,which_atom);
}
double PhiPsiEnergy::delta_energy(systems::CartesianAtoms& old_conformation,core::index4 atom_from,core::index4 atom_to,systems::CartesianAtoms& new_conformation){
    double en=0;
    std::set<core::index2> to_calc{};
    auto cartesian_chains = dynamic_cast<systems::CartesianChains &>(new_conformation);
    for (auto i=atom_from;i<=atom_to;++i) {
        auto res = cartesian_chains.residue_for_atom(i);
        to_calc.insert(res);
    }
    for (auto i=to_calc.begin();i!=to_calc.end();++i) {
        auto atm = cartesian_chains.atoms_for_residue(*i).first_atom;
        en += delta_energy(old_conformation, atm, new_conformation);
    }
    return en;
}


std::string PhiPsiEnergy::missing_error_msg(const std::string & key) const {

    const std::vector<std::string> valid_keys = known_distributions();
    std::copy(valid_keys.cbegin(), valid_keys.cend(), std::ostream_iterator<std::string>(std::cerr, "\n"));
    std::string msg("Can't find the distribution: " + key + " \n\tValid keys are:");
    std::stringstream o;
    std::copy(valid_keys.cbegin(), valid_keys.cend(), std::ostream_iterator<std::string>(o));
    return msg + o.str() + " ... for a PhiPsiEnergy instance ";
}

const std::string PhiPsiEnergy::name_ = "PhiPsiEnergy";
}
}
}
