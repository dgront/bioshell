#ifndef SIMULATIONS_FORCEFIELDS_CARTESIAN_DSSPEnergy_HH
#define SIMULATIONS_FORCEFIELDS_CARTESIAN_DSSPEnergy_HH

#include <vector>

#include <core/calc/structural/interactions/BackboneHBondCollector.hh>
#include <utils/Logger.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/systems/CartesianChains.hh>
#include <core/data/basic/Vec3I.hh>

namespace simulations {
namespace forcefields {
namespace cartesian {

using namespace simulations::systems;
using namespace core::data::basic;

/** @brief Hydrogen bond energy function based on the DSSP formula.
 *
 */
class DSSPEnergy : public ByAtomEnergy {
public:

    /// The maximum distance between acceptor O and H atoms when a H-bond may exist
    const static double MAX_NO_DISTANCE;

    const static double DSSP_CONST;

    DSSPEnergy(const CartesianChains & system, const std::vector<std::string> & parameters) : DSSPEnergy (system) { }

    DSSPEnergy(const CartesianChains & system);

    virtual ~DSSPEnergy() {}

    double energy(CartesianAtoms& );
    double energy(CartesianAtoms&,core::index4);
    double delta_energy(CartesianAtoms&, core::index4,CartesianAtoms&);
    double delta_energy(CartesianAtoms &,core::index4 ,core::index4,CartesianAtoms&);

    virtual double energy_kernel(CartesianAtoms& conformation,const core::index2 the_moved_residue, const core::index2 the_other_residue);
    double energy_single_bond(CartesianAtoms&,const core::index2 residue_donor, const core::index2 residue_acceptor) ;

private:
    const CartesianChains & the_system;
    utils::Logger logger;
    std::vector<unsigned int> index_NCaCO;
    const static double MAX_NO_DISTANCE2;
};

}
}
}
#endif