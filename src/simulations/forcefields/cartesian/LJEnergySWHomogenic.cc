#include <simulations/forcefields/cartesian/LJEnergySWHomogenic.hh>
#include <utils/exit.hh>

namespace simulations {
namespace forcefields {
namespace cartesian {

LJEnergySWHomogenic::LJEnergySWHomogenic(const systems::CartesianAtoms &system, const double sigma, const double epsilon,
                                         const double nb_border_thick, const double cutoff_on, const double cutoff_off)
    : ByAtomEnergy("LJEnergySWHomogenic"), nbl(system.coordinates(), system.n_atoms,
        cutoff_off, nb_border_thick), lj_spline({0.0, 0.5, 1.0}, {0.0, 0.5, 1.0}) {


  sigma_ = sigma;
  epsilon_ = epsilon;
  sigma2 = sigma_ * sigma_;
  epsilon_four = epsilon * 4.0;
  set_cutoff(cutoff_on, cutoff_off);

  // ---------- initialize spline interpolation of the energy function
  std::vector<double> x, y;
  for (double d = 0.5; d <= CUTOFF_OFF2; d += 0.1) {
    x.push_back(d);
    double r2_s = sigma2 / d;
    double r6 = r2_s * r2_s * r2_s;
    double r12 = r6 * r6;
    y.push_back(epsilon_four * (r12 - r6) * sw(d, CUTOFF_ON2, CUTOFF_OFF2));
  }
  lj_spline = INTERPOLATOR(x, y);
}

void LJEnergySWHomogenic::set_cutoff(const double cutoff_on, double cutoff_off) {

  CUTOFF_OFF = cutoff_off;
  CUTOFF_ON = cutoff_on;
  CUTOFF_ON2 = CUTOFF_ON * CUTOFF_ON;
  CUTOFF_OFF2 = CUTOFF_OFF * CUTOFF_OFF;
}


double LJEnergySWHomogenic::energy(simulations::systems::CartesianAtoms & system) {

  double energy = 0.0;
  for (size_t which_atom = 0; which_atom < system.n_atoms; which_atom++) {
    const core::data::basic::Vec3I & o = system[which_atom];
    for (size_t i = 0; i < which_atom; i++) {
      double r = o.closest_delta_x(system[i]);
      double r2 = r * r;
      if (r2 > CUTOFF_OFF2) continue;
      r = o.closest_delta_y(system[i]);
      r2 += r * r;
      if (r2 > CUTOFF_OFF2) continue;
      r = o.closest_delta_z(system[i]);
      r2 += r * r;
      if (r2 > CUTOFF_OFF2) continue;

      double r2_s = sigma2 / r2;
      double r6 = r2_s * r2_s * r2_s;
      double r12 = r6 * r6;
      energy += epsilon_four * (r12 - r6) * sw(r2, CUTOFF_ON2, CUTOFF_OFF2);

    }
  }

  return energy;
}

double LJEnergySWHomogenic::delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 atom_from,
    const core::index4 atom_to, systems::CartesianAtoms & new_conformation) {

  utils::exit_with_error("LJEnergySWHomogenic.cc", 70,
      "delta_energy() by chunk has not been implemented; use single - atom mover instead");
}

double LJEnergySWHomogenic::calculate_by_atom_exact(simulations::systems::CartesianAtoms &system,
                                                    const core::index4 which_atom) {

  nbl.update(which_atom);

  double energy = 0.0;
  const core::data::basic::Vec3I &o = system[which_atom];
  for (core::index4 i = 0; i < system.n_atoms; ++i) {
    if (i == which_atom) continue;
    double r = o.closest_delta_x(system[i]);
    double r2 = r * r;
    r = o.closest_delta_y(system[i]);
    r2 += r * r;
    r = o.closest_delta_z(system[i]);
    r2 += r * r;
    if (r2 > CUTOFF_OFF2) continue;

    double r2_s = sigma2 / r2;
    double r6 = r2_s * r2_s * r2_s;
    double r12 = r6 * r6;
    energy += epsilon_four * (r12 - r6) * sw(r2, CUTOFF_ON2, CUTOFF_OFF2);
  }

  return energy;
}

} // ~ simulations
} // ~ cartesian
} // ~ ff
