#ifndef SIMULATIONS_FORCEFIELDS_CARTESIAN_PhiPsiEnergy_HH
#define SIMULATIONS_FORCEFIELDS_CARTESIAN_PhiPsiEnergy_HH

#include <cmath>

#include <iostream>
#include <memory>
#include <vector>
#include <map>
#include <stdexcept>

#include <core/index.hh>
#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/InterpolatePeriodic2D.hh>
#include <core/calc/structural/angles.hh>
#include <core/data/sequence/Sequence.hh>
#include <core/data/basic/Array2D.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/systems/CartesianChains.hh>
#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/forcefields/mf/MeanFieldDistribution2D.hh>

namespace simulations {
namespace forcefields {
namespace cartesian {

/** @brief Energy term based on \f$ \Phi, |Psi \f$ 2D probability distribution (Ramachandran map)
*/
class PhiPsiEnergy : public mf::MeanFieldDistribution2D, public ByAtomEnergy {
public:

/** @brief Creates Ramachandran force field
 *
 * @param system - the system to be scored
 * @param strctr - protein structure for which the system was created. It must be provided so the constructor
 *    can index backbone atoms properly
 * @param params - a vector of string parameters : two are required :
 *     - the list of files with Ramachandran distributions data (say "-" to use the interal BioShell data)
 *     - pseudocounts value, e.g. <code>"0.001"</code>
 */
PhiPsiEnergy(const systems::CartesianChains & system, const std::vector<std::string> & params) ;
/** @brief Creates Ramachandran force field
 *
 * @param system - the system to be scored
 * @param strctr - protein structure for which the system was created. It must be provided so the constructor
 *    can index backbone atoms properly
 * @param listfile - a file that lists files with Ramachandran distributions data (say "-" to use the interal BioShell data)
 * @param pseudocounts - pseudocounts value
 */
PhiPsiEnergy(const systems::CartesianChains & system, const std::string & listfile, double pseudocounts = 0.01) ;

/// Bare virtual destructor
virtual ~PhiPsiEnergy() {}

const std::string & name() const { return name_; }

inline double calculate_by_residue(const core::index2 which_residue,const double phi,const double psi) {
    return ff_for_sequence[which_residue]->operator ()(phi, psi);
}
double energy_by_residue(systems::CartesianAtoms& system,core::index4 which_residue);

virtual double energy(systems::CartesianAtoms&);

virtual double energy(systems::CartesianAtoms &,core::index4);

virtual double delta_energy(systems::CartesianAtoms&,core::index4,systems::CartesianAtoms&);

virtual double delta_energy(systems::CartesianAtoms&,core::index4,core::index4,systems::CartesianAtoms&);

protected:
    const core::index2 n_pos; ///< The number of residues in the system
    std::vector<core::index4> index_NCaCO; ///< Indexes of N, CA, C and O atoms of every residue

private:
    static const std::string name_;
    std::vector<mf::EnergyComponent2D_SP> ff_for_sequence;
    utils::Logger logger;
    
    std::string missing_error_msg(const std::string & key) const ;
};

}
}
}

#endif
