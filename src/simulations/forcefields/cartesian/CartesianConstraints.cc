#include <simulations/forcefields/cartesian/CartesianConstraints.hh>

namespace simulations {
namespace forcefields {
namespace cartesian {

CartesianConstraints::CartesianConstraints(const CartesianAtoms &system, const double grace_radius, const double steep) :
    grace_radius2_(grace_radius*grace_radius), steep_(steep), reference_(system) {}

double CartesianConstraints::energy(CartesianAtoms &conformation, const core::index4 which_atom) {
  double d2 = conformation[which_atom].closest_distance_square_to(reference_[which_atom]);
  if (d2 < grace_radius2_) return 0.0;
  return (d2 - grace_radius2_) * steep_;
}

double CartesianConstraints::energy(CartesianAtoms &conformation) {
  double en = 0.0;
  for(core::index4 i=0;i<conformation.n_atoms;i++)
    en += energy(conformation,i);

  return en;
}

double CartesianConstraints::delta_energy(CartesianAtoms &reference_conformation, const core::index4 which_atom,
    CartesianAtoms &new_conformation) {

  return energy(new_conformation,which_atom) - energy(reference_conformation,which_atom);
}

double CartesianConstraints::delta_energy(CartesianAtoms &reference_conformation, const core::index4 atom_from, const core::index4 atom_to,
    CartesianAtoms &new_conformation) {
  double en = 0.0;
  for (core::index4 i = atom_from; i <= atom_to; i++)
    en += energy(new_conformation, i) - energy(reference_conformation, i);

  return en;
}
}
}
}