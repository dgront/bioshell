#ifndef SIMULATIONS_FORCEFIELDS_CARTESIAN_CartesianConstraints_HH
#define SIMULATIONS_FORCEFIELDS_CARTESIAN_CartesianConstraints_HH

#include <core/index.hh>
#include <utils/string_utils.hh>
#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/systems/CartesianAtoms.hh>

namespace simulations {
namespace forcefields {
namespace cartesian {

using namespace simulations::systems;

/** @brief Contact potential of a square-well shape and excluded volume included.
 */
class CartesianConstraints : public ByAtomEnergy {
public:

  CartesianConstraints(const CartesianAtoms &system, const std::vector<std::string> &parameters) :
      CartesianConstraints(system, utils::to_double(parameters[0].c_str()), utils::to_double(parameters[1].c_str())) {}

  CartesianConstraints(const CartesianAtoms &system, const double grace_radius, const double steep = 100.0);

  ~CartesianConstraints() override = default;

  double energy(CartesianAtoms &conformation) override;

  double energy(CartesianAtoms &conformation, const core::index4 which_atom) override;

  double delta_energy(CartesianAtoms &reference_conformation, const core::index4 which_atom,
      CartesianAtoms &new_conformation) override;

  double delta_energy(CartesianAtoms &reference_conformation, const core::index4 atom_from, const core::index4 atom_to,
      CartesianAtoms &new_conformation) override;

private:
  const double grace_radius2_;
  const double steep_;
  const CartesianAtoms reference_ ;
};

}
}
}

#endif
