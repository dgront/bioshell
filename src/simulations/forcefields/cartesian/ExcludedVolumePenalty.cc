#include <simulations/forcefields/cartesian/ExcludedVolumePenalty.hh>

namespace simulations {
namespace forcefields {
namespace cartesian {

using simulations::systems::CartesianAtoms;

ExcludedVolumePenalty::ExcludedVolumePenalty(const CartesianAtoms &system, const std::vector<std::string> &parameters) :
    ExcludedVolumePenalty(system, utils::from_string<double>(parameters[0]), utils::from_string<double>(parameters[1])) {}

ExcludedVolumePenalty::ExcludedVolumePenalty(const CartesianAtoms &system, const double distance,
    const double penalty_value): ByAtomEnergy("ExcludedVolumePenalty"), nbl(system.coordinates(), system.n_atoms, distance, 4) {

  repulsion_distance = distance;
  excluded_volume_distance2 = distance * distance;
  repulsion_energy = penalty_value;
  auto ptr = std::static_pointer_cast<NeighborListRules>(std::make_shared<ExcludeSequenceNeighbors>(4, nbl));
  nbl.neighbor_rules(ptr);
}

double ExcludedVolumePenalty::energy(CartesianAtoms &conformation) {

  double total = 0;
  nbl.update_all();
  for (int which_atom = 0; which_atom < conformation.n_atoms; which_atom++) {
    total += energy(conformation, which_atom);
  }
  return total / 2.0;
}

double ExcludedVolumePenalty::energy(CartesianAtoms &conformation, const core::index4 which_atom) {

  nbl.update(which_atom);
  double total = 0;
  const core::data::basic::Vec3I &o = conformation[which_atom];
  for (auto it = nbl.cbegin(which_atom); it != nbl.cend(which_atom); ++it) {
    double r2 = o.closest_distance_square_to(conformation[*it], excluded_volume_distance2);
    if (r2 < excluded_volume_distance2)
      total += repulsion_energy;
  }

  return total;
}

double ExcludedVolumePenalty::delta_energy(CartesianAtoms &reference_conformation, const core::index4 which_atom,
    CartesianAtoms &new_conformation) {

  return energy(new_conformation, which_atom) - energy(reference_conformation, which_atom);
}

double ExcludedVolumePenalty::delta_energy(CartesianAtoms &reference_conformation, const core::index4 atom_from,
    const core::index4 atom_to, CartesianAtoms &new_conformation) {

  double total = 0;
  for (int which_atom = atom_from; which_atom <= atom_to; which_atom++) {
    total += delta_energy(reference_conformation, which_atom, new_conformation);
  }

  return total;
}


}
}
}
