#ifndef SIMULATIONS_FORCEFIELDS_CARTESIAN_LJEnergySWHomogenic_HH
#define SIMULATIONS_FORCEFIELDS_CARTESIAN_LJEnergySWHomogenic_HH

#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/Interpolate1D.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/forcefields/NeighborList.hh>
#include <simulations/systems/CartesianAtoms.hh>

namespace simulations {
namespace forcefields {
namespace cartesian {

/** @brief Lennard-Jones potential for a system of identical atoms, e.g. argon.
 *
 * Switching function is used to turn off the interaction after <code>CUTOFF_OFF</code> Angstroms. The switching
 * function has a sigmoidal shape and ranges from <code>CUTOFF_ON</code> to <code>CUTOFF_OFF</code>
 *
 */
class LJEnergySWHomogenic : public ByAtomEnergy {
  typedef typename core::calc::numeric::Interpolate1D<std::vector<double>, double, core::calc::numeric::CatmullRomInterpolator<double>> INTERPOLATOR;
public:

  /** Initiates an energy function for a given system
   *
   * @param system - a system of identical atoms
   * @param non_bonded_neighbors - list of neighbors used to calculate LJ energy
   * @param sigma - sigma parameter for LJ interactions
   * @param epsilon - epsilon parameter for LJ interactions
   */
  LJEnergySWHomogenic(const systems::CartesianAtoms &system, const double sigma, const double epsilon,
                      const double nb_border_thick = 6.0, const double cutoff_on = 7.0, const double cutoff_off = 9.0);

  /// reference to the nonbonded list this energy function is using
  NeighborList & non_bonded_neighbors() { return nbl; }

  /** @brief Sets cutoff for thi LJ energy function
   * @param cutoff_on - the distance where a switching function starts modyfying the LJ potential
   * @param cutoff_off  - the distance where a switching function stops modyfying the LJ potential; LJ energy is 0 after that distance
   */
  void set_cutoff(const double cutoff_on, double cutoff_off);

  virtual double energy(simulations::systems::CartesianAtoms & system, const core::index4 which_atom) final {

    nbl.update(which_atom);

    double energy = 0.0;
    const core::data::basic::Vec3I & o = system[which_atom];
    for (auto it = nbl.cbegin(which_atom); it != nbl.cend(which_atom); ++it) {
      double r2 = o.closest_distance_square_to(system[*it], CUTOFF_OFF2);

      energy += lj_spline(r2);    // ---------- use spline to evaluate the energy
    }

    return energy;
  }

  virtual double delta_energy(simulations::systems::CartesianAtoms &reference_conformation,
                              const core::index4 which_atom, simulations::systems::CartesianAtoms &new_conformation)  final {

    nbl.update(which_atom);

    double delta_en = 0.0;
    const core::data::basic::Vec3I & o_ref = reference_conformation[which_atom];
    const core::data::basic::Vec3I & o_new = new_conformation[which_atom];
    for (auto it = nbl.cbegin(which_atom); it != nbl.cend(which_atom); ++it) {
      double r2b = o_ref.closest_distance_square_to(reference_conformation[*it], CUTOFF_OFF2);
      delta_en -= lj_spline(r2b);    // ---------- use spline to evaluate the energy
      double r2a = o_new.closest_distance_square_to(new_conformation[*it], CUTOFF_OFF2);
      delta_en += lj_spline(r2a);
    }

    return delta_en;
  }

  double delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 atom_from,
      const core::index4 atom_to, systems::CartesianAtoms & new_conformation) override;

  virtual double energy(simulations::systems::CartesianAtoms & system) final;

  double calculate_by_atom_exact(simulations::systems::CartesianAtoms & system, const core::index4 which_atom);

private:
  double sigma_;
  double epsilon_;
  double epsilon_four;
  double sigma2;
  double CUTOFF_ON = 7.0;
  double CUTOFF_OFF = 9.0;
  double CUTOFF_ON2;
  double CUTOFF_OFF2;
  NeighborList nbl;

  INTERPOLATOR lj_spline;

  inline double sw(const double x, const double xon, const double xoff) const {

    if (x <= xon) {
      return 1.0;
    } else if (x > xon && x <= xoff) {
      double d = (xoff - xon);
      double d3 = d * d * d;
      double w = (xoff - x) * (xoff - x);
      w *= (xoff + 2 * x - 3 * xon);
      w /= d3;
      return w;
    } else {
      return 0.0;
    }
  }

};


} // ~ simulations
} // ~ cartesian
} // ~ ff
#endif
