#ifndef SIMULATIONS_FORCEFIELDS_CARTESIAN_CAHydrogenBond_HH
#define SIMULATIONS_FORCEFIELDS_CARTESIAN_CAHydrogenBond_HH

#include <core/index.hh>
#include <core/data/basic/Array2D.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/systems/CartesianChains.hh>
#include <simulations/forcefields/NeighborList.hh>

namespace simulations {
namespace forcefields {
namespace cartesian {

using namespace simulations::systems;

class CAHydrogenBondNeighbors : public NeighborListRules {
public:
  CAHydrogenBondNeighbors(const NeighborList & enclosing_list, const std::string & secondary,
      const std::string & scored_ss = "E") : enclosing_list_(enclosing_list) {
    sequence_separation_ = 3;
    secondary_ = secondary;
    scored_ss_ = scored_ss;
  }

  ~CAHydrogenBondNeighbors() override = default;

  bool if_atom_excluded(core::index4 i) override {
#ifdef DEBUG
    if(i>=secondary_.size())
      throw std::runtime_error("CAHydrogenBondNeighbors::if_atom_excluded(): secondary structure string shorter than a requested index");
#endif
    // --- exclude position whose SS code can't be found on the list of allowed SS types (H, E, or both)
    if (scored_ss_.find(secondary_[i]) == std::string::npos) return true;
    // --- the first and the last residue do not interact - we can't build LCS for them
    if (i == 0 || i == enclosing_list_.n_atoms() - 1)  return true;
    // --- the atom must not be a tail of its chain, otherwise LCS definition is wrong
    if (enclosing_list_.the_system[i].chain_id != enclosing_list_.the_system[i - 1].chain_id ||
        enclosing_list_.the_system[i].chain_id != enclosing_list_.the_system[i + 1].chain_id)
      return true;

    return false;
  }

  bool if_pair_excluded(core::index4 i, core::index4 j) override {

    if (std::abs(int(i - j)) < sequence_separation_) {          // --- if they are closer than sequence_separation
      // --- if they belong to the same chain: do not include on a contact list
      if (enclosing_list_.the_system[i].chain_id == enclosing_list_.the_system[j].chain_id) return true;
      if (i == j) return true;    // --- obviously exclude also self-contacts
    }

    return false;
  }

private:
  int sequence_separation_;
  const NeighborList & enclosing_list_;
  std::string secondary_;
  std::string scored_ss_;
};

enum HbondType {
  PARALLEL,
  ANTIPARALLEL,
  HELICAL,
  helical
};

//struct HBCenter {
//  double avg_y, avg_z;
//  char type;
//};

/** @brief Contact potential of a square-well shape and excluded volume included.
 */
class CAHydrogenBond : public ByAtomEnergy {
public:

  double hbond_energy_cutoff = -0.1;

  static constexpr double width = 3.2;

  static constexpr double ca_min_distance = 4.0;
  static constexpr double ca_max_distance = 6.1;
  static constexpr double ca_min_distance_2 = ca_min_distance * ca_min_distance;
  static constexpr double ca_max_distance_2 = ca_max_distance * ca_max_distance;


//  CAHydrogenBond(const CartesianChains &system, const std::vector<std::string> &parameters)
//      : ByAtomEnergy("ContactEnergy"), nbl(system.coordinates(), system.n_atoms, 0.0, 4.0), energy_(20, 20) {
//
//    if (parameters.size() != 1) {
//      std::runtime_error( "CAHydrogenBond requires exactly one parameters: secondary structure as a string");
//    }
//
//    // initialize NBL with r=0, will be changed by this constructor
//    nbl.cutoff(ca_max_distance);
//    nbl.update_all();
//  }

  /** @brief Hydrogen bond defined by geometric criteria based on 6 alpha-carbons
   */
  CAHydrogenBond(const CartesianAtoms &system, const std::string & secondary, const std::string & scored_ss_types = "HE");

  ~CAHydrogenBond() override = default;

  double energy(CartesianAtoms &conformation) override;

  double energy(CartesianAtoms &conformation, const core::index4 which_atom) override;

  double delta_energy(CartesianAtoms &reference_conformation, const core::index4 which_atom,
      CartesianAtoms &new_conformation) override;

  double delta_energy(CartesianAtoms &reference_conformation, const core::index4 atom_from, const core::index4 atom_to,
      CartesianAtoms &new_conformation) override;

  /** @brief Calculates the energy of a plausible H-bond between two C-alphas.
   *
   * This method does not include sequence neighbors in energy evaluation;
   * it cannot be used to evaluate delta energy value.
   * Set <code>raw_energy</code> flag to <code>false</code> to evaluate the actual energy of an interaction,
   * leave <code>true</code> to just detect a hydrogen bond according to the 3CA definition.
   *
   * @param conformation - conformation of the scored system
   * @param i_at - index of an interacting atom
   * @param j_at - index of an interacting atom
   * @param raw_energy - when true (and this is the default), it calculates raw energy,
   * without the correction for amino acid types of the interacting residues
   * @return energy value
   */
  double evaluate_raw_energy(CartesianAtoms &conformation, core::index4 i_at, core::index4 j_at, bool raw_energy = true);

  static double evaluate_energy(const Vec3 &i_ca_prev, const Vec3 & the_i_ca, const Vec3 & i_ca_next,
      const Vec3 &j_ca_prev, const Vec3 & the_j_ca, const Vec3 & j_ca_next, HbondType bond_type);
  
  double sequence_correction(core::index1, core::index1);

  /// Returns the secondary structure of a given residue, according to this scoring function
  char secondary(const core::index4 pos) { return secondary_[pos]; }

private:
  std::string secondary_;
  NeighborList nbl;
  core::data::basic::Array2D<double> energy_H_;
  core::data::basic::Array2D<double> energy_E_;

  /// This can't be static because it uses the neighbor list
  double evaluate_energy_single_ca(CartesianAtoms &conformation, core::index4 i_at, bool calc_only_forward=false);

  /// Evaluate energy between two atoms
  static double evaluate_energy_unsafe(CartesianAtoms &conformation, core::index4 i_at, core::index4 j_at, char ss);

  /// Reads a matrix of pairwise scores
  void read_scores(const AtomTypingInterface_SP atyping, const std::string & fname = "forcefield/hbond_3ca.dat");

  /// Prints energy scores in a nice table
  void show_scores(const AtomTypingInterface_SP atyping);
};

}
}
}
#endif
