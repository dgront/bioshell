#ifndef SIMULATIONS_FORCEFIELDS_CARTESIAN_ContactEnergy_HH
#define SIMULATIONS_FORCEFIELDS_CARTESIAN_ContactEnergy_HH

#include <core/index.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/forcefields/NeighborList.hh>

namespace simulations {
namespace forcefields {
namespace cartesian {

using namespace simulations::systems;

/** @brief Contact potential of a square-well shape and excluded volume included.
 */
class ContactEnergy : public ByAtomEnergy {
public:

  ContactEnergy(const CartesianAtoms &system, const std::vector<std::string> &parameters);

  /** @brief Define the square-well shape of energy function.
   *
   */
  ContactEnergy(const CartesianAtoms &system, const double high_energy_level, const double low_energy_level,
      const double excluded_volume_distance, const double contact_shortest_distance,
      const double contact_longest_distance);

  ~ContactEnergy() override = default;

  double energy(CartesianAtoms &conformation) override;

  double energy(CartesianAtoms &conformation, const core::index4 which_atom) override;

  double delta_energy(CartesianAtoms &reference_conformation, const core::index4 which_atom,
      CartesianAtoms &new_conformation) override;

  double delta_energy(CartesianAtoms &reference_conformation, const core::index4 atom_from, const core::index4 atom_to,
      CartesianAtoms &new_conformation) override;

  /** @brief Sets neighbor exclusion for the list of neigbors used by this energy
   * @param new_rules defines atoms and atom pairs excluded from energy evaluation
   */
  void neighbor_rules(NeighborListRules_SP new_rules) { nbl.neighbor_rules(new_rules); }

protected:
  double high_energy_level_;
  double low_energy_level_;
  double excluded_volume_distance_;
  double contact_shortest_distance_;
  double contact_longest_distance_;
  double contact_shortest_distance2;
  double contact_longest_distance2;
  double excluded_volume_distance2;

  inline void init(const double high_energy_level, const double low_energy_level, const double excluded_volume_distance,
      const double contact_shortest_distance, const double contact_longest_distance) {

    high_energy_level_ = high_energy_level;
    low_energy_level_ = low_energy_level;
    excluded_volume_distance_ = excluded_volume_distance;
    contact_shortest_distance_ = contact_shortest_distance;
    contact_longest_distance_ = contact_longest_distance;

    contact_shortest_distance2 = contact_shortest_distance * contact_shortest_distance;
    contact_longest_distance2 = contact_longest_distance * contact_longest_distance;
    excluded_volume_distance2 = excluded_volume_distance * excluded_volume_distance;
  }

private:
  NeighborList nbl;
};


}
}
}
#endif
