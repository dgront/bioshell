#include <core/calc/structural/transformations/transformation_utils.hh>

#include <simulations/forcefields/cartesian/CAHydrogenBond.hh>
#include <core/calc/statistics/NormalDistribution.hh>

#include <core/BioShellEnvironment.hh>
#include <core/data/io/DataTable.hh>
#include <core/chemical/Monomer.hh>
#include <core/calc/numeric/Function1D.hh>
#include <set>

namespace simulations {
namespace forcefields {
namespace cartesian {

using namespace simulations::systems;
using core::calc::statistics::NormalDistribution;


class SmoothStepFunction : core::calc::numeric::Function1D<double>{
public:
  double from;
  double to;
  double smooth;

  SmoothStepFunction(double from, double to, double smooth = 0.2) {
    this->from = from;
    this->to = to;
    this->smooth = smooth;
  } 
  /// Virtual destructor (empty)
  ~SmoothStepFunction() override = default;

  /** @brief Call a 1D function
   * @param x function's argument
   * @return function value \f$ f(x) \f$
   */
  double operator()(const double x) const override {
    return (tanh((x - from) / smooth) - tanh((x - to) / smooth)) / 2.0;
  }
};

SmoothStepFunction hbca_A_gt5_x{-0.512375 - CAHydrogenBond::width * 0.299921, -0.512375 + CAHydrogenBond::width * 0.299921};
SmoothStepFunction hbca_A_gt5_r{4.74849 - CAHydrogenBond::width * 0.173994, 4.74849 + CAHydrogenBond::width * 0.173994};
SmoothStepFunction hbca_A_gt5_a{6.52424 - CAHydrogenBond::width * 0.216474, 6.52424 + CAHydrogenBond::width * 0.216474};
SmoothStepFunction hbca_A_lt5_x{-0.715211 - CAHydrogenBond::width * 0.304241, -0.715211 + CAHydrogenBond::width * 0.304241};
SmoothStepFunction hbca_A_lt5_r{4.81181 - CAHydrogenBond::width * 0.127693, 4.81181 + CAHydrogenBond::width * 0.127693};
SmoothStepFunction hbca_A_lt5_a{3.37516 - CAHydrogenBond::width * 0.243099, 3.37516 + CAHydrogenBond::width * 0.243099};
SmoothStepFunction hbca_P_gt5_x{0.0783662 - CAHydrogenBond::width * 0.240884, 0.0783662 + CAHydrogenBond::width * 0.240884};
SmoothStepFunction hbca_P_gt5_r{4.79591 - CAHydrogenBond::width * 0.145513, 4.79591 + CAHydrogenBond::width * 0.145513};
SmoothStepFunction hbca_P_gt5_a{6.42867 - CAHydrogenBond::width * 0.160225, 6.42867 + CAHydrogenBond::width * 0.160225};
SmoothStepFunction hbca_P_lt5_x{-0.0747412 - CAHydrogenBond::width * 0.300409, -0.0747412 + CAHydrogenBond::width * 0.300409};
SmoothStepFunction hbca_P_lt5_r{4.79221 - CAHydrogenBond::width * 0.16418, 4.79221 + CAHydrogenBond::width * 0.16418};
SmoothStepFunction hbca_P_lt5_a{3.2715 - CAHydrogenBond::width * 0.175267, 3.2715 + CAHydrogenBond::width * 0.175267};

SmoothStepFunction hbca_H_x{-0.778056 - CAHydrogenBond::width * 0.236439, -0.778056 + CAHydrogenBond::width * 0.236439};
SmoothStepFunction hbca_H_r{4.49166 - CAHydrogenBond::width * 0.0947177, 4.49166 + CAHydrogenBond::width * 0.0947177};
SmoothStepFunction hbca_H_a{3.13868 - CAHydrogenBond::width * 0.0686717, 3.13868 + CAHydrogenBond::width * 0.0686717};

SmoothStepFunction hbca_h_x{0.762176 - CAHydrogenBond::width * 0.259259, 0.762176 + CAHydrogenBond::width * 0.259259};
SmoothStepFunction hbca_h_r{4.83785 - CAHydrogenBond::width * 0.0782727, 4.83785 + CAHydrogenBond::width * 0.0782727};
SmoothStepFunction hbca_h_a{6.38959 - CAHydrogenBond::width * 0.0673698, 6.38959 + CAHydrogenBond::width * 0.0673698};

// --- Center of a ring of points formed by YX local coordinates of hydrogen bonds.
// --- These values must be subtracted before local Cartesian coordinates are transformed into radial coordinates (i.e. r, alpha)
struct XYRingCenter {
  double avg_y, avg_z;
};

// --- We need four different centers to fit four cases:
XYRingCenter H_center {-0.2, -1.3 };  // --- H-bond in a helix along the bond direction N->O
XYRingCenter h_center {0.0, -1.3 };    // --- H-bond in a helix against the bond direction N->O // 0.1, -1.8
XYRingCenter P_center {0.0554903, -0.364022}; // --- H-bond in a parallel strand
XYRingCenter A_center {-0.204107, -0.63542};  // --- H-bond in an antiparallel strand


CAHydrogenBond::CAHydrogenBond(const CartesianAtoms &system, const std::string & secondary,
    const std::string & scored_ss_types) : ByAtomEnergy("CAHydrogenBond"),
    nbl(system.coordinates(), system.n_atoms, ca_max_distance, 4), energy_H_(20,20), energy_E_(20,20) {

  secondary_ = secondary;
  auto ptr = std::make_shared<CAHydrogenBondNeighbors>(nbl, secondary, scored_ss_types);
  nbl.neighbor_rules(std::dynamic_pointer_cast<NeighborListRules>(ptr));
  read_scores(system.atom_typing);
}

/** @brief Helper function to calculate x, r, and a internal coordinates of the j-th atom.
 * The internal coordinates are expressed in LCS defined by (i-1)-st, i-th and (i+1)-st atoms
 * @param a_prev - (i-1) atom
 * @param a_cent - the i-th atom that is supposed in interact with j-th atom
 * @param a_next - (i+1) atom
 * @param a_rot - the j-th atom that is supposed in interact with i-th atom
 * @param correct_y - correction to make the Y of the point where the transformed j-th vector aims at to 0
 * @param correct_z - correction to make the Z of the point where the transformed j-th vector aims at to 0
 * @param x - calculated x coordinate
 * @param r - calculated r internal coordinate
 * @param a - calculated alfa internal coordinate
 */
bool compute_x_r_a(const Vec3 &a_prev, const Vec3 &a_cent, const Vec3 &a_next, const Vec3 &a_rot,
    double correct_y, double correct_z, double &x, double &r, double &a) {

  // ---------- local coordinate system (LCS) centered on the i-th atom
  auto i_rt_sp = core::calc::structural::transformations::local_coordinates_three_atoms(a_prev, a_cent, a_next);
  Vec3 copy;
  // ---------- position of the j-th atom in the LCS
  i_rt_sp->apply(a_rot, copy);
  x = copy.x;                                   // --- X coordinate
  copy.y -= correct_y;                          // --- correct for the center of Y-Z circle
  copy.z -= correct_z;
  if (copy.z > 4.0) return false;               // --- "stacking" strands is not allowed!
  r = sqrt(copy.y * copy.y + copy.z * copy.z);  // --- polar coordinates from Y-Z
  a = atan2(copy.z, copy.y);                    // --- angle should be [-pi,pi]
  if (a < 0) a += 2 * M_PI;                     // --- take care the angle is in the range [0,2pi]
  if (a < M_PI / 2.0) a += 2 * M_PI;

  return true;
}

double CAHydrogenBond::evaluate_energy(const Vec3 &i_ca_prev, const Vec3 & the_i_ca, const Vec3 & i_ca_next,
    const Vec3 &j_ca_prev, const Vec3 & the_j_ca, const Vec3 & j_ca_next, HbondType bond_type) {


  double x_j, r_j, a_j;     // --- coordinates of the j-th atom in LCS(i)
  double x_i, r_i, a_i;     // --- coordinates of the i-th atom in LCS(j)
  double out_i, out_j;
  double correct_y, correct_z;
  switch (bond_type) {
    case PARALLEL:     // --- Parallel sheet: residue J in the LCS of I
      correct_y = P_center.avg_y;
      correct_z = P_center.avg_z;
      if (!compute_x_r_a(i_ca_prev, the_i_ca, i_ca_next, the_j_ca, correct_y, correct_z, x_j, r_j, a_j)) return 0.0;
      if (!compute_x_r_a(j_ca_prev, the_j_ca, j_ca_next, the_i_ca, correct_y, correct_z, x_i, r_i, a_i)) return 0.0;

      if (a_j > 5.0) out_j = hbca_P_gt5_x(x_j) * hbca_P_gt5_a(a_j) * hbca_P_gt5_r(r_j);
      else out_j = hbca_P_lt5_x(x_j) * hbca_P_lt5_a(a_j) * hbca_P_lt5_r(r_j);
      // --- Parallel sheet: residue I in the LCS of J
      if (a_i > 5.0) out_i = hbca_P_gt5_x(x_i) * hbca_P_gt5_a(a_i) * hbca_P_gt5_r(r_i);
      else out_i = hbca_P_lt5_x(x_i) * hbca_P_lt5_a(a_i) * hbca_P_lt5_r(r_i);
      break;
    case ANTIPARALLEL:         // --- Antiparallel sheet: residue J in the LCS of I
      correct_y = A_center.avg_y;
      correct_z = A_center.avg_z;
      if (!compute_x_r_a(i_ca_prev, the_i_ca, i_ca_next, the_j_ca, correct_y, correct_z, x_j, r_j, a_j)) return 0.0;
      if (!compute_x_r_a(j_ca_prev, the_j_ca, j_ca_next, the_i_ca, correct_y, correct_z, x_i, r_i, a_i)) return 0.0;

      if (a_j > 5.0) out_j = hbca_A_gt5_x(x_j) * hbca_A_gt5_a(a_j) * hbca_A_gt5_r(r_j);
      else out_j = hbca_A_lt5_x(x_j) * hbca_A_lt5_a(a_j) * hbca_A_lt5_r(r_j);
      // --- Antiparallel sheet: residue I in the LCS of J
      if (a_i > 5.0) out_i = hbca_A_gt5_x(x_i) * hbca_A_gt5_a(a_i) * hbca_A_gt5_r(r_i);
      else out_i = hbca_A_lt5_x(x_i) * hbca_A_lt5_a(a_i) * hbca_A_lt5_r(r_i);
      break;
    case HELICAL:         // --- Helical H-bond along N->O
      correct_y = H_center.avg_y;
      correct_z = H_center.avg_z;
      if (!compute_x_r_a(i_ca_prev, the_i_ca, i_ca_next, the_j_ca, correct_y, correct_z, x_j, r_j, a_j)) return 0.0;
      correct_y = h_center.avg_y;
      correct_z = h_center.avg_z;
      if (!compute_x_r_a(j_ca_prev, the_j_ca, j_ca_next, the_i_ca, correct_y, correct_z, x_i, r_i, a_i)) return 0.0;

      out_j = hbca_H_x(x_j) * hbca_H_a(a_j) * hbca_H_r(r_j);
      out_i = hbca_h_x(x_i) * hbca_h_a(a_i) * hbca_h_r(r_i);
      break;
    case helical:         // --- Helical H-bond against N->O
      correct_y = h_center.avg_y;
      correct_z = h_center.avg_z;
      if (!compute_x_r_a(i_ca_prev, the_i_ca, i_ca_next, the_j_ca, correct_y, correct_z, x_j, r_j, a_j)) return 0.0;
      correct_y = H_center.avg_y;
      correct_z = H_center.avg_z;
      if (!compute_x_r_a(j_ca_prev, the_j_ca, j_ca_next, the_i_ca, correct_y, correct_z, x_i, r_i, a_i)) return 0.0;

      out_j = hbca_h_x(x_j) * hbca_h_a(a_j) * hbca_h_r(r_j);
      out_i = hbca_H_x(x_i) * hbca_H_a(a_i) * hbca_H_r(r_i);
      break;
  }

  return -pow(out_i * out_j, 1.0/6);
}

double CAHydrogenBond::evaluate_raw_energy(CartesianAtoms &conformation, core::index4 i_at, core::index4 j_at, bool raw_energy) {

  if(nbl.if_atom_excluded(i_at) || nbl.if_atom_excluded(j_at)) return 0.0;
  if(nbl.if_pair_excluded(i_at, j_at)) return 0.0;
  char i_ss = secondary_[i_at];
  char j_ss = secondary_[j_at];
  if(raw_energy) {
    if(i_ss=='E' && j_ss=='E')
      return CAHydrogenBond::evaluate_energy_unsafe(conformation, i_at, j_at, 'E');
    if (i_ss == 'H' && j_ss == 'H')
      return CAHydrogenBond::evaluate_energy_unsafe(conformation, i_at, j_at,'H');
  }
  else {
    core::index1 i_type = conformation[i_at].residue_type;
    core::index1 j_type = conformation[j_at].residue_type;
    double en = 0.0;
    if(i_ss=='E' && j_ss=='E')
      en =  CAHydrogenBond::evaluate_energy_unsafe(conformation, i_at, j_at, 'E');
    if (i_ss == 'H' && j_ss == 'H')
      en =  CAHydrogenBond::evaluate_energy_unsafe(conformation, i_at, j_at,'H');

    if (!raw_energy && en < hbond_energy_cutoff)
      return en + energy_E_.get(i_type, j_type) * energy_E_.get(j_type, i_type);
  }
  return 0.0;
}

double CAHydrogenBond::evaluate_energy_unsafe(CartesianAtoms &conformation, core::index4 i_at, core::index4 j_at, char ss) {

  // ---------- the middle CA are too far away from or too close to each other
  double d = conformation[i_at].distance_square_to(conformation[j_at]);
  if ((d >= ca_max_distance_2) || (d <= ca_min_distance_2)) return 0.0;
  d = conformation[i_at - 1].distance_square_to(conformation[j_at - 1]);
  if (d >= ca_min_distance_2 && d <= ca_max_distance_2) { // --- try parallel case
    double d2 = conformation[i_at + 1].distance_square_to(conformation[j_at + 1]);
    if (d2 >= ca_min_distance_2 && d2 <= ca_max_distance_2) {
      // --- the parallel case
      Vec3 ai(conformation[i_at]);
      Vec3 ai_prev, ai_next, aj_prev, aj_next, aj;
      conformation[i_at-1].get_closest(conformation[i_at],ai_prev);
      conformation[i_at+1].get_closest(conformation[i_at],ai_next);
      conformation[j_at].get_closest(conformation[i_at],aj);
      conformation[j_at-1].get_closest(conformation[i_at],aj_prev);
      conformation[j_at+1].get_closest(conformation[i_at],aj_next);

      HbondType hb_type = (ss == 'H') ? ((i_at > j_at) ? HELICAL : helical) : PARALLEL;

      return evaluate_energy(ai_prev, ai, ai_next,aj_prev, aj, aj_next, hb_type);
    }
  } else {
    if(ss!='E') return 0.0;
    d = conformation[i_at - 1].distance_square_to(conformation[j_at + 1]);
    if (d >= ca_min_distance_2 && d <= ca_max_distance_2) { // --- try ANTI-parallel case
      double d2 = conformation[i_at + 1].distance_square_to(conformation[j_at - 1]);
      if (d2 >= ca_min_distance_2 && d2 <= ca_max_distance_2) {
        // --- the ANTI-parallel case
        Vec3 ai(conformation[i_at]);
        Vec3 ai_prev, ai_next, aj_prev, aj_next, aj;

        if (i_at > j_at) {
          conformation[i_at - 1].get_closest(conformation[i_at], ai_prev);
          conformation[i_at + 1].get_closest(conformation[i_at], ai_next);
          conformation[j_at].get_closest(conformation[i_at], aj);
          conformation[j_at + 1].get_closest(conformation[i_at], aj_prev);
          conformation[j_at - 1].get_closest(conformation[i_at], aj_next);
        } else {
          conformation[i_at + 1].get_closest(conformation[i_at], ai_prev);
          conformation[i_at - 1].get_closest(conformation[i_at], ai_next);
          conformation[j_at].get_closest(conformation[i_at], aj);
          conformation[j_at - 1].get_closest(conformation[i_at], aj_prev);
          conformation[j_at + 1].get_closest(conformation[i_at], aj_next);
        }

        return evaluate_energy(ai_prev, ai, ai_next, aj_prev, aj, aj_next, ANTIPARALLEL);
      }
    }
  }

  return 0.0;
}

double CAHydrogenBond::evaluate_energy_single_ca(CartesianAtoms &conformation, core::index4 i_at,bool calc_only_forward) {
  double en_total = 0.0;
  core::index1 i_type = conformation[i_at].residue_type;
  char i_ss = secondary_[i_at];
  for (auto it = nbl.cbegin(i_at); it != nbl.cend(i_at); ++it) {
      //this flag is used only while by chunk calculation to calculate only forward
      if (calc_only_forward && i_at < *it ) continue;
    core::index1 j_type = conformation[*it].residue_type;
    char j_ss = secondary_[*it];
    double  en_no_crct = 0.0;
    if ((i_ss == 'H') && (j_ss == 'H')) {
      en_no_crct = evaluate_energy_unsafe(conformation, i_at, *it, 'H');
      if (en_no_crct < hbond_energy_cutoff)
        en_total += en_no_crct + energy_H_.get(i_type, j_type) * energy_H_.get(j_type, i_type);
    } else
      if((i_ss == 'E') && (j_ss == 'E')) {
        en_no_crct = evaluate_energy_unsafe(conformation, i_at, *it, 'E');
        if (en_no_crct < hbond_energy_cutoff)
          en_total += en_no_crct + energy_E_.get(i_type, j_type) * energy_E_.get(j_type, i_type);
      }
  }
  return en_total;
}

double CAHydrogenBond::energy(CartesianAtoms &conformation) {

  nbl.update_all();
  CartesianChains &chains = dynamic_cast<CartesianChains &>(conformation);
  double en = 0;
  for (core::index2 ic = 0; ic < chains.count_chains(); ++ic) {
    const auto &ar = chains.atoms_for_chain(ic);
    // range is from ar.first_atom + 1 to ar.last_atom-1 because in evaluate energy we take i+1 and i-1 so we want to stay in atoms range;
    for (core::index4 which_atom = ar.first_atom + 1; which_atom < ar.last_atom; ++which_atom) {
        if (!nbl.if_atom_excluded(which_atom)) {
            en += evaluate_energy_single_ca(conformation, which_atom);
        }
    }
  }
  // in evaluate_energy_single_ca we calculate every hbond twice (because we recalculate n, n-1,n+1) so here we divide the sum over 2.0
  return en / 2.0;
}

double CAHydrogenBond::energy(CartesianAtoms &conformation, const core::index4 which_atom) {


  double en = 0.0;
  if (which_atom > 0 && !nbl.if_atom_excluded(which_atom - 1)) {
    nbl.update(which_atom-1);
    en += evaluate_energy_single_ca(conformation, which_atom - 1);
  }

  if (!nbl.if_atom_excluded(which_atom)) {
    nbl.update(which_atom);
    en += evaluate_energy_single_ca(conformation, which_atom);
  }

  if (which_atom < nbl.n_atoms() - 1 && !nbl.if_atom_excluded(which_atom + 1)) {
    nbl.update(which_atom + 1);
    en += evaluate_energy_single_ca(conformation, which_atom + 1);
  }

  return en;
}

double CAHydrogenBond::delta_energy(CartesianAtoms &reference_conformation, const core::index4 which_atom,
    CartesianAtoms &new_conformation) {

  double delta_en = 0.0;
  if (which_atom > 0 && !nbl.if_atom_excluded(which_atom - 1)) {
    nbl.update(which_atom - 1);
    delta_en += evaluate_energy_single_ca(new_conformation, which_atom - 1) -
                evaluate_energy_single_ca(reference_conformation, which_atom - 1);
  }

  if (!nbl.if_atom_excluded(which_atom)) {
    nbl.update(which_atom);
    delta_en += evaluate_energy_single_ca(new_conformation, which_atom) -
                evaluate_energy_single_ca(reference_conformation, which_atom);
  }

  if (which_atom < nbl.n_atoms() - 1 && !nbl.if_atom_excluded(which_atom + 1)) {
    nbl.update(which_atom + 1);
    delta_en += evaluate_energy_single_ca(new_conformation, which_atom + 1) -
                evaluate_energy_single_ca(reference_conformation, which_atom + 1);
  }

  return delta_en;
}

double CAHydrogenBond::delta_energy(CartesianAtoms &reference_conformation,
    const core::index4 atom_from, const core::index4 atom_to, CartesianAtoms &new_conformation) {
    core::index4 from_safe =  atom_from > 0 ? atom_from - 1 :atom_from;
    core::index4 to_safe = atom_to < nbl.n_atoms() - 1 ? atom_to + 1 :atom_to;
    std::set<core::index4> to_calc;

  double delta_en = 0.0;
    //adding which_atom and its neigbors to the std::set
  for(core::index4 which_atom=from_safe;which_atom<=to_safe;++which_atom) {
      if (!nbl.if_atom_excluded(which_atom)) {
          nbl.update(which_atom);
          to_calc.insert(which_atom);
          for (auto it = nbl.cbegin(which_atom); it != nbl.cend(which_atom); ++it) {
              to_calc.insert(*it);
          }
      }
  }

    for (auto it = to_calc.cbegin(); it != to_calc.cend(); ++it) {
        if (!nbl.if_atom_excluded(*it)) {
            //calculate only forward to ensure every hbond will be calculated once
            delta_en += evaluate_energy_single_ca(new_conformation, *it,true) -
                        evaluate_energy_single_ca(reference_conformation, *it,true);
        }
    }

  return delta_en;
}

void CAHydrogenBond::read_scores(const AtomTypingInterface_SP atyping, const std::string & fname) {

  utils::Logger logger("CAHydrogenBond");
  logger << utils::LogLevel::FILE << "Reading ff file: " << fname << "\n";
  core::data::io::DataTable dt(core::BioShellEnvironment::from_file_or_db(fname));

  for(const auto & row: dt) {
    char aai = row[0][0];
    char aaj = row[0][1];
    core::index1 idi = atyping->residue_type(core::chemical::Monomer::get(aai).code3,AtomTypingVariants::STANDARD);
    core::index1 idj = atyping->residue_type(core::chemical::Monomer::get(aaj).code3,AtomTypingVariants::STANDARD);
    double real_H = row.get<double>(1);
    double expected_H = row.get<double>(2);
    energy_H_.set(idi, idj, -log(real_H / expected_H));
    double real_E = row.get<double>(3);
    double expected_E = row.get<double>(4);
    energy_E_.set(idi, idj, -log(real_E / expected_E));
  }

  show_scores(atyping);
}

void CAHydrogenBond::show_scores(const AtomTypingInterface_SP atyping) {
  utils::Logger logger("CAHydrogenBond");

  if(! logger.is_logable(utils::LogLevel::FINE)) return;
  logger << utils::string_format("#i j aai aaj en_H enE\n");
  for (int i = 0; i < 20; ++i) {
    for (int j = 0; j < 20; ++j) {
      logger << utils::string_format("%2d %2d %s %s %f %f\n", i, j, atyping->residue_internal_name(i).c_str(),
          atyping->residue_internal_name(j).c_str(), energy_H_(i, j), energy_E_(i, j));
    }
  }
}

double CAHydrogenBond::sequence_correction(core::index1 i, core::index1 j) { return energy_E_.get(i, j); }

}
}
}
