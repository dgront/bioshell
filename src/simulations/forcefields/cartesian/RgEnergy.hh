#ifndef SIMULATIONS_FORCEFIELDS_CARTESIAN_RgEnergy_HH
#define SIMULATIONS_FORCEFIELDS_CARTESIAN_RgEnergy_HH

#include <cmath>

#include <core/calc/structural/calculate_from_structure.hh>

#include <utils/string_utils.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>

namespace simulations {
namespace forcefields {
namespace cartesian {

using namespace core::calc::numeric;

/** @brief Penalty for the difference between theoretical and actual \f$ R_g \f$ values.
 *
 * The theoretical value of a radius of gyration for a protein of \f$ N \f$ residues is calculated according to the
 * formula
 *
 * \f[
 * R_g = 0.395 N^{3/5} +  7.257
 * \f]
 * @todo Take PBC into account in energy evaluation
 */
class RgEnergy : public ByAtomEnergy {
public:

  /** \brief Creates energy function based on string parameters.
   *
   * @param system - the system whose energy will be evaluated
   * @param parameters - target Rg value may be passed as a parameter here; otherwise a theoretical fit is used
   */
  RgEnergy(const systems::CartesianAtoms &system, const std::vector<std::string> &parameters) :
    ByAtomEnergy(""), logger("RgEnergy") {

    if ((parameters.size() > 0) && (parameters[0] != "-"))
      target = utils::from_string<double>(parameters[0]);
    else
      target = 0.395 * pow(system.n_atoms, 3.0 / 5.0) + 7.257;
    double rg = sqrt(core::calc::structural::calculate_Rg_square(&system[0], &system[system.n_atoms]));
    logger << utils::LogLevel::INFO << "Rg target for " << system.n_atoms << " is " << target <<
           " initial value: " << rg << "\n";
  }

  /// Empty virtual constructor to satisfy the compiler
  virtual ~RgEnergy() {}

  /// Returns the name of this energy term, which is "RgEnergy"
  const std::string & name() const { return name_; }

  double energy(systems::CartesianAtoms & conformation, const core::index4 which_atom) override {

    return energy(conformation);
  }

  double delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 which_atom,
      systems::CartesianAtoms & new_conformation) override {

    return energy(new_conformation) - energy(reference_conformation);
  }

  double delta_energy(systems::CartesianAtoms & reference_conformation, const core::index4 atom_from,
      const core::index4 atom_to, systems::CartesianAtoms & new_conformation) override {

    return energy(new_conformation) - energy(reference_conformation);
  }


  double energy(systems::CartesianAtoms & conformation) override {

    double s = target
               - sqrt(core::calc::structural::calculate_Rg_square(&conformation[0], &conformation[conformation.n_atoms]));
    return s*s;
  }

private:
  double target = 0;
  static const std::string name_;
  utils::Logger logger;
};


}
}
}

#endif

