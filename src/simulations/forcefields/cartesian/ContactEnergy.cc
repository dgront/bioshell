#include <simulations/forcefields/cartesian/ContactEnergy.hh>
#include <set>
namespace simulations {
namespace forcefields {
namespace cartesian {

ContactEnergy::ContactEnergy(const CartesianAtoms &system, const double high_energy_level, const double low_energy_level,
    const double excluded_volume_distance,
    const double contact_shortest_distance, const double contact_longest_distance) :
    ByAtomEnergy("ContactEnergy"), nbl(system.coordinates(), system.n_atoms, contact_longest_distance, 4) {

    auto ptr = std::static_pointer_cast<NeighborListRules>(std::make_shared<ExcludeSequenceNeighbors>(1, nbl));
    nbl.neighbor_rules(ptr);
    init(high_energy_level, low_energy_level, excluded_volume_distance, contact_shortest_distance,
        contact_longest_distance);
}

ContactEnergy::ContactEnergy(const CartesianAtoms &system, const std::vector<std::string> &parameters)
    : ByAtomEnergy("ContactEnergy"), nbl(system.coordinates(), system.n_atoms, 0.0, 4.0) {

    if (parameters.size() < 5) {
        std::runtime_error(
            "ContactEnergy requires exactly four parameters: high_energy_level, low_energy_level, excluded_volume_distance ,contact_shortest_distance, contact_longest_distance!");
    }
    init(utils::from_string<double>(std::string(parameters[0])), utils::from_string<double>(std::string(parameters[1])),
        utils::from_string<double>(std::string(parameters[2])), utils::from_string<double>(std::string(parameters[3])),
        utils::from_string<double>(std::string(parameters[4])));

    nbl.cutoff(contact_longest_distance_);
    auto ptr = std::static_pointer_cast<NeighborListRules>(std::make_shared<ExcludeSequenceNeighbors>(1, nbl));
    nbl.neighbor_rules(ptr);
}

double ContactEnergy::energy(CartesianAtoms &conformation) {

    double energy = 0.0;
    for (int which_atom = 0; which_atom < conformation.n_atoms; which_atom++) {
        const core::data::basic::Vec3I & o = conformation[which_atom];
        for (int i = 0; i < which_atom; i++) {
            if (!nbl.if_pair_excluded(which_atom,i)) {
                double r2 = o.closest_distance_square_to(conformation[i], contact_longest_distance2);
                if (r2 < contact_longest_distance2) {
                    if (r2 > contact_shortest_distance2)
                        energy += low_energy_level_;
                    if (r2 < excluded_volume_distance2)
                        energy += high_energy_level_;
                }
            }
        }
    }
    return energy;

}

double ContactEnergy::energy(CartesianAtoms &conformation, const core::index4 which_atom) {

    nbl.update(which_atom);
    double energy = 0.0;
    const core::data::basic::Vec3I &o = conformation[which_atom];
    for (auto it = nbl.cbegin(which_atom); it != nbl.cend(which_atom); ++it) {
        double r2 = o.closest_distance_square_to(conformation[*it], contact_longest_distance2);
        if (r2 < contact_longest_distance2) {
            if (r2 > contact_shortest_distance2)
                energy += low_energy_level_;
            if (r2 < excluded_volume_distance2)
                energy += high_energy_level_;
        }
    }
    return energy;
}


double ContactEnergy::delta_energy(CartesianAtoms & reference_conformation, const core::index4 which_atom, CartesianAtoms & new_conformation){

    nbl.update(which_atom);
    double delta_en = 0.0;
    const core::data::basic::Vec3I & o_ref = reference_conformation[which_atom];
    const core::data::basic::Vec3I & o_new = new_conformation[which_atom];
    for (auto it = nbl.cbegin(which_atom); it != nbl.cend(which_atom); ++it) {
        double r2b = o_ref.closest_distance_square_to(reference_conformation[*it],contact_longest_distance2);
        if (r2b < contact_longest_distance2) {
            if (r2b > contact_shortest_distance2)
                delta_en -= low_energy_level_;
            if (r2b < excluded_volume_distance2)
                delta_en -= high_energy_level_;
        }
        double r2a = o_new.closest_distance_square_to(new_conformation[*it],contact_longest_distance2);
        if (r2a < contact_longest_distance2) {
            if (r2a > contact_shortest_distance2)
                delta_en += low_energy_level_;
            if (r2a < excluded_volume_distance2)
                delta_en += high_energy_level_;
        }
    }
    return delta_en;

}
double ContactEnergy::delta_energy(CartesianAtoms & reference_conformation, const core::index4 atom_from, const core::index4 atom_to ,CartesianAtoms & new_conformation) {
    std::set<core::index4> to_calc;
    double en = 0.0;
    for (auto i=atom_from;i<=atom_to;++i){
        to_calc.insert(i);
        nbl.update(i);
        for (auto it = nbl.cbegin(i); it != nbl.cend(i); ++it) {
            to_calc.insert(*it);
        }
    }
    for (auto i=to_calc.cbegin();i!=to_calc.cend();++i){
        const core::data::basic::Vec3I & o_ref = reference_conformation[*i];
        const core::data::basic::Vec3I & o_new = new_conformation[*i];
        for (auto j=i;j!=to_calc.cend();++j){
                double r2a = o_ref.closest_distance_square_to(reference_conformation[*j],contact_longest_distance2);
                if (r2a < contact_longest_distance2) {
                    if (r2a > contact_shortest_distance2)
                        en-=low_energy_level_;
                    if (r2a < excluded_volume_distance2)
                        en-=high_energy_level_;
                }
            double r2 = o_new.closest_distance_square_to(new_conformation[*j],contact_longest_distance2);
            if (r2 < contact_longest_distance2) {
                if (r2 > contact_shortest_distance2)
                    en+=low_energy_level_;
                if (r2 < excluded_volume_distance2)
                    en+=high_energy_level_;
            }
            }
        }

    return en;
}

}
}
}