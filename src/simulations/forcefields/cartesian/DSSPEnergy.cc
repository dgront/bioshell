#include <simulations/forcefields/cartesian/DSSPEnergy.hh>
#include <set>

namespace simulations {
namespace forcefields {
namespace cartesian {

DSSPEnergy::DSSPEnergy(const CartesianChains &system) : ByAtomEnergy("DSSPEnergy"), the_system(system),
                                                        logger("DSSPEnergy"), index_NCaCO(system.count_residues() * 4) {

    for (core::index2 ri = 0; ri < system.count_residues(); ++ri) {
        try {
            index_NCaCO[ri * 4] = system.find_atom_index(ri, " N  ");
            index_NCaCO[ri * 4 + 1] = system.find_atom_index(ri, " CA ");
            index_NCaCO[ri * 4 + 2] = system.find_atom_index(ri, " C  ");
            index_NCaCO[ri * 4 + 3] = system.find_atom_index(ri, " O  ");
        } catch (std::invalid_argument &e) {
            logger << utils::LogLevel::SEVERE << "Can't find a backbone atom in a residue!";
            throw e;
        }
    }
}
double DSSPEnergy::energy_kernel(CartesianAtoms& conformation, const core::index2 the_moved_residue, const core::index2 the_other_residue) {
    double en=0;
    if (the_moved_residue > 0)
        en+=energy_single_bond(conformation,the_moved_residue, the_other_residue);
    if (the_other_residue > 0)
        en+=energy_single_bond(conformation,the_other_residue, the_moved_residue);

    return en;
}
double DSSPEnergy::energy_single_bond(CartesianAtoms& conformation,const core::index2 residue_donor, const core::index2 residue_acceptor) {
    double energy=0;
    Vec3 hydrogen;

    const Vec3 & donor_N = Vec3(conformation[index_NCaCO[residue_donor * 4]]);
    const Vec3 & acceptor_O = Vec3( conformation[index_NCaCO[residue_acceptor * 4 + 3]]);
    if (donor_N.distance_square_to(acceptor_O) < MAX_NO_DISTANCE2) {
        const Vec3 & prev_donor_C = Vec3(conformation[index_NCaCO[(residue_donor - 1) * 4 + 2]]);
        const Vec3 & donor_N = Vec3( conformation[index_NCaCO[residue_donor * 4]]);
        const Vec3 & donor_Ca = Vec3( conformation[index_NCaCO[residue_donor * 4 + 1]]);
        const Vec3 & acceptor_C = Vec3( conformation[index_NCaCO[residue_acceptor * 4 + 2]]);

        core::calc::structural::interactions::peptide_hydrogen(prev_donor_C, donor_N, donor_Ca, hydrogen);
        double e = 1.0 / donor_N.distance_to(acceptor_O); // N-O
        e += 1.0 / hydrogen.distance_to(acceptor_C); // C-H
        e -= 1.0 / hydrogen.distance_to(acceptor_O); // O-H
        e -= 1.0 / donor_N.distance_to(acceptor_C); // N-C

        energy += e * DSSP_CONST;
    }

    return energy;
}

double DSSPEnergy::energy(CartesianAtoms& conformation){
    double e=0;
    for (core::index4 ri=0;ri<the_system.count_residues();ri++)
        for (core::index4 rj=0;rj<ri;rj++)
            e+=energy_kernel(conformation,ri,rj);
    return e;
}

double DSSPEnergy::energy(CartesianAtoms& conformation,core::index4 which_atom){
    double en=0;
    if (std::find(index_NCaCO.begin(),index_NCaCO.end(),which_atom)!=index_NCaCO.end()) {
        auto cartesian_chains = dynamic_cast<CartesianChains &>(conformation);
        core::index2 res = cartesian_chains.residue_for_atom(which_atom);
        for (core::index4 ri=0;ri<res;ri++)
            en+=energy_kernel(conformation,res,ri);
        for (core::index4 ri=res+1;ri<the_system.count_residues();ri++)
            en+=energy_kernel(conformation,res,ri);
    }
    return en;
}
double DSSPEnergy::delta_energy(CartesianAtoms& old_conformation, core::index4 which_atom,CartesianAtoms& new_conformation){
    return energy(new_conformation,which_atom)-energy(old_conformation,which_atom);
}
double DSSPEnergy::delta_energy(CartesianAtoms& old_conformation, core::index4 atom_from,core::index4 atom_to,CartesianAtoms& new_conformation){
    double en=0;
    std::set<core::index4> to_calc;
    auto cartesian_chains = dynamic_cast<CartesianChains &>(new_conformation);
    for (auto i=atom_from;i<=atom_to;++i) {
        to_calc.insert(cartesian_chains.residue_for_atom(i));
    }
    for (auto i=to_calc.begin();i!=to_calc.end();++i) {
        auto atm = cartesian_chains.atoms_for_residue(*i).first_atom;
        en += delta_energy(old_conformation, atm, new_conformation);
    }
    return en;
}

const double DSSPEnergy::MAX_NO_DISTANCE = 5.0;

const double DSSPEnergy::MAX_NO_DISTANCE2  = MAX_NO_DISTANCE * MAX_NO_DISTANCE;

const double DSSPEnergy::DSSP_CONST = 0.42 * 0.2 * 332.0;

}
}
}
