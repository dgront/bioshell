#ifndef SIMULATIONS_FORCEFIELDS_CARTESIAN_ExcludedVolumePenalty_HH
#define SIMULATIONS_FORCEFIELDS_CARTESIAN_ExcludedVolumePenalty_HH

#include <core/index.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/forcefields/NeighborList.hh>

namespace simulations {
namespace forcefields {
namespace cartesian {

class ExcludedVolumePenalty : public ByAtomEnergy {
public:

  ExcludedVolumePenalty(const simulations::systems::CartesianAtoms &system, const std::vector<std::string> &parameters);

  /** @brief Define the square-well shape of energy function.
   *
   */
  ExcludedVolumePenalty(const systems::CartesianAtoms &system, const double distance, const double penalty_value);


  ~ExcludedVolumePenalty() override = default;

  double energy(systems::CartesianAtoms &conformation) override;

  double energy(systems::CartesianAtoms &conformation, const core::index4 which_atom) override;

  double delta_energy(systems::CartesianAtoms &reference_conformation, const core::index4 which_atom,
      systems::CartesianAtoms &new_conformation) override;

  double delta_energy(systems::CartesianAtoms &reference_conformation, const core::index4 atom_from, const core::index4 atom_to,
      systems::CartesianAtoms &new_conformation) override;

  /** @brief Sets neighbor exclusion for the list of neigbors used by this energy
   * @param new_rules defines atoms and atom pairs excluded from energy evaluation
   */
  void neighbor_rules(NeighborListRules_SP new_rules) { nbl.neighbor_rules(new_rules); }


protected:
  double repulsion_distance;
  double repulsion_energy;

private:
  double excluded_volume_distance2;
  NeighborList nbl;
};


}
}
}
#endif
