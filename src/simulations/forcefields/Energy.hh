#ifndef SIMULATIONS_FORCEFIELDS_Energy_HH
#define SIMULATIONS_FORCEFIELDS_Energy_HH

#include <string>
#include <core/data/basic/Array2D.hh>
#include <simulations/systems/CartesianAtoms.hh>

namespace simulations {
namespace forcefields {

/** @brief Bare interface for a class that can calculate energy.
 */
class Energy {
public:

  Energy() { name_ = "energy"; }

  /** @brief Constructor sets the name of a derived energy term
   * @param name - name of an energy to be printed on screen when reporting energy values
   */
  Energy(const std::string & name) { name_ = name; }

  /// Virtual destructor
  virtual ~Energy() = default;

  /** @brief Calculates the total energy of a given conformation.
   * @param conformation - conformation to assess
   * @return energy value
   */
  virtual double energy(systems::CartesianAtoms & conformation) = 0;

  /// Returns the name of a derived energy term
  const std::string & name() const { return name_; }

  /// Sets the name of a derived energy term
  void name(const std::string & name) { name_ = name; }

  /** @brief Returns a string that identifies this energy function.
   *
   * This method is helpful when running several instances of a system in a single simulation. The <code>forcefield_tag</code>
   * may be written together with observables to identify the respective system's copy. In particular,
   * <code>ReplicaExchangeMC</code> sampler changes the tag so it contains both the replica ID and temperature ID.
   * In that case the tag may change every replica exchange event
   *
   * @return a string identifying this energy function; the value of that string may change over
   * time but must be unique among all the FF copies
   */
  inline const std::string & get_forcefield_tag() const { return energy_tag_; }

  /** @brief Sets the new <code>forcefield_tag</code> value for this system.
   *
   * @param new_forcefield_tag - the new value of the string that identifies this force field
   */
  inline void set_forcefield_tag(const std::string &new_forcefield_tag) { energy_tag_ = new_forcefield_tag; }

private:
  std::string energy_tag_;
  std::string name_;  // name of this energy, e.g. printed on a screen
};

} // ~ simulations
} // ~ ff
#endif
