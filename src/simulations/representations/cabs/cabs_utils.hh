/** \file cabs_utils.hh
 * @brief Provide utility methods for CABS and CABSBB models
 */
#ifndef SIMULATIONS_REPRESENTATIONS_cabs_utils_HH
#define SIMULATIONS_REPRESENTATIONS_cabs_utils_HH

#include <core/data/structural/Structure.hh>

namespace simulations {
namespace representations {

/** @brief Converts an all-atom structure to CABS representation.
 *
 * The simple program shows how to do the conversion:
 * \include ex_cabs_representation.cc
 * @param strctr - a full atom structure (at least all the heavy atoms must be present)
 * @param mark_incomplete_lowercase - if true, incomplete residue side chains will be marked by a lowercase atom name
 * @return a structure in the CABS representation:
<code><pre>
# An example of CABS representation given in PDB format
  - atom indexing always starts from 1
  - residue indexing is the same as in input full atom structure
  - side chain atom name (e.g. SK1) means: S - side chain, K - one letter amino acid code, 1 - rotamer type
    (if full atom side chain is not complete, lowercase is used  as in: "sk1")
ATOM     13  bb  LYS A   7     -11.030  19.585  15.550  1.00 14.92           C  
ATOM     14  CA  LYS A   7      -9.788  18.972  16.853  1.00 12.46           C  
ATOM     15  CB  LYS A   7      -8.860  17.775  16.674  1.00 14.62           C  
ATOM     16  SK1 LYS A   7      -6.729  16.044  18.191  1.00 17.39           C  
ATOM     17  bb  SER A   8      -8.947  20.399  17.796  1.00 12.46           C  
ATOM     18  CA  SER A   8      -8.106  21.826  18.739  1.00 12.47           C  
ATOM     19  CB  SER A   8      -8.983  22.793  19.534  1.00 14.82           C  
ATOM     20  SS1 SER A   8      -9.045  22.399  20.894  1.00 16.43           C  
</pre></code>
 */
core::data::structural::Structure_SP cabs_representation(const core::data::structural::Structure & strctr,
  bool mark_incomplete_lowercase = false);

/** @brief Converts an all-atom structure to CABSBB representation.
 *
 * The simple program shows how to do the conversion:
 * \include ex_cabsbb_representation.cc
 * @param strctr - a full atom structure (at least all the heavy atoms must be present)
 * @param mark_incomplete_lowercase - if true, incomplete residue side chains will be marked by a lowercase atom name
 * @return a structure in the CABSBB representation:
<code><pre>
# An example of CABSBB representation given in PDB format
  - atom indexing always starts from 1
  - residue indexing is the same as in input full atom structure
  - side chain atom name (e.g. SK1) means: S - side chain, K - one letter amino acid code, 1 - rotamer type
    (if full atom side chain is not complete, lowercase is used  as in: "sk1")
ATOM     13  N   LYS A   7     -10.556  19.169  15.635  1.00 12.42           N  
ATOM     14  CA  LYS A   7      -9.788  18.972  16.853  1.00 12.46           C  
ATOM     15  C   LYS A   7      -8.976  20.236  17.130  1.00 11.97           C  
ATOM     16  O   LYS A   7      -8.461  20.861  16.203  1.00 12.20           O  
ATOM     17  CB  LYS A   7      -8.860  17.775  16.674  1.00 14.62           C  
ATOM     18  SK1 LYS A   7      -6.729  16.044  18.191  1.00 17.39           C  
ATOM     19  N   SER A   8      -8.859  20.625  18.390  1.00 12.92           N  
ATOM     20  CA  SER A   8      -8.106  21.826  18.739  1.00 12.47           C  
ATOM     21  C   SER A   8      -6.900  21.445  19.590  1.00 11.09           C  
ATOM     22  O   SER A   8      -6.807  20.317  20.077  1.00 10.99           O  
ATOM     23  CB  SER A   8      -8.983  22.793  19.534  1.00 14.82           C  
ATOM     24  SS1 SER A   8      -9.045  22.399  20.894  1.00 16.43           C 
</pre></code>
 */
core::data::structural::Structure_SP cabsbb_representation(const core::data::structural::Structure & strctr,
                                                           bool mark_incomplete_lowercase = false);

/** @brief Returns true if a given structure is a correct model in CABS representation
 * @param strctr - an input structure
 * @return true if the argument in a CABS model
 */
bool is_cabs_model(const core::data::structural::Structure & strctr);

/** @brief Returns true if a given structure is a correct model in CABSBB representation
 * @param strctr - an input structure
 * @return true if the argument in a CABSBB model
 */
bool is_cabsbb_model(const core::data::structural::Structure & strctr);

}
}

#endif

/**
 * \example ex_cabs_representation.cc
 * \example ex_cabsbb_representation.cc
 */
