#include <cmath>
#include <iostream>
#include <algorithm>    // std::sort
#include <vector>
#include <core/index.hh>
#include <simulations/representations/cabs/CGRotamerDistribution.hh>
#include <core/calc/statistics/NormalDistribution.hh>
#include <core/calc/statistics/VonMisesDistribution.hh>
#include <core/calc/structural/transformations/CartesianToSpherical.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>

namespace simulations {
namespace representations {
namespace cabs {

CGRotamerDistribution::CGRotamerDistribution(const std::vector<double> &parameters) : core::calc::statistics::PDF(parameters),
    D1_normal(parameters[0], parameters[3]), D2_normal(parameters[1], parameters[4]), D3_misses(parameters[2], parameters[5]) {

  if (parameters_.size() == 0) {
    parameters_.resize(6, 0.0);
    parameters_[3] = parameters_[4] = parameters_[5] = 0.1;
  }
}

void CGRotamerDistribution::copy_parameters_from(const std::vector<double> &source) {

  for (unsigned int i = 0; i < parameters_.size(); ++i) parameters_[i] = source[i];
}

void CGRotamerDistribution::backup_parameters(std::vector<double> &backup) const {

  for (unsigned int i = 0; i < parameters_.size(); ++i) backup[i] = parameters_[i];
}

const std::vector<double> & CGRotamerDistribution::estimate(const std::vector<std::vector<double>> & observations) {

//    estimate averages and standard deviations for each 1D distribution
  std::vector<double> params = D1_normal.estimate(observations, 0);
  parameters_[0] = params[0];
  parameters_[3] = params[1];
  params = D2_normal.estimate(observations, 1);
  parameters_[1] = params[0];
  parameters_[4] = params[1];
  params = D3_misses.estimate(observations, 2);
  parameters_[2] = params[0];
  if (params[1] >= 200) {
    parameters_[5] = 200;
    params[1] = 200;
    D3_misses.copy_parameters_from(params);
  }
  else parameters_[5] = params[1];

  return parameters_;
}

const std::vector<double> & CGRotamerDistribution::estimate(const std::vector<std::vector<double>> & observations,
                                                 const std::vector<double> & weights) {

  //    estimate averages and standard deviations for each 1D distribution
  std::vector<double> params = D1_normal.estimate(observations, weights, 0);
  parameters_[0] = params[0];
  parameters_[3] = params[1];
  params = D2_normal.estimate(observations, weights, 1);
  parameters_[1] = params[0];
  parameters_[4] = params[1];
  params = D3_misses.estimate(observations, weights, 2);
  parameters_[2] = params[0];
  if (params[1] >= 200) {
    parameters_[5] = 200;
    params[1] = 200;
    D3_misses.copy_parameters_from(params);
  }
  else parameters_[5] = params[1];

  return parameters_;
}


double CGRotamerDistribution::evaluate(double x, double y, double z) const {

  double pdf_1D_x = D1_normal.evaluate(x);
  double pdf_1D_y = D2_normal.evaluate(y);
  double pdf_1D_z = D3_misses.evaluate(z);
  return pdf_1D_x * pdf_1D_y * pdf_1D_z;
}

core::calc::structural::transformations::Rototranslation rt;
core::calc::structural::transformations::CartesianToSpherical cts;


core::data::basic::Vec3 & CGRotamerDistribution::cartesian_coordinates(const core::data::basic::Vec3 & N,
  const core::data::basic::Vec3 & CA, const core::data::basic::Vec3 & C, core::data::basic::Vec3 & returned_SC) const {

  using namespace core::calc::structural::transformations;
  local_coordinates_three_atoms(N, CA, C, rt); // --- we assume the following order of atoms in every residue: N, CA, C

  returned_SC.set(parameters_);
  cts.apply_inverse(returned_SC);
  rt.apply_inverse(returned_SC);

  return returned_SC;
}

}
}
}

