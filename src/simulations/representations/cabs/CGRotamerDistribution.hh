#ifndef SIMULATIONS_REPRESENATIONS_CABS_CGRotamersDistribution_HH
#define SIMULATIONS_REPRESENATIONS_CABS_CGRotamersDistribution_HH

#include <math.h>
#include <algorithm>    // std::sort
#include <vector>
#include <core/calc/statistics/PDF.hh>
#include <core/calc/statistics/NormalDistribution.hh>
#include <core/calc/statistics/VonMisesDistribution.hh>
#include <core/data/basic/Vec3.hh>

namespace simulations {
namespace representations {
namespace cabs {

/** \brief Combine three 1D distribution function: two normal and one von Misses
 *
 * \include ex_CGRotamersDistribution.cc
 */
class CGRotamerDistribution: public core::calc::statistics::PDF {
public:

  /** @brief Constructor creates an instance of this distribution based on the given parameters : mean and variance and covariance
   * @param parameters - parameters of the distribution, e.g. expectation, variance, moments, etc.
   */
  CGRotamerDistribution(const double ave1, const double ave2, const double ave3, const double sdev1,
                        const double sdev2, const double sdev3) :
      CGRotamerDistribution(std::vector<double>({ave1, ave2, ave3, sdev1, sdev2, sdev3})) {}

  /** @brief Constructor creates an instance of this distribution based on the given parameters
   * @param parameters - parameters of the joint distribution given in the following order:
   *   - expected value for the first 1D normal distribution - the X (first) dimension
   *   - expected value for the second 1D normal distribution - the Y (second) dimension
   *   - expected value for the third 1D (von Misses) distribution - the Z (third) dimension
   *   - standard deviation for the first 1D (normal) distribution
   *   - standard deviation for the second 1D (normal) distribution
   *   - standard deviation for the third 1D (von Misses) distribution
   */
  CGRotamerDistribution(const std::vector<double> &parameters);

   /// Default virtual destructor does nothing
  virtual ~CGRotamerDistribution() {}

  /** @brief Estimates parameters of this distribution based on a given sample.
   *
   * @param observations - sample of points generated from a normal distribution used to estimate its parameters
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations);

  /** @brief Estimates parameters of this distribution based on given weighted observations.
   *
   * @param observations - sample of points generated from a normal distribution used to estimate its parameters
   * @param weights - real values to weight the observations
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations,
                                                   const std::vector<double> & weights);

  /** @brief Evaluate the probability of withdrawing a given vector of values from this distribution.
   *
   * @param random_value - vector of PDF function arguments : X for the first 1D normal, Y for the second 1D normal anz Z for the third 1D von Misses
   */
  virtual inline double evaluate(const std::vector<double> & random_value) const {
    return evaluate(random_value[0], random_value[1], random_value[2]);
  }

  /** @brief Evaluate the probability of withdrawing a given value from distribution.
   *
   * @param x1 - x for the first 1D (normal) term
   * @param x2 - x for the second 1D (normal) term
   * @param x3 - x for the third 1D (von Misses) term
   */
  double evaluate(double x1, double x2, double x3) const;

  /** @brief Modifies parameters of this distribution by replacing them with a given values
   *
   * @param source - source parameters
   */
  virtual void copy_parameters_from(const std::vector<double> &source);

  /** @brief Backup parameters of this distribution by copying them into a given vector.
   *
   * @param backup - vector with last parameters
   */
  void backup_parameters(std::vector<double> &backup) const;

  /** @brief Calculates global coordinates of a CG side chain atom based on backbone atoms positions
   *
   * @param N - position of the backbone amide N atom
   * @param CA - position of the backbone alpha carbon
   * @param C - position of the backbone carbonyl C atom
   * @param returned_SC - vector where the computed coordinates will be stored
   */
  core::data::basic::Vec3 & cartesian_coordinates(const core::data::basic::Vec3 & N, const core::data::basic::Vec3 & CA,
                               const core::data::basic::Vec3 & C, core::data::basic::Vec3 & returned_SC) const;

private:
  core::calc::statistics::NormalDistribution D1_normal;
  core::calc::statistics::NormalDistribution D2_normal;
  core::calc::statistics::VonMisesDistribution D3_misses;
};

}
}
}

#endif
