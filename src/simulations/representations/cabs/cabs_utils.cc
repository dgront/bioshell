#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/chemical/ChiAnglesDefinition.hh>

#include <core/data/structural/Structure.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.hh>
#include <utils/Logger.hh>

#include <simulations/representations/cabs/cabs_utils.hh>

namespace simulations {
namespace representations {

utils::Logger cabs_utils_logger("cabs_utils");

core::data::structural::Structure_SP cabs_representation(const core::data::structural::Structure &strctr,
                                                         bool mark_incomplete_lowercase) {

  using namespace core::data::structural;
  using namespace core::data::io;

  Structure_SP structure = std::make_shared<Structure>(strctr.code());
  PdbAtom_SP SC_sp;
  core::index2 res_id, previous_res_id = 0;        //indexing of residues to check the completeness of residues
  core::index4 cabs_atom_index = 1;            //new atom indexing in CABS representation
  unsigned int n_atom_in_sc = 0;
  double avg_bf = 0.00;
  bool is_OK = true;
  core::data::structural::selectors::IsCA ca_test;
  core::data::structural::selectors::IsCB cb_test;
  core::data::structural::selectors::IsHydrogen is_hydrogen;
  core::data::structural::selectors::IsAlternateLocation is_alt;

// Iterate over all chains
  for (auto it_chain = strctr.begin(); it_chain != strctr.end(); ++it_chain) {
    Chain_SP all_atom_chain_sp = *it_chain;
    Chain_SP cabs_chain_sp = std::make_shared<core::data::structural::Chain>(all_atom_chain_sp->id());

// Iterate over all residues in the chain
    for (auto it_resid = (*it_chain)->begin(); it_resid != (*it_chain)->end(); ++it_resid) {
      Residue_SP all_atom_residue_sp = (*it_resid);
      res_id = all_atom_residue_sp->id();
      const core::chemical::Monomer &m = all_atom_residue_sp->residue_type();
      Residue_SP cabs_residue_sp = std::make_shared<core::data::structural::Residue>(res_id, m);

// Check the completeness of residues in the chain and atoms in the residue
      if (m.type != 'P') continue;
      if (previous_res_id + 1 == res_id) ++previous_res_id;
      else {
        cabs_utils_logger << utils::LogLevel::WARNING << "Missing all residue(s) from " << previous_res_id + 1 << " to " << res_id - 1 << "\n";
        previous_res_id = res_id;
      }
      core::index4 n_s = all_atom_residue_sp->count_atoms() - 5;  // number of heavy atoms in the side chain of this residue in the structure
      core::index4 n_m = m.n_heavy_atoms - 1;          // number of heavy atoms this monomer type has
      if (it_resid == (*it_chain)->end() - 1) n_m += 1;        //number of heavy atoms in the side chain of LAST amino acid in the structure is incremented by one due to OXT atom
      if (n_m > n_s + 5) {
        cabs_utils_logger << utils::LogLevel::WARNING << "Residue " << all_atom_residue_sp->id() << " is not complete!\n";
        is_OK = false;
      }

      // --- Make new atom - in the reduced representation
      std::string atom_name = ((!is_OK) && mark_incomplete_lowercase) ? utils::string_format(" s%c1", tolower(m.code1))
                                                                      : utils::string_format(" S%c1", m.code1);
      SC_sp = std::make_shared<PdbAtom>(cabs_atom_index, atom_name);
      SC_sp->is_heteroatom(false);
      SC_sp->occupancy(1.00);
      n_atom_in_sc = 0;
      avg_bf = 0.00;                        //b-factor for SC atom is the average of b-factors for all side chain atoms
      // Iterate over all atoms in the residue
      for (auto it_atom = all_atom_residue_sp->begin(); it_atom != all_atom_residue_sp->end(); ++it_atom) {
        PdbAtom_SP atom_sp = (*it_atom);

        if(is_hydrogen(*atom_sp)) continue;
        if (is_alt(*atom_sp)) {
          cabs_utils_logger << utils::LogLevel::WARNING << "Residue " << all_atom_residue_sp->id() << " " << m.code3
                            << " has alternative locator of " << atom_sp->atom_name() << ".\n";
          continue;
        }

        if (ca_test(*atom_sp) || cb_test(*atom_sp)) { // if atom_sp is alpha carbon
          atom_sp->id(cabs_atom_index);
          (*cabs_residue_sp).push_back(atom_sp);
          ++cabs_atom_index;
          continue;
        }
        avg_bf += atom_sp->b_factor();
        ++n_atom_in_sc;
        (*SC_sp) +=* (atom_sp);
      }
      if ((m.code3 != "ALA") && (m.code3 != "GLY")) {
        avg_bf /= double(n_atom_in_sc);
        (*SC_sp) /= double(n_atom_in_sc);
        SC_sp->b_factor(avg_bf);
        cabs_residue_sp->push_back(SC_sp);
      }
      (*cabs_chain_sp).push_back(cabs_residue_sp);
    }
    for (core::index2 ires = 0; ires < cabs_chain_sp->count_residues() - 1; ++ires) {
      auto i_res_sp = (*cabs_chain_sp)[ires];
      PdbAtom_SP bb_sp = std::make_shared<PdbAtom>(0, " bb ", 1); // the " bb " is made hydrogen so pymol displays it as sth. small
      (*bb_sp) += *(i_res_sp->find_atom(" CA "));
      (*bb_sp) += *((*cabs_chain_sp)[ires+1]->find_atom(" CA "));
      (*bb_sp) /= 2.0;
      bb_sp->b_factor(((i_res_sp->find_atom(" CA "))->b_factor() + ((*cabs_chain_sp)[ires+1]->find_atom(" CA "))->b_factor())/2.0);
      auto ca_iterator = std::find(i_res_sp->begin(), i_res_sp->end(), i_res_sp->find_atom(" CA "));
      (*cabs_chain_sp)[ires]->insert(++ca_iterator, bb_sp);
      bb_sp->owner(i_res_sp);
    }
    // --- reorder  atoms (from 1)
    (*structure).push_back(cabs_chain_sp);
  }
  core::index4 i_atom = 0;
  std::for_each(structure->first_atom(), structure->last_atom(), [&](PdbAtom_SP e) {(e)->id(++i_atom);});

  return structure;
}

core::data::structural::Structure_SP cabsbb_representation(const core::data::structural::Structure &strctr,
                                                           bool mark_incomplete_lowercase) {

  using namespace core::data::structural;
  using namespace core::data::io;
  using namespace core::data::structural::selectors;

  Structure_SP structure = std::make_shared<Structure>(strctr.code());
  PdbAtom_SP SC_sp;
  core::index2 res_id, previous_res_id = 0;        //indexing of residues to check the completeness of residues
  core::index4 cabs_atom_index = 1;            //new atom indexing in CABS representation
  unsigned int n_atom_in_sc = 0;
  double avg_bf = 0.00;
  bool is_OK = true;
  IsBBCB bb_cb_test;
  IsHydrogen is_hydrogen;
  IsAlternateLocation is_alt;
  ResidueHasBBCB has_full_bb_cb;

// Iterate over all chains
  for (auto it_chain = strctr.begin(); it_chain != strctr.end(); ++it_chain) {
    Chain_SP all_atom_chain_sp = *it_chain;
    Chain_SP cabs_chain_sp = std::make_shared<core::data::structural::Chain>(all_atom_chain_sp->id());

// Iterate over all residues in the chain
    for (auto it_resid = (*it_chain)->begin(); it_resid != (*it_chain)->end(); ++it_resid) {
      Residue_SP all_atom_residue_sp = (*it_resid);
      res_id = all_atom_residue_sp->id();
      const core::chemical::Monomer &m = all_atom_residue_sp->residue_type();
      Residue_SP cabs_residue_sp = std::make_shared<core::data::structural::Residue>(res_id, m);

// Check the completeness of residues in the chain and atoms in the residue
      if (m.type != 'P') continue;
      if (previous_res_id + 1 == res_id) ++previous_res_id;
      else {
        cabs_utils_logger << utils::LogLevel::WARNING << "Missing all residue(s) from " << previous_res_id + 1 << " to " << res_id - 1 << "\n";
        previous_res_id = res_id;
      }
      if(has_full_bb_cb(*all_atom_residue_sp)) {
        core::index4 n_s = all_atom_residue_sp->count_atoms() - 5;  // number of heavy atoms in the side chain of this residue in the structure
        core::index4 n_m = m.n_heavy_atoms - 1;          // number of heavy atoms this monomer type has
        if (it_resid == (*it_chain)->end() - 1) n_m += 1;        //number of heavy atoms in the side chain of LAST amino acid in the structure is incremented by one due to OXT atom
        if (n_m > n_s + 5) {
          cabs_utils_logger << utils::LogLevel::WARNING
                            << "Residue " << all_atom_residue_sp->id() << " is not complete!\n";
          is_OK = false;
        }
      } else {
        cabs_utils_logger << utils::LogLevel::WARNING
                          << "Residue " << all_atom_residue_sp->id() << " has incomplete backbone!\n";
      }

      // --- Make new atom - in the reduced representation
      std::string atom_name = ((!is_OK) && mark_incomplete_lowercase) ? utils::string_format(" s%c1", tolower(m.code1))
                                                                      : utils::string_format(" S%c1", m.code1);
      SC_sp = std::make_shared<PdbAtom>(cabs_atom_index, atom_name);
      SC_sp->is_heteroatom(false);
      SC_sp->occupancy(1.00);
      n_atom_in_sc = 0;
      avg_bf = 0.00;                        //b-factor for SC atom is the average of b-factors for all side chain atoms
      // Iterate over all atoms in the residue
      for (auto it_atom = all_atom_residue_sp->begin(); it_atom != all_atom_residue_sp->end(); ++it_atom) {
        PdbAtom_SP atom_sp = (*it_atom);

        if(is_hydrogen(*atom_sp)) continue;
        if (is_alt(*atom_sp)) {
          cabs_utils_logger << utils::LogLevel::WARNING << "Residue " << all_atom_residue_sp->id() << " " << m.code3
                            << " has alternative locator of " << atom_sp->atom_name() << ".\n";
          continue;
        }

        if ((bb_cb_test(*atom_sp)) && (atom_sp->atom_name() != " OXT")) { // if atom_sp is backbone atom or CB
          atom_sp->id(cabs_atom_index);
          (*cabs_residue_sp).push_back(atom_sp);
          ++cabs_atom_index;
          continue;
        }
        avg_bf += atom_sp->b_factor();
        ++n_atom_in_sc;
        (*SC_sp) +=* (atom_sp);
      }
      if ((m.code3 != "ALA") && (m.code3 != "GLY")) {
        if (n_atom_in_sc > 0) {
          avg_bf /= double(n_atom_in_sc);
          (*SC_sp) /= double(n_atom_in_sc);
          SC_sp->b_factor(avg_bf);
          cabs_residue_sp->push_back(SC_sp);
        } else {
          cabs_utils_logger << utils::LogLevel::WARNING << "Can't create the SC atom for residue "
                            << all_atom_residue_sp->id() << " : missing side chain!\n";
        }
      }
      (*cabs_chain_sp).push_back(cabs_residue_sp);
    }
    (*structure).push_back(cabs_chain_sp);
  }
  return structure;
}

bool is_cabs_model(const core::data::structural::Structure &strctr) {

  using namespace core::data::structural;

  bool is_OK = false;
  for (auto it_resid = strctr.first_const_residue(); it_resid != strctr.last_const_residue(); ++it_resid) {
    const core::chemical::Monomer &m = (*it_resid)->residue_type();
    core::index2 n_atoms = (*it_resid)->count_atoms();
    if (n_atoms == 4) {
      for (auto it_atom = (*it_resid)->begin(); it_atom != (*it_resid)->end(); ++it_atom) {
        std::string name = (*it_atom)->atom_name();
        std::string name1 = utils::string_format(" s%c1", tolower(m.code1));
        std::string name2 = utils::string_format(" S%c1", m.code1);
        if ((name == " bb ") || (name != " CA ") || (name != " CB ") || (name != name1) || (name != name2)) { is_OK = true; }
        else is_OK = false;
        break;
      }
    } else if (n_atoms > 4) {
      is_OK = false;
      break;
    }
    else continue;
  }
  if (is_OK == false) cabs_utils_logger << utils::LogLevel::WARNING << "Structure is not in CABS representation!\n";
  return is_OK;
}

bool is_cabsbb_model(const core::data::structural::Structure &strctr) {

  using namespace core::data::structural;

  bool is_OK = false;
  for (auto it_resid = strctr.first_const_residue(); it_resid != strctr.last_const_residue(); ++it_resid) {
    const core::chemical::Monomer &m = (*it_resid)->residue_type();
    core::index2 n_atoms = (*it_resid)->count_atoms();
    if (n_atoms == 6) {
      for (auto it_atom = (*it_resid)->begin(); it_atom != (*it_resid)->end(); ++it_atom) {
        std::string name = (*it_atom)->atom_name();
        std::string name1 = utils::string_format(" s%c1", tolower(m.code1));
        std::string name2 = utils::string_format(" S%c1", m.code1);
        if ((name == " N  ") || (name != " CA ") || (name != " CB ") || (name != name1) || (name != name2) ||
            (name != " C  ") || (name != " O  ")) { is_OK = true; }
        else is_OK = false;
        break;
      }
    } else if (n_atoms > 6) {
      is_OK = false;
      break;
    }
    else continue;
  }
  if (is_OK == false) cabs_utils_logger << utils::LogLevel::WARNING << "Structure is not in CABSBB representation!\n";
  return is_OK;
}

}
}


