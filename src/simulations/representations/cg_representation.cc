#include <memory>
#include <stdexcept>
#include <map>

#include <core/BioShellEnvironment.hh>

#include <utils/io_utils.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/representations/cg_representation.hh>

namespace simulations {
namespace representations {


utils::Logger cg_representation_logger("cg_representation");

bool validate_residue(const core::data::structural::Residue & r, const double tolerance) {

  return true;
}

core::data::structural::Structure_SP change_representation(
    const std::map<std::string, std::vector<std::string>> & representation_map,
     const core::data::structural::Structure & all_atom_structure) {

  cg_representation_logger << utils::LogLevel::INFO << "Applying a CG representation\n";

  core::data::structural::Structure_SP out_str = std::make_shared<core::data::structural::Structure>(all_atom_structure.code());

  // --- iterate over all residues
  size_t atom_id = 0;
  for (auto const & ic : all_atom_structure) {
    core::data::structural::Chain_SP a_chain = std::make_shared<core::data::structural::Chain>(ic->id());
    out_str->push_back(a_chain);
    for (auto const & ir : *ic) {
      const std::string & code3 = ir->residue_type().code3;
      cg_representation_logger << utils::LogLevel::FINE << "Processing " << code3 << "\n";
      const std::string key = "atoms."+code3;
      if (representation_map.find(key) == representation_map.end()) {
        cg_representation_logger << utils::LogLevel::WARNING << "Can't find reduced definition for: " << key << "\n";
        continue;
      }
      core::data::structural::Residue_SP ires = std::make_shared<core::data::structural::Residue>(ir->id(),
              ir->residue_type());
      for (const std::string &atom_name : representation_map.find(key)->second) {
        cg_representation_logger << utils::LogLevel::FINE << "Creating atom " << atom_name << "\n";
        core::data::structural::PdbAtom_SP a = std::make_shared<core::data::structural::PdbAtom>(++atom_id, atom_name, 6);
        double n = 0.0;
        for (const std::string &nn : representation_map.find(code3 + "." + atom_name)->second) {
          auto o = (ir->find_atom(nn));
          if (o == nullptr) {
            cg_representation_logger << utils::LogLevel::WARNING << "Missing atom " << nn << " necessary to make residue " << code3 << "\n";
            continue;
          } else {
            (*a) += (*ir->find_atom(nn));
            n += 1.0;
          }
        }
        if (n > 0) {
          (*a) /= representation_map.find(code3 + "." + atom_name)->second.size();
          ires->push_back(a);
        }
      }
      a_chain->push_back(ires);
    }
    out_str->push_back(a_chain);
  }

  return out_str;
}

void load_representation(const std::string & in_file,
    std::map<std::string, std::vector<std::string>> & representation_map) {

  cg_representation_logger << utils::LogLevel::FILE << "Loading CG representation from " << in_file << "\n";

  std::string fname;
  utils::find_file(in_file, fname, core::BioShellEnvironment::bioshell_db_path());
  utils::read_properties_file(fname, representation_map, true);
}

}
}
