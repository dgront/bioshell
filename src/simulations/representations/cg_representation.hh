/** \file cg_representation.hh
 * @brief Provide methods that convert an all-atom structure into a coarse-grained one.
 *
 * A representation is actually a definition what atoms are present in the CG structure and how to make them.
 * An example representation definition file is given below:
<code><pre>
# The first section defines what atoms each amino acid has
atoms.ALA : _CA_  _CEN
atoms.ARG : _CA_  _CEN
atoms.ASN : _CA_  _CEN

# The second section defines, how to create each atom: either as a copy (e.g. CA atoms) or by averaging several atoms from
the all-atom representation
ALA._CA_ : _CA_
ALA._CEN : _CB_
ARG._CA_ : _CA_
ARG._CEN : _CG_ _CD_ _NE_ _CZ_ _NH1 _NH2 _CB_
ASN._CA_ : _CA_
ASN._CEN : _CG_ _OD1 _ND2 _CB_
</pre></code>
 */
#ifndef SIMULATIONS_REPRESENTATIONS_cg_representation_HH
#define SIMULATIONS_REPRESENTATIONS_cg_representation_HH

#include <map>
#include <string>

#include <core/data/structural/Residue.hh>
#include <core/data/structural/Structure.hh>

namespace simulations {
namespace representations {

/** @brief Checks a residue for completeness and clashes.
 *
 * Returns true if the given residue is complete (i.e. contains all its heavy atoms) and does not clash with any other residue in the structure it belongs to.
 * @param r - the residue to be checked
 * @param tolerance - parameter saying how closely to each other two atoms must be to detect a clash between them. Actually,
 * a clash between atoms \f$A\f$ and \f$B\f$ is defined based on the distance \f$ r(A,B)\f$ between them :
 * \f[
 * r(A,B) < r_A + r_B + c
 * \f]
 * where \f$r_A\f$ is the Van der Waals radii of an atom \f$A\f$ and \f$c\f$ is the tolerance
 */
bool validate_residue(const core::data::structural::Residue & r, const double tolerance);

/** @brief Changes an all-atom structure into a coarse-grained one following a given representation definition.
 *
 * @param  representation_map - a std::map object that holds the representation definition
 * @param  all_atom_structure - an all-atom structure to be transformed
 * @return a new structure in the CG representation
 */
core::data::structural::Structure_SP change_representation(
    const std::map<std::string, std::vector<std::string>> & representation_map,
    const core::data::structural::Structure & all_atom_structure);

/** @brief Loads a representation definition from a file.
 *
 * The method will first look in the current directory for the representation definition file. If not found,
 * it will try to locate it in the bioshell database location.
 *
 * @param in_file - input file in the <code>.properties</code> format
 * @param representation_map - map where the representation will be stored
 */
void load_representation(const std::string & in_file, std::map<std::string, std::vector<std::string>> & representation_map);

}
}

#endif
