/** \file surpass_utils.hh
 * @brief Provide utility methods to convert from all-atom representation to the SURPASS model
 */
#ifndef SIMULATIONS_REPRESENTATIONS_surpass_utils_HH
#define SIMULATIONS_REPRESENTATIONS_surpass_utils_HH

#include <vector>

#include <core/data/structural/Structure.hh>

namespace simulations {
namespace representations {

/** @brief Lists all four-character secondary structure strings that will be converted to E surpass type
 *
 * The vector contains four character strings such as 'CHHH' of all atom protein tetrapeptide, that can form a single
 * SURPASS united atom of type E
 */
extern const std::vector<std::string> surpass_H_definition;

/** @brief Lists all four-character secondary structure strings that will be converted to E surpass type
 *
 * The vector contains four character strings such as 'CEEE' of all atom protein tetrapeptide, that can form a single
 * SURPASS united atom of type E
 */
extern const std::vector<std::string> surpass_E_definition;

/** @brief Assigns secondary structure for all residues in the given structure according to Surpass atom names
 * @param strctr - structure to be updated
 */
core::data::structural::Structure & fix_surpass_ss_assignment(core::data::structural::Structure &surpass_strctr);

/** @brief Converts an all-atom structure to SURPASS representation.
 *
 * Residues of the input structure must be properly annotated with secondary structure. One way to get such a structure
 * is to load it from a PDB file with full header, that contains SHEET and HELIX fields.
 *
 * The simple program shows how to do the conversion:
 * \include ex_surpass_representation.cc
 * @param strctr - a full atom structure (at least all the backbone atoms must be present)
 * @return a structure in the SURPASS representation
 */
core::data::structural::Structure_SP surpass_representation(const core::data::structural::Structure & strctr);

/** @brief Converts an all-atom structure to the SURPASS representation using given secondary struture (e.g. dssp, psipred).
 *
 * The simple program shows how to do the conversion:
 * \include ex_surpass_representation.cc
 * @param strctr - a full atom structure (at least all CA atoms must be present)
 * @param secstr - secondary structure assignment (especially if different from PDB header)
 * @return a structure in the SURPASS representation
 */
core::data::structural::Structure_SP surpass_representation(const core::data::structural::Structure &strctr,
                                                            const core::data::sequence::SecondaryStructure &secstr);

/** @brief Converts a SecondaryStructure object to the SURPASS representation.
 *
 * The features of the resulting object:
 *   - the amino acid sequence will be three residues shorter
 *   - the sequence is changed, e.g. to all - GLY
 *   - secondary structure is modified according to SURPASS conversion rules
 * @param secstr input secondary structure for a regular protein sequence
 * @param check_if_converted - if set to true (the default behavior), the method will check if the sequence is all-GLY.
 * If so, it assumes the input object has already been converted to SURPASS representation and no conversion is required.
 * say <code>false</code> to always convert regardless the sequence of this chain
 * @return a newly created secondary structure object in the SURPASS representation. If the given input is already in SURPASS,
 *  returns a pointer to a deep copy of the input SecondaryStructure
 */
core::data::sequence::SecondaryStructure_SP surpass_representation(const core::data::sequence::SecondaryStructure &secstr,
  const bool check_if_converted = true);

/** @brief Converts a secondary structure string to the SURPASS representation.
 *
 * The returned string is horter by 3 characters since a SURPASS chain is shorter by 3 residues
 * @param secstr - an input "regular" secondary structure as a string
 * @return a newly created secondary structure string object in the SURPASS representation.
 */
std::string surpass_representation(const std::string &secstr);
char surpass_ss(const std::string& ss, core::index2 first_pos=0);

}
}

#endif

/**
 * \example ex_surpass_representation.cc
 */
