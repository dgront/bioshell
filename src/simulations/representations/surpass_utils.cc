#include <memory>
#include <core/data/basic/Vec3Cubic.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <core/data/structural/Structure.hh>
#include <core/chemical/Monomer.hh>
#include <utils/Logger.hh>
#include <utils/string_utils.hh>

#include <simulations/representations/surpass_utils.hh>

namespace simulations {
namespace representations {

using namespace core::data::structural;

utils::Logger surpass_utils_logger("surpass_utils");


core::data::structural::Structure &fix_surpass_ss_assignment(core::data::structural::Structure &surpass_strctr) {

  for (auto at_it = surpass_strctr.first_atom(); at_it != surpass_strctr.last_atom(); ++at_it) {
    if ((**at_it).atom_name() == " S  ") {
      (**at_it).owner()->ss('E');
      continue;
    }
    if ((**at_it).atom_name() == " H  ") {
      (**at_it).owner()->ss('H');
      continue;
    }
    (**at_it).owner()->ss('C');
  }
  return surpass_strctr;
}

std::vector<std::string> create_surpass_H_definition() {
  std::vector<std::string> out{"HHHH","HHHC","CHHH","HHHE","EHHH","HHHS","SHHH"};
  return out;
}

std::vector<std::string> create_surpass_E_definition() {
  std::vector<std::string> out{"EEEE","CEEE","EEEC","HEEE","EEEH", "SSSS","CSSS","SSSC","HSSS","SSSH"};

  return out;
}


const std::vector<std::string> surpass_H_definition = create_surpass_H_definition();

const std::vector<std::string> surpass_E_definition = create_surpass_E_definition();


char surpass_ss(const std::string & ss, core::index2 first_pos) {

  std::string substr = ss.substr(first_pos, 4);
  if (std::find(surpass_E_definition.cbegin(), surpass_E_definition.cend(), substr) != surpass_E_definition.cend())
    return 'E';
  if (std::find(surpass_H_definition.cbegin(), surpass_H_definition.cend(), substr) != surpass_H_definition.cend())
    return 'H';
  return 'C';
}

Residue_SP create_surpass_residue(const Residue_SP r0,const Residue_SP r1,const Residue_SP r2,const Residue_SP r3) {

  std::vector<PdbAtom_SP> ai{r0->find_atom_safe(" CA "), r1->find_atom_safe(" CA "),
                             r2->find_atom_safe(" CA "), r3->find_atom_safe(" CA ")};

  for (core::index2 i = 1; i < 4; ++i)
    if (ai[i - 1]->distance_to(*ai[i]) > 4.2) {
      surpass_utils_logger << utils::LogLevel::WARNING << "Too long distance between alpha carbons of residues: "
                           << ai[i-1]->owner()->id() << " " << ai[i-1]->owner()->residue_type().code3 << " and "
                           << ai[i]->owner()->id() << " " << ai[i]->owner()->residue_type().code3 <<"("<< ai[i - 1]->distance_to(*ai[i])<<")"<<"\n";
      return nullptr;
    }

  std::string ss4(4,'C');
  for (core::index2 i = 0; i < 4; ++i) ss4[i] = ai[i]->owner()->ss();

  Residue_SP surpass_residue = std::make_shared<core::data::structural::Residue>(r0->id(), core::chemical::Monomer::GLY);
  char ss = surpass_ss(ss4);
  surpass_residue->ss(ss);

  PdbAtom_SP surpass_atom = std::make_shared<core::data::structural::PdbAtom>(r0->id(), " SU ");
  if (ss == 'H') surpass_atom->atom_name(" H  ");
  else if (ss == 'E') surpass_atom->atom_name(" S  ");
  else if (ss == 'C') surpass_atom->atom_name(" C  ");
  surpass_atom->occupancy(1.00);
  surpass_atom->b_factor(0.00);
  for (core::index2 i = 0; i < 4; ++i) *(surpass_atom) += *(ai[i]);
  *(surpass_atom) /= 4.0;

  (*surpass_residue).push_back(surpass_atom);

  return surpass_residue;
}

core::data::structural::Structure_SP surpass_representation(const core::data::structural::Structure &strctr) {

  using namespace core::data::structural;

  Residue_SP residue_sp, resid_sp, next_res_sp;
  Structure_SP structure = std::make_shared<Structure>(strctr.code());

  surpass_utils_logger << utils::LogLevel::INFO << "Number of residues in the structure is "
                       << strctr.count_residues()  << "\n";

// ---------- Iterate over all chains
  for (auto it_chain = strctr.begin(); it_chain != strctr.end(); ++it_chain) {
    const Chain & chain = **it_chain;
    surpass_utils_logger << utils::LogLevel::INFO << "Number of residues in the  chain " << chain.id() << " is "
                         << chain.count_aa_residues() << "\n";
    Chain_SP ch_sp = std::make_shared<core::data::structural::Chain>(chain.id()); // Make the new SURPASS chain
    structure->push_back(ch_sp);

// ---------- Iterate over all residues in the chain
    for (auto res_index = 0; res_index < chain.terminal_residue_index() - 2; ++res_index) {
      Residue_SP surpass_res = create_surpass_residue(chain[res_index], chain[res_index + 1], chain[res_index + 2],
                                                      chain[res_index + 3]);
      if(surpass_res== nullptr)
        surpass_utils_logger << utils::LogLevel::WARNING << "Can't create SURPASS residue from all-atom residue: "
          << chain[res_index]->id() << " " << chain[res_index]->residue_type().code3 << "\n";
      else ch_sp->push_back(surpass_res);
    }
  }
  surpass_utils_logger << utils::LogLevel::INFO << "Number of residues in SURPASS representation is: "
                       << structure->count_residues() << "\n";
  return structure;
}

core::data::structural::Structure_SP surpass_representation(const core::data::structural::Structure &strctr,
                                                            const core::data::sequence::SecondaryStructure &secstr) {

  using namespace core::data::structural;
  unsigned int id = 0;
  if (strctr.count_residues() != secstr.length()) {
    surpass_utils_logger << utils::LogLevel::WARNING << "Number of residues in the structure ("
                         << strctr.count_residues() << ") is not equal to the length (" << secstr.length()
                         << ") of secondary structure pattern!\n";
    exit(0);
  } else
    surpass_utils_logger << utils::LogLevel::INFO << "Number of residues in the structure (" << strctr.count_residues()
                         << ") is equal to the length (" << secstr.length() << ") of secondary structure pattern!\n";

// Iterate over all residues in the strctr
  for (auto it_chain = strctr.begin(); it_chain != strctr.end(); ++it_chain) {
    for (auto it_resid = (*it_chain)->begin(); it_resid != (*it_chain)->end(); ++it_resid) {
      (*it_resid)->ss(secstr.ss(id));
      ++id;
    }
  }
  surpass_utils_logger << utils::LogLevel::INFO << "New all-atom secondary structure was assigned!\n";
  Structure_SP structure = surpass_representation(strctr);
  return structure;
}

core::data::sequence::SecondaryStructure_SP
surpass_representation(const core::data::sequence::SecondaryStructure &secstr,
                       const bool check_if_converted) {

  using namespace core::data::sequence;

  // --- We say the sequence has been converted to surpass if all its residues are GLY
  // --- Otherwise it has to be converted
  bool is_converted = true;
  if (check_if_converted) {
    is_converted = true;
    for (char c : secstr.sequence)
      if (c != 'G') {
        is_converted = false;
        break;
      }
  } else {
    is_converted = false;
  }
  if (is_converted) // --- if converted, return a pointer to a deep copy of a given SecondaryStructure
    return std::make_shared<SecondaryStructure>(secstr.header(), secstr.sequence, secstr.first_pos(), secstr.str());

  std::string seq(secstr.length() - 3, 'G');
  std::string sec(secstr.length() - 3, 'C');
  core::data::sequence::SecondaryStructure_SP out = std::make_shared<SecondaryStructure>(secstr.header(), seq,
                                                                                         secstr.first_pos(), sec);
  std::string tmp = secstr.str();
  for (core::index2 i = 0; i < secstr.length() - 3; ++i) {
    if (tmp.compare(i, 4, "HHHH") == 0) {
      out->fractions(i, 1.0, 0, 0);
      continue;
    }
    if (tmp.compare(i, 4, "EEEE") == 0) {
      out->fractions(i, 0, 1.0, 0);
      continue;
    }
    if ((tmp.compare(i, 4, "CHHH") == 0) || (tmp.compare(i, 4, "HHHC") == 0)) {
      out->fractions(i, 0.75, 0, 0.25);
      continue;
    }
    if ((tmp.compare(i, 4, "EHHH") == 0) || (tmp.compare(i, 4, "HHHE") == 0)) {
      out->fractions(i, 0.75, 0.25, 0);
      continue;
    }
    if ((tmp.compare(i, 4, "CEEE") == 0) || (tmp.compare(i, 4, "EEEC") == 0)) {
      out->fractions(i, 0.0, 0.75, 0.25);
      continue;
    }
    if ((tmp.compare(i, 4, "HEEE") == 0) || (tmp.compare(i, 4, "EEEH") == 0)) {
      out->fractions(i, 0.25, 0.75, 0);
      continue;
    }
  }

  return out;
}

std::string surpass_representation(const std::string &secstr) {

  std::string out;

  for (core::index2 i = 0; i < secstr.length() - 3; ++i) {
    if (secstr.compare(i, 4, "HHHH") == 0) {
      out += "H";
      continue;
    }
    if (secstr.compare(i, 4, "EEEE") == 0) {
      out += "E";
      continue;
    }
    if ((secstr.compare(i, 4, "CHHH") == 0) || (secstr.compare(i, 4, "HHHC") == 0)) {
      out += "H";
      continue;
    }
    if ((secstr.compare(i, 4, "EHHH") == 0) || (secstr.compare(i, 4, "HHHE") == 0)) {
      out += "H";
      continue;
    }
    if ((secstr.compare(i, 4, "CEEE") == 0) || (secstr.compare(i, 4, "EEEC") == 0)) {
      out += "E";
      continue;
    }
    if ((secstr.compare(i, 4, "HEEE") == 0) || (secstr.compare(i, 4, "EEEH") == 0)) {
      out += "E";
      continue;
    }
    out += "C";
  }

  return out;
}

}
}


