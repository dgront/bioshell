#include <simulations/movers/TranslateAtom.hh>

#include <core/data/basic/Vec3I.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>

namespace simulations {
namespace movers {

core::index2 TranslateAtom::move(const core::index2 n_moves, simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  core::index2 ret = 0;
  for (core::index2 i = 0; i < n_moves; i++) if (move(mc_scheme)) ret++;

  return ret;
}

void TranslateAtom::max_move_range(const double new_max_step) {

  max_step_ = new_max_step;
  logs << utils::LogLevel::INFO << "Translation range in each direction set to [-" << max_step_ << "," << max_step_ << "]\n";
  rand_coordinate = std::uniform_real_distribution<double>(-max_step_, max_step_);
}

bool TranslateAtom::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

    i_moved = rand_atom_index(generator);         // --- index of the moved atom
    logs << utils::LogLevel::FINER << "Moving at pos " << i_moved << "\n";

//  backup[i_moved].set(the_system[i_moved]);     // --- backup the current conformations
  // ---------- make the move
  double dx = rand_coordinate(generator);
  double dy = rand_coordinate(generator);
  double dz =  rand_coordinate(generator);
  the_system[i_moved].add( dx, dy, dz);
  inc_move_counter();
  the_system.prepare_for_scoring(i_moved);
  double delta_e = the_energy.delta_energy(backup, i_moved, the_system);

  // @todo This doesn't work for Wang-Landay sampling; total energy should be stored somewhere!
  if (!mc_scheme.test(0, delta_e)) {
    undo();
    return false;
  } else {
#ifdef DEBUG
      double total_after = the_energy.energy(the_system);
      double total_before = the_energy.energy(backup);

      double delta_total = (total_after - total_before);
      if ((fabs(delta_e - delta_total) > 0.01) && (delta_total < 10000.0)) {
          std::cerr << "local delta: " << delta_e << "\n";
          std::cerr << "global delta: " << (total_after - total_before) << "\n";
          std::cerr << "energy error is " << fabs(delta_e - delta_total) << "\n";
          throw std::runtime_error("TranslateAtom: inconsistent energy!");
      }
#endif

      backup[i_moved].set(the_system[i_moved]);
      backup.clear_after_scoring(i_moved);
      return true;
  }
}


}
}

