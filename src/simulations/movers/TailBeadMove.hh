#ifndef SIMULATIONS_CARTESIAN_MOVERS_TailBeadMove_HH
#define SIMULATIONS_CARTESIAN_MOVERS_TailBeadMove_HH

#include <vector>
#include <core/index.hh>
#include <core/calc/statistics/Random.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/movers/Mover.hh>
#include <simulations/sampling/AbstractAcceptanceCriterion.hh>


#include <utils/string_utils.hh>
#include <utils/Logger.hh>
#include <simulations/systems/CartesianChains.hh>

namespace simulations {
namespace movers {


class TailBeadMove : public simulations::movers::Mover {
public:

  TailBeadMove(systems::CartesianChains &system, systems::CartesianChains &backup, forcefields::ByAtomEnergy &energy,
      core::index2 n_moved=6);

  /** @brief This method actually does not adjust anything.
   *
   * Since this mover places a tail bead in a random position on a sphere, this method is unnecessary; provided only
   * to implement the interface
   * @param new_max_step - unused
   */
  virtual void max_move_range(const double new_max_step) ;

  virtual double max_move_range() const ;

  inline bool move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme);

  inline void undo();

  virtual const std::string &  name() const;

    core::index4 tail = 0; /// Which bead has been moved recently
    core::index4 other_end = 0;

private:
  std::vector<core::index4> tail_at_start;
  std::vector<core::index4> tail_at_end;

  double max_step_;
  systems::CartesianChains & the_system;
  forcefields::ByAtomEnergy & energy;
  core::index2 n_moved_;
  systems::CartesianChains& backup;
  core::calc::statistics::Random & generator = core::calc::statistics::Random::get();
  std::uniform_int_distribution<core::index2> which_end_generator;
  std::uniform_real_distribution<double> rand_coordinate;
  utils::Logger logger;
  static const std::string name_;

  void add_tails(core::index4 tail_start, core::index4 tail_end) ;
};

}
}

#endif
