#ifndef SIMULATIONS_CARTESIAN_MOVERS_TranslateAtom_HH
#define SIMULATIONS_CARTESIAN_MOVERS_TranslateAtom_HH

#include <random>
#include <memory>

#include <core/calc/statistics/Random.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/movers/Mover.hh>
#include <simulations/sampling/AbstractAcceptanceCriterion.hh>

#include <simulations/systems/CartesianAtoms.hh>

namespace simulations {
namespace movers {

/** @brief Translates an atom by a random vector in Cartesian space.
 *
 */
class TranslateAtom : public simulations::movers::Mover {
public:

  /** @brief Creates a mover for a given set of atoms and a given energy function
   *
   * @param system - the set of atoms that are subjected to moves
   * @param backup - a conformation identical to the system, that is used to store coordinates before a moved is proposed
   * @param energy - energy function used to accept or to deny a move in a Monte Carlo process
   */
  TranslateAtom(simulations::systems::CartesianAtoms & system, simulations::systems::CartesianAtoms & backup,
                simulations::forcefields::ByAtomEnergy & energy) :
      Mover("TranslateAtom"), the_system(system), backup(backup), the_energy(energy), logs("TranslateAtom") {
    i_moved = 0;
    max_move_range(0.2);
    rand_coordinate = std::uniform_real_distribution<double>(-Mover::max_move_range(), Mover::max_move_range());
    rand_atom_index = std::uniform_int_distribution<core::index4>(0, the_system.n_atoms - 1);
  }

  /** @brief Makes <code>n_moves</code> independent moves.
   *
   * @param n_moves - how many moves should be attempted
   * @param mc_scheme - criterion used to apply or to deny a move
   * @return the number of successful moves
   */
  core::index2 move(const core::index2 n_moves, simulations::sampling::AbstractAcceptanceCriterion & mc_scheme);

  /** @brief Define the maximal displacement for this mover.
   *
   * @param new_max_step - largest possible displacement along each axis
   */
  virtual void max_move_range(const double new_max_step);

  /** @brief Attempts a single move.
   *
   * The single move event includes:
   *    - random selection of a atom of the system
   *    - random selection of a displacement vector
   *    - acceptance criterion call
   *    - reverting of the move if the criterion test failed
   *
   * @param mc_scheme - a Monte Carlo acceptance criterion (e.g. Metropolis criterion)
   * @return true if the move was accepted, false otherwise
   */
  virtual bool move(simulations::sampling::AbstractAcceptanceCriterion & mc_scheme);

  /// Cancels the most recent move
  virtual void undo() {
    dec_move_counter();
    the_system[i_moved].set(backup[i_moved]);
    the_system.clear_after_scoring(i_moved);

  }

  /// Returns the name of this mover
  virtual const std::string &  name() const { return name_; }

private:
  int i_moved = -1;
  simulations::systems::CartesianAtoms & the_system;
  simulations::systems::CartesianAtoms & backup;
  simulations::forcefields::ByAtomEnergy & the_energy;
  std::uniform_int_distribution<core::index4> rand_atom_index;
  std::uniform_real_distribution<double> rand_coordinate;
  core::calc::statistics::Random & generator = core::calc::statistics::Random::get();
  utils::Logger logs;
};

}
}

#endif
