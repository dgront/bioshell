#include <simulations/movers/TailBeadMove.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace simulations {
namespace movers {


TailBeadMove::TailBeadMove(systems::CartesianChains &system, systems::CartesianChains &backup,
    forcefields::ByAtomEnergy &energy, core::index2 n_moved) : Mover("TailBeadMove"),
    max_step_(0.5), the_system(system), energy(energy), n_moved_(n_moved), backup(backup),
    rand_coordinate(-max_step_,max_step_),logger("TailBeadMove") {

  for (core::index2 ic = 0; ic < the_system.count_chains(); ++ic) {
    const auto &chain_range = the_system.atoms_for_chain(ic);
    add_tails(chain_range.first_atom, chain_range.last_atom);
    logger << utils::LogLevel::FINER << "Adding a tail this mover will sample: " << (int) chain_range.first_atom << "\n";
    logger << utils::LogLevel::FINER << "Adding a tail this mover will sample: " << (int) chain_range.last_atom << "\n";
  }
}

void TailBeadMove::max_move_range(const double new_max_step) {
  max_step_ = new_max_step;
  rand_coordinate = std::uniform_real_distribution<double>(-max_step_, max_step_);
  logger << utils::LogLevel::INFO << "Maximum move range set to [-" << -max_step_ << "," << max_step_ << "]\n";
}

double TailBeadMove::max_move_range() const { return max_step_; }

inline bool TailBeadMove::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  int mod;      // ---------- Move the first or the last bead or atom: -1 or 1
  const core::index2 which_vector = which_end_generator(generator) %2; //  %2 because we want to randomly pick 0 (tail_at_start vector) or 1(tail_at_end vector)
  if (which_vector == 1) {
    core::index1 ind = which_end_generator(generator) % tail_at_start.size();
    tail = tail_at_start[ind];
    other_end = tail + n_moved_ - 1;
    mod = 1;
    logger << utils::LogLevel::FINER << "moving the bead : " << (int) tail << "-" << (int) other_end << "\n";
  } else {
    core::index1 ind = which_end_generator(generator) % tail_at_end.size();
    tail = tail_at_end[ind];
    other_end = int(tail) - n_moved_ + 1;
    mod = -1;
    logger << utils::LogLevel::FINER << "moving the bead : " << (int) other_end << "-" << (int) tail << "\n";
  }

  double f = 1.0 / (1.0 + n_moved_);
  double dx = rand_coordinate(generator) * f;
  double dy = rand_coordinate(generator) * f;
  double dz = rand_coordinate(generator) * f;

  int j = int(n_moved_);
  for (int i = int(tail); j > 0; i += mod) {
    the_system[i].add(dx * j, dy * j, dz * j);
    j--;
  }

  if (tail > other_end) std::swap(tail, other_end);
  the_system.prepare_for_scoring(tail,other_end);

  double after = (tail == other_end) ? energy.delta_energy(backup, tail, the_system) :
      energy.delta_energy(backup, tail, other_end, the_system);

  inc_move_counter();
  if (mc_scheme.test(0, after)) {
    logger << utils::LogLevel::FINEST << "move accepted!\n";
    logger << utils::LogLevel::FINEST << "delta energy " << after << "\n";
#ifdef DEBUG
      double en_before = energy.energy(backup);
      double en_after = energy.energy(the_system);
    if (fabs(after - en_after + en_before ) > 0.001) {
      std::cout << "global delta energy is " << en_after - en_before << "\n";
      std::cout << "local delta energy is " << after << "\n";

      std::cout << en_after << " " << en_before << " " << " " << after<< "\n";
      throw std::runtime_error("TailBeadMove: inconsistent energy!");
    }
#endif
    for (auto i=tail;i<=other_end;++i) backup[i].set(the_system[i]);
    backup.clear_after_scoring(tail,other_end);
    return true;
  } else {
    undo();
    logger << utils::LogLevel::FINEST << "move cancelled!\n";
    logger << utils::LogLevel::FINEST << "delta energy " << after << "\n";
    return false;
  }
}

inline void TailBeadMove::undo() {
  dec_move_counter();
  for (auto i=tail;i<=other_end;++i) the_system[i].set(backup[i]);
  the_system.clear_after_scoring(tail,other_end);

}

const std::string &TailBeadMove::name() const { return name_; }

void TailBeadMove::add_tails(core::index4 tail_start, core::index4 tail_end) {
  tail_at_start.push_back(tail_start);
  tail_at_end.push_back(tail_end);
}


const std::string TailBeadMove::name_ = "TailBeadMove";
}
}