
#include <core/data/basic/Vec3Cubic.hh>

#include <simulations/movers/TranslateMolecule.hh>

namespace simulations {
namespace movers {


TranslateMolecule::TranslateMolecule(simulations::systems::CartesianChains & system,
                simulations::systems::CartesianChains & backup,
                simulations::forcefields::ByAtomEnergy & energy) :
    Mover("TranslateMolecule"), the_system(system), the_energy(energy), backup(backup),
    logs("TranslateMolecule") {

  max_step_ = 0.25;             // --- 0.25 Angstrom move by default
  rand_coordinate = std::uniform_real_distribution<double>(-max_step_, max_step_);
  rand_molecule_index = std::uniform_int_distribution<core::index4>(0, the_system.count_residues() - 1);
}


core::index2 TranslateMolecule::move(const core::index2 n_moves, simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  core::index2 ret = 0;
  for (core::index2 i = 0; i < n_moves; i++) if (move(mc_scheme)) ret++;

  return ret;
}


void TranslateMolecule::max_move_range(const double new_max_step) {

  max_step_ = new_max_step;
  logs << utils::LogLevel::INFO << "Translation range in each direction set to [-" << max_step_ << "," << max_step_ << "]\n";
  rand_coordinate = std::uniform_real_distribution<double>(-max_step_, max_step_);
}


void TranslateMolecule::max_move_range_allowed(const double new_range_max) {
  max_range_allowed_ = new_range_max;
}


bool TranslateMolecule::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  core::index4 i_moved = rand_molecule_index(generator);
  logs << utils::LogLevel::FINE << "moving molecule " << i_moved  << "\n";

  const simulations::systems::AtomRange & moved = the_system.atoms_for_residue(i_moved);
  first_moved_ = moved.first_atom;
  last_moved_ = moved.last_atom;

  double cx = rand_coordinate(generator);
  double cy = rand_coordinate(generator);
  double cz = rand_coordinate(generator);
  for (core::index4 i = first_moved_; i <= last_moved_; ++i) {
    backup[i].set(the_system[i]);
    the_system[i].add( cx, cy, cz);
  }
  inc_move_counter();

  double delta_e = the_energy.delta_energy(backup, first_moved_, the_system);
#ifdef DEBUG
  double total_after = the_energy.energy(the_system);
  double total_before = the_energy.energy(backup);

  double delta_total = (total_after - total_before);
  if ((fabs(delta_e - delta_total) > 0.1) && (delta_total < 1000.0)) {
    std::cerr << "local delta: " << delta_e << "\n";
    std::cerr << "global delta: " << (total_after - total_before) << "\n";
    std::cerr << "energy error is " << fabs(delta_e - delta_total) << "\n";
    throw std::runtime_error(utils::string_format("TranslateMolecule: inconsistent energy in move %d by atoms %d - %d!",
                                                  n_total, first_moved_, last_moved_));
  }
#endif

  if (!mc_scheme.test(0, delta_e)) {
    undo();
    return false;
  } else {
    for (core::index4 i = first_moved_; i <= last_moved_; ++i)
      backup[i].set(the_system[i]);
    return true;
  }
}


void TranslateMolecule::undo() {

  dec_move_counter();

  for (core::index4 i = first_moved_; i <= last_moved_; ++i)
    the_system[i].set(backup[i]);
}


}
}

