#ifndef SIMULATIONS_CARTESIAN_MOVERS_RotateAASideChains_HH
#define SIMULATIONS_CARTESIAN_MOVERS_RotateAASideChains_HH

#include <random>
#include <memory>
#include <algorithm>
#include <stdexcept>

#include <core/calc/statistics/Random.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/chemical/Molecule.hh>
#include <core/chemical/molecule_utils.hh>
#include <core/data/structural/Structure.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>
#include <utils/exit.hh>

#include <simulations/systems/CartesianChains.hh>
#include <simulations/forcefields/mm/MMResidueType.hh>
#include <simulations/forcefields/mm/MMBondedParameters.hh>
#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/movers/Mover.hh>
#include <simulations/sampling/AbstractAcceptanceCriterion.hh>

namespace simulations {
namespace movers {

/** \brief Mover that rotates side chain atoms around a randomly selected Chi angle.
 *
 */
class RotateAASideChains : public simulations::movers::Mover {
private:

  /// Data structure holds data necessary for a single rotational degree of freedom
  struct OneRotation {
    core::index4 my_residue;
    core::index4 rotable_1;
    core::index4 rotable_2;
    std::vector<core::index4> to_be_rotated;
  };

public:
  /** \brief Mover that rotates side chain atoms around a specified bond.
   *
   * @param system - a system whose atoms will be altered
   * @param res_types - defines rotable bonds
   * @param energy - energy function called at every move to accept or cancel it
   */

  /** @brief Creates a mover for a given system
   * @param system - a system to be sampled
   * @param structure - a biomacromolecular structure object (loaded from PDB) corresponding to the modeled system.
   *    The structure is necessary to create the mover, i.e. to establish rotable bonds
   * @param backup - a deep copy of the sampled system
   * @param res_types - residue typing map provides a list of rotable bonds
   * @param energy - energy function used to score the modeled system
   */
  RotateAASideChains(systems::CartesianChains &system, const core::data::structural::Structure & structure,
                     systems::CartesianChains &backup, const std::map<std::string, simulations::forcefields::mm::MMResidueType> &res_types,
                     forcefields::ByAtomEnergy &energy);

  /** @brief Defines the rotation maximum angle for this mover.
   *
   * When this mover rotates a chain fragment, the angle of rotation must be in the range @code [-max_range,max_range] @endcode.
   * The move range can be automatically adapted by AdjustMoversAcceptance but the value cannot exceed Mover::max_move_range_allowed()
   * @param new_max_step - the new move range value
   */
  void max_move_range(const double new_max_step) override;

  bool move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) override;

  inline void undo() override;

  /// Returns the number of rotation degrees of freedom this mover can alter
  core::index4 count_dofs() { return rotables.size(); }

private:
  systems::CartesianChains &the_system;
  systems::CartesianChains &backup;
  std::vector<OneRotation> rotables;
  simulations::forcefields::ByAtomEnergy &energy;
  std::uniform_real_distribution<double> rand_coordinate;
  core::calc::statistics::Random &generator = core::calc::statistics::Random::get();
  utils::Logger logger;
  core::index2 last_rotation_index;
  core::calc::structural::transformations::Rototranslation rt_operation;
};


}
}

#endif
