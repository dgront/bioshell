#include <core/data/basic/Vec3.hh>
#include <core/data/basic/Vec3I.hh>

#include <simulations/movers/RotateRigidMolecule.hh>
#include <simulations/movers/movers_utils.hh>
#include <simulations/systems/AtomRange.hh>
#include <simulations/systems/CartesianChains.hh>

#include <utils/exit.hh>

namespace simulations {
namespace movers {

RotateRigidMolecule::RotateRigidMolecule(systems::CartesianChains & system, systems::CartesianChains & backup,
          forcefields::ByAtomEnergy & energy, core::index2 center_atom) :
    Mover("RotateRigidMolecule"), the_system(system), backup(backup), energy(energy), logs("RotateRigidMolecule") {

  center_atom_ = center_atom;
  max_step_ = 0.5236;             // --- 30 degrees in radians
  rand_coordinate = std::uniform_real_distribution<double>(-max_step_, max_step_);
}


void RotateRigidMolecule::max_move_range(const double new_max_step) {

  Mover::max_move_range(new_max_step);
  logs << utils::LogLevel::INFO << "rotation range set to [-" << Mover::max_move_range() << "," << Mover::max_move_range() << "]\n";
  rand_coordinate = std::uniform_real_distribution<double>(-Mover::max_move_range(), Mover::max_move_range());
}


bool RotateRigidMolecule::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  recent_move_radians_ = rand_coordinate(generator);
  random_vector_on_sphere(recent_move_axis_);
  core::index4 i_moved = rand_molecule_index(generator);
  logs << utils::LogLevel::FINE << "rotating molecule " << i_moved << " around : " << recent_move_axis_ << " by " << recent_move_radians_ << "\n";

  const simulations::systems::AtomRange & moved = the_system.atoms_for_residue(i_moved);

  first_moved_ = moved.first_atom;
  last_moved_ = moved.last_atom;

  core::data::basic::Vec3 cm{float(the_system[first_moved_ + center_atom_].x()),
          float(the_system[first_moved_ + center_atom_].y()),float(the_system[first_moved_ + center_atom_].z())};
  const core::data::basic::Vec3I & cm_i = the_system[first_moved_ + center_atom_];

  core::calc::structural::transformations::Rototranslation::around_axis(recent_move_axis_, recent_move_radians_, cm, rot);

  core::data::basic::Vec3 rot_tmp;
  core::index4 rot_index = first_moved_ + center_atom_;
  for (core::index4 i = first_moved_; i < rot_index; ++i) {
    backup[i].set(the_system[i]);
    rot_tmp.x = cm.x + the_system[i].closest_delta_x(cm_i);
    rot_tmp.y = cm.y + the_system[i].closest_delta_y(cm_i);
    rot_tmp.z = cm.z + the_system[i].closest_delta_z(cm_i);

    rot.apply(rot_tmp);
    the_system[i].set(rot_tmp);
  }
  for (core::index4 i = rot_index + 1; i <= last_moved_; ++i) {
    backup[i].set(the_system[i]);
    rot_tmp.x = cm.x + the_system[i].closest_delta_x(cm_i);
    rot_tmp.y = cm.y + the_system[i].closest_delta_y(cm_i);
    rot_tmp.z = cm.z + the_system[i].closest_delta_z(cm_i);

    rot.apply(rot_tmp);
    the_system[i].set(rot_tmp);
  }

  inc_move_counter();

  double delta_e = energy.delta_energy(backup, i_moved, the_system);

  if (!mc_scheme.test(0, delta_e)) {
    undo();
    return false;
  } else {
    for (core::index4 i = first_moved_; i <= last_moved_; ++i)
      backup[i].set(the_system[i]);
    return true;
  }
}

void RotateRigidMolecule::undo() {

  dec_move_counter();
  for (core::index4 i = first_moved_; i <= last_moved_; ++i)
    the_system[i].set(backup[i]);
}


}
}

