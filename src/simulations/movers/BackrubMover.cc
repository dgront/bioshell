#include <random>

#include <simulations/movers/BackrubMover.hh>

#include <utils/Logger.hh>

namespace simulations {
namespace movers {


BackrubMover::BackrubMover(systems::CartesianChains &system, const core::data::structural::Structure & structure,
    systems::CartesianChains &backup, forcefields::ByAtomEnergy &energy,
    const std::string &pdb_atom_name, const core::index2 max_sequence_separation, const double max_step):
    SwingChainFragment(system, backup, energy), logger("BackrubMover") {

  max_separation_ = max_sequence_separation;
  min_separation_ = 2;
  core::index4 i=0;
  for(auto ait = structure.first_const_atom();ait!=structure.last_const_atom();++ait) {
    if ((**ait).atom_name() == pdb_atom_name)
      pivot_indexes_.push_back(i);
    ++i;
  }

  rand_atom_index = std::uniform_int_distribution<core::index4>(0, pivot_indexes_.size() - 1 - min_separation_);
  max_move_range(max_step);
}


bool BackrubMover::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  do {
    core::index4 i_idx = rand_atom_index(generator);
    core::index2 step = rand_atom_index(generator) % (max_separation_ - min_separation_ + 1) + min_separation_;
    core::index4 j_idx = i_idx + step;
    if (j_idx >= pivot_indexes_.size()) {
      j_idx = pivot_indexes_.size() - 1;
      i_idx = j_idx - step;
    }
    moved_from = pivot_indexes_[i_idx];
    moved_to = pivot_indexes_[j_idx];
  } while (the_system[moved_from].chain_id != the_system[moved_to].chain_id);

  double angle = rand_coordinate(generator);

  return SwingChainFragment::move(mc_scheme, moved_from, moved_to, angle);
}

}
}

