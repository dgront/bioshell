#ifndef SIMULATIONS_CARTESIAN_MOVERS_BackrubMover_HH
#define SIMULATIONS_CARTESIAN_MOVERS_BackrubMover_HH

#include <memory>

#include <simulations/movers/SwingChainFragment.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>
#include <simulations/systems/CartesianChains.hh>

namespace simulations {
namespace movers {

/** @brief Backrub mover swings a segment of a protein backbone around an axis connecting two atoms,
 * alpha carbons
 */
class BackrubMover : public SwingChainFragment {
public:

  BackrubMover(systems::CartesianChains &system, const core::data::structural::Structure & structure,
      systems::CartesianChains &backup, forcefields::ByAtomEnergy & energy,
               const std::string &pdb_atom_name = " CA ", const core::index2 max_sequence_separation = 3,
               const double max_step = 0.01);

  ~BackrubMover() override = default;

  bool move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) override;

private:
  core::index2 max_separation_;
  core::index2 min_separation_;
  utils::Logger logger;
  std::vector<core::index4> pivot_indexes_;   //< Indexes of pivot atoms, typically CA
  static const std::string name_;
};


}
}

#endif
