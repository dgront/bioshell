#ifndef SIMULATIONS_CARTESIAN_MOVERS_PhiPsiMover_HH
#define SIMULATIONS_CARTESIAN_MOVERS_PhiPsiMover_HH

#include <random>
#include <memory>

#include <core/calc/statistics/Random.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/movers/Mover.hh>
#include <simulations/sampling/AbstractAcceptanceCriterion.hh>

#include <simulations/systems/CartesianChains.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace simulations {
namespace movers {

/** \brief Mover that affects Phi or Psi dihedral angle
 *
 */
class PhiPsiMover : public simulations::movers::Mover {
public:

  enum class DOFs {
    Phi,    // --- Enum key to identify Phi rotation
    Psi,     // --- Enum key to identify Psi rotation
    both
  };

  /** \brief Mover that affects Phi or Psi dihedral angle
   *
   * @param system - a system whose atoms will be rotated
   * @param energy - energy function called at every move to accept or cancel it
   */
  PhiPsiMover(systems::CartesianChains &system, const core::data::structural::Structure & structure,
      systems::CartesianChains &backup, simulations::forcefields::ByAtomEnergy &energy);

  bool move(simulations::sampling::AbstractAcceptanceCriterion & mc_scheme) override ;

  inline void undo() override {}

  void max_move_range(const double max_range) override;

  /** @brief Switches off or on sampling for a number of residues
 *
 * @param set_fixed - if true, the given residue will not be moved by this mover; if true - the otherwise
 * @param which_residues - which residues to be affected
 */
  inline void freeze( bool set_fixed, std::vector<core::index2> which_residues, DOFs which_dof) {

    for(core::index2 i : which_residues) freeze(set_fixed, i, which_dof);
  }

  /** @brief Switches off or on sampling for a given residue
   *
   * @param set_fixed - if true, the given residue will not be moved by this mover; if true - the otherwise
   * @param which_residue - which residue to be affected
   * @param which_dof - degree of freedom to be affected: Phi, Psi or both
   */
  inline void freeze( bool set_fixed, core::index2 which_residue, DOFs which_dof) {
    if (which_dof == DOFs::Phi || which_dof == DOFs::both)
      freeze(set_fixed, which_residue, which_dof, movemap_phi_);
    if (which_dof == DOFs::Psi || which_dof == DOFs::both)
      freeze(set_fixed, which_residue, which_dof, movemap_psi_);
  }

  /// Returns the number of rotation degrees of freedom this mover can alter
  core::index4 count_dofs() { return movemap_phi_.size() + movemap_psi_.size(); }

  /** @brief makes all residues moveable or frozen.
   *
   * @param set_fixed - if true, all residues will be frozen for this mover
   */
  void freeze(bool set_fixed);

  /// Provides the index of the last atom affected by the most recent move
  core::index4 recently_moved_residue() const { return moved_res_; }

  /// Provides the angle (in radians) of the most recent move
  double recent_rotation_angle() const { return angle_;}

  /// Says whether the most recent rotation was around Phi or Psi angle
  DOFs recent_move_type() const { return move_type_;}

  /// Provides the rototranslation object of the most recent move
  const core::calc::structural::transformations::Rototranslation & recent_transformation() const { return rot_; }

private:
  DOFs move_type_;
  core::index2 moved_res_;
  core::index4 first_rotated;
  core::index4 last_rotated;
  double angle_;
  systems::CartesianChains & system;
  systems::CartesianChains & backup;
  forcefields::ByAtomEnergy & energy;
  core::calc::structural::transformations::Rototranslation rot_;
  /// The ncacoh vector holds indexes of N, CA, C, O and amide H atoms from every residue, i.e. ncacoh[5*i]
  /// holds the index of the N atom of the i-th residue; CA is ncacoh[5*i + 1] and so on.
  /// The atom itself can be accessed as system[ncacoh[5*i]]
  std::vector<core::index4> ncacoh;
  std::vector<core::index2> movemap_phi_;       ///< residues for which Phi move is allowed
  std::vector<core::index2> movemap_psi_;       ///< residues for which Psi move is allowed
  std::uniform_real_distribution<double> rand_coordinate;
  core::calc::statistics::Random & generator = core::calc::statistics::Random::get();
  utils::Logger logger;

  void perform_rotation(const core::data::basic::Vec3I & from,
      const core::data::basic::Vec3I & to);  ///< Helper function that actually rotate the relevant atoms

  void freeze(bool set_fixed, core::index2 which_residue, DOFs which_dof, std::vector<core::index2> &mmap);

  inline core::index4 index_N(core::index2 which_res) const { return ncacoh[which_res * 5 + 0]; }
  inline core::index4 index_CA(core::index2 which_res) const { return ncacoh[which_res * 5 + 1]; }
  inline core::index4 index_C(core::index2 which_res) const { return ncacoh[which_res * 5 + 2]; }
  inline core::index4 index_O(core::index2 which_res) const { return ncacoh[which_res * 5 + 3]; }
  inline core::index4 index_H(core::index2 which_res) const { return ncacoh[which_res * 5 + 4]; }
  inline core::data::basic::Vec3I atom_N(core::index2 which_res) { return system[ncacoh[which_res * 5 + 0]]; }
  inline core::data::basic::Vec3I atom_CA(core::index2 which_res) { return system[ncacoh[which_res * 5 + 1]]; }
  inline core::data::basic::Vec3I atom_C(core::index2 which_res) { return system[ncacoh[which_res * 5 + 2]]; }
  inline core::data::basic::Vec3I atom_O(core::index2 which_res) { return system[ncacoh[which_res * 5 + 3]]; }
  inline core::data::basic::Vec3I atom_H(core::index2 which_res) { return system[ncacoh[which_res * 5 + 4]]; }
};


}
}

#endif
