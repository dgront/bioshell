#include <algorithm>
#include <stdexcept>

#include <core/calc/structural/transformations/Rototranslation.hh>

#include <simulations/movers/SwingChainFragment.hh>
#include "movers_utils.hh"

namespace simulations {
namespace movers {

using core::calc::structural::transformations::Rototranslation;

using simulations::systems::CartesianAtoms;

SwingChainFragment::SwingChainFragment(CartesianAtoms & system, CartesianAtoms & backup,
    forcefields::ByAtomEnergy & energy, core::index2 n_moved) : Mover("SwingChainFragment"),
    the_system(system), backup(backup), the_energy_(energy), logger("SwingChainFragment") {

  n_moved_ = n_moved;
  angle_ = 0;
  rand_atom_index = std::uniform_int_distribution<core::index4>(0, the_system.n_atoms - 1 - n_moved_);
  // ---------- Default maximum move range is 0.01 [rad] = 3.6 [deg] in one direction (7.2 [deg] both sides)
  max_move_range(0.01);
}

bool SwingChainFragment::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme,
    core::index4 start_pos, core::index4 end_pos, double angle) {

  moved_from = start_pos;                         // --- select the first moved atom
  moved_to = end_pos;
  angle_ = angle;
    logger << utils::LogLevel::FINER << "Moving at pos "<<moved_from<<" "<<moved_to<<"\n";


    backup_and_rotate(angle); // ---------- The move itself
    the_system.prepare_for_scoring(moved_from+1,moved_to);
    double delta_en = the_energy_.delta_energy(backup,moved_from, moved_to, the_system);
  inc_move_counter();

#ifdef DEBUG
  self_test();
  double en_after = the_energy_.energy(the_system);
  double en_before = the_energy_.energy(backup);
  if (fabs(en_after - en_before - delta_en) > 0.001)
    throw std::runtime_error("SwingChainFragment: inconsistent energy!");
#endif
  if (!mc_scheme.test(0, delta_en)) {
    undo();
    logger << utils::LogLevel::FINEST << "move cancelled!\n";
    return false;
  } else {
    logger << utils::LogLevel::FINEST <<moved_from<<" "<<moved_to<<" move accepted!\n";
    for (core::index4 i = moved_from +1; i < moved_to; i++)
      backup[i].set(the_system[i]);
    backup.clear_after_scoring(moved_from+1,moved_to);

    return true;
  }
}

bool SwingChainFragment::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  core::index4 start_pos, end_pos;
  do {
    start_pos = rand_atom_index(generator);        // --- select the first moved atom
    end_pos = moved_from + n_moved_ - 1;          // --- last moved
  } while (the_system[start_pos].chain_id != the_system[end_pos].chain_id);

  double angle = rand_coordinate(generator);

  return move(mc_scheme, start_pos, start_pos + n_moved_ - 1, angle);
}

inline void SwingChainFragment::undo() {
  dec_move_counter();
  for (size_t i = moved_from + 1; i < moved_to; i++)
    the_system[i].set(backup[i]);
  the_system.clear_after_scoring(moved_from+1,moved_to);

}


void SwingChainFragment::backup_and_rotate(const double angle) {

  Vec3 from{the_system[moved_from]}, to{the_system[moved_to]};
  Vec3 axis(to);
  axis -= from;
  axis.norm();
  Rototranslation::around_axis(axis, angle, from, rot_);
  for (core::index4 i = moved_from + 1; i < moved_to; i++) {
  //  backup[i].set(the_system[i]);
    rotate_atom(the_system[i-1], the_system[i], rot_);
  }
}

void SwingChainFragment::max_move_range(const double max_range) {

  Mover::max_move_range(max_range);
  logger << utils::LogLevel::INFO << "Rotation range set to [-" << max_range << "," << max_range << "]\n";
  rand_coordinate = std::uniform_real_distribution<double>(-Mover::max_move_range(), Mover::max_move_range());
}

bool SwingChainFragment::self_test() {

  for (core::index4 i = moved_from + 1; i <= moved_to; i++) {
    double d_old = sqrt(backup[i].closest_distance_square_to(backup[i - 1]));
    double d_new = sqrt(the_system[i].closest_distance_square_to(the_system[i - 1]));
    if (fabs(d_old - d_new) > 0.01) {
      std::string s = utils::string_format(
          "Bond length affected after a pivot move!\nThe pivots were: %d %d\nThe broken bond between %d and %d should be %f but is %f",
          moved_from, moved_to, i - 1, i, d_old, d_new);
      logger << utils::LogLevel::CRITICAL << s << "\n";
      throw std::runtime_error(s);
    }
  }
  logger << utils::LogLevel::FINEST << "move OK\n";

  return true;
}

}
}
