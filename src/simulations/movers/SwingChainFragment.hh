#ifndef SIMULATIONS_CARTESIAN_MOVERS_SwingChainFragment_HH
#define SIMULATIONS_CARTESIAN_MOVERS_SwingChainFragment_HH

#include <random>
#include <memory>

#include <core/calc/statistics/Random.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/movers/Mover.hh>
#include <simulations/sampling/AbstractAcceptanceCriterion.hh>
#include <simulations/systems/CartesianAtoms.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace simulations {
namespace movers {

using simulations::systems::CartesianAtoms;

class SwingChainFragment : public Mover {
public:

  /**
   * @brief Creates a mover for a given system
   * @param system - a system to be sampled
   * @param backup - a backup copy of the system (both should be identical)
   * @param energy - energy function
   * @param n_moved - the number of atoms affected by a single move
   */
  SwingChainFragment(CartesianAtoms & system, CartesianAtoms & backup,
      forcefields::ByAtomEnergy & energy, core::index2 n_moved=6);

  /** @brief Defines the rotation maximum angle for this mover.
   *
   * When this mover rotates a chain fragment, the angle of rotation must be in the range @code [-max_range,max_range] @endcode.
   * The move range can be automatically adapted by AdjustMoversAcceptance but the value cannot exceed Mover::max_move_range_allowed()
   * @param max_range - the new move range value
   */
  void max_move_range(const double max_range) override;

  bool move(simulations::sampling::AbstractAcceptanceCriterion & mc_scheme) override;

  /** @brief Introduce a desired change into a system.
   *
   * This method makes a non-random change ad is called by a @code move(AbstractAcceptanceCriterion) @endcode
   * with randomly selected parameters of a move
   *
   * @param mc_scheme - Monte Carlo acceptance criterion
   * @param start_pos - the first anchor of the moved fragment
   * @param end_pos  - the second anchor of the moved fragment
   * @param angle - angle of rotation
   * @return true if the move was accepted
   */
  bool move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme,
      core::index4 start_pos, core::index4 end_pos, double angle);

  inline void undo() override;

  /// Provides the index of the first atom affected by the most recent move
  core::index4 recently_moved_from() const { return moved_from; }

  /// Provides the index of the last atom affected by the most recent move
  core::index4 recently_moved_to() const { return moved_to; }

  /// Provides the angle (in radians) of the most recent move
  double recent_rotation_angle() const { return angle_;}

  /// Provides the rototranslation object of the most recent move
  const core::calc::structural::transformations::Rototranslation & recent_transformation() const { return rot_; }

protected:
  core::index4 moved_from = 0, moved_to = 0;
  double angle_;
  CartesianAtoms & the_system;
  CartesianAtoms & backup;
  std::uniform_int_distribution<core::index4> rand_atom_index;
  std::uniform_real_distribution<double> rand_coordinate;
  core::calc::statistics::Random & generator = core::calc::statistics::Random::get();
  forcefields::ByAtomEnergy & the_energy_;
  core::calc::structural::transformations::Rototranslation rot_;

  bool self_test();

  inline void backup_and_rotate(const double angle);

private:
  double max_range_allowed_;
  core::index2 n_moved_;
  utils::Logger logger;
};

}
}

#endif
