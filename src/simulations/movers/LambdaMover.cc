#include <algorithm>
#include <stdexcept>

#include <simulations/movers/LambdaMover.hh>
#include <simulations/movers/movers_utils.hh>
#include <core/calc/structural/angles.hh>


namespace simulations {
namespace movers {

LambdaMover::LambdaMover(systems::CartesianChains &system, const core::data::structural::Structure &structure,
    systems::CartesianChains &backup, simulations::forcefields::ByAtomEnergy &energy) :
    Mover("LambdaMover"), system(system), backup(backup), energy(energy), logger("LambdaMover") {

  max_move_range(0.1);
  rand_res_index = std::uniform_int_distribution<core::index4>(0, system.count_residues() - 2);

    std::vector<std::string> bb_index_to_name{" N  ", " CA ", " C  ", " O  "};
    std::map<std::string, int> bb_names_to_index{{" N  ", 0},
                                                 {" CA ", 1},
                                                 {" C  ", 2},
                                                 {" O  ", 3}};
    std::vector<std::string> atom_pdb_names;

    max_move_range(0.1);
    ncaco.resize(system.count_residues() * 4), std::numeric_limits<core::index4>::max();

    core::index4 ia = 0;
    core::index4 ir = 0;
//    for (auto ir=0;ir<system.count_residues();++ir){
//        auto range = system.atoms_for_residue(ir);
//        for (auto ia=range.first_atom;ia<=range.last_atom;++ia) {
//            auto type = system[ia].atom_type;
//            ncaco[ir * 4 + type - 1] = ia; //types are 1,2,3,4 and indexes are 0,1,2,3
//        }
//    }

    for (auto r_it = structure.first_const_residue(); r_it != structure.last_const_residue(); ++r_it) {
        const core::data::structural::Residue &r = (**r_it);
        for (auto a_sp: r) {
            if (bb_names_to_index.find(a_sp->atom_name()) != bb_names_to_index.cend()) {
                int ia_bb = bb_names_to_index[a_sp->atom_name()];
                ncaco[ir * 4 + ia_bb] = ia;
            }
            ++ia;
        }
        ++ir;
    }
}

/** @brief Define the maximal torsion rotation angle for this mover.
 *
 * @param new_max_step - largest possible change of each dihedral angle (in radians, in the range [0,pi])
 */
void LambdaMover::max_move_range(const double max_range) {


  Mover::max_move_range(max_range);
  logger << utils::LogLevel::INFO << "Torsion rotation range set to [-" << Mover::max_move_range() << ","
         << Mover::max_move_range() << "]\n";
    rand_angle =     std::uniform_real_distribution<double>(-max_range,max_range);
}

void LambdaMover::perform_rotation(const core::data::basic::Vec3I & from,const core::data::basic::Vec3I & to) {

  // --- find rotation axis
  Vec3 axis{to}, start{from};
  axis -= start;
  axis.norm();
  core::calc::structural::transformations::Rototranslation::around_axis(axis, angle_, start, rot_);

  for (core::index4 i = first_rotated; i <= last_rotated; i++) {
    backup[i].set(system[i]);
    rotate_atom(from, system[i], rot_);
  }
}


bool LambdaMover::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  using core::calc::structural::transformations::Rototranslation;
  using core::data::basic::Vec3;
  using core::data::basic::Vec3I;

  inc_move_counter();
  angle_ = rand_angle(generator);                  // --- angle of rotation
  moved_res_ = rand_res_index(generator);                  // --- angle of rotation
  logger<< utils::LogLevel::FINE << "Moving lambda on residue "<<moved_res_<<"\n";
  first_rotated=ncaco[moved_res_*4+2];                 //C of moved residue
  last_rotated=ncaco[(moved_res_+1)*4];              // N of the next residue

  double delta_en;

    // --- find rotation axis CA -> CA+1
    Vec3 axis{system[ncaco[(moved_res_+1) * 4 + 1]]}, start{system[ncaco[moved_res_ * 4 + 1]]};
    axis -= start;
    axis.norm();
    core::calc::structural::transformations::Rototranslation::around_axis(axis, angle_, start, rot_);

//    double c = cos(angle_);
//    double s = sin(angle_);
//    rot_.rot_x(1,0,0);
//    rot_.rot_y(0,c,-s);
//    rot_.rot_z(0,s,c);
    //we move from C (2) to the N of next residue (4)
    for (core::index4 i=2;i<5;++i) {
        rotate_atom(system[ncaco[moved_res_ * 4+1]],system[ncaco[moved_res_ * 4 + i]],rot_);     // --- rotate by lambda
    }
    //auto first_calc = first_rotated -1 ? first_rotated > 0 :first_rotated;
    //auto last_calc = last -last_rotated ? first_rotated > 0 :first_rotated;

    delta_en = energy.delta_energy(backup, first_rotated, last_rotated, system);


    if (!mc_scheme.test(0, delta_en)) {
    dec_move_counter();
    for (core::index4 i = first_rotated; i <= last_rotated; i++) system[i].set(backup[i]);
    logger << utils::LogLevel::FINER << "move cancelled!\n";
    return false;
  } else {
#ifdef DEBUG
        double total_after = energy.energy(system);
        double total_before = energy.energy(backup);

        double delta_total = (total_after - total_before);
        if ((fabs(delta_en - delta_total) > 0.01) && (delta_total < 10000.0)) {
            std::cerr << "local delta: " << delta_en << "\n";
            std::cerr << "global delta: " << (total_after - total_before) << "\n";
            std::cerr << "energy error is " << fabs(delta_en - delta_total) << "\n";
            throw std::runtime_error("LambdaMover: inconsistent energy!");
        }
#endif
    logger << utils::LogLevel::FINER << "move accepted!\n";
    for (core::index4 i = first_rotated; i <= last_rotated; i++)
      backup[i].set(system[i]);

    return true;
  }
}


}
}

