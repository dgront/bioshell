#ifndef SIMULATIONS_CARTESIAN_MOVERS_movers_utils_HH
#define SIMULATIONS_CARTESIAN_MOVERS_movers_utils_HH

#include <random>
#include <memory>
#include <algorithm>
#include <stdexcept>

#include <core/data/basic/Vec3.hh>
#include <core/data/basic/Vec3I.hh>

namespace simulations {
namespace movers {

/** @brief Generates random vectors uniformly distributed on a unit sphere.
 * @param out - result will be stored in the given vector
 */
void random_vector_on_sphere(core::data::basic::Vec3 & out);

/** @brief Transforms a single atom by a given rototranslation
 * @param reference - a reference atom that is a very close neighbor of the rotated atom. This atom is necessary
 *  to define a periodic box copy to be rotated; this method rotates this PBC image of a given atom
 *  that is the closest to the given reference
 * @param rotated - atom to be rotated
 * @param rot - rototranslation transformation that actually defines the rotation
 */
void rotate_atom(const core::data::basic::Vec3I & reference, core::data::basic::Vec3I &rotated,
    const core::calc::structural::transformations::Rototranslation &rot);

}
}

#endif
