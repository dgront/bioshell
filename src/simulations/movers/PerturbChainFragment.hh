#ifndef SIMULATIONS_CARTESIAN_MOVERS_PerturbChainFragment_HH
#define SIMULATIONS_CARTESIAN_MOVERS_PerturbChainFragment_HH

#include <random>
#include <memory>

#include <core/calc/statistics/Random.hh>

#include <utils/Logger.hh>

#include <simulations/movers/Mover.hh>
#include <simulations/sampling/AbstractAcceptanceCriterion.hh>
#include <simulations/systems/CartesianChains.hh>
#include <simulations/forcefields/ByAtomEnergy.hh>

namespace simulations {
namespace movers {

/** @brief Moves N beads of a polymer chain
 *
 * This mover will displace N subsequent beads. The beads must belong to the very same chain. The first and last
 * bead of a chain will not be moved by this mover
 *
 * @tparam C - the type used to express coordinates; Vec3 for simple models, Vec3Cubic for modeling in periodic boundary conditions
 */
class PerturbChainFragment : public simulations::movers::Mover {
public:
  /** @brief Create a mover for a given system.
   *
   * @param system - a ResidueChain<C> object to be altered by this mover
   * @param n_moved - the number of atom affected by a single move (constant for all moves)
   * @param energy - energy function used to accept / deny a move
   */
  PerturbChainFragment(systems::CartesianChains &system,systems::CartesianChains &backup, core::index2 n_moved,
      forcefields::ByAtomEnergy &energy);

  /** @brief Make MC moves.
   *
   * This method calls <code>move(simulations::generic::sampling::AbstractAcceptanceCriterion &)</move> method
   * multiple times.
   * @param n_moves - how many moves to make
   * @param mc_scheme - an acceptance criterion, used to accept or deny a move based on a system's energy before and after the move
   */
  core::index2 move(const core::index2 n_moves, simulations::sampling::AbstractAcceptanceCriterion & mc_scheme);

  /// Sets the maximum range for a move
  void max_move_range(const double step) override;


  /// Provides the index of the first atom affected by the most recent move
  core::index4 recently_moved_from() const { return last_moved_from; }

  /// Provides the index of the last atom affected by the most recent move
  core::index4 recently_moved_to() const { return last_moved_to; }

  /** @brief Make MC moves.
   *
   * This method:
   *     - selects a a fragment to be moved
   *     - for each atom of the residue, it creates a random vector and move the atom by the vector
   *     - move is accepted (or cancelled) according to Monte Carlo criterion
   * @param mc_scheme - an acceptance criterion, used to accept or deny a move based on a system's energy before and after the move
   * @return true if the move got accepted; false otherwise
   */
   bool move(simulations::sampling::AbstractAcceptanceCriterion & mc_scheme) override ;

  /** @brief Cancels the most recent move.
   *
   * This method is automatically called by move(simulations::generic::sampling::AbstractAcceptanceCriterion &) when
   * the respective AbstractAcceptanceCriterion said so. User doesn't need to call <code>undo()</code> by himself.
   */
  void undo() override;

  /// Returns the name of this mover
  const std::string &  name() const override { return name_; }

private:
  /// Maximum range of a move for each coordinate.
  core::index2 n_moved_;
  core::index4 last_moved_from = 0, last_moved_to = 0;
  systems::CartesianChains & the_system;
  systems::CartesianChains & backup;
  forcefields::ByAtomEnergy & the_energy;
  std::uniform_int_distribution<core::index4> rand_bead_index;
  std::uniform_real_distribution<double> rand_coordinate;
  core::calc::statistics::Random & generator = core::calc::statistics::Random::get();
  utils::Logger logger;
};


}
}

#endif
