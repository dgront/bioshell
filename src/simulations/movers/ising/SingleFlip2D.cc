#include <string>

#include <simulations/movers/Mover.hh>
#include <simulations/movers/ising/SingleFlip2D.hh>

namespace simulations {
namespace movers {
namespace ising {

template<typename S, typename I>
bool SingleFlip2D<S, I>::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  I pos = generate_spins(rnd);
  double e_before = system_.point_energy(pos);
  S old = system_[pos];
  system_.flip_spin(pos);
//  double e_after = system_.point_energy(pos);
  inc_move_counter();
  if (mc_scheme.test(e_before, -e_before))
    return true;
  else {
    dec_move_counter();
    system_[pos] = old;
    return false;
  }
}

template
class SingleFlip2D<core::index1, core::index2>;

template
class SingleFlip2D<core::index1, core::index4>;

}
}
}
