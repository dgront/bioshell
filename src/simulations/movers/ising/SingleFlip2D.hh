#ifndef SIMULATIONS_ISING_SingleFlip2D_HH
#define SIMULATIONS_ISING_SingleFlip2D_HH

#include <string>
#include <random>

#include <core/calc/statistics/Random.hh>

#include <simulations/movers/Mover.hh>
#include <simulations/systems/ising/Ising2D.hh>

namespace simulations {
namespace movers {
namespace ising {

/** @brief Mover which randomly flips a spin of an Ising 2D system
 *
 */
template<typename S, typename I>
class SingleFlip2D : public movers::Mover {
public:

  /// Initialize the mover for a given system
  SingleFlip2D(simulations::systems::ising::Ising2D<S, I> &system) :
    Mover("SingleFlip2D"), system_(system), generate_spins(0, system.count_spins() - 1),
    rnd(core::calc::statistics::Random::get()) {}

  /// Empty default destructor
  virtual ~SingleFlip2D() = default;

  /// Makes the move
  virtual bool move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme);

  /// Does nothing, <code>move()</code> method un-does a move
  virtual void undo() {};

  virtual const std::string &name() const { return name_; }

  /** @brief This method actually does not adjust anything.
   *
   * Since this mover places a tail bead in a random position on a sphere, this method is unnecessary; provided only
   * to implement the interface
   * @param new_max_step - unused
   */
  virtual void max_move_range(const double new_max_step) {}

  /// Always return 0.0
  virtual double max_move_range() const { return 0.0; }

private:
  simulations::systems::ising::Ising2D<S, I> &system_;
  std::uniform_int_distribution<I> generate_spins;
  core::calc::statistics::Random &rnd;
};
class SingleFlip2DBB : public SingleFlip2D<core::index1,core::index2>  {};
}
}
}

#endif //SIMULATIONS_ISING_SingleFlip2D_HH
