
#include <string>
#include <random>

#include <core/calc/statistics/Random.hh>

#include <simulations/movers/Mover.hh>
#include <simulations/systems/ising/Ising2D.hh>
#include <simulations/movers/ising/WolffMove2D.hh>

namespace simulations {
namespace movers {
namespace ising {

template<typename S, typename I>
void WolffMove2D<S, I>::find_cluster(std::vector<I> &result, I point, S spin) {

  in_cluster_flags[point] = 1;

  const std::vector<S> &spins = system_.expose_vector();

  I t = system_.top(point);
  if (spins[t] == spin) {
    if (!in_cluster_flags[t]) {
//      in_cluster_flags[t] = true;
      result.push_back(t);
      find_cluster(result, t, spin);
    }
  }

  I b = system_.bottom(point);
  if (spins[b] == spin) {
    if (!in_cluster_flags[t]) {
//      in_cluster_flags[t] = true;
      result.push_back(t);
      find_cluster(result, t, spin);
    }
  }

  I l = system_.left(point);
  if (spins[l] == spin) {
    if (!in_cluster_flags[t]) {
//      in_cluster_flags[t] = true;
      result.push_back(t);
      find_cluster(result, t, spin);
    }
  }

  I r = system_.right(point);
  if (spins[r] == spin) {
    if (!in_cluster_flags[t]) {
//      in_cluster_flags[t] = true;
      result.push_back(t);
      find_cluster(result, t, spin);
    }
  }
};

template<typename S, typename I>
bool WolffMove2D<S, I>::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  std::fill(in_cluster_flags.begin(), in_cluster_flags.end(), false);
  spins_in_cluster.clear();
  I pos = generate_spins(rnd);
  S the_spin = system_[pos];

  find_cluster(spins_in_cluster, pos, the_spin);
//  std::cerr << "Flipping a cluster of size "<<spins_in_cluster.size()<<"\n";
  double e_before = 0.0;
  for (I ii : spins_in_cluster) e_before += system_.point_energy(ii);
  for (I ii : spins_in_cluster) system_.flip_spin(ii);
  double e_after = 0.0;
  for (I ii : spins_in_cluster) e_after += system_.point_energy(ii);

  inc_move_counter();
  if (mc_scheme.test(e_before, e_after))
    return true;
  else {
    dec_move_counter();
    for (I ii : spins_in_cluster) system_.flip_spin(ii);
    return false;
  }
}

template
class WolffMove2D<core::index1, core::index2>;

template
class WolffMove2D<core::index1, core::index4>;

}
}
}
