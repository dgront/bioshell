#ifndef SIMULATIONS_ISING_WolffMove2D_HH
#define SIMULATIONS_ISING_WolffMove2D_HH

#include <string>
#include <random>

#include <core/calc/statistics/Random.hh>

#include <simulations/movers/Mover.hh>
#include <simulations/systems/ising/Ising2D.hh>
#include <core/algorithms/UnionFind.hh>

namespace simulations {
namespace movers {
namespace ising {

/** @brief Mover which randomly flips a cluster of spins.
 * This mover implements Wolff algorithm of a single spin cluster flip.
 */
template<typename S, typename I>
class WolffMove2D : public movers::Mover {
public:

  /// Initialize the mover for a given system
  WolffMove2D(systems::ising::Ising2D <S, I> &system) :
    Mover("WolffMove2D"), system_(system), generate_spins(0, system.count_spins() - 1),
    rnd(core::calc::statistics::Random::get()), in_cluster_flags(system.count_spins()) {
    spins_in_cluster.reserve(system.count_spins());
  }

  /// Empty default destructor
  virtual ~WolffMove2D() = default;

  /** @brief This method actually does not adjust anything.
   *
   * Since this mover places a tail bead in a random position on a sphere, this method is unnecessary; provided only
   * to implement the interface
   * @param new_max_step - unused
   */
  virtual void max_move_range(const double new_max_step) {}

  /// Always return 0.0
  virtual double max_move_range() const { return 0.0; }

  /** @brief Select a random cluster and try to flip it.
   *
   * @param mc_scheme - criterion used to accept the move
   * @return true if th emove was successfull
   */
  virtual bool move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme);

  /// Does nothing, <code>move()</code> method un-does a move
  virtual void undo() {};

  virtual const std::string &name() const { return name_; }

private:
  systems::ising::Ising2D <S, I> &system_;
  std::uniform_int_distribution<I> generate_spins;
  core::calc::statistics::Random &rnd;
  std::vector<bool> in_cluster_flags;
  std::vector<I> spins_in_cluster;

  void find_cluster(std::vector<I> &result, I point, S spin);
};
class WolffMove2DBB : public WolffMove2D<core::index1,core::index2>  {};
}
}
}

#endif //SIMULATIONS_ISING_WolffMove2D_HH
