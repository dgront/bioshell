/** @file Mover.hh
 * @brief Provides Mover base class
 */
#ifndef SIMULATIONS_GENERIC_MOVERS_Mover_HH
#define SIMULATIONS_GENERIC_MOVERS_Mover_HH

#include <memory> // for shared pointers
#include <string>

#include <core/index.hh>
#include <simulations/sampling/AbstractAcceptanceCriterion.hh>

namespace simulations {
namespace movers {

/** Mover represents a Monte Carlo - type update of a system: modifies its coordinates and accepts the change according to a given criterion
 */
class Mover {
public:

  /** @brief Constructor sets a name of this mover
   * @param mover_name - a string that will be used to refer to a mover derived from this class
   */
  Mover(const std::string & mover_name) : name_(mover_name) {}

  /// Virtual destructor (empty)
  virtual ~Mover() { }

  /** @brief Propose a move and accept it according to a given criterion.
   *
   * @param mc_scheme - acceptance criterion object
   * @return true if the move was successful, false otherwise
   */
  virtual bool move(sampling::AbstractAcceptanceCriterion & mc_scheme) = 0;

  /** @brief Attempts a number of MC moves
   *
   * @param n_moves - the number of moves to be attempted
   * @param mc_scheme - acceptance criterion object
   * @return the number of successful moves
   */
  virtual core::index2 n_moves(const core::index2 n_moves, sampling::AbstractAcceptanceCriterion & mc_scheme) {
    core::index2 ret = 0;
    for (core::index2 i = 0; i < n_moves; i++) ret += move(mc_scheme);
    return ret;
  }

  /// Steps back the most recent move
  virtual void undo() = 0;

  /** @brief Advances both counters of attempted and accepted moves by one
   * This method is called by <code>move()</code> to maintain the ratio of successful moves
   */
  inline void inc_move_counter() {
    ++n_attempted;
    ++n_successful;
    ++n_total;
  }

  /** @brief Decrements the counter of successful moves by one
   * This method is called by <code>undo()</code> to maintain the ratio of successful moves
   */
  inline void dec_move_counter() { n_successful--; }

  /// Clears both counters of attempetd and accepted moves by one
  inline void clear_move_counter() {
    n_successful = 0;
    n_attempted = 0;
  }

  /** @brief Returns the success rate for this mover.
   *
   * @return success rate computed since the last reset of the counters
   */
  inline double get_success_rate() { return ((double) n_successful) / ((double) n_attempted); }

  /** @brief Returns the success rate for this mover and resets the counters.
   *
   * @return success rate computed since the last reset of the counters
   */
  inline double get_and_clear_success_rate() {

    double s = ((double) n_successful) / ((double) n_attempted);
    clear_move_counter();

    return s;
  }

  /** @brief Define the maximum change of a coordinate a mover can make
   *
   * @param new_max_step - largest possible change in a coordinate value
   */
  virtual void max_move_range(const double new_max_step) { max_step_ = new_max_step; }

    virtual /// Reads the maximum value of move range
  double max_move_range() const { return max_step_; }

  /** @brief Sets the upper bound for maximum move range.
   *
   * When the move range is increased by AdjustMoversAcceptance to match given acceptance ratio, it cannot exceed
   * that value. This barrier is introduced to avoid divergence during simulations
   * @param new_range_max - the maximum allowed value of a move range
   */
  virtual void max_move_range_allowed(const double new_range_max) { max_range_allowed_ = new_range_max; }

  /// Reads the upper bound for maximum move range.
  virtual double max_move_range_allowed() const { return max_range_allowed_; }

    virtual /// Returns the name of this mover, so the name may appear in the output when required
  const std::string &  name() const { return name_; }

protected:
  double max_step_;
  double max_range_allowed_;
  core::index4 n_attempted = 0;
  core::index4 n_successful = 0;
  core::index4 n_total = 0;
  const std::string name_;
};

/// Type representing a shared pointer to a Mover
typedef std::shared_ptr<Mover> Mover_SP;

}
}

#endif
