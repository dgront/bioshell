#include <algorithm>
#include <stdexcept>

#include <simulations/movers/PhiPsiMover.hh>
#include <simulations/movers/movers_utils.hh>

namespace simulations {
namespace movers {

PhiPsiMover::PhiPsiMover(systems::CartesianChains &system, const core::data::structural::Structure &structure,
    systems::CartesianChains &backup, simulations::forcefields::ByAtomEnergy &energy) :
    Mover("PhiPsiMover"), system(system), backup(backup), energy(energy), logger("PhiPsiMover") {

  std::vector<std::string> bb_index_to_name{" N  ", " CA ", " C  ", " O  ", " H  "};
  std::map<std::string, int> bb_names_to_index{{" N  ", 0},
                                               {" CA ", 1},
                                               {" C  ", 2},
                                               {" O  ", 3},
                                               {" H  ", 4}};
  std::vector<std::string> atom_pdb_names;

  max_move_range(0.1);
  ncacoh.resize(system.count_residues() * 5), std::numeric_limits<core::index4>::max();

  core::index4 ia = 0;
  core::index4 ir = 0;
  for (auto r_it = structure.first_const_residue(); r_it != structure.last_const_residue(); ++r_it) {
    const core::data::structural::Residue &r = (**r_it);
    for (auto a_sp: r) {
      if (bb_names_to_index.find(a_sp->atom_name()) != bb_names_to_index.cend()) {
        int ia_bb = bb_names_to_index[a_sp->atom_name()];
        ncacoh[ir * 5 + ia_bb] = ia;
      }
      ++ia;
    }
    ++ir;
  }

  for (core::index4 i = 0; i < ncacoh.size(); ++i) {
    if (ncacoh[i] == std::numeric_limits<core::index4>::max()) {
      int i_at = i % 5;
      if (i_at == 4)
        logger << utils::LogLevel::FINE << "H atom not found for res " << i / 5 << "\n";
      else
        logger << utils::LogLevel::WARNING << bb_index_to_name[i_at] << " not found for res " << i / 5 << "\n";
    }
  }
  freeze(false);        // --- by default all DOFs are moveable
}

/** @brief Define the maximal dihedral rotation angle for this mover.
 *
 * @param new_max_step - largest possible change of each dihedral angle (in radians, in the range [0,pi])
 */
void PhiPsiMover::max_move_range(const double max_range) {


  Mover::max_move_range(max_range);
  logger << utils::LogLevel::INFO << "Dihedral rotation range set to [-" << Mover::max_move_range() << ","
         << Mover::max_move_range() << "]\n";
  rand_coordinate = std::uniform_real_distribution<double>(-Mover::max_move_range(), Mover::max_move_range());
}

void PhiPsiMover::freeze(bool set_fixed, core::index2 which_residue, DOFs which_dof, std::vector<core::index2> &mmap) {
  auto pos = std::find(mmap.begin(), mmap.end(), which_residue);
  if (set_fixed) {
    if (pos != mmap.end()) {
      core::index2 idx = std::distance(mmap.begin(), pos);  // --- index of the element to be removed
      mmap[idx] = mmap.back();                              // --- override the element by the last elelemn
      mmap.pop_back();                                      // --- shrink the vector by one
    }
  } else {
    if (pos == mmap.end()) mmap.push_back(which_residue);
  }
}

void PhiPsiMover::freeze(bool set_fixed) {

  movemap_phi_.clear();
  movemap_psi_.clear();
  if (!set_fixed) {
    for (core::index2 i = 0; i < system.count_residues(); ++i) {
      movemap_psi_.push_back(i);
      movemap_phi_.push_back(i);
    }
  }
}

void PhiPsiMover::perform_rotation(const core::data::basic::Vec3I & from,const core::data::basic::Vec3I & to) {

  // --- find rotation axis
  Vec3 axis{to}, start{from};
  axis -= start;
  axis.norm();
  core::calc::structural::transformations::Rototranslation::around_axis(axis, angle_, start, rot_);

  for (core::index4 i = first_rotated; i <= last_rotated; i++) {
    backup[i].set(system[i]);
    rotate_atom(from, system[i], rot_);
  }
}

/// A Helper function to randomly select either Phi or Psi move
PhiPsiMover::DOFs random_move_type() {
  if (core::calc::statistics::Random::get()()%2==0) return PhiPsiMover::DOFs::Phi;
  else return PhiPsiMover::DOFs::Psi;
}

bool PhiPsiMover::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  using core::calc::structural::transformations::Rototranslation;
  using core::data::basic::Vec3;
  using core::data::basic::Vec3I;

  inc_move_counter();
  angle_ = rand_coordinate(generator);                  // --- angle of rotation
  double delta_en;
  move_type_ = random_move_type();
  bool rotate_n;
  if (move_type_ == DOFs::Phi) {                           // --- Phi move
    moved_res_ = generator() % movemap_phi_.size();        // --- pivot residue index (on a list of movable positions)
    moved_res_ = movemap_phi_[moved_res_];                 // --- actual pivot residue
    logger << utils::LogLevel::FINER << "Phi-move at residue " << moved_res_ << " by " << angle_ << "\n";

    // --- rotate all atoms from the relevant part of the current chain
    const auto atoms_for_chain = system.atoms_for_chain(atom_N(moved_res_).chain_id);
    rotate_n = (atoms_for_chain.last_atom + atoms_for_chain.first_atom) / 2 < ncacoh[moved_res_ * 5];
    // --- rotation of the N-teminal part involves atoms of the current chain up to N of the moved resid
    if(rotate_n) {
      first_rotated = atoms_for_chain.first_atom;           // --- rotate from the first atom in this chain
      last_rotated = ncacoh[moved_res_ * 5];                // -- to the N atom of the moved residue
      // --- Rotation axis vector starts at CA and ends at N
      perform_rotation(atom_CA(moved_res_), atom_N(moved_res_));
      // --- Rotate also amide H, if the residue has it
      if (index_H(moved_res_) != 0) {
//        rotate_one_atom(system[index_H(moved_res_)], backup[index_H(moved_res_)],rot_);
        backup[index_H(moved_res_)].set(system[index_H(moved_res_)]);
        rotate_atom(atom_N(moved_res_), system[index_H(moved_res_)], rot_);
      }
    } else {
      first_rotated = ncacoh[moved_res_ * 5 + 1];         // --- CA is the first rotated atom, amide-H will be corrected later
      last_rotated = atoms_for_chain.last_atom;           // --- rotate all atoms towards C terminus
      // --- Rotation axis vector starts at N and ends at CA
      perform_rotation(atom_N(moved_res_), atom_CA(moved_res_));
      // --- revert amide-H after rotation, this H doesn't move in this case
      if (index_H(moved_res_) != 0)
        system[index_H(moved_res_)].set(backup[index_H(moved_res_)]);
    }
    delta_en = energy.delta_energy(backup, first_rotated, last_rotated, system);
  } else { // Psi move
    moved_res_ = generator() % movemap_psi_.size();        // --- pivot residue index (on a list of movable positions)
    moved_res_ = movemap_psi_[moved_res_];                 // --- actual pivot residue

    logger << utils::LogLevel::FINER << "Psi-move at residue " << moved_res_ << " by " << angle_ << "\n";
    // --- rotate all atoms from the relevant part of the current chain
    const auto atoms_for_chain = system.atoms_for_chain(atom_N(moved_res_).chain_id);
    rotate_n = (atoms_for_chain.last_atom + atoms_for_chain.first_atom) / 2 < ncacoh[moved_res_ * 5];
    if(rotate_n) {
      first_rotated = atoms_for_chain.first_atom;           // --- rotate from the first atom in this chain
      last_rotated = index_O(moved_res_);                   // --- to the O atom of the moved residue
      // --- Rotation axis vector starts at C and ends at CA
      perform_rotation(atom_C(moved_res_), atom_CA(moved_res_));
      // --- Rotate also amide H, if the residue has it
      if (index_H(moved_res_) != 0) {
//        rotate_one_atom(system[index_H(moved_res_)], backup[index_H(moved_res_)], rot_);
        backup[index_H(moved_res_)].set(system[index_H(moved_res_)]);
        rotate_atom(atom_CA(moved_res_), system[index_H(moved_res_)], rot_);
      }
      // --- energy evaluation must include the whole moved residue
      const auto & atms_for_res = system.atoms_for_residue(moved_res_);
      delta_en = energy.delta_energy(backup, first_rotated, atms_for_res.last_atom, system);

    } else { // --- rotate the C-terminal part
      // --- the first "regular" rotated atom is actually N from the next residue (and its hydrogen, too)
      first_rotated = index_N(moved_res_ + 1);
      last_rotated = atoms_for_chain.last_atom;
      // --- Rotation axis vector starts at CA and ends at C
      perform_rotation(atom_CA(moved_res_), atom_C(moved_res_));

      // --- rotate carbonyl-O as well
      core::index4 idx_o = index_O(moved_res_);
      backup[idx_o].set(system[idx_o]);
      rotate_atom(atom_C(moved_res_), system[index_O(moved_res_)], rot_);
//      rotate_one_atom(system[idx_o],backup[idx_o], rot_);

      // --- energy evaluation must start at carbonyl O; this include the SC of the moved residue which does not move
      // --- but it's technically difficult to exclude these SC atoms from energy evaluation
      delta_en = energy.delta_energy(backup, idx_o, last_rotated, system);
    }
  }

  if (!mc_scheme.test(0, delta_en)) {
    dec_move_counter();
    for (core::index4 i = first_rotated; i < last_rotated; i++) system[i].set(backup[i]);
    if (move_type_ == DOFs::Psi) {
      if (rotate_n)
        system[index_H(moved_res_)].set(backup[index_H(moved_res_)]); // revert moved H, which is outside the moved range
      else
        system[index_O(moved_res_)].set(backup[index_O(moved_res_)]); // revert moved O, which is outside the moved range
    } else {
      if (rotate_n)
        system[index_H(moved_res_)].set(backup[index_H(moved_res_)]); // revert moved H, which is outside the moved range
    }
    logger << utils::LogLevel::FINER << "move cancelled!\n";
    return false;
  } else {
    logger << utils::LogLevel::FINER << "move accepted!\n";
    for (core::index4 i = first_rotated; i <= last_rotated; i++)
      backup[i].set(system[i]);

    if (move_type_ == DOFs::Psi) {
      if (rotate_n)
        backup[index_H(moved_res_)].set(system[index_H(moved_res_)]); // revert moved H, which is outside the moved range
      else
        backup[index_O(moved_res_)].set(system[index_O(moved_res_)]); // revert moved O, which is outside the moved range
    } else {
      if (rotate_n)
        backup[index_H(moved_res_)].set(system[index_H(moved_res_)]); // revert moved H, which is outside the moved range
    }

    return true;
  }
}


}
}

