#ifndef SIMULATIONS_CARTESIAN_MOVERS_TranslateMolecule_HH
#define SIMULATIONS_CARTESIAN_MOVERS_TranslateMolecule_HH

#include <random>
#include <memory>

#include <core/calc/statistics/Random.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/movers/Mover.hh>
#include <simulations/sampling/AbstractAcceptanceCriterion.hh>

#include <simulations/systems/CartesianChains.hh>

namespace simulations {
namespace movers {

/** @brief Translates an atom by a random vector in Cartesian space.
 *
 */
class TranslateMolecule : public simulations::movers::Mover {
public:

  /** @brief Creates a mover for a given set of atoms and a given energy function
   *
   * @param system - the set of atoms that are subjected to moves
   * @param energy - energy function used to accept or to deny a move in a Monte Carlo process
   */
  TranslateMolecule(simulations::systems::CartesianChains & system, simulations::systems::CartesianChains & backup,
                    simulations::forcefields::ByAtomEnergy& energy);

  /** @brief Makes <code>n_moves</code> independent moves.
   *
   * @param n_moves - how many moves should be attempted
   * @param mc_scheme - criterion used to apply or to deny a move
   * @return the number of successful moves
   */
  core::index2 move(const core::index2 n_moves, simulations::sampling::AbstractAcceptanceCriterion & mc_scheme);

  /** @brief Define the maximal displacement for this mover.
   *
   * @param new_max_step - largest possible displacement along each axis
   */
  virtual void max_move_range(const double new_max_step) final;

  /// Reads the maximum range for a move - in Angstroms
  virtual double max_move_range() const final { return max_step_; }

  /** @brief Sets the upper bound for maximum move range.
   *
   * When the move range is increased by AdjustMoversAcceptance to match given acceptance ratio, it cannot exceed
   * that value. This barrier is introduced to avoid divergence during simulations
   * @param new_range_max - the maximum allowed value of a move range
   */
  virtual void max_move_range_allowed(const double new_range_max) final;

  /// Reads the upper bound for maximum move range.
  virtual double max_move_range_allowed() const final { return max_range_allowed_; }

  /** @brief Attempts a single move.
   *
   * The single move event includes:
   *    - random selection of a molecule of the system
   *    - random rotation of that molecule around a randomly selected axis
   *    - acceptance criterion call
   *    - reverting of the move if the criterion test failed
   *
   * @param mc_scheme - a Monte Carlo acceptance criterion (e.g. Metropolis criterion)
   * @return true if the move was accepted, false otherwise
   */
  bool move(simulations::sampling::AbstractAcceptanceCriterion & mc_scheme) final;

  /// Cancels the most recent move
  virtual void undo() final;

private:
  double max_step_;
  double max_range_allowed_;
  core::index4 first_moved_;       // --- index of the first moved atom, backup starts here
  core::index4 last_moved_;        // --- index of the last moved atom, backup ends here (inclusive)
  simulations::systems::CartesianChains & the_system;
  simulations::forcefields::ByAtomEnergy & the_energy;
  std::uniform_int_distribution<core::index4> rand_molecule_index;
  std::uniform_real_distribution<double> rand_coordinate;
  core::calc::statistics::Random & generator = core::calc::statistics::Random::get();
  simulations::systems::CartesianChains & backup;
  utils::Logger logs;
};

}
}

#endif
