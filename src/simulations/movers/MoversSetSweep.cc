#include <vector>
#include <memory>
#include <algorithm>
#include <iomanip>
#include <random>

#include <core/index.hh>
#include <core/calc/statistics/Random.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

#include <simulations/movers/Mover.hh>
#include <simulations/movers/MoversSetSweep.hh>

namespace simulations {
namespace movers {

void MoversSetSweep::add_mover(Mover_SP mover, const size_t moves_each_step) {

  if (moves_each_step==0) {
    logger << utils::LogLevel::WARNING << "Mover " << mover->name() << " not added because 0 steps requested\n";
    return;
  }
  factors.push_back(moves_each_step);
  movers.push_back(mover);
  for (size_t i = 0; i < moves_each_step; ++i) sweep_.push_back(mover);
  sw.push_back(std::max(min_width, core::index2(mover->name().size())));
  logger << utils::LogLevel::INFO << "added " << mover->name() << " which attempts " << moves_each_step
         << " moves at each MC step\n";
}

core::calc::statistics::RandomSequenceIterator<Mover_SP> MoversSetSweep::begin() {

  return core::calc::statistics::RandomSequenceIterator<Mover_SP>::begin(sweep_, sweep_.size());
}

float MoversSetSweep::sweep(sampling::AbstractAcceptanceCriterion & acc, core::index2 n_repeats) {
  float succ = 0.0;
  for (core::index2 i = 0; i < n_repeats; ++i) {
    for (MoversIterator m_it = begin(); m_it != end(); ++m_it)
      succ += (*m_it)->move(acc);
  }
  
  return succ / (sweep_size()*n_repeats);
}

const std::string MoversSetSweep::header_string() const {

  std::stringstream ss;
  ss << std::setw(sw[0]) << movers[0]->name() << '_' << "rate, ";
    ss << std::setw(sw[0]) << movers[0]->name() << '_' << "range ";
  for (size_t i = 1; i < movers.size(); ++i) {
      ss << std::setw(sw[i]) << ","<<movers[i]->name() << '_' << "rate ";
      ss << std::setw(sw[i]) <<","<< movers[i]->name() << '_' << "range ";
  }

  return ss.str();
}

utils::Logger MoversSetSweep::logger("MoversSet");

const core::index1 MoversSetSweep::precision = 4;
const core::index2 MoversSetSweep::min_width = 7;

std::ostream &operator<<(std::ostream &out, const MoversSetSweep &e) {

  out << std::fixed;
  for (size_t i = 0; i < e.movers.size(); ++i) {
    out << ',' << std::setw(e.sw[i]+4) << std::setprecision(e.precision) << e.movers[i]->get_and_clear_success_rate()<<","
        << std::setw(e.sw[i]+6) << std::setprecision(3) << e.movers[i]->max_move_range();
  }
  return out;
}

} // ~ simulations
} // ~ movers
