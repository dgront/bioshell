#ifndef SIMULATIONS_CARTESIAN_MOVERS_LambdaMover_HH
#define SIMULATIONS_CARTESIAN_MOVERS_LambdaMover_HH

#include <random>
#include <memory>

#include <core/calc/statistics/Random.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>

#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/movers/Mover.hh>
#include <simulations/sampling/AbstractAcceptanceCriterion.hh>

#include <simulations/systems/CartesianChains.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace simulations {
namespace movers {

/** \brief Mover that affects Lambda torsion angle
 *
 */
class LambdaMover : public simulations::movers::Mover {
public:

  /** \brief Mover that affects Lambda torsion angle
   *
   * @param system - a system whose atoms will be rotated
   * @param energy - energy function called at every move to accept or cancel it
   */
  LambdaMover(systems::CartesianChains &system, const core::data::structural::Structure & structure,
      systems::CartesianChains &backup, simulations::forcefields::ByAtomEnergy &energy);

  bool move(simulations::sampling::AbstractAcceptanceCriterion & mc_scheme) override ;

  inline void undo() override {}

  void max_move_range(const double max_range) override;

  /// Provides the index of the last atom affected by the most recent move
  core::index4 recently_moved_residue() const { return moved_res_; }

  /// Provides the angle (in radians) of the most recent move
  double recent_torsion_angle() const { return angle_;}

  /// Provides the rototranslation object of the most recent move
  const core::calc::structural::transformations::Rototranslation & recent_transformation() const { return rot_; }

private:
  core::index2 moved_res_;
  core::index4 first_rotated;
  core::index4 last_rotated;
  double angle_;
  systems::CartesianChains & system;
  systems::CartesianChains & backup;
  forcefields::ByAtomEnergy & energy;
  core::calc::structural::transformations::Rototranslation rot_;
  std::uniform_int_distribution<core::index4> rand_res_index;
  std::uniform_real_distribution<double> rand_angle;
  std::vector<core::index4> ncaco;

    core::calc::statistics::Random & generator = core::calc::statistics::Random::get();
  utils::Logger logger;

  void perform_rotation(const core::data::basic::Vec3I & from,
      const core::data::basic::Vec3I & to);  ///< Helper function that actually rotate the relevant atoms

};


}
}

#endif
