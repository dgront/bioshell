#ifndef SIMULATIONS_CARTESIAN_MOVERS_PerturbRigidMolecule_HH
#define SIMULATIONS_CARTESIAN_MOVERS_PerturbRigidMolecule_HH

#include <random>
#include <memory>

#include <core/calc/statistics/Random.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>

#include <simulations/movers/Mover.hh>
#include <simulations/sampling/AbstractAcceptanceCriterion.hh>

#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/systems/CartesianChains.hh>

namespace simulations {
namespace movers {

/** @brief Rotates a molecule randomly around a random axis.
 *
 * @tparam C - type of coordinates
 */
class RotateRigidMolecule : public simulations::movers::Mover {
public:

  /** @brief Creates a mover for a given set of atoms and a given energy function
   *
   * @param system - the set of atoms that are subjected to moves
   * @param energy - energy function used to accept or to deny a move in a Monte Carlo process
   * @param center_atom - relative index of an atom that is used as the center of rotation
   * - to avoid costly calculations of CM. This atom index is relative to the beginning of this molecule, e.g.
   * @code center_atom=1 @endcode means that the very first atom of each molecule will be used as the center of its rotation
   */
  RotateRigidMolecule(systems::CartesianChains & system, systems::CartesianChains & backup,
                      forcefields::ByAtomEnergy & energy, core::index2 center_atom);

  /** @brief Attempts a single move.
   *
   * The single move event includes:
   *    - random selection of a molecule of the system
   *    - random rotation of that molecule around a randomly selected axis
   *    - acceptance criterion call
   *    - reverting of the move if the criterion test failed
   *
   * @param mc_scheme - a Monte Carlo acceptance criterion (e.g. Metropolis criterion)
   * @return true if the move was accepted, false otherwise
   */
  bool move(simulations::sampling::AbstractAcceptanceCriterion & mc_scheme) override;

  /// Cancels the most recent move
  void undo() override;

  /** @brief Define the maximal displacement for this mover.
   *
   * @param new_max_step - largest possible displacement along each axis
   */
  void max_move_range(const double new_max_step) override;

  /// Returns the axis of the most recent move (that might had been unsuccessful)
  const core::data::basic::Vec3 &recent_move_axis() const { return recent_move_axis_; }

  /// Returns the angle of the most recent move (that might had been unsuccessful)
  double recent_move_radians() const { return recent_move_radians_; }

private:
  core::data::basic::Vec3 recent_move_axis_;  // --- axis of the most recent move (that might had been unsuccessful)
  double recent_move_radians_;                // --- angle of the most recent move
  core::index4 first_moved_;       // --- index of the first moved atom, backup starts here
  core::index4 last_moved_;        // --- index of the last moved atom, backup ends here (inclusive)
  core::index2 center_atom_;       // --- atom used as the center of rotation - to avoid costly calculations of CM
  core::calc::structural::transformations::Rototranslation rot;
  simulations::systems::CartesianChains & the_system;
  simulations::systems::CartesianChains & backup;
  simulations::forcefields::ByAtomEnergy& energy;
  std::uniform_int_distribution<core::index4> rand_molecule_index;
  std::uniform_real_distribution<double> rand_coordinate;
  core::calc::statistics::Random & generator = core::calc::statistics::Random::get();
  utils::Logger logs;
};

}
}

#endif
