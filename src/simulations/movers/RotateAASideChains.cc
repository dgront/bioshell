#include <random>
#include <memory>
#include <algorithm>
#include <stdexcept>

#include <core/calc/statistics/Random.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/chemical/Molecule.hh>
#include <core/data/structural/Structure.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>
#include <utils/exit.hh>

#include <simulations/systems/CartesianChains.hh>
#include <simulations/forcefields/mm/MMResidueType.hh>
#include <simulations/forcefields/mm/MMBondedParameters.hh>
#include <simulations/forcefields/ByAtomEnergy.hh>
#include <simulations/movers/RotateAASideChains.hh>

namespace simulations {
namespace movers {

RotateAASideChains::RotateAASideChains(systems::CartesianChains &system,
    const core::data::structural::Structure &structure, systems::CartesianChains &backup,
    const std::map<std::string, simulations::forcefields::mm::MMResidueType> &res_types,
    forcefields::ByAtomEnergy &energy) :
    Mover("RotateAASideChains"), the_system(system), backup(backup), energy(energy), logger("RotateAASideChains") {

  using namespace core::data::structural;

  max_move_range(3.14159);

  // --- test if the system has the same number of residues at the source protein structure
  size_t n_pdb_residues = std::distance(structure.first_const_residue(), structure.last_const_residue());

  // --- this maps ID of each PdbAtom of a given structure to the index of that atom in the modelled system
  // --- it is assumed that system atoms goes in the same order as in the Structure object
  std::map<core::index4,core::index4> from_pdbatoms_to_system;
  core::index4 system_id = 0;
  for(auto a_it=structure.first_const_atom();a_it!=structure.last_const_atom();++a_it) {
    from_pdbatoms_to_system.insert({(**a_it).id(), system_id});
    ++system_id;
  }
  core::chemical::PdbMolecule_SP molecule_sp = core::chemical::structure_to_molecule(structure);

// --- here we detect disulphide bonds and remove them
  for (auto bond_it = molecule_sp->cbegin_bonds(); bond_it != molecule_sp->cend_bonds(); ++bond_it) {
    PdbAtom_SP vi = molecule_sp->get_atom((*bond_it).first.first);
    PdbAtom_SP vj = molecule_sp->get_atom((*bond_it).first.second);
    if (vi->atom_name()== " SG " && vj->atom_name()== " SG ")
      molecule_sp->remove_bond(vi, vj);
  }

  auto resid_it = structure.first_const_residue();
  std::vector<PdbAtom_SP> sc;
  // --- go over all residues
  for (core::index2 ires = 0; ires < the_system.count_residues(); ++ires) {
    const Residue & r = **(resid_it);

// ---------- the range of atoms for the current residue
    const auto my_atoms = the_system.atoms_for_residue(ires);
// ---------- establish the residue type for ires residue
    core::index1 rt = the_system[my_atoms.first_atom].residue_type;
    std::string patched_name = forcefields::mm::patch_residue_name(system.atom_typing->residue_internal_name(rt),
                                                                   systems::AtomTypingVariants::STANDARD);

    if (res_types.find(patched_name)== res_types.end()) {
      std::string msg = "Unknown residue type: " + patched_name + " patched from " + system.atom_typing->residue_internal_name(rt) +"\n";
      logger << utils::LogLevel::SEVERE << msg;
      throw std::invalid_argument(msg);
    }

    const forcefields::mm::MMResidueType &mmrt = res_types.at(patched_name);
    for (auto rotable: mmrt.rotatable_bonds) {               // --- Loop over rotations within a given residue
    // ---------- Fix the name of each rotable atom, so it has 4 characters and can be matched to atom names in Structure object
      std::string rotable_name_1 = core::data::structural::format_pdb_atom_name(rotable.pdb_atom_name1);
      std::string rotable_name_2 = core::data::structural::format_pdb_atom_name(rotable.pdb_atom_name2);
      core::index4 rotable_1 = r.find_atom(rotable_name_1)->id();     // --- index of the rotable atom according to the PDB indexing
      rotable_1 = from_pdbatoms_to_system[rotable_1];
      core::index4 rotable_2 = r.find_atom(rotable_name_2)->id();     // --- index of the rotable atom according to the PDB indexing
      rotable_2 = from_pdbatoms_to_system[rotable_2];
      logger << utils::LogLevel::FINER
             << utils::string_format("Setting up rotation for residue %d %3s around %4d %4s - %4d %4s bond\n",
              ires, patched_name.c_str(), rotable_1, rotable_name_1.c_str(), rotable_2,rotable_name_2.c_str());
      OneRotation rot{ires, rotable_1, rotable_2, {}};

      // ---------- locate the atoms in a molecule
      auto ca = molecule_sp->get_atom(rotable_1); // the atom preceding the rotation
      auto cb = molecule_sp->get_atom(rotable_2); // rotation center
      if ((ca == nullptr) || (cb == nullptr)) continue; // --- if not found then that rotation can't be created
      sc.clear();

      // ---------- find the part that will be rotated around the given bond
      core::chemical::find_side_group<PdbAtom_SP>(ca, cb, *molecule_sp, sc);
      for (auto atom_sp : sc)
        rot.to_be_rotated.push_back(from_pdbatoms_to_system[atom_sp->id()]);
      rotables.push_back(rot);

      if (logger.is_logable(utils::LogLevel::FINER)) {
        logger << "\tit rotates atoms :";
        for (auto atom_sp: sc) logger << " " << atom_sp->atom_name();
        logger << "\n";
      }
    }
    ++resid_it; // --- move to the next residue in a structure
  }
}


void RotateAASideChains::max_move_range(const double new_max_step) {

  Mover::max_move_range(new_max_step);
  logger << utils::LogLevel::INFO << "Rotation angle range set to [-" << new_max_step << "," << new_max_step << "]\n";
  rand_coordinate = std::uniform_real_distribution<double>(-new_max_step, new_max_step);
}


bool RotateAASideChains::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  inc_move_counter();
  last_rotation_index = generator() % rotables.size();
  const OneRotation &rotable = rotables[last_rotation_index];
  double angular_step = rand_coordinate(generator);

  logger << utils::LogLevel::FINER
         << utils::string_format("rotating around axis %d : %d of residue %d\n", rotable.rotable_1, rotable.rotable_2,
                                 rotable.my_residue);

  Vec3 a1(the_system[rotable.rotable_1].x(), the_system[rotable.rotable_1].y(), the_system[rotable.rotable_1].z());
  Vec3 a2(the_system[rotable.rotable_2].x(), the_system[rotable.rotable_2].y(), the_system[rotable.rotable_2].z());
  Vec3 tmp;
  core::calc::structural::transformations::Rototranslation::around_axis(a1, a2, angular_step, a2, rt_operation);
  for (core::index4 i_rot: rotable.to_be_rotated) {
    backup[i_rot].set(the_system[i_rot]);
    tmp.set(the_system[i_rot].x(), the_system[i_rot].y(), the_system[i_rot].z());
    rt_operation.apply(tmp);
    the_system[i_rot].set(tmp);
  }

  double delta_en = energy.delta_energy(backup, rotable.my_residue, the_system);
  inc_move_counter();
  logger << utils::LogLevel::FINEST
         << utils::string_format("moving residue %d; delta(Energy): %f\n", rotable.my_residue, delta_en);

  if (!mc_scheme.test(0, delta_en)) {
    undo();
    logger << utils::LogLevel::FINEST << "move cancelled!\n";
    return false;
  } else {
    logger << utils::LogLevel::FINEST << "move accepted!\n";
    for (core::index4 i_rot: rotable.to_be_rotated) {
      backup[i_rot].set(the_system[i_rot]);
    }
    return true;
  }
}

void RotateAASideChains::undo() {

  dec_move_counter();
  const OneRotation &rotable = rotables[last_rotation_index];
  for (core::index4 i_rot: rotable.to_be_rotated) {
    the_system[i_rot].set(backup[i_rot]);
  }
}

}
}


