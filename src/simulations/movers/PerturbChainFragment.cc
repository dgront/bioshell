#include <random>
#include <memory>
#include <math.h>

#include <simulations/movers/PerturbChainFragment.hh>

namespace simulations {
namespace movers {

PerturbChainFragment::PerturbChainFragment(systems::CartesianChains &system, systems::CartesianChains &backup,core::index2 n_moved,
                                           forcefields::ByAtomEnergy &energy) :
  Mover("PerturbChainFragment"), n_moved_(n_moved), the_system(system), backup(backup), the_energy(energy),
  logger("PerturbChainFragment") {

  n_moved_ = n_moved;
  max_move_range(max_step_);
  max_move_range_allowed(max_step_ * 5);
  rand_bead_index = std::uniform_int_distribution<core::index4>(0,system.n_atoms - n_moved_);
  rand_coordinate = std::uniform_real_distribution<double> (-max_step_, max_step_);
}


core::index2 PerturbChainFragment::move(const core::index2 n_moves,
                                           simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

  int ret = 0;
  for (int i = 0; i < n_moves; i++) if (move(mc_scheme)) ret++;
  return ret;
}


void PerturbChainFragment::max_move_range(const double step) {

  Mover::max_move_range(step);
  rand_coordinate = std::uniform_real_distribution<double>(-max_step_, max_step_);
  logger << utils::LogLevel::INFO << "Maximum move range set to [-" << -max_step_ << "," << max_step_ << "]\n";
}


bool PerturbChainFragment::move(simulations::sampling::AbstractAcceptanceCriterion &mc_scheme) {

    do {
      last_moved_from = rand_bead_index(generator);
      last_moved_to = last_moved_from + n_moved_ - 1;
    }
    while (the_system[last_moved_from].chain_id != the_system[last_moved_to].chain_id); // --- first and last in chain will never move

  double f = 2.0 / (1.0 + n_moved_);
  double dx = rand_coordinate(generator) * f;
  double dy = rand_coordinate(generator) * f;
  double dz = rand_coordinate(generator) * f;
  logger << utils::LogLevel::FINER << "moving the beads : " << (int) last_moved_from << " - " << (int) last_moved_to << "\n";

    for (core::index4 i = 0; i < n_moved_ / 2; ++i) {
        the_system[last_moved_from + i].add(dx * (i + 1),dy * (i + 1),dz * (i + 1));
        the_system[last_moved_to - i].add(dx * (i + 1),dy * (i + 1),dz * (i + 1));
    }
    if (n_moved_ % 2 == 1) {
        const core::index4 ii = (last_moved_from+last_moved_to)/2;
        the_system[ii].add(dx/f,dy/f,dz/f);
    }
  the_system.prepare_for_scoring(last_moved_from,last_moved_to);
  double delta_energy = the_energy.delta_energy(backup, last_moved_from, last_moved_to, the_system);
  inc_move_counter();
  if (!mc_scheme.test(0, delta_energy)) {
    undo();
    if (logger.is_logable(utils::LogLevel::FINEST))
      logger << utils::LogLevel::FINEST <<
          utils::string_format("move cancelled: beads %d - %d; delta(Energy): %f\n",
              last_moved_from, last_moved_to, delta_energy);
    return false;
  } else {
      if (logger.is_logable(utils::LogLevel::FINEST)) {
          logger << utils::LogLevel::FINEST
                 << utils::string_format("move accepted: beads %d - %d; delta(Energy): %f\n",
                                         last_moved_from, last_moved_to, delta_energy);
      }
#ifdef DEBUG
      double en_before = the_energy.energy(backup);

      double en_after = the_energy.energy(the_system);
      if (fabs(en_after - en_before - delta_energy) > 0.001) {
          std::cout << "delta energy error is: " << fabs(en_after - en_before - delta_energy) << "\n";
          std::cout << "delta energy is: " << delta_energy << "\n";
          throw std::runtime_error("PerturbChainFragment: inconsistent energy!");
      }
#endif
      for (size_t i = last_moved_from; i <= last_moved_to; i++) backup[i].set(the_system[i]);
      backup.clear_after_scoring(last_moved_from,last_moved_to);
  }
    return true;

}


void PerturbChainFragment::undo() {
  dec_move_counter();
  for (size_t i = last_moved_from; i <= last_moved_to; i++) the_system[i].set(backup[i]);
  the_system.clear_after_scoring(last_moved_from,last_moved_to);

}

}
}
