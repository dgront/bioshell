#include <core/calc/statistics/Random.hh>

#include <simulations/movers/movers_utils.hh>

namespace simulations {
namespace movers {

void random_vector_on_sphere(core::data::basic::Vec3 & out) {

  double s;
  static core::calc::statistics::Random r = core::calc::statistics::Random::get();
  do {
    out.x = 2.0 * r() / r.max() - 1.0;
    out.y = 2.0 * r() / r.max() - 1.0;
    s = out.x * out.x + out.y * out.y;
  } while (s > 1.0);
  double l = 2 * sqrt(1.0 - s);
  out.x *= l;
  out.y *= l;
  out.z = 2 * s - 1.0;
}

void rotate_atom(const core::data::basic::Vec3I & reference, core::data::basic::Vec3I &rotated,
    const core::calc::structural::transformations::Rototranslation &rot) {

  // --- find the nearest image of the rotated atom in respect to the reference atom
  Vec3 tmp;
  tmp.set(reference);
  tmp.x += rotated.closest_delta_x(reference);
  tmp.y += rotated.closest_delta_y(reference);
  tmp.z += rotated.closest_delta_z(reference);
  // --- rotate it!
  rot.apply(tmp);
  rotated.set(tmp);
}

}
}

