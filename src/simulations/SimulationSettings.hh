#ifndef SIMULATIONS_SimulationSettings_HH
#define SIMULATIONS_SimulationSettings_HH

#include <string>

#include <core/data/basic/ThreadSafeMap.hh>
#include <utils/options/OptionParser.hh>

namespace simulations {

/** @brief Settings to control a simulation run.
 *
 * This object is a map connecting key:value pairs, both key and values are std::string objects. The map is
 * thread-safe i.e. may be modified on the fly.
 */
class SimulationSettings : public core::data::basic::ThreadSafeMap<std::string,std::string> {
public:

  /** @brief Creates a new map to keep simulation setting.
   *
   * Initially the new map is empty
   *
   * @param settings_name - name of this setting map used as ID
   */
  SimulationSettings(const std::string & settings_name="") : settings_name_(settings_name) {}

  /** @brief Returns <code>true</code> if this settings map contains a given key
   *
   * @param o - the key option you are looking for
   * @return true if the key has been found in this map
   */
  bool has_key(const utils::options::Option &o) const {
    return core::data::basic::ThreadSafeMap<std::string,std::string>::contains(o.get_element_name());
  }

  /** @brief Returns <code>true</code> if this settings map contains a given key
   *
   * @param key - the key you are looking for
   * @return true if the key has been found in this map
   */
  bool has_key(const std::string& key) const {return core::data::basic::ThreadSafeMap<std::string,std::string>::contains(key); }

  /** @brief Returns a value associated with the given key
   *
   * @param key - the key you are looking for
   * @return value as a string
   */
  const std::string& get(const std::string& key) const;

  /** @brief Returns the name (ID string) of this settings
   *
   * @return string ID
   */
  const std::string & settings_name() const { return settings_name_; }

  /** @brief Sets a new name for this set of settings
   *
   * @param new_name - string ID
   */
  void settings_name(const std::string & new_name) { settings_name_ = new_name; }

  /** @brief Returns a value associated with the given key
   *
   * @param key - the key you are looking for
   * @return value
   */
  template<typename T>
  T get(const std::string &key) const { return utils::from_string<T>(get(key)); }

  /** @brief Returns a value associated with the given option
   *
   * @param key - command line option whose value is requested
   * @return value given with that option
   */
  template<typename T>
  T get(const utils::options::Option &o) const { return utils::from_string<T>(get(o.get_element_name())); }

  /** @brief Returns a value associated with the given key
   *
   * @param key - the key you are looking for
   * @param default - default value, returned if the given key cannotbe found in this settings map
   * @return value
   */
  template<typename T>
  T get(const std::string &key, T default_val) const { return (has_key(key)) ? utils::from_string<T>(get(key)) : default_val; }

  /** @brief Returns a value associated with the given option
   *
   * @param key - command line option whose value is requested
   * @return value given with that option
   */
  template<typename T>
  T get(const utils::options::Option &o, T default_val) const {
    return (has_key(o.get_element_name()) && get(o.get_element_name()).size() > 0) ? utils::from_string<T>(
      get(o.get_element_name())) : default_val;
  }

  /** @brief Reads option=value pairs from command line and stores them in this map object.
   *
   * A full option name will be used as the key; every double colon (i.e. <code>"::"</code>) will be substituted
   * with single (i.e. <code>":"</code>) as double colon does not comply to XML standard. String value of an option
   * will be used as the key.
   * @param option_parser - command line option parser
   * @param insert_all - if <code>true</code>, all the registered options will be extracted from the given
   *    OptionParser instance (possibly with their defaults or an empty value);
   *    if <code>false</code> (and this is the default behavior) only the options
   *    actually called at command line will be inserted
   */
  void insert_or_assign(const utils::options::OptionParser & option_parser, bool insert_all = false);

private:
  std::string settings_name_;
};

typedef std::shared_ptr<SimulationSettings> SimulationSettings_SP;

} // ~ simulations

#endif
