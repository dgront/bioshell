#ifndef SIMULATIONS_GENERIC_Evaluator_HH
#define SIMULATIONS_GENERIC_Evaluator_HH

#include <memory>
#include <vector>
#include <string>

#include <core/index.hh>

namespace simulations {
namespace evaluators {

/** @brief VectorEvaluator is an object that returns a vector of real values at each <code>evaluate()</code> call.
 *
 * Value of an evaluator may be also printed by a specialized ostream operator, which takes care of proper formatting
 * of the numeric value. The format is established based on <code>width()</code> and <code>precision()</code> methods
 * which define the numeric format.
 *
 * Among provided implementations you may find <code>CallEvaluator</code> and <code>EchoEvaluator</code>
 */
class Evaluator {
public:

  /** @brief Create a new evaluator with a given name.
   * @param name - name of this evaluator
   */
  Evaluator(const std::vector<std::string> & names) {
    names_ = names;
    precision_ = 3;
    width_ = 8;
  }

  /// Virtual destructor
  virtual ~Evaluator() = default;

  /** @brief Return the vector of values calculated at the moment of the call.
   *
   * Each evaluator must return a vector of real values.
   */
  virtual std::vector<double> evaluate() = 0;

  ///  Return a string that is a header of this evaluator's column
  std::string header() const;

  ///  Return the name of a derived evaluator
  const std::vector<std::string> & names() const { return names_; }

  /** @brief Return precision used to write each value.
   * Says how many significant digits will be printed.
   */
  core::index1 precision() const { return precision_; }

  /** @brief Sets precision used to write each value.
   * Defines how many significant digits will be printed.
   */
  void precision(core::index1 p)  { precision_ = p; }

  /** @brief Return the width used to write a single value from a vector of evaluations.
   * @return a field width (same for every evaluated value)
   */
  core::index1 width() const { return width_; }

  /** @brief Defines the width used to write a single value from a vector of evaluations.
   * @return a field width (same for every evaluated value)
   */
  void width(core::index1 w) { width_ = w; }

  /** @brief Returns the actual value of this evaluator converted to string
   * @return evaluated value as a string
   */
  virtual std::string to_string();

protected:
  core::index1 precision_;
  core::index1 width_;
  std::vector<std::string> names_;
};

/// Evaluator_SP is a shared pointer to Evaluator
typedef std::shared_ptr<Evaluator> Evaluator_SP;


} // ~ simulations
} // ~ sweep
#endif
