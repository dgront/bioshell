#ifndef SIMULATIONS_GENERIC_EVALUATORS_CallEvaluator_HH
#define SIMULATIONS_GENERIC_EVALUATORS_CallEvaluator_HH

#include <simulations/evaluators/Evaluator.hh>

namespace simulations {
namespace evaluators {

/** @brief CallEvaluator calls a function and returns the value it returned.
 *
 * At construction time, a call-operation must be provided. Later on, at every <code>evaluate()</code> call
 * the value of this operator is returned.
 *
 * The following example declares a function that calls <code>SimulatedAnnealing::get_temperature()</code>
 * It also declares two <code>EchoEvaluator</code> instances
 * \include ex_SimulatedAnnealing.cc
 */
template<typename Op>
class CallEvaluator : public Evaluator {
public:

  /** @brief Creates an evaluator that just returns a value stored in a given reference.
   * @param call - value returned by this call operator  will be reported at every <code>evaluate()</code> call
   * @param name - a name assigned to the observed variable
   */
  CallEvaluator(Op &call, const std::string &name, core::index1 precision = 2) :
      Evaluator({name}), called_op(call) { Evaluator::precision(precision); }

  /// Calls the call operator and reports the value it returned
  std::vector<double> evaluate() override { return {(double) called_op()}; }

  /// Default virtual destructor
  ~CallEvaluator() override = default;

private:
  Op &called_op;
};

} // ~ evaluators
} // ~ simulations
#endif
