#ifndef SIMULATIONS_GENERIC_EVALUATORS_EchoEvaluator_HH
#define SIMULATIONS_GENERIC_EVALUATORS_EchoEvaluator_HH

#include <simulations/evaluators/Evaluator.hh>

namespace simulations {
namespace evaluators {

/** @brief EchoEvaluator just returns a value stored under a reference.
 *
 * At construction time, a reference to a value of a generic type T must be provided. Later on, at every <code>evaluate()</code> call
 * the value of this reference is returned.
 *
 * The following example declares two  <code>EchoEvaluator</code> instances. It also declares a
 * <code>CallEvaluator</code> instance
 * \include ex_SimulatedAnnealing.cc
 */
template <typename T>
class EchoEvaluator : public Evaluator {
public:

  /** @brief Creates an evaluator that just returns a value stored in a given reference.
   * @param var - value of this variable will be returned at every <code>evaluate()</code> call
   * @param name - a name assigned to the observed variable
   */
  EchoEvaluator(const T & var,const std::string & name) : Evaluator({name}), observed(var) { }

  std::vector<double> evaluate() override { return {(double) observed}; }

  ~EchoEvaluator() override = default;

private:
  const T & observed;
};

} // ~ evaluators
} // ~ simulations
#endif
