
#include <memory>
#include <iostream>
#include <iomanip>

#include <simulations/evaluators/Evaluator.hh>
#include <sstream>

namespace simulations {
namespace evaluators {

std::string Evaluator::header() const {

  int i=0;
  std::string full_header = "";
  for(const std::string & name:names_) {
    core::index2 l = (width_ > names_[i].length()) ? width_ - core::index1(names_[i].length()) : 0;
    std::string headr = (l > 1) ? std::string(size_t(l / 2), ' ') : "";
    full_header += " " + headr + name;
    if (l > headr.size()) full_header += std::string(l - headr.size(), ' ');
  }

  return full_header;
}

std::string Evaluator::to_string() {

  std::stringstream ss;
  std::vector<double> val = evaluate();
  for (double v: val)
    ss << " " << std::setw(width()) << std::setprecision(precision()) << std::fixed << std::showpoint << v;
  return ss.str();
}

} // ~ evaluators
} // ~ simulations
