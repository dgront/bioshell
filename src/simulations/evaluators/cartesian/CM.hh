#ifndef SIMULATIONS_CARTESIAN_EVALUATORS_CM_HH
#define SIMULATIONS_CARTESIAN_EVALUATORS_CM_HH

#include <memory>

#include <core/data/basic/Vec3.hh>
#include <utils/string_utils.hh>
#include <simulations/evaluators/Evaluator.hh>
#include <simulations/systems/CartesianAtoms.hh>

namespace simulations {
namespace evaluators {
namespace cartesian {

/** \brief Evaluator that reports the distance made by the center of mass of a monitored system.
 *
 * This evaluator also reports the vector of CM location
 */
class CMDisplacement : public simulations::evaluators::Evaluator {
public:

  /** \brief Create the evaluator for a given system.
   *
   * @param system - system to be monitored
   */
  CMDisplacement(const systems::CartesianAtoms & system);

  /** \brief Evaluate the monitored property i.e. the distance of the CM from the origin
   *
   * @return the distance of the CM from the origin
   */
  std::vector<double> evaluate() override;

  ~CMDisplacement() override = default;


private:
  mutable core::data::basic::Vec3 cm;
  core::data::basic::Vec3 start_;
  const systems::CartesianAtoms & xyz_;
};


/** \brief Evaluator that reports the position of the center of mass of a monitored system.
 */
class CM : public simulations::evaluators::Evaluator {
public:

  /** \brief Create the evaluator for a given system.
   *
   * @param system - system to be monitored
   */
  CM(const systems::CartesianAtoms & system) : Evaluator({"CM-X", "CM-Y", "CM-Z"}), xyz(system) { width_ = 5; }

  /** \brief Evaluate the monitored property i.e. the CM vector
   *
   * @return the CM vector
   */
  std::vector<double> evaluate() override;

  ~CM() override = default;

private:
  mutable core::data::basic::Vec3 cm;
  const systems::CartesianAtoms & xyz;
};

} // ~ cartesian
} // ~ evaluators
} // ~ simulations

#endif
