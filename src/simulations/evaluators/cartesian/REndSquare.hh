#ifndef SIMULATIONS_CARTESIAN_EVALUATORS_REndSquare_HH
#define SIMULATIONS_CARTESIAN_EVALUATORS_REndSquare_HH

#include <memory>

#include <core/index.hh>
#include <core/data/basic/Vec3.hh>
#include <utils/string_utils.hh>
#include <simulations/evaluators/Evaluator.hh>
#include <simulations/systems/CartesianAtoms.hh>

namespace simulations {
namespace evaluators {
namespace cartesian {

/** \brief Evaluator that reports the end-to-end length.
 */
class REndSquare : public simulations::evaluators::Evaluator {
public:

  /** \brief Create the evaluator that measures the square of end-to-end vector.
   *
   * @param system - system to be monitored
   */
  REndSquare(const systems::CartesianAtoms & system) : Evaluator({"REndSquare"}), xyz(system) { }

  /** \brief Evaluate the length of an end-to-end vector for a single chain
   *
   * @return the end-to-end vector length
   */
  std::vector<double> evaluate() override {

    r_end.set(xyz[xyz.n_atoms-1]);
    r_end.x -= xyz[0].x();
    r_end.y -= xyz[0].y();
    r_end.z -= xyz[0].z();

    return {r_end.length()};
  }

  /// Virtual destructor to satisfy a compiler
  ~REndSquare() override = default;

private:
  mutable Vec3 r_end;
  const systems::CartesianAtoms & xyz;
};


} // ~ cartesian
} // ~ evaluators
} // ~ simulations

#endif
