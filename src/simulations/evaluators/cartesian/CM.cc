#include <utils/string_utils.hh>
#include <simulations/evaluators/cartesian/CM.hh>

namespace simulations {
namespace evaluators {
namespace cartesian {

CMDisplacement::CMDisplacement(const systems::CartesianAtoms & system) : Evaluator({"dCM"}), xyz_(system) {
  width_ = 8;
  calculate_cm(xyz_, start_);
}

std::vector<double> CMDisplacement::evaluate() {

  calculate_cm(xyz_, cm);

  return {cm.distance_to(start_)};
}


std::vector<double> CM::evaluate() {

  calculate_cm(xyz, cm);

  return {cm.x, cm.y, cm.z};
}



} // ~ cartesian
} // ~ evaluators
} // ~ simulations
