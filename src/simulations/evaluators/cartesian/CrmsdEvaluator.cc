#include <memory>

#include <utils/exit.hh>

#include <simulations/evaluators/cartesian/CrmsdEvaluator.hh>

namespace simulations {
namespace evaluators {
namespace cartesian {

CrmsdEvaluator::CrmsdEvaluator(core::data::structural::Structure_SP reference, const systems::CartesianAtoms & system) :
  Evaluator({"crmsd"}), n(reference->count_atoms()), xyz(system) {

  if(reference->count_atoms()!=system.n_atoms)
    utils::exit_OK_with_message("the number of atoms in the reference structure differs from the size of the modelled system!");

  ref = std::make_shared<core::data::basic::Coordinates>(n);
  core::data::structural::structure_to_coordinates(reference, *ref);
  xyz_coords = std::make_shared<core::data::basic::Coordinates>(n);
}

std::vector<double> CrmsdEvaluator::evaluate() {
  for(core::index4 i=0;i<n;++i) {
    (*xyz_coords)[i].x = xyz[i].x();
    (*xyz_coords)[i].y = xyz[i].y();
    (*xyz_coords)[i].z = xyz[i].z();
  }
  return {rms.crmsd(*xyz_coords, *ref, n)};
}


} // ~ cartesian
} // ~ evaluators
} // ~ simulations

