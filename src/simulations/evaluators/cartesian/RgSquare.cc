#include <memory>
#include <math.h>

#include <simulations/evaluators/cartesian/RgSquare.hh>

namespace simulations {
namespace evaluators {
namespace cartesian {

std::vector<double> RgSquare::evaluate() {
  return {simulations::systems::calculate_rg_square(xyz)};
}


} // ~ cartesian
} // ~ evaluators
} // ~ simulations

