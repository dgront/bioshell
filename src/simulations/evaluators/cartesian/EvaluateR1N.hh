#ifndef SIMULATIONS_EVALUATORS_ObserveR1N_HH
#define SIMULATIONS_EVALUATORS_ObserveR1N_HH

#include <utils/string_utils.hh>

#include <core/data/basic/Vec3.hh>
#include <simulations/systems/CartesianChains.hh>
#include <simulations/evaluators/Evaluator.hh>

namespace simulations {
namespace evaluators {
namespace cartesian {

/** @brief Writes a PDB trajectory for an observed system.
 *
 */
class EvaluateR1N : public Evaluator {
public:

  /** @brief Creates an observer that records R1N distances along each chain in a given system
   * @param observed_object - system whose coordinates will be stored in the file
   * @param pdb_format_source - biomolecular structure that corresponds to the system. PDB format data:
   *   - atom names
   *   - residue names
   *   - atom and residue numbering
   *   - chain ID
   * will be extracted from this structure. Coordinates will be taken from <code>observed_object</code>
   * @param out_fname - name of the output file
   */
  EvaluateR1N(const simulations::systems::CartesianChains &observed_chains, core::index2 N) :
      Evaluator({utils::string_format("R1%d distances",N)}), N_(N - 1), observed_object_(observed_chains) { }


  /// Default virtual destructor
  ~EvaluateR1N() override = default;

  /// Append PDB - formatted data to the file
  std::vector<double> evaluate() override;

private:
  const core::index2 N_;
  const simulations::systems::CartesianChains &observed_object_;
};

}
}
}
#endif
