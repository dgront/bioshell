#include <iostream>
#include <fstream>

#include <simulations/evaluators/cartesian/EvaluateR1N.hh>

namespace simulations {
namespace evaluators {
namespace cartesian {


std::vector<double> EvaluateR1N::evaluate() {

  std::vector<double> output;
  for (core::index2 ic = 0; ic < observed_object_.count_chains(); ++ic) {
    const auto &cr = observed_object_.atoms_for_chain(ic);
    for (core::index4 ia = cr.first_atom; ia < cr.last_atom - N_ + 1; ++ia)
      output.push_back(observed_object_[ia].distance_to(observed_object_[ia + N_]));
  }

  return output;
}

}
}
}
