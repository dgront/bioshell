#ifndef SIMULATIONS_CARTESIAN_EVALUATORS_RgSquare_HH
#define SIMULATIONS_CARTESIAN_EVALUATORS_RgSquare_HH

#include <memory>
#include <vector>

#include <utils/string_utils.hh>
#include <simulations/evaluators/Evaluator.hh>
#include <simulations/systems/CartesianAtoms.hh>

namespace simulations {
namespace evaluators {
namespace cartesian {

/** @brief Evaluates square of the radius of gyration of a given system.
 */
class RgSquare : public simulations::evaluators::Evaluator {
public:

  /** @brief set up \f$R_g^2\f$ evaluator for a given system
   * @param system - \f$R_g^2\f$ of this system will be evaluated at every <code>evaluate()</code> call
   */
  RgSquare(const systems::CartesianAtoms & system) : Evaluator({"RgSquare"}), xyz(system) {}

  /// Evaluates \f$R_g^2\f$ of the system of interest
  std::vector<double> evaluate() override;

  /// virtual destructor
  virtual ~RgSquare() {}

private:
  const systems::CartesianAtoms & xyz;
};

} // ~ cartesian
} // ~ evaluators
} // ~ simulations

#endif
