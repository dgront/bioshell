#ifndef SIMULATIONS_GENERIC_EVALUATORS_CrmsdEvaluator_HH
#define SIMULATIONS_GENERIC_EVALUATORS_CrmsdEvaluator_HH

#include <memory>
#include <iostream>
#include <iomanip>
#include <chrono>

#include <core/data/basic/Vec3.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/transformations/Crmsd.hh>

#include <simulations/systems/CartesianAtoms.hh>
#include <simulations/evaluators/Evaluator.hh>

namespace simulations {
namespace evaluators {
namespace cartesian {

/** @brief Evaluates crmsd between a current conformation and the reference
 */
class CrmsdEvaluator : public Evaluator {
public:

  /** @brief Creates an evaluator that observes crmsd between a modelled system and the given reference
   * @param reference - the reference structure - must contain the same number of atoms as the modelled system!
   * @param system - the system being compared to the reference
   */
  CrmsdEvaluator(core::data::structural::Structure_SP reference, const systems::CartesianAtoms & system);

  std::vector<double> evaluate() override;

  /// empty virtual destructor
  ~CrmsdEvaluator() override = default;

private:
  const core::index4 n;
  core::data::basic::Coordinates_SP ref;
  core::data::basic::Coordinates_SP xyz_coords;
  const systems::CartesianAtoms & xyz;
  core::calc::structural::transformations::Crmsd<core::data::basic::Coordinates,core::data::basic::Coordinates> rms;
};

} // ~ cartesian
} // ~ evaluators
} // ~ simulations
#endif
