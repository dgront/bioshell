#ifndef SIMULATIONS_GENERIC_SAMPLING_IsothermalMC_HH
#define SIMULATIONS_GENERIC_SAMPLING_IsothermalMC_HH

#include <simulations/movers/MoversSetSweep.hh>
#include <simulations/sampling/SamplingProtocolBase.hh>
#include <simulations/SimulationSettings.hh>

namespace simulations {
namespace sampling {

/** @brief Simple isothermal MC protocol.
 *
 * This protocol executes a Monte Carlo sweep defined by the given MoversSet instance
 * \f$ N_I \times N_O \f$ times.
 */
class IsothermalMC : public SamplingProtocolBase {
public:

  /** @brief Creates a new isothermal sampler.
   *
   * @param ms - set of movers used for sampling
   * @param temperature - temperature of the isothermal run
   */
  explicit IsothermalMC(movers::MoversSet_SP & ms, const double temperature = 1.0) : movers(ms) { temperature_ = temperature; }

  /// Virtual destructor
  virtual ~IsothermalMC() = default;

  /** Brief Runs the sampling protocol.
   * At every call the method makes  \f$ N_O \f$ = <code>outer_cycles()</code> of
   * \f$ N_I \f$ = <code>inner_cycles()</code> of Monte Carlo steps.
   * The size of each MC sweep is defined within the MoversSet instance given to constructor.
   */
  void run() override { run_(); }

  /// Sets the new temperature value and calls <code>run()</code>
  void run(const double new_temperature) { temperature_ = new_temperature; run_(); }

  /// Returns current simulation temperature
  double temperature() const { return temperature_; }

  /** @brief Reset parameters of this class from a given settings
   *
   * Only the temperature value will be copied from settings.
   * @param settings - simulation settings
   */
  void reset(const simulations::SimulationSettings & settings) override;

protected:
  movers::MoversSet_SP movers; ///< Movers to be called to sample
  double temperature_ = 0; ///< Current temperature

  /// Sets the new value of the simulation temperature
  void temperature(const double new_temperature) { temperature_ = new_temperature; }

private:
  void run_();
};

typedef std::shared_ptr<IsothermalMC> IsothermalMC_SP;

} // ~ sampling
} // ~ simulations

#endif
