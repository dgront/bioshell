#ifndef SIMULATIONS_GENERIC_SAMPLING_AbstractAcceptanceCriterion_HH
#define SIMULATIONS_GENERIC_SAMPLING_AbstractAcceptanceCriterion_HH

namespace simulations {
namespace sampling {

/**
 * @brief Base class to represent an acceptance criterion for a Markov CHain Monte Carlo sampling
 */
class AbstractAcceptanceCriterion {
public:
  /**
   * @brief Virtual method to accept or deny a Monte Carlo move
   * @param old_energy - old energy of a system (before the move)
   * @param new_energy - new energy of a system (after the move)
   * @return true if the move should be accepted, false otherwise
   */
	virtual bool test(const double old_energy,const double new_energy) = 0;

	/// Virtual destructor
	virtual ~AbstractAcceptanceCriterion() {};
};

} // ~ movers
} // ~ simulations

#endif
