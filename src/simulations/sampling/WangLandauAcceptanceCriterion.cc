#include <simulations/sampling/WangLandauAcceptanceCriterion.hh>
#include <utils/Logger.hh>

namespace simulations {
namespace sampling {

utils::Logger logs("WangLandauAcceptanceCriterion");

bool WangLandauAcceptanceCriterion::test(const double old_energy, const double new_energy) {

  double proposed_energy = new_energy - old_energy + total_energy;
  bool ok_to_test = (!is_bound) || (proposed_energy < energy_limit);
  bool accepted = false;
  if(ok_to_test)
  {
      if (histogram_H.find(bin_from_energy(proposed_energy)) == histogram_H.end())
      {
          histogram_H[bin_from_energy(proposed_energy)] = 0;
          histogram_S[bin_from_energy(proposed_energy)] = 1;
          histogram_C[bin_from_energy(proposed_energy)] = 0;
          histogram_E[bin_from_energy(proposed_energy)] = 0;
      }

      if (rd(generator) < std::min(1.0, std::exp(histogram_S[bin_from_energy(total_energy)]
                                                 - histogram_S[bin_from_energy(proposed_energy)])))
      {
          total_energy = proposed_energy;
          accepted = true;
      }
  }
  ++histogram_H[bin_from_energy(total_energy)];
  histogram_S[bin_from_energy(total_energy)] += f;
  ++histogram_C[bin_from_energy(total_energy)];
  histogram_E[bin_from_energy(total_energy)] += total_energy;
  return accepted;
}

void WangLandauAcceptanceCriterion::is_flat(double allowed_fluct) {

  double low = 1.0 - allowed_fluct;
  double high = 1.0 + allowed_fluct;

  double mean = 0.0;
  for (const auto &it : histogram_H) mean += it.second;
  mean /= (double) histogram_H.size();

  int counter = 0;
  for (const auto &it : histogram_H) {
    if ((it.second / mean > low) && (it.second / mean < high)) ++counter;
  }

  if (counter > 0.9 * (double) histogram_H.size()) {
    for (auto &it : histogram_H) it.second = 0;
    logs << utils::LogLevel::INFO << "Histogram S is flat for f = " << f << "\n";
    f /= 2.0;
  }
}

}
}