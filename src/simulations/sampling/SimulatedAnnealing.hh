#ifndef SIMULATIONS_GENERIC_SAMPLING_SimulatedAnnealing_HH
#define SIMULATIONS_GENERIC_SAMPLING_SimulatedAnnealing_HH

#include <utils/Logger.hh>

#include <simulations/movers/MoversSetSweep.hh>
#include <simulations/sampling/IsothermalMC.hh>
#include <simulations/SimulationSettings.hh>

namespace simulations {
namespace sampling {

/** @brief Simulated annealing protocol.
 *
 * This protocol executes a Monte Carlo sweep defined by the given MoversSet instance
 * \f$ N_I \times N_O \f$ times for each of  \f$ N_T \f$ temperature where  \f$ N_I \f$ and  \f$ N_O \f$
 * is the number of inner and outer cycles, respectively. Thus the total number of Monte Carlo sweeps in this protocol is
 * \f$ N_I \times N_O \times N_T \f$.
 *
 * \include ex_SimulatedAnnealing.cc
 */
class SimulatedAnnealing : public IsothermalMC {
public:

  /** @brief Starts a simulated annealing run for a given set of movers
   * @param ms - a set of movers that constitutes to a single MC cycle
   * @param temperatures - temperatures the annealing scheme
   */
  SimulatedAnnealing(movers::MoversSet_SP ms, const std::vector<float> &temperatures)
    : IsothermalMC(ms), logger("SimulatedAnnealing") {

    for(float t:temperatures) temperatures_.push_back(t);
  }

  /** @brief Starts a simulated annealing run for a given set of movers
   * @param ms - a set of movers that constitutes to a single MC cycle
   */
  explicit SimulatedAnnealing(movers::MoversSet_SP ms) : IsothermalMC(ms), logger("SimulatedAnnealing") {}

  /// Virtual destructor
  virtual ~SimulatedAnnealing() = default;

  /** Brief Runs the sampling protocol.
   * For each temperature, the method makes  \f$ N_O \f$ = <code>outer_cycles()</code> of
   * \f$ N_I \f$ = <code>inner_cycles()</code> of Monte Carlo steps.
   * The size of each MC sweep is defined within the MoversSet instance given to constructor.
   */
  void run() override;

  /** @brief Reset parameters of this class from a given settings
   *
   * Only temperature value will be copied from settings.
   * @param settings - simulation settings
   */
  void reset(const simulations::SimulationSettings & settings) override;

private:
  utils::Logger logger;
  std::vector<float> temperatures_;
};

} // ~ sampling
} // ~ simulations

#endif

/**
 * \example ex_SimulatedAnnealing.cc
 */