#include <simulations/movers/Mover.hh>
#include <simulations/sampling/SimulatedAnnealing.hh>
#include <simulations/sampling/MetropolisAcceptanceCriterion.hh>

#include <utils/options/sampling_options.hh>

namespace simulations {
namespace sampling {

void SimulatedAnnealing::run() {

  for (core::index2 itemp = 0; itemp < temperatures_.size(); itemp++) {
    logger << utils::LogLevel::INFO << itemp << " cycle: temperature set to " << temperatures_[itemp] << "\n";
    IsothermalMC::run(temperatures_[itemp]);
  }
}

void SimulatedAnnealing::reset(const simulations::SimulationSettings &settings) {

  IsothermalMC::reset(settings);
  if (settings.has_key(utils::options::begin_temperature) && settings.has_key(utils::options::end_temperature)) {
    core::index2 t_steps = settings.get<core::index2>(utils::options::temp_steps, 2);
    float t_from = settings.get<float>(utils::options::begin_temperature);
    if (t_steps == 1) {
      temperatures_.clear();
      temperatures_.push_back(t_from);
    } else {
      float t_end = settings.get<float>(utils::options::end_temperature);
      core::calc::numeric::evenly_spaced_values(t_from, t_end, t_steps, temperatures_);
    }
  } else {
    float t = 1.0;
    if (settings.has_key(utils::options::begin_temperature))
      t = settings.get<float>(utils::options::begin_temperature);
    else if (settings.has_key(utils::options::end_temperature))
      t = settings.get<float>(utils::options::end_temperature);
    temperatures_.clear();
    temperatures_.push_back(t);
  }
  logger << utils::LogLevel::INFO << "Annealing run will use " << temperatures_.size() << " temperature values\n";
}

}
}
