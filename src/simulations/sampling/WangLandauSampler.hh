#ifndef SIMULATIONS_SAMPLING_WangLandauSampler_H
#define SIMULATIONS_SAMPLING_WangLandauSampler_H

#include <map>
#include <utility>

#include <simulations/movers/MoversSetSweep.hh>
#include <simulations/sampling/WangLandauAcceptanceCriterion.hh>
#include <simulations/sampling/SamplingProtocolBase.hh>
#include <simulations/SimulationSettings.hh>
#include <simulations/systems/ising/Ising2D.hh>

namespace simulations {
namespace sampling {

/** @brief  Wang-Landau MC sampling method.
 *
 * This sampler run a Wang-Landau Monte Carlo simulation for a given system in a multicanonical ensemble
 *
 * @see Berg, B.; Neuhaus, T. (1992). "Multicanonical ensemble: A new approach to simulate first-order phase transitions". Physical Review Letters. 68 (1): 9–12
 * @see Wang, Fugao & Landau, D. P. (Mar 2001). "Efficient, Multiple-Range Random Walk Algorithm to Calculate the Density of States". Phys. Rev. Lett. 86 (10): 2050–2053
 */
class WangLandauSampler : public SamplingProtocolBase {
public:

  /** @brief Create a new sampler
   * @param ms - set of moves that comprise a single MC step
   * @param total_energy - total energy of the starting conformation must be provided to the sampler.
   * Unlike other MC methods (e.g. MC with Metropolis criterion), Wang-Landau method must know the total energy
   * of a sampled system to correctly maintain H and S histograms
   * @param bin_from_energy - a function that converts energy value into an integer bin index
   */
  WangLandauSampler(movers::MoversSet_SP ms, double total_energy, std::function<int(double)> bin_from_energy);

  /** @brief Create a new sampler
   * @param ms - set of moves that comprise a single MC step
   * @param total_energy - total energy of the starting conformation must be provided to the sampler, has to be smaller than @param maximal_energy
   * Unlike other MC methods (e.g. MC with Metropolis criterion), Wang-Landau method must know the total energy
   * of a sampled system to correctly maintain H and S histograms
   * @param bin_from_energy - a function that converts energy value into an integer bin index
   * @param maximal_energy - upper energy limit, moves with higher proposed energy will not be considered
   */
  WangLandauSampler(movers::MoversSet_SP ms, double total_energy, std::function<int(double)> bin_from_energy, double maximal_energy);

  /// Empty destructor
  virtual ~WangLandauSampler() = default;

  /** @brief Returns a reference to the H histogram.
   *
   * After a Wang-Landau sampling process a respective H histogram must be made flat (with some tolerance)
   * @return a reference to the H histogram, e.g. to check if it's flat indeed
   */
  inline const std::map<int, int> &get_histogram_H() const { return wl_acc.get_histogram_H(); }

  /** @brief Returns a reference to the S histogram.
   *
   * After a Wang-Landau sampling process, when the H histogram has become flat, the S histogram
   * approximates the microcanonical entropy of a modelled system
   * @return a reference to the S histogram, i.e. log(Omega(E))
   */
  inline const std::map<int, double> &get_histogram_S() const { return wl_acc.get_histogram_S(); }

  /** @brief Returns a histogram that counts how many times each bin has been visited
   *
   * Required to find average energy in a given bin
   * @return a reference to the C histogram, i.e. number of counts in given bin
   */
  inline const std::map<int, unsigned long int> &get_histogram_C() const { return wl_acc.get_histogram_C(); }

  /** @brief Returns a reference to the E histogram.
   *
   * Each bin of the histogram provides a sum of all energies observed for that bin accumulated over all visits.
   * @return a reference to the SUM histogram, i.e. total energy summed in given bin
   */
  inline const std::map<int, double> &get_histogram_E() const { return wl_acc.get_histogram_E(); }

  void run() override;

private:
    simulations::movers::MoversSet_SP movers;
    const std::function<int(double)> bfe;
    WangLandauAcceptanceCriterion wl_acc;
};

}
}

#endif //SIMULATIONS_SAMPLING_WangLandauSampler_H