#ifndef SIMULATIONS_SAMPLING_WangLandauAcceptanceCriterion_H
#define SIMULATIONS_SAMPLING_WangLandauAcceptanceCriterion_H

#include <map>
#include <utility>

#include <core/calc/statistics/Random.hh>
#include <simulations/sampling/AbstractAcceptanceCriterion.hh>

namespace simulations {
namespace sampling {

/** @brief Acceptance criterion for a Wang-Landau Monte Carlo simulation
 *
 * @see Berg, B.; Neuhaus, T. (1992). "Multicanonical ensemble: A new approach to simulate first-order phase transitions". Physical Review Letters. 68 (1): 9–12
 * @see Wang, Fugao & Landau, D. P. (Mar 2001). "Efficient, Multiple-Range Random Walk Algorithm to Calculate the Density of States". Phys. Rev. Lett. 86 (10): 2050–2053
 */
class WangLandauAcceptanceCriterion : public AbstractAcceptanceCriterion {
public:

  /** @brief Acceptance criterion for a Wang-Landau Monte Carlo simulation
   * @param total_energy - initial total energy of the sampled system
   * @param bfe - function that assigns an energy observation to an integer bin index
   */
  WangLandauAcceptanceCriterion(double total_energy, std::function<int(double)> bfe, bool is_bound, double energy_limit)
      : total_energy(total_energy), bin_from_energy(std::move(bfe)), is_bound(is_bound), energy_limit(energy_limit), rd(0.0, 1.0) {

    histogram_H[bin_from_energy(total_energy)] = 0;
    histogram_S[bin_from_energy(total_energy)] = 1;
    histogram_C[bin_from_energy(total_energy)] = 0;
    histogram_E[bin_from_energy(total_energy)] = 0;
  };

  /** @brief Accepts or rejects a MC move according to the Wang-Landau criterion.
   *
   * @param old_energy - energy before the proposed move
   * @param new_energy - energy after the proposed move
   * @return true if the move should be accepted, false otherwise
   */
  bool test(const double old_energy, const double new_energy) final;

  /// Tests whether the H histogram governed by this WL process is flat
  void is_flat(double allowed_fluct = 0.1);

  /** @brief Returns a reference to the S histogram.
   *
   * After a Wang-Landau sampling process, when the H histogram has become flat, the S histogram
   * approximates the microcanonical entropy of a modelled system
   * @return a reference to the S histogram, i.e. log(Omega(E))
   */
  inline const std::map<int, double> &get_histogram_S() const { return histogram_S; }

  /** @brief Returns a histogram that counts how many times each bin has been visited
   *
   * Required to find average energy in a given bin
   * @return a reference to the C histogram, i.e. number of counts in given bin
   */
  inline const std::map<int, unsigned long int> &get_histogram_C() const { return histogram_C; }

  /** @brief Returns a reference to the histogram of energy.
   *
   * Each bin of the histogram provides a sum of all energies observed for that bin accumulated over all visits.
   * This total energy, divided by a respective count from the C histogram will yield the average energy for a bin
   * @return a reference to the E histogram, i.e. total energy summed in a given bin
   */
  inline const std::map<int, double> &get_histogram_E() const { return histogram_E; }

  /** @brief Returns a reference to the H histogram.
   *
   * After a Wang-Landau sampling process a respective H histogram must be made flat (with some tolerance)
   * @return a reference to the H histogram, e.g. to check if it's flat indeed
   */
  inline const std::map<int, int> &get_histogram_H() const { return histogram_H; }

  /** @brief Returns the currently used value of the <code>f</code> parameter.
   * @return the f value
   */
  inline double get_f() const { return f; }

  /** @brief Returns the currently used value of the <code>epsilon</code> parameter.
   * @return the epsilon value
   */
  inline double get_epsilon() const { return epsilon; }

private:
  double total_energy;
  std::map<int, int> histogram_H;
  std::map<int, double> histogram_S;
  std::map<int, unsigned long int> histogram_C;
  std::map<int, double> histogram_E;
  const double epsilon = pow(2, -20);
  double f = 1.0;
  const std::function<int(double)> bin_from_energy;
  const bool is_bound;
  const double energy_limit;

  std::uniform_real_distribution<double> rd;
  core::calc::statistics::Random &generator = core::calc::statistics::Random::get();
};

}
}

#endif //SIMULATIONS_SAMPLING_WangLandauAcceptanceCriterion_H