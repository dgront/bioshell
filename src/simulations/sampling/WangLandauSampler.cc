#include <simulations/sampling/WangLandauSampler.hh>

namespace simulations {
namespace sampling {

void WangLandauSampler::run() {

  while (wl_acc.get_f() > wl_acc.get_epsilon()) {
    for (core::index4 i = 0; i < n_outer_cycles; i++) {
      for (core::index4 j = 0; j < n_inner_cycles; j++) {
        movers->sweep(wl_acc, n_cycle_size);
        call_inner_cycle_evaluators();
        call_inner_cycle_observers();
        wl_acc.is_flat();
      }
      call_outer_cycle_evaluators();
      call_outer_cycle_observers();
    }
  }
}

WangLandauSampler::WangLandauSampler(movers::MoversSet_SP ms, const double total_energy, std::function<int(double)> bin_from_energy)
    : movers(std::move(ms)), bfe(std::move(bin_from_energy)), wl_acc(total_energy, bfe, false, 0)
{
    n_inner_cycles = n_outer_cycles = 1;
}

WangLandauSampler::WangLandauSampler(movers::MoversSet_SP ms, const double total_energy, std::function<int(double)> bin_from_energy, const double maximal_energy)
    : movers(std::move(ms)), bfe(std::move(bin_from_energy)), wl_acc(total_energy, bfe, true, std::max(maximal_energy, total_energy))
{
    n_inner_cycles = n_outer_cycles = 1;
}

}
}

