#ifndef SIMULATIONS_GENERIC_SAMPLING_AlwaysAccept_HH
#define SIMULATIONS_GENERIC_SAMPLING_AlwaysAccept_HH

#include <simulations/sampling/AbstractAcceptanceCriterion.hh>

namespace simulations {
namespace sampling {

class AlwaysAccept: public AbstractAcceptanceCriterion {
public:
	virtual bool test(const double old_energy,
			const double new_energy) {
		return true;
	}
};

} // ~ movers
} // ~ sampling

#endif
