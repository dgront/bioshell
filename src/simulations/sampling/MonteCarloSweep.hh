#ifndef SIMULATIONS_SAMPLING_MonteCarloSweep_HH
#define SIMULATIONS_SAMPLING_MonteCarloSweep_HH

#include <core/index.hh>
#include <simulations/sampling/AbstractAcceptanceCriterion.hh>

namespace simulations {
namespace sampling {

/** @brief Represents a Monte Carlo sweep.
 *
 * This is a base class that defines a Monte Carlo sweep, i.e. a number of system modifications (moves)
 * that are performed within a single MC time unit
 */
class MonteCarloSweep  {
public:

  /** @brief Perform a single Monte Carlo sweep.
   * This method should give a chance to change each degree of freedom of a sampled system, e.g. to move every atom in a box
   * @param acc - Monte Carlo acceptance criterion do accept or cancel a move
   * @param n_repeats - how many times this sweep should be repeated (default factor = 1)
   * @return success rate of the sweep() call, i.e. a fraction of successful moves
   */
  virtual float sweep(AbstractAcceptanceCriterion & acc, core::index2 n_repeats = 1) = 0;
};

} // ~ sampling
} // ~ simulations

#endif
