#include <utils/options/sampling_options.hh>
#include <simulations/movers/Mover.hh>
#include <simulations/sampling/IsothermalMC.hh>
#include <simulations/sampling/MetropolisAcceptanceCriterion.hh>

namespace simulations {
namespace sampling {


void IsothermalMC::run_() {

  MetropolisAcceptanceCriterion mc(temperature_);

  for (core::index4 i = 0; i < n_outer_cycles; i++) {
    for (core::index2 j = 0; j < n_inner_cycles; j++) {
      movers->sweep(mc, n_cycle_size);
      call_inner_cycle_evaluators();
      call_inner_cycle_observers();
    }
    call_outer_cycle_evaluators();
    call_outer_cycle_observers();
  }
}

void IsothermalMC::reset(const simulations::SimulationSettings & settings) {

  SamplingProtocolBase::reset(settings);
  temperature_ = settings.get(utils::options::temperature.get_element_name(),0);
}

}
}
