#include <utils/options/sampling_options.hh>
#include <simulations/sampling/SamplingProtocolBase.hh>

namespace simulations {
namespace sampling {

void SamplingProtocolBase::reset(const simulations::SimulationSettings & settings) {

  n_outer_cycles = settings.get<core::index4>(utils::options::mc_outer_cycles,1);
  n_inner_cycles = settings.get<core::index4>(utils::options::mc_inner_cycles,1);
  n_cycle_size = settings.get<core::index4>(utils::options::mc_cycle_factor,1);

  logs << utils::LogLevel::INFO << "Running Monte Carlo for " << int(n_outer_cycles) << "outer x " << int(n_inner_cycles)
       << " x inner cycles, each cycle multiplied by " << int(n_cycle_size) << "\n";
}

} // ~ sampling
} // ~ simulations

