#ifdef __SSE__
#include <core/calc/sse_math.hh>
#endif

namespace core {
namespace calc {

#ifdef __SSE__
std::ostream & operator<<(std::ostream & out,__m128 r) {

  out << utils::string_format("%f %f %f %f",r[0],r[1],r[2],r[3]);

  return out;
}
#endif

}
}

