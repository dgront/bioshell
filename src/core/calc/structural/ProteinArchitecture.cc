#include <algorithm>

#include <core/data/structural/Structure.hh>
#include <core/data/structural/SecondaryStructureElement.hh>
#include <core/data/structural/Helix.hh>
#include <core/data/structural/Strand.hh>
#include <core/calc/structural/ProteinArchitecture.hh>
#include <core/algorithms/basic_algorithms.hh>

namespace core {
namespace calc {
namespace structural {

using namespace core::data::structural;

ProteinArchitecture::ProteinArchitecture(const Structure &strctr, bool if_assign_by_dssp, const double max_dssp_energy) :
    logs("ProteinArchitecture"), hb_map(strctr, max_dssp_energy) {

  // ---------- Initialize ss_bits and ss_string data structures
  for (const auto c_it:strctr) {
    const auto end = c_it->terminal_residue() + 1;
    for(auto r_it = c_it->cbegin(); r_it != end; ++r_it) {
      const Residue_SP ri = *r_it;
      SSBits ssb;
      // ---------- Annotate H and E bits where secondary structure says 'H' or 'E'
      // ---------- The actual types of helices are not possible from HEC string of course
      // ---------- Parallel / antiparallel assignment will be figured out later
      if (ri->ss() == 'H') ssb.is_H = true;
      else if (ri->ss() == 'E') ssb.is_E = true;
      ssb.my_residue = ri;
      ss_string += ri->ss();
      ss_bits.push_back(ssb);
    }
  }

  if(if_assign_by_dssp) assign_by_dssp(strctr);
  else assign_by_existing_ss(strctr);

  auto it = strctr.first_const_residue();
  core::index2 it_res_id = 0;
  for (auto ir = (++strctr.first_const_residue()); ir != strctr.last_const_residue(); ++ir) {
   if( (*it)->owner()->id() != (*ir)->owner()->id())
     ss_bits[it_res_id].is_at_chain_break = ss_bits[it_res_id+1].is_at_chain_break = true;
   ++it;            // --- advance iterator of the prev residue, next resid iterator will be advanced by the loop
    ++it_res_id;    // --- advance index of the prev residue
  }
}

void ProteinArchitecture::annotate(Structure &s) const {

  core::index2 i = 0;
  for (auto ir = s.first_residue(); ir != s.last_residue(); ++ir) {
    (**ir).ss(ss_string[i]);
    ++i;
  }
}

void ProteinArchitecture::assign_by_existing_ss(const Structure &strctr) {

  ConsecutiveSS are_consecutive;
  std::vector<std::pair<core::index2, core::index2>> islands;
  core::algorithms::consecutive_find(ss_bits.begin(), ss_bits.end(), are_consecutive, islands);

  create_strands(strctr, islands, sse);
  create_helices(strctr, islands, sse);

  // ---------- and finally we assign names (IDs) to secondary structure elements
  if ((strctr.pdb_header.find("SHEET") == strctr.pdb_header.cend()) &&
      (strctr.pdb_header.find("HELIX") == strctr.pdb_header.cend()))
    rename_sse();
  else
    rename_sse(strctr);
}

void ProteinArchitecture::assign_by_dssp(const Structure &strctr) {

  assign_n_helix();
  assign_bridges();

  ConsecutiveSS are_consecutive;
  std::vector<std::pair<core::index2, core::index2>> islands;
  core::algorithms::consecutive_find(ss_bits.begin(), ss_bits.end(), are_consecutive, islands);

  create_strands(strctr, islands, sse);
  create_helices(strctr, islands, sse);

  // ---------- Unfortunately this step is necessary with our DSSP implementation
  merge_overlapping_strands();

  // ---------- and finally we assign names (IDs) to secondary structure elements
  rename_sse();
}


int island_size(std::pair<core::index2, core::index2> isl) { return isl.second - isl.first + 1; }

void ProteinArchitecture::create_strands(const Structure & strctr,
    const std::vector<std::pair<core::index2, core::index2>> &islands, std::vector<SecondaryStructureElement_SP> &sse_sink) {

  std::vector<Residue_SP> residues;
  for (unsigned int i = 0; i < islands.size(); i++) {
    residues.clear();
    std::pair<core::index2, core::index2> island = islands[i];
    if (!ss_bits[island.first].is_E) continue;
    if (island_size(island) < 2) continue;

    Residue_SP first = ss_bits[island.first].my_residue;
    Residue_SP last = ss_bits[island.second].my_residue;
    std::vector<Residue_SP> residues = strctr.get_residue_range(first, last);

    core::index2 n_parl = 0;
    core::index2 n_anti = 0;
    for (index4 j = island.first; j <= island.second; ++j) {
      if (ss_bits[j].is_A) ++n_anti;
      if (ss_bits[j].is_P) ++n_parl;
    }
    if (n_anti == 0)
      sse_sink.push_back(std::make_shared<Strand>(residues, StrandTypes::Parallel));
    else if (n_parl == 0)
      sse_sink.push_back(std::make_shared<Strand>(residues, StrandTypes::Antiparallel));
    else sse_sink.push_back(std::make_shared<Strand>(residues, StrandTypes::Other));

    for (unsigned int j = island.first; j <= island.second; ++j) ss_string[j] = 'E';
  }
}

void ProteinArchitecture::create_helices(const Structure & strctr,
    const std::vector<std::pair<core::index2, core::index2>> &islands, std::vector<SecondaryStructureElement_SP> &sse_sink) {

  for (unsigned int i = 0; i < islands.size(); i++) {
    std::pair<core::index2, core::index2> island = islands[i];
    if (island_size(island) < 2) continue;

    Residue_SP first = ss_bits[island.first].my_residue;
    Residue_SP last = ss_bits[island.second].my_residue;
    std::vector<Residue_SP> residues = strctr.get_residue_range(first, last);


    if (ss_bits[island.first].is_H) {
      sse_sink.push_back(std::make_shared<Helix>(residues, HelixTypes::RightAlpha));
      continue;
    }

    if (ss_bits[island.first].is_G) {
      sse_sink.push_back(std::make_shared<Helix>(residues, HelixTypes::Right310));
      continue;
    }

    if (ss_bits[island.first].is_I) {
      sse_sink.push_back(std::make_shared<Helix>(residues, HelixTypes::RightPi));
      continue;
    }

    for (unsigned int j = island.first; j <= island.second; ++j) ss_string[j] = 'H';
  }
}

void ProteinArchitecture::merge_overlapping_strands() {

  std::sort(sse.begin(), sse.end()); // --- Sort SSEs as they appear in the protein sequence

  for (auto it1 = sse.begin(); it1 != sse.end(); ++it1) {
    if ((*it1)->hec_code() != 'E') continue;
    auto it2 = it1 + 1;
    while (it2 != sse.end()) {
      if ((*it2)->hec_code() != 'E') {
        ++it2;
        continue;
      }
      Strand_SP s1 = std::dynamic_pointer_cast<Strand>(*it1);
      Strand_SP s2 = std::dynamic_pointer_cast<Strand>(*it2);
      if ((s1->contains(*s2->front())) || (s1->contains(*s2->front()))) {
        std::vector<Residue_SP> residues; // --- Copy residues from both strands into a single vector
        for (auto ri: *s1) residues.push_back(ri);
        for (auto ri: *s2) {
          if (std::find(residues.cbegin(), residues.cend(), ri) == residues.cend())
            residues.push_back(ri);
        }
        Strand_SP merged_strand =
            std::make_shared<Strand>(residues, (s1->type == s2->type) ? s2->type : StrandTypes::Other);
        *it1 = merged_strand;
        it2 = sse.erase(it2);
      } else
        ++it2;
    }
  }
}

void ProteinArchitecture::rename_sse() {

  std::map<std::string, index2> count_by_chain;
  for (auto sse_i : sse) {
    std::string key = sse_i->hec_code() + (*sse_i)[0]->owner()->id();
    if (count_by_chain.find(key) == count_by_chain.end())
      count_by_chain[key] = 1;
    else ++count_by_chain[key];
    key = utils::string_format("%s%d", key.c_str(), count_by_chain[key]);
    sse_i->name(key);
  }
}

bool is_the_residue(const std::string & chain, int id, char icode, const Residue &rb) {
  if ((rb.owner()->id() != chain) || (rb.id() != id) || (rb.icode() != icode)) return false;
  return true;
}

void ProteinArchitecture::rename_sse(const Structure & strctr) {

  using core::data::io::SheetField;
  using core::data::io::HelixField;

  const std::multimap<std::string, std::shared_ptr<core::data::io::PdbField>> & header = strctr.pdb_header;

  std::map<std::string, index2> count_by_chain;
  for (auto sse_i : sse) {
    const Residue & rb = *sse_i->front();
    const Residue & re = *sse_i->back();

    if(sse_i->hec_code()=='H') {
      auto ret = header.equal_range("HELIX");
      for (auto it = ret.first; it != ret.second; ++it) {
        const HelixField & h = *(std::static_pointer_cast<HelixField>(it->second));
        if (is_the_residue(h.chain_from, h.residue_id_from, h.insert_from, rb) &&
            is_the_residue(h.chain_to, h.residue_id_to, h.insert_to, re)) {
          std::string key = utils::string_format("H%s", h.helix_id.c_str());
          std::replace( key.begin(), key.end(), ' ', '_');
          sse_i->name(key);
          break;
        }
      }
      if(sse_i->name().size()==0) {
          logs << utils::LogLevel::INFO << "the helix from " << rb << " to " << re << " not found in the PDB header\n";
      }
    } else {
      auto ret = header.equal_range("SHEET");
      for (auto it = ret.first; it != ret.second; ++it) {
        const SheetField & s = *(std::static_pointer_cast<SheetField>(it->second));
        if (is_the_residue(s.chain_from, s.residue_id_from, s.insert_from, rb) &&
            is_the_residue(s.chain_to, s.residue_id_to, s.insert_to, re)) {
          std::string key = utils::string_format("E%s%2d", s.sheet_id.c_str(), s.strand_id);
          std::replace( key.begin(), key.end(), ' ', '_');
          sse_i->name(key);
          break;
        }
      }
      if(sse_i->name().size()==0) {
        logs << utils::LogLevel::INFO << "the strand from " << rb << " to " << re << " not found in the PDB header\n";
      }
    }
  }
}

void ProteinArchitecture::assign_bridges() {

  for (core::index2 i = 0; i < hb_map.length(); ++i) { // --- find strands
    if (ss_bits[i].is_H || ss_bits[i].is_I || ss_bits[i].is_G) continue;
    for (core::index2 j = 0; j < hb_map.length(); ++j) {
      if (ss_bits[j].is_H || ss_bits[j].is_I || ss_bits[j].is_G) continue;

      if (hb_map.is_antiparallel_bridge(i, j)) {
        ss_bits[i].is_A = true;
        ss_bits[j].is_A = true;
        ss_bits[i].is_E = true;
        ss_bits[j].is_E = true;
      }
      if (hb_map.is_parallel_bridge(i, j)) {
        ss_bits[i].is_P = true;
        ss_bits[j].is_P = true;
        ss_bits[i].is_E = true;
        ss_bits[j].is_E = true;
      }
    }
  }
}

void ProteinArchitecture::assign_n_helix()  {

  bool if_h = hb_map.accepts_N_turn(0, 3);
  for (core::index2 i = 1; i < hb_map.length(); ++i) {
    if (hb_map.accepts_N_turn(i, 3)) {
      if (if_h) { // --- continue a 3-10 helix
        for (int k = 0; k < 3; ++k) {
          ss_bits[i + k].clear_ss();
          ss_bits[i + k].is_G = true;
        }
      }
      if_h = true; // --- start a new helix or just continue the existing one
    } else if_h = false;
  }

  if_h = hb_map.accepts_N_turn(0, 5);
  for (core::index2 i = 1; i < hb_map.length(); ++i) {
    if (hb_map.accepts_N_turn(i, 5)) {
      if (if_h) { // --- continue a PI helix
        for (int k = 0; k < 5; ++k) {
          ss_bits[i + k].clear_ss();
          ss_bits[i + k].is_I = true;
        }
      }
      if_h = true; // --- start a new helix or just continue the existing one
    } else if_h = false;
  }

  if_h = hb_map.accepts_N_turn(0, 4);
  for (core::index2 i = 1; i < hb_map.length(); ++i) {
    if (hb_map.accepts_N_turn(i, 4)) {
      if (if_h) { // --- continue an alpha helix
        for (int k = 0; k < 4; ++k) {
          ss_bits[i + k].clear_ss();
          ss_bits[i + k].is_H = true;
        }
      }
      if_h = true; // --- start a new helix or just continue the existing one
    } else if_h = false;
  }

}

core::data::structural::BetaStructuresGraph_SP  ProteinArchitecture::create_strand_graph() {

  using namespace core::data::structural;

  BetaStructuresGraph_SP output = std::make_shared<BetaStructuresGraph>();
  std::vector<Strand_SP> strands;
  for (auto sse_sp : sse)
    if (sse_sp->hec_code() == 'E') strands.push_back(std::static_pointer_cast<Strand>(sse_sp));
  std::vector<interactions::BackboneHBondInteraction_SP> hbonds_ij;
  for (auto it_i = strands.begin(); it_i != strands.end(); ++it_i) {
    for (auto it_j = strands.begin(); it_j != it_i; ++it_j) {
      hbonds_ij.clear();
      index2 n_A = 0, n_P = 0;
      for (const auto &res_i : (**it_i)) {
        for (const auto &res_j : (**it_j)) {
          if(hb_map.are_H_bonded(*res_i,*res_j)) hbonds_ij.push_back(hb_map.at(*res_i,*res_j));
          if(hb_map.are_H_bonded(*res_j,*res_i)) hbonds_ij.push_back(hb_map.at(*res_j,*res_i));
          if(hb_map.is_antiparallel_bridge(hb_map.residue_index(*res_i),hb_map.residue_index(*res_j))) ++n_A;
          if(hb_map.is_parallel_bridge(hb_map.residue_index(*res_i),hb_map.residue_index(*res_j))) ++n_P;
        }
      }
      if (hbonds_ij.size() > 0) {
        Strand_SP ssp_i = std::dynamic_pointer_cast<Strand>(*it_i);
        Strand_SP ssp_j = std::dynamic_pointer_cast<Strand>(*it_j);
        core::data::structural::StrandPairing_SP sp = std::make_shared<StrandPairing>(ssp_i, ssp_j,
          (n_P < n_A) ? StrandTypes::Antiparallel : StrandTypes::Parallel);
        for(const auto hb:hbonds_ij) sp->add_hbond(hb);
        output->add_strand_pairing(*it_i, *it_j, sp);
      }
    }
  }

  return output;
}

}
}
}
