#include <core/calc/structural/TorsionBinType.hh>

#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>

#include <core/calc/structural/angles.hh>

namespace core {
namespace calc {
namespace structural {

using namespace core::data::structural;

static const double PI = 3.14159;
static const double g_boundary = 100 * PI / 180.0;
static const double a_from = -125 * PI / 180.0;
static const double a_to = 50 * PI / 180.0;
static const double o_from = 90 * PI / 180.0;

TorsionBinType torsion_bin(const Residue_SP prev_residue, const Residue_SP the_residue,
    const Residue_SP next_residue) {

  return torsion_bin(*prev_residue,*the_residue,*next_residue);
}

TorsionBinType torsion_bin(const Residue & prev_residue, const Residue & the_residue,
                           const Residue & next_residue) {

  PdbAtom_SP prev_c = prev_residue.find_atom(" C  ");
  PdbAtom_SP the_n = the_residue.find_atom(" N  ");
  PdbAtom_SP the_ca = the_residue.find_atom(" CA ");
  PdbAtom_SP the_c = the_residue.find_atom(" C  ");
  PdbAtom_SP next_n = next_residue.find_atom(" N  ");
  PdbAtom_SP next_ca = next_residue.find_atom(" CA ");

  double phi = evaluate_dihedral_angle<core::data::basic::Vec3>(*prev_c,*the_n,*the_ca,*the_c);
  double psi = evaluate_dihedral_angle<core::data::basic::Vec3>(*the_n,*the_ca,*the_c,*next_n);
  double omega = evaluate_dihedral_angle<core::data::basic::Vec3>(*the_ca,*the_c,*next_n,*next_ca);

  return torsion_bin(phi,psi,omega);
}

TorsionBinType torsion_bin(const double phi,const double psi,const double omega) {

  if (fabs(omega) < o_from) {
    return O; // cis-omega
  } else
    if (phi >= 0.0) {
      if ((-g_boundary < psi) && (psi <= g_boundary)) {
        return G; // alpha-L
      } else {
        return E; // E
      }
    } else {
      if ((a_from < psi) && (psi <= a_to)) {
        return A; // helical
      } else {
        return B; // extended
      }
    }
}

}
}
}

