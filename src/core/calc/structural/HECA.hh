#ifndef CORE_CALC_STRUCTURAL_HECA_HH
#define CORE_CALC_STRUCTURAL_HECA_HH

#include <core/data/structural/Residue.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/structural/ResidueSegment.hh>
#include <core/data/structural/ResidueSegmentProvider.hh>
#include <core/data/structural/selectors/ResidueSegmentSelector.hh>

#include <core/calc/structural/local_backbone_geometry.hh>

#include <utils/string_utils.hh>
#include <utils/options/Option.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>
#include <utils/options/select_options.hh>

#include <utils/options/structures_from_cmdline.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <simulations/forcefields/cartesian/CAHydrogenBond.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>
#include <core/protocols/selection_protocols.hh>

#define FDEEP_FLOAT_TYPE double
#include <fdeep/fdeep.hpp>

namespace core {
namespace calc {
namespace structural {


class CAHBondEnergy : public core::calc::structural::ResidueSegmentGeometry {
public:

    CAHBondEnergy(core::index2 which_res, double en_cutoff);
    double operator()(const core::data::structural::ResidueSegment &rs) const;
    virtual ~CAHBondEnergy() = default;

    void set_strctr(const core::data::structural::Structure_SP strcr) ;

private:

    core::index2 ires_;
    double en_cutoff_;
    std::map<std::string, core::index4> residue_id_to_index_;
    std::string sequence, secondary;
    std::shared_ptr<simulations::systems::surpass::SurpassAlfaChains> system_;
    std::shared_ptr<simulations::forcefields::cartesian::CAHydrogenBond> energy_;
};

class HECA {

public:

    HECA(const std::string model_file,core::index2 frag_len);

    std::string assign(const data::structural::Structure_SP s);

private:
    utils::Logger logger;
    std::vector<ResidueSegmentGeometry_SP> properties;
    std::vector<std::shared_ptr<CAHBondEnergy>> hbonds;
    core::index2 frag_len_;
    fdeep::model model;
    std::vector<std::vector<double>> Nend;
    std::vector<std::vector<double>> Cend;
    void generate_Nend(std::vector<double> &ref_row);
    void generate_Cend(std::vector<double> &ref_row);
    void prepare_properties();

};
}
}
}

#endif // CORE_CALC_STRUCTURAL_HECA_HH
