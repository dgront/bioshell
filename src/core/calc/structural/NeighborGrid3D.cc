
#include <core/calc/structural/NeighborGrid3D.hh>

namespace core {
namespace calc {
namespace structural {

NeighborGrid3D::NeighborGrid3D(const double cx, const double cy, const double cz, const double grid_mesh) : grid_mesh(
    grid_mesh), logs("NeighborGrid3D") {

  cx_ = cx;
  cy_ = cy;
  cz_ = cz;
}

NeighborGrid3D::NeighborGrid3D(const core::data::structural::Structure &strctr,
  core::data::structural::selectors::AtomSelector & selector, const double grid_mesh) :
  grid_mesh(grid_mesh), logs("NeighborGrid3D") {

  double cnt = 0.0;
  cx_ = cy_ = cz_ =  0.0;
  for(auto it = strctr.first_const_atom(); it != strctr.last_const_atom();++it) {
    if (!selector(**it)) continue;
    cx_ += (**it).x;
    cy_ += (**it).y;
    cz_ += (**it).z;
    ++cnt;
  }
  cx_ /= cnt;
  cy_ /= cnt;
  cz_ /= cnt;

  for (auto it = strctr.first_const_atom(); it != strctr.last_const_atom(); ++it) {
    if (!selector(**it)) continue;
    core::index4 h = hash(**it);
    if (grid_.find(h) == grid_.end()) {
      grid_[h] = std::vector<core::data::structural::PdbAtom_SP>();
      filled_cells_.push_back(h);
    }
    grid_[h].push_back(*it);
  }
  logs << utils::LogLevel::INFO << grid_.size() << " grids are not empty\n";
}

static core::data::structural::selectors::AtomSelector select_all = core::data::structural::selectors::SelectEverything();

NeighborGrid3D::NeighborGrid3D(const core::data::structural::Structure &strctr, const double grid_mesh) :
    NeighborGrid3D(strctr,select_all,grid_mesh) {}

core::index4 NeighborGrid3D::hash(const core::data::structural::PdbAtom &v) const {

  index2 xd = (int(floor((v.x - cx_) / grid_mesh)) + 512) % 1024;
  index2 yd = (int(floor((v.y - cy_) / grid_mesh)) + 512) % 1024;
  index2 zd = (int(floor((v.z - cz_) / grid_mesh)) + 512) % 1024;

  return (xd << 20) + (yd << 10) + zd;
}

void NeighborGrid3D::xyz_from_hash(core::index4 h,  core::index2 &ix, core::index2 &iy, core::index2 &iz) const {

  core::index4 mask = 1023;
  iz = h & mask;
  h = h >> 10;
  iy = h & mask;
  h = h >> 10;
  ix = h & mask;
}

void NeighborGrid3D::insert(core::data::structural::PdbAtom_SP v) {

  core::index4 h = hash(*v);
  if(grid_.find(h)==grid_.end()) {
    grid_[h] = std::vector<core::data::structural::PdbAtom_SP>();
    filled_cells_.push_back(h);
  }
  grid_[h].push_back(v);
}

static const core::index4 z_mask = 1023;
static const core::index4 y_mask = 1023 << 10;
static const core::index4 x_mask = 1023 << 20;

void NeighborGrid3D::get_neighbors(const core::data::structural::PdbAtom &v,
                                   std::vector<core::data::structural::PdbAtom_SP> & sink) {
  core::index4 h = hash(v);
  get_neighbors(h,sink);
}

void NeighborGrid3D::get_neighbor_cells(const core::index4 hash,std::vector<index4> & sink) {

  logs << utils::LogLevel::FINE << "Fetching neighbors of " << hash << " by hash " << hash << "\n";
  int xd = (hash & x_mask) >> 20;
  int yd = (hash & y_mask) >> 10;
  int zd = (hash & z_mask);
  core::index2 by_x[3], by_y[3], by_z[3];
  int n_x = -1, n_y = -1, n_z = -1;
  if (xd > 0) by_x[++n_x] = xd - 1;
  by_x[++n_x] = xd;
  if (xd < 1023) by_x[++n_x] = xd + 1;
  if (yd > 0) by_y[++n_y] = yd - 1;
  by_y[++n_y] = yd;
  if (yd < 1023) by_y[++n_y] = yd + 1;
  if (zd > 0) by_z[++n_z] = zd - 1;
  by_z[++n_z] = zd;
  if (zd < 1023) by_z[++n_z] = zd + 1;
  for (int i = 0; i <= n_x; ++i)
    for (int j = 0; j <= n_y; ++j)
      for (int k = 0; k <= n_z; ++k) {
        core::index4 index = (by_x[i] << 20) + (by_y[j] << 10) + by_z[k];
        sink.push_back(index);
        }

}

void NeighborGrid3D::get_neighbors(const core::index4 hash, std::vector<core::data::structural::PdbAtom_SP> & sink) {

  std::vector<index4> neighbor_hashes;
  get_neighbor_cells(hash,neighbor_hashes);

  for (index4 index: neighbor_hashes) {
    if (grid_.find(index) != grid_.cend()) {
      for (auto atom_sp:grid_.at(index)) sink.push_back(atom_sp);
    }
  }
}

}
}
}
