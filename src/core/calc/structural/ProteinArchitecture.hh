/** @file ProteinArchitecture.hh
 * Provide ProteinArchitecture object
 */
#ifndef CORE_DATA_STRUCTURAL_ProteinArchitecture_HH
#define CORE_DATA_STRUCTURAL_ProteinArchitecture_HH

#include <memory>

#include <core/algorithms/SimpleGraph.hh>

#include <core/data/structural/Residue.fwd.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/Helix.hh>
#include <core/data/structural/Strand.hh>
#include <core/data/structural/StrandPairing.hh>
#include <core/data/structural/SecondaryStructureElement.hh>
#include <core/calc/structural/interactions/BackboneHBondMap.hh>
#include <core/data/structural/BetaStructuresGraph.hh>
#include <core/data/structural/SecondaryStructureElement.fwd.hh>

#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace structural {

/** \brief Creates a topological representation of a protein structure.
 *
 * The class holds secondary structure elements: helices, strands and sheets.
 * It finds also all pairings between strand.
 *
 *  @see secondary structure elements: Helix and Sheet that are derived from SecondaryStructureElement base class
 *  @see StrandPairing an object that holds information about residues that are connected within a strand
 */
class ProteinArchitecture {
public:

/** @brief Describes the architecture of a given protein structure.
 *
 * <strong>Note</strong>, that this method do not alters the original structure. Call <code>annotate()</code>
 * method to annotate a structure with secondary structure data.
 *
 * @param s - the input structure
 * @param if_assign_by_dssp - if true, DSSP method will be used to detect secondary structure elements;
 *  if false, this information will be extracted from PDB file header
 * @param max_dssp_energy -  hydrogen bond whose DSSP energy is higher that the cutoff will be ignored
 */
  ProteinArchitecture(const core::data::structural::Structure &strctr, bool if_assign_by_dssp = true,
      const double max_dssp_energy = -0.4);

  /** @brief Creates a graph of beta strands connected by hydrogen bonds.
   *
   * @return a simple graph with Strands represented by vertices and StrandPairing as edges.
   */
  core::data::structural::BetaStructuresGraph_SP create_strand_graph();

//  core::data::structural::BetaStructuresGraph_SP  create_strand_graph(Structure &strctr);

  /// Provides const access to a map of hydrogen bonds in the structure.
  const interactions::BackboneHBondMap & backbone_HBond_map() const { return hb_map;}

  /// Provides const access to a vector of secondary structure elements defined for the structure.
  const std::vector<core::data::structural::SecondaryStructureElement_SP> & sse_vector() const { return sse;}

  /// Provides secondary structure as a string (in HEC code); all chains combined
  const std::string & hec_string() const { return ss_string; }

  /// Annotates a given structure with the secondary structure data stored in this object
  void annotate(core::data::structural::Structure &s) const;

private:

  utils::Logger logs;

  struct SSBits {

    bool is_A = false, is_P = false, is_H = false, is_G = false, is_I = false, is_E = false;
    bool is_at_chain_break = false;
    core::data::structural::Residue_SP my_residue = nullptr;
    void clear_ss() { is_A = is_P = is_H = is_I = is_G = false; }
  };

  struct ConsecutiveSS {

    bool operator()(const SSBits &lhs, const SSBits &rhs) {

      if (lhs.is_at_chain_break && rhs.is_at_chain_break) return false;
      if (lhs.is_E && rhs.is_E) return true;
      if (lhs.is_G && rhs.is_G) return true;
      if (lhs.is_H && rhs.is_H) return true;
      if (lhs.is_I && rhs.is_I) return true;

      return false;
    }
  };

  interactions::BackboneHBondMap hb_map; ///< Map of main chain hydrogen bonds detected by DSSP algorithm (filled by constructor)

  /// Vector of secondary structure elements created by <code>assign_by_dssp()</code> method
  std::vector<core::data::structural::SecondaryStructureElement_SP> sse;
  /// Secondary structure of a given protein - as a string, all chains concatenated
  std::string ss_string;
  std::vector<SSBits> ss_bits;

  /** @brief Creates secondary structure elements based on sec str annotations from a given Structure object.
   *
   * The method does not run DSSP method, just take the given SS info
   * @param strctr - a source structure
   */
  void assign_by_existing_ss(const core::data::structural::Structure &strctr);

  /** @brief Provides secondary structure annotation for a given Structure object.
   *
   * The method also creates secondary structure element objects (SSEs)
   * @param strctr - a source structure
   */
  void assign_by_dssp(const core::data::structural::Structure &strctr);

  /** @brief Works through the private list of SSEs and merges beta strands adjacent in sequence if they overlap
   */
  void merge_overlapping_strands();

  /** @brief Annotates SSEs with unique string IDs
   */
  void rename_sse();

  /** @brief Annotates SSEs with string IDs created based on PDB file header records
   */
  void rename_sse(const core::data::structural::Structure &strctr);

  void assign_n_helix();
  void assign_bridges();

  void create_strands(const core::data::structural::Structure & strctr, const std::vector<std::pair<core::index2, core::index2>> & islands,
          std::vector<core::data::structural::SecondaryStructureElement_SP> & sse_sink);

  void create_helices(const core::data::structural::Structure & strctr, const std::vector<std::pair<core::index2, core::index2>> &islands,
          std::vector<core::data::structural::SecondaryStructureElement_SP> &sse_sink);

  };

/// Declare shared pointer type for ProteinArchitecture
typedef std::shared_ptr<ProteinArchitecture> ProteinArchitecture_SP;

}
}
}

#endif // --- CORE_DATA_STRUCTURAL_ProteinArchitecture_HH
