#ifndef CORE_CALC_STRUCTURAL_AssignHEC_HH
#define CORE_CALC_STRUCTURAL_AssignHEC_HH

#include <core/data/structural/Residue.hh>
#include <core/calc/statistics/NormalDistribution.hh>
#include <core/data/structural/Structure.hh>

namespace core {
namespace calc {
namespace structural {

class AssignHEC {

public:

  AssignHEC();

  std::string assign(const data::structural::Structure & strctr);

  void describe_helical_ca_geometry(const data::structural::Residue &donor, const data::structural::Residue &acceptor,
                                    std::tuple<double, double, double> &result) const;

  void describe_beta_ca_geometry(const data::structural::Residue &donor, const data::structural::Residue &acceptor,
                                 std::tuple<double, double, double, char> &result) const;
private:
  double P_HELIX;
  double P_STRAND;
  core::calc::statistics::NormalDistribution n_4_1; ///< Distribution to assess alpha-helices
  core::calc::statistics::NormalDistribution n_4_2; ///< Distribution to assess alpha-helices
  core::calc::statistics::NormalDistribution n_4_3; ///< Distribution to assess alpha-helices
  core::calc::statistics::NormalDistribution n_A_1_1; ///< Distribution to assess parallel strands
  core::calc::statistics::NormalDistribution n_A_1_2; ///< Distribution to assess parallel strands
  core::calc::statistics::NormalDistribution n_A_2_1; ///< Distribution to assess parallel strands
  core::calc::statistics::NormalDistribution n_A_2_2; ///< Distribution to assess parallel strands
  core::calc::statistics::NormalDistribution n_P_1; ///< Distribution to assess parallel strands
  core::calc::statistics::NormalDistribution n_P_2; ///< Distribution to assess parallel strands

  double probability_H4(double d13, double d24, double dot_product);

  double probability_EP(double d13, double d24);

  double probability_EA(double d13, double d24);
};

}
}
}


#endif // CORE_CALC_STRUCTURAL_AssignHEC_HH
