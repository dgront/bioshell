
#include <string>
#include <iostream>

#include <core/data/structural/selectors/structure_selectors.hh>

#include <core/calc/structural/angles.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/RotamerGeometry.hh>

namespace core {
namespace calc {
namespace structural {

using namespace core::data::structural;

utils::Logger RotamerGeometry::logs("RotamerGeometry");

RotamerGeometry::RotamerGeometry(const Residue & r) :
    RotamerGeometry(r.owner()->owner()->code(), r.id(), r.icode(), r.owner()->id(), r.residue_type()) {

  logs << utils::LogLevel::FINER << "Taking rotamer observations for residue " << r.id() << r.residue_type().code3 << "\n";

  evaluate_chi(r,chi_);
  rotamer_ = define_rotamer(chi_);

  core::data::structural::selectors::IsBB bb_test;
  core::data::structural::selectors::IsElement if_hydrogen("H");
  bfactor_average_ = 0.0;
  short int n = 0.0;
  for (const PdbAtom_SP & a : r)
    if (!bb_test(*a) && if_hydrogen(*a)) {
      bfactor_average_ += a->b_factor();
      n++;
    }
  bfactor_average_ /= float(n);

  is_complete_ = (type.n_heavy_atoms - 5 <= n);
}


std::ostream & operator<<(std::ostream & out, const RotamerGeometry & o) {

  out << "[" << '"' << o.protein_code << '.' << o.chain_id << o.res_id;
  if (o.i_code != ' ') out << o.i_code;
  out << '"' << ',' << '"' << o.rotamer() << '"';
  for (core::index1 i = 0; i < o.count_chi(); ++i)
    out << ',' << utils::string_format("%.1f",core::calc::structural::start_deg_angle_from_zero(core::calc::structural::to_degrees(o.chi_angles()[i])));
  out << "]";

  return out;
}

}
}
}
