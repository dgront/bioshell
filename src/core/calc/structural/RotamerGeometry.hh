#ifndef CORE_DATA_SEQUENCE_RotamerGeometry_HH
#define CORE_DATA_SEQUENCE_RotamerGeometry_HH

#include <string>
#include <iostream>

#include <core/index.hh>
#include <core/chemical/Monomer.hh>
#include <core/data/structural/Structure.hh>
#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace structural {

/** @brief Represents an observation taken for a single amino acid side chain
 *
 * todo_example Provide web-example for this class
 */
class RotamerGeometry {
public:
  const core::index2 res_id;                 ///< PDB ID of a residue for which the geometry was observed
  const char i_code;                         ///< PDB insertion code of a residue for which the geometry was observed
  const std::string chain_id;                ///< ID of a chain the residue belongs to
  const core::chemical::Monomer & type;      ///< Chemical type of the residue
  const std::string protein_code;            ///< string identifying a source protein

  /** @brief Observe a side chain geometry (a rotameric state) for a given residue
   */
  RotamerGeometry(const core::data::structural::Residue & r);

  /** @brief Counts \f$\chi\f$ angles in this rotamer observation.
   * @returns 1, 2, 3 or 4
   */
  core::index1 count_chi() const { return chi_.size(); }

  /// A string identifying the type of this rotamer, e.g. "MP, "PP", and so on
  const std::string & rotamer() const {return rotamer_; }

  /// \f$\chi\f$ dihedral angles for the rotamer
  const std::vector<double> & chi_angles() const { return chi_; }

  /// b-factor averaged over all atoms of this side chain
  float bfactor_average() const { return bfactor_average_; }

  /// True when this side chain has all its atoms present in the structure
  bool is_complete() const { return is_complete_; }

private:
  std::string rotamer_;          ///< A string identifying the type of this rotamer, e.g. "MP, "PP", and so on
  std::vector<double> chi_;       ///< \f$\chi\f$ dihedral angles for the rotamer_
  float bfactor_average_ = 99.99;    ///< b-factor averaged over all atoms of this side chain
  bool is_complete_ = true;          ///< True when this side chain has all its atoms present in the structure
  static utils::Logger logs;

  RotamerGeometry(const std::string & code, const core::index2 id, const char i_code, const std::string & chain_id,
        const core::chemical::Monomer & m) : res_id(id), i_code(i_code), chain_id(chain_id),
        type(m), protein_code(code) {}
};

/// Write the observation to a stream
std::ostream & operator<<(std::ostream & out, const RotamerGeometry & o);

}
}
}

#endif
