#ifndef CORE_DATA_SEQUENCE_SaturatedRing6Geometry_HH
#define CORE_DATA_SEQUENCE_SaturatedRing6Geometry_HH

#include <string>
#include <iostream>

#include <core/index.hh>
#include <core/chemical/Monomer.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/structural/PdbAtom.fwd.hh>
#include <core/data/structural/Residue.fwd.hh>

#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace structural {

using namespace core::data::structural;

/** @brief Describes internal geometry of a six-member saturated ring, such as in Glucopyranose.
 *
 * The geometry description is based on three angles: twist angle and two wing-angles. The twist angle
 * is evaluated as the angle between the two vectors formed by the four atoms forming the two opposite sides of the flat part of a hexagon.
 * Each of the two other angles (wing angles) is calculated as a dihedral angle between the flat part of a ring and a wing atom.
 *
 * <p>The angles describing geometry of a six-member saturated ring is described according to the following rules:</p>
 * <ul><li>a pair of two opposing (parallel) bonds i.e. four atoms is found such that a deviation from planarity is minimal.
 * The deviation is measured by a dot product between vectors corresponding to the two bonds. The four atoms
 * may be retrieved from SaturatedRing6.planeAtoms array. They are placed in the order as they go around the ring</li>
 * <li>Once the planar part of a ring has been established, the two remaining atoms are considered as two wings that create
 * some angle in respect to the planar quadrilateral. The two angles \f$ \omega_1, \omega_2 \f$
 * (stored in firstWingAngle and in secondWingAngle)
 * define the conformation of a chain, which may be either chair (\f$ \omega_1 \cdot \omega_2 <0\f$)
 * or boat (\f$ \omega_1 \cdot \omega_2 >0\f$)</li>
 * </ul>
 *
 * The following example reads a PDB file and describes geometry of MES and EPE molecules who feature six member saturated rings
 * @deprecated This class should be removed in future releases
 * @todo Remove this class
 */
class SaturatedRing6Geometry {
public:

  const core::index2 res_id;            ///< PDB ID of a residue for which the geometry was observed
  const char i_code;                    ///< PDB insertion code of a residue for which the geometry was observed
  const std::string chain_id;           ///< ID of a chain the residue belongs to
  const core::chemical::Monomer & type; ///< Chemical type of the residue
  const std::string protein_code;       ///< string identifying a source protein

  /** @brief Observe the geometry of a six-member ring formed by the given six atoms.
   * <p>Atoms must be given <strong>exactly</strong> in the order along a ring. It is not important
   * which atom comes first and if they are listed clockwise or counterclockwise.</p>
   *
   * @param a1, a2, a3, a4, a5, a6 - the six vertices of a hexagonal ring
   */
  SaturatedRing6Geometry(PdbAtom_SP a1, PdbAtom_SP a2, PdbAtom_SP a3, PdbAtom_SP a4, PdbAtom_SP a5, PdbAtom_SP a6);

  /** @brief Observe the geometry of a six-member ring formed by the given six atoms.
   * <p>Atoms must be given <strong>exactly</strong> in the order along a ring. It is not important
   * which atom comes first and if they are listed clockwise or counterclockwise.</p>
   *
   * @param r - a residue with a ring to be tested
   * @param atom_names - a list of atoms to be fetched from the given residue
   */
  SaturatedRing6Geometry(const core::data::structural::Residue & r,const std::vector<std::string> & atom_names);

  /** \brief Deviation from planarity for the central 'quadrilateral' of the ring (in radians).
   *  Value must be in the range \f$ [0,\pi0 \f$. The smaller the better - the less distorted is the chain under study
   */
  double twist_angle() { return twist_angle_; }

  /// Atom that creates the first wing
  PdbAtom_SP first_wing() {return first_wing_; }

  /** \brief Angle between the planar part of a ring and one of the two wings (in radians).
   *  Value must be in the range \f$ [-\pi,\pi) \f$. Ideal value for cyclohexane is \f$ \pi/3 \f$
   */
  double first_wing_angle() {return first_wing_angle_; }

  /// Atom that creates the second wing.
  PdbAtom_SP second_wing() {return second_wing_; }

  /** \brief Angle between the planar part of a ring and one of the two wings (in radians).
   *  Value must be in the range \f$ [-\pi,\pi0 \f$. Ideal value for cyclohexane is \f$ \pi/3 \f$
   */
  double second_wing_angle() {return second_wing_angle_; }

private:
  double twist_angle_ = 0.0;
  double first_wing_angle_ = 0.0;
  PdbAtom_SP first_wing_;
  double second_wing_angle_ = 0.0;
  PdbAtom_SP second_wing_;
  std::vector<PdbAtom_SP> planeAtoms;

  SaturatedRing6Geometry(const Residue & r);

  void bestPlaneInCyclohexane(PdbAtom_SP a1, PdbAtom_SP a2, PdbAtom_SP a3, PdbAtom_SP a4, PdbAtom_SP a5, PdbAtom_SP a6);

  friend std::ostream & operator<<(std::ostream & out, SaturatedRing6Geometry & g);
};

std::ostream & operator<<(std::ostream & out, SaturatedRing6Geometry & g);
}
}
}

#endif

