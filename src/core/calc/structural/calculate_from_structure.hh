/** \file calculate_from_structure.hh
 * @brief Simple calculations from Cartesian coordinates: \f$ R_g \f$, center of mass, etc.
 */
#ifndef CORE_CALC_STRUCTURE_calculate_from_structure_H
#define CORE_CALC_STRUCTURE_calculate_from_structure_H

#include <core/index.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/basic/Vec3Cubic.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/PdbAtom.fwd.hh>
#include <simulations/systems/surpass/SurpassAlfaChains.hh>
#include <simulations/forcefields/cartesian/CAHydrogenBond.hh>

namespace core {
namespace calc {
namespace structural {

/** @brief Calculates the center-of-mass (CM) from given atoms
 *
 * @param from - begin iterator pointing to the first atom
 * @param to - end iterator pointing to the last atom (exclusive!)
 * @tparam It - iterator type pointing to Vec3 or Vec3Cubic coordinate types
 * @param s - input structure
 * @return  \f$ V_{CM} \f$ vector
 */
template<typename It>
core::data::basic::Vec3 calculate_cm(const It from, const It to) {

  double cx = 0, cy = 0, cz = 0, n = 0;
  for (auto ia = from; ia != to; ++ia) {
      const auto &c = static_cast<core::data::basic::Vec3>(*ia);
    ++n;
    cx += c.x;
    cy += c.y;
    cz += c.z;
  }

  return core::data::basic::Vec3{float(cx / n), float(cy / n), float(cz / n)};
}

/** @brief Calculates the square of radius of gyration from given atoms
 *
 * @param from - begin iterator pointing to the first atom
 * @param to - end iterator pointing to the last atom (exclusive!)
 * @tparam It - iterator type pointing to Vec3 or Vec3Cubic coordinate types
 * @return  \f$ R_g^2 \f$ value
 */
template<typename It>
double calculate_Rg_square(const It from, const It to) {

  core::data::basic::Vec3 cm = calculate_cm<It>(from, to);

  double s = 0, n = 0, cc = 0;
  for (auto ic = from; ic != to; ++ic) {
    ++n;
    const auto &c = static_cast<core::data::basic::Vec3>(*ic);

    cc = c.x - cm.x;
    s += cc * cc;
    cc = c.y - cm.y;
    s += cc * cc;
    cc = c.z - cm.z;
    s += cc * cc;
  }

  return s / n;
}

/** @brief Calculates the square of radius of gyration from given set of coordinates
 *
 * This function is provided because the templated version is not visible in PyBioShell
 * @param from - pointer holding address of the very first atom included in \f$ R_g^2 \f$ calculation
 * @param to - pointer holding address behind the very last atom included in \f$ R_g^2 \f$ calculation
 * @return  \f$ R_g^2 \f$ value
 */
double calculate_Rg_square(const core::data::basic::Vec3 *from,const core::data::basic::Vec3 *to);

/** @brief Calculates the square of radius of gyration from given set of coordinates
 *
 * This function is provided because the templated version is not visible in PyBioShell
 * @param from - pointer holding address of the very first atom included in \f$ R_g^2 \f$ calculation
 * @param to - pointer holding address behind the very last atom included in \f$ R_g^2 \f$ calculation
 * @return  \f$ R_g^2 \f$ value
 */
double calculate_Rg_square(const core::data::basic::Vec3Cubic *from, const core::data::basic::Vec3Cubic *to);

/** @brief Calculates the square of radius of gyration for a given structure
 *
 * @param s - input structure
 * @return  \f$ R_g^2 \f$ value
 */
double calculate_Rg_square(const core::data::structural::Structure & s);

/** @brief Calculates the center-of-mass (CM) of a given structure.
 *
 * @param s - input structure
 * @return  \f$ V_{CM} \f$ vector
 */
core::data::basic::Vec3 calculate_cm(const core::data::structural::Structure & s);


/** @brief Translates a given structure
 *
 * @param s - input structure
 * @param new_cx - x coordinate of the new center
 * @param new_cy - y coordinate of the new center
 * @param new_cz - z coordinate of the new center
 */
void center_at(core::data::structural::Structure & s, double new_cx, double new_cy, double new_cz);

/** @brief Translates a given structure
 *
 * @param s - input structure
 * @param new_cm - the new location of the CM
 */
void center_at(core::data::structural::Structure & s, core::data::basic::Vec3 cm);

void copy_ca(const data::structural::Structure &source, std::vector<data::structural::PdbAtom_SP> & dest);

int count_hbonds(core::index2 which_pos, simulations::systems::surpass::SurpassAlfaChains &system,
                     simulations::forcefields::cartesian::CAHydrogenBond & hb_energy);

void count_neighbors(std::vector<data::structural::PdbAtom_SP> ca, int which_pos, int & n4, int & n45, int & n5, int & n6);

bool compute_bbq_features(const data::structural::Structure &source, std::ostream & out,bool if_count_lambda=true);

bool compute_lambda(const data::structural::Structure &source, std::vector<double> &lambdas);

}
}
}

#endif
