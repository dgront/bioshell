#include <string>
#include <iostream>

#include <core/index.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/calc/structural/SaturatedRing6Geometry.hh>
#include <core/calc/structural/angles.hh>

#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace structural {

using namespace core::data::structural;

SaturatedRing6Geometry::SaturatedRing6Geometry(const PdbAtom_SP a1, const PdbAtom_SP a2, const PdbAtom_SP a3,
    const PdbAtom_SP a4, const PdbAtom_SP a5, const PdbAtom_SP a6) :
    SaturatedRing6Geometry::SaturatedRing6Geometry(*(a1->owner())) {

  bestPlaneInCyclohexane(a1, a2, a3, a4, a5, a6);
}

SaturatedRing6Geometry::SaturatedRing6Geometry(const Residue & r, const std::vector<std::string> & atom_names) :
    SaturatedRing6Geometry::SaturatedRing6Geometry(r) {

  PdbAtom_SP a1 = r.find_atom_safe(atom_names[0]);
  PdbAtom_SP a2 = r.find_atom_safe(atom_names[1]);
  PdbAtom_SP a3 = r.find_atom_safe(atom_names[2]);
  PdbAtom_SP a4 = r.find_atom_safe(atom_names[3]);
  PdbAtom_SP a5 = r.find_atom_safe(atom_names[4]);
  PdbAtom_SP a6 = r.find_atom_safe(atom_names[5]);
  bestPlaneInCyclohexane(a1, a2, a3, a4, a5, a6);
}

SaturatedRing6Geometry::SaturatedRing6Geometry(const Residue & r) :
  res_id(r.id()), i_code(r.icode()), chain_id(r.owner()->id()), type(r.residue_type()), protein_code(r.owner()->owner()->code()),planeAtoms(6) {
}

void SaturatedRing6Geometry::bestPlaneInCyclohexane(PdbAtom_SP a1, PdbAtom_SP a2, PdbAtom_SP a3, PdbAtom_SP a4,
    PdbAtom_SP a5, PdbAtom_SP a6) {

  using namespace core::data::structural;

  PdbAtom_SP l = a6;
  PdbAtom_SP r = a3;
  Vec3 v1(*a2);
  v1 -= *a1;
  v1.norm();

  planeAtoms[0] = a1;
  planeAtoms[1] = a2;

  Vec3 v2(*a5);
  v2 -= *a4;
  v2.norm();
  planeAtoms[2] = a4;
  planeAtoms[3] = a5;
  double bestVal = fabs(v1.dot_product(v2));
  v1 = Vec3(*a3);
  v1 -= *a2;
  v1.norm();
  v2 = Vec3(*a6);
  v2 -= *a5;
  v2.norm();
  if (fabs(v1.dot_product(v2)) > bestVal) {
    bestVal = fabs(v1.dot_product(v2));
    planeAtoms[0] = a2;
    planeAtoms[1] = a3;
    planeAtoms[2] = a5;
    planeAtoms[3] = a6;
    l = a1;
    r = a4;
  }

  v1 = Vec3(*a4);
  v1 -= *a3;
  v1.norm();
  v2 = Vec3(*a1);
  v2 -= *a6;
  v2.norm();
  if (fabs(v1.dot_product(v2)) > bestVal) {
    bestVal = fabs(v1.dot_product(v2));
    planeAtoms[0] = a3;
    planeAtoms[1] = a4;
    planeAtoms[2] = a6;
    planeAtoms[3] = a1;
    l = a2;
    r = a5;
  }
  first_wing_ = l;
  second_wing_ = r;

  double s = acos(bestVal);
  twist_angle_ = (s > M_PI / 2.0) ? M_PI - s : s;

  second_wing_angle_ = (evaluate_dihedral_angle(*planeAtoms[0], *planeAtoms[3], *planeAtoms[2], *second_wing_)
      - evaluate_dihedral_angle(*planeAtoms[3], *planeAtoms[0], *planeAtoms[1], *second_wing_)) / 2.0;

  first_wing_angle_ = (evaluate_dihedral_angle(*planeAtoms[2], *planeAtoms[1], *planeAtoms[0], *first_wing_)
      - evaluate_dihedral_angle(*planeAtoms[1], *planeAtoms[2], *planeAtoms[3], *first_wing_)) / 2.0;
}

std::ostream & operator<<(std::ostream & out, SaturatedRing6Geometry & g) {

  out
      << utils::string_format("%6.2f %7.2f %7.2f | %4s %4s %4s %4s | %4s %4s", to_degrees(g.twist_angle()), to_degrees(g.first_wing_angle()),
          to_degrees(g.second_wing_angle()), g.planeAtoms[0]->atom_name().c_str(), g.planeAtoms[1]->atom_name().c_str(),
          g.planeAtoms[2]->atom_name().c_str(), g.planeAtoms[3]->atom_name().c_str(), g.first_wing()->atom_name().c_str(),
          g.second_wing()->atom_name().c_str());

  return out;
}

}
}
}

