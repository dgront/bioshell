/** @file PairwiseResidueMap.hh
 * @brief Provides a generic class to hold pairwise residue features such as contacts, hydrogen bonds, etc.
 */
#ifndef CORE_CALC_STRUCTURAL_PairwiseResidueMap_HH
#define CORE_CALC_STRUCTURAL_PairwiseResidueMap_HH

#include <memory>

#include <core/index.hh>
#include <core/data/basic/SparseMap2D.hh>
#include <core/data/structural/ResidueIndex.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Structure.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

using namespace core::data::basic;
using namespace core::data::structural;

/** @brief A 2D container indexed by Residue instances.
 *
 * The purpose for this container is to hold structural features defined for a pair of residues, such as
 * interatomic contacts, hydrogen bonds, etc. The container is derived from \f$SparseMatrix2D<core::index2, T>\f$ type
 * which provides the storage capabilities and from ResidueIndexer, used for residue hashing.
 *
 * @see BackboneHBondMap
 */
template<class T>
class PairwiseResidueMap : public SparseMap2D<core::index2, T>, public core::data::structural::ResidueIndexer {
public:

  /// Create a new container for a given structure.
  PairwiseResidueMap(const Structure &strctr) : ResidueIndexer(strctr) {}

  /// Create a new container for selected residues of a given structure.
  PairwiseResidueMap(const Structure &strctr, const selectors::ResidueSelector &only_selected_residues)
      : ResidueIndexer(strctr, only_selected_residues) {}

  /// Create a new container from a range of residues.
  template<typename It>
  PairwiseResidueMap(It &residue_begin, It &residue_end) : ResidueIndexer(residue_begin, residue_end) {}

  /** @brief Insert a new element into this container.
   * That element will be assigned to \f$(row,column)\f$ ResidueIndex pair
   * @param row - identifies the first residue of the indexing pair
   * @param column - identifies the second residue of the indexing pair
   * @param t - the object stored in the map
   */
  void add(const ResidueIndex &row, const ResidueIndex &column, T t) {
    core::data::basic::SparseMap2D<core::index2, T>::insert(core::data::structural::ResidueIndexer::at(row),
                                                            core::data::structural::ResidueIndexer::at(column), t);
  }

  /** @brief Insert a new element into this container.
   * That element will be assigned to \f$(row,column)\f$ Residue pair
   * @param row -  the first residue of the indexing pair
   * @param column -  the second residue of the indexing pair
   * @param t - the object stored in the map
   */
  void add(const Residue &row, const Residue &column, T t) {
    core::index2 r, c;
    try {
      r = core::data::structural::ResidueIndexer::at(row);
    } catch (...) {
      utils::Logger logger("PairwiseResidueMap");
      logger << utils::LogLevel::WARNING << "Can't find the index for residue" << row
             << ", object not placed in a residue map\n";
      return;
    }
    try {
      c = core::data::structural::ResidueIndexer::at(column);
    } catch (...) {
      utils::Logger logger("PairwiseResidueMap");
      logger << utils::LogLevel::WARNING << "Can't find the index for residue" << column
             << ", object not placed in a residue map\n";
      return;
    }
    core::data::basic::SparseMap2D<core::index2, T>::insert(r, c, t);
  }

  /** @brief Insert a new element into this container.
   * That element will be assigned to \f$(row,column)\f$ Residue pair
   * @param row -  the index of the first residue of the indexing pair
   * @param column -  the index of the second residue of the indexing pair
   * @param t - the object stored in the map
   */
  void add(const index2 row, const index2 column, T t) {
    core::data::basic::SparseMap2D<core::index2, T>::insert(row, column, t);
  }

  /** @brief Access an element indexed by two residues.
   *
   * Returns a requested element if found, nullptr otherwise
   * @param res_i - index of the first residue
   * @param res_j - index of the second residue
   */
  const T &at(const core::index2 res_i, const core::index2 res_j, const T &empty_element) const {

    return SparseMap2D<core::index2, T>::at(res_i, res_j, empty_element);
  }

  /** @brief Access an element based on the definition of two residues.
   *
   * Returns a requested element if found, nullptr otherwise
   * @param chain_i - chain ID for the first residue
   * @param res_i - ID of the first residue
   * @param i_code_i - insertion code of the first residue
   * @param chain_j - chain ID for the second residue
   * @param res_j - ID of the second residue
   * @param i_code_j - insertion code of the second residue
   */
  const T &at(const char chain_i, const int res_i, const char i_code_i,
              const char chain_j, const int res_j, const char i_code_j, const T &empty_element) const {

    return SparseMap2D<core::index2, T>::at(at(chain_i, res_i, i_code_i), at(chain_j, res_j, i_code_j), empty_element);
  }

  /** @brief Access an element based on the definition of two residues.
   *
   * Returns a requested element if found, nullptr otherwise
   * @param ri - defines the first residue
   * @param rj - defines the second residue
   */
  const T &at(const ResidueIndex &ri, const ResidueIndex &rj, const T &empty_element) const {

    return SparseMap2D<core::index2, T>::at(core::data::structural::ResidueIndexer::at(ri),
                                            core::data::structural::ResidueIndexer::at(rj), empty_element);
  }

  /** @brief Access an element based on the definition of two residues.
 *
 * Returns a requested element if found, nullptr otherwise
 * @param ri - defines the first residue
 * @param rj - defines the second residue
 */
  const T &at(const Residue &row, const Residue &column, const T &empty_element) const {

    return SparseMap2D<core::index2, T>::at(core::data::structural::ResidueIndexer::at(row),
                                            core::data::structural::ResidueIndexer::at(column), empty_element);
  }

};

}
}
}
}
#endif
