#include <core/calc/structural/interactions/VdWInteraction.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>


namespace core {
namespace calc {
namespace structural {
namespace interactions {

VdWInteraction::VdWInteraction(const PdbAtom_SP & first_atom, const PdbAtom_SP & second_atom)
	:ResiduePair(first_atom->owner(), second_atom->owner()),atom_a(first_atom),atom_b(second_atom) {

	compute_geometry();
}

void VdWInteraction::compute_geometry() {

	d_ = atom_a->distance_to(*atom_b);

}


std::string VdWInteraction::output_header() {
  return "# first residue    | second residue:       d";
}

std::ostream &operator<<(std::ostream &o, const VdWInteraction &si){

   const core::data::structural::Residue & d = *si.first_residue();
   const core::data::structural::Residue & a = *si.second_residue();

  o << utils::string_format("%4s %4d%c %3s %4s    %4s %4d%c %3s %4s  %6.2f",
                            d.owner()->id().c_str(), d.id(), d.icode(), d.residue_type().code3.c_str(),
                            si.atom_a->atom_name().c_str(), a.owner()->id().c_str(), a.id(), a.icode(),
                            a.residue_type().code3.c_str(),
                            si.atom_b->atom_name().c_str(), si.d());
  return o;
 }

}
}
}
}

