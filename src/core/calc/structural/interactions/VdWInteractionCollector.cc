#include <core/calc/structural/interactions/VdWInteractionCollector.hh>
#include <core/calc/structural/interactions/VdWInteraction.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

VdWInteractionCollector::VdWInteractionCollector(bool include_protein_backbone) : logs("VdWInteractionCollector") {
  cutoff2_ = 2.0 + 2.0 + MAX_DISTANCE;
  cutoff2_ *= cutoff2_;
}

core::index4 VdWInteractionCollector::collect(const core::data::structural::Residue &i_residue,
                                              const core::data::structural::Residue &j_residue,
                                              std::vector<ResiduePair_SP> &sink) {
  index4 cnt = 0;
  for (auto atom_a_it = i_residue.cbegin(); atom_a_it != i_residue.cend(); ++atom_a_it)
    for (auto atom_it_b = j_residue.cbegin(); atom_it_b != j_residue.cend(); ++atom_it_b) {
      auto atom_a = *atom_a_it;
      auto atom_b = *atom_it_b;
      if (atom_a->owner() == atom_b->owner()) break;    // VdW interaction is symmetric, it's enough to test it one-way
      if (calculate_vdw_distance(atom_a, atom_b, sink))
        cnt += 1;
    }
  return cnt;
}

core::index4 VdWInteractionCollector::collect(const core::data::structural::Structure &strctr,
                                              std::vector<ResiduePair_SP> &sink) {
  index4 cnt = 0;
  auto start = std::chrono::high_resolution_clock::now();

  for (auto r1_it = strctr.first_const_residue(); r1_it != strctr.last_const_residue(); ++r1_it) {
    for (auto r2_it = strctr.first_const_residue(); r2_it != strctr.last_const_residue(); ++r2_it) {
      cnt += collect(**r1_it, **r2_it, sink);
    }
  }
  //---------- show timer stats
  auto end = std::chrono::high_resolution_clock::now();
  logs << utils::LogLevel::INFO << "VdW interactions collected after "
       << (double) (std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()) << " [ms]\n";

  return cnt;
}

core::index4 VdWInteractionCollector::collect(const core::data::structural::Chain &chain,
                                              std::vector<ResiduePair_SP> &sink) {
  index4 cnt = 0;

  for (const Residue_SP ri:chain) {
    for (auto r_it = chain.cbegin(); r_it != chain.cend(); ++r_it) {
      cnt += collect(*ri, **r_it, sink);
    }
  }
  return cnt;
}

bool VdWInteractionCollector::calculate_vdw_distance(PdbAtom_SP atom_a, PdbAtom_SP atom_b,
                                                     std::vector<ResiduePair_SP> &sink) {
  double d2 = atom_a->distance_square_to(*atom_b, cutoff2_);
  if (d2 < cutoff2_) {
    double radius_a = core::chemical::AtomicElement::vdw_radius[(atom_a)->element_index()];
    double radius_b = core::chemical::AtomicElement::vdw_radius[(atom_b)->element_index()];
    if(sqrt(d2) <= radius_a + radius_b + MAX_DISTANCE) {
      logs << utils::LogLevel::FINER << "van der Waals interaction detected between " << *(atom_a->owner())
           << " and " << *(atom_b->owner()) << "\n";
      sink.push_back(std::make_shared<VdWInteraction>(atom_a, atom_b));
    }
    return true;
  }
  return false;
}

}
}
}
}


