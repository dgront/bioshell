
#include <core/calc/structural/interactions/ContactMap.hh>
#include <core/algorithms/basic_algorithms.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

using namespace core::data::structural;


ContactMap::ContactMap(const core::data::structural::Structure &structure, double cutoff_distance,
    selectors::AtomSelector_SP atoms_in_contact) :
    PairwiseResidueMap<index4>(structure), contact_distance(cutoff_distance), atom_selector(atoms_in_contact) {

  add(structure);
}

ContactMap::ContactMap(const core::data::structural::Structure &structure, double cutoff_distance,
                       const std::string &atom_in_contact) : PairwiseResidueMap<index4>(structure),
                                                             contact_distance(cutoff_distance) {

  atom_selector = std::make_shared<selectors::IsNamedAtom>(atom_in_contact);
  add(structure);
}

void ContactMap::add(const core::data::structural::Structure &structure) {

  std::shared_ptr<selectors::IsNamedAtom> named_selector = std::dynamic_pointer_cast<selectors::IsNamedAtom>(atom_selector);
  if (named_selector) {
    const std::string &atom_name = named_selector->selector_string();
    std::vector<core::data::structural::PdbAtom_SP> atoms(structure.count_residues());
    int i = -1;
    for (const core::data::structural::Chain_SP &ci : structure)
      for (const core::data::structural::Residue_SP &ri : (*ci))
        atoms[++i] = ri->find_atom(atom_name, nullptr);
    add(atoms);
  } else {
    int i = -1;
    std::vector<core::data::structural::Residue_SP> resids(structure.count_residues());
    for (const core::data::structural::Chain_SP &ci : structure)
      for (const core::data::structural::Residue_SP &ri : (*ci))
        resids[++i] = ri;

    if (atom_selector != nullptr) add(resids, atom_selector);
    else add(resids, std::make_shared<selectors::IsSC>());
  }
  nonempty_indexes(contacts_list_);
  std::sort(contacts_list_.begin(), contacts_list_.end());
}

void ContactMap::add(const std::vector<core::data::structural::Residue_SP> &residues,
                     core::data::structural::selectors::AtomSelector_SP selector) {

  for (core::index4 i = 1; i < residues.size(); ++i) {
    for (core::index4 j = 0; j < i; ++j) {
      double d = residues[j]->min_distance(*residues[i], selector, contact_distance);
      if (d < contact_distance) {
        index4 cnt = PairwiseResidueMap<index4>::at(i, j, 0);
        PairwiseResidueMap<index4>::add(i, j, cnt + 1);
        PairwiseResidueMap<index4>::add(j, i, cnt + 1);
      }
    }
  }
}

void ContactMap::add(const std::vector<core::data::structural::PdbAtom_SP> &atoms) {

  double contact_distance2 = contact_distance * contact_distance;
  for (core::index4 i = 1; i < atoms.size(); ++i) {
    if (atoms[i] == nullptr) continue;
    for (core::index4 j = 0; j < i; ++j) {
      if (atoms[j] != nullptr) {
        if (atoms[i]->distance_square_to(*atoms[j]) < contact_distance2) {
          index4 cnt = PairwiseResidueMap<index4>::at(i, j, 0);
          PairwiseResidueMap<index4>::add(i, j, cnt + 1);
          PairwiseResidueMap<index4>::add(j, i, cnt + 1);
        }
      }
    }
  }
}

double ContactMap::jaccard_overlap_coefficient(const ContactMap &another_map) const {

  return jaccard_overlap_coefficient(another_map.contacts_list_);
}

double ContactMap::jaccard_overlap_coefficient(
    const std::vector<std::pair<core::index2, core::index2>> &another_contacts) const {

  return core::calc::structural::interactions::jaccard_overlap_coefficient(contacts_list_, another_contacts);
}


double jaccard_overlap_coefficient(const std::vector<std::pair<core::index2, core::index2>> &contacts,
                                   const std::vector<std::pair<core::index2, core::index2>> &another_contacts) {

  index2 n_id = 0;
  for(const auto & ai:contacts) {
    for(const auto & aj:another_contacts) {
      if (ai.first == aj.first && ai.second == aj.second) ++n_id;
    }
  }

  return double(n_id) / double(contacts.size()+another_contacts.size()-n_id);
}

}
}
}
}
