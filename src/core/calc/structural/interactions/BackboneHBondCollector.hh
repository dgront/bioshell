/** \file InteractionCollector.hh
 * @brief Defines a collector of hydrogen bonds.
 */
#ifndef CORE_CALC_STRUCTURAL_INTERACTIONS_BackboneHBondCollector_HH
#define CORE_CALC_STRUCTURAL_INTERACTIONS_BackboneHBondCollector_HH

#include <string>
#include <memory>

#include <core/calc/structural/interactions/HydrogenBondCollector.hh>
#include <core/calc/structural/interactions/BackboneHBondInteraction.hh>

#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

using namespace data::structural;


/** @name peptide_hydrogen_group
 * @brief Methods that reconstruct a peptide plate hydrogen atom based on amide group geometry.
 *
 * The hydrogen atom is reconstructed based on the planar geometry of C(i-1)-N(i)-CA(i) atoms. Hydrogen atom
 * is placed on the bisector of C(i-1)-N(i)-CA(i)  planar angle
 * These methods will add the newly created atom to the respective residue.
 *
 * The example below shows how to reconstruct all amide hydrogens in a given protein structure
 * \include ex_peptide_hydrogen.cc
 */
///@{
/** @brief Finds the position of an amide hydrogen atom in a peptide group based on its geometry
 *
 * @param C - carbonyl C atom of the previous residue
 * @param N - amide N atom of the residue where H is to be reconstructed
 * @param CA - alpha carbon atom of the residue where H is to be reconstructed
 * @param H - where the coordinates of H will be stored
 * @return reference to the newly reconstructed atom
 */
core::data::basic::Vec3 &peptide_hydrogen(const core::data::basic::Vec3 &C, const core::data::basic::Vec3 &N,
                                          const core::data::basic::Vec3 &CA, core::data::basic::Vec3 &H);

/** @brief Finds the position of an amide hydrogen atom in a peptide group based on its geometry.
 *
 * The hydrogen atom is reconstructed based on the planar geometry of C(i-1)-N(i)-CA(i) atoms. Hydrogen atom
 * is placed on the bisector of C(i-1)-N(i)-CA(i)  planar angle
 * This method will add the newly created atom to the respective residue.
 * @param prev_residue - residue that directly precedes the residue being reconstructed
 * @param the_residue - a residue with the missing hydrogen
 * @param H - reference to a vector where the coordinates of the hydrogen atom will be stored
 */
core::data::basic::Vec3 &peptide_hydrogen(const core::data::structural::Residue_SP prev_residue,
                                          const core::data::structural::Residue_SP the_residue,
                                          core::data::basic::Vec3 &H);

/** @brief Creates an amide hydrogen atom in a peptide group and adds it to the residue.
 *
 * This method will add the newly created atom to the respective residue.
 * The residue will also be made the owner of the hydrogen atom.
 * @param prev_residue - residue that directly precedes the residue being reconstructed
 * @param the_residue - a residue with the missing hydrogen
 */
core::data::structural::PdbAtom_SP peptide_hydrogen(const core::data::structural::Residue_SP prev_residue,
                                                    const core::data::structural::Residue_SP the_residue);

/** @brief Creates an amide hydrogen atom in a peptide group.
 *
 * This method will NOT add the newly created atom to the respective residue.
 * @param C - carbonyl C atom
 * @param N - amide N atom whose hydrogen is reconstructed
 * @param CA - alpha carbon of the very next amino acid in this chain
 */
core::data::structural::PdbAtom_SP peptide_hydrogen(const core::data::structural::PdbAtom &C,
                                                    const core::data::structural::PdbAtom &N,
                                                    const core::data::structural::PdbAtom &CA);

///@}

/** @name peptide_hydrogen_dssp_group
 * @brief Methods that reconstruct a peptide plate hydrogen according to the DSSP method
 *
 * These methods use C(i-1) and O(i-1) atoms to reconstruct hydrogen atom at N(i)
 * placing it alogn a vector parallel to O->C vector of a carbonyl group of the relevant peptide plate
 * The example below shows how to reconstruct all amide hydrogens in a given protein structure
 * \include ex_peptide_hydrogen.cc
 */
///@{
/** @brief Finds the position of an amide hydrogen atom according to the DSSP method
 *
 * @param C - carbonyl C atom of the previous residue
 * @param O - carbonyl O atom of the previous residue
 * @param N - amide N atom of the residue where H is to be reconstructed
 * @param H - where the coordinates of H will be stored
 * @return reference to the newly reconstructed atom
 */
core::data::basic::Vec3 &peptide_hydrogen_dssp(const core::data::basic::Vec3 &C, const core::data::basic::Vec3 &O,
                                               const core::data::basic::Vec3 &N, core::data::basic::Vec3 &H);

/** @brief Finds the position of an amide hydrogen atom in a peptide group based on its geometry
 * @param prev_residue - residue that directly precedes the residue being reconstructed
 * @param the_residue - a residue with the missing hydrogen
 * @param H - reference to a vector where the coordinates of the hydrogen atom will be stored
 */
core::data::basic::Vec3 &peptide_hydrogen_dssp(const core::data::structural::Residue_SP prev_residue,
                                               const core::data::structural::Residue_SP the_residue,
                                               core::data::basic::Vec3 &H);

/** @brief Creates an amide hydrogen atom in a peptide group and adds it to the residue.
 *
 * Unlike the other methods of this group, this method will add the newly created atom to the respective residue.
 * The residue will also be made the owner of the hydrogen atom.
 * @param prev_residue - residue that directly precedes the residue being reconstructed
 * @param the_residue - a residue with the missing hydrogen
 */
core::data::structural::PdbAtom_SP peptide_hydrogen_dssp(const core::data::structural::Residue_SP prev_residue,
                                                         const core::data::structural::Residue_SP the_residue);
///@}


class BackboneHBondCollector : public HydrogenBondCollector {
public:
  /// Default constructor
  BackboneHBondCollector() : logs("BackboneHBondCollector"), cutoff_value_(0.0) {
    logs << utils::LogLevel::INFO << "detecting hydrogen bonds with DSSP energy below: " << cutoff_value_ << "\n";
  }

  /// Default virtual destructor
  virtual ~BackboneHBondCollector() = default;

  /** @brief Defines an energy cutoff to filter out spurious hydrogen bonds.
   *
   * The collector will collect only these bonds whose energy computed according to the DSSP formula
   * is below that cutoff. Bu default the cutoff value is set to -0.4, as in the original DSSP paper.
   * @param cutoff_value - new energy value used to filter out hydrogen bonds (in [kcal/mol])
   */
  void dssp_energy_cutoff(double cutoff_value) {
    cutoff_value_ = cutoff_value;
    logs << utils::LogLevel::INFO << "detecting hydrogen bonds with DSSP energy below: " << cutoff_value_ << "\n";
  }

  /** @brief Return the current value of the energy cutoff that is used to to filter out spurious hydrogen bonds.
   *
   * @return cutoff value in [kcal/mol]
   */
  double dssp_energy_cutoff() { return cutoff_value_; }

  static BackboneHBondInteraction_SP create_bond(const PdbAtom_SP donating_n, const PdbAtom_SP the_h,
                                                 const PdbAtom_SP acceptor_o, const PdbAtom_SP acceptor_c,
                                                 const double dssp_energy_cutoff);

protected:
  /** Collects interactions based on atoms stored in donor_atoms and acceptor_atoms private vectors
   */
  virtual core::index4 collect_prepared_interactions(std::vector<ResiduePair_SP> &sink);

  void prepare_donor_atoms(const core::data::structural::Residue &ri,
                           std::vector<std::pair<core::data::structural::PdbAtom_SP, core::data::structural::PdbAtom_SP>> &donor_atoms);

  void prepare_acceptor_atoms(const core::data::structural::Residue &ri,
                              std::vector<std::pair<core::data::structural::PdbAtom_SP, core::data::structural::PdbAtom_SP>> &donor_atoms);

private:
  utils::Logger logs;
  double cutoff_value_;
};

}
}
}
}

#endif

