#include <vector>

#include <core/calc/structural/angles.hh>
#include <core/calc/structural/interactions/HydrogenBondInteraction.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

const double HydrogenBondInteraction::N_H_BOND_LENGTH = 1.0;

std::ostream & operator<<(std::ostream & o, const HydrogenBondInteraction & hb) {

  using core::calc::structural::to_degrees;

  const core::data::structural::Residue & d = *hb.first_residue();
  const core::data::structural::Residue & a = *hb.second_residue();
  o << utils::string_format("%4s %4d%c  %3s %4s -> %4s %3s %4d%c %4s : %5.3f %5.3f %6.2f %6.2f %7.2f",
      d.owner()->id().c_str(), d.id(), d.icode(), d.residue_type().code3.c_str(), hb.hydrogen->atom_name().c_str(),
      hb.acceptor_atom->atom_name().c_str(), a.residue_type().code3.c_str(), a.id(), a.icode(), a.owner()->id().c_str(),
      hb.r_AH_, hb.r_AD_, to_degrees(hb.a_aAH_), to_degrees(hb.a_aHP_), to_degrees(hb.t_aAHD_));
  return o;
}

std::string HydrogenBondInteraction::output_header() {
  return "# donor residue   | acceptor residue  : d(AH) d(AD) a(DHA) a(HAB) t(bAHD)";
}

HydrogenBondInteraction::HydrogenBondInteraction(const PdbAtom_SP donor_atom, const PdbAtom_SP hydrogen,
    const PdbAtom_SP acceptor_atom, const PdbAtom_SP behind_acceptor_atom) :
        ResiduePair(donor_atom->owner(), acceptor_atom->owner()), donor_atom(donor_atom),
        acceptor_atom(acceptor_atom), behind_acceptor_atom(behind_acceptor_atom), hydrogen(hydrogen) {
  compute_geometry();
}

void HydrogenBondInteraction::compute_geometry() {

  r_AH_ = acceptor_atom->distance_to(*hydrogen);
  r_AD_ = acceptor_atom->distance_to(*donor_atom);
  a_aAH_ = evaluate_planar_angle(*donor_atom, *hydrogen, *acceptor_atom);
  a_aHP_ = evaluate_planar_angle(*hydrogen, *acceptor_atom, *behind_acceptor_atom);
  t_aAHD_ = evaluate_dihedral_angle(*behind_acceptor_atom, *acceptor_atom, *hydrogen, *donor_atom);
}

}
}
}
}