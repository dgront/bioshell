#ifndef CORE_CALC_STRUCTURAL_HydrogenBondInteraction_HH
#define CORE_CALC_STRUCTURAL_HydrogenBondInteraction_HH

#include <vector>
#include <iostream>

#include <core/index.hh>
#include <core/data/structural/ResiduePair.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.hh>

#include <core/calc/structural/angles.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

using namespace core::data::structural;

/** @brief Describes geometry of a hydrogen bond.
 *
 * This class provides some geometric properties of a hydrogen bond such as distances and angles.
 * \todo_doc better description, add graphics that shows how the planar and the dihedral angles are defined
 */
class HydrogenBondInteraction: public ResiduePair {
public:

  friend std::ostream &operator<<(std::ostream &o, const HydrogenBondInteraction &hb);

  const static double N_H_BOND_LENGTH; ///< Used in reconstruction of amide H position

  const PdbAtom_SP donor_atom; ///< Atom that donates H atom (usually amide N or hydroxyl O)
  const PdbAtom_SP acceptor_atom; ///< Atom that accepts H atom (usually carbonyl O)
  const PdbAtom_SP behind_acceptor_atom; ///< Atom that is covalently connected to the acceptor, e.g. carbonyl C in C=O group
  const PdbAtom_SP hydrogen; ///< The hydrogen atom

  /** @brief Creates an object that describes a hydrogen bond geometry.
   *
   * @param donor_atom - atom that donates its hydrogen atom to the bond
   * @param hydrogen - the hydrogen atom involved in this bond
   * @param acceptor_atom - the atom that accepts the hydrogen to from this bond
   * @param behind_acceptor_atom - another heavy atom bound to the <code>acceptor_atom</code>. It is necessary to define
   * a dihedral angle. A good choice is to use carbonyl carbon atom in the case of a hydrogen bond formed by a peptide group.
   */
  HydrogenBondInteraction(const PdbAtom_SP donor_atom, const PdbAtom_SP hydrogen, const PdbAtom_SP acceptor_atom,
                          const PdbAtom_SP behind_acceptor_atom);

  /// The residue which donates its hydrogen in this hydrogen bond
  const core::data::structural::Residue_SP donor_residue() const { return first_residue(); }

  /// The residue which accepts a hydrogen in this hydrogen bond
  const core::data::structural::Residue_SP acceptor_residue() const { return second_residue(); }

  /// Returns the distance between a hydrogen atom and an acceptor atom (e.g. carbonyl oxygen)
  inline double r_AH() const { return r_AH_; }

  /// Returns the distance between a donor atom and an acceptor atom (e.g. between amine nitrogen and carbonyl oxygen)
  inline double r_AD() const { return r_AD_; }

  /** @brief Returns the planar angle (in radians) formed by this hydrogen bond
   * @return the planar angle created by the three atoms: a donor, the hydrogen and an acceptor; e.g. the O..H-N angle
   */
  inline double planar() const { return a_aAH_; }

  /** @brief Returns the planar angle (in radians) formed by atom behind the donor
   * @return the planar angle created by the three atoms: the hydrogen, an acceptor and the atom behind the acceptor;
   * e.g. the C=O..H  angle
   */
  inline double planar_behind() const { return a_aHP_; }

  /// Returns the dihedral angle (in radians) formed by this hydrogen bond
  inline double dihedral() const { return t_aAHD_; }

  /** @brief Returns a string that is a nicely formatted header for data printed by ostream operator.
   * Use this string as a header for a nicely formatted table of hydrogen bonds detected by this class.
   * Note, that output produced by BackboneHBondInteraction is a bit different ant that class provides its own header line
   * @return a table header as a string
   */
  static std::string output_header();

protected:
  /** @brief Calculates geometry of the bond: distances, planar and dihedral angles.
   * These values are stored as private properties of this object.
   */
  void compute_geometry();

private:
  double r_AH_; ///< The distance between donor atom and hydrogen atom (the first spherical coordinate of H atom)
  double r_AD_; ///< The distance between donor atom and hydrogen atom (the first spherical coordinate of H atom)
  double a_aAH_; ///< planar angle from acceptor to the H atom
  double a_aHP_; ///< planar angle from hydrogen to the behind-acceptor atom
  double t_aAHD_; ///< dihedral angle from the acceptor to the donor atom
};

/// @brief Prints which atoms are involved and geometric parameters of a hydrogen bond
std::ostream &operator<<(std::ostream &o, const HydrogenBondInteraction &hb);

typedef std::shared_ptr<HydrogenBondInteraction> HydrogenBondInteraction_SP;

}
}
}
}
#endif // ~CORE_CALC_STRUCTURAL_HydrogenBondInteraction_HH
