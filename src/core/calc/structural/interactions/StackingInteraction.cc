#include <core/calc/structural/interactions/StackingInteraction.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

StackingInteraction::StackingInteraction(const std::vector<PdbAtom_SP> & first_ring, const std::vector<PdbAtom_SP> & second_ring):
	ResiduePair(first_ring[0]->owner(), second_ring[0]->owner()){

	for (const auto atom: first_ring) ring_a.push_back(atom);
	for (const auto atom: second_ring) ring_b.push_back(atom);

	compute_geometry();
}

void StackingInteraction::compute_geometry(){

	using namespace core::calc::structural::transformations; // --- transformations
	
	//calculating center mass of both rings
	Vec3 cm_a;
	for (auto vi:(ring_a)) cm_a += *vi;
	cm_a /= ring_a.size();

	Vec3 cm_b;
	for (auto vi:(ring_b)) cm_b += *vi;
	cm_b /= ring_b.size();

	Rototranslation_SP rt_a = local_BBQ_coordinates(*ring_a[2],cm_a,*ring_a[4]);
	Rototranslation_SP rt_b = local_BBQ_coordinates(*ring_b[2],cm_b,*ring_b[4]);	

	rt_a->apply(cm_b);

  // ---------- calculating c1-cm and c3-cm for ring_b
	Vec3 atom1 = *ring_b[2];
	Vec3 atom2 = *ring_b[4];
	rt_a->apply(atom1);
	atom1-=cm_b;
	rt_a->apply(atom2);
	atom2-=cm_b;

	// ---------- and cross product to calculate normal vector
	auto x = atom1.y*atom2.z - atom1.z *atom2.y;
	auto y = atom1.z *atom2.x -atom1.x *atom2.z;
	auto z = atom1.x*atom2.y - atom1.y  *atom2.x;

	Vec3 norm_for_b(x,y,z);

	a_ = acos(y/norm_for_b.length())*180/3.14159; // dot product of ring_a (it is just Y axis (0,1,0) ) and ring_b normal vectors

	z_ = fabs(cm_b.y); 
	xy_ = sqrt(cm_b.x * cm_b.x +cm_b.z *cm_b.z);
}


std::string StackingInteraction::output_header() {
  return "#first residue |second residue:   z     xy  angle  r";
}

std::ostream &operator<<(std::ostream &o, const StackingInteraction &si){

  const core::data::structural::Residue & d = *si.first_residue();
  const core::data::structural::Residue & a = *si.second_residue();

  o << utils::string_format("%4s %4d%c %3s     %4s %4d%c %3s  %6.2f %6.2f %6.2f %6.2f",
                            d.owner()->id().c_str(), d.id(), d.icode(), d.residue_type().code3.c_str(),
                            a.owner()->id().c_str(), a.id(),
                            a.icode(), a.residue_type().code3.c_str(),
                            si.xy(), si.z(), si.angle(), si.r());
  return o;
}

}
}
}
}

