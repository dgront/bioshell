/** \file ResidueInteraction.hh
 * @brief Defines an abstract interaction.
 */
#ifndef CORE_CHEMICAL_ResidueInteraction_HH
#define CORE_CHEMICAL_ResidueInteraction_HH

#include <memory>

#include <core/data/structural/Residue.fwd.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

/** \brief Defines an abstract interaction between two residues.
 */
class ResidueInteraction {
public:

  /** @brief Base class constructor to define an abstract interaction between two residues
   *
   * @param first_residue - first residue
   * @param second_residue - second residue
   */
  ResidueInteraction(const core::data::structural::Residue_SP first_residue,
                     const core::data::structural::Residue_SP second_residue) :
      first_residue_(first_residue), second_residue_(second_residue) {}

  /// Default virtual destructor
  virtual ~ResidueInteraction() {}

  const core::data::structural::Residue_SP first_residue() const { return first_residue_; }

  const core::data::structural::Residue_SP second_residue() const { return second_residue_; }

protected:
  const core::data::structural::Residue_SP first_residue_; ///< the first interacting residue
  const core::data::structural::Residue_SP second_residue_; ///< the second interacting residue
};

/// A shared pointer to ResidueInteraction type - for polymorphic behavior
typedef std::shared_ptr<ResidueInteraction> ResidueInteraction_SP;

}
}
}
}

#endif

