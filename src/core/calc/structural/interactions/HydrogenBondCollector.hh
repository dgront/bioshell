/** \file HydrogenBondCollector.hh
 * @brief Defines a collector of hydrogen bonds.
 */
#ifndef CORE_CALC_STRUCTURAL_INTERACTIONS_HydrogenBondCollector_HH
#define CORE_CALC_STRUCTURAL_INTERACTIONS_HydrogenBondCollector_HH

#include <string>
#include <map>

#include <core/data/structural/ResiduePairCollector.hh>
#include <core/data/structural/ResiduePair.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

/** @brief Detects hydrogen bonds in a macromolecular structure.
 *
 * The collector picks all donors and all acceptors from residues of a given object (Structure, Chain, etc)
 * and checks each-vs-each for a possible hydrogen bonds. Protein backbone hydrogen bonds are reported
 * as BackboneHBondInteraction instances, all others as HydrogenBondInteraction objects.
 *
 * The donor and acceptor atoms detected in the most recent <code>collect()</code> method call
 * can be retrieved by <code>recent_donor_atoms()</code> and <code>recent_acceptor_atoms()</code> methods, respectively.
 * These donors and acceptors are assigned based on chemical structure of a monomer. Respective MonomerStructure
 * objects are retrieved from MonomerStructureFactory. Alternatively, donors and acceptors may be defined with
 * <code>add_donor_definition()</code> and <code>add_acceptor_definition()</code> methods.
 *
 * The input structure must provide hydrogen atoms, otherwise no hydrogen bonds can be detected!
 *
 * @see MonomerStructureFactory
 */
class HydrogenBondCollector : public core::data::structural::ResiduePairCollector {
public:
  /// Define the maximum allowed distance between a hydrogen atom and its acceptor to still record a hydrogen bond
  static constexpr const double MAX_AH_DISTANCE = 3.0;

  /// Define the maximum allowed distance between the two heavy atoms: donor and acceptor to still record a hydrogen bond
  static constexpr const double MAX_AD_DISTANCE = 4.0;

  /// The minimum value of O..H-N angle to exist in a hydrogen bond (in degrees). If the angle is smaller, a H-bond is not detected
  static constexpr const double MIN_AHD_ANGLE = 100;

  /// The minimum value of C=O..H angle to exist in a hydrogen bond (in degrees). If the angle is smaller, a H-bond is not detected
  static constexpr const double MIN_PAH_ANGLE = 100;

  /** @brief Create an object that collects all hydrogen bonds from a given structures.
   * @param include_protein_backbone - if true (which is the default), the filter will also include protein backbone
   * as hydrogen bond donors / acceptors. If false, only amino acid side chains will be taken into account
   */
  explicit HydrogenBondCollector(bool include_protein_backbone = true);

  /// Default virtual destructor
  virtual ~HydrogenBondCollector() = default;

  /** @brief Collect hydrogen bonds between the two given residues.
   * Each of the two residues can be both a donor and an acceptor of a hydrogen bond, so this method is symmetric.
   * If a backbone-backbone hydrogen bond is detected, an instance of BackboneHBondInteraction will be created
   * rather than a HydrogenBondInteraction object.
   * @param i_residue - an amino acid residue
   * @param j_residue - an amino acid residue
   * @param sink - a vector where the newly created hydrogen bonds are pushed back
   * @return the number of hydrogen bonds created
   */
  virtual core::index4 collect(const core::data::structural::Residue &i_residue,
                               const core::data::structural::Residue &j_residue,
                               std::vector<core::data::structural::ResiduePair_SP> &sink);

  /** @brief Collect hydrogen bonds between the two given residues.
   * @param strctr - a structure where to look for hydrogen bonds. This method attempts to find all hydrogen bonds,
   * between all protein chains of this structure.
   * If a backbone-backbone hydrogen bond is detected, an instance of BackboneHBondInteraction will be created
   * rather than a HydrogenBondInteraction object.
   * @param sink - a vector where the newly created hydrogen bonds are pushed back
   * @return the number of hydrogen bonds created
   */
  virtual core::index4 collect(const core::data::structural::Structure &strctr,
                               std::vector<core::data::structural::ResiduePair_SP> &sink);
  /** @brief Collect hydrogen bonds between the two given residues.
   * @param chain - a chain where to look for hydrogen bonds. This method attempts to find all hydrogen bonds
   * within this chain only. Bonds to other chains will not be reported.
   * If a backbone-backbone hydrogen bond is detected, an instance of BackboneHBondInteraction will be created
   * rather than a HydrogenBondInteraction object.
   * @param sink - a vector where the newly created hydrogen bonds are pushed back
   * @return the number of hydrogen bonds created
   */
  virtual core::index4 collect(const core::data::structural::Chain &chain,
                               std::vector<core::data::structural::ResiduePair_SP> &sink);

  /** @brief Provides the list of hydrogen bond donors collected by the most recent <code>collect()</code> call.
   *
   * Each hydrogen bond donor is represented by a pair of two atoms: hydrogen donors atom (e.g. amine nitrogen)
   * and the hydrogen atom that it donates.
   * The returned vector of donors will be not empty only if <code>collect()</code> has been called.
   *
   * @return a list of hydrogen bond donors collected during the most recent <code>collect()</code> call.
   */
  const std::vector<std::pair<core::data::structural::PdbAtom_SP, core::data::structural::PdbAtom_SP>> &
      recent_donor_atoms() const { return donor_atoms; }

  /** @brief Provides the list of hydrogen bond acceptors collected by the most recent <code>collect()</code> call.
   *
   * Each hydrogen bond acceptor is represented by a pair of two atoms: hydrogen acceptor atom (e.g. carbonyl oxygen)
   * and its directly preceding atom, e.g. (e.g. carbonyl carbon).
   * The returned vector of acceptors will be not empty only if <code>collect()</code> has been called.
   *
   * @return a list of hydrogen bond acceptors collected during the most recent <code>collect()</code> call.
   */
  const std::vector<std::pair<core::data::structural::PdbAtom_SP, core::data::structural::PdbAtom_SP>> &
      recent_acceptor_atoms() const { return acceptor_atoms; }

  /** @brief Add a new atom to the internal list of possible hydrogen bond donors.
   *
   * Only atoms from the internal list will be considered as donors. If your system contains also ligands, you must
   * register donor atoms with this method
   * @param residue_code3 - 3-letter code of a donor residue
   * @param donor_atom_name - 4-letter code of a donor (heavy) atom
   * @param hydrogen_atom_name - 4-letter code of a donated hydrogen atom
   */
  void add_donor_definition(const std::string &residue_code3,
                            const std::string &donor_atom_name, const std::string &hydrogen_atom_name);

  /** @brief Add a new atom to the internal list of possible hydrogen bond acceptors.
   *
   * Only atoms from the internal list will be considered as acceptors. If your system contains also ligands, you must
   * register acceptor atoms with this method
   * @param residue_code3 - 3-letter code of an acceptor residue
   * @param acceptor_atom_name - 4-letter code of an acceptor atom
   * @param preceding_atom_name - 4-letter code of an atom directly preceding the aceptor, e.g. a carbonyl C
   */
  void add_acceptor_definition(const std::string &residue_code3, const std::string &accetor_atom_name,
      const std::string &preceding_atom_name);

  /** @brief Rename atoms of a monomer.
   *
   * This method renames atoms of a monomer as it is observed by this class. Original MonomerStructure object
   * will not be affected.
   *
   * @param old_code3 - code3 of a monomer to be renamed (as it is defined in CIF files loaded by
   *    MonomerStructureFactory)
   * @param new_code3 - the new code to be assigned to this monomer (may be the same os the old one)
   * @param names_dict - dictionary that defines what atoms should be renamed: old atom name should be a key
   * assigned to the respective new atom name value (four character strings)
   */
  void rename_atoms_in_monomer_definition(const std::string &old_code3, const std::string &new_code3,
                                          std::map<std::string, std::string> names_dict);

protected:
  /** @brief H-bond donors detected during the most recent collect() call.
   * Each donor is defined as a pair of atoms: donor heavy atom and the hydrogen
   */
  std::vector<std::pair<core::data::structural::PdbAtom_SP, core::data::structural::PdbAtom_SP>> donor_atoms;

  /** @brief H-bond acceptors detected during the most recent collect() call.
   * Each acceptor is defined as a pair of atoms: acceptor atom and a preceding heavy atom (e.g. carbonyl C in C=O group)
   */
  std::vector<std::pair<core::data::structural::PdbAtom_SP, core::data::structural::PdbAtom_SP>> acceptor_atoms;

  bool test_atom(const core::data::structural::PdbAtom_SP a, const std::string& atom_name,
      const core::data::structural::Residue & res);

  /** @brief Collects interactions based on atoms stored in donor_atoms and acceptor_atoms private vectors.
   *
   * Goes through each donor-acceptor group in double nested loop and detects possible interactions.
   * Once a hydrogen bond is detected, a respective object will be created and pushed back to a given sink vector
   * @param sink - a vector to store interaction objects (HydrogenBondInteraction instances)
   * @return the number of HydrogenBondInteraction   objects created
   */
  virtual core::index4 collect_prepared_interactions(std::vector<core::data::structural::ResiduePair_SP> &sink);

  /// Prepares donor atoms
  virtual void prepare_donor_atoms(const core::data::structural::Residue &ri,
            std::vector<std::pair<core::data::structural::PdbAtom_SP, core::data::structural::PdbAtom_SP>> & donor_atoms);

  /// Prepares acceptor atoms
  virtual void prepare_acceptor_atoms(const core::data::structural::Residue &ri,
            std::vector<std::pair<core::data::structural::PdbAtom_SP, core::data::structural::PdbAtom_SP>> & donor_atoms);

private:
  utils::Logger logs;
  bool include_protein_backbone_;

std::map<std::string, std::vector<std::pair<std::string,std::string>>> donors_;
std::map<std::string, std::vector<std::pair<std::string,std::string>>> acceptors_;

};

}
}
}
}

#endif

