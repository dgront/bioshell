/** \file StackingInteraction.hh
 * @brief Defines a collector of hydrogen bonds.
 */
#ifndef CORE_CALC_STRUCTURAL_INTERACTIONS_StackingInteractionCollector_HH
#define CORE_CALC_STRUCTURAL_INTERACTIONS_StackingInteractionCollector_HH

#include <map>

#include <core/data/structural/ResiduePairCollector.hh>
#include <core/chemical/MonomerStructureFactory.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

using namespace core::data::structural;
using namespace core::chemical;

/** @brief Detects stacking interactions in a macromolecular structure.
 *
 * The collector first finds all aromatic rings in a given object (Structure, Chain, etc)
 * and checks each-vs-each for a possible interaction.
 *
 * The rings detected in the most recent <code>collect()</code> method call
 * can be retrieved by <code>recent_ring_atoms()</code>. Detection of rings is based on MonomerStructure definition
 * which knows which atoms of a monomer are actually aromatic
 *
 * @see MonomerStructureFactory
 */
class StackingInteractionCollector : public core::data::structural::ResiduePairCollector {
public:
  /// Define the maximum allowed distance between the centers of the two rings
  static constexpr const double MAX_DISTANCE = 7.0;

  /** @brief Create an object that collects stacking interactions
   */
  StackingInteractionCollector();
  /// Default virtual destructor
  virtual ~StackingInteractionCollector() = default;

  virtual core::index4 collect(const core::data::structural::Residue &i_residue,
                                            const core::data::structural::Residue &j_residue,
                                            std::vector<ResiduePair_SP> &sink);

  virtual core::index4 collect(const core::data::structural::Structure &strctr,
                                            std::vector<ResiduePair_SP> &sink);

  virtual core::index4 collect(const core::data::structural::Chain &chain,
                                            std::vector<ResiduePair_SP> &sink);

  /** @brief Provides the list of aromatic rings atoms collected by the most recent <code>collect()</code> call.
   *
   * Each aromatic ring is represented by a vector of its atoms.
   *
   * @return a list of aromatic rings atoms collected during the most recent <code>collect()</code> call.
   */
  const std::vector<std::vector<core::data::structural::PdbAtom_SP>> & recent_ring_atoms() const { return ring_atoms; }

  /** @brief Add a new atom to the internal list of possible aromatic rings.
   *
   * Only atoms from the internal list will be considered as aromatic rings. If your system contains also ligands, you must
   * register aromatic ring atoms with this method
   * @param residue_code3 - 3-letter code of a donor residue
   * @param ring_atom_names - a vector of 4-letter names of ring atoms
   */
  void add_ring_definition(const std::string &residue_code3, const std::vector<std::string> & ring_atom_names);

  /** @brief Rename atoms of a monomer.
   *
   * This method renames atoms of a monomer as it is observed by this class. Original MonomerStructure object
   * will not be affected.
   *
   * @param old_code3 - code3 of a monomer to be renamed (as it is defined in CIF files loaded by
   *    MonomerStructureFactory)
   * @param new_code3 - the new code to be assigned to this monomer (may be the same os the old one)
   * @param names_dict - dictionary that defines what atoms should be renamed: old atom name should be a key
   * assigned to the respective new atom name value (four character strings)
   */
  void rename_atoms_in_monomer_definition(const std::string &old_code3, const std::string &new_code3,
                                          std::map<std::string, std::string> names_dict);

protected:
  /** @brief Aromatic rings detected during the most recent collect() call.
   * Each ring is stored as a vector of its atoms. This vector of rings is thus a 2D array or atoms
   */
  std::vector<std::vector<core::data::structural::PdbAtom_SP>> ring_atoms;

  /// Prints an error message if a givena tom is missing
  bool test_atom(const PdbAtom_SP a, const std::string &atom_name, const Residue &res);

  /** @brief Collects interactions based on atoms stored in ring_a and ring_b private vectors.
   *
   * Goes through each ring_a and ring_b group in double nested loop and detects possible interactions.
   * Once a stacking interaction is detected, a respective object will be created and pushed back to a given sink vector
   * @param sink - a vector to store interaction objects (StackingInteraction instances)
   * @return the number of StackingCollector objects created
   */
  virtual core::index4 collect_prepared_interactions(std::vector<ResiduePair_SP> &sink);

  /// Collects rings from a given residue
  virtual void prepare_ring_atoms(const core::data::structural::Residue &ri,
                                  std::vector<std::vector<core::data::structural::PdbAtom_SP>> & ring_atoms);

private:
  utils::Logger logs;
  std::map<std::string, std::vector<std::vector<std::string>>> ring_definitions_ ;

};

}
}
}
}

#endif

