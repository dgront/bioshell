#ifndef CORE_CALC_STRUCTURAL_ContactMap_HH
#define CORE_CALC_STRUCTURAL_ContactMap_HH

#include <core/index.hh>
#include <core/calc/structural/interactions/PairwiseResidueMap.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Structure.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

/** @brief Holds a contact map computed for a protein structure.
 *
 * More than one structure (model) of a given macromolecule may be accumulated in this map.
 *
 * By default this object calculates contacts only between side chains. A contact is recorded if any two atoms coming
 * from two different residues are closer to each other than a given cutoff (specified when this object is constructed)
 *
 * \include ap_contact_map.cc
 * \include ap_contact_map_overlap.cc
 */
class ContactMap : public PairwiseResidueMap<index4> {
public:

  /// Contact distance
  const double contact_distance;

  /** Creates a contact map for a given biomacromolecular structure
   *
   * @param structure - input structure
   * @param cutoff_distance - contact distance; use from 4.5 to 5.0 for all-atom contact map, 10.0 for CA etc
   * @param atoms_in_contact - atom selector used to define which atoms may form a contact; It's <code>nullptr</code>
   *    by default which means all the atoms from both residues participate. Use <code>IsSC</code> instance to restrict contacts
   *    to side chain or <code>IsCA</code> to compute CA-only contact map
   */
  ContactMap(const core::data::structural::Structure &structure, double cutoff_distance,
             core::data::structural::selectors::AtomSelector_SP atoms_in_contact = nullptr);

  /** Creates a contact map for a given biomacromolecular structure based on particular atoms
   *
   * @param structure - input structure
   * @param cutoff_distance - contact distance; use from 4.5 to 5.0 for all-atom contact map, 10.0 for CA etc
   * @param atoms_in_contact - use <code>" CA "</code> to compute CA-only contact map or
   *    <code>" CB "</code> to compute CB-only contact map
   */
  ContactMap(const core::data::structural::Structure &structure, double cutoff_distance,
             const std::string &atom_in_contact);

  /** @brief Accumulates additional structure to this map
   *
   * @param structure - another model of the same biomacromolecular as was used to construct this object.
   */
  void add(const core::data::structural::Structure &structure);

  /** Accumulates additional contact map, computed for a given vector of residues.
   *
   * @param residues - residues of the input structure; the number of residues in this input vector must match exactly
   *    the number of residues od the structure used to construct this object
   * @param selector - selector used to restrict the set of atoms used to calculate contacts
   */
  void add(const std::vector<core::data::structural::Residue_SP> &residues,
           core::data::structural::selectors::AtomSelector_SP selector);

  /** Accumulates additional contact map, computed for a given vector of atoms.
   *
   * The purpose of this method is to calculate map in one-atom-per-residue scenario (e.g. contact map based on C-alpha atoms).
   * The number of atoms in the input vector must match exactly the number of residues od the structure.
   * used to construct this object. This special case is much faster that calculating contact map on whole residues
   * with <code>IsCA</code> selector.
   *
   * @param atoms - atoms of the input structure
   */
  void add(const std::vector<core::data::structural::PdbAtom_SP> &atoms);

  /** @brief Calculates Jaccard coefficient to measure the overlap
   *
   * Both contact maps must be of the same size; it is assumed the two maps are derived for two different structures
   * of the same protein, e.g. the native and a model. Jaccard coefficient is 1.0 when the two maps are identical and 0.0
   * when completely different
   *
   * @param another_map - another contact map to be compared
   * @return Jaccard coefficient value from 0.0 to 1.0
   */
  ///@{

  /** @brief  Calculates Jaccard coefficient to measure the overlap between this map and another contact map.
   *
   * @param another_map - another contact map to be compared
   * @return Jaccard coefficient value from 0.0 to 1.0
   */
  double jaccard_overlap_coefficient(const ContactMap &another_map) const;

  /** @brief  Calculates Jaccard coefficient to measure the overlap between this map and another contact map.
   *
   * @param another_contacts - vector of index pairs that lists residues in contact
   * @return Jaccard coefficient value from 0.0 to 1.0
   */
  double jaccard_overlap_coefficient(const std::vector<std::pair<core::index2, core::index2>> &another_contacts) const;
///@}

  /// Returns a list of all contacts recorded by this map.
  const std::vector<std::pair<core::index2, core::index2>> &contacts_list() const { return contacts_list_; }

private:
  core::data::structural::selectors::AtomSelector_SP atom_selector;
  std::vector<std::pair<core::index2, core::index2>> contacts_list_;
};

typedef std::shared_ptr<ContactMap> ContactMap_SP;

/** @brief  Calculates Jaccard coefficient to measure the overlap between two lists of contacts
 *
 * @param contacts - vector of index pairs that lists residues in contact
 * @param another_contacts - vector of index pairs that lists residues in contact
 * @return Jaccard coefficient value from 0.0 to 1.0
 */
double jaccard_overlap_coefficient(const std::vector<std::pair<core::index2, core::index2>> &contacts,
                                   const std::vector<std::pair<core::index2, core::index2>> &another_contacts);

}
}
}
}
#endif // ~CORE_CALC_STRUCTURAL_ContactMap_HH
