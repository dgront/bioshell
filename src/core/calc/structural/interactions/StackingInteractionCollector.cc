#include <core/calc/structural/interactions/StackingInteractionCollector.hh>
#include <core/calc/structural/interactions/StackingInteraction.hh>
#include <utils/string_utils.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

StackingInteractionCollector::StackingInteractionCollector() : logs("StackingInteractionCollector") {

  MonomerStructureFactory factory = MonomerStructureFactory::get_instance();
  for (auto monomer_it = factory.cbegin(); monomer_it != factory.cend(); ++monomer_it) {
    std::vector<std::vector<std::string>> v_rings;
    logs << utils::LogLevel::FINE << "processing " << monomer_it->first << "\n";
    auto mnmr = (*monomer_it).second;
    for (auto ring:mnmr->rings()) {
      std::vector<std::string> v_one_ring;
      if (ring.size() < 5) continue;
      if (ring.size() > 7) continue;
      bool is_aromatic = false;
      for(PdbAtom_SP ai:mnmr->aromatic_atoms()) {
        if (ai==ring[0]) {
          is_aromatic = true;
          break;
        }
      }
      if(!is_aromatic) continue;

      for (auto ring_atom:ring)
        v_one_ring.push_back(ring_atom->atom_name());
      v_rings.push_back(v_one_ring);
    }
    ring_definitions_[mnmr->code3] = v_rings;
  }

  if (logs.is_logable(utils::LogLevel::FINE)) {
    for (auto r:ring_definitions_) {
      logs << utils::LogLevel::FINE << "Ring definitions for " << r.first << "\n";
      int i = 0;
      for (auto ri:r.second) logs << "\tring " << (++i) << " :  atoms:" << ri.size() << "\n";
    }
  }
}

bool StackingInteractionCollector::test_atom(const PdbAtom_SP a, const std::string &atom_name, const Residue &res) {

  if (a == nullptr) {
    logs << utils::LogLevel::WARNING << "Skipping possible stacking interaction due to missing atom " << atom_name
         << " of residue " << res << "\n";
    return false;
  }

  return true;
}

core::index4 StackingInteractionCollector::collect(const core::data::structural::Residue &i_residue,
                                                                const core::data::structural::Residue &j_residue,
                                                                std::vector<ResiduePair_SP> &sink) {
  ring_atoms.clear();
  prepare_ring_atoms(i_residue, ring_atoms);
  prepare_ring_atoms(j_residue, ring_atoms);

  if(ring_atoms.size()==0 )
    return 0;

  return collect_prepared_interactions(sink);
}

core::index4 StackingInteractionCollector::collect(const core::data::structural::Structure &strctr,
                                                                std::vector<ResiduePair_SP> &sink) {
  ring_atoms.clear();
  for(const Chain_SP ci:strctr) {
    for(auto r_it = ci->cbegin(); r_it != ci->cend(); ++r_it) {
      const Residue & ri = **r_it;
      prepare_ring_atoms(ri, ring_atoms);
    }
  }

  return collect_prepared_interactions(sink);
}

core::index4 StackingInteractionCollector::collect(const core::data::structural::Chain &chain,
                                                                std::vector<ResiduePair_SP> &sink) {
  ring_atoms.clear();

  for(const Residue_SP ri:chain) {
      prepare_ring_atoms(*ri, ring_atoms);
  }

  return collect_prepared_interactions(sink);

}

void StackingInteractionCollector::add_ring_definition(const std::string &residue_code3,
                                                       const std::vector<std::string> &ring_atom_names) {
  if (ring_definitions_.find(residue_code3) == ring_definitions_.cend())
    ring_definitions_[residue_code3] = std::vector<std::vector<std::string>>{};
  ring_definitions_[residue_code3].push_back(ring_atom_names);
}

core::index4 StackingInteractionCollector::collect_prepared_interactions(std::vector<core::data::structural::ResiduePair_SP> &sink) {

  index4 cnt = 0;
  for (auto ring_a_it = ring_atoms.cbegin(); ring_a_it != ring_atoms.cend(); ++ring_a_it)
    for (auto ring_it_b = ring_atoms.cbegin(); ring_it_b != ring_a_it; ++ring_it_b) {
      auto ring_a = *ring_a_it;
      auto ring_b = *ring_it_b;

      if (ring_a[0]->owner() == ring_b[0]->owner()) continue;
      Vec3 cm_a;
      Vec3 cm_b;
      for (auto vi:(ring_a)) cm_a += *vi;
      cm_a /= ring_a.size();

      for (auto vi:(ring_b)) cm_b += *vi;
      cm_b /= ring_b.size();
      if (cm_a.distance_to(cm_b) <= MAX_DISTANCE) {
        cnt += 1;
        logs << utils::LogLevel::INFO << "stacking interaction detected between " << *((ring_a[0])->owner())
             << " and " << *((ring_b[0])->owner()) << "\n";

        sink.push_back(std::make_shared<StackingInteraction>(ring_a, ring_b));
      }
    }
  return cnt;
}

void StackingInteractionCollector::prepare_ring_atoms(const core::data::structural::Residue &ri,
            std::vector<std::vector<core::data::structural::PdbAtom_SP>> &ring_atoms) {

  // --- collect definitions of rings for this residue type
  const auto ring_defs_it = ring_definitions_.find(ri.residue_type().code3);
  if(ring_defs_it == ring_definitions_.end()) return;                            // ---this residue is not a recognized acceptor
  const std::vector<std::vector<std::string>> &ring_defs = ring_defs_it->second;

  for (const auto &ring_def: ring_defs) {
    std::vector<PdbAtom_SP> r_atoms;
    for (const auto &ring_atom: ring_def) {
      const PdbAtom_SP c = ri.find_atom(ring_atom, nullptr);   // --- the  atom directly behind the acceptor
      if (!test_atom(c, ring_atom, ri)) continue;
      r_atoms.push_back(c);
    }
    if (r_atoms.size()!=0) ring_atoms.push_back(r_atoms);
  }
}

void StackingInteractionCollector::rename_atoms_in_monomer_definition(const std::string& old_code3,
            const std::string& new_code3,std::map<std::string,std::string> names_dict) {

  const auto ring_defs_it = ring_definitions_.find(old_code3);
  if (ring_defs_it == ring_definitions_.end()) return;                            // ---this residue is not a recognized acceptor
  const std::vector<std::vector<std::string>> &ring_defs = ring_defs_it->second;
  std::vector<std::vector<std::string>> new_rings;
  for (const auto &ring_def: ring_defs) {
    std::vector<std::string> r_atoms;
    for (auto ring_atom: ring_def) {
      if (names_dict.find(utils::trim(ring_atom, " ")) == names_dict.end()) return;
      logs << utils::LogLevel::FINER << "Renaming atom " << ring_atom << " to " << names_dict[ring_atom] << "\n";

      r_atoms.push_back(core::data::structural::format_pdb_atom_name(names_dict[ring_atom]));
    }
    new_rings.push_back(r_atoms);
  }
  ring_definitions_[new_code3] = new_rings;
}

}
}
}
}

