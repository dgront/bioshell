#ifndef CORE_CALC_STRUCTURAL_StackingInteraction_HH
#define CORE_CALC_STRUCTURAL_StackingInteraction_HH

#include <vector>
#include <iostream>

#include <core/index.hh>
#include <core/data/structural/ResiduePair.hh>
#include <core/data/structural/PdbAtom.hh>

#include <core/calc/structural/angles.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

using namespace core::data::structural;

/** @brief Describes geometry of a stacking interaction.
 *
 * This class provides some geometric properties of a stacking interaction such as distances and angles between the two rings.
 * \todo_doc better description, add graphics that shows how the planar and the dihedral angles are defined
 */
class StackingInteraction: public ResiduePair {
public:

  friend std::ostream &operator<<(std::ostream &o, const StackingInteraction &hb);

  std::vector<PdbAtom_SP> ring_a; ///< Atom that comprises the first ring
  std::vector<PdbAtom_SP> ring_b; ///< Atom that comprises the second ring

  /** @brief Creates an object that describes a stacking interaction geometry.
   *
   * @param first_ring - vectors of atoms comprising the first aromatic ring
   * @param second_ring - vectors of atoms comprising the second aromatic ring
   */
  StackingInteraction(const std::vector<PdbAtom_SP> & first_ring, const std::vector<PdbAtom_SP> & second_ring);

  /// Returns the distance between the two rings centers along the Z axis
  inline double z() const { return z_; }

  /// Returns the distance between the two rings centers projected on the XY plane
  inline double xy() const { return xy_; }

  /// Returns the distance between the two rings
  inline double r() const { return sqrt(xy_*xy_+z_*z_); }

  /// Returns the planar angle between ring normals
  inline double angle() const { return a_; }

  static std::string output_header();

protected:
  void compute_geometry();

private:
  double z_;  ///< The distance between the two rings centers along the Z axis
  double xy_; ///< The distance between the two rings centers projected on the XY plane
  double a_;  ///< planar angle between ring normals
};

/// @brief Prints geometric parameters of a stacking interaction
std::ostream &operator<<(std::ostream &o, const StackingInteraction &hb);

typedef std::shared_ptr<StackingInteraction> StackingInteraction_SP;

}
}
}
}
#endif // ~CORE_CALC_STRUCTURAL_StackingInteraction_HH
