#ifndef CORE_CALC_STRUCTURAL_VdWInteraction_HH
#define CORE_CALC_STRUCTURAL_VdWInteraction_HH

#include <vector>
#include <iostream>

#include <core/index.hh>
#include <core/data/structural/ResiduePair.hh>
#include <core/data/structural/PdbAtom.hh>

#include <core/calc/structural/angles.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

using namespace core::data::structural;

/** @brief Describes geometry of a Van der Waals interaction.
 *
 * This class provides some geometric properties of a van der Waals interaction such as distances.
 */
class VdWInteraction: public ResiduePair {
public:

  friend std::ostream &operator<<(std::ostream &o, const VdWInteraction &hb);

  const PdbAtom_SP atom_a; ///< Atom that comprises the first residue
  const PdbAtom_SP atom_b; ///< Atom that comprises the second residue

  /** @brief Creates an object that describes a Van der Waals interaction geometry.
   *
   * @param first_atom - atom comprising the first residue
   * @param second_atom - atom comprising the second residue
   */
  VdWInteraction(const PdbAtom_SP & first_atom, const PdbAtom_SP & second_atom);

  /// Returns the distance between the two atoms 
  inline double d() const { return d_; }

  static std::string output_header();

protected:
  void compute_geometry();

private:
  double d_;  ///< The distance between the two atoms
  
};

/// @brief Prints geometric parameters of a Van der Waals interaction
std::ostream &operator<<(std::ostream &o, const VdWInteraction &hb);

typedef std::shared_ptr<VdWInteraction> VdWInteraction_SP;

}
}
}
}
#endif // ~CORE_CALC_STRUCTURAL_VdWInteraction_HH
