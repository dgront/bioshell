#include <memory>

#include <core/calc/structural/interactions/BackboneHBondInteraction.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/calc/structural/transformations/CartesianToSpherical.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

std::ostream &operator<<(std::ostream &o, const BackboneHBondInteraction &hb) {

  using core::calc::structural::to_degrees;
  const core::data::structural::Residue & d = *hb.first_residue();
  const core::data::structural::Residue & a = *hb.second_residue();
  o << utils::string_format("%4s %4d%c  %3s %4s -> %4s %3s %4d%c %4s : %5.3f %5.3f %6.2f %6.2f %7.2f %7.3f   %7.3f %7.3f %7.3f   %7.2f %7.2f",
                            d.owner()->id().c_str(), d.id(), d.icode(), d.residue_type().code3.c_str(), hb.hydrogen->atom_name().c_str(),
                            hb.acceptor_atom->atom_name().c_str(), a.residue_type().code3.c_str(), a.id(), a.icode(), a.owner()->id().c_str(),
                            hb.acceptor_atom->distance_to(*hb.hydrogen), hb.acceptor_atom->distance_to(*hb.donor_atom),
                            to_degrees(hb.planar()), to_degrees(hb.planar_behind()), to_degrees(hb.dihedral()), hb.dssp_energy(),
                            hb.local_H.x, hb.local_H.y, hb.local_H.z,
                            to_degrees(hb.theta()), to_degrees(hb.phi()));
  return o;
}


std::string BackboneHBondInteraction::output_header() {
  return "# donor residue   | acceptor residue  : d(AH) d(AD) a(DHA) a(HAB) t(bAHD)  energy   local-X local-Y local-Z     theta    phi";
}


void BackboneHBondInteraction::compute_geometry() {

  HydrogenBondInteraction::compute_geometry();

  // ---  reconstruct amide N of (i+1) residue it from amide geometry
  Vec3 ca_c(*behind_acceptor_atom);       // --- CA -> C versor
  ca_c -= *Ca;
  ca_c.norm();
  Vec3 o_c(*behind_acceptor_atom);        // --- O -> C versor
  o_c -= *acceptor_atom;
  o_c.norm();
  Vec3 N(o_c);
  N += ca_c;
  N.norm(1.33);
  N += *behind_acceptor_atom;

  core::calc::structural::transformations::Rototranslation_SP rt =
      core::calc::structural::transformations::local_coordinates_three_atoms(*Ca, *acceptor_atom, N);
  rt->apply(*hydrogen, local_H);

  core::calc::structural::transformations::CartesianToSpherical tr;
  tr.apply(local_H, tmp_sph);
  theta_ = tmp_sph.y;
  phi_ = tmp_sph.z;
}

const double BackboneHBondInteraction::DSSP_CONST = 0.42 * 0.2 * 332.0;

double BackboneHBondInteraction::dssp_energy() const {

  double e = 1.0 / donor_atom->distance_to(*acceptor_atom); // N-O
  e += 1.0 / hydrogen->distance_to(*behind_acceptor_atom); // C-H
  e -= 1.0 / hydrogen->distance_to(*acceptor_atom); // O-H
  e -= 1.0 / donor_atom->distance_to(*behind_acceptor_atom); // N-C

  return e * DSSP_CONST;
}

}
}
}
}
