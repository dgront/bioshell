#include <string>

#include <core/calc/structural/interactions/BackboneHBondInteraction.hh>
#include <core/calc/structural/interactions/BackboneHBondCollector.hh>
#include <core/calc/structural/interactions/HydrogenBondCollector.hh>
#include <core/calc/structural/angles.hh>
#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

using namespace core::data::structural;
utils::Logger llogs("BackboneHBondCollector");

void BackboneHBondCollector::prepare_acceptor_atoms(const Residue &ri,
        std::vector<std::pair<PdbAtom_SP, PdbAtom_SP>> & accptr_atoms) {

  const PdbAtom_SP O = ri.find_atom(" O  ", nullptr);
  if (!test_atom(O," O  ", ri)) return;
  const PdbAtom_SP dC = ri.find_atom(" C  ", nullptr);
  if (!test_atom(dC," C  ", ri)) return;
  const PdbAtom_SP CA = ri.find_atom(" CA ", nullptr);
  if (!test_atom(CA," CA ", ri)) return;
  accptr_atoms.emplace_back(O, dC);
}

void BackboneHBondCollector::prepare_donor_atoms(const Residue &ri,
          std::vector<std::pair<PdbAtom_SP, PdbAtom_SP>> & donor_atoms) {

    if (ri.residue_type() == core::chemical::Monomer::PRO) return; // --- PRO cannot be a H donor!
    const PdbAtom_SP N = ri.find_atom(" N  ", nullptr);
    if (!test_atom(N," N  ", ri)) return;

    PdbAtom_SP H = std::make_shared<PdbAtom>(0, " H  ", 1);
    const PdbAtom_SP hh = ri.find_atom(" H  ", nullptr);
    if(hh != nullptr) {
      H->set(*hh);
      H->id(hh->id());
    } else {
      Residue_SP prev = ri.previous();
      if (prev==nullptr) {
        logs << utils::LogLevel::FINE << "The residue " << ri
             << " is the first in chain. can't reconstruct its amide hydrogen!\n";
        return;
      }
      const PdbAtom_SP pC = prev->find_atom(" C  ", nullptr);
      if (pC == nullptr) {
        logs << utils::LogLevel::WARNING << "Skipping possible hydrogen bond due to missing atom C of residue "
             << ri.residue_type().code3 << "\n";
        return;
      }
      const PdbAtom_SP CA = ri.find_atom(" CA ", nullptr);
      if (CA == nullptr) {
        logs << utils::LogLevel::WARNING << "Skipping possible hydrogen bond due to missing atom CA of residue "
             << ri.residue_type().code3 << "\n";
        return;
      }

      H = peptide_hydrogen(*pC, *N, *CA);
    }
    donor_atoms.emplace_back(N,H);
}

BackboneHBondInteraction_SP  BackboneHBondCollector::create_bond(const PdbAtom_SP donating_n,
  const PdbAtom_SP the_h, const PdbAtom_SP acceptor_o, const PdbAtom_SP acceptor_c, const double dssp_energy_cutoff) {

  BackboneHBondInteraction_SP b = std::make_shared<BackboneHBondInteraction>(donating_n, the_h, acceptor_o, acceptor_c);

  if (b->dssp_energy() <= dssp_energy_cutoff) {
    llogs << utils::LogLevel::FINE << "hydrogen bond detected between " << *donating_n->owner()
         << " and " << *(acceptor_o->owner()) << "\n";
    return b;
  } else {
    llogs << utils::LogLevel::FINE <<
         utils::string_format("possible hydrogen bond has too high DSSP energy (%5.2f)",b->dssp_energy())
         << *donating_n->owner()
         << " and " << *(acceptor_o->owner()) << "\n";
    return nullptr;
  }
}

core::index4 BackboneHBondCollector::collect_prepared_interactions(std::vector<core::data::structural::ResiduePair_SP> &sink) {

  index4 cnt = 0;
  for(const std::pair<PdbAtom_SP, PdbAtom_SP> & donor:donor_atoms) {
    const PdbAtom_SP N = donor.first;
    const PdbAtom_SP H = donor.second;
    const Residue_SP donor_residue= N->owner();
    for(const std::pair<PdbAtom_SP, PdbAtom_SP> & acceptor:acceptor_atoms) {
      const PdbAtom_SP O = acceptor.first;
      const PdbAtom_SP C = acceptor.second;
      if (O->owner() == donor_residue) continue;
      if(H->distance_to(*O) > HydrogenBondCollector::MAX_AH_DISTANCE) continue;
      double angle = to_degrees(evaluate_planar_angle(*N, *H, *O));  // check D-H..A planar angle
      if (angle >= HydrogenBondCollector::MIN_AHD_ANGLE) {
        BackboneHBondInteraction_SP b = create_bond(N, H, O, C, dssp_energy_cutoff());
        if (b != nullptr) {
          sink.push_back(b);
          ++cnt;
        }
      }
    }
  }

  return cnt;
}

core::data::basic::Vec3 & peptide_hydrogen(const core::data::basic::Vec3 &C, const core::data::basic::Vec3 &N,
        const core::data::basic::Vec3 &CA, core::data::basic::Vec3 &H) {

  H.set(N);
  H -= C;
  H.norm();

  core::data::basic::Vec3 tmp2(N);
  tmp2 -= CA;
  tmp2.norm();

  H += tmp2;
  H.norm(HydrogenBondInteraction::N_H_BOND_LENGTH);
  H += N;

  return H;
}

core::data::basic::Vec3 & peptide_hydrogen(const core::data::structural::Residue_SP prev_residue,
        const core::data::structural::Residue_SP the_residue, core::data::basic::Vec3 &H) {

  return peptide_hydrogen(*prev_residue->find_atom_safe(" C  "), *the_residue->find_atom_safe(" N  "),
                          *the_residue->find_atom_safe(" CA "), H);
}

core::data::structural::PdbAtom_SP peptide_hydrogen(const core::data::structural::Residue_SP prev_residue,
      const core::data::structural::Residue_SP the_residue) {

  core::data::structural::PdbAtom_SP H = std::make_shared<core::data::structural::PdbAtom>(0, " H  ", 1);
  peptide_hydrogen(prev_residue, the_residue, *H);
  the_residue->push_back(H);

  return H;
}

PdbAtom_SP peptide_hydrogen(const PdbAtom &C, const PdbAtom &N, const PdbAtom &CA) {

  core::data::structural::PdbAtom_SP H = std::make_shared<core::data::structural::PdbAtom>(0, " H  ", 1);
  peptide_hydrogen(C, N, CA, *H);

  return H;
}

core::data::basic::Vec3 &peptide_hydrogen_dssp(const core::data::basic::Vec3 &C, const core::data::basic::Vec3 &O,
                                               const core::data::basic::Vec3 &N, core::data::basic::Vec3 &H) {

  H.set(N);

  core::data::basic::Vec3 tmp2(C);
  tmp2 -= O;
  tmp2.norm();
  tmp2 *= HydrogenBondInteraction::N_H_BOND_LENGTH;
  H += tmp2;

  return H;
}

core::data::basic::Vec3 & peptide_hydrogen_dssp(const core::data::structural::Residue_SP prev_residue, 
      const core::data::structural::Residue_SP the_residue, core::data::basic::Vec3 &H) {

  return peptide_hydrogen_dssp(*prev_residue->find_atom_safe(" C  "), *prev_residue->find_atom_safe(" O  "),
                               *the_residue->find_atom_safe(" N  "), H);
}

core::data::structural::PdbAtom_SP peptide_hydrogen_dssp(const core::data::structural::Residue_SP prev_residue,
        const core::data::structural::Residue_SP the_residue) {

  core::data::structural::PdbAtom_SP H = std::make_shared<core::data::structural::PdbAtom>(0, " H  ", 1);
  peptide_hydrogen_dssp(prev_residue, the_residue, *H);
  the_residue->push_back(H);

  return H;
}

}
}
}
}

