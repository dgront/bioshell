/** @file BackboneHBondMap.hh
 * @brief Provides a map of main chain hydrogen bonds calculated from a protein structure
 */
#ifndef CORE_CALC_STRUCTURAL_BackboneHBondMap_HH
#define CORE_CALC_STRUCTURAL_BackboneHBondMap_HH

#include <core/data/basic/SparseMap2D.hh>
#include <core/calc/structural/interactions/PairwiseResidueMap.hh>
#include <core/calc/structural/interactions/BackboneHBondInteraction.hh>
#include <core/calc/structural/interactions/BackboneHBondCollector.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

using namespace core::data::structural;

/** \brief Creates a map of all main chain hydrogen bonds within a protein structure
 *
 * Hydrogen bond between different protein chains are properly handled.
 * @see ProteinArchitecture for example application of a hydrogen bonds map
 *
 * The following example program reads a PDB structure (all backbone atoms required) and prints secondary structure assigned
 * according to the DSSP definition
 * \include ap_dssp.cc
 *
 * Another example lists all hydrogen bonds found in a protein structure:
 * \include ap_BackboneHBondMap.cc
 */
class BackboneHBondMap : private PairwiseResidueMap<BackboneHBondInteraction_SP> {
public:

  /** @brief Extract all main chain hydrogen bonds from a given structure and store them in a map.
   *
   * @param structure - an input protein structure
   * @param cutoff - DSSP energy cutoff; hydrogen bond whose DSSP energy is higher that the cutoff will not be inserted into this map
   */
  BackboneHBondMap(const Structure &structure, const double cutoff = -0.4) :
        PairwiseResidueMap(structure, selectors::IsAA()) {

    BackboneHBondCollector collect_hbonds;

    std::vector<ResiduePair_SP> hbonds;
    collect_hbonds.dssp_energy_cutoff(cutoff);
    collect_hbonds.collect(structure, hbonds);

    for (const ResiduePair_SP ri: hbonds) {
      add(std::static_pointer_cast<BackboneHBondInteraction>(ri));
    }
  }

  /// begin() const-iterator for hydrogen bonds
  inline SparseMap2D<core::index2, BackboneHBondInteraction_SP>::const_iterator cbegin() const {
    return PairwiseResidueMap<BackboneHBondInteraction_SP>::cbegin();
  };

  /// Returns true if a given residue donates its amide hydrogen to form a hydrogen bond
  inline bool donates_hydrogen(const core::index2 donor_res_index) const {
    return PairwiseResidueMap<BackboneHBondInteraction_SP>::has_row(donor_res_index);
  }

  /// end() const-iterator for hydrogen bonds
  inline SparseMap2D<core::index2, BackboneHBondInteraction_SP>::const_iterator cend() const {
    return PairwiseResidueMap<BackboneHBondInteraction_SP>::cend();
  };

  /// begin() const-iterator for hydrogen bonds where hydrogen atom is donated by a given residue
  inline typename std::map<core::index2, BackboneHBondInteraction_SP>::const_iterator
  cbegin(const core::index2 donor_res_index) const {
    return PairwiseResidueMap<BackboneHBondInteraction_SP>::cbegin(donor_res_index);
  };

  /// end() const-iterator for hydrogen bonds where hydrogen atom is donated by a given residue
  inline typename std::map<core::index2, BackboneHBondInteraction_SP>::const_iterator
  cend(const core::index2 donor_res_index) const {
    return PairwiseResidueMap<BackboneHBondInteraction_SP>::cend(donor_res_index);
  };

  /** @brief Returns an integer index corresponding to a residue.
   *
   * ResidueIndexer::at() is called internally by this method
   * @param r - a residue covered by this map
   * @return integer index corresponding to a given residue. The index may be used in calling other methods of this class;
   * it's also useful e.g. to store H-bonds in a vector or to hash them
   */
  core::index2 residue_index(const Residue &r) { return ResidueIndexer::at(r); }

  /** @brief Returns a residue corresponding to an integer index.
   *
   * This method is the opposite to <code>residue_index(const Residue &)</code>;  ResidueIndexer::residue() is called
   * internally for this purpose.
   *
   * @param index - an integer index of a residue covered by this map
   * @return a pointer to a residue.
   */
  const Residue_SP residue(core::index2 index) { return ResidueIndexer::residue(index);}

  /// Returns the number of residues in the structure.
  core::index2 length() const { return max_index() + 1; }

  /// Counts hydrogen bonds found in the structure.
  core::index2 count_bonds() const { return PairwiseResidueMap<BackboneHBondInteraction_SP>::size(); }

  /** @brief Provides a list of residue pairs that form a hydrogen bond
   *
   * This method creates a list of pairs to hold donor,acceptor residue pairs; i.e. the returned list is not symmetric
   * @return a list of indexes where one can find in this map a hydrogen bond object
   */
  std::vector<std::pair<core::index2,core::index2>> list_bonds() {
    std::vector<std::pair<core::index2,core::index2>> lista;
    SparseMap2D::nonempty_indexes(lista);
    return lista;
  }

  /** @brief Inserts a hydrogen bond into this map.
   *
   * After this call, hbond->donor_residue will be considered as hydrogen bonded to hbond->acceptor_residue
   * @param hbond - a main chain hydrogen bond geometry object
   */
  void add(const BackboneHBondInteraction_SP &hbond) {
    PairwiseResidueMap<BackboneHBondInteraction_SP>::add(*hbond->donor_residue(), *hbond->acceptor_residue(), hbond);
  }

  /// Returns true if this map has recorded a hydrogen bond between a given pair of residues
  bool has_element(const core::index2 row, const core::index2 column) const {

    return SparseMap2D<core::index2,BackboneHBondInteraction_SP>::has_element(row, column);
  }

  /** \brief Returns a hydrogen bond between two residues.
   *
   * If the two residues are connected with a hydroned bond, this method returns null value.
   * <strong>Note</strong> that this method is not symmetric and allows access a hydrogen where  <code>donor_residue</code>
   * is the donor and <code>acceptor_residue</code> - the acceptor
   * @param donor_residue - the residue that shares its amide proton in a hydrogen bond
   * @param acceptor_residue - the residue that accepts the proton
   * @return hydrogen bond object or null pointer
   */
  const BackboneHBondInteraction_SP at(const Residue &donor_residue, const Residue &acceptor_residue) const {
    return PairwiseResidueMap<BackboneHBondInteraction_SP>::at(donor_residue, acceptor_residue, nullptr);
  }

  /** \brief Returns a hydrogen bond between two residues.
   *
   * If the two residues are not bonded, this method returns null value
   * @param donor_res_index - zero-referenced index of the residue that shares its amide proton in a hydrogen bond
   * @param acc_res_index - zero-referenced index of the residue that accepts the proton
   * @return hydrogen bond object or null pointer
   */
  const BackboneHBondInteraction_SP at(const core::index2 donor_res_index, const core::index2 acc_res_index) const {
    return PairwiseResidueMap<BackboneHBondInteraction_SP>::at(donor_res_index, acc_res_index, nullptr);
  }

  /** \brief Returns true if a given donor_residue donates its proton to acc_residue and forms hydrogen bond.
   *
   * If the two residues are bonded in such a way that donor_residue is an acceptor, this method returns false
   * @param donor_residue - donor of a hydrogen atom
   * @param acc_residue - acceptor of a hydrogen atom
   * @return true if the two residues are bonded (in the proper arrangement)
   */
  inline bool are_H_bonded(const core::index2 donor_residue, const core::index2 acc_residue) const {

    return has_element(donor_residue, acc_residue);
  }

  /** \brief Returns true if a given donor_residue donates its proton to acc_residue and forms hydrogen bond.
   *
   * If the two residues are bonded in such a way that donor_residue is an acceptor, this method returns false
   * @param donor_residue - donor of a hydrogen atom
   * @param acc_residue - acceptor of a hydrogen atom
   * @return true if the two residues are bonded (in the proper arrangement)
   */
  inline bool are_H_bonded(const Residue &donor_residue, const Residue &acceptor_residue) const {

    return has_element(ResidueIndexer::at(donor_residue), ResidueIndexer::at(acceptor_residue));
  }

  /** \brief Removes a hydrogen bond.
   * @param donor_res_index - residue that shares its amide proton in a hydrogen bond
   * @param acc_res_index - residue that accepts the proton
   */
  void remove(const core::index2 donor_res_index, const core::index2 acc_res_index) {
    erase(donor_res_index, acc_res_index);
  }

  /** \brief Checks if <code>which_residue</code> accepts a proton in a N-turn hydrogen bonds.
   *
   * In such a case the hydrogen bond is formed with residue <code>which_residue</code> + N.
   * Proper values of N are:
   *   - N = 3 means a part of 3<sub>10</sub> helix
   *   - N = 4 means a part of \f$ \alpha \f$ helix
   *   - N = 5 means a part of \f$ \pi \f$ helix
   *
   * @param which_residue - the residue of interest
   * @param N - turn type; should be 3, 4 or 5
   * @return true if <code>whichResidue</code> forms an N-turn hydrogen bond
   */
  inline bool accepts_N_turn(const core::index2 which_residue, const core::index1 n) const {

    return are_H_bonded(which_residue + n, which_residue);
  }

  /** \brief Checks if <code>which_residue</code> donates a proton in a N-turn hydrogen bonds.
 *
 * In such a case the hydrogen bond is formed with residue <code>which_residue</code> - N.
 * Proper values of N are:
 *   - N = 3 means a part of 3<sub>10</sub> helix
 *   - N = 4 means a part of \f$ \alpha \f$ helix
 *   - N = 5 means a part of \f$ \pi \f$ helix
 *
 * @param which_residue - index of the residue of interest
 * @param N - turn type; should be 3, 4 or 5
 * @return true if <code>which_residue</code> forms an N-turn hydrogen bond
 */
  inline bool donates_N_turn(const core::index2 which_residue, const core::index1 n) const {

    return are_H_bonded(which_residue, which_residue - n);
  }

  /** \brief Returns <code>true</code> if i_residue and j_residue form a parallel beta bridge.
   *
   * Order of residues in unimportant in this case.
   * @param i_residue - index of the first residue of interest
   * @param j_residue - index of the second residue of interest
   * @return <code>true</code> if <code>i_residue</code> <code>j_residue</code> form a parallel beta bridge.
   */
  inline bool is_parallel_bridge(const core::index2 i_residue, const core::index2 j_residue) const {
    return ((are_H_bonded(j_residue, i_residue - 1) && (are_H_bonded(i_residue + 1, j_residue))) ||
            (are_H_bonded(i_residue, j_residue - 1) && are_H_bonded(j_residue + 1, i_residue)));
  }

  /** \brief Returns <code>true</code> if i_residue and j_residue form an antiparallel beta bridge.
   *
   * Order of residues in unimportant in this case.
   * @param i_residue - the first residue of interest
   * @param j_residue - the second residue of interest
   * @return <code>true</code> if <code>i_residue</code> <code>j_residue</code> form an antiparallel beta bridge.
   */
  inline bool is_antiparallel_bridge(const core::index2 i_residue, const core::index2 j_residue) const {

    return ((are_H_bonded(j_residue, i_residue) && (are_H_bonded(i_residue, j_residue))) ||
            (are_H_bonded(j_residue + 1, i_residue - 1) && are_H_bonded(i_residue + 1, j_residue - 1)));
  }
};

}
}
}
}

#endif
/**
 * @example ap_dssp.cc
 * @example ap_BackboneHBondMap.cc
 */
