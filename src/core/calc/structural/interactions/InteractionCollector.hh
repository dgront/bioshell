/** \file InteractionCollector.hh
 * @brief Defines an abstract collector of interactions.
 */
#ifndef CORE_CALC_STRUCTURAL_INTERACTIONS_InteractionCollector_HH
#define CORE_CALC_STRUCTURAL_INTERACTIONS_InteractionCollector_HH

#include <string>

#include <core/data/structural/Chain.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Structure.hh>

#include <core/calc/structural/interactions/ResidueInteraction.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

/** \brief Defines an abstract collector of interactions.
 */
class InteractionCollector {
public:

  /** @brief Collects all hydrogen bond interactions that can be detected within a given Structure object.
   *
   * Pointers to newly created Interaction instances will be appended to the given vector. The vector is not cleared
   * by this call, so its content will not be lost
   * @param strctr - a structure where hydrogen bond interactions will be detected
   * @param sink - destinations, where the interactions will be stored
   * @return number of new interactions created
   */
  virtual core::index4 collect_interactions(const core::data::structural::Structure &strctr,
                                            std::vector<ResidueInteraction_SP> &sink) = 0;

  virtual core::index4 collect_interactions(const core::data::structural::Chain &chain,
                                            std::vector<ResidueInteraction_SP> &sink) = 0;

  virtual core::index4 collect_interactions(const core::data::structural::Residue &i_residue,
                                            const core::data::structural::Residue &j_residue,
                                            std::vector<ResidueInteraction_SP> &sink) = 0;

  /// Default virtual destructor
  virtual ~InteractionCollector() {}
};

}
}
}
}


#endif

