/** @file BackboneHBondInteraction.hh
 *  @brief Describes a hydrogen bond located in a protein backbone
 *
 *  This file provides also methods to reconstruct amide hydrogen atom based on local geometry of a peptide plate.
 */
#ifndef CORE_CALC_STRUCTURAL_BackboneHBondInteraction_HH
#define CORE_CALC_STRUCTURAL_BackboneHBondInteraction_HH

#include <vector>
#include <memory>
#include <iostream>

#include <core/index.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>

#include <core/calc/structural/angles.hh>
#include <core/calc/structural/interactions/PairwiseResidueMap.hh>
#include <core/calc/structural/interactions/HydrogenBondInteraction.hh>
#include <core/calc/structural/transformations/transformation_utils.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

using namespace core::data::structural;

/** @brief Describes geometry of a hydrogen bond formed between two peptide plates.
 *
 * The geometry description inherits all the features of HydrogenBondGeometry, and also provides
 * local coordinates of a hydrogen atoms both in Cartesian and spherical coordinate systems.
 *
 * The following demo reads a PDB file and print all backbone hydrogen bonds:
 * \include ex_BackboneHBondMap.cc
 */
class BackboneHBondInteraction : public HydrogenBondInteraction {
public:

  friend std::ostream &operator<<(std::ostream &o, const BackboneHBondInteraction &hb);

  /** @brief Creates an object based on a donor and an acceptor residues.
   *
   * Neighbor residues are also necessary to reconstruct amide hydrogen and properly define local coordinate system.
   * @param prev_donor - a residue directly preceding the donor residue;
   *        must be provided in order to compute amide hydrogen coordinates
   * @param donor - a donor residue i.e. the one whose amide hydrogen is attracted by an acceptor
   * @param acceptor - an acceptor residue i.e. the one whose carbonyl group attracts a hydrogen atom
   * @param past_acceptor - a residue directly following the acceptor residue;
   *    must be provided to define a local coordinate system on a carbonyl oxygen
   */
  BackboneHBondInteraction(const PdbAtom_SP donating_n, const PdbAtom_SP the_h, const PdbAtom_SP acceptor_o,
      const PdbAtom_SP acceptor_c) : HydrogenBondInteraction(donating_n, the_h, acceptor_o, acceptor_c) {
    Ca = acceptor_o->owner()->find_atom_safe(" CA ");
    compute_geometry();
  }

  /// Returns the (donor - acceptor) difference in residue numbers along the protein chain sequence.
  short int bond_offset() const { return donor_residue()->id() - acceptor_residue()->id(); }

  /** @brief returns coordinates of the amide hydrogen atoms in the local coordinate system (LCS).
   *
   * The LCS at residue \f$i\f$ is based on three atoms: \f$C\alpha_i\f$, \f$C_i\f$ and \f$N_{i+1}\f$. It is computed by
   * core::calc::structural::transformations::local_coordinates_three_atoms() method.
   */
  const Vec3 &local_H_coordinates() const { return local_H; }

  /// Returns the first spherical angle (the second spherical coordinate) of an amide hydrogen atom
  double theta() const { return theta_; }

  /// Returns the second spherical angle (the third spherical coordinate) of an amide hydrogen atom
  double phi() const { return phi_; }

  /** @brief Calculates the DSSP energy for a bond.
   *
   * DSSP energy was defined by Kabsch and Sander by the formula:
   * \f[
   * E = 0.084 \left\{ \frac{1}{r_{ON}} +  \frac{1}{r_{CH}} - \frac{1}{r_{OH}} - \frac{1}{r_{CN}} \right\} \cdot 332 \, \mathrm{kcal/mol}
   * \f]
   * @see Kabsch W, Sander C (1983). "Dictionary of protein secondary structure: pattern recognition of hydrogen-bonded and geometrical features". Biopolymers 22 (12): 2577–637
   */
  double dssp_energy() const;

  /** @brief Returns a string that is a nicely formatted header for data printed by ostream operator.
   * Use this string as a header for a nicely formatted table of hydrogen bonds detected by this class.
   * Note, that output produced by HydrogenBondInteraction is a bit different ant that class provides its own header line
   * @return a table header as a string
   */
  static std::string output_header();

protected:
  void compute_geometry();

private:
  double theta_; ///< The second spherical coordinate of H atom
  double phi_; ///< The third spherical coordinate of H atom
  PdbAtom_SP Ca; ///< Alpha carbon bonded to the acceptor carbonyl group; i.e. the alpha carbon of the acceptor's residue
  Vec3 local_H;
  const static double DSSP_CONST;
  data::basic::Vec3 tmp_sph;
};

typedef std::shared_ptr<BackboneHBondInteraction> BackboneHBondInteraction_SP;

/** @brief Prints geometric parameters of a hydrogen bond.
 * @param o - output stream object
 * @param hb - a backbone hydrogen bond object
 * @return the given stream reference
 */
std::ostream &operator<<(std::ostream &o, const BackboneHBondInteraction &hb);

}
}
}
}
#endif
/**
 * \example ex_peptide_hydrogen.cc
 */

