/** \file StackingInteraction.hh
 * @brief Defines a collector of stacking interactions.
 */
#ifndef CORE_CALC_STRUCTURAL_INTERACTIONS_VdWInteractionCollector_HH
#define CORE_CALC_STRUCTURAL_INTERACTIONS_VdWInteractionCollector_HH

#include <map>

#include <core/data/structural/ResiduePairCollector.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

using namespace core::data::structural;

class VdWInteractionCollector : public core::data::structural::ResiduePairCollector {
public:
  /// Define the maximum allowed distance between the van der Waals spheres of interacting atom
  static constexpr const double MAX_DISTANCE = 0.5;

  /** @brief Create an object that collects van der Waals interactions
   */
  explicit VdWInteractionCollector(bool include_protein_backbone=false);

  /// Default virtual destructor
  virtual ~VdWInteractionCollector() = default;

  /** @brief Collects interactions based on all atoms in two residues.
   *
   * Goes through each residue in double nested loop and detects possible interactions.
   * Once a van der Waals interaction is detected, a respective object will be created and pushed back to a given sink vector
   * @param sink - a vector to store interaction objects (VdWInteraction instances)
   * @return the number of VdWCollector objects created
   */

  virtual core::index4 collect(const core::data::structural::Residue &i_residue,
                               const core::data::structural::Residue &j_residue,
                               std::vector<ResiduePair_SP> &sink);

  /** @brief Collects interactions based on atoms in a given structure.
   *
   * Goes through each atom in a structure in double nested loop and detects possible interactions.
   * Once a van der Waals interaction is detected, a respective object will be created and pushed back to a given sink vector
   * @param sink - a vector to store interaction objects (VdWInteraction instances)
   * @return the number of VdWCollector objects created
   */
  virtual core::index4 collect(const core::data::structural::Structure &strctr,
                               std::vector<ResiduePair_SP> &sink);

  /** @brief Collects interactions based on atoms in a given chain.
   *
   * Goes through each atom in the chain in double nested loop and detects possible interactions.
   * Once a van der Waals interaction is detected, a respective object will be created and pushed back to a given sink vector
   * @param sink - a vector to store interaction objects (VdWInteraction instances)
   * @return the number of VdWCollector objects created
   */
  virtual core::index4 collect(const core::data::structural::Chain &chain,
                               std::vector<ResiduePair_SP> &sink);

protected:
  bool calculate_vdw_distance(PdbAtom_SP atom_a,PdbAtom_SP atom_b,std::vector<ResiduePair_SP> &sink);

private:
  utils::Logger logs;
  double cutoff2_;
};

}
}
}
}

#endif

