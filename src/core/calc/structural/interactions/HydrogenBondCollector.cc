#include <string>
#include <map>
#include <utility>

#include <core/chemical/Monomer.hh>
#include <core/calc/structural/angles.hh>
#include <core/calc/structural/interactions/HydrogenBondCollector.hh>
#include <core/calc/structural/interactions/HydrogenBondInteraction.hh>
#include <core/calc/structural/interactions/BackboneHBondInteraction.hh>
#include <core/calc/structural/interactions/BackboneHBondCollector.hh>
#include <core/chemical/MonomerStructureFactory.hh>

namespace core {
namespace calc {
namespace structural {
namespace interactions {

using namespace core::data::structural;
using namespace core::chemical;

HydrogenBondCollector::HydrogenBondCollector(bool include_protein_backbone) : logs("HydrogenBondCollector") {

  include_protein_backbone_ = include_protein_backbone;
  MonomerStructureFactory factory = MonomerStructureFactory::get_instance();
  for (auto monomer_it = factory.cbegin(); monomer_it != factory.cend(); ++monomer_it) {
    auto mnmr = (*monomer_it).second;
    std::vector<std::pair<std::string, std::string>> v_donors;
    std::vector<std::pair<std::string, std::string>> v_acceptors;

    for (auto hydrogen:mnmr->polar_hydrogens()) {
      auto atm2_index = mnmr->get_adjacent(hydrogen->id() - 1)[0];
      v_donors.emplace_back(mnmr->get_atom(atm2_index)->atom_name(), hydrogen->atom_name());
    }
    for (auto accp:mnmr->hydrogen_acceptors())
      for (auto atm2_index:mnmr->get_adjacent(accp->id() - 1))
        if (mnmr->get_atom(atm2_index)->element_index() != 1) {
          v_acceptors.emplace_back(accp->atom_name(), mnmr->get_atom(atm2_index)->atom_name());
          break;
        }
    donors_[mnmr->code3] = v_donors;
    acceptors_[mnmr->code3] = v_acceptors;
  }
}

bool HydrogenBondCollector::test_atom(const PdbAtom_SP a, const std::string &atom_name, const Residue &res) {

  if (a == nullptr) {
    logs << utils::LogLevel::WARNING << "Skipping possible hydrogen bond due to missing atom " << atom_name
         << " of residue " << res << "\n";
    return false;
  }

  return true;
}

core::index4 HydrogenBondCollector::collect(const core::data::structural::Residue &i_residue,
                                            const core::data::structural::Residue &j_residue,
                                            std::vector<ResiduePair_SP> & sink) {

  donor_atoms.clear();
  acceptor_atoms.clear();
  prepare_donor_atoms(i_residue, donor_atoms);
  prepare_acceptor_atoms(j_residue, acceptor_atoms);
  if(donor_atoms.size()==0 || acceptor_atoms.size() == 0)
    return 0;

  return collect_prepared_interactions(sink);
}

core::index4 HydrogenBondCollector::collect(const Chain &chain,
                                            std::vector<ResiduePair_SP> &sink) {

  donor_atoms.clear();
  acceptor_atoms.clear();
  for(const Residue_SP ri:chain) {
    prepare_donor_atoms(*ri, donor_atoms);
    prepare_acceptor_atoms(*ri, acceptor_atoms);
  }

  return collect_prepared_interactions(sink);
}

core::index4 HydrogenBondCollector::collect(const Structure &strctr,
                                            std::vector<ResiduePair_SP> &sink) {

  donor_atoms.clear();
  acceptor_atoms.clear();
  for(const Chain_SP ci:strctr) {
    for(auto r_it = ci->cbegin(); r_it != ci->cend(); ++r_it) {
      const Residue & ri = **r_it;
      prepare_donor_atoms(ri, donor_atoms);
      prepare_acceptor_atoms(ri, acceptor_atoms);
    }
  }

  return collect_prepared_interactions(sink);
}

core::index4 HydrogenBondCollector::collect_prepared_interactions(std::vector<ResiduePair_SP> &sink) {

  index4 cnt = 0;
  for (const std::pair<PdbAtom_SP, PdbAtom_SP> &donor:donor_atoms) {
    const PdbAtom_SP N = donor.first;
    const PdbAtom_SP H = donor.second;
    const Residue_SP donor_residue= N->owner();
    for (const std::pair<PdbAtom_SP, PdbAtom_SP> &acceptor:acceptor_atoms) {
      const PdbAtom_SP O = acceptor.first;
      const PdbAtom_SP C = acceptor.second;
      if (O->owner() == donor_residue) continue;
      if (H->distance_to(*O) > HydrogenBondCollector::MAX_AH_DISTANCE) continue;
      double angle = to_degrees(evaluate_planar_angle(*H, *O, *C));  // check P-A..H planar angle
      if (angle < HydrogenBondCollector::MIN_PAH_ANGLE) continue;
      angle = to_degrees(evaluate_planar_angle(*N, *H, *O));  // check D-H..A planar angle
      if (angle < HydrogenBondCollector::MIN_AHD_ANGLE) continue;

      if ((H->atom_name() == " H  ") && (N->atom_name() == " N  ") &&
          (C->atom_name() == " C  ") && (O->atom_name() == " O  ")) {
        if (include_protein_backbone_) {
          BackboneHBondInteraction_SP b = BackboneHBondCollector::create_bond(N, H, O, C, -0.4);
          if (b != nullptr) {
            sink.push_back(b);
            ++cnt;
          }
        }
      } else {
        logs << utils::LogLevel::FINE << "hydrogen bond detected between " << *donor_residue
             << " and " << *(O->owner()) << "\n";
        sink.push_back(std::make_shared<HydrogenBondInteraction>(N, H, O, C));
        ++cnt;
      }
    }
  }

  return cnt;
}

void HydrogenBondCollector::prepare_acceptor_atoms(const Residue &ri,
          std::vector<std::pair<PdbAtom_SP, PdbAtom_SP>> & accptr_atoms) {

  // --- collect definitions of acceptors for this residue type
  const auto accptrs_it = acceptors_.find(ri.residue_type().code3);
  if(accptrs_it == acceptors_.end()) return;                            // ---this residue is not a recognized acceptor
  const std::vector<std::pair<std::string, std::string>> &accptrs = accptrs_it->second;

  for (const auto &acceptor_pair: accptrs) {                            // --- iterate over all acceptors of this residue
    const PdbAtom_SP c = ri.find_atom(acceptor_pair.second, nullptr);   // --- the  atom directly behind the acceptor
    if(! test_atom(c, acceptor_pair.second, ri)) continue;
    const PdbAtom_SP a = ri.find_atom(acceptor_pair.first, nullptr);    // --- the acceptor of the hydrogen
    if(! test_atom(a, acceptor_pair.first, ri)) continue;
    accptr_atoms.emplace_back(a,c);
  }
}

void HydrogenBondCollector::prepare_donor_atoms(const Residue &ri,
          std::vector<std::pair<PdbAtom_SP, PdbAtom_SP>> & donor_atoms) {

  auto donors_it = donors_.find(ri.residue_type().code3);
  if (donors_it == donors_.end()) return;       // ---this residue is not a recognized donor
  const std::vector<std::pair<std::string, std::string>> &donors = donors_it->second;

  for (const auto &donor_pair: donors) {          // --- iterate over all donors of this residue
    const PdbAtom_SP h = ri.find_atom(donor_pair.second, nullptr);   // the hydrogen atom
    if(! test_atom(h, donor_pair.second, ri)) continue;
    const PdbAtom_SP d = ri.find_atom(donor_pair.first, nullptr);    // the donor of the hydrogen
    if(! test_atom(d, donor_pair.first, ri)) continue;
    donor_atoms.emplace_back(d,h);
  }
}

void HydrogenBondCollector::add_donor_definition(const std::string &residue_code3,
                const std::string &donor_atom_name, const std::string &hydrogen_atom_name) {

  if (donors_.find(residue_code3) == donors_.end())
    donors_[residue_code3] = std::vector<std::pair<std::string,std::string>>{};
  std::pair<std::string,std::string> p {donor_atom_name, hydrogen_atom_name};
  donors_[residue_code3].push_back(p);
}

void HydrogenBondCollector::add_acceptor_definition(const std::string &residue_code3,
                                                    const std::string &accetor_atom_name,
                                                    const std::string &preceding_atom_name) {

  if (acceptors_.find(residue_code3) == acceptors_.end())
    acceptors_[residue_code3] = std::vector<std::pair<std::string,std::string>>{};
  std::pair<std::string, std::string> p{accetor_atom_name, preceding_atom_name};
  acceptors_[residue_code3].push_back(p);
}

void HydrogenBondCollector::rename_atoms_in_monomer_definition(const std::string &old_code3, const std::string &new_code3,
                                                          std::map<std::string, std::string> names_dict) {
  const auto donors_it = donors_.find(old_code3);
  if (donors_it == donors_.end()) return;
  const std::vector<std::pair<std::string, std::string>> &old_donors = donors_it->second;

  std::vector<std::pair<std::string, std::string>> new_donors;
  for (auto atom_pair : old_donors) {

    logs << utils::LogLevel::FINER << "Renaming atom " << atom_pair.first << " to "
         << names_dict[utils::trim(atom_pair.first)] << "\n";
    logs << utils::LogLevel::FINER << "Renaming atom " << atom_pair.second << " to "
         << names_dict[utils::trim(atom_pair.second)] << "\n";

    auto atom1 = core::data::structural::format_pdb_atom_name(names_dict[utils::trim(atom_pair.first)]);
    auto atom2 = core::data::structural::format_pdb_atom_name(names_dict[utils::trim(atom_pair.second)]);

    std::pair<std::string, std::string> p{atom1, atom2};
    new_donors.push_back(p);
  }
  donors_[new_code3] = new_donors;

  const auto acceptor_it = acceptors_.find(old_code3);
  if (acceptor_it == acceptors_.end()) return;
  const std::vector<std::pair<std::string, std::string>> &old_acceptors = acceptor_it->second;

  std::vector<std::pair<std::string, std::string>> new_acceptors;
  for (auto atom_pair : old_acceptors) {

    logs << utils::LogLevel::FINER << "Renaming atom " << atom_pair.first << " to "
         << names_dict[utils::trim(atom_pair.first)] << "\n";
    logs << utils::LogLevel::FINER << "Renaming atom " << atom_pair.second << " to "
         << names_dict[utils::trim(atom_pair.second)] << "\n";

    auto atom1 = core::data::structural::format_pdb_atom_name(names_dict[utils::trim(atom_pair.first)]);
    auto atom2 = core::data::structural::format_pdb_atom_name(names_dict[utils::trim(atom_pair.second)]);

    std::pair<std::string, std::string> p{atom1, atom2};
    new_acceptors.push_back(p);
  }
  acceptors_[new_code3] = new_acceptors;
}

}
}
}
}

