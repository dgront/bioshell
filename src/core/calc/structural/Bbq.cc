#include <core/data/basic/Vec3.hh>
#include <core/calc/structural/Bbq.hh>
#include <core/calc/structural/angles.hh>
#include <core/chemical/Monomer.hh>
#include <core/BioShellEnvironment.hh>
#include <core/calc/structural/protein_angles.hh>


namespace core {
namespace calc {
namespace structural {

using namespace core::data::structural;
using namespace core::calc::structural::transformations;

Bbq::Bbq() : logs("Bbq") {

  lambda_rotation = std::make_shared<transformations::Rototranslation>();

  // --- Load ideal peptide plates
  load_atom_from_pdb(core::BioShellEnvironment::from_file_or_db("bbq/bb-P_no_CB.pdb"), P);
  load_atom_from_pdb(core::BioShellEnvironment::from_file_or_db("bbq/bbH-P.pdb"), PH);
  load_atom_from_pdb(core::BioShellEnvironment::from_file_or_db("bbq/bb_no_CB.pdb"), A);
  load_atom_from_pdb(core::BioShellEnvironment::from_file_or_db("bbq/bbH.pdb"), AH);
}

/// Reset a given rotation so it rotates around X axis by a given angle
void set_rotation_by_lambda(const double lambda_angle, transformations::Rototranslation_SP lambda_rotation) {

  double c = cos(lambda_angle);
  double s = sin(lambda_angle);
  lambda_rotation->rot_x(1, 0, 0);
  lambda_rotation->rot_y(0, c, -s);
  lambda_rotation->rot_z(0, s, c);
}

/// Helper function that creates a dummy atom at N termini
data::structural::PdbAtom_SP
dummy_ca_N(const data::basic::Vec3 &ca0, const data::basic::Vec3 &ca1, const data::basic::Vec3 &ca2) {

  data::structural::PdbAtom_SP out = std::make_shared<data::structural::PdbAtom>(1, " CA ", ca1.x, ca1.y, ca2.z);
  (*out) -= ca2;
  (*out) += ca0;

  return out;
}

/// Helper function that creates a dummy atom at C termini
data::structural::PdbAtom_SP
dummy_ca_C(const data::basic::Vec3 &ca_2, const data::basic::Vec3 &ca_1, const data::basic::Vec3 &ca) {

  data::structural::PdbAtom_SP out = std::make_shared<data::structural::PdbAtom>(1, " CA ", ca_1.x, ca_1.y, ca_1.z);
  (*out) -= ca_2;
  (*out) += ca;

  return out;
}

/// Helper function that creates the OXT atom (a quite crude approximation)
data::structural::PdbAtom_SP
create_OXT(const data::basic::Vec3 &CA, const data::basic::Vec3 &C, const data::basic::Vec3 &O) {

  data::basic::Vec3 ca_c(C);
  ca_c -= CA;
  data::basic::Vec3 c_o(O);
  c_o -= C;
  double d = c_o.dot_product(ca_c) / sqrt(ca_c.length() * c_o.length());
  d *= ca_c.length();
  ca_c.norm(d);
  ca_c += C;
  ca_c -= O;
  ca_c *= 2.0;
  ca_c += O;

  return std::make_shared<data::structural::PdbAtom>(1, " OXT", ca_c.x, ca_c.y, ca_c.z, 1.0, 0.0,
      core::chemical::AtomicElement::OXYGEN.z);
}

PdbAtom_SP Bbq::transform_atom(PdbAtom_SP atom, Rototranslation_SP ltr) {
  PdbAtom_SP a = atom->clone();
  lambda_rotation->apply(*a);                             // --- rotate by lambda
  ltr->apply_inverse(*a);                                 // --- transform to GCS
  return a;
}

void Bbq::rebuild(const double lambda_value, Rototranslation_SP ltr, Residue_SP destin_residue,
    Residue_SP destin_residue_next) {

  set_rotation_by_lambda(lambda_value, lambda_rotation);

  const std::vector<data::structural::PdbAtom_SP> &atoms_set = (destin_residue->residue_type() ==
                                                                chemical::Monomer::PRO) ? P : A;
  index1 first = 0;
  if (destin_residue->find_atom(" N  ") != nullptr) first = 1;
  for (index1 ai = first; ai < atoms_set.size() - 1; ++ai) {
    if ((destin_residue->residue_type() == chemical::Monomer::GLY) && (atoms_set[ai]->atom_name() == " CB ")) continue;
    PdbAtom_SP a = transform_atom(atoms_set[ai], ltr);                     // --- make a deep copy of an atom
    destin_residue->push_back(a);
  }
  if (destin_residue_next != nullptr) {
    PdbAtom_SP a = transform_atom(atoms_set[atoms_set.size() - 1], ltr);
    destin_residue_next->push_back(a);                       // --- amide N goes to the next residue
  }
}

data::structural::Chain_SP Bbq::rebuild(const Chain &chain, std::vector<double> lambdas) {

  // --- set a timer to check how long doest it take to reconstruct a given chain
  std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

  Chain_SP out_chain = std::make_shared<Chain>(chain.id());
  ca.clear();
  index2 nres = chain.count_residues();

  for (const Residue_SP ri: chain) {
    PdbAtom_SP i_ca = ri->find_atom(" CA ");
    ca.push_back(i_ca);
    Residue_SP r = std::make_shared<Residue>(ri->id(), ri->residue_type()); // --- deep copy of each residue
    r->icode(ri->icode());
    out_chain->push_back(r);
  }
  Residue_SP destin_residue;
  Residue_SP destin_residue_next;
  transformations::Rototranslation_SP lcs;
  // --- the loop over all residues in the chain that will be reconstructed
  for (core::index4 ires = 1; ires < nres - 2; ++ires) {
    destin_residue = (*out_chain)[ires];
    destin_residue_next = (*out_chain)[ires + 1];
    // --- CA atoms indexing is shifted by one comparing to lambdas vector! out_chain is the same as CA but we ommit index 0 and index nres not to rebuild dummy residues
    lcs = core::calc::structural::transformations::local_BBQ_coordinates(*ca[ires-1], *ca[ires], *ca[ires + 1]);
    // --- if statement is neccesary for -real_lambda option to not rebuilding 1st and last residue with lambda=0 (added by hand to match the indexing without -real_lambda option)
    if (lambdas[ires-1]!=0)
        rebuild(lambdas[ires-1], lcs, destin_residue, destin_residue_next);

#ifdef DEBUG
//    // --- If debug, test whether the reconstructed lambda is similar to the imposed value; for cases where omega=180, both lambda values should be identical
//    if (ires > 0) {
//      auto prev_ca = (*out_chain)[ires - 1]->find_atom(" CA ");
//      auto the_ca = (*out_chain)[ires]->find_atom(" CA ");
//      auto next_ca = ca[ires + 1];
//      auto the_o = (*out_chain)[ires]->find_atom(" O  ");
//      double l = evaluate_dihedral_angle(*prev_ca, *the_ca, *next_ca, *the_o);
////      std::cerr << l << "  " << lambdas[ires]<<"\n";
//      if (fabs(l - lambdas[ires]) > 0.175) {
//        throw std::runtime_error("lambda angle after reconstruction differs by more than 10deg from the assumed value");
//      }
//    }
#endif
  }

  // last residue is build separately because we don't want to add the amide nitrogen to the next residue because its only dummy one
  // this is why we give a nullptr instead of destin_residue_next
  lcs = core::calc::structural::transformations::local_BBQ_coordinates(*ca[nres - 3], *ca[nres-2], *ca[nres -1]);
  destin_residue = (*out_chain)[nres - 2];
    if (lambdas[nres-3]!=0) {
        rebuild(lambdas[nres - 3], lcs, destin_residue, nullptr);
        PdbAtom_SP ca = destin_residue->find_atom(" CA ");
        PdbAtom_SP c = destin_residue->find_atom(" C  ");
        PdbAtom_SP o = destin_residue->find_atom(" O  ");
        destin_residue->push_back(create_OXT(*ca, *c, *o));
    }

  // --- renumber atoms
  index2 i_atom = 0;
  std::for_each(out_chain->first_atom(), out_chain->last_atom(), [&](PdbAtom_SP e) { (e)->id(++i_atom); });

  std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(now - start);
  logs << utils::LogLevel::INFO << "Chain of " << nres-2 << " residues reconstructed in " << time_span.count() << " sec.\n";
  return out_chain;
}


void Bbq::load_atom_from_pdb(const std::string &fname, std::vector<PdbAtom_SP> &destination) {

  core::data::io::Pdb reader(fname, core::data::io::keep_all, core::data::io::keep_all, true, true);
  auto strctr = reader.create_structure(0);
  for (auto atom_it = strctr->first_atom(); atom_it != strctr->last_atom(); ++atom_it) destination.push_back(*atom_it);
  destination.pop_back();           //remove CA from next residue (it is in PDB file to look cool in pymol)
}


}
}
}
