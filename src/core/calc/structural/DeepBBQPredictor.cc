//
// Created by Maksymilian Głowacki on 04/04/2022.
//

#include <core/calc/structural/DeepBBQPredictor.hh>

std::map<char, std::vector<double>> DeepBBQPredictor::amino_acids;

void DeepBBQPredictor::initialize() {
    const std::string aa_list = "ARNDBCEQZGHILKMFPSTWYVUOZJX?";
    std::vector<std::vector<double>> aa_table(aa_list.size(), std::vector<double>(aa_list.size(), 0.0f));
    for(int i = 0; i < aa_list.size(); ++i)
    {
        aa_table[i][i] = 1.0f;
        amino_acids[aa_list[i]] = aa_table[i];
    }
}

std::vector<double> DeepBBQPredictor::predict(std::istream &str) {
    double lambda, pos, r13_b, r13_f, r14_b, r14_f, r15_b, r15_f, n_hb_E, n_hb_H, n4, n45, n5, n6, omega,cis;
    std::string pdb_code;
    char aa, ss;

    std::vector<double> zeros(44, 0.0f);
    std::vector<std::vector<double>> input(num_zeros, zeros);
    while(str >> pdb_code >> pos >> omega >> lambda >> aa >> ss >> cis >> r13_b >> r13_f >> r14_b >> r14_f >> r15_b >>
                       r15_f >> n_hb_E >> n_hb_H >> n4 >> n45 >> n5 >> n6)
    {
        std::vector<double> input_row = {r13_b, r13_f, r14_b, r14_f, r15_b, r15_f, n_hb_E, n_hb_H, n4, n45, n5, n6};
        if(ss == 'H')
        {
            input_row.insert(input_row.end(), {1.0f, 0.0f, 0.0f});
        }
        else
        {
            if(ss == 'E')
            {
                input_row.insert(input_row.end(), {0.0f, 1.0f, 0.0f});
            }
            else
            {
                input_row.insert(input_row.end(), {0.0f, 0.0f, 1.0f});
            }
        }
        input_row.insert(input_row.end(), amino_acids[aa].begin(), amino_acids[aa].end());
        input_row.push_back(cis);
        input.emplace_back(input_row);
    }
    for(int i = 0; i < num_zeros; ++i)
    {
        input.emplace_back(zeros);
    }

    fdeep::tensor tensor(fdeep::tensor_shape(input.size(), 44), 0.0f);
    for(int x = 0; x < input.size(); ++x)
    {
        for(int c = 0; c < zeros.size(); ++c)
        {
            tensor.set(fdeep::tensor_pos(x, c), input[x][c]);
        }
    }
    const auto result = model.predict({tensor})[0];
    std::vector<double> lambdas(result.shape().width_);
    for(int i = 0; i < lambdas.size(); ++i)
    {
        lambdas[i] = std::atan2(result.get(fdeep::tensor_pos(i, 0)), result.get(fdeep::tensor_pos(i, 1)));
    }
    return lambdas;
}
