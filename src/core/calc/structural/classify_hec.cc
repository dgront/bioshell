#include <vector>

#include <core/index.hh>
#include <core/calc/structural/classify_hec.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/structural/PdbAtom.hh>

namespace core {
namespace calc {
namespace structural {

static const float centroids[12][4] = {
    { 6.21, 5.03, 5.09, 5.39 }, // c0 - alpha
    { 6.13, 8.14, 5.26, 5.40 }, // c26 - alpha
    { 8.16, 8.75, 5.54, 5.50 }, // c54 - alpha
    { 7.88, 5.60, 6.12, 5.48 }, // c74 - alpha
    { 13.00, 9.79, 9.89, 6.54 }, // c2 - beta
    { 10.85, 8.24, 10.10, 6.82 }, // c42 - highly twisted beta
    { 6.37, 5.15, 8.00, 5.49 }, // c48 - beta
    { 5.30, 5.29, 7.36, 5.40 }, // c90 - highly twisted beta
    { 8.25, 8.06, 8.71, 7.05 }, // c111 - beta
    { 11.65, 9.42, 8.03, 6.21 }, // c19 - extended loop
    { 8.95, 5.53, 8.37, 5.69 }, // c21 - extended loop
    { 10.43, 8.91, 8.75, 6.45 } // c39 - turn
};

void  classify_12(const core::alignment::scoring::LocalStructure5 & local_distances, std::vector<unsigned char> & result) {

  if(result.size() < local_distances.size() / 4) result.resize(local_distances.size() / 4);
  for (core::index4 q_pos = 0; q_pos < local_distances.size() / 4; ++q_pos) {
    int best_i = -1;
    float best_d = 100000.0;
    std::vector<float>::const_iterator q_it = local_distances.get_distances(q_pos);
    for(int i=0;i<12;++i) {
      float d =
          core::alignment::scoring::local_structure_match<std::vector<float>::const_iterator, float*, 4>(q_it, (float*)(centroids[i]));
      if(d < best_d) {
        best_d = d;
        best_i = i;
      }
    }
    result[q_pos] = best_i;
  }
}

std::string classify_hec(const core::alignment::scoring::LocalStructure5 & local_distances, float cutoff) {

  std::string out(local_distances.size()/4,'C');
  for (core::index4 q_pos = 0; q_pos < local_distances.size() / 4; ++q_pos) {
    int best_i = -1;
    float best_d = 100000.0;
    std::vector<float>::const_iterator q_it = local_distances.get_distances(q_pos);
    for(int i=0;i<12;++i) {
      float d = core::alignment::scoring::local_structure_match<std::vector<float>::const_iterator,
          float*, 4>(q_it, (float*)(centroids[i]));
      if (d < best_d) {
        best_d = d;
        best_i = i;
      }
    }
    if ((best_i < 4) && (best_d <= cutoff)) {
      out[q_pos] = 'H';
      continue;
    }
    if ((best_i < 9) && (best_d <= cutoff)) out[q_pos] = 'E';
  }

  return out;
}

void describe_helical_ca_geometry(const core::data::structural::Residue &donor,
                                  const core::data::structural::Residue &acceptor,
                                  std::tuple<float, float, float> &result) {

  using namespace data::structural;

  const PdbAtom &donor_ca = *(donor.find_atom_safe(" CA "));
  const PdbAtom &before_donor_ca = *(donor.previous()->find_atom_safe(" CA "));
  const PdbAtom &acceptor_ca = *(acceptor.find_atom_safe(" CA "));
  const PdbAtom &after_acceptor_ca = *(acceptor.next()->find_atom_safe(" CA "));

  data::basic::Vec3 r1(after_acceptor_ca);
  r1 -= donor_ca;
  data::basic::Vec3 r2(acceptor_ca);
  r2 -= (before_donor_ca);

  std::get<0>(result) = r1.length();
  std::get<1>(result) = r2.length();
  std::get<2>(result) = r1.dot_product(r2);
}

std::tuple<float, float, float> describe_helical_ca_geometry(const data::structural::Residue &donor,
                                                          const data::structural::Residue &acceptor) {

  std::tuple<float, float, float> out =  std::make_tuple<float, float, float>(0,0,0);
  describe_helical_ca_geometry(donor,acceptor,out);

  return out;
}

void describe_beta_ca_geometry(const core::data::structural::Residue &donor,
                                  const core::data::structural::Residue &acceptor,
                                  std::tuple<float, float, float, char> &result) {

  using namespace data::structural;

  const PdbAtom &donor_ca = *(donor.find_atom_safe(" CA "));
  const PdbAtom &before_donor_ca = *(donor.previous()->find_atom_safe(" CA "));
  const PdbAtom &after_donor_ca = *(donor.next()->find_atom_safe(" CA "));
  const PdbAtom &acceptor_ca = *(acceptor.find_atom_safe(" CA "));
  const PdbAtom &before_acceptor_ca = *(acceptor.previous()->find_atom_safe(" CA "));
  const PdbAtom &after_acceptor_ca = *(acceptor.next()->find_atom_safe(" CA "));

  float d = acceptor_ca.distance_to(donor_ca);
  float daa = after_acceptor_ca.distance_to(after_donor_ca);
  float dbb = before_acceptor_ca.distance_to(before_donor_ca);
  float dba = before_acceptor_ca.distance_to(after_donor_ca);
  float dab = after_acceptor_ca.distance_to(before_donor_ca);

  if (daa + dbb < dab + dba) { // --- parallel case
    std::get<0>(result) = daa;
    std::get<1>(result) = dbb;
    std::get<2>(result) = d;
    std::get<3>(result) = 'P';
  } else { // --- antiparallel case
    std::get<0>(result) = dba;
    std::get<1>(result) = dab;
    std::get<2>(result) = d;
    std::get<3>(result) = 'A';
  }
}

std::tuple<float, float, float, char> describe_beta_ca_geometry(const data::structural::Residue &donor,
                                                          const data::structural::Residue &acceptor) {

  std::tuple<float, float, float, char> out =  std::make_tuple<float, float, float, char>(0,0,0,'P');
  describe_beta_ca_geometry(donor,acceptor,out);

  return out;
}

}
}
}
