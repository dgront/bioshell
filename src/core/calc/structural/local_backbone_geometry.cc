#include <core/data/basic/Vec3.hh>
#include <core/calc/structural/local_backbone_geometry.hh>

#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/angles.hh>
#include <core/calc/structural/TorsionBinType.hh>
#include <core/data/sequence/SecondaryStructure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/Structure.hh>

namespace core {
namespace calc {
namespace structural {

double Distance::operator()(const core::data::structural::ResidueSegment &rs) const {

  const core::data::structural::PdbAtom &a = *(rs[i_]->find_atom(iname_));
  const core::data::structural::PdbAtom &b = *(rs[j_]->find_atom(jname_));
  return a.distance_to(b);
}


double R14x::operator()(const core::data::structural::ResidueSegment &rs) const {

  const core::data::structural::PdbAtom &a = *(rs[ires_]->find_atom(" CA "));
  const core::data::structural::PdbAtom &b = *(rs[ires_+1]->find_atom(" CA "));
  const core::data::structural::PdbAtom &c = *(rs[ires_+2]->find_atom(" CA "));
  const core::data::structural::PdbAtom &d = *(rs[ires_+3]->find_atom(" CA "));
  return core::data::basic::r14x(a, b, c,d);
}

double PlanarAngle::operator()(const core::data::structural::ResidueSegment &rs) const {

  const core::data::structural::PdbAtom &a = *(rs[i_]->find_atom(iname_));
  const core::data::structural::PdbAtom &b = *(rs[j_]->find_atom(jname_));
  const core::data::structural::PdbAtom &c = *(rs[k_]->find_atom(kname_));
  return calc::structural::evaluate_planar_angle(a,b,c);
}



double DihedralAngle::operator()(const core::data::structural::ResidueSegment &rs) const {

  const core::data::structural::PdbAtom &a = *(rs[i_]->find_atom(iname_));
  const core::data::structural::PdbAtom &b = *(rs[j_]->find_atom(jname_));
  const core::data::structural::PdbAtom &c = *(rs[k_]->find_atom(kname_));
  const core::data::structural::PdbAtom &d = *(rs[l_]->find_atom(lname_));
  return calc::structural::evaluate_dihedral_angle(a,b,c, d);
}


double Phi::operator()(const core::data::structural::ResidueSegment &rs) const {

  const auto &r = *(rs[ires_]);    // --- the residue at which Phi is evaluated
  if (r.previous() != nullptr)
    return calc::structural::evaluate_phi(*r.previous(), r);
  else if (ires_ > 0)
    return calc::structural::evaluate_phi(*(rs[ires_ - 1]), r);
  else return std::numeric_limits<double>::quiet_NaN();
}

double LambdaDihedral::operator()(const core::data::structural::ResidueSegment &rs) const {

  const auto &r = *(rs[ires_]);    // --- the residue at which lambda is evaluated
  if ((r.previous() != nullptr) && (r.next() != nullptr))
    return calc::structural::evaluate_lambda(*r.previous(), r, *r.next());
  else if ((ires_ > 0) && (ires_ < rs.length() - 1))
    return calc::structural::evaluate_lambda(*(rs[ires_ - 1]), r, *(rs[ires_ - 1]));
  else return std::numeric_limits<double>::quiet_NaN();
}

double Psi::operator()(const core::data::structural::ResidueSegment &rs) const {

  const auto &r = *(rs[ires_]);    // --- the residue at which Phi is evaluated
  if (r.next() != nullptr)
    return calc::structural::evaluate_psi(r, *r.next());
  else if (ires_ < rs.length() - 1)
    return calc::structural::evaluate_psi(r, *(rs[ires_ + 1]));
  else return std::numeric_limits<double>::quiet_NaN();
}


double Omega::operator()(const core::data::structural::ResidueSegment &rs) const {

  const auto &r = *(rs[ires_]);    // --- the residue at which Phi is evaluated
  if (r.next() != nullptr)
    return calc::structural::evaluate_omega(r, *r.next());
  else if (ires_ < rs.length() - 1)
    return calc::structural::evaluate_omega(r, *(rs[ires_ + 1]));
  else return std::numeric_limits<double>::quiet_NaN();
}

double CaNeighborsCount::operator()(const core::data::structural::ResidueSegment &rs) const {

  double cnt = 0;
  const core::data::structural::PdbAtom_SP the_ca = rs[ires_]->find_atom(" CA ");
  if (the_ca == nullptr) return 0.0;
  const core::data::structural::Structure &s = *(rs.structure());
  for (auto it = s.first_const_residue(); it != s.last_const_residue(); ++it) {
    const core::data::structural::PdbAtom_SP ca = (**it).find_atom(" CA ");
    if (ca == nullptr) continue;
    if (((**it).owner()->id() == rs[ires_]->owner()->id()) && (abs((**it).id() - rs[ires_]->id()) < seq_sep_)) continue;
    if (the_ca->distance_to(*ca) < d_cutoff_) ++cnt;
  }

  return cnt;
}

}
}
}
