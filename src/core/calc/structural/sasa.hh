#ifndef CORE_CALC_STRUCTURAL_sasa_HH
#define CORE_CALC_STRUCTURAL_sasa_HH

#include <core/data/structural/Structure.hh>

namespace core {
namespace calc {
namespace structural {

/** @brief Calculate Solvent Accessible Surface Area of a biomacromolecular structure using Shrake - Rupley algorithm.
 *
 * This function calculates SASA value for every atom of an input structure, the number of values stored should match
 * the number of atoms.
 * @param s - input structure
 * @param sasa - vector where results will be stored
 * @param probe_radius - radius of a water (solvent) sphere
 * @param n_sphere_points - number of points uniformly distributed on a sphere to approximate it's surface
 */
void shrake_rupley_sasa(const core::data::structural::Structure &s, std::vector<double> &sasa, double probe_radius = 1.6,
                        core::index2 n_sphere_points = 960);

/** @brief Calculate Solvent Accessible Surface Area of a biomacromolecular structure using Shrake - Rupley algorithm.
 *
 * This function calculates SASA value for every atom of an input structure, the size of the returned vector
 * should matchthe number of atoms.
 * @param s - input structure
 * @param probe_radius - radius of a water (solvent) sphere
 * @param n_sphere_points - number of points uniformly distributed on a sphere to approximate it's surface
 * @return a vector of atomic SASA values
 */
std::vector<double> shrake_rupley_sasa(const core::data::structural::Structure &s, double probe_radius = 1.6,
                        core::index2 n_sphere_points = 960);
}
}
}

#endif