#include <core/calc/structural/HECA.hh>
#include <core/data/structural/PdbAtom.hh>

namespace core {
namespace calc {
namespace structural {

utils::Logger heca_hb_logger("CAHBondEnergy");

using namespace core::data::structural;

    CAHBondEnergy::CAHBondEnergy(core::index2 which_res, double en_cutoff) : ResidueSegmentGeometry("%7.2f") {
    ires_ = which_res;
    en_cutoff_ = en_cutoff;
    name_ = "CAHBondEnergy";
}

    double CAHBondEnergy::operator()(const core::data::structural::ResidueSegment &rs) const {

        int en_cnt = 0;

        std::string key;
        try {
            key = rs[ires_]->residue_id() + ":" + rs[ires_]->owner()->id();
            core::index4 j = residue_id_to_index_.at(key);

            for (int i = 0; i < system_->n_atoms; ++i) {
                double e = energy_->evaluate_raw_energy(*system_, j, i);
                if (e < en_cutoff_) en_cnt += 1;
            }
        } catch (...) {
            heca_hb_logger << utils::LogLevel::CRITICAL << "Can't find residue object for a hash:" << key << "\n";
        }
        return en_cnt;
    }

    void CAHBondEnergy::set_strctr(const core::data::structural::Structure_SP strcr) {

        residue_id_to_index_.clear();
        sequence = "";
        secondary = "";
        core::index4 cnt = 0;
        for (auto chain: *strcr) {      // --- combine sequence from all chains if input fasta_ss data was given
            auto sec_str = chain->create_sequence();
            sequence += sec_str->sequence;
            secondary += sec_str->str();
            for (auto ri:*chain) {
                std::string key = ri->residue_id() + ":" + ri->owner()->id();
                residue_id_to_index_.insert(std::pair<std::string, core::index4>(key, cnt));
                ++cnt;
            }
        }
        system_ = std::make_shared<simulations::systems::surpass::SurpassAlfaChains>(*strcr);
        energy_ = std::make_shared<simulations::forcefields::cartesian::CAHydrogenBond>(*system_, secondary, "E");
        heca_hb_logger << utils::LogLevel::INFO << "New structure set: " << strcr->code() << "\n";
        heca_hb_logger << utils::LogLevel::INFO << cnt << " amino acid residues hashed\n";
    }

HECA::HECA(const std::string model_file,core::index2 frag_len):logger("HECA"),frag_len_(frag_len),
    model(fdeep::load_model(model_file,false,fdeep::dev_null_logger)){
    prepare_properties();
}

void HECA::prepare_properties() {
    for (int i = 0; i < frag_len_ - 2; ++i) {
        properties.push_back(std::make_shared<R13>(i)); properties.back()->format(" %5.3f");
    }
    for (int i = 0; i < frag_len_ - 3; ++i) {
        properties.push_back(std::make_shared<R14x>(i)); properties.back()->format(" %7.3f");
    }
    for (int i = 0; i < frag_len_ - 4; ++i) {
        properties.push_back(std::make_shared<R15>(i)); properties.back()->format(" %6.3f");
    }

    for (int i = 0; i < frag_len_; ++i) {
        properties.push_back(std::make_shared<CaNeighborsCount>(i, 4.0, 4));
        if (i==0) properties.back()->format("     %1.0f");
        else properties.back()->format(" %1.0f");
        properties.push_back(std::make_shared<CaNeighborsCount>(i, 4.5, 4)); properties.back()->format(" %1.0f");
        properties.push_back(std::make_shared<CaNeighborsCount>(i, 5.0, 4)); properties.back()->format(" %1.0f");
        properties.push_back(std::make_shared<CaNeighborsCount>(i, 6.0, 4)); properties.back()->format(" %1.0f");
    }

    for (int i = 0; i < frag_len_; ++i) {
        auto hb_prop = std::make_shared<CAHBondEnergy>(i, -0.1);
        properties.push_back(hb_prop);
        hbonds.push_back(hb_prop);
        if(i==0)
            hb_prop->format("     %1.0f");
        else
            hb_prop->format(" %1.0f");
    }
}

std::string HECA::assign(const core::data::structural::Structure_SP s) {

    selectors::HasProperlyConnectedCA is_connected;
    core::data::structural::selectors::IsCA is_ca;
    std::string predicted_secondary;

    logger << utils::LogLevel::INFO << "Processing " << s->code() << "\n";
    for (auto ci: *s) ci->remove_ligands();   // --- remove ligands from each chain of a structure
    Structure_SP ca_only = s->clone();
    core::protocols::keep_selected_atoms(is_ca, *ca_only);
    ResidueSegmentProvider rsp(s, frag_len_);
    for(auto hbonds_prop:hbonds) hbonds_prop->set_strctr(ca_only);
    const ResidueSegment_SP  seg = rsp.peek();
    int predicted_size=predicted_secondary.size(); // if the result is 0.5 0.5 (unknown) it checks if the size of predicted string is 1 more longer
    std::vector<double> first_full_row;
    if (is_connected(*seg)) {
        for(const auto &p:properties) {
            first_full_row.push_back((*p)(*seg));
        }
    }
    else {
        predicted_secondary+=std::string(int(frag_len_/2),'-');
        first_full_row.resize(int(frag_len_/2));}
    generate_Nend(first_full_row);
    for (auto vec: Nend) {
        fdeep::tensor t(fdeep::tensor_shape(8*frag_len_-9), vec);
        fdeep::tensor t_ex = expand_width(t,1);
        t_ex.shrink_rank();
        const auto i = model.predict_class({t_ex});
     //   for (int i=0;i<3;i++)
      //      if  ((int)std::round(result[0].get(fdeep::tensor_pos(i)))==1) {
                if (i==0) predicted_secondary+="H";
                else if (i==1) predicted_secondary+="E" ;
                else if (i==2) predicted_secondary+="C";
                else return 0;
      //      }
        if (predicted_size<predicted_secondary.size()) predicted_size=predicted_secondary.size();
        else { predicted_secondary+="C"; predicted_size=predicted_secondary.size();}
    }
    std::vector<double> last_full_row;
    while(rsp.has_next()) {
        int cn=0;
        fdeep::tensor t(fdeep::tensor_shape(8*frag_len_-9), 1);
        fdeep::tensor t_ex = expand_width(t,1);
        t_ex.shrink_rank();
        const ResidueSegment_SP  seg = rsp.next();
        last_full_row.clear();
        if (is_connected(*seg)) {
            for(const auto &p:properties) {

                //std::cout<<utils::string_format(p->format(), (*p)(*seg));
                t_ex.set(fdeep::tensor_pos(cn), (*p)(*seg));
                last_full_row.push_back((*p)(*seg));
                cn++;

            }

            const auto result = model.predict({t_ex});
            for (int i=0;i<3;i++){
                if  ((int)std::round(result[0].get(fdeep::tensor_pos(i)))==1) {
                    if (i==0) predicted_secondary+="H";
                    else if (i==1) predicted_secondary+="E" ;
                    else if (i==2) predicted_secondary+="C";
                    else return 0;
                }
            }
            if (predicted_size<predicted_secondary.size()) predicted_size=predicted_secondary.size();
            else { predicted_secondary+="C"; predicted_size=predicted_secondary.size();}
        } else predicted_secondary+="-";

    }
    if (last_full_row.size()==0) {last_full_row.resize(8*frag_len_-9);}
    generate_Cend(last_full_row);
    for (auto vec: Cend) {
        fdeep::tensor t(fdeep::tensor_shape(8*frag_len_-9), 1);
        fdeep::tensor t_ex = expand_width(t,1);
        t_ex.shrink_rank();
        for (int i=0;i<vec.size();i++)
            t_ex.set(fdeep::tensor_pos(i), vec[i]);
        const auto result = model.predict({t_ex});
        for (int i=0;i<3;i++){
            if  ((int)std::round(result[0].get(fdeep::tensor_pos(i)))==1) {
                if (i==0) predicted_secondary+="H";
                else if (i==1) predicted_secondary+="E" ;
                else if (i==2) predicted_secondary+="C";
                else return 0;
            }
        }
        if (predicted_size<predicted_secondary.size()) predicted_size=predicted_secondary.size();
        else { predicted_secondary+="C"; predicted_size=predicted_secondary.size();}
    }
    return predicted_secondary;
}

void HECA::generate_Nend(std::vector<double>& ref_row){
    int n_half = frag_len_/2.0-0.5;
    int vector_len = 8*frag_len_-9;

    for (int i=0;i<n_half;i++){
        Nend.push_back({});
        for (int j=0;j<vector_len;j++){
            Nend[i].push_back(0);
        }
    }
    int first_section_start = 0;
    int second_section_start = frag_len_ - 2 ;
    int third_section_start = 2*frag_len_ - 5;
    int fourth_section_start = 3*frag_len_ - 9;

    for (int k=0;k<n_half;k++){

        for (int j=0;j<second_section_start-n_half+k;j++){
            Nend[k][first_section_start+n_half-k+j]=ref_row[first_section_start+j];
        }
        for (int j=0;j<second_section_start-n_half-1+k;j++){
            Nend[k][second_section_start+n_half-k+j]=ref_row[second_section_start+j];
        }
        for (int j=0;j<second_section_start-n_half-2+k;j++){
            Nend[k][third_section_start+n_half+j-k]=ref_row[third_section_start+j];
        }
        for (int s=0;s<5;s++){

            for (int j=0;j<frag_len_-n_half+k;j++){
                Nend[k][fourth_section_start+frag_len_*s+n_half-k+j]=ref_row[fourth_section_start+frag_len_*s+j];
            }
        }
    }

}

void HECA::generate_Cend(std::vector<double>& ref_row){
    int n_half = frag_len_/2.0-0.5;
    int vector_len = 8*frag_len_-9;

    for (int i=0;i<n_half;i++){
        Cend.push_back({});
        for (int j=0;j<vector_len;j++){
            Cend[i].push_back(0);
        }
    }
    int first_section_start = 0;
    int second_section_start = frag_len_ - 2 ;
    int third_section_start = 2*frag_len_ - 5;
    int fourth_section_start = 3*frag_len_ - 9;

    for (int k=0;k<n_half;k++){

        for (int j=0;j<second_section_start-k;j++){
            Cend[k][first_section_start+j]=ref_row[first_section_start+j];
        }
        for (int j=0;j<second_section_start-k-1;j++){
            Cend[k][second_section_start+j]=ref_row[second_section_start+j];
        }
        for (int j=0;j<second_section_start-k-2;j++){
            Cend[k][third_section_start+j]=ref_row[third_section_start+j];
        }
        for (int s=0;s<5;s++){

            for (int j=0;j<frag_len_-k;j++){
                Cend[k][fourth_section_start+frag_len_*s+j]=ref_row[fourth_section_start+frag_len_*s+j];
            }
        }
    }

}

}
}
}
