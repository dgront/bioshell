#include <core/calc/structural/AssignHEC.hh>
#include <core/data/structural/PdbAtom.hh>

namespace core {
namespace calc {
namespace structural {


using namespace core::data::structural;

AssignHEC::AssignHEC() : n_4_1(5.17534, 0.243017),n_4_2(5.18232, 0.231134),n_4_3(19.4785, 1.77508),
                         n_P_1(4.97765, 0.388865),n_P_2(4.89869, 0.33315),
                         n_A_1_1(4.6, 0.35),n_A_1_2(4.6, 0.35),
                         n_A_2_1(5.3, 0.35),n_A_2_2(5.3, 0.35){
  P_HELIX = 0.001;
  P_STRAND = 0.1;
}

double AssignHEC::probability_EP(double d13,double d24) {

  return n_P_1.evaluate(d13) * n_P_2.evaluate(d24);
}

double AssignHEC::probability_EA(double d13,double d24) {

  return std::max(n_A_1_1.evaluate(d13) * n_A_1_2.evaluate(d24), n_A_2_1.evaluate(d13) * n_A_2_2.evaluate(d24));
}

double AssignHEC::probability_H4(double d13,double d24,double dot_product) {

  return n_4_1.evaluate(d13) * n_4_2.evaluate(d24) * n_4_3.evaluate(dot_product);
}

std::string AssignHEC::assign(const data::structural::Structure & strctr) {

  std::string hec(strctr.count_residues(),'C'); // -- output string (assigned SS)
  std::vector<core::data::structural::Residue_SP> residues; // --- we repack residues here for easier access
  for(auto ri=strctr.first_const_residue();ri!=strctr.last_const_residue();++ri)
    residues.push_back( *ri );

  std::tuple<double, double, double> tuple_H;
  std::tuple<double, double, double, char> tuple_E;

  // --- Assign alpha helices first
  // --- Check the first residue if we have a turn or not
  describe_helical_ca_geometry(*residues[4], *residues[0], tuple_H);
  bool helix_started = (probability_H4(std::get<0>(tuple_H), std::get<1>(tuple_H), std::get<2>(tuple_H)) > P_HELIX);
  for (core::index2 i_donor = 5; i_donor < residues.size(); ++i_donor) {
    describe_helical_ca_geometry(*residues[i_donor], *residues[i_donor - 4], tuple_H);
    double score = probability_H4(std::get<0>(tuple_H), std::get<1>(tuple_H), std::get<2>(tuple_H));
    if (score > P_HELIX) {
      if (helix_started) hec[i_donor - 4] = hec[i_donor - 1] = hec[i_donor - 2] = hec[i_donor - 3] = 'H';
      else helix_started = true;
    } else helix_started = false;
  }

  std::cerr << hec<<"\n";

  // --- Assign strands
  std::vector<std::pair<double, index2>> energies_partners(hec.size()); // --- holds all hydrogen bonds that may be formed by a residue; this vector will be used to find the best Hbonds for a residue
  for (core::index2 i_donor = 1; i_donor < residues.size()-1; ++i_donor) {
    if (hec[i_donor] == 'H') continue; // --- helical residues can't participate in strands
    energies_partners.clear(); // --- clear the list of hbonds as we are about to start processing the next donor residue

    for (index2 i_acptr = 1; i_acptr < residues.size()-1; ++i_acptr) { // --- iterate over acceptor residues

      if (abs(i_donor - i_acptr) < 4) continue;
      if (hec[i_acptr] == 'H') continue;// --- helical residues can't participate in strands

      describe_beta_ca_geometry(*residues[i_donor], *residues[i_acptr], tuple_E);
      if (std::get<3>(tuple_E) == 'P') {     // --- Parallel strands
        double score = probability_EP(std::get<0>(tuple_E), std::get<1>(tuple_E));
//        std::cerr << "HB P " << residues[i_donor]->id()<<" -> "<<residues[i_acptr]->id()<<" "<<score<<" "<<std::get<0>(tuple_E)<<" "<< std::get<1>(tuple_E)<<"\n";
        if (score > P_STRAND) energies_partners.emplace_back( score, i_acptr);
      } else {     // --- Parallel strands
        double score = probability_EA(std::get<0>(tuple_E), std::get<1>(tuple_E));
//        std::cerr << "HB A " << residues[i_donor]->id()<<" -> "<<residues[i_acptr]->id()<<" "<<score<<" "<<std::get<0>(tuple_E)<<" "<< std::get<1>(tuple_E)<<"\n";
        if (score > P_STRAND) energies_partners.emplace_back( score, i_acptr);
      }
    }

    std::cerr << energies_partners.size()<<"\n";
    if (energies_partners.size() > 0) {
      std::sort(energies_partners.begin(), energies_partners.end(),
        [](const std::pair<double, index2> &lhs, const std::pair<double, index2> &rhs) { return lhs.first > rhs.first; });
      hec[i_donor] = 'E';
      hec[energies_partners[0].second] = 'E';
      std::cerr << "best " << residues[i_donor]->id() << " -> " << residues[energies_partners[0].second]->id() << " " << energies_partners[0].first << "\n";
      index2 i = 0;
      while ((abs(energies_partners[0].second - energies_partners[i].second) < 5) && (i < energies_partners.size()))
        ++i; // --- the two best HBonds must be separated in sequence!
      if (i < energies_partners.size()) {
        hec[energies_partners[i].second] = 'E';
        std::cerr << "best " << residues[i_donor]->id() << " -> " << residues[energies_partners[i].second]->id() << " " << energies_partners[i].first << "\n";
      }
    }
  }

  return hec;
}

void AssignHEC::describe_helical_ca_geometry(const Residue &donor, const Residue &acceptor,
                                             std::tuple<double, double, double> &result) const {

  const PdbAtom &donor_ca = *(donor.find_atom_safe(" CA "));
  const PdbAtom &before_donor_ca = *(donor.previous()->find_atom_safe(" CA "));
  const PdbAtom &acceptor_ca = *(acceptor.find_atom_safe(" CA "));
  const PdbAtom &after_acceptor_ca = *(acceptor.next()->find_atom_safe(" CA "));

  data::basic::Vec3 r1(after_acceptor_ca);
  r1 -= donor_ca;
  data::basic::Vec3 r2(acceptor_ca);
  r2 -= (before_donor_ca);

  std::get<0>(result) = r1.length();
  std::get<1>(result) = r2.length();
  std::get<2>(result) = r1.dot_product(r2);
}

void AssignHEC::describe_beta_ca_geometry(const Residue &donor, const Residue &acceptor,
                                          std::tuple<double, double, double, char> &result) const {

  using namespace data::structural;

  const PdbAtom &donor_ca = *(donor.find_atom_safe(" CA "));
  const PdbAtom &before_donor_ca = *(donor.previous()->find_atom_safe(" CA "));
  const PdbAtom &after_donor_ca = *(donor.next()->find_atom_safe(" CA "));
  const PdbAtom &acceptor_ca = *(acceptor.find_atom_safe(" CA "));
  const PdbAtom &before_acceptor_ca = *(acceptor.previous()->find_atom_safe(" CA "));
  const PdbAtom &after_acceptor_ca = *(acceptor.next()->find_atom_safe(" CA "));

  double d = acceptor_ca.distance_to(donor_ca);
  double daa = after_acceptor_ca.distance_to(after_donor_ca);
  double dbb = before_acceptor_ca.distance_to(before_donor_ca);
  double dba = before_acceptor_ca.distance_to(after_donor_ca);
  double dab = after_acceptor_ca.distance_to(before_donor_ca);

  if(daa+dbb < dab+dba) { // --- parallel case
    std::get<0>(result) = daa;
    std::get<1>(result) = dbb;
    std::get<2>(result) = d;
    std::get<3>(result) = 'P';
  } else { // --- antiparallel case
    std::get<0>(result) = dba;
    std::get<1>(result) = dab;
    std::get<2>(result) = d;
    std::get<3>(result) = 'A';
  }
}

}
}
}
