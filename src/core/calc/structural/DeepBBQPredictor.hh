//
// Created by Maksymilian Głowacki on 04/04/2022.
//

#ifndef LIBRA_DEEPBBQPREDICTOR_H
#define LIBRA_DEEPBBQPREDICTOR_H

#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <fdeep/fdeep.hpp>

class DeepBBQPredictor {
private:
    static std::map<char, std::vector<double>> amino_acids;
    int num_zeros;
    const fdeep::model model;

public:
    static void initialize();
    DeepBBQPredictor(const std::string& model_path, const std::string& model_name) : model(fdeep::load_model(model_path))
    {
        num_zeros = (int)std::strtol(model_name.substr(8, 2).c_str(), nullptr, 10);
    }
    std::vector<double> predict(std::istream &str);
};


#endif //LIBRA_DEEPBBQPREDICTOR_H
