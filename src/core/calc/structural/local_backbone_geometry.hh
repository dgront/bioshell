/** @file local_backbone_geometry.hh
* @brief Methods that calculate local geometrical properties of a protein backbone, such as distances, angles and torsions.
*/

#ifndef CORE_CALC_STRUCTURAL_LocalBackboneGeometry_HH
#define CORE_CALC_STRUCTURAL_LocalBackboneGeometry_HH

#include <memory>

#include <core/index.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/ResidueSegment.hh>

namespace core {
namespace calc {
namespace structural {

class ResidueSegmentGeometry {
public:

  explicit ResidueSegmentGeometry(const std::string & name, const std::string & format = "%f") {
    name_ = name;
    format_ = format;
  }

virtual ~ResidueSegmentGeometry() = default;

  /** @brief Evaluates the respective structural property
   * @param rs - a residue segment
   * @return double value of the property, e.g. a distance or an angle
   */
  virtual double operator()(const core::data::structural::ResidueSegment &rs) const = 0;

  /** @brief Returns name of this property.
   * @return a derived class  must return a string identifying a property
   */
  const std::string & name() const { return name_; }

  /** @brief Returns a string used to format output of this property to string (R&K syntax)
   * @return  R&K - style formatting string, e.g. @code "%7.2f" @endcode
   */
  const std::string & format() const { return format_; }

  /** @brief Sets a string used to format output of this property to string (R&K syntax)
   * @param  fmt - R&K - style formatting string, e.g. @code "%7.2f" @endcode
   */
  void format(const std::string & fmt) { format_ = fmt; }

protected:
  std::string format_;  ///< The formatting string is stored by the base class
  std::string name_;  ///< The name of this property, e.g. to be printed in a table's header
};

/// A shared pointer to ResidueSegmentGeometry helps using pointers to derived classes
typedef typename std::shared_ptr<ResidueSegmentGeometry> ResidueSegmentGeometry_SP;

class Distance : public ResidueSegmentGeometry {
public:

  Distance(core::index2 i_res, const std::string &i_atname, const core::index2 j_res, const std::string &j_atname,
      const std::string & format = "%f") : ResidueSegmentGeometry("Distance", format), iname_(i_atname), jname_(j_atname) {
    i_ = i_res;
    j_ = j_res;
  }

  double operator()(const core::data::structural::ResidueSegment &rs) const override;

private:
  core::index2 i_;
  core::index2 j_;
  const std::string iname_;
  const std::string jname_;
};

class R13 : public Distance {
public:

  R13(core::index2 i_res) : Distance(i_res, " CA ", i_res + 2, " CA ", "%5.3f") { name_ = "R13"; }

  virtual ~R13() = default;
};

class R14 : public Distance {
public:

  R14(core::index2 i_res) : Distance(i_res, " CA ", i_res + 3, " CA ", "%6.3f") { name_ = "R14"; }

  virtual ~R14() = default;
};

class R14x : public ResidueSegmentGeometry {
public:

  R14x(core::index2 i_res) : ResidueSegmentGeometry("R14x", "%7.3f") { ires_ = i_res; }

  virtual ~R14x() = default;

  double operator()(const core::data::structural::ResidueSegment &rs) const override;

private:
  core::index2 ires_;
};

class R15 : public Distance {
public:

  R15(core::index2 i_res) : Distance(i_res, " CA ", i_res + 4, " CA ", "%6.3f") { name_ = "R15"; }

  virtual ~R15() = default;
};


class PlanarAngle : public ResidueSegmentGeometry {
public:

  PlanarAngle(core::index2 i_res, const std::string &i_atname, const core::index2 j_res, const std::string &j_atname,
      const core::index2 k_res, const std::string &k_atname)
      : ResidueSegmentGeometry("%6.2f"), iname_(i_atname), jname_(j_atname), kname_(k_atname) {
    i_ = i_res;
    j_ = j_res;
    k_ = k_res;
    name_ = "Planar";
  }

  double operator()(const core::data::structural::ResidueSegment &rs) const override;

private:
  core::index2 i_;
  core::index2 j_;
  core::index2 k_;
  const std::string iname_;
  const std::string jname_;
  const std::string kname_;
};


class A13 : public PlanarAngle {
public:

  A13(core::index2 first_res) : PlanarAngle(first_res, " CA ", first_res + 1, " CA ", first_res + 2, " CA ") { name_ = "A13"; }

  virtual ~A13() = default;
};


class TauPlanar : public PlanarAngle {
public:

  TauPlanar(core::index2 which_res) : PlanarAngle(which_res, " N  ", which_res, " CA ", which_res, " C  ") { name_ = "Tau"; }

  virtual ~TauPlanar() = default;
};

class DihedralAngle : public ResidueSegmentGeometry {
public:

  DihedralAngle(core::index2 i_res, const std::string &i_atname, const core::index2 j_res, const std::string &j_atname,
      const core::index2 k_res, const std::string &k_atname, const core::index2 l_res, const std::string &l_atname)
      : ResidueSegmentGeometry("%7.2f"), iname_(i_atname), jname_(j_atname), kname_(k_atname), lname_(l_atname) {
    i_ = i_res;
    j_ = j_res;
    k_ = k_res;
    l_ = l_res;
    name_ = "Dihedral";
  }

  double operator()(const core::data::structural::ResidueSegment &rs) const override;

private:
  core::index2 i_;
  core::index2 j_;
  core::index2 k_;
  core::index2 l_;
  const std::string iname_;
  const std::string jname_;
  const std::string kname_;
  const std::string lname_;
};


class T14 : public DihedralAngle {
public:

  T14(core::index2 first_res) : DihedralAngle(first_res, " CA ", first_res + 1, " CA ",
      first_res + 2, " CA ", first_res + 3, " CA ") { name_ = "T14"; }

  virtual ~T14() = default;
};

class Phi : public ResidueSegmentGeometry {
public:
  Phi(core::index2 which_res) : ResidueSegmentGeometry("%7.2f") { ires_ = which_res;  name_ = "Phi"; }

  double operator()(const core::data::structural::ResidueSegment &rs) const override;

  virtual ~Phi() = default;

private:
  core::index2 ires_;
};


class Psi : public ResidueSegmentGeometry {
public:
  Psi(core::index2 which_res)  : ResidueSegmentGeometry("%7.2f") { ires_ = which_res;  name_ = "Psi"; }

  double operator()(const core::data::structural::ResidueSegment &rs) const override;

  virtual ~Psi() = default;
private:
  core::index2 ires_;
};


class Omega : public ResidueSegmentGeometry {
public:
  Omega(core::index2 which_res)  : ResidueSegmentGeometry("%7.2f") { ires_ = which_res;  name_ = "Omega"; }

  double operator()(const core::data::structural::ResidueSegment &rs) const override;

  virtual ~Omega() = default;

private:
  core::index2 ires_;
};

class LambdaDihedral : public ResidueSegmentGeometry {
public:
  LambdaDihedral(core::index2 which_res)  : ResidueSegmentGeometry("%7.2f") { ires_ = which_res;  name_ = "Lambda"; }

  double operator()(const core::data::structural::ResidueSegment &rs) const override;

  virtual ~LambdaDihedral() = default;

private:
  core::index2 ires_;
};

class CaNeighborsCount : public ResidueSegmentGeometry {
public:
  CaNeighborsCount(core::index2 which_res, double d_cutoff, core::index2 min_seq_separation=4)  : ResidueSegmentGeometry("%7.2f") {
    ires_ = which_res;
    seq_sep_ = min_seq_separation;
    d_cutoff_ = d_cutoff;
    name_ = "CaNeighbors";
  }

  double operator()(const core::data::structural::ResidueSegment &rs) const override;

  virtual ~CaNeighborsCount() = default;

private:
  core::index2 ires_;
  double d_cutoff_;
  core::index2 seq_sep_;
};

}
}
}

#endif //CORE_CALC_STRUCTURAL_LocalBackboneGeometry_HH
