#ifndef CORE_DATA_STRUCTURAL_Bbq_H
#define CORE_DATA_STRUCTURAL_Bbq_H

#include <string>

#include <core/index.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/structural/Structure.hh>
#include <utils/Logger.hh>
#include <core/data/structural/Chain.fwd.hh>
#include <core/data/structural/PdbAtom.fwd.hh>
#include <core/calc/structural/transformations/Rototranslation.fwd.hh>

namespace core {
namespace calc {
namespace structural {

/** @brief Reconstructs backbone atoms in a given CA-only structure
 *
 */
class Bbq {
public:

  /** @brief Constructor loads data necessary for BB reconstruction
   */
  Bbq();

 /** @brief reconstruct backbone using the vector values of \f$\lambdas \f$  for each peptide plate
  *
  * @param chain input chain
  * @param lambdas vector of lambda values
  * @return a newly created chain object with full backbone and beta-carbons
  */
  data::structural::Chain_SP rebuild(const data::structural::Chain &chain, std::vector<double> lambdas);

 /** @brief translate given atom with Rototranslation (given local coordinates) and rotates it lambda_rotation
  *
  * @param atom input chain
  * @param ltr local coordinates to translate atom
  * @return an atom with new coordinates
  */
  data::structural::PdbAtom_SP transform_atom(data::structural::PdbAtom_SP atom, transformations::Rototranslation_SP ltr);

 /** @brief reconstruct one peptide plate using the given \f$\lambda \f$ value
  * this method rebuilds N (if it's not already in a residue), CA, C, O, CB in destin_residue and N in destin_residue_next
  *
  * @param lambda_value lambda value
  * @param ltr Rototranslation object which is local_BBQ_coordinates for three CA atoms (method rebuilds the middle residue)
  * @param destin_residue everything except amide N goes to this residue (if it is N-term then N also will be rebuild)
  * @param destin_residue_next amide N goes to this residue
  */
  void rebuild(const double lambda_value, core::calc::structural::transformations::Rototranslation_SP ltr,
      data::structural::Residue_SP destin_residue, data::structural::Residue_SP destin_residue_next = nullptr);


private:
  utils::Logger logs;
  std::vector<data::structural::PdbAtom_SP> P, A, PH, AH; ///< Ideal peptide plates
  core::calc::structural::transformations::Rototranslation_SP lambda_rotation; ///< Rotation around X axis by lambda angle

  std::string ss_string; ///< Secondary structure of the target (a chain currently being reconstructed)
  std::vector<data::structural::PdbAtom_SP> ca; ///< CA atoms of the target (a chain currently being reconstructed)

  /// Helper function used to load BBQ's peptide plate from PDB files
  void load_atom_from_pdb(const std::string &fname, std::vector<data::structural::PdbAtom_SP> &destination);

};

/// Calculate a dummy alpha carbon posiiotn
data::structural::PdbAtom_SP dummy_ca_N(const data::basic::Vec3 &ca0, const data::basic::Vec3 &ca1, const data::basic::Vec3 &ca2);

data::structural::PdbAtom_SP dummy_ca_C(const data::basic::Vec3 &ca_2, const data::basic::Vec3 &ca_1, const data::basic::Vec3 &ca);
}
}
}

#endif
