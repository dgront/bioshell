#include <cmath>

#include <core/data/basic/Vec3.hh>
#include <core/calc/structural/sasa.hh>
#include <core/calc/structural/NeighborGrid3D.hh>
#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace structural {

utils::Logger logger("sasa");

// Returns list of coordinates on a sphere using the golden-section spiral algorithm.
void generate_sphere_points(const index2 n, std::vector<data::basic::Vec3> &points) {

  if (points.size() != n) points.resize(n);
  double inc = M_PI * (3.0 - sqrt(5.0));
  double offset = 2.0 / n;
  for (core::index2 i = 0; i < n; ++i) {
    double y = i * offset - 1 + (offset / 2);
    double r = sqrt(1 - y * y);
    double phi = i * inc;
    points[i].x = cos(phi) * r;
    points[i].y = y;
    points[i].z = sin(phi) * r;
  }
}

void shrake_rupley_sasa(const core::data::structural::Structure &s, std::vector<double> &sasa, double probe_radius,
                        core::index2 n_sphere_points) {

  // Start timer
  auto start = std::chrono::high_resolution_clock::now();

  core::index4 atoms_cnt = s.count_atoms();
  if(sasa.size() != atoms_cnt) sasa.resize(atoms_cnt);
  std::vector<data::basic::Vec3> points(n_sphere_points);
  generate_sphere_points(n_sphere_points, points);
  double cnst = 4.0 * M_PI / n_sphere_points;
  core::calc::structural::NeighborGrid3D grid(s,probe_radius+2.0);
  std::vector<double> area(atoms_cnt);

  std::vector<core::data::structural::PdbAtom_SP> neighbors;
  core::index4 i_atom = 0;
  for (auto it_atom_i = s.first_const_atom(); it_atom_i != s.last_const_atom(); ++it_atom_i) {
    neighbors.clear();
    grid.get_neighbors(**it_atom_i,neighbors);
    double radius = core::chemical::AtomicElement::vdw_radius[(**it_atom_i).element_index()] + probe_radius;
    core::index4 n_accessible_points = 0;
    for (const auto & point : points) {
      bool is_accessible = true;
      Vec3 tested_point(point);
      tested_point *= radius;
      tested_point += (**it_atom_i);
      for(auto atom_j : neighbors) {
        double radius_j = core::chemical::AtomicElement::vdw_radius[(*atom_j).element_index()] + probe_radius;
        if (atom_j->distance_square_to(tested_point) < radius_j*radius_j) {
          is_accessible = false;
        }
      } // ~ end of neighbors
      if (is_accessible) n_accessible_points ++;
    } // ~ end of grid points on atom i
    sasa[i_atom] = cnst*n_accessible_points*radius*radius;
    ++i_atom;
  }

  auto end = std::chrono::high_resolution_clock::now();
  logger << " SASA for " << i_atom << " atoms calculated after "
         << (double) (std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()) << " [ms]\n";
}

std::vector<double> shrake_rupley_sasa(const core::data::structural::Structure &s, double probe_radius,
                                       core::index2 n_sphere_points) {

  std::vector<double> sasa;
  shrake_rupley_sasa(s, sasa, probe_radius, n_sphere_points);

  return sasa;
}

}
}
}
