/** \file protein_angles.hh
 * @brief Defines functions to compute protein-specific dihedral angles: \f$\Phi\f$, \f$\Psi\f$, \f$\omega\f$
 *
 * The following example list all Chi angles in a given protein structure (input PDB file):
 * \include ex_evaluate_chi.cc
 * The following example define all rotamer type (trans = T; gauche+ = P; gauche- = M) in a given protein structure (input PDB file):
 * \include ex_define_rotamer.cc
 */
#ifndef CORE_CALC_STRUCTURE_protein_angles_H
#define CORE_CALC_STRUCTURE_protein_angles_H

#include <vector>

#include <core/index.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/structural/Residue.fwd.hh>

namespace core {
namespace calc {
namespace structural {

/** @brief Evaluates the \f$\Phi\f$ dihedral angle at a given residue.
 * @param prev_residue - residue that precedes <code>the_residue</code> in the sequence; necessary to provide coordinates of carbonyl C atom
 * @param the_residue - the residue for which the \f$\Phi\f$ dihedral angle will be evaluated
 * @return \f$\Phi\f$ torsion angle value
 */
double evaluate_phi(const core::data::structural::Residue & prev_residue,const core::data::structural::Residue & the_residue);

/** @brief Evaluates the \f$\Psi\f$ dihedral angle at a given residue.
 * @param the_residue - the residue for which the \f$\Psi\f$ dihedral angle will be evaluated
 * @param next_residue - residue that immediately follows <code>the_residue</code> in the sequence; necessary to provide coordinates of amide nitrogen
 * @return \f$\Psi\f$ torsion angle value
 */
double evaluate_psi(const core::data::structural::Residue & the_residue,const core::data::structural::Residue & next_residue);

/** @brief Evaluates the \f$\omega\f$ dihedral angle at a given residue.
 * @param the_residue - the residue for which the \f$\omega\f$ dihedral angle will be evaluated
 * @return \f$\omega\f$ torsion angle value
 */
double evaluate_omega(const core::data::structural::Residue & the_residue,const core::data::structural::Residue & next_residue);

/** @brief Evaluates the \f$\omega\f$ dihedral angle at a given residue.
 * @param the_residue - the residue for which the \f$\omega\f$ dihedral angle will be evaluated
 * @return \f$\omega\f$ torsion angle value
 */
double evaluate_omega(const core::data::structural::Residue_SP the_residue,const core::data::structural::Residue_SP next_residue);

/** @brief Evaluates the \f$\chi\f$ dihedral angle at a given residue.
 * @param the_residue - the residue for which the \f$\chi\f$ dihedral angle will be evaluated
 * @return \f$\chi\f$ torsion angle value
 */
double evaluate_chi(const core::data::structural::Residue & the_residue, const core::index2 dihedral_index);

/** @brief Evaluates all the \f$\chi\f$ dihedral angles for a given residue and stores results in a vector.
 *
 * Thevector will be emptied prior the evaluation
 * @param the_residue - the residue for which the \f$\chi\f$ dihedral angles will be evaluated
 * @param destination - vector where the values will be stored
 */
void evaluate_chi(const core::data::structural::Residue & the_residue, std::vector<double> & destination);

/** @brief Evaluates the \f$\chi\f$ dihedral angle at a given residue
 * @param the_residue - the residue for which the \f$\chi\f$ dihedral angle will be evaluated
 * @return \f$\chi\f$ torsion angle value
 */
double evaluate_chi(const core::data::structural::Residue & the_residue, const core::index2 dihedral_index);

/** @brief Returns the rotamer code ("T", "M", or "P") for a given dihedral angle
 * @param chi_1 - dihedral angle value (from -pi to pi)
 * @return rotamer code as a string
 */
std::string define_rotamer(const double chi_1);

/** @brief Returns the rotamer code ("TM", "MM" etc) for a given rotamer defined by its two dihedral angle values
 * @param chi_1 - dihedral angle value (from -pi to pi)
 * @param chi_2 - dihedral angle value (from -pi to pi)
 * @return rotamer code as a string
 */
std::string define_rotamer(const double chi_1,const double chi_2);

/** @brief Returns the rotamer code ("TMP", "MMT" etc) for a given rotamer defined by its three dihedral angle values
 * @param chi_1 - dihedral angle value (from -pi to pi)
 * @param chi_2 - dihedral angle value (from -pi to pi)
 * @param chi_3 - dihedral angle value (from -pi to pi)
 * @return rotamer code as a string
 */
std::string define_rotamer(const double chi_1,const double chi_2,const double chi_3);

/** @brief Returns the rotamer code ("TMPT", "MMMT" etc) for a given rotamer defined by its four dihedral angle values
 * @param chi_1 - dihedral angle value (from -pi to pi)
 * @param chi_2 - dihedral angle value (from -pi to pi)
 * @param chi_3 - dihedral angle value (from -pi to pi)
 * @param chi_4 - dihedral angle value (from -pi to pi)
 * @return rotamer code as a string
 */
std::string define_rotamer(const double chi_1,const double chi_2,const double chi_3,const double chi_4);

/** @brief Returns the rotamer code ("TMPT", "MMMT" etc) for a given rotamer defined by its four dihedral angle values.
 * @param chi - a vector of dihedral angle values (from -pi to pi)
 * @return rotamer code as a string
 */
std::string define_rotamer(std::vector<double> & chi);

/** @brief Returns the rotamer code ("T", "MPTT", "MP", "PTM" etc) for a given residue
 * @param the_residue - the residue for which the rotamer type will be evaluated
 * @return rotamer code as a string
 */
std::string define_rotamer(const core::data::structural::Residue & the_residue);

/** @brief Evaluates the \f$\lambda\f$ dihedral angle at a given residue.
 *
 * \f$\lambda\f$ is defined as a dihedral angle formed by \f$C\alpha_{i-1}\f$, \f$C\alpha_{i}\f$, \f$C\alpha_{i+1}\f$ and \f$O_{i}\f$
 * and describes the angle between a peptide plate and a plane formed by three subsequent alpha carbons.
 *
 * @param prev_residue - residue that precedes <code>the_residue</code> in the sequence; necessary to provide coordinates of CA atom
 * @param the_residue - the residue for which the \f$\lambda\f$ dihedral angle will be evaluated
 * @param next_residue - residue that immediately follows <code>the_residue</code> in the sequence; necessary to provide coordinates of CA atom
 * @return \f$\Phi\f$ torsion angle value
 */
double evaluate_lambda(const core::data::structural::Residue & prev_residue,const core::data::structural::Residue & the_residue,const core::data::structural::Residue & next_residue);


/**  @brief Calculate cartesian coordinates from z-matrix coordinates.
 *
 * @param behind_3 - third-behind atom
 * @param behind_2 - second-behind atom
 * @param behind_1 - atom directly preceding the new coordinated being computed
 * @param bond_length - length of a bond between atoms <code>behind_1</code> and <code>new_atom</code>
 * @param planar_angle - planar angle between atoms <code>behind_2</code>, <code>behind_1</code> and <code>new_atom</code>
 * @param dihedral_angle - dihedral angle between atoms <code>behind_3</code>, <code>behind_2</code>, <code>behind_1</code> and <code>new_atom</code>
 * @param new_atom coordinates of new atom
 */
void z_matrix_to_cartesian(const data::basic::Vec3 &behind_3, const data::basic::Vec3 &behind_2,
                           const data::basic::Vec3 &behind_1, double bond_length,
                           const double planar_angle, const double dihedral_angle, data::basic::Vec3 & new_atom);

}
}
}

#endif

/**
 * \example ex_evaluate_chi.cc
 * \example ex_define_rotamer.cc
 */
