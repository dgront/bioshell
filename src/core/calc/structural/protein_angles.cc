#include <string.h>
#include <cmath>

#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>

#include <core/calc/structural/angles.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/calc/structural/TorsionBinType.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <core/chemical/ChiAnglesDefinition.hh>

namespace core {
namespace calc {
namespace structural {

using namespace core::data::structural;

utils::Logger protein_angles_logger("protein_angles");

double evaluate_phi(const Residue &prev_residue, const Residue &the_residue) {

  const core::data::structural::selectors::ResidueHasBB validator = core::data::structural::selectors::ResidueHasBB();
  if (!validator(the_residue)) {
    protein_angles_logger << utils::LogLevel::WARNING << "Residue " << the_residue.residue_type().code3 << " "
                          << the_residue.id() << " lacks a backbone atom so omega angle can't be evaluated!\n";
    return 0.0;
  }
  if (!validator(prev_residue)) {
    protein_angles_logger << utils::LogLevel::WARNING << "Residue " << prev_residue.residue_type().code3 << " "
                          << prev_residue.id() << " lacks a backbone atom so Phi angle can't be evaluated!\n";
    return 0.0;
  }

  return evaluate_dihedral_angle(*std::static_pointer_cast<core::data::basic::Vec3>(prev_residue.find_atom(" C  ")),
                                 *std::static_pointer_cast<core::data::basic::Vec3>(the_residue.find_atom(" N  ")),
                                 *std::static_pointer_cast<core::data::basic::Vec3>(the_residue.find_atom(" CA ")),
                                 *std::static_pointer_cast<core::data::basic::Vec3>(the_residue.find_atom(" C  ")));
}


double evaluate_psi(const Residue &the_residue, const Residue &next_residue) {

  const core::data::structural::selectors::ResidueHasBB validator = core::data::structural::selectors::ResidueHasBB();
  if (!validator(the_residue)) {
    protein_angles_logger << utils::LogLevel::WARNING << "Residue " << the_residue.residue_type().code3 << " "
                          << the_residue.id() << " lacks a backbone atom so Psi angle can't be evaluated!\n";
    return 0.0;
  }
  if (!validator(next_residue)) {
    protein_angles_logger << utils::LogLevel::WARNING << "Residue " << next_residue.residue_type().code3 << " "
                          << next_residue.id() << " lacks a backbone atom so Psi angle can't be evaluated!\n";
    return 0.0;
  }

  return evaluate_dihedral_angle(*std::static_pointer_cast<core::data::basic::Vec3>(the_residue.find_atom(" N  ")),
                                 *std::static_pointer_cast<core::data::basic::Vec3>(the_residue.find_atom(" CA ")),
                                 *std::static_pointer_cast<core::data::basic::Vec3>(the_residue.find_atom(" C  ")),
                                 *std::static_pointer_cast<core::data::basic::Vec3>(next_residue.find_atom(" N  ")));
}

double evaluate_omega(const core::data::structural::Residue_SP the_residue,
                          const core::data::structural::Residue_SP next_residue) {

  return evaluate_omega(*the_residue, *next_residue);
}

double evaluate_omega(const Residue &the_residue, const Residue &next_residue) {

  const core::data::structural::selectors::ResidueHasBB validator = core::data::structural::selectors::ResidueHasBB();
  if (!validator(the_residue)) {
    protein_angles_logger << utils::LogLevel::WARNING << "Residue " << the_residue.residue_type().code3 << " "
                          << the_residue.id() << " lacks a backbone atom so omega angle can't be evaluated!\n";
    return 0.0;
  }
  if (!validator(next_residue)) {
    protein_angles_logger << utils::LogLevel::WARNING << "Residue " << next_residue.residue_type().code3 << " "
                          << next_residue.id() << " lacks a backbone atom so omega angle can't be evaluated!\n";
    return 0.0;
  }

  return evaluate_dihedral_angle(*std::static_pointer_cast<core::data::basic::Vec3>(the_residue.find_atom(" CA ")),
                                 *std::static_pointer_cast<core::data::basic::Vec3>(the_residue.find_atom(" C  ")),
                                 *std::static_pointer_cast<core::data::basic::Vec3>(next_residue.find_atom(" N  ")),
                                 *std::static_pointer_cast<core::data::basic::Vec3>(next_residue.find_atom(" CA ")));
}

double evaluate_chi(const core::data::structural::Residue &the_residue, const core::index2 dihedral_index) {

  const std::vector<std::string> &atoms = core::chemical::ChiAnglesDefinition::chi_angle_atoms(
      the_residue.residue_type(), dihedral_index);

  return evaluate_dihedral_angle(*the_residue.find_atom_safe(atoms[0]), *the_residue.find_atom_safe(atoms[1]),
                                 *the_residue.find_atom_safe(atoms[2]), *the_residue.find_atom_safe(atoms[3]));
}


void evaluate_chi(const core::data::structural::Residue &the_residue, std::vector<double> &destination) {

  destination.clear();
  unsigned short n = core::chemical::ChiAnglesDefinition::count_chi_angles(the_residue.residue_type());
  for (unsigned short i = 1; i <= n; ++i)
    destination.push_back(core::calc::structural::evaluate_chi(the_residue, i));
}

char define_one(const double chi_value) {

  static const double PI_2_3 = M_PI * 2.0 / 3.0;
  if ((chi_value > PI_2_3) || (chi_value < -PI_2_3)) {
    return 'T';
  } else if ((chi_value >= 0.0) && (chi_value <= PI_2_3)) {
    return 'P';
  } else if ((chi_value < 0.0) && (chi_value >= -PI_2_3)) {
    return 'M';
  }

  return 'X';
}

std::string define_rotamer(const double chi_1) {

  std::string s(1, define_one(chi_1));
  return s;
}

std::string define_rotamer(const double chi_1, const double chi_2) {

  std::string rot_type = "XX";
  rot_type[0] = define_one(chi_1);
  rot_type[1] = define_one(chi_2);

  return rot_type;
}

std::string define_rotamer(const double chi_1, const double chi_2, const double chi_3) {

  std::string rot_type = "XXX";
  rot_type[0] = define_one(chi_1);
  rot_type[1] = define_one(chi_2);
  rot_type[2] = define_one(chi_3);

  return rot_type;
}

std::string
define_rotamer(const double chi_1, const double chi_2, const double chi_3, const double chi_4) {

  std::string rot_type = "XXX";
  rot_type[0] = define_one(chi_1);
  rot_type[1] = define_one(chi_2);
  rot_type[2] = define_one(chi_3);
  rot_type[3] = define_one(chi_4);

  return rot_type;
}

std::string define_rotamer(std::vector<double> &chi) {

  std::string rot_type;
  for (core::index1 i = 0; i < chi.size(); ++i)
    rot_type += define_one(chi[i]);

  return rot_type;
}

std::string define_rotamer(const core::data::structural::Residue &the_residue) {

  std::vector<double> chi;
  unsigned short n = core::chemical::ChiAnglesDefinition::count_chi_angles(the_residue.residue_type());
  for (unsigned short i = 1; i <= n; ++i)
    chi.push_back(core::calc::structural::evaluate_chi(the_residue, i));

  return define_rotamer(chi);
}

double evaluate_lambda(const core::data::structural::Residue &prev_residue,
                           const core::data::structural::Residue &the_residue,
                           const core::data::structural::Residue &next_residue) {

  return evaluate_dihedral_angle(*std::static_pointer_cast<core::data::basic::Vec3>(prev_residue.find_atom_safe(" CA ")),
                                 *std::static_pointer_cast<core::data::basic::Vec3>(the_residue.find_atom_safe(" CA ")),
                                 *std::static_pointer_cast<core::data::basic::Vec3>(next_residue.find_atom_safe(" CA ")),
                                 *std::static_pointer_cast<core::data::basic::Vec3>(the_residue.find_atom_safe(" O  ")));
}

void z_matrix_to_cartesian(const data::basic::Vec3 &behind_3, const data::basic::Vec3 &behind_2,
                           const data::basic::Vec3 &behind_1, double bond_length,
                           const double planar_angle, const double dihedral_angle, data::basic::Vec3 &new_atom) {

  double sin_planar = sin(M_PI - planar_angle);
  double cos_planar = cos(M_PI - planar_angle);
  double sin_dih = sin(dihedral_angle);
  double cos_dih = cos(dihedral_angle);
  double bs = bond_length * sin_planar;
  new_atom.x = bond_length * cos_planar;
  new_atom.y = bs * cos_dih;
  new_atom.z = bs * sin_dih;

  // Translate atoms so behind_1 is at the origin.
  Vec3 a3(behind_3);
  a3 -= behind_1;
  Vec3 a2(behind_2);
  a2 -= behind_1;

  double d2_xz = a2.x * a2.x + a2.z * a2.z;
  double d2 = sqrt(d2_xz + a2.y * a2.y);
  double dxz = sqrt(d2_xz);

  double d2_inverse, dxz_inverse;
  double xx1, x2o, y2o, z2o, xz2o;
  if (d2 < 0.001) d2_inverse = 1.0 / 0.001;
  else d2_inverse = 1.0 / d2;

  if (dxz < 0.001) {
    xx1 = a3.x;
    x2o = 1.0;
    z2o = 0.0;
  } else {
    dxz_inverse = 1.0 / dxz;
    x2o = a2.x * dxz_inverse;
    z2o = a2.z * dxz_inverse;
    xx1 = a3.x * x2o + a3.z * z2o;
    a3.z = a3.z * x2o - a3.x * z2o;
  }

  xz2o = dxz * d2_inverse;
  y2o = a2.y * d2_inverse;
  a3.x = (-xx1 * xz2o) - a3.y * y2o;
  a3.y = xx1 * y2o - a3.y * xz2o;

  double dyz = sqrt(a3.y * a3.y + a3.z * a3.z);
  double dyz_inverse = 1.0 / dyz;
  double y1o = a3.y * dyz_inverse;
  double z1o = a3.z * dyz_inverse;
  double yy4 = y1o * new_atom.y - z1o * new_atom.z;
  double zz4 = y1o * new_atom.z + z1o * new_atom.y;

  double xx4 = y2o * yy4 - xz2o * new_atom.x;
  new_atom.y = (-xz2o * yy4) - y2o * new_atom.x;
  new_atom.x = x2o * xx4 - z2o * zz4;
  new_atom.z = z2o * xx4 + x2o * zz4;

  new_atom += behind_1;
}


}
}
}
