#ifndef CORE_CALC_STRUCTURAL_TorsionBinType_HH
/** \file TorsionBinType.hh
 * @brief Defines the torsion bin type (ABEGO) classification based on \f$\Phi\f$, \f$\Psi\f$, \f$\omega\f$  dihedral angles.
 *
 */
#define CORE_CALC_STRUCTURAL_TorsionBinType_HH

#include <core/data/structural/Residue.hh>

namespace core {
namespace calc {
namespace structural {

/** \brief Definition of 5 state Phi-Psi-Omega classification.
 * The five states denotes:
 *    - A : alpha
 *    - B : beta strand
 *    - E : everything else
 *    - G : left-handed helices
 *    - O : cis-omega
 */
enum TorsionBinType { A, B, E, G, O };

/** \brief Converts a TorsionBinType enum value into its <code>char</code> representation
 */
inline char torsion_bin_char(const TorsionBinType abego) {
  switch(abego) {
    case A : return 'A';
    case B : return 'B';
    case G : return 'G';
    case O : return 'O';
    default: return 'E';
  }
}

/** \brief Detects a bin for a given  Phi-Psi-Omega observation.
 * @param prev_residue - the preceding residue
 * @param the_residue - the residue for which the ABEGO symbol is determined
 * @param next_residue - the following residue
 * @return torsion bin type
 */
TorsionBinType torsion_bin(const core::data::structural::Residue_SP prev_residue,
    const core::data::structural::Residue_SP the_residue,const core::data::structural::Residue_SP next_residue);

/** \brief Detects a bin for a given  Phi-Psi-Omega observation.
 * @param prev_residue - the preceding residue
 * @param the_residue - the residue for which the ABEGO symbol is determined
 * @param next_residue - the following residue
 * @return torsion bin type
 */
TorsionBinType torsion_bin(const core::data::structural::Residue & prev_residue,
    const core::data::structural::Residue & the_residue,const core::data::structural::Residue & next_residue);

/** \brief Detects a bin for a given  Phi-Psi-Omega observation.
 * @param phi - Phi angle at a residue of interest
 * @param psi - Psi angle at a residue of interest
 * @param omega - omega angle at a residue of interest
 * @return torsion bin type
 */
TorsionBinType torsion_bin(const double phi,const double psi,const double omega);

}
}
}

#endif // ~CORE_CALC_STRUCTURAL_TorsionBinType_HH
