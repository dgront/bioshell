#ifndef CORE_CALC_STRUCTURAL_NeighborGrid3D_HH
#define CORE_CALC_STRUCTURAL_NeighborGrid3D_HH

#include <memory>

#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace structural {

/** @brief 3D grid used to hash atomic spatial neighborhood.
 *
 * 3D Cartesian space is divided into regular cubes. Atoms in the same cube are stored under the same hash in
 * a <code>std::map</code>
 */
class NeighborGrid3D {
public:

  /// Width of each cube - space element
  const double grid_mesh;

  /** @brief Creates a grid to hash atoms of a given structure
   * @param cx - X center of the gid
   * @param cy - Y center of the gid
   * @param cz - Z center of the gid
   * @param grid_mesh - width of a cubic box - space element
   */
  NeighborGrid3D(const double cx, const double cy, const double cz, const double grid_mesh);

  /** @brief Creates a grid to hash atoms
   * @param strctr - structure to be hashed
   * @param grid_mesh - width of a cubic box - space element
   */
  NeighborGrid3D(const core::data::structural::Structure &strctr, const double grid_mesh);

  /** @brief Creates a grid to hash atoms of a given structure
   * @param strctr - structure to be hashed
   * @param selector - selector used to pick a subset of atoms to be hashed
   * @param grid_mesh - width of a cubic box - space element
   */
  NeighborGrid3D(const core::data::structural::Structure &strctr,
                 core::data::structural::selectors::AtomSelector & selector, const double grid_mesh);

  /** @brief Calculates a hash value for a given atom based on its coordinates
   * @param v - atom
   * @return integer hash
   */
  core::index4 hash(const core::data::structural::PdbAtom &v) const;

  /** @brief Calculates a hash value for a given integer coordinates of a grid cell
   * @param ix - x number of a cell
   * @param iy - y number of a cell
   * @param iz - z number of a cell
   * @return integer hash
   */
  core::index4 hash(const core::index2 ix,const core::index2 iy,const core::index2 iz) const { return (ix << 20) + (iy << 10) + iz; }

  /** @brief Calculates a hash value for a given integer coordinates of a grid cell
   * @param h - integer hash value
   * @param ix - returned x number of a cell
   * @param iy - returned y number of a cell
   * @param iz - returned z number of a cell
   */
  void xyz_from_hash(core::index4 h, core::index2 &ix, core::index2 &iy, core::index2 &iz) const;

  /** @brief Adds a single atom to the grid
   * @param v - atom to be added
   */
  void insert(core::data::structural::PdbAtom_SP v);

  /** @brief Returns plausible neighbors of a given atom.
   *
   * This method copies to the given vector of neighbors  all members of the cube the given atom is located plus
   * all members of 26 surrounding cubes (+1 and -1 in every direction)
   *
   * @param v - a query atom
   * @param sink - vector where pointers to neighbor atoms are copied. Previous content is not cleared!
   */
  void get_neighbors(const core::data::structural::PdbAtom &v,std::vector<core::data::structural::PdbAtom_SP> & sink);

  /** @brief Returns plausible neighbors of a given hash.
   *
   * This method copies to the given vector of neighbors  all members of the cube identified by a hash plus
   * all members of 26 surrounding cubes (+1 and -1 in every direction)
   *
   * @param v - a query atom
   * @param sink - vector where pointers to neighbor atoms are copied. Previous content is not cleared!
   */
  void get_neighbors(const core::index4 hash,std::vector<core::data::structural::PdbAtom_SP> & sink);

  /** @brief Returns hash keys pointing to all cells neighboring with the given cell.
   *
   * The middle cell (argument of this function) will also be inserted into the <code>sink</code> vector
   * @param hash - center cell
   * @param sink - vector where the cell indexes are inserted; the vector is not cleared before insertion
   */
  void get_neighbor_cells(const core::index4 hash,std::vector<index4> & sink);

  /** @brief Returns a list of hash keys to non-empty grid cells.
   * @return const-vector of hash values.
   */
  const std::vector<core::index4> & filled_cells() const { return filled_cells_; }

  /** @brief Gives access to a grid cell identified by a hash key
   * @param cell_hash - a hash key
   * @return atoms in that cell
   */
  const std::vector<core::data::structural::PdbAtom_SP> & get_cell(core::index4 cell_hash) const { return grid_.at(cell_hash); }

  /// Returns the X coordinate of the center of this grid
  double cx() const { return cx_; }

  /// Returns the X coordinate of the center of this grid
  double cy() const { return cy_; }

  /// Returns the X coordinate of the center of this grid
  double cz() const { return cz_; }

private:
  std::map<core::index4, std::vector<core::data::structural::PdbAtom_SP>> grid_;
  std::vector<core::index4> filled_cells_;
  double cx_, cy_, cz_;
  utils::Logger logs;
};

typedef typename std::shared_ptr<NeighborGrid3D> NeighborGrid3D_SP;

}
}
}

#endif