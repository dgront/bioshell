#include <core/data/basic/Vec3.hh>
#include <core/calc/structural/calculate_from_structure.hh>
#include <core/protocols/selection_protocols.hh>
#include <core/calc/structural/protein_angles.hh>


namespace core {
namespace calc {
namespace structural {

using namespace core::data::structural;


double calculate_Rg_square(const core::data::basic::Vec3 *from, const core::data::basic::Vec3 *to) {

  return calculate_Rg_square<const core::data::basic::Vec3*>(from, to);
}

double calculate_Rg_square(const core::data::basic::Vec3Cubic *from, const core::data::basic::Vec3Cubic *to) {

  return calculate_Rg_square<const core::data::basic::Vec3*>(from, to);
}

core::data::basic::Vec3 calculate_cm(const Structure &s) {

  double cx = 0, cy = 0, cz = 0, n = 0;
  for (auto ia = s.first_const_atom(); ia != s.last_const_atom(); ++ia) {
    ++n;
    cx += (**ia).x;
    cy += (**ia).y;
    cz += (**ia).z;
  }

  return core::data::basic::Vec3{float(cx / n), float(cy / n), float(cz / n)};
}

double calculate_Rg_square(const core::data::structural::Structure & strctr) {

  double s=0, n = 0, cc = 0;
  core::data::basic::Vec3 cm = calculate_cm(strctr);
  for (auto ic = strctr.first_const_atom(); ic != strctr.last_const_atom(); ++ic) {
    ++n;
    const auto &c = **ic;
    cc = c.x - cm.x;
    s += cc * cc;
    cc = c.y - cm.y;
    s += cc * cc;
    cc = c.z - cm.z;
    s += cc * cc;
  }

  return s / n;
}


void center_at(core::data::structural::Structure &s, core::data::basic::Vec3 cm) {
  center_at(s, cm.x, cm.y, cm.z);
}

void center_at(core::data::structural::Structure & s, double new_cx, double new_cy, double new_cz) {

  core::data::basic::Vec3 cm = calculate_cm(s);
  cm.x -= new_cx;
  cm.y -= new_cy;
  cm.z -= new_cz;
  for (auto ia=s.first_atom(); ia!=s.last_atom();++ia) (**ia) -= cm;
}

void copy_ca(const Structure &source, std::vector<PdbAtom_SP> & dest) {
    selectors::IsCA is_ca;
    for (auto a_it = source.first_const_atom(); a_it != source.last_const_atom(); ++a_it)
        if (is_ca(**a_it)) dest.push_back(*a_it);
}

int count_hbonds(core::index2 which_pos, simulations::systems::surpass::SurpassAlfaChains &system,
                 simulations::forcefields::cartesian::CAHydrogenBond & hb_energy) {

    int n_hb = 0;
    for (int i = 1; i < system.n_atoms - 1; ++i) {
        double e = hb_energy.evaluate_raw_energy(system, which_pos, i);
        if (e < hb_energy.hbond_energy_cutoff) n_hb += 1;
    }

    return n_hb;
}

void count_neighbors(std::vector<PdbAtom_SP> ca, int which_pos, int & n4, int & n45, int & n5, int & n6) {

    PdbAtom &a = *ca[which_pos];
    for (int i = 0; i < ca.size(); ++i) {
        if(abs(which_pos-i)<3) continue;
        double d = a.distance_to(*ca[i]);
        if (d > 6) continue;
        ++n6;
        if (d < 5) {
            ++n5;
            if (d < 4.5) {
                ++n45;
                if (d < 4) ++n4;
            }
        }
    }
}

bool compute_bbq_features(const Structure &source, std::ostream & out,bool if_calc_lambda) {

    bool result = true;

    selectors::IsCA is_ca;
    Structure_SP ca_only = source.clone();
    core::protocols::keep_selected_atoms(is_ca, *ca_only);

    std::vector<PdbAtom_SP> ca;
    for (auto a_it = source.first_const_atom(); a_it != source.last_const_atom(); ++a_it)
        if (is_ca(**a_it)) ca.push_back(*a_it);
    if(source.count_residues()!=ca_only->count_residues())
        return false;
    std::string secondary = source[0]->create_sequence()->str();  // --- secondary structure
    std::string code = source.code() + source[0]->id();
    simulations::systems::surpass::SurpassAlfaChains system(*ca_only);
    simulations::forcefields::cartesian::CAHydrogenBond hb_energy_E(system, secondary);
    simulations::forcefields::cartesian::CAHydrogenBond hb_energy_H(system, secondary, "H");
    core::index2 len = ca.size();

//  out << "#PDB   pos lambda  aa ss     caX   caY       caZ    r13b  r13f       r14b     r14f     r15b  r15f   hbonds  neighbors\n";
    out << "#PDB   pos omega lambda  aa ss cis  r13b  r13f       r14b     r14f    r15b  r15f   hbonds  neighbors\n";
    for (core::index2 which_pos = 1; which_pos < len-1; ++which_pos) {

        try {
            double lambda;
            double omega;
            int if_cis;
            if (if_calc_lambda) {
                 omega = core::calc::structural::evaluate_omega(*ca[which_pos]->owner(), *ca[which_pos + 1]->owner());
                 lambda = core::calc::structural::evaluate_lambda(*ca[which_pos - 1]->owner(), *ca[which_pos]->owner(),
                                                                    *ca[which_pos + 1]->owner());
              if_cis = int(fabs(omega) < 1.59);
            }
            else  {
              lambda= 0.0;
              omega = 0.0;
              if_cis = int(ca[which_pos]->distance_to(*ca[which_pos + 1]) < 3.6);
            }

            double r13_f = (which_pos >= len - 2) ? 0 : ca[which_pos]->distance_to(*ca[which_pos + 2]);
            double r13_b = (which_pos < 2) ? 0 : ca[which_pos]->distance_to(*ca[which_pos - 2]);
            double r14_f = (which_pos >= len - 3) ? 0 :
                           core::data::basic::r14x(*ca[which_pos], *ca[which_pos + 1], *ca[which_pos + 2], *ca[which_pos + 3]);
            double r14_b = (which_pos < 3) ? 0 :
                           core::data::basic::r14x(*ca[which_pos], *ca[which_pos - 1], *ca[which_pos - 2], *ca[which_pos - 3]);
            double r15_f = (which_pos >= len - 4) ? 0 : ca[which_pos]->distance_to(*ca[which_pos + 4]);
            double r15_b = (which_pos < 4) ? 0 : ca[which_pos]->distance_to(*ca[which_pos - 4]);

            int n_hb_H = count_hbonds(which_pos, system, hb_energy_H);
            int n_hb_E = count_hbonds(which_pos, system, hb_energy_E);

            int n4 = 0, n45 = 0, n5 = 0, n6 = 0;
            count_neighbors(ca, which_pos, n4, n45, n5, n6);
            core::data::structural::Residue &r = *(ca[which_pos]->owner());
            out << utils::string_format(
                    "%s %4d  %6.3f  %6.3f  %c %c  %d  %5.3f %5.3f    %7.3f %7.3f   %6.3f %6.3f    %1d %1d   %1d %1d %1d %1d\n",
                    code.c_str(), r.id(), omega, lambda, r.residue_type().code1, r.ss(), if_cis,
                    r13_b, r13_f, r14_b, r14_f, r15_b, r15_f, n_hb_E, n_hb_H, n4, n45, n5, n6);
        } catch (utils::exceptions::AtomNotFound & e) {
//                logger << utils::LogLevel::WARNING << code << ": can't find atom " << e.missing_atom_name << " at pos "
//                       << ca[which_pos]->owner()->id() << "\n";
            result = false;
        }
    }
    return result;
}

    bool compute_lambda(const Structure &source, std::vector<double> &lambdas) {
        bool result=true;
        core::index2 len = source.count_residues();

        for (core::index2 which_pos = 1; which_pos < len-1; ++which_pos) {
                double lambda = core::calc::structural::evaluate_lambda(*source.get_residue(which_pos-1),
                        *source.get_residue(which_pos),
                        *source.get_residue(which_pos+1));
                lambdas.push_back(lambda);
                std::cerr << lambda<<"\n";
        }
        return result;
}

}
}
}
