#ifndef CORE_CALC_STRUCTURAL_TRANSFORMATIONS_StructureSuperpositionSeed_H
#define CORE_CALC_STRUCTURAL_TRANSFORMATIONS_StructureSuperpositionSeed_H

#include <algorithm>
#include <memory>

#include <utils/exit.hh>
#include <utils/string_utils.hh>
#include <core/data/basic/Vec3.hh>
#include <core/calc/structural/transformations/Crmsd.hh>

namespace core {
namespace calc {
namespace structural {
namespace transformations {

using namespace core::data::basic;

/** @brief Represents a seed for a structural alignment that may be expanded by acquiring new aligned pairs of atoms.
 *
 */
class StructureSuperpositionSeed : public Crmsd<Coordinates,Coordinates> {
public:

  /** @brief Creates a new seed that will be used to align the given <code>tmplt</code> and <code>query</code> coordinates.
   * @param tmplt - atoms from the template structure
   * @param query - atoms from the query structure
   */
  StructureSuperpositionSeed(const Coordinates & query, const Coordinates & tmplt) :
      tmplt(tmplt), query(query),  matching_for_query(query.size()) {
    query_start = tmplt_start =  0;
    query_seed_start = tmplt_seed_start = 0;
    diagonal_length = std::min(tmplt.size(),query.size());
    seed_length = diagonal_length;
  }

  /** @brief Redefine this structure superposition seed.
   *
   * @param start_in_query - the first residue aligned in the query coordinates
   * @param start_in_tmplt  - the first residue aligned in the template coordinates
   * @param length - length of the aligned stretch
   * @return always true
   */
  bool define_seed(const core::index2 start_in_query, const core::index2 start_in_tmplt, const core::index2 length) {

    if((diagonal_length = std::min(query.size(), tmplt.size()) - abs(start_in_query - start_in_tmplt))<length) return false;

    query_seed_start = start_in_query;
    tmplt_seed_start = start_in_tmplt;
    query_start = std::max(0, start_in_query - start_in_tmplt);
    tmplt_start = std::max(0, start_in_tmplt - start_in_query);

    if(seed_tmplt.size()<unsigned(diagonal_length)) seed_tmplt.resize(diagonal_length);
    if(seed_query.size()<unsigned(diagonal_length)) seed_query.resize(diagonal_length);
    for (seed_length = 0; seed_length < length; ++seed_length) {
      seed_tmplt[seed_length].set(tmplt[seed_length + start_in_tmplt]);
      seed_query[seed_length].set(query[seed_length + start_in_query]);
    }

    return true;
  }

  /// Counts how many atoms are in the query set (which may differ from the template size).
  inline core::index2 count_query_atoms() const { return query.size(); }

  /// Counts how many atoms are in the template set (which may differ from the query size).
  inline core::index2 count_tmplt_atoms() const { return tmplt.size(); }

protected:
  index2 query_start;         ///< Index of the first atom in the query structure
  index2 tmplt_start;         ///< Index of the first atom in the template structure
  short int diagonal_length;     ///< the maximum number of alignable pairs
  index2 query_seed_start;    ///< Index of the first seed atom in the query structure
  index2 tmplt_seed_start;    ///< Index of the first seed atom in the template structure
  core::index2 seed_length;    ///< The number of atoms in the seed
  const Coordinates & tmplt;  ///< coordinates of the whole template structure
  const Coordinates & query;  ///< coordinates of the whole query structure
  Coordinates seed_tmplt;     ///< Seed coordinates from the template structure
  Coordinates seed_query;     ///< Seed coordinates from the query structure
  std::vector<short int> matching_for_query;
};

}
}
}
}

#endif
