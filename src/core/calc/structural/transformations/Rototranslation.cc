#include <iostream>

#include <core/data/basic/Vec3.hh>
#include <utils/string_utils.hh>
#include <core/data/structural/PdbAtom.hh>

#include <core/calc/structural/transformations/Rototranslation.hh>

using core::data::basic::Vec3;

namespace core {
namespace calc {
namespace structural {
namespace transformations {

Rototranslation::Rototranslation() :
    rot_x_(1, 0, 0), rot_y_(0, 1, 0), rot_z_(0, 0, 1), tr_before_(0.0), tr_after_(0.0) { update_mm(); }

std::ostream& operator<<(std::ostream &out, const Rototranslation &r) {

	out << utils::string_format("x`  =  |%9.6f %9.6f %9.6f|     (x - %9.4f)     %9.4f\n",r.rot_x_.x,r.rot_x_.y,r.rot_x_.z,r.tr_before_.x,r.tr_after_.x);
	out << utils::string_format("y`  =  |%9.6f %9.6f %9.6f|  *  (y - %9.4f)  +  %9.4f\n",r.rot_y_.x,r.rot_y_.y,r.rot_y_.z,r.tr_before_.y,r.tr_after_.y);
	out << utils::string_format("z`  =  |%9.6f %9.6f %9.6f|     (z - %9.4f)     %9.4f\n",r.rot_z_.x,r.rot_z_.y,r.rot_z_.z,r.tr_before_.z,r.tr_after_.z);
	return out;
}

void Rototranslation::apply(core::data::structural::Structure &s) const {
  for (core::data::structural::Chain_SP ci:s) apply(*ci);
}

void Rototranslation::apply_inverse(
    core::data::structural::Structure &s) const { for (core::data::structural::Chain_SP ci:s) apply_inverse(*ci); }

void Rototranslation::apply(core::data::structural::Chain &c) const { for(core::data::structural::Residue_SP ri:c) apply(*ri); }

void Rototranslation::apply_inverse(core::data::structural::Chain &c) const { for(core::data::structural::Residue_SP ri:c) apply_inverse(*ri); }

void Rototranslation::apply(core::data::structural::Residue &r) const { for(core::data::structural::PdbAtom_SP ai:r) apply(*ai); }

void Rototranslation::apply_inverse(core::data::structural::Residue &r) const { for(core::data::structural::PdbAtom_SP ai:r) apply_inverse(*ai); }

/** @brief Prepares a transformation that rotates points (e.g. atoms) around a given axis.
 *
 * The transformation may be used to rotate atoms around a bond. In the following example atoms are rotated
 * so to alter a \f$Phi\f$, \f$\Psi\f$ or \f$\Omega\f$ angle.
 *
 * @params axis_from - the first endpoint of the rotation axis, e.g. one of the two bonded atoms
 * @params axis_to - the second endpoint of the rotation axis, e.g. the second of the two bonded atoms
 * @params angle - the angle of rotation
 * @params center - the center point of rotation
 * @params destination - Rototranslation object where the transformation parameters (rotation matrix and translation vectors) will be stored
 */
void Rototranslation::around_axis(const Vec3 &axis_from,
		const Vec3 &axis_to, const double angle, const Vec3 &center,Rototranslation & destination) {

	Vec3 axis{axis_to};
	axis -= axis_from;
	axis.norm();

	around_axis(axis, angle, center, destination);
}

Rototranslation Rototranslation::around_axis(const Vec3 &axis_from,
		const Vec3 &axis_to, const double angle, const Vec3 &center) {

	Rototranslation r;
	around_axis(axis_from,axis_to,angle,center,r);

	return r;
}

/** @brief Prepares a transformation that rotates points (e.g. atoms) around a given axis.
 *
 * Rotation matrix formula taken from that document:
 * https://www.tu-chemnitz.de/informatik/KI/edu/robotik/ws2017/trans.mat.pdf
 *
 * and checked with this online calculator:
 * https://www.andre-gaschler.com/rotationconverter/
 *
 * @params axis -  the rotation axis, e.g. a bond
 * @params angle - the angle of rotation
 * @params center - the center point of rotation
 * @params destination - Rototranslation object where the transformation parameters (rotation matrix and translation vectors) will be stored
 * @see around_axis(const Vec3 &, const Vec3 &, const, const Vec3 &, Rototranslation &)
 */
void Rototranslation::around_axis(const Vec3 &axis,
		const double angle, const Vec3 &center, Rototranslation & destination) {

	double cosa = cos(angle);
	double sina = sin(angle);

	destination.tr_before_.set(center);
	destination.tr_after_.set(center);

	double x = axis.x;
	double y = axis.y;
	double z = axis.z;
	double x2 = axis.x * axis.x;
	double y2 = axis.y * axis.y;
	double z2 = axis.z * axis.z;
	double vt = 1 - cosa;

	destination.rot_x_.x = x2 * vt + cosa;
	destination.rot_x_.y = (x * y * vt - z * sina);
	destination.rot_x_.z = (x * z * vt + y * sina);

	destination.rot_y_.x = (x * y * vt + z * sina);
	destination.rot_y_.y = (y2 * vt + cosa);
	destination.rot_y_.z = (y * z * vt - x * sina);

	destination.rot_z_.x = (x * z * vt - y * sina);
	destination.rot_z_.y = (y * z * vt + x * sina);
	destination.rot_z_.z = (z2 * vt + cosa);
}


/** @brief Prepares a transformation that rotates points (e.g. atoms) around a given axis.
 *
 * @params axis -  the rotation axis, e.g. a bond
 * @params angle - the angle of rotation
 * @params center - the center point of rotation
 * @see around_axis(const Vec3 &, const Vec3 &, const, const Vec3 &, Rototranslation &)
 */
Rototranslation Rototranslation::around_axis(const Vec3 &axis, const double angle, const Vec3 &center) {

	Rototranslation r;
	around_axis(axis, angle, center, r);

	return r;
}

}
}
}
}
