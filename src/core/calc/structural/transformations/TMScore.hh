#ifndef CORE_CALC_STRUCTURAL_TRANSFORMATIONS_TMScore_H
#define CORE_CALC_STRUCTURAL_TRANSFORMATIONS_TMScore_H

#include <memory>

#include <utils/exit.hh>
#include <utils/string_utils.hh>
#include <core/data/basic/Vec3.hh>
#include <core/calc/structural/classify_hec.hh>
#include <core/calc/structural/transformations/TMScore.fwd.hh>
#include <core/calc/structural/transformations/StructureSuperpositionSeed.hh>

#include <core/alignment/scoring/LocalStructure5.hh>

#include <core/alignment/TMAlign.fwd.hh>

namespace core {
namespace calc {
namespace structural {
namespace transformations {

using namespace core::data::basic;

/** \brief Computes TM-score.
 *
 * TM-score value will be returned by <code>tmscore()</code> method. Once this method is called,
 * this object will hold also the optimal transformation.
 *
 * @see Zhang, Skolnick, "Scoring Function for Automated Assessment of Protein Structure Template Quality", Proteins 57 702-710 2004
 */
class TMScore : public StructureSuperpositionSeed {
public:

  friend core::alignment::TMAlign;
  friend std::ostream& operator<<(std::ostream & out, const TMScore & tmscore);

  /** @brief Creates the tm-score calculator for the given two sets of atoms <strong>of the same size</code>.
   *
   * @param query - the set of query atoms which remain in their positions
   * @param tmplt - the set of template atoms which remain in their positions
   */
  TMScore(const Coordinates & query, const Coordinates & tmplt) :
      StructureSuperpositionSeed(query, tmplt), fragment_size(7), eps(0.0001), l("TMScore") {
    recent_raw_tmscore_ = -1.0;
    recent_iterations_ = 0;
    diagonal_length = std::min(tmplt.size(),query.size());
  }

  /** @brief Calculates the \f$d_0\f$ constant for a given protein length
   *
   * @param length - the number of residues in the reference structure
   */
  static inline double d0(const index2 length) {
    return (length > 15) ? (1.24 * pow(double(length - 15.0), 1.0 / 3.0) - 1.8) : 0.5;
  }

  /** @brief Calculates the transformation and the subset of atoms that maximizes tm-score value.
   *
   * @return the tmscore value, normalized by the number of residues in the template structure
   */
  double tmscore() { return tmscore(tmplt.size()); }

  /** @brief Calculates tmscore value.
   *
   * The calculations will be performed based on the rotation-translation transformation
   * computed during the most recent call of <code>tmscore()</code> method.
   * Neiter <code>templt</code> nor <code>query</code> atoms will be moved
   * @param query - the set of query atoms which remain in their positions
   * @param templt - the set of template atoms which remain in their positions
   * @param n_atoms - the number of atoms in each of the two sets being compared
   */
  double calculate_tmscore_value(const Coordinates & query, const Coordinates & templt) const;

  /** @brief Calculates the transformation and the subset of atoms that maximizes tm-score value.
   *
   * @param reference_length - the number of residues used to normalize the tm-score value
   * @return the tmscore value, normalized by the given number of residues
   */
  double tmscore(core::index2 reference_length);

  /** @brief Returns the tm-score value found in the most recent evaluation.
   *
   * If the <code>tmscore()</code> method has not been called yet, a negative value is returned
   */
  double recent_tmscore(const double reference_length) const { return recent_raw_tmscore_ / reference_length; }

  /** @brief Returns the tm-score value found in the most recent evaluation.
   *
   * If the <code>tmscore()</code> method has not been called yet, <code>-1.0</code> is returned
   */
  double recent_tmscore() const { return recent_tmscore(tmplt.size()); }

  /** @brief Returns the number of atoms matched in the most recent tmscore() call
   *
   * If the <code>tmscore()</code> method has not been called yet, zero is returned
   */
  core::index2 recent_matched_length() const { return seed_length; }

private:
  const index2 fragment_size;
  index2 recent_iterations_;
  double recent_raw_tmscore_;
  const double eps;
  utils::Logger l;
  std::string query_hec;
  std::string tmplt_hec;
  Coordinates best_seed_tmplt;     ///< To store the set of atoms that provides the best tm-score
  Coordinates best_seed_query;     ///< To store the set of atoms that provides the best tm-score
  std::vector<core::index2> best_matching_for_query;
  core::index2 best_seed_length;
  double best_tmscore;
  Rototranslation best_rt;

  /** @brief Extends the initial seed to get as many aligned pairs as possible.
   *
   * @param reference_length - the reference number of atoms used for tm-score normalization
   * @param max_iterations - the maximum number of iterations
   * @param search_radius - include also pairs that are a bit further than the cutoff. The method will include all the pairs that are
   * closer than \f$d_0 + r\f$ there \f$r\f$ is the  <code>search_radius</code>
   * @param approximate - when set to true, only same secondary structure methods will be considered for superposition
   */
  double extend(const index2 reference_length, const index2 max_iterations = 10, const double search_radius = 0.0, const bool approximate = false);
};


/** \brief Prints the tmscore value and the matching pairs of atoms.
 *
 * @param out - output stream
 * @param tmscore - TMScore object
 */
std::ostream& operator<<(std::ostream & out, const TMScore & tmscore);

}
}
}
}

#endif
