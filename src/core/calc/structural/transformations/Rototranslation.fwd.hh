#ifndef CORE_CALC_STRUCTURAL_TRANSFORMATIONS_Rototranslation_FWD_H
#define CORE_CALC_STRUCTURAL_TRANSFORMATIONS_Rototranslation_FWD_H

#include <memory>

namespace core {
namespace calc {
namespace structural {
namespace transformations {

class Rototranslation;    ///< forward declaration of the Rototranslation class to be able to define a shared pointer type
typedef std::shared_ptr<Rototranslation> Rototranslation_SP; ///< Shared pointer to a Rototranslation type


}
}
}
}

#endif
