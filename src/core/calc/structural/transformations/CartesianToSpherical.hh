#ifndef CORE_CALC_STRUCTURAL_TRANSFORMATIONS_CartesianToSpherical_H
#define CORE_CALC_STRUCTURAL_TRANSFORMATIONS_CartesianToSpherical_H

#include <core/data/basic/Vec3.hh>

using core::data::basic::Vec3;

namespace core {
namespace calc {
namespace structural {
namespace transformations {

/** \brief Transformation that converts between Cartesian and spherical coordinates.
 *
 * The transformation calculates spherical coordinates from Cartesian. The transformation in calculated in a local
 * coordinate frame, i.e. an atom should be oriented properly before applying this transformation.
 *
 * The resulting coordinates follow ISO standard 80000-2:2009 (radius r, inclination \f$ \theta \f$, azimuth \f$ \phi \f$):
 *   - \f$ r \in [0,\infty) \f$
 *   - \f$ \theta \in [0,\pi) \f$
 *   - \f$ \phi \in [0,2\pi) \f$
 *
 * \include ex_CartesianToSpherical.cc
 */
class CartesianToSpherical {
public:

  /** \brief Applies this transformation to a given point
   *
   * After this call is finished, fields of the argument <code>v</code> vector will hold \f$r\f$, \f$\theta\f$ and \f$\phi\f$
   * spherical coordinates.
   * @param v - vector to be transformed
   */
  inline void apply(Vec3 &v) const {

    double r = v.length();
    double theta = 0.0, phi = 0.0;
    if (r != 0.0) {
      theta = acos(v.z / r);
      phi =  atan2(v.y, v.x);
    }
    v.x = r;
    v.y = theta;
    v.z = phi;
  }

  inline void apply_inverse(Vec3 &v) const {

    double x = v.x * sin(v.y)*cos(v.z);
    double y = v.x * sin(v.y)*sin(v.z);
    double z = v.x * cos(v.y);

    v.x = x;
    v.y = y;
    v.z = z;
  }

  /** \brief Applies this transformation to a given point
   *
   * Resulting spherical coordinates: \f$r\f$, \f$\theta\f$ and \f$\phi\f$ will be stored in a given vector
   * @param v - vector to be transformed
   * @param result - where the output coordinates are stored
   */
  inline void apply(const Vec3 &v, Vec3 &result) const {

    result.x = v.length();
    if (result.x != 0.0) {
      result.y = acos(v.z / result.x);
      result.z = atan2(v.y, v.x);
    } else
      result.y = result.z = 0.0;
  }

  inline void apply_inverse(const Vec3 &v, Vec3 &result) const {

    result.x = v.x * sin(v.y)*cos(v.z);
    result.y = v.x * sin(v.y)*sin(v.z);
    result.z = v.x * cos(v.y);
  }
};

typedef std::shared_ptr<CartesianToSpherical> CartesianToSpherical_SP; ///< Shared pointer to a CartesianToSpherical type

}
}
}
}

#endif
/**
 * \example ex_CartesianToSpherical.cc
 */