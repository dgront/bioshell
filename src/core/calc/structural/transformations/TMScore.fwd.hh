#ifndef CORE_CALC_STRUCTURAL_TRANSFORMATIONS_TMScore_FWD_HH
#define CORE_CALC_STRUCTURAL_TRANSFORMATIONS_TMScore_FWD_HH

#include <memory>

namespace core {
namespace calc {
namespace structural {
namespace transformations {

class TMScore;
typedef std::shared_ptr<TMScore> TMScore_SP;

}
}
}
}

#endif
