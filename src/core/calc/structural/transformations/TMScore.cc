#include <iostream>

#include <core/alignment/scoring/LocalStructure5.hh>
#include <core/alignment/scoring/LocalStructure7.hh>
#include <core/calc/structural/transformations/TMScore.hh>


namespace core {
namespace calc {
namespace structural {
namespace transformations {

double TMScore::extend(const index2 reference_length, const index2 max_iterations, const double search_radius,const bool approximate) {

  if(matching_for_query.size()<tmplt.size()) matching_for_query.resize(tmplt.size());
  if (best_seed_query.size() < unsigned(diagonal_length)) {
    best_seed_query.resize(diagonal_length);
    best_seed_tmplt.resize(diagonal_length);
  }

  double d0 = TMScore::d0(reference_length);
  double d0_search = d0;
  if (d0_search > 8) d0_search = 8.0;
  if (d0_search < 4.5) d0_search = 4.5;
  d0_search += search_radius;

  core::data::basic::Vec3 tmp_v;
  double tmscore = 0.0;
  double tmscore_raw = 0.0;
  double tmscore_prev = 0.0;
  recent_iterations_ = 0;
  double d0_sq = d0 * d0;
  double d0_search_sq = d0_search*d0_search;
  do {
    if (seed_length < 7) return -1.0;
//    std::fill(matching_for_query.begin(), matching_for_query.end(), -1);
    tmscore_prev = tmscore;
    tmscore_raw = 0.0;

    Crmsd<Coordinates, Coordinates>::crmsd(seed_query, seed_tmplt, seed_length, true);

    seed_length = 0;
    for (index2 i = 0; i < diagonal_length; ++i) {
      if ((approximate) && (query_hec[i + query_start] != tmplt_hec[i + tmplt_start])) continue;

      const double d2 = Crmsd<Coordinates, Coordinates>::distance_squared(query[i + query_start], tmplt[i + tmplt_start]);

      tmscore_raw += 1.0 / (1.0 + d2 / d0_sq);
      if (d2 < d0_search_sq) {
        seed_query[seed_length].set(query[i + query_start]);
        seed_tmplt[seed_length].set(tmplt[i + tmplt_start]);
        matching_for_query[i + query_start] = i + tmplt_start;
        ++seed_length;
      }
      tmscore = tmscore_raw/reference_length;
    }
    if(best_tmscore<tmscore) {
      for(index2 i=0;i<seed_length;++i) {
        best_seed_query[i].set(seed_query[i]);
        best_seed_tmplt[i].set(seed_tmplt[i]);
      }
      best_seed_length = seed_length;
      best_tmscore = tmscore;
      matching_for_query.swap(matching_for_query);
      best_rt.set(*this);
    }
    ++recent_iterations_;
  } while ((fabs(tmscore_prev - tmscore) > eps) && (recent_iterations_ <= max_iterations));
  recent_raw_tmscore_ = tmscore_raw;

  return best_tmscore;
}

double TMScore::tmscore(core::index2 reference_length) {

  if ((query.size() < 5) || (tmplt.size() < 5)) return 0.0;

  core::alignment::scoring::LocalStructure5 query_data5(query);
  query_hec = core::calc::structural::classify_hec(query_data5);
  core::alignment::scoring::LocalStructure5 tmplt_data5(tmplt);
  tmplt_hec = core::calc::structural::classify_hec(tmplt_data5);
  best_tmscore = 0.0;
  best_seed_length = 0;
  core::alignment::scoring::LocalStructure7 query_data7(query);
  core::alignment::scoring::LocalStructure7 tmplt_data7(tmplt);
  core::alignment::scoring::LocalStructureMatch<core::alignment::scoring::LocalStructure7,8> local_match(query_data7,tmplt_data7);

  if (query.size() >= tmplt.size()) {
    diagonal_length = tmplt.size();
    tmplt_start = 0;
    for (index2 offset = 0; offset <= query.size() - tmplt.size(); ++offset) {
      query_start = offset;
      for (index2 i_pos = 0; i_pos < diagonal_length - fragment_size; ++i_pos) {
        if (local_match(i_pos + query_start, i_pos + tmplt_start) > 32.0) continue;
        define_seed(i_pos + query_start, i_pos + tmplt_start, fragment_size);
        extend(reference_length, 10, 1.0);
      }
    }
  } else {
    query_start = 0;
    diagonal_length = query.size();
    for (index2 offset = 0; offset <= tmplt.size() - query.size(); ++offset) {
      tmplt_start = offset;
      for (index2 i_pos = 0; i_pos < diagonal_length - fragment_size; ++i_pos) {
        if (local_match(i_pos + query_start, i_pos + tmplt_start) > 32.0) continue;
        define_seed(i_pos + query_start, i_pos + tmplt_start, fragment_size);
        extend(reference_length, 10, 1.0);
      }
    }
  }

  recent_raw_tmscore_ = best_tmscore * reference_length;
  Rototranslation::set(best_rt);

  return best_tmscore;
}

double TMScore::calculate_tmscore_value(const Coordinates & query, const Coordinates & templt) const {

  double ret_val = 0.0;
  const double d0 = TMScore::d0(query.size())*TMScore::d0(query.size());
  for (size_t i = 0; i < query.size(); i++) {
    const double d2 = Crmsd<Coordinates, Coordinates>::distance_squared(query[i], templt[i]);
    ret_val += 1.0 / (1.0 + d2 / d0);
  }
  return ret_val /query.size();
}

std::ostream& operator<<(std::ostream & out, const TMScore & tmscore) {

  out << utils::string_format("%5.3f  ", tmscore.recent_tmscore());
  for (index2 i = 0; i < tmscore.tmplt.size(); ++i)
    out << ((tmscore.matching_for_query[i] < 0) ? '-' : '*');
  out << " "<< tmscore.diagonal_length;
  return out;
}

}
}
}
}
