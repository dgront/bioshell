/** \file classify_hec.hh
 * @brief Methods that detect secondary structure based on local C\f$\alpha\f$ trace geometry.
 */
#ifndef CORE_CALC_STRUCTURAL_classify_hec_HH
#define CORE_CALC_STRUCTURAL_classify_hec_HH

#include <vector>

#include <core/index.hh>
#include <core/alignment/scoring/LocalStructure5.hh>
#include <core/alignment/scoring/LocalStructureMatch.hh>

#include <core/data/structural/Residue.hh>

namespace core {
namespace calc {
namespace structural {

void classify_12(const core::alignment::scoring::LocalStructure5 & local_distances, std::vector<unsigned char> & result);

std::string classify_hec(const core::alignment::scoring::LocalStructure5 & local_distances, float cutoff = 5);

/** @brief Tests whether \f$ C\alpha \f$ geometry is close to alpha-helical.
 *
 * This method assesses mutual orientation of four \f$ C\alpha \f$  atoms: \f$ d-1, d, a, a+1 \f$
 * where \f$ d \f$ is the index of the hydrogen donor and \f$ a \f$ is the index of the acceptor. This method assumes
 * that \f$ d - a = 4\f$  (which is the definition of an \f$ \alpha \f$ helix.
 *
 * @param donor - residue that  donates the amide hydrogen to a hydrogen bond
 * @param acceptor  - acceptor residue in a hydrogen bond
 * @return three real parameters describing the local geometry of the \f$ C\alpha \f$ trace :
 *   - \f$ |\vec{r_1}| \f$
 *   - \f$ |\vec{r_2}| \f$
 *   \f$ \vec{r_1} \vec{r_1} \f$ (dot product)
 */
std::tuple<float, float, float> describe_helical_ca_geometry(const core::data::structural::Residue &donor,
                                                          const core::data::structural::Residue &acceptor);
/** @brief Tests whether \f$ C\alpha \f$ geometry is close to beta-strand.
 *
 * This method assesses mutual orientation of four \f$ C\alpha \f$  atoms: \f$ d-1, d, a, a+1 \f$
 * where \f$ d \f$ is the index of the hydrogen donor and \f$ a \f$ is the index of the acceptor.
 *
 * @param donor - residue that  donates the amide hydrogen to a hydrogen bond
 * @param acceptor  - acceptor residue in a hydrogen bond
 * @param result -  three real parameters describing the local geometry of the \f$ C\alpha \f$ trace are stored in a tuple :
 *   - \f$ |\vec{r_1}| \f$
 *   - \f$ |\vec{r_2}| \f$
 *   \f$ \vec{r_1} \vec{r_1} \f$ (dot product)
 *
 */
void describe_helical_ca_geometry(const core::data::structural::Residue &donor,
                                  const core::data::structural::Residue &acceptor, std::tuple<float, float, float> & result);


/** @brief Tests whether \f$ C\alpha \f$ geometry is close to alpha-helical.
 *
 *
 * @param donor - residue that  donates the amide hydrogen to a hydrogen bond
 * @param acceptor  - acceptor residue in a hydrogen bond
 * @param result -  three real parameters describing the local geometry of the \f$ C\alpha \f$ trace are stored in a tuple :
 *
 */
void describe_beta_ca_geometry(const core::data::structural::Residue &donor,
                                  const core::data::structural::Residue &acceptor, std::tuple<float, float, float, char> & result);

/** @brief Tests whether \f$ C\alpha \f$ geometry is close to beta-strand.
 *
 * @param donor - residue that  donates the amide hydrogen to a hydrogen bond
 * @param acceptor  - acceptor residue in a hydrogen bond
 * @return three real parameters describing the local geometry of the \f$ C\alpha \f$ trace :
 */
std::tuple<float, float, float, char> describe_beta_ca_geometry(const core::data::structural::Residue &donor,
                                                          const core::data::structural::Residue &acceptor);


}
}
}
#endif // ~CORE_CALC_STRUCTURAL_ClassifyHEC_HH
