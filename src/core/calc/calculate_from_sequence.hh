/** @file calculate_from_sequence.hh
 * @brief simple calculations for a protein sequence, e.g. an isoelectric point
 */
#ifndef CORE_CALC_calculate_from_sequence_HH
#define CORE_CALC_calculate_from_sequence_HH

#include <string>

#include <core/chemical/PKaValues.hh>

namespace core {
namespace calc {

/** \brief  Calculates net charge for a given amino acid sequence.
 * @param s - a protein sequence
 * @param dataSet - defines pKa values to be
 * @param pH - pH value at which the net charge of a sequence is computed
 * @return net charge i.e total charge of the sequence in a given pH.
 * @see PKaValues - available pKa values definitions
 */
double net_charge(const std::string & s, const core::chemical::PKaValues & dataSet, double pH);

/** \brief Calculates isoelectric point for a given amino acid sequence.
 *
 * The function uses Henderson-Hasselbalch equation and a set of pKa values that are defined in BioShell
 *
 * @param s - a protein sequence
 * @param dataSet - defines pKa values to be used in calculations
 * @return isoelectric point i.e. a pH in which net charge of protein is zero.
 * @see jbcl.data.dict.PKaValues - available pKa values definitions
 */
double pI(const std::string & s, const core::chemical::PKaValues & dataSet);

}
}

#endif

