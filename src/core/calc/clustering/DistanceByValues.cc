#include <core/calc/clustering/DistanceByValues.hh>

namespace core {
namespace calc {
namespace clustering {

template <typename T>
void DistanceByValues<T>::update_min(size_t row) {

  const size_t start = Array2D<T>::row_starts(row);
  const size_t stop = Array2D<T>::row_ends(row);

  const auto min_p = std::min_element(Array2D<T>::the_data.begin() + start, Array2D<T>::the_data.begin() + stop + 1);
  size_t std_min_index = std::distance(Array2D<T>::the_data.begin(), min_p);
  T std_last_min = Array2D<T>::the_data[std_min_index];
  std_min_index -= start;

  if (logger.is_logable(utils::LogLevel::FINER)) {
    logger << utils::LogLevel::FINER << "row " << row << " updated, min value is " <<
           ((std::is_same<core::index1, T>::value) ? int(std_last_min) : std_last_min) << " at index " << std_min_index
           << "\n";
  }
  min_value_4_row[row] = std_last_min;
  min_index_4_row[row] = std_min_index;
  is_row_min_updated[row] = true;
}

int scanf_row(const std::string & line, char* name_i,char* name_j,double & val) {
  return sscanf(line.c_str(), "%s %s %lf", name_i, name_j, &val);
}

int scanf_row(const std::string & line, char* name_i,char* name_j,float &val) {
  return sscanf(line.c_str(), "%s %s %f", name_i, name_j, &val);
}

int scanf_row(const std::string & line, char* name_i,char* name_j,int & val) {
  return sscanf(line.c_str(), "%s %s %d", name_i, name_j, &val);
}

int scanf_row(const std::string & line, char* name_i,char* name_j,core::index1 & val) {
  return sscanf(line.c_str(), "%s %s %hhu", name_i, name_j, &val);
}

int scanf_row(const std::string & line, char* name_i,char* name_j,core::index2 & val) {
  return sscanf(line.c_str(), "%s %s %hu", name_i, name_j, &val);
}

int scanf_row(const std::string & line, char* name_i,char* name_j,core::index4 & val) {
  return sscanf(line.c_str(), "%s %s %u", name_i, name_j, &val);
}

template <typename T>
void DistanceByValues<T>::create_object(std::istream & infile) {
  logger << utils::LogLevel::INFO << "missing value set to " << missing_value << "\n";

  std::fill(Array2D<T>::the_data.begin(), Array2D<T>::the_data.end(), missing_value);
  read_distances(infile);

  exclude_diagonal();

  for (const std::string & s : label_4_index) {
    if (s.length() > max_label_length) max_label_length = s.length();
    min_value_4_row.push_back(max_value);
    is_row_min_updated.push_back(false);
    min_index_4_row.push_back(0);
  }
  for (index4 i = 0; i < label_4_index.size(); ++i) {
    update_min(i);
    if (min_value_4_row[i] == max_value)
      logger << utils::LogLevel::WARNING << "Dangling object at index " << int(i)
             << "\n\tCheck if your distance matrix is complete!\n";
  }
}

template <typename T>
core::index4 DistanceByValues<T>::read_distances(const std::string & fname) {
  std::ifstream in(fname);
  core::index4 nn = read_distances(in);
  in.close();
  if (nn == 0) utils::exit_OK_with_message(fname + " file does not exist or is empty!\n");
  return nn;
}

template <typename T>
core::index4 DistanceByValues<T>::read_distances(std::istream & infile) {

  auto start = std::chrono::high_resolution_clock::now();
  logger << utils::LogLevel::FINE << "Expecting distances between : " << size_t(Array2D<T>::n_columns) << " elements from an input file\n";

  std::string line;
  char name_i[MAX], name_j[MAX];
  T val;
  int n_data = -1;
  size_t nn = 0;
  size_t step = (int) ((Array2D<T>::count_columns() * Array2D<T>::count_rows() / 2.0) / 100.0);
  size_t i_index = 0;
  size_t j_index = 0;
  while (std::getline(infile, line)) {
    if(line[0]=='#') continue;
    int n = scanf_row(line,name_i, name_j, val);
//    int n = sscanf(line.c_str(), "%s %s %f", name_i, name_j, &val);
    if (n < 3) {
      logger << utils::LogLevel::WARNING << "Skipping line that is too short: " + line + "\n";
      continue;
    }
    auto i_iterator = index_for_label.find(name_i);
    if (i_iterator == index_for_label.end()) {
      ++n_data;
      i_index = n_data;
      if (i_index >= Array2D<T>::count_rows()) {
        logger << utils::LogLevel::WARNING << "row index too high:" << i_index << " for the element: " << name_i
               << " (skipped). Highest row index: " << Array2D<T>::count_rows() << "\n";
        logger << utils::LogLevel::WARNING << "the following line neglected: " << line << "\n";
        n_data--;
        continue;
      }
      index_for_label[name_i] = n_data;
      label_4_index.push_back(name_i);
    } else i_index = i_iterator->second;
    auto j_iterator = index_for_label.find(name_j);
    if (j_iterator == index_for_label.end()) {
      ++n_data;
      j_index = n_data;
      if (j_index >= Array2D<T>::count_rows()) {
        logger << utils::LogLevel::WARNING << "column index too high:" << j_index << " for the element: " << name_j
               << " (skipped). Highest column index: " << Array2D<T>::count_rows() << "\n";
        logger << utils::LogLevel::WARNING << "the following line neglected: " << line << "\n";
        n_data--;
        continue;
      }
      index_for_label[name_j] = n_data;
      label_4_index.push_back(name_j);
    } else j_index = j_iterator->second;
    Array2D<T>::set(i_index, j_index, (T) val);
    Array2D<T>::set(j_index, i_index, (T) val);
    nn++;
    if (step > 0 )
      if (nn % step == 0) logger << utils::LogLevel::INFO << "got " << nn << " lines [appx " << (nn / step) << "%]\n";
  }
  auto end = std::chrono::high_resolution_clock::now();
  logger << utils::LogLevel::INFO << nn << " lines read within "
         << (double) (std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()) << " [ms]\n";

  return nn;
}

template class DistanceByValues<core::index1>;
template class DistanceByValues<core::index2>;
template class DistanceByValues<core::index4>;
template class DistanceByValues<float>;
template class DistanceByValues<double>;
}
}
}