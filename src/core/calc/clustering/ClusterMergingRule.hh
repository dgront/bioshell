/** \file ClusterMergingRule.hh
 * @brief Rules to merge two clusters into one; used by HierarchicalClustering.
 *
 * The purpose of this class is to compute the distance between a newly created cluster \f$C_{i+j}\f$ and a cluster \f$C_k\f$.
 * The new cluster is created by merging two clusters \f$C_i\f$ and \f$C_j\f$, whose size is \f$|C_i|\f$ and \f$|C_j|\f$, respectively.
 * The distance is based on the size of the three considered clusters  <tt>i</tt>, <tt>k</tt> and <tt>k</tt> as well as
 * their mutual distances.
 *
 * @see F. Murtagh, "A survey of recent advances in hierarchical clustering algorithms", The Computer Journal, 26 (1983)
 */

#ifndef CALC_CLUSTERING_ClusterMergingRule_HH
#define CALC_CLUSTERING_ClusterMergingRule_HH

#include <string>

#include <core/index.hh>

namespace core {
namespace calc {
namespace clustering {

/** @brief An abstract base class for all the merging rules used by bioshell's hierarchical clustering
 *
 * @tparam D - distance type, e.g. core::real
 * @param size_i - the size of the cluster <tt>i</tt>  \f$|C_i|\f$
 * @param size_j - the size of the cluster <tt>i</tt>  \f$|C_j|\f$
 * @param size_k - the size of the cluster <tt>i</tt>  \f$|C_k|\f$
 * @param distance_ij - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_j)\f$
 * @param distance_ik - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_k)\f$
 * @param distance_jk - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_j,C_k)\f$
 */
template<typename D>
class ClusterMergingRule {
public:

  /** @brief call-operator used to compute the distance between clusters.
   *
   * @tparam D - distance type, e.g. core::real
   * @param size_i - the size of the cluster <tt>i</tt>  \f$|C_i|\f$
   * @param size_j - the size of the cluster <tt>i</tt>  \f$|C_j|\f$
   * @param size_k - the size of the cluster <tt>i</tt>  \f$|C_k|\f$
   * @param distance_ij - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_j)\f$
   * @param distance_ik - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_k)\f$
   * @param distance_jk - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_j,C_k)\f$
   */
  virtual D operator()(const core::index4 size_i, const core::index4 size_j, const core::index4 size_k,
      const D distance_ij, const D distance_ik, const D distance_jk) const = 0;

  /// Returns the name of the strategy
  virtual const std::string & name() const = 0;
};

/** @brief Merges two clusters into one based on the <strong>single</strong> link criterion.
 *
 * The new distance is defined as:
 * \f[
 * d(C_{i+j},C_k) = min(d(C_i,C_k),d(C_j,C_k))
 * \f]
 * @tparam D - distance type, e.g. core::real
 * @param size_i - the size of the cluster <tt>i</tt>  \f$|C_i|\f$ - <strong>ignored (may be 0)</strong>
 * @param size_j - the size of the cluster <tt>i</tt>  \f$|C_j|\f$ - <strong>ignored (may be 0)</strong>
 * @param size_k - the size of the cluster <tt>i</tt>  \f$|C_k|\f$ - <strong>ignored (may be 0)</strong>
 * @param distance_ij - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_j)\f$  - <strong>ignored</strong>
 * @param distance_ik - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_k)\f$
 * @param distance_jk - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_j,C_k)\f$
 */
template<typename D>
class SingleLink : public ClusterMergingRule<D> {
public:
  D operator()(const core::index4 size_i, const core::index4 size_j, const core::index4 size_k, const D distance_ij,
      const D distance_ik, const D distance_jk) const {
    return std::min(distance_ik, distance_jk);
  }

  /// Default virtual destructor
  virtual ~SingleLink() = default;

  /// Returns the <tt>"single_link"</tt> string
  inline const std::string & name() const {
    return name_;
  }

private:
  static const std::string name_;
};

/** @brief Merges two clusters into one based on the <strong>complete</strong> link criterion.
 *
 * The new distance is defined as:
 * \f[
 * d(C_{i+j},C_k) = max(d(C_i,C_k),d(C_j,C_k))
 * \f]
 * @tparam D - distance type, e.g. core::real
 * @param size_i - the size of the cluster <tt>i</tt>  \f$|C_i|\f$ - <strong>ignored (may be 0)</strong>
 * @param size_j - the size of the cluster <tt>i</tt>  \f$|C_j|\f$ - <strong>ignored (may be 0)</strong>
 * @param size_k - the size of the cluster <tt>i</tt>  \f$|C_k|\f$ - <strong>ignored (may be 0)</strong>
 * @param distance_ij - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_j)\f$  - <strong>ignored</strong>
 * @param distance_ik - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_k)\f$
 * @param distance_jk - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_j,C_k)\f$
 */
template<typename D>
class CompleteLink : public ClusterMergingRule<D> {
public:
  D operator()(const core::index4 size_i, const core::index4 size_j, const core::index4 size_k, const D distance_ij,
      const D distance_ik, const D distance_jk) const {
    return std::max(distance_ik, distance_jk);
  }

  /// Default virtual destructor
  virtual ~CompleteLink() = default;

  /// Returns the <tt>"complete_link"</tt> string
  inline const std::string & name() const {
    return name_;
  }

private:
  static const std::string name_;
};

/** @brief Merges two clusters into one based on the <strong>complete</strong> link criterion (UPGMA).
 *
 * The new distance is defined as:
 * \f[
 * d(C_{i+j},C_k) = \frac{|C_i|}{|C_i|+|C_j|}d(C_i,C_k) +  \frac{|C_j|}{|C_i|+|C_j|}d(C_j,C_k)
 * \f]
 * @tparam D - distance type, e.g. core::real
 * @param size_i - the size of the cluster <tt>i</tt>  \f$|C_i|\f$
 * @param size_j - the size of the cluster <tt>i</tt>  \f$|C_j|\f$
 * @param size_k - the size of the cluster <tt>i</tt>  \f$|C_k|\f$ <strong>ignored (may be 0)</strong>
 * @param distance_ij - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_j)\f$  <strong>ignored</strong>
 * @param distance_ik - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_k)\f$
 * @param distance_jk - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_j,C_k)\f$
 */
template<typename D>
class AverageLink : public ClusterMergingRule<D> {
public:
  D operator()(const core::index4 size_i, const core::index4 size_j, const core::index4 size_k, const D distance_ij,
      const D distance_ik, const D distance_jk) const {
    D d = 1.0 / ((D) (size_i + size_j));
    return d * size_i * distance_ik + d * size_j * distance_jk;
  }

  /// Default virtual destructor
  virtual ~AverageLink() = default;

  /// Returns the <tt>"average_link"</tt> string
  inline const std::string & name() const {
    return name_;
  }

private:
  static const std::string name_;
};

/** @brief Merges two clusters into one based on the <strong>median</strong> link criterion (WPGMC).
 *
 * The new distance is defined as:
 * \f[
 * d(C_{i+j},C_k) = \frac{d(C_i,C_k)}{2} +  \frac{d(C_j,C_k)}{2} - \frac{d(C_i,C_j)}{4}
 * \f]
 * @tparam D - distance type, e.g. core::real
 * @param size_i - the size of the cluster <tt>i</tt>  \f$|C_i|\f$  <strong>ignored</strong>
 * @param size_j - the size of the cluster <tt>i</tt>  \f$|C_j|\f$  <strong>ignored</strong>
 * @param size_k - the size of the cluster <tt>i</tt>  \f$|C_k|\f$   <strong>ignored</strong>
 * @param distance_ij - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_j)\f$
 * @param distance_ik - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_k)\f$
 * @param distance_jk - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_j,C_k)\f$
 */
template<typename D>
class MedianLink : public ClusterMergingRule<D> {
public:
  D operator()(const core::index4 size_i, const core::index4 size_j, const core::index4 size_k, const D distance_ij,
      const D distance_ik, const D distance_jk) const {
    return 0.5 * distance_ik + 0.5 * distance_jk - 0.25 * distance_ij;
  }

  /// Default virtual destructor
  virtual ~MedianLink() = default;

  /// Returns the <tt>"median_link"</tt> string
  inline const std::string & name() const {
    return name_;
  }

private:
  static const std::string name_;
};

/** @brief Merges two clusters into one based on the <strong>centroid</strong> link criterion (UPGMC).
 *
 * The new distance is defined as:
 * \f[
 * d(C_{i+j},C_k) = \frac{|C_i|}{|C_i|+|C_j|}d(C_i,C_k) +  \frac{|C_j|}{|C_i|+|C_j|}d(C_j,C_k) - \frac{|C_j||C_j|}{(|C_i|+|C_j|)^2}d(C_j,C_k)
 * \f]
 * @tparam D - distance type, e.g. core::real
 * @param size_i - the size of the cluster <tt>i</tt>  \f$|C_i|\f$
 * @param size_j - the size of the cluster <tt>i</tt>  \f$|C_j|\f$
 * @param size_k - the size of the cluster <tt>i</tt>  \f$|C_k|\f$ <strong>ignored (may be 0)</strong>
 * @param distance_ij - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_j)\f$
 * @param distance_ik - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_k)\f$
 * @param distance_jk - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_j,C_k)\f$
 */
template<typename D>
class CentroidLink : public ClusterMergingRule<D> {
public:
  D operator()(const core::index4 size_i, const core::index4 size_j, const core::index4 size_k, const D distance_ij,
      const D distance_ik, const D distance_jk) const {
    D d = 1.0 / ((D) (size_i + size_j));
    return d * size_i * distance_ik + d * size_j * distance_jk - size_i * size_j * d * d * distance_ij;
  }

  /// Default virtual destructor
  virtual ~CentroidLink() = default;

  /// Returns the <tt>"centroid_link"</tt> string
  inline const std::string & name() const {
    return name_;
  }

private:
  static const std::string name_;
};

/** @brief Merges two clusters into one based on the <strong>minimum variance</strong> criterion (Ward's method).
 *
 * The new distance is defined as:
 * \f[
 * d(C_{i+j},C_k) = \frac{|C_i|+|C_k|}{|C_i|+|C_j|+|C_k|}d(C_i,C_k) +  \frac{|C_j|+|C_k|}{|C_i|+|C_j|+|C_k|}d(C_j,C_k) - \frac{|C_k|}{(|C_i|+|C_j|+|C_k|)}d(C_j,C_k)
 * \f]
 * @tparam D - distance type, e.g. core::real
 * @param size_i - the size of the cluster <tt>i</tt>  \f$|C_i|\f$
 * @param size_j - the size of the cluster <tt>i</tt>  \f$|C_j|\f$
 * @param size_k - the size of the cluster <tt>i</tt>  \f$|C_k|\f$
 * @param distance_ij - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_j)\f$
 * @param distance_ik - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_i,C_k)\f$
 * @param distance_jk - the distance between clusters  <tt>i</tt> and <tt>j</tt>  \f$d(C_j,C_k)\f$
 */
template<typename D>
class WardsMethod : public ClusterMergingRule<D> {
public:
  D operator()(const core::index4 size_i, const core::index4 size_j, const core::index4 size_k, const D distance_ij,
      const D distance_ik, const D distance_jk) const {
    D d = 1.0 / ((D) (size_i + size_j+ size_k));
    return d * (size_i+size_k) * distance_ik + d * (size_j+size_k) * distance_jk - size_k *d* distance_ij;
  }

  /// Default virtual destructor
  virtual ~WardsMethod() = default;

  /// Returns the <tt>"Ward's_method"</tt> string
  inline const std::string & name() const {
    return name_;
  }

private:
  static const std::string name_;
};

template<typename T>
const std::string CompleteLink<T>::name_ = "complete_link";

template<typename T>
const std::string SingleLink<T>::name_ = "single_link";

template<typename T>
const std::string AverageLink<T>::name_ = "average_link";

template<typename T>
const std::string MedianLink<T>::name_ = "median_link";

template<typename T>
const std::string WardsMethod<T>::name_ = "Ward's_method";

template<typename T>
const std::string CentroidLink<T>::name_ = "centroid_link";

typedef SingleLink<core::index1> SingleLink1B;
typedef AverageLink<core::index1> AverageLink1B;
typedef CompleteLink<core::index1> CompleteLink1B;

}
}
}
#endif
