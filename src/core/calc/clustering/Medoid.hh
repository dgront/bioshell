/** @file Medoid.hh
 * @brief Provides Medoid class and functions that select a medoid for a given cluster
 */
#ifndef CORE_CALC_CLUSTERING_Medoid_HH
#define CORE_CALC_CLUSTERING_Medoid_HH

#include <limits>       // for max double value constant
#include <vector>
#include <memory>       // for shared_ptr and static_pointer_cast

#include <core/calc/clustering/DistanceByValues.hh>
#include <core/calc/clustering/HierarchicalCluster.hh>

namespace core {
namespace calc {
namespace clustering {

/** @brief Medoid of a cluster.
 *
 * Medoid is a "central" element of a cluster.
 *
 * @tparam T - type used to store distance values, such a double, float, int etc.
 * @tparam E - type of the element data
 * @tparam D - distance function object type
 */
template<typename T,typename E,typename D>
class Medoid {
  template<class A,class B,class C>
  friend Medoid<A,B,C> medoid_by_average_distance(const std::shared_ptr<HierarchicalCluster<A,B> > cluster_root, const C & distance_function);

public:
  const E & medoid; ///< the medoid element
  const T max_distance; ///< maximum distance within a cluster
  const double average_distance; ///< average distance within a cluster
  const double stdev_distance; ///< standard deviation of a distance between cluster elements
  const int id;
private:
  Medoid(const E &element,const T max,const double mean,const double stdev,const int id) : medoid(element),max_distance(max),
    average_distance(mean),stdev_distance(stdev), id(id) {}
  static Medoid<T,E,D> create_medoid(std::shared_ptr<BinaryTreeNode< E> >m,const std::vector<std::shared_ptr<BinaryTreeNode<E> > > &nodes,const D & distance_function);
};

/** @brief Creates a medoid object.
 *
 * @tparam T - type used to store distance values, such a double, float, int etc.
 * @tparam E - type of the element data
 * @tparam D - distance function object type
 * @param m  - tree node selected as the medoid
 * @param nodes - nodes of the cluster
 * @param distance_function - function that calculates distance between cluster members
 * @return a medoid object
 */
template<class T,class E,class D>
Medoid<T,E,D> Medoid<T,E,D>::create_medoid(std::shared_ptr<BinaryTreeNode< E> >m,const std::vector<std::shared_ptr<BinaryTreeNode<E> > > &nodes,const D & distance_function) {

  double sum2 = 0;
  double sum = 0;
  T max = 0;
  for(const std::shared_ptr<BinaryTreeNode< E> > & i: nodes) {
	  if(i==m) continue;
    const T t = distance_function(m->id,i->id);
    if(max < t) max = t;
    sum +=t;
    sum2 += t*t;
  }
  sum /= (double) nodes.size();
  sum2 = sqrt(sum2/(double) nodes.size() - sum*sum);

  return Medoid<T, E, D>(m->element,max,sum,sum2, m->id);
}

/** @brief Select a medoid object that minimizes sum of distances to other members of its cluster.
 *
 * This function selects a cluster member to be a medoid of this cluster by minimising sum of distances.
 * The medoid is a cluster element \f$ c_i \f$ for which the value:
 * \[
 * \sum_{j=0}^{N-1} d(c_i,c_j)
 * \]
 * is minimal. \f$ d() \f$ is the distance between cluster members
 *
 * @tparam T - type used to store distance values, such a double, float, int etc.
 * @tparam E - type of the element data
 * @tparam D - distance function object type
 * @param cluster_root - a cluster
 * @param distance_function - function that calculates distance between cluster members
 * @return a medoid object
 */
template<typename T, typename E, typename D>
Medoid<T,E,D>  medoid_by_average_distance(
  const std::shared_ptr<HierarchicalCluster<T, E> > cluster_root,
  const D & distance_function) {

  std::vector<std::shared_ptr<BinaryTreeNode<E> > >nodes;
  std::shared_ptr<BinaryTreeNode<E> > node_root = std::static_pointer_cast<BinaryTreeNode<std::string> >(cluster_root);
  collect_leaf_nodes(node_root, nodes);
  double min_dist = std::numeric_limits<double>::max();
  double sum = 0;
  std::shared_ptr<BinaryTreeNode< E> > min_node;
  for(const std::shared_ptr<BinaryTreeNode< E> > & i: nodes) {
    sum = 0;
    for(const std::shared_ptr<BinaryTreeNode< E> > & j: nodes) sum += distance_function(i->id,j->id);
    if(sum<min_dist) {
      min_dist = sum;
      min_node = i;
    }
  }

  return Medoid<T,E,D>::create_medoid(min_node,nodes,distance_function);
}

/** @brief Select a medoid object according to min-max criterion.
 *
 * This function selects a cluster member to be a medoid of this cluster by minimising the maximum distance.
 * The medoid is a cluster element \f$ c_i \f$ for which the value:
 * \[
 * \max_{c_i} d(c_i,c_j)
 * \]
 * is minimal over all cluster members \f$ c_j \f$ . \f$ d() \f$ is the distance between cluster members
 *
 * @tparam T - type used to store distance values, such a double, float, int etc.
 * @tparam E - type of the element data
 * @tparam D - distance function object type
 * @param cluster_root - a cluster
 * @param distance_function - function that calculates distance between cluster members
 * @return a medoid object
 */
 template<typename T, typename E, typename D>
Medoid<T,E,D>  medoid_by_minmax_distance(
  const std::shared_ptr<HierarchicalCluster<T, E> > cluster_root,
  const D & distance_function) {

  std::vector<std::shared_ptr<BinaryTreeNode<E> > >nodes;
  std::shared_ptr<BinaryTreeNode<E> > node_root = std::static_pointer_cast<BinaryTreeNode<std::string> >(cluster_root);
  collect_leaf_nodes(node_root, nodes);
  double min_dist = std::numeric_limits<double>::max();
  double min_max = 0;
  std::shared_ptr<BinaryTreeNode< E> > min_node;
  for(const std::shared_ptr<BinaryTreeNode< E> > & i: nodes) {
    double max = 0;
    for(const std::shared_ptr<BinaryTreeNode< E> > & j: nodes) {
      max = distance_function(i->id,j->id);
      if(max<min_dist) min_max = max;
    }
    if(min_max<min_dist) {
      min_dist = min_max;
      min_node = i;
    }
  }

  return Medoid<T,E,D>::create_medoid(min_node,nodes,distance_function);
}

}
}
}

#endif
