#ifndef CORE_CALC_CLUSTERING_HierarchicalCluster_HH
#define CORE_CALC_CLUSTERING_HierarchicalCluster_HH

#include <string>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <memory>

#include <core/index.hh>

#include <core/algorithms/trees/BinaryTreeNode.hh>
#include <core/calc/clustering/DistanceByValues.hh>
#include <core/algorithms/trees/algorithms.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace clustering {

using core::algorithms::trees::BinaryTreeNode;

/** @brief A binary node of a hierarchical clustering tree.
 */
template<typename D, typename E>
class HierarchicalCluster : public BinaryTreeNode<E> {
public:
  const D merging_distance;

/** @brief Creates a new clustering node for a given merging distance value
 * @param no - node id staring from zero
 * @param elem - if this node is a singleton, place the data here
 * @param merging_distance - the merging distance value; if this node is a singleton, use zero
 */
  HierarchicalCluster(int no, const E &elem, D merging_distance) :
      BinaryTreeNode<E>(no, elem), merging_distance(merging_distance) {}

  static std::vector<E> cluster_items(std::shared_ptr<HierarchicalCluster<D, E>> ci);
};

template<typename D, typename E>
std::vector<E> HierarchicalCluster<D, E>::cluster_items(std::shared_ptr<HierarchicalCluster<D, E>> ci) {

  std::vector<E> elements;
  auto bi = std::static_pointer_cast<core::algorithms::trees::BinaryTreeNode<E>>(ci);
  core::algorithms::trees::collect_leaf_elements(bi,elements);

  return elements;
}

template<typename T, typename E>
std::ostream &operator<<(std::ostream &out, const HierarchicalCluster<T, E> &ci) {

  if (ci.has_left()) {
    if (ci.has_right()) {
      out << utils::string_format("%4d %4d > %4d @ ", ci.get_left()->id, ci.get_right()->id, ci.id);
      if (std::is_same<core::index1, T>::value)
        out << int(ci.merging_distance);
      else
        out << ci.merging_distance;
    } else {
      out << utils::string_format("%4d      > %4d @ ", ci.get_left()->id, ci.id);
      if (std::is_same<core::index1, T>::value)
        out << int(ci.merging_distance);
      else
        out << ci.merging_distance;
    }
  } else if (ci.has_right()) {
    out << utils::string_format("     %4d > %4d @ ", ci.get_right()->id, ci.id);
    if (std::is_same<core::index1, T>::value)
      out << int(ci.merging_distance);
    else
      out << ci.merging_distance;
  } else {
    out << utils::string_format("          > %4d @ ", ci.id);
    if (std::is_same<core::index1, T>::value)
      out << int(ci.merging_distance);
    else
      out << ci.merging_distance;
  }
  return out;
}

}
}
}

#endif
