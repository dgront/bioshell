/** \file analyse_clustering.hh
 * @brief Methods to analyse results of hierarchical clustering
 */

#ifndef CALC_CLUSTERING_analyse_clustering_HH
#define CALC_CLUSTERING_analyse_clustering_HH

#include <memory>

#include <core/calc/clustering/HierarchicalCluster.hh>
#include <core/calc/clustering/HierarchicalClustering.hh>

namespace core {
namespace calc {
namespace clustering {

/** @brief Collects statistics on how the clusters grow during the clustering process
 *
 * @tparam T - the type used for distance values
 * @tparam E - the type of the data stored on the tree
 * @param hac - the object provides clustering data
 * @param sink - where to write the statistics
 * @param how_often - when equal to 1, each merging event results in a separate row of output.
 * Otherwise, every n-th line is recorded
 */
template<typename T, typename E>
void growing_clusters_size(const std::shared_ptr<core::calc::clustering::HierarchicalClustering<T, E> > hac,
    std::ostream & sink, const core::index2 how_often = 1) {

  using namespace core::calc::clustering;

  const core::index4 n_data = hac->count_steps() + 1;

  std::vector<core::index4> sizes(n_data, 0);

  for (core::index4 i = 0; i < hac->count_steps(); i++) {
    const std::shared_ptr<HierarchicalCluster<T, E> > step = hac->clustering_step(i);
    sizes[hac->cluster_size(step->id)]++;
    if (step->has_left()) sizes[hac->cluster_size(step->get_left()->id)]--;
    if (step->has_right()) sizes[hac->cluster_size(step->get_right()->id)]--;
    if (step->is_leaf()) continue;
    if (i % how_often == 0) {
      for (core::index4 j = 0; j < sizes.size(); ++j)
        if (sizes[j] > 0) sink << utils::string_format("%5d %5d %5d\n", i, j, sizes[j]);
    }
  }
}

}
}
}
#endif
