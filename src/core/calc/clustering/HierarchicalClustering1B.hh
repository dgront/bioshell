#ifndef CALC_CLUSTERING_HierarchicalClustering1B_HH
#define CALC_CLUSTERING_HierarchicalClustering1B_HH


#include <core/calc/clustering/HierarchicalClustering.hh>

namespace core {
namespace calc {
namespace clustering {

/** @brief HierarchicalClustering  based on 1-byte integer distance values and std::string keys
 */
class HierarchicalClustering1B : public HierarchicalClustering<index1, std::string> {
public:
  HierarchicalClustering1B(const std::vector<std::string> &clustered_tokens, const std::string &empty_label = "") :
      HierarchicalClustering(clustered_tokens, empty_label) {}

  const  std::shared_ptr<HierarchicalClustering> clustering_step(const size_t which_step) const {

    std::shared_ptr<HierarchicalCluster<index1, std::string>> c =
        HierarchicalClustering<index1, std::string>::clustering_step(which_step);

    return nullptr; // a quick and dirty solution for now
  }

};

}
}
}

#endif /* CALC_CLUSTERING_HierarchicalClustering1B_HH */
