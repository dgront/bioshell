#ifndef CALC_CLUSTERING_HierarchicalClustering_HH
#define CALC_CLUSTERING_HierarchicalClustering_HH

#include <algorithm>
#include <vector>
#include <iostream>
#include <memory>
#include <set>
#include <stdexcept>
#include <chrono>
#include <tuple>

#include <core/calc/clustering/DistanceByValues.hh>
#include <core/calc/clustering/HierarchicalCluster.hh>
#include <core/calc/clustering/ClusterMergingRule.hh>
#include <core/calc/clustering/Medoid.hh>
#include <core/algorithms/trees/algorithms.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>
#include <utils/Profiler.hh>
#include <utils/ProfilingManager.hh>

namespace core {
namespace calc {
namespace clustering {

template<typename T, typename E>
class HierarchicalClustering {
public:
  HierarchicalClustering(const std::vector<E> &clustered_tokens, const E &empty_label) :
    clusters_size(clustered_tokens.size() * 2), clustered_tokens(clustered_tokens), empty_label(empty_label) {}

  void run_clustering(DistanceByValues<T> &distances, const std::string & merging_rule);

  void run_clustering(DistanceByValues<T> &distances, const ClusterMergingRule<T> &merging_rule);

  inline size_t count_steps() const { return merging_order.size(); }

  inline size_t count_objects() const { return clustered_tokens.size(); }

  const std::shared_ptr<HierarchicalCluster<T, E> > clustering_step(const size_t which_step) const {

    if (which_step < count_steps())
      return merging_order[which_step];
    else
      throw std::out_of_range(
        utils::string_format("Clustering step index %d too high (above %d", which_step, count_steps()));
  }

  const inline std::shared_ptr<HierarchicalCluster<T, E> > get_root() { return merging_order[merging_order.size() - 1]; }

  inline core::index4 cluster_size(core::index4 const cluster_index) const { return clusters_size[cluster_index]; }

  std::vector<std::shared_ptr<HierarchicalCluster<T, E> > > get_clusters(const T max_merging_distance, core::index4 min_size);

  void write_merging_steps(std::ostream &out_stream) const;

  void write_merging_steps_data(std::ostream &out_stream) const;

  void write_merging_steps_medoids(std::ostream &out_stream, const DistanceByValues<T> &distances) const;

  static std::shared_ptr<HierarchicalClustering<T, E> > from_file(const std::string &fname, E empty_label);

  static std::shared_ptr<HierarchicalClustering<T, E> > from_stream(std::istream &input, E empty_label);

private:
  std::vector<std::shared_ptr<HierarchicalCluster<T, E> > > merging_order;
  std::vector<core::index4> clusters_size;
  std::vector<E> clustered_tokens;
  const E &empty_label;
  static utils::Logger l;

  inline std::string merging_line(const HierarchicalCluster<T, E> &ci) const;
};


template<typename T, typename E>
utils::Logger HierarchicalClustering<T, E>::l = utils::Logger("HierarchicalClustering");

template<typename T, typename E>
void HierarchicalClustering<T, E>::run_clustering( DistanceByValues<T> &distances, const std::string & merging_rule) {

  if(merging_rule=="SINGLE_LINK") {
    SingleLink<T> merge;
    run_clustering(distances, merge);
    return;
  }
  if(merging_rule=="COMPLETE_LINK") {
    CompleteLink<T> merge;
    run_clustering(distances, merge);
    return;
  }
  if(merging_rule=="AVERAGE_LINK") {
    AverageLink<T> merge;
    run_clustering(distances, merge);
    return;
  }

  throw std::invalid_argument(std::string("Unknown merging rule name: ") + merging_rule);
}

template<typename T, typename E>
void HierarchicalClustering<T, E>::run_clustering(
  DistanceByValues<T> &distances, const ClusterMergingRule<T> &merging_rule) {

  if (distances.n_data() != clustered_tokens.size()) {
    std::string msg
        = utils::string_format(
            "The number of tokens subjected to clustering %d does not match the size of distance matrix %d!\n",
            clustered_tokens.size(), distances.n_data());
    l << utils::LogLevel::CRITICAL << msg;
    throw std::out_of_range(msg);
  }

  l << utils::LogLevel::INFO << "Clustering " << distances.n_data() << " items using " << merging_rule.name() << " strategy\n";
  auto start = std::chrono::high_resolution_clock::now();

  //---------- Create singletons
  int cluster_id = 0;
  std::set<int> clusters_to_merge;
  std::vector<int> cluster_to_distmatrix(distances.n_data() * 2);
  std::vector<int> distmatrix_to_cluster(distances.n_data());
  for (size_t i = 0; i < distances.n_data(); i++) {
    std::shared_ptr<HierarchicalCluster<T, E> > ci =
      std::make_shared<HierarchicalCluster<T, E> >(cluster_id, clustered_tokens[i], 0.0);
    merging_order.push_back(ci);
    clusters_size[cluster_id] = 1;
    clusters_to_merge.insert(cluster_id);
    cluster_to_distmatrix[cluster_id] = cluster_id;
    distmatrix_to_cluster[cluster_id] = cluster_id;
    cluster_id++;
  }
  std::shared_ptr<utils::Profiler> profiler_min_dist =
    utils::ProfilingManager::get().get_profiler(
      "HierarchicalClustering::run_clustering  min_distance");

  index4 report_every = std::max(1ul, clusters_to_merge.size() / 100);
  while (clusters_to_merge.size() > 1) {
    l << utils::LogLevel::INFO << "Step: " << merging_order.size()<<" left: "<<clusters_to_merge.size() << "\n";
    double percent = double(distances.n_data() - clusters_to_merge.size() + 1) / distances.n_data() * 100.0;
    if (merging_order.size() % report_every==0)
      l << utils::LogLevel::INFO << percent << " percent done\n";

//---------- Find the smallest distance
    profiler_min_dist->start();
    std::tuple<int, int, float> min_loc = distances.min();

#ifdef DEBUG
    float d_calc = distances.get(std::get<0>(min_loc),std::get<1>(min_loc));
    float d_ret = std::get<2>(min_loc);
    if(d_calc!=d_ret) {
      std::string msg = utils::string_format(
          "minimum distance %f differs fom the actual distance between the items %f! The difference is: %f\n",
          d_ret, d_calc, d_calc - d_ret);
      EXIT(msg);
    }
#endif
    profiler_min_dist->stop();

    if (std::get<2>(min_loc) == distances.max_value) {
      l << utils::LogLevel::WARNING << "maximum distance = "
        << ((std::is_same<core::index1, T>::value) ? int(distances.max_value) : distances.max_value)
        << " reached before the end of clustering, "
        << (clusters_to_merge.size() - 1) << " steps remained\n Clusters left:";
      for (int i:clusters_to_merge) l << i << " ";
      l << "\n";
      break;
    }
//---------- Merge clusters_to_merge
    size_t i_left = std::get<0>(min_loc); // --- indexes of the two clusters to be merged
    size_t i_right = std::get<1>(min_loc);
    if (i_left > i_right) { // --- index of the left one must be lower; swap if it is not the case
      size_t t = i_left;
      i_left = i_right;
      i_right = t;
    }
    std::shared_ptr<HierarchicalCluster<T, E> > c_left = merging_order[distmatrix_to_cluster.at(i_left)];
    std::shared_ptr<HierarchicalCluster<T, E> > c_right = merging_order[distmatrix_to_cluster.at(i_right)];

    if (std::is_same<core::index1, T>::value) {
      l << utils::LogLevel::FINE << utils::string_format("Merging %5d with %5d d = %d, step %5d\n",
         c_left->id, c_right->id, int(std::get<2>(min_loc)), (cluster_id - distances.n_data()));
    } else {
      l << utils::LogLevel::FINE << utils::string_format("Merging %5d with %5d d = %8.3f, step %5d\n",
         c_left->id, c_right->id, std::get<2>(min_loc), (cluster_id - distances.n_data()));
    }
    std::shared_ptr<HierarchicalCluster<T, E> > ci(
      new HierarchicalCluster<T, E>(cluster_id, empty_label,
        std::get<2>(min_loc)));
    clusters_size[cluster_id] = clusters_size[c_left->id] + clusters_size[c_right->id];
    merging_order.push_back(ci);
    ci->set_left_right(c_left, c_right);
    clusters_to_merge.erase(clusters_to_merge.find(i_right));
    cluster_to_distmatrix[cluster_id] = i_left;
    distmatrix_to_cluster[i_left] = cluster_id;
    cluster_id++;

//---------- update distances
    for (const int p : clusters_to_merge) {
      const std::shared_ptr<HierarchicalCluster<T, E> > cj =
        merging_order[distmatrix_to_cluster[p]];
      size_t j_index = cluster_to_distmatrix[cj->id];
      T val = merging_rule(0, 0, 0, 0, distances.get(i_left, j_index), distances.get(i_right, j_index));

      distances.set(i_left, j_index, val);
      distances.set(j_index, i_left, val);
    }
    distances.set(i_left, i_left, distances.max_value);
    distances.exclude_column(i_right);
    distances.exclude_row(i_right);
  }

  //---------- show timer stats
  auto end = std::chrono::high_resolution_clock::now();
  l << utils::LogLevel::INFO << "Clustered after "
    << (double) (std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()) << " [ms]\n";
}

template<typename T, typename E>
std::vector<std::shared_ptr<HierarchicalCluster<T, E> > > HierarchicalClustering<T, E>::get_clusters(const T max_merging_distance,
                      core::index4 min_size) {

  std::vector<std::shared_ptr<HierarchicalCluster<T, E> > > clusters;
  std::vector<std::shared_ptr<HierarchicalCluster<T, E> >> stack;
  stack.push_back(get_root());

  l << utils::LogLevel::FINE << "Retrieving clusters of minimum size: " << min_size << "\n";
  while (stack.size() > 0) {

    std::shared_ptr<HierarchicalCluster<T, E> > c = stack.back();
    stack.pop_back();

    if (c->has_left()) {
      std::shared_ptr<BinaryTreeNode<E> > l_node = c->get_left();
      std::shared_ptr<HierarchicalCluster<T, E> > l_cluster(std::static_pointer_cast<HierarchicalCluster<T, E>>(l_node));

      l << utils::LogLevel::FINER << "considering left node of " << c->id << " : "
        << float(c->merging_distance) << " " << float(l_cluster->merging_distance) << "\n";

      if ((l_cluster->merging_distance <= max_merging_distance) && (c->merging_distance > max_merging_distance)) {
        if (core::algorithms::trees::count_leaves(l_node) >= min_size) {
          l << utils::LogLevel::FINER << "cluster " << c->id << " of size " << core::algorithms::trees::size(l_node) << "\n";
          clusters.push_back(l_cluster);
        }
      } else {
        stack.push_back(l_cluster);
        l << utils::LogLevel::FINER << "node " << l_cluster->id << " put on stack\n";
      }
    }
    if (c->has_right()) {
      std::shared_ptr<BinaryTreeNode<E> > r_node = c->get_right();
      std::shared_ptr<HierarchicalCluster<T, E> > r_cluster(std::static_pointer_cast<HierarchicalCluster<T, E>>(r_node));

      l << utils::LogLevel::FINER << "considering right node of " << c->id << " : "
        << float(c->merging_distance) << " " << float(r_cluster->merging_distance) << "\n";

      if ((r_cluster->merging_distance <= max_merging_distance) && (c->merging_distance > max_merging_distance)) {
        if (core::algorithms::trees::count_leaves(r_node) >= min_size)
          clusters.push_back(r_cluster);
      } else {
        stack.push_back(r_cluster);
        l << utils::LogLevel::FINER << "node " << r_cluster->id << " put on stack\n";
      }
    }
  }

  return clusters;
}

template<typename T, typename E>
void HierarchicalClustering<T, E>::write_merging_steps(std::ostream &out_stream) const {

  for (const std::shared_ptr<HierarchicalCluster<T, E> > ci : merging_order) {
    if (ci->is_leaf())
      out_stream << *ci << " : " << ci->element << "\n";
    else
      out_stream << *ci << "\n";
  }
}

template<typename T, typename E>
void HierarchicalClustering<T, E>::write_merging_steps_data(std::ostream &out_stream) const {

  std::vector<E> elements;
  for (const std::shared_ptr<HierarchicalCluster<T, E> > ci : merging_order) {

    out_stream << *ci;
    elements.clear();
    const std::shared_ptr<BinaryTreeNode<std::string>> &step =
      static_cast<const std::shared_ptr<BinaryTreeNode<std::string>> &>(ci);
    core::algorithms::trees::collect_leaf_elements(step, elements);
    out_stream << " :";
    for (const E &e : elements) out_stream << " " << e;
    out_stream << "\n";
  }
}

template<typename T, typename E>
void HierarchicalClustering<T, E>::write_merging_steps_medoids(
  std::ostream &out_stream, const DistanceByValues<T> &distances) const {

  std::vector<E> elements;
  for (const std::shared_ptr<HierarchicalCluster<T, E> > ci : merging_order) {
    out_stream << *ci;
    elements.clear();
    out_stream << " : " <<
        medoid_by_average_distance<float, std::string, DistanceByValues<float> >(ci, distances).medoid << "\n";
  }
}

template<typename T, typename E>
std::shared_ptr<HierarchicalClustering<T, E> > HierarchicalClustering<T, E>::from_file(
  const std::string &fname, E empty_label) {

  std::ifstream infile(fname);
  l << utils::LogLevel::FILE << "reading a clustering tree from: " << fname << "\n";

  if (!infile) {
    l << utils::LogLevel::WARNING
      << "Cannot read the input clustering three form file: " << fname << " !\n";
  }

  return from_stream(infile, empty_label);
}

template<typename T, typename E>
std::shared_ptr<HierarchicalClustering<T, E> > HierarchicalClustering<T, E>::from_stream(
  std::istream &input, E empty_label) {

  std::string line;
  char entry[1024];
  core::index4 cluster_id;
  core::index4 cluster_id_left, cluster_id_rigth;
  double merging_distance;

  std::vector<std::string> tokens;
  std::vector<std::tuple<core::index4, core::index4, double> > steps;
  try {
    while (std::getline(input, line)) {
      if (input.fail()) break;
      if (line.length() < 2) continue;
      if (line[0] == '#') continue;
      utils::trim(line);
      if (line[0] == '>') { /// Singletons come here
        const int nl = sscanf(line.c_str(), "%*c %u %*c %*f %*c %s", &cluster_id, entry);
        if (nl < 2) {
          l << utils::LogLevel::WARNING << "Failed to read 2 tokens from the line: " << line << "\n";
          continue;
        }
        tokens.push_back(entry);
      } else { /// Here we have clusters as merging steps
        const int nl = sscanf(line.c_str(), "%u %u %*c %u %*c %lf", &cluster_id_left, &cluster_id_rigth, &cluster_id, &merging_distance);
        if (nl < 3) {
          l << utils::LogLevel::WARNING << "Failed to read 3 tokens from the line: " << line << "\n";
          continue;
        }
        steps.push_back(std::make_tuple(cluster_id_left, cluster_id_rigth, merging_distance));
      }
    }
  } catch (const std::ios_base::failure &e) {
    if (input.eof()) l << utils::LogLevel::INFO << "Caught std::ios_base::failure while reading EOF\n";
    else l << utils::LogLevel::WARNING << "Caught std::ios_base::failure!\n";
  }

  l << utils::LogLevel::INFO << "Found " << tokens.size() << " clustered tokens in an input stream\n";
  std::shared_ptr<HierarchicalClustering<T, E> > clustering = std::make_shared<HierarchicalClustering<T, E> >(tokens, empty_label);

  cluster_id = 0;
  for (std::string token : tokens) {
    std::shared_ptr<HierarchicalCluster<T, E> > ci = std::make_shared<
      HierarchicalCluster<T, E> >(cluster_id, token, 0.0);
    clustering->merging_order.push_back(ci);
    clustering->clusters_size[cluster_id] = 1;
    cluster_id++;
  }
  for (auto step : steps) {
    std::shared_ptr<HierarchicalCluster<T, E> > ci =
      std::make_shared<HierarchicalCluster<T, E> >(cluster_id, empty_label, std::get<2>(step));
    clustering->merging_order.push_back(ci);
    ci->set_left(clustering->merging_order[std::get<0>(step)]);
    ci->set_right(clustering->merging_order[std::get<1>(step)]);
    clustering->clusters_size[cluster_id] =
      clustering->clusters_size[std::get<0>(step)] + clustering->clusters_size[std::get<1>(step)];
    cluster_id++;
  }

  return clustering;
}
}
}
}

#endif /* CALC_CLUSTERING_HierarchicalClustering_HH */
