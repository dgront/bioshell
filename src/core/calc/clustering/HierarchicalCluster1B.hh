#ifndef CALC_CLUSTERING_HierarchicalCluster1B_HH
#define CALC_CLUSTERING_HierarchicalCluster1B_HH


#include <core/calc/clustering/HierarchicalCluster.hh>

namespace core {
namespace calc {
namespace clustering {

using core::algorithms::trees::BinaryTreeNode;

/** @brief A binary node of a hierarchical clustering tree.
 */
class HierarchicalCluster1B : public HierarchicalCluster<index1, std::string> {
public:

  HierarchicalCluster1B(int no, const std::string &elem, index1 merging_distance) :
      HierarchicalCluster(no, element, merging_distance) {}

  HierarchicalCluster1B(const HierarchicalCluster<index1, std::string> & ci);
};

typedef std::shared_ptr<HierarchicalCluster1B> HierarchicalCluster1B_SP;

}
}
}

#endif
