/** @file greedy_clustering.hh
 *
 * Quickly divides \f$N\f$ objects into \f$k\f$ clusters based on a given distance operator.
 */
#ifndef CORE_CALC_CLUSTERING_greedy_clustering_HH
#define CORE_CALC_CLUSTERING_greedy_clustering_HH

#include <vector>
#include <chrono>
#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace clustering {

/** @brief Quickly divides \f$N\f$ objects into \f$k\f$ clusters in \f$\mathcal{O}(Nk)\f$ time.
 *
 * This method goes through all the elements from <code>elements</code> vector and attempts to assign them
 * into one of already created clusters. An element is assigned to the closest cluster (i.e. to the cluster
 * whose representative element minimizes the distance <code>distance_op()</code>) if and only if
 * the minimal distance is below the given cutoff. Otherwise that element forms a new cluster and becomes its representative element.
 *
 * At the very beginning the very first element (i.e. <code>elements[0]</code>) forms the very first cluster
 * and becomes its representative.
 *
 * @param elements - a vector of elements to be clustered; the size of that vector is \f$N\f$
 * @param distance_op - distance operator which must take two integer indexes <code>i</code> and <code>j</code>
 * as arguments and return the distance between <code>elements[i]</code> and <code>elements[j]</code>
 * @param cutoff - critical distance value to control when a new cluster will be created
 * @param clusters - holds indexes of cluster representatives; when this call is finished, the size of that vector is \f$k\f$
 * @param cluster_assignments - for each element holds an indexes of a cluster that element belongs to; when this call is finished, the size of that vector is \f$N\f$
 *
 * The following example withdraws 20 random numbers from two different normal distributions (10 from each of them)
 * and then attempts to divide these values into clusters. Note, how a distance operator is defined:
 * \include ex_greedy_clustering.cc
 */
template<typename T, typename Op, typename D>
void greedy_clustering(std::vector<T> & elements, Op & distance_op, const D cutoff, std::vector<size_t> & clusters,
                       std::vector<size_t> & cluster_assignments, const core::index2 num_threads = 1) {

  D distance_not_in_cluster = cutoff * 1000;
  std::vector<std::thread> t(num_threads);
  utils::Logger l("greedy_clustering");
  auto start = std::chrono::system_clock::now();
  size_t n_elements_per_tick = elements.size() / 20;

  cluster_assignments.push_back(0);
  clusters.push_back(0);
  std::vector<size_t> best_c(num_threads); // --- Holds the index of the best cluster assigned to a given element (These are not element indexes!!)
  std::vector<D> best_d(num_threads);
  for (size_t element_i = 1; element_i < elements.size(); ++element_i) {

    std::fill(best_d.begin(), best_d.end(), distance_not_in_cluster);

    size_t clusters_per_thread = clusters.size() / num_threads;
    if (clusters_per_thread > 0) {
      for (size_t i = 0; i < num_threads; ++i) {
        size_t i_thrd  = i;
        t[i_thrd] = std::thread([i_thrd,element_i,clusters_per_thread,distance_not_in_cluster,&clusters,&distance_op,&best_d,&best_c]() {

          size_t thread_best_id = 0;
          D thread_best_distance = distance_not_in_cluster;
          for (size_t ii = i_thrd * clusters_per_thread; ii < (i_thrd + 1) * clusters_per_thread; ++ii) {
            D dist = distance_op(clusters[ii], element_i);

            if (dist < thread_best_distance) {
              thread_best_distance = dist;
              thread_best_id = ii;
            }
          }
          best_d[i_thrd] = thread_best_distance;
          best_c[i_thrd] = thread_best_id;
        });
      }
      for (int i = 0; i < num_threads; ++i) t[i].join();
    }

    // ---------- Reduction step : select the best of all best from threads
    D best_distance = best_d[0];
    size_t best_id = best_c[0];
    for (size_t i_thread = 1; i_thread < num_threads; ++i_thread) { // should be from 1 !!!
      if (best_d[i_thread] < best_distance) {
        best_distance = best_d[i_thread];
        best_id = best_c[i_thread];
      }
    }

    for (size_t i_cluster = clusters_per_thread*num_threads; i_cluster < clusters.size(); ++i_cluster) {
      D dist = distance_op(clusters[i_cluster], element_i);
      if (dist < best_distance) {
        best_distance = dist;
        best_id = i_cluster;
      }
    }

    if (best_distance < cutoff) {
      cluster_assignments.push_back(best_id);
    }
    else {
      cluster_assignments.push_back(clusters.size());
      clusters.push_back(element_i);
    }
    if ((elements.size() > 20) && (element_i % n_elements_per_tick == 0) && (element_i != 0)) {
      std::chrono::duration<double> elapsed_seconds = std::chrono::system_clock::now()-start;
      l << element_i<<" done after " << elapsed_seconds.count() << "[s], "<<clusters.size()<<" clusters created so far\n";
    }
  }
}


//template<typename T, typename Op, typename D>
//void greedy_clustering(std::vector<T> & elements, Op & distance_op, const D cutoff, std::vector<size_t> & clusters,
//    std::vector<size_t> & cluster_assignments) {
//
//  utils::Logger l("greedy_clustering");
//  auto start = std::chrono::system_clock::now();
//  size_t n_elements_per_tick = elements.size() / 20;
//
//  cluster_assignments.push_back(0);
//  clusters.push_back(0);
//
//  for (size_t element_i = 1; element_i < elements.size(); ++element_i) {
//    size_t cluster_i = clusters.size();
//    size_t best_id = cluster_i - 1;
//    D best_distance = distance_op(clusters[best_id], element_i);
//
//    while (cluster_i > 0) {
//      --cluster_i;
//      D dist = distance_op(clusters[cluster_i], element_i);
//      if (best_distance > dist) {
//        best_distance = dist;
//        best_id = cluster_i;
//      }
//    }
//
//    if (best_distance < cutoff) cluster_assignments.push_back(best_id);
//    else {
//      cluster_assignments.push_back(clusters.size());
//      clusters.push_back(element_i);
//    }
//    if ((elements.size() > 20) && (element_i % n_elements_per_tick == 0) && (element_i != 0)) {
//      std::chrono::duration<double> elapsed_seconds = std::chrono::system_clock::now()-start;
//      l << element_i<<" done after " << elapsed_seconds.count() << "[s], "<<clusters.size()<<" clusters created so far\n";
//    }
//  }
//}


/** @brief Quickly divides \f$N\f$ objects into \f$k\f$ clusters in \f$\mathcal{O}(Nk)\f$ time.
 *
 * This method goes through all the elements from <code>elements</code> vector and attempts to assign them
 * into one of already created clusters. An element is assigned to the closest cluster (i.e. to the cluster
 * whose representative element minimizes the distance <code>distance_op()</code>) if and only if
 * the minimal distance is below the given cutoff. Otherwise that element forms a new cluster and becomes its representative element.
 *
 * At the very beginning the very first element (i.e. <code>elements[0]</code>) forms the very first cluster
 * and becomes its representative.
 *
 * @param elements - a vector of elements to be clustered; the size of that vector is \f$N\f$
 * @param distance_op - distance operator which must take two integer indexes <code>i</code> and <code>j</code>
 * as arguments and return the distance between <code>elements[i]</code> and <code>elements[j]</code>
 * @param cutoff - critical distance value to control when a new cluster will be created
 * @param less_than_op - operator used to check whether a distance value is less-then a given cutoff. Greater-than
 * operator can be used here; then clustering may be based on scores (the higher - the better) rather than distance values (the smaller, the better)
 * @param clusters - holds indexes of cluster representatives; when this call is finished, the size of that vector is \f$k\f$
 * @param cluster_assignments - for each element holds an indexes of a cluster that element belongs to; when this call is finished, the size of that vector is \f$N\f$
 * @param num_threads - the number of threads working on clustering
 * The following example withdraws 20 random numbers from two different normal distributions (10 from each of them)
 * and then attempts to divide these values into clusters. Note, how a distance operator is defined:
 * \include ex_greedy_clustering.cc
 */
template<typename T, typename Op, typename D,typename LTOp>
void greedy_clustering(std::vector<T> & elements, Op & distance_op, const D cutoff, LTOp & less_than_op,std::vector<size_t> & clusters,
    std::vector<size_t> & cluster_assignments, const core::index2 num_threads = 1) {

  D distance_not_in_cluster = cutoff * 1000;
  if (less_than_op(distance_not_in_cluster, cutoff)) distance_not_in_cluster = cutoff / 1000;
  std::vector<std::thread> t(num_threads);
  utils::Logger l("greedy_clustering");
  auto start = std::chrono::system_clock::now();
  size_t n_elements_per_tick = elements.size() / 20;

  cluster_assignments.push_back(0);
  clusters.push_back(0);
  std::vector<size_t> best_c(num_threads); // --- Holds the index of the best cluster assigned to a given element (These are not element indexes!!)
  std::vector<D> best_d(num_threads);
  for (size_t element_i = 1; element_i < elements.size(); ++element_i) {

    std::fill(best_d.begin(), best_d.end(), distance_not_in_cluster);

    size_t clusters_per_thread = clusters.size() / num_threads;
    if (clusters_per_thread > 0) {
      for (size_t i = 0; i < num_threads; ++i) {
        size_t i_thrd  = i;
        t[i_thrd] = std::thread([i_thrd,element_i,clusters_per_thread,distance_not_in_cluster,&clusters,&distance_op,&less_than_op,&best_d,&best_c]() {

          size_t thread_best_id = 0;
          D thread_best_distance = distance_not_in_cluster;
          for (size_t ii = i_thrd * clusters_per_thread; ii < (i_thrd + 1) * clusters_per_thread; ++ii) {
            D dist = distance_op(clusters[ii], element_i);

            if (less_than_op(dist, thread_best_distance)) {
              thread_best_distance = dist;
              thread_best_id = ii;
            }
          }
          best_d[i_thrd] = thread_best_distance;
          best_c[i_thrd] = thread_best_id;
        });
      }
      for (int i = 0; i < num_threads; ++i) t[i].join();
    }

    // ---------- Reduction step : select the best of all best from threads
    D best_distance = best_d[0];
    size_t best_id = best_c[0];
    for (size_t i_thread = 1; i_thread < num_threads; ++i_thread) { // should be from 1 !!!
      if (less_than_op(best_d[i_thread], best_distance)) {
        best_distance = best_d[i_thread];
        best_id = best_c[i_thread];
      }
    }

    for (size_t i_cluster = clusters_per_thread*num_threads; i_cluster < clusters.size(); ++i_cluster) {
      D dist = distance_op(clusters[i_cluster], element_i);
      if (less_than_op(dist, best_distance)) {
        best_distance = dist;
        best_id = i_cluster;
      }
    }

    if (less_than_op(best_distance, cutoff)) {
      cluster_assignments.push_back(best_id);
    }
    else {
      cluster_assignments.push_back(clusters.size());
      clusters.push_back(element_i);
    }
    if ((elements.size() > 20) && (element_i % n_elements_per_tick == 0) && (element_i != 0)) {
      std::chrono::duration<double> elapsed_seconds = std::chrono::system_clock::now()-start;
      l << element_i<<" done after " << elapsed_seconds.count() << "[s], "<<clusters.size()<<" clusters created so far\n";
    }
  }
}

}
}
}
/**
 * \example ex_greedy_clustering.cc
 */

#endif // ~CORE_CALC_CLUSTERING_greedy_clustering_HH
