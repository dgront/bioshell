#ifndef CORE_CALC_CLUSTERING_DistanceByValues1B_HH
#define CORE_CALC_CLUSTERING_DistanceByValues1B_HH

#include <core/calc/clustering/DistanceByValues.hh>

namespace core {
namespace calc {
namespace clustering {

/** @brief DistanceByValue1B class holds 1 byte distance values.
 */
class DistanceByValues1B : public DistanceByValues<index1> {
public:
  /** @brief Creates a distance object witch simply offers distance values from a given file
 * @param infile - input file with three columns: label label distance_value
 * @param n_data - the number of distinct elements (rank of the distance matrix)
 * @param missing_value - the value used when distance is undefined in the input file;
 *    should be smaller than <code>max_value</code>
 * @param max_value - maximum distance value
 */
  DistanceByValues1B(const std::string & infile, int n_data, const index1 missing_value, const index1 max_value) :
      DistanceByValues<index1>(infile,n_data,missing_value,max_value) {}

  /** @brief Creates a distance matrix for a given set of objects, defined by labels.
   *
   * The matrix by default is filled with <code>missing_value</code>. The size of the matrix is equal to the number of labels
   * @param labels - a label for every object subjected to clustering
   * @param missing_value - value used when no distance is defined;
   *    should be smaller than <code>max_value</code> (254 by default)
   * @param max_value - maximum distance value, 255 by default
   */
  DistanceByValues1B(const std::vector<std::string> &labels, const index1 missing_value = 254, const index1 max_value = 255) :
      DistanceByValues<index1>(labels, missing_value, max_value) {}
};

}
}
}

#endif
