#include <core/calc/clustering/HierarchicalCluster1B.hh>

namespace core {
namespace calc {
namespace clustering {

using core::algorithms::trees::BinaryTreeNode;


HierarchicalCluster1B::HierarchicalCluster1B(const HierarchicalCluster<index1, std::string> &ci) :
    HierarchicalCluster<index1, std::string>(ci.id, ci.element, ci.merging_distance) {

  if (ci.has_left()) {
    std::shared_ptr<HierarchicalCluster<index1, std::string>> cl =
        std::static_pointer_cast<HierarchicalCluster<index1, std::string>>(ci.get_left());
    HierarchicalCluster1B_SP c_left =
        std::make_shared<HierarchicalCluster1B>(cl->id, cl->element, cl->merging_distance);
    set_left(c_left);
  }
  if (ci.has_right()) {
    std::shared_ptr<HierarchicalCluster<index1, std::string>> cr =
        std::static_pointer_cast<HierarchicalCluster<index1, std::string>>(ci.get_right());
    HierarchicalCluster1B_SP c_right =
        std::make_shared<HierarchicalCluster1B>(cr->id, cr->element, cr->merging_distance);
    set_right(c_right);
  }
}

}
}
}
