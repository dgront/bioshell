#ifndef CORE_CALC_CLUSTERING_DistanceByValues_HH
#define CORE_CALC_CLUSTERING_DistanceByValues_HH

#include <string>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <chrono>
#include <map>

#include <core/index.hh>
#include <utils/exit.hh>
#include <utils/Logger.hh>
#include <utils/string_utils.hh>
#include <core/data/basic/Array2D.hh>

namespace core {
namespace calc {
namespace clustering {

using core::data::basic::Array2D;

/** @brief A distance matrix used by hierarchical clustering procedure
 * @tparam T - data type used to represent a distance value
 */
template<class T>
class DistanceByValues : public core::data::basic::Array2D<T> {
public:

  const T missing_value; ///< The value used when the actual value is missing in the input file
  const T max_value; ///< The value used to mark the distance between object that should not be merged into a cluster.

  /** @brief Creates a distance object witch simply offers distance values from a given file
   * @param infile - input file with three columns: label label distance_value
   * @param n_data - the number of distinct elements (rank of the distance matrix)
   * @param missing_value - the value used when distance is undefined in the input file;
   *    should be smaller than <code>max_value</code>
   * @param max_value - maximum distance value
   */
  DistanceByValues(const std::string & infile, int n_data, const T missing_value, const T max_value) :
      Array2D<T>(n_data, n_data),
      missing_value( (missing_value < max_value) ? missing_value : T(max_value-0.001)), max_value(max_value) {

    std::ifstream in(infile);
    create_object(in);
    in.close();
  }

  /** @brief Creates a distance object witch simply offers distance values from a given file
   * @param infile - input stream with three columns: label label distance_value
   * @param n_data - the number of distinct elements (rank of the distance matrix)
   * @param missing_value - the value used when distance is undefined in the input file;
   *    should be smaller than <code>max_value</code>
   * @param max_value - maximum distance value
   */
  DistanceByValues(std::istream & infile, int n_data, const T missing_value, const T max_value) :
      Array2D<T>(n_data, n_data),
      missing_value( (missing_value < max_value) ? missing_value : T(max_value-0.001)), max_value(max_value) {

    create_object(infile);
  }

  /** @brief Creates a distance matrix for a given set of objects, defined by labels.
   *
   * The matrix by default is filled with <code>missing_value</code>. The size of the matrix is equal to the number of labels
   * @param labels - a label for every object subjected to clustering
   * @param missing_value - value used when no distance is defined;
   *    should be smaller than <code>max_value</code>
   * @param max_value - maximum distance value
   */
  DistanceByValues(const std::vector<std::string> & labels, const T missing_value, const T max_value) :
      Array2D<T>(labels.size(), labels.size()),
      missing_value( (missing_value < max_value) ? missing_value : T(max_value-0.001)), max_value(max_value) {

    logger << utils::LogLevel::INFO << "missing value set to " <<
      ((std::is_same<core::index1, T>::value) ? int(missing_value) : missing_value) << "\n";
    std::fill(Array2D<T>::the_data.begin(), Array2D<T>::the_data.end(), missing_value);
    label_4_index.reserve(labels.size());
    for (size_t i = 0; i < labels.size(); i++) {
      label_4_index.push_back(labels[i]);
      index_for_label.insert(std::make_pair(labels[i], i));
    }
    for (const std::string & s : label_4_index) {
      if (s.length() > max_label_length) max_label_length = s.length();
      min_value_4_row.push_back(max_value);
      is_row_min_updated.push_back(false);
      min_index_4_row.push_back(0);
    }
    exclude_diagonal();
  }

  /// Returns the number of  objects subjected to clustering i.e. the size of this matrix
  size_t n_data() const { return label_4_index.size(); }

  size_t n_data_declared() const { return Array2D<T>::n_columns; }

  /** @brief Returns the const-reference to a label assigned to a given object
   * @param which_data - index of an element
   * @return string identifying the requested object
   */
  const std::string& label(const int which_data) const { return label_4_index[which_data]; }

  /** @brief Set distance value between two objects given by their indexes
   * @param row - index of the first object
   * @param column  - index of the second object
   * @param t - the distance value
   */
  void set(const size_t row, const size_t column, T t) {

    Array2D<T>::the_data[Array2D<T>::to1D(row, column)] = t;
    if ((t < min_value_4_row[row]) || (column == min_index_4_row[row])) is_row_min_updated[row] = false;
  }

  /** @brief Find the smallest distance value in the matrix
   * @return the minimum distance value
   */
  std::tuple<size_t, size_t, T> min() {

    if (!is_row_min_updated[0]) update_min(0);
    T min_min = min_value_4_row[0];
    size_t min_idx = min_index_4_row[0];
    size_t min_row = 0;
    for (size_t i = 1; i < min_index_4_row.size(); ++i) {
      if (!is_row_min_updated[i]) update_min(i);
      if (min_value_4_row[i] < min_min) {
        min_min = min_value_4_row[i];
        min_idx = min_index_4_row[i];
        min_row = i;
      }
    }
#ifdef DEBUG
		std::tuple<size_t, size_t, T> true_min = Array2D<T>::min();
    if (std::is_same<core::index1, T>::value) {
      if(std::get<2>(true_min) != min_min) {
        EXIT(
            utils::string_format("inconsistent minimum:\ntrue:  %d at %5d %5d\nfound: %d at %5d %5d",
                                 size_t(std::get<0>(true_min)), size_t(std::get<1>(true_min)),
                                 std::get<2>(true_min), min_row, min_idx,
                                 min_min));
      }
    } else {
      if (fabs(std::get<2>(true_min) - min_min) > 0.000001) {
        EXIT(
            utils::string_format("inconsistent minimum:\ntrue:  %6g at %5d %5d\nfound: %6g at %5d %5d",
                                 std::get<0>(true_min), std::get<1>(true_min),
                                 std::get<2>(true_min), min_row, min_idx,
                                 min_min));
      }
    }
#endif
    return std::tuple<size_t, size_t, T>(min_row, min_idx, min_min);
  }

  /** @brief Prints this matrix into a stream
   * @param out_stream - output stream
   * @param format - printf-style format
   */
  void print(std::ostream &out_stream, const std::string &format = "%s ") const {

    int k = -1;
    const std::string fmat = utils::string_format("%%%ds", max_label_length);
    for (size_t i = 0; i < Array2D<T>::n_rows; i++) {
      out_stream << utils::string_format(fmat, label_4_index[i].c_str());
      for (size_t j = 0; j < Array2D<T>::n_columns; j++)
        out_stream << utils::string_format(format, Array2D<T>::the_data[++k]);
      out_stream << std::endl;
    }
  }

  /// Returns the vector of labels assigned to objects being clustered
  const std::vector<std::string> & labels() const { return label_4_index; }

  /// Set all diagonal elements to <code>max_value</code>
  void exclude_diagonal() { for (size_t i = 0; i < n_data(); i++) Array2D<T>::set(i, i, max_value); }

  /// Set all elements in the given row to <code>max_value</code>
  void exclude_row(size_t row) {

    const size_t start = Array2D<T>::row_starts(row);
    const size_t stop = Array2D<T>::row_ends(row);
    for (size_t i = start; i <= stop; ++i)
      Array2D<T>::the_data[i] = max_value;
    min_value_4_row[row] = max_value;
    is_row_min_updated[row] = true;
  }

  /// Set all elements in the given column to
  void exclude_column(size_t column) {

    size_t i_row = 0;
    for (size_t i = column; i < Array2D<T>::the_data.size(); i += Array2D<T>::n_columns) {
      Array2D<T>::the_data[i] = max_value;
      if (column == min_index_4_row[i_row]) is_row_min_updated[i_row] = false;
      ++i_row;
    }
  }

  /// Returns integer index assigned for a given data label
  size_t at(const std::string &label) { return index_for_label.at(label); }

private:
  std::map<std::string, size_t> index_for_label;
  std::vector<std::string> label_4_index;
  std::vector<size_t> min_index_4_row;
  std::vector<T> min_value_4_row;
  std::vector<char> is_row_min_updated;
  size_t max_label_length = 0;
  static const int MAX = 100;
  static utils::Logger logger;

  void update_min(size_t row);

  core::index4 read_distances(const std::string & fname);
  core::index4 read_distances(std::istream & infile);
  void create_object(std::istream & infile);
};

template<typename T>
utils::Logger DistanceByValues<T>::logger = utils::Logger("DistanceByValues");

/// Reads a <code>double</code> distance value between two items
int scanf_row(const std::string &line, char *name_i, char *name_j, double &val);

/// Reads a <code>float</code> distance value between two items
int scanf_row(const std::string &line, char *name_i, char *name_j, float &val);

/// Reads an <code>int</code> distance value between two items
int scanf_row(const std::string &line, char *name_i, char *name_j, int &val);

/// Reads an <code>index1</code> distance value between two items
int scanf_row(const std::string &line, char *name_i, char *name_j, core::index1 &val);

/// Reads an <code>index2</code> distance value between two items
int scanf_row(const std::string &line, char *name_i, char *name_j, core::index2 &val);

/// Reads an <code>index4</code> distance value between two items
int scanf_row(const std::string &line, char *name_i, char *name_j, core::index4 &val);

}
}
}

#endif
