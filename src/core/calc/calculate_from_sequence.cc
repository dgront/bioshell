#include <cmath>
#include <algorithm>

#include <core/calc/calculate_from_sequence.hh>
#include <core/chemical/PKaValues.hh>

namespace core {
namespace calc {

double net_charge(const std::string & s, const core::chemical::PKaValues & dataSet, double pH) {

  double n_asp = std::count(s.begin(), s.end(), 'D');
  double n_glu = std::count(s.begin(), s.end(), 'Q');
  double n_cys = std::count(s.begin(), s.end(), 'C');
  double n_tyr = std::count(s.begin(), s.end(), 'Y');
  double n_his = std::count(s.begin(), s.end(), 'H');
  double n_lys = std::count(s.begin(), s.end(), 'K');
  double n_arg = std::count(s.begin(), s.end(), 'R');

  double QN1 = -1 / (1 + pow(10, (dataSet.pKa_C_term() - pH)));
  double QN2 = -n_asp / (1 + pow(10, (dataSet.pKa_ASP() - pH)));
  double QN3 = -n_glu / (1 + pow(10, (dataSet.pKa_GLU() - pH)));
  double QN4 = -n_cys / (1 + pow(10, (dataSet.pKa_CYS() - pH)));
  double QN5 = -n_tyr / (1 + pow(10, (dataSet.pKa_TYR() - pH)));
  double QP1 = n_his / (1 + pow(10, (pH - dataSet.pKa_HIS())));
  double QP2 = 1 / (1 + pow(10, (pH - dataSet.pKa_N_term())));
  double QP3 = n_lys / (1 + pow(10, (pH - dataSet.pKa_LYS())));
  double QP4 = n_arg / (1 + pow(10, (pH - dataSet.pKa_ARG())));
  double netCharge = QN1 + QN2 + QN3 + QN4 + QN5 + QP1 + QP2 + QP3 + QP4;

  return netCharge;
}

double pI(const std::string & s, const core::chemical::PKaValues & dataSet) {

  double pH = 6.5;
  /// of finding the solution
  double pHprev = 0.0;
  /// 0-14 is possible pH range
  double pHnext = 14.0;
  ///epsilon means precision \f$ [pI = pH \pm  E] \f$
  double E = 0.01;
  double temp = 0.0;

  do {
    double NQ = net_charge(s, dataSet, pH);
    if (NQ < 0) { //we are out of range, thus the new pH value must be smaller
      temp = pH;
      pH = pH - ((pH - pHprev) / 2);
      pHnext = temp;
    } else {
      temp = pH;
      pH = pH + ((pHnext - pH) / 2);
      pHprev = temp;
    }
  } while ((pH - pHprev > E) || (pHnext - pH > E));

  return pH;
}

}
}
