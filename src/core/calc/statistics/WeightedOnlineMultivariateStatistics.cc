#include <limits>
#include <iostream>

#include <core/calc/statistics/WeightedOnlineMultivariateStatistics.hh>

namespace core {
namespace calc {
namespace statistics {

WeightedOnlineMultivariateStatistics::WeightedOnlineMultivariateStatistics(const core::index2 dim)
    : M1(dim, 0), M2(dim, 0), cov(dim, dim), dim(dim) {}

void WeightedOnlineMultivariateStatistics::operator()(std::vector<double> d, double w) {

  W_ += w;
  cnt_++;
  for (core::index2 i = 0; i < dim; ++i) {
    double delta_x = d[i] - M1[i];
    M1[i] += delta_x * w / W_;  // --- M1[i] is now the new average for i-th dimension
    M2[i] += w * delta_x * (d[i] - M1[i]);

    for (core::index2 j = i+1; j < dim; ++j) {
      double delta_y = d[j] - M1[j];
      cov(i, j) = w * delta_y * (d[i] - M1[i]);
    }
  }
}

}
}
}

