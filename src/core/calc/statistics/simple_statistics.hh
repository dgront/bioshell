/** @file simple_statistics.hh
 * @brief provides some basic statistical methods.
 *
 * Note, that some of these methods have already been implemented in STL, e.g. std::accumulate() or std::min_element()
 *
 */
#ifndef CORE_CALC_STATISTICS_simple_statistics_HH
#define CORE_CALC_STATISTICS_simple_statistics_HH

#include <algorithm>
#include <numeric>
#include <iterator>
#include <cmath>
#include <core/calc/statistics/Random.hh>

#include <core/data/basic/Array2D.hh>

namespace core {
namespace calc {
namespace statistics {

/** @brief Generic method to compute average from a sample.
 *
 * @param begin - points to the first data element in a range
 * @param end - points to behind the last data element in a range
 * @tparam It - iterator type providing the data
 */
template<typename It>
double avg(It begin, It end) {
  double sum = std::accumulate(begin, end, 0.0);
  double cnt = std::distance(begin, end);
  return sum / cnt;
}

/** @brief Generic method to compute standard deviation from a sample.
 *
 * @param begin - points to the first data element in a range
 * @param end - points to behind the last data element in a range
 * @tparam It - iterator type providing the data
 */
template<typename It>
double stdev(It begin, It end) {

  double sum = std::accumulate(begin, end, 0.0);
  double cnt = std::distance(begin, end);
  double m = sum / cnt;

  double accum = 0.0;
  std::for_each(begin, end, [&](const double d) {
      accum += (d - m) * (d - m);
  });

  return sqrt(accum / (cnt - 1));
}

/** @brief Generic method to compute trunctated average from a sample.
 *
 * Truncated (trimmed) mean, also known as Windsor mean is evaluated after removing
 * k-fraction data point from poth tails; i.e. 2*k*N of data points are ingnored
 * when the average is computed 
 * @param begin - points to the first data element in a range
 * @param end - points to behind the last data element in a range
 * @param k - fraction of points to be ignored at each tail (e.g. 0.1)
 * @tparam It - iterator type providing the data
 */
template<typename It>
double avg(It begin, It end, const double k) {

  std::sort(begin, end);
  auto b = begin + k * std::distance(begin, end);
  auto e = end - k * std::distance(begin, end);
  double sum = std::accumulate(b, e, 0.0);
  double cnt = std::distance(b, e);
  return sum / cnt;
}

/** @brief Generic method to compute trunctated standard deviation from a sample.
 *
 * Truncated (trimmed) sdev, also known as Windsor sdev is evaluated after removing
 * k-fraction data point from poth tails; i.e. 2*k*N of data points are ingnored
 * when the sdev is computed 
 * @param begin - points to the first data element in a range
 * @param end - points to behind the last data element in a range
 * @param k - fraction of points to be ignored at each tail (e.g. 0.1)
 * @tparam It - iterator type providing the data
 */
template<typename It>
double stdev(It begin, It end, const double k) {

  std::sort(begin, end);
  auto b = begin + k * std::distance(begin, end);
  auto e = end - k * std::distance(begin, end);
  double sum = std::accumulate(b, e, 0.0);
  double cnt = std::distance(b, e);
  double m = sum / cnt;

  double accum = 0.0;
  std::for_each(b, e, [&](const double d) {
      accum += (d - m) * (d - m);
  });

  return sqrt(accum / (cnt - 1));
}

/** @brief Generic method to compute autocorrelation
 *
 * @param begin - points to the first data element in a range
 * @param end - points to behind the last data element in a range
 * @param out - vector to store results
 * @tparam It - iterator type providing the data
 */
template<typename It>
void autocorrelate(It begin, It end, core::index2 n_steps, std::vector<double> &out) {

  double ave = std::accumulate(begin, end, 0.0) / double(std::distance(begin, end));
  for (core::index2 t_lag = 0; t_lag <= n_steps; ++t_lag) {
    double v = 0;
    It i2 = begin;
    for (It i = begin + t_lag; i != end; ++i) {
      v += (*i - ave) * (*i2 - ave);
      ++i2;
    }
    v /= double(std::distance(begin, end) - t_lag);
    out.push_back(v);
  }
  for (index2 i = 1; i < out.size(); ++i) out[i] /= out[0];
}

/** @brief Calculates a quantile value of a given data sample.
 *
 * Quantile \f$ q_p \f$ at probability level \f$ p \f$ is a value \f$ x \f$ from a given distribution
 * such that probability of withdrawing value not larger than \f$ q_p \f$ from that distribution is equal to \f$ p \f$
 * For example, tossing a regular 6-faced dice has may give 1, 2, 3, 4, 5 or 6 with equal probability.
 * Quantile \f$ q_{1/3} \f$ is equal to 2, because there is 1 in 3 chance  that one gets at most 2 dots.
 * There is also 50% chance of getting either 1, 2 or 3 so \f$ q_{0.5} = 3 \f$
 *
 * @tparam T - type of the input data
 * @param data - the vector containing the input sample
 * @param quantile_level - quantile (probability) value in the range (0,1)
 * @return quantile value
 */
template<typename T>
T quantile(std::vector<T> &data, double quantile_level) {

  std::sort(data.begin(), data.end());
  return data[int(quantile_level * data.size())];
}

/** @brief Generic method to compute autocorrelation
 *
 * @param begin - points to the first data element in a range
 * @param end - points to behind the last data element in a range
 * @param out - vector to store results
 * @tparam It - iterator type providing the data
 */
template<typename T>
void autocorrelate(std::vector<std::vector<T>> &data, core::index2 n_steps, std::vector<double> &out) {

  core::index2 dim = data[0].size(); // --- dimension of each input vector
  std::vector<T> avg(dim); // --- holds an average vector
  for (const std::vector<T> &v : data) { // --- sum up all the vectors
    for (index2 i = 0; i < dim; ++i) avg[i] += v[i];
  }
  for (index2 i = 0; i < dim; ++i) avg[i] /= double(data.size()); // --- divide to compute the average
  for (std::vector<T> &v : data) { // --- subtract the average from each vector
    for (index2 i = 0; i < dim; ++i) v[i] -= avg[i];
  }

  for (core::index2 t_lag = 0; t_lag <= n_steps; ++t_lag) {
    double v = 0;

    core::index4 i_first = 0;
    for (core::index4 i_last = i_first + t_lag; i_last < data.size(); ++i_last) {
      for (index2 i = 0; i < dim; ++i) v += data[i_first][i] * data[i_last][i];
      ++i_first;
    }
    v /= double(data.size() - t_lag);
    out.push_back(v);
  }
  for (index2 i = 1; i < out.size(); ++i) out[i] /= out[0];
}

/** @brief Calculates average displacement from 3D coordinates, e.g. a center-of-mass
 *
 * @param data - 3D vectors
 * @param n_steps - the number of points to be evaluated
 * @param out - vector to store results
 */
void average_displacement(std::vector<core::data::basic::Vec3> &data, core::index2 n_steps, std::vector<double> &out);

/** @brief Calculates \f$\chi^2\f$ value cased on a given contingency matrix .
 *
 * @param contingency_matrix - contingency matrix row-wise; i.e. this function will test independence of <strong>rows</strong>
 */
double chi2_independence_test(const core::data::basic::Array2D<core::index4> &contingency_matrix);

/** @brief Calculates Goodman and Kruskal's \f$\gamma\f$ value which is a measure of rank correlation.
 *
 * @param contingency_matrix - contingency matrix row-wise; i.e. this function will test independence of <strong>rows</strong>
 */
double goodman_kruskal_rank_correlation(const core::data::basic::Array2D<core::index4> &contingency_matrix);

/** @brief Calculates Matthews correlation coefficient -  measure of the quality of binary (two-class) classifications.
 * @param true_pos - the fraction of true positive cases
 * @param true_neg - the fraction of true negative cases
 * @param false_pos - the fraction of false positive cases
 * @param false_neg - the fraction of false negative cases
 */
double matthews_correlation(double true_pos, double true_neg, double false_pos, double false_neg);

/** @brief Calculates covariance between two sets of data.
 *
 * @param i_begin - begin iterator to the first set of points
 * @param j_begin - begin iterator to the second set of points
 * @param n_steps - the number of points - the same for both sets
 * @return covariance value
 */
template<typename It>
double cov(It i_begin, It j_begin, core::index4 n_steps) {
  double c = 0.0;
  for (int k = 0; k < n_steps; ++k) {
    c += (*i_begin) * (*j_begin);
    ++i_begin;
    ++j_begin;
  }

  return c / double(n_steps - 1);
}

/** @brief Linear approximation through a set of (unweighted) points
 * @tparam T - type of data (e.g. double or float)
 * @param x - x coordinates of the points
 * @param y - y coordinates of the points
 * @param from - select a subset of points <code>[from,to]</code>
 * @param to  - select a subset of points <code>[from,to]</code>
 * @return a pair holding \f$ a,b \f$ values of \f$ ax + b \f$ equation defining a line approximating the data
 */
template<typename T>
std::pair<T, T> regression_line(const std::vector<T> &x, const std::vector<T> &y, core::index4 from = 0,
                                core::index4 to = 0);

/** @brief Estimates a quantile value by a multiple bootstrap procedure.
 *
 * This function uses <code>quantile()</code> function for computing a quantile value. Refer to its documentation
 * for an intuitive explanation of a quantile.
 *
 * This function resamples \f$ N \f$ elements from the input sample with repetitions
 * (where \f$ N \f$ is the size of the input sample).
 *
 * @tparam T - type of the input data
 * @param data - amn input data sample (e.g. double or float)
 * @param quantile_level - quantile level
 * @param n_samples - number of bootstrap resampling
 * @return expected value and standard deviation of the quantile value
 */
template<typename T>
std::pair<double, double> bootstrap_quantile(const std::vector<T> &data, double quantile_level, int n_samples);

}
}
}

#endif
