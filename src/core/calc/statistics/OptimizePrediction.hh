#ifndef CORE_CALC_STATISTICS_OptimizePrediction_HH
#define CORE_CALC_STATISTICS_OptimizePrediction_HH

#include <algorithm>
#include <numeric>
#include <iterator>
#include <iostream>
#include <iomanip>
#include <cmath>

#include <core/calc/statistics/simple_statistics.hh>

namespace core {
namespace calc {
namespace statistics {

template<typename P, typename L>
class OptimizePrediction {
public:
  OptimizePrediction(std::vector<P> parameter_values, std::vector<L> labels);

  core::index4 true_positives() const { return tp_; }

  core::index4 true_negatives() const { return tn_; }

  core::index4 false_positives() const { return fp_; }

  core::index4 false_negatives() const { return fn_; }

  P optimal_parameter_value() const { return best_parameter_value; }

  double recent_score() const { return best_score; }

/** @brief Finds the optimal assignemnt of labels based on values of a numerical parameter.
 *
 * @param parameter_values - a parameter value for each observation
 * @param labels - a true label for each observation
 * @param true_label - the true label which is to be assigned based on a numeric value
 * @return the optimal value of the parameter, where the assignment is optimal
 */
  double maximize_matthews_correlation(const L true_label);

private:
  P best_parameter_value;
  double best_score;
  core::index4 tp_, tn_, fp_, fn_;
  std::vector<std::pair<P, L>> labeled_values;
};

template<typename P, typename L>
OptimizePrediction<P,L>::OptimizePrediction(std::vector<P> parameter_values, std::vector<L> labels) {

  // --- repack values and the respective scores into pairs
  for (size_t i = 0; i < labels.size(); ++i) labeled_values.emplace_back(parameter_values[i], labels[i]);

  // --- sort these pairs by score
  std::sort(labeled_values.begin(), labeled_values.end(), [](const std::pair<P, L> &lh, const std::pair<P, L> &rh) { return lh > rh; });
};

template<typename P, typename L>
double OptimizePrediction<P,L>::maximize_matthews_correlation(const L true_label) {

  size_t n_true = std::count_if(labeled_values.begin(), labeled_values.end(),
    [&true_label](const std::pair<P, L> &p) { return p.second == true_label; });

  double max_corr = 0;
  size_t best_i = 0;
  double tp = 0;
  best_score = 0;
  for (size_t i = 0; i < labeled_values.size(); ++i) {
    if (labeled_values[i].second == true_label)
      ++tp; // --- the i-th element is the true-positive if it is labeled accordingly
    double fp = i - tp + 1; // --- all other cases seen so far are false-positives (classified as labeled, but incorrectly)
    double fn = n_true - tp; // --- the number of 'true' cases not seen so far
    double tn = labeled_values.size() - tp - fn - fp; // --- all other cases
    double c = matthews_correlation(tp, tn, fp, fn);
//    std::cerr <<i<<" "<< labeled_values[best_i].first<<" "<<tp << " " << tn << " " << fp << " " << fn << " " << c << "\n";
    if (c > max_corr) {
      max_corr = c;
      best_i = i;
      fn_ = core::index4(fn);
      fp_ = core::index4(fp);
      tn_ = core::index4(tn);
      tp_ = core::index4(tp);
    }
  }
  best_parameter_value = labeled_values[best_i].first;
  best_score = max_corr;

  return max_corr;
};


template<typename P, typename L>
std::ostream & operator<<(std::ostream & out, const OptimizePrediction<P,L> opt) {

  out << opt.recent_score() << " " << opt.optimal_parameter_value() << "\n";
  out << std::setw(5) << opt.true_positives() << " " << std::setw(5) << opt.true_negatives() << "\n";
  out << std::setw(5) << opt.false_positives() << " " << std::setw(5) << opt.false_negatives() << "\n";
  return out;
};


}
}
}

#endif
