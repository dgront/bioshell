#ifndef CORE_CALC_STATISTICS_OnlineStatistics_HH
#define CORE_CALC_STATISTICS_OnlineStatistics_HH

#include <math.h>

#include <core/index.hh>

namespace core {
namespace calc {
namespace statistics {

/** @brief Calculates basic statistics on the fly.
 *
 * The class uses Welford's online algorithm. Just toss some double data in and ask the object for average, variance etc.
 *
 * @see https://www.johndcook.com/blog/skewness_kurtosis/
 *
 * The following example application calculates basic statistics from data read from a file; if no input file
 * is given, a random sample will be used for demonstration:
 *
 * @include ap_OnlineStatistics.cc
 */
class OnlineStatistics {
public:

  /// Starts the calculator
  OnlineStatistics();

  /// adds observations given by an iteration range
  template <typename It>
  void operator()(It begin, It end) { for (It it = begin; it != end; ++it) operator()(*it); }

  /// adds an observation
  void operator()(double d);

  /// Returns the number of observed samples
  unsigned long long cnt() const { return cnt_; }

  /// Returns the minimum value observed so far
  double min() const { return min_; }

  /// Returns the maximum value observed so far
  double max() const { return max_; }

  /// Returns the average of the values observed so far
  double avg() const { return M1; }

  /// Returns the variance of the values observed so far
  double var() const { return M2 / (cnt_ - 1.0); }

  /// Returns the skewness of the values observed so far
  double skewness() const { return sqrt(double(cnt_)) * M3 / pow(M2, 1.5); }

  /// Returns the kurtosis of the values observed so far
  double kurtosis() const { return double(cnt_) * M4 / (M2 * M2); }

  /// Returns the bimodality coefficient estimated from the values observed so far
  double bimodality_coefficient() const;

private:
  unsigned long long cnt_ = 0;
  double M1 = 0.0, M2 = 0.0, M3 = 0.0, M4 = 0.0;
  double min_;
  double max_;
};

}
}
}

#endif // ~ CORE_CALC_STATISTICS_OnlineStatistics_HH
/*
 * @example ap_OnlineStatistics.cc
 */