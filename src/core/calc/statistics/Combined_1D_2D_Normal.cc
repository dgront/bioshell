#include <cmath>
#include <iostream>
#include <algorithm>    // std::sort
#include <vector>
#include <core/calc/statistics/Combined_1D_2D_Normal.hh>

namespace core {
namespace calc {
namespace statistics {

Combined_1D_2D_Normal::Combined_1D_2D_Normal(const std::vector<double> &parameters) :
   PDF(parameters), d1(parameters[0], parameters[3]), 
   d2(parameters[1], parameters[2], parameters[4], parameters[5], parameters[6]) {}

void Combined_1D_2D_Normal::copy_parameters_from(const std::vector<double> &source) {

  for(unsigned int i = 0; i < parameters_.size(); ++i) parameters_[i] = source[i];
  d1.copy_parameters_from(parameters_[0],parameters_[3]);
  d2.copy_parameters_from(std::vector<double>{parameters_[1],parameters_[2],parameters_[4],parameters_[5],parameters_[6]});
}

const std::vector<double> & Combined_1D_2D_Normal::estimate(const std::vector<std::vector<double>> & observations) {

  // ---------- Copy data 1D and estimate NormalDistribution
  std::vector<std::vector<double>> data(observations.size());
  for(auto const& v : observations) {
    std::vector<double> vi{v[0]};
    data.push_back(vi);
  }
  d1.estimate(data);
  parameters_[0] = d1.parameters()[0];
  parameters_[3] = d1.parameters()[1];

  // ---------- Copy data 2D and estimate BivariateNormal
  data.clear();
  for(auto const& v : observations) {
    std::vector<double> vi{v[1], v[2]};
    data.push_back(vi);
  }
  d2.estimate(data);
  parameters_[1] = d2.parameters()[0];
  parameters_[2] = d2.parameters()[1];
  parameters_[4] = d2.parameters()[2];
  parameters_[5] = d2.parameters()[3];
  parameters_[6] = d2.parameters()[4];

  return parameters_;
}

const std::vector<double> & Combined_1D_2D_Normal::estimate(const std::vector<std::vector<double>> & observations,
                                                 const std::vector<double> & weights) {
  // ---------- Copy data 1D and estimate NormalDistribution
  std::vector<std::vector<double>> data(observations.size());
  for(auto const& v : observations) {
    std::vector<double> vi{v[0]};
    data.push_back(vi);
  }
  d1.estimate(data, weights);
  parameters_[0] = d1.parameters()[0];
  parameters_[3] = d1.parameters()[1];

  // ---------- Copy data 2D and estimate BivariateNormal
  data.clear();
  for(auto const& v : observations) {
    std::vector<double> vi{v[1], v[2]};
    data.push_back(vi);
  }
  d2.estimate(data, weights);
  parameters_[1] = d2.parameters()[0];
  parameters_[2] = d2.parameters()[1];
  parameters_[4] = d2.parameters()[2];
  parameters_[5] = d2.parameters()[3];
  parameters_[6] = d2.parameters()[4];

  return parameters_;
}

double Combined_1D_2D_Normal::evaluate(double x, double y, double z) const {

  return d1.evaluate(x) * d2.evaluate(y, z);
}

}
}
}

