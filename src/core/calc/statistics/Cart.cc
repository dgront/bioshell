
#include <core/calc/statistics/Cart.hh>
#include <iomanip>
#include <core/algorithms/trees/algorithms.hh>

namespace core {
namespace calc {
namespace statistics {

double data_purity(const std::vector<index2> &class_ids, const std::vector<LabelledObservationVector_SP> &training_data) {

  if(training_data.size()==0) return 0; // --- emty set is not pure at all

  index4 max_cnt = 0;
  for (index2 cl: class_ids) {
    index4 cnt = std::count_if(training_data.cbegin(), training_data.cend(), [cl](const LabelledObservationVector_SP v){ return (v->label()==cl);});
    if (cnt > max_cnt) max_cnt = cnt;
  }

  return double(max_cnt) / double(training_data.size());
}

ObservationVector::ObservationVector(const core::data::io::TableRow &r, index2 first_column) :
  ObservationVector(r, first_column, r.size() - 1) {}


ObservationVector::ObservationVector(const core::data::io::TableRow &r, index2 first_column, index2 last_column) {

  for (index2 i = first_column; i <= last_column; ++i) push_back(r.get<double>(i));
}


LabelledObservationVector::LabelledObservationVector(const core::data::io::TableRow &r, index2 label,
    index2 first_data_column) : ObservationVector(r, first_data_column), label_(label), p_label_(label) {}

LabelledObservationVector::LabelledObservationVector(const core::data::io::TableRow &r, index2 label,
    index2 first_data_column, index2 last_column) : ObservationVector(r, first_data_column, last_column) {

  p_label_ = label_ = label;
}

index1 GiniImpuritySplit::classify(const ObservationVector &v) const {

  if (split_column_ < v.count_real_columns()) return v.get_real(split_column_) > split_point_;
  if (split_column_ < v.count_int_columns()) return v.get_int(split_column_ - v.count_real_columns()) > split_point_;
  return v.get_string(split_column_ - v.count_real_columns() - v.count_string_columns()) != split_string_;
}


double GiniImpuritySplit::optimal_split(std::vector<LabelledObservationVector_SP> & vv, std::vector<index1> &assigned_groups) {

  split_cost_ = 2.0;
  index2 best_column = 0;
  index4 best_index = 0;
  double best_s = 0.0, best_gi = 2;
  std::string best_string = "";
//  std::vector<index2> true_classes_;
//  for(const auto & v:vv) true_classes_.push_back(v->label());

  data::basic::Array2D<index4> counts_by_group_class(2,n_class); // --- array 2 x C where C is the number of classes (distinct labels)

  index2 ir = vv[0]->count_real_columns();
  index2 ii = vv[0]->count_int_columns();
  for (index2 icolumn = 0; icolumn < vv[0]->size(); ++icolumn) { // --- loop over data columns
    split_column_ = icolumn;
    // --- Sort data by the column we use to make the split
    Cart::sort(vv, icolumn);
    counts_by_group_class.clear(0); // --- clear the array of counts and fill it
    for (index4 i = 0; i < vv.size(); ++i) {
      ++last_split_counts(0,(*vv[i]).label()); // --- at the beginning all elements are assigned to the left group (indexed by 0)
//      std::cerr << i<<" "<<(*vv[i]).label() << " "<<last_split_counts(0,(*vv[i]).label())<<"\n";
    }

    for (index2 i = 0; i < min_branch_size_ - 1; ++i) {
      --last_split_counts(0, (*vv[i]).label()); // --- move the first bunch of observation from class-0 to class-1
      ++last_split_counts(1, (*vv[i]).label());
    }

    for (index4 i = min_branch_size_ - 1; i < vv.size() - min_branch_size_; ++i) {
      --last_split_counts(0, (*vv[i]).label()); // --- move the next observation from class-0 to class-1
      ++last_split_counts(1, (*vv[i]).label());

      double gi = gini_imputiry(last_split_counts);
      if (gi < best_gi) {
        best_gi = gi;
        best_column = split_column_;
        best_index = i;
//        std::cerr << "Best " << best_gi << " " << best_column << " " << best_s << "\n";
        if (split_column_ < ir) {
          best_s = 0.5 * (vv[i]->get_real(icolumn) + vv[i + 1]->get_real(icolumn));
        } else if (split_column_ < ii) {
          best_s = 0.5 * (vv[i]->get_int(icolumn - ir) + vv[i + 1]->get_int(icolumn - ir));
        } else {
          best_string = vv[i]->get_string(icolumn - ir - ii);
        }
      }
    }
  } // --- all data columns tested

  split_column_ = best_column;
  split_point_ = best_s;
  split_cost_ = best_gi;

  // --- Sort data by the winning column
  Cart::sort(vv,best_column);

  last_split_counts.clear(0);
  for (index4 i = 0; i <= best_index; ++i) {
    assigned_groups[i] = 0;
    ++last_split_counts(0,(*vv[i]).label());
  }
  for (index4 i = best_index + 1; i < assigned_groups.size(); ++i) {
    assigned_groups[i] = 1;
    ++last_split_counts(1,(*vv[i]).label());
  }

  logs << utils::LogLevel::INFO << utils::string_format("Optimal split by column no. %d at value %f gi: %f\n\tL: %5d\n\tR: %5d",
    split_column_, split_point_, split_cost_, best_index + 1, assigned_groups.size() - (best_index + 1));

  for (index2 ic = 0; ic < n_class; ++ic)
    logs << utils::string_format(" %5d",last_split_counts(0, ic));
  for (index2 ic = 0; ic < n_class; ++ic)
    logs << utils::string_format(" %5d",last_split_counts(1, ic));
  logs << "\n";

  return split_cost_;
}

double GiniImpuritySplit::gini_imputiry(data::basic::Array2D<index4> & counts) {

  core::index2 n_classes = counts.count_columns();
  double cnt0 = 0, cnt1 = 0; // --- sum counts separately for the left and the right branch
  for (index2 i = 0; i < n_classes; ++i) {
    cnt0 += counts(0, i);
    cnt1 += counts(1, i);
  }
  double gi_l = 0.0, gi_r = 0.0; // -- Gini impurity for left and right branch
  for (index2 i = 0; i < n_classes; ++i) {
    if (cnt0 > 0) {
      double tmp = double(counts(0, i)) / cnt0;
      gi_l += tmp * tmp;
    }
    if (cnt1 > 0) {
      double tmp = double(counts(1, i)) / cnt1;
      gi_r += tmp * tmp;
    }
  }

  return (1.0 - gi_l) * cnt0 / (cnt0 + cnt1) + (1.0 - gi_r) * cnt1 / (cnt0 + cnt1);
}

std::ostream & operator<<(std::ostream & out, const GiniImpuritySplit & splitting_point) {

  out << utils::string_format("[%d] < %f\n", splitting_point.split_column(), splitting_point.split_point());
  out << "  L:";
  for (index2 i = 0; i < splitting_point.class_cnt(); ++i)
    out << utils::string_format(" %5d", splitting_point.left_counts_for_class(i));

  out << "\n  R:";
  for (index2 i = 0; i < splitting_point.class_cnt(); ++i)
    out << utils::string_format(" %5d", splitting_point.right_counts_for_class(i));
  return out;
}

Cart::Cart(const std::vector<index2> &classes, int node_id) :
  logs("Cart"), classes(classes),
  root_sp(std::make_shared<core::algorithms::trees::BinaryTreeNode<GiniImpuritySplit>>(node_id, GiniImpuritySplit(classes.size()))) {

  purity_ratio_ = 1.0;
  min_branch_size_ = 1;
  tree_nodes_index_ = 0; // root created by the constructor is the first tree node and has index 0
}

void Cart::sort(std::vector<LabelledObservationVector_SP> & data, const index2 by_which_column) {

  index2 ir = data[0]->count_real_columns();
  index2 ii = data[0]->count_int_columns();

  if (by_which_column<ir) {
    std::sort(data.begin(), data.end(),
      [by_which_column](LabelledObservationVector_SP l, LabelledObservationVector_SP r) {
        return l->get_real(by_which_column) < r->get_real(by_which_column);
      });
  } else if (by_which_column<ii) {
    std::sort(data.begin(), data.end(),
      [by_which_column,ir](LabelledObservationVector_SP l, LabelledObservationVector_SP r) {
        return l->get_int(by_which_column-ir) < r->get_int(by_which_column-ir);
      });
  } else {
    std::sort(data.begin(), data.end(),
      [by_which_column,ir,ii](LabelledObservationVector_SP l, LabelledObservationVector_SP r) {
        return l->get_string(by_which_column - ir - ii) < r->get_string(by_which_column - ir - ii);
      });
  }
}

bool Cart::stop_branch(const std::vector<LabelledObservationVector_SP>& data) const {

  if (data.size() < min_branch_size_) return true;
  if (data_purity(classes, data) >= purity_ratio_) return true;
  return false;
}

void Cart::train(std::vector<LabelledObservationVector_SP> & vv, CartNode_SP node) {

  left_right_assignment.resize(vv.size()); // --- prepare vectors with data assignment to either left or right subtree

  node->element.optimal_split(vv, left_right_assignment); // --- find optimal split

  // --- repack data into two subsets
  std::vector<LabelledObservationVector_SP> left_data, right_data;

  for (index4 i = 0; i < vv.size(); ++i) {
    if (left_right_assignment[i] == 0) {
      left_data.push_back(vv[i]);
    } else {
      right_data.push_back(vv[i]);
    }
  }

  CartNode_SP left_sp = std::make_shared<CartNode>(++tree_nodes_index_, GiniImpuritySplit(classes.size()));
  CartNode_SP right_sp = std::make_shared<CartNode>(++tree_nodes_index_, GiniImpuritySplit(classes.size()));
  if (!stop_branch(left_data)) {
    train(left_data, left_sp);
    node->set_left(left_sp);
  }
  if (!stop_branch(right_data)) {
    train(right_data, right_sp);
    node->set_right(right_sp);
  }
}

index2 Cart::classify(const LabelledObservationVector_SP v) {

  return classify(std::static_pointer_cast<ObservationVector>(v));
}

index2 Cart::classify(const ObservationVector_SP v) {

  auto node = root_sp;
  while(true) {
    index1 c = node->element.classify(*v); // --- 0 means left, 1 means right
    if(c==0) {
      if (node->has_left()) node = node->get_left();
      else return node->element.best_left_class();
    } else {
      if (node->has_right()) node = node->get_right();
      else return node->element.best_right_class();
    }
  }
}

std::ostream & operator<<(std::ostream & out, const Cart & cart_tree) {

  core::algorithms::trees::breadth_first_preorder(cart_tree.root(), [&out](CartNode_SP n) {

    const auto &splt = n->element;
    out << utils::string_format("Node %4d : IF [%d] < %.3f THEN ", n->id, splt.split_column(), splt.split_point());
    if (n->has_left()) out << utils::string_format("Node  %4d ELSE ", n->get_left()->id);
    else out << utils::string_format("Class %4d ELSE ", splt.best_left_class());
    if (n->has_right()) out << utils::string_format("Node  %4d\n", n->get_right()->id);
    else out << utils::string_format("Class %4d\n", splt.best_right_class());
  });

  return out;
}

}
}
}
