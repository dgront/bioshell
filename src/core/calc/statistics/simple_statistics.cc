#include <core/calc/statistics/simple_statistics.hh>
#include <core/calc/statistics/OnlineStatistics.hh>

namespace core {
namespace calc {
namespace statistics {

double chi2_independence_test(const core::data::basic::Array2D<core::index4> &contingency_matrix) {

  double total = 0;
  std::vector<core::index2> column_totals(contingency_matrix.count_columns());
  std::vector<core::index2> row_totals(contingency_matrix.count_rows());
  for (index2 i_row = 0; i_row < contingency_matrix.count_rows(); ++i_row) {
    for (index2 i_col = 0; i_col < contingency_matrix.count_columns(); ++i_col) {
      double t = contingency_matrix.get(i_row, i_col);
      column_totals[i_col] += t;
      row_totals[i_row] += t;
      total += t;
    }
  }

  index2 n_cols = 0, n_rows = 0;
  for (index2 nc: column_totals) if (nc > 0) ++n_cols;
  for (index2 nr: column_totals) if (nr > 0) ++n_rows;
  double chi = 0;
  for (index2 i_row = 0; i_row < contingency_matrix.count_rows(); ++i_row) {
    if (row_totals[i_row] == 0) continue;
    for (index2 i_col = 0; i_col < contingency_matrix.count_columns(); ++i_col) {
      if (column_totals[i_col] == 0) continue;

      double e = row_totals[i_row] * column_totals[i_col] / total;
      double t = contingency_matrix.get(i_row, i_col) - e;
      chi += t * t / e;
    }
  }

  return core::calc::numeric::chi_square_pvalue((n_cols - 1) * (n_rows - 1), chi);
}


core::index4 sum_lower_right_rectangle(const core::data::basic::Array2D<core::index4> & matrix, const core::index2 i, const core::index2 j) {

  core::index4  sum = 0;
  for (index2 i_row = i+1; i_row < matrix.count_rows(); ++i_row) 
    for (index2 i_col = j+1; i_col < matrix.count_columns(); ++i_col) sum += matrix.get(i_row,i_col);
  
  return sum;
}


core::index4 sum_lower_left_rectangle(const core::data::basic::Array2D<core::index4> & matrix, const core::index2 i, const core::index2 j) {

  core::index4  sum = 0;
  for (index2 i_row = i+1; i_row < matrix.count_rows(); ++i_row) 
    for (index2 i_col = 0; i_col < j; ++i_col) sum += matrix.get(i_row,i_col);
  
  return sum;
}


double goodman_kruskal_rank_correlation(const core::data::basic::Array2D<core::index4> &contingency_matrix) {

  double n_plus = 0.0, n_minus = 0.0;
  for (index2 i_row = 0; i_row < contingency_matrix.count_rows() - 1; ++i_row) 
    for (index2 i_col = 0; i_col < contingency_matrix.count_columns() - 1; ++i_col) {
      n_plus += contingency_matrix.get(i_row,i_col) * sum_lower_right_rectangle(contingency_matrix,i_row,i_col);
    }

  for (index2 i_row = 0; i_row < contingency_matrix.count_rows() - 1; ++i_row) 
    for (index2 i_col = 1; i_col < contingency_matrix.count_columns(); ++i_col) {
      n_minus += contingency_matrix.get(i_row,i_col) * sum_lower_left_rectangle(contingency_matrix,i_row,i_col);
    }
  
  return (n_plus - n_minus) / (n_plus + n_minus);
}

double matthews_correlation(double true_pos,double true_neg, double false_pos, double false_neg) {

  double n = true_pos * true_neg - false_pos * false_neg;
  double d = (true_pos + false_neg) * (true_pos + false_pos) * (true_neg + false_pos) * (true_neg + false_neg);
  return n / sqrt(d);
}

void average_displacement(std::vector<core::data::basic::Vec3> & data,core::index2 n_steps,std::vector<double> & out) {

  for (core::index2 t_lag = 0; t_lag <= n_steps; ++t_lag) {
    core::index4 i_first=0;
    double d2 = 0, d = 0;
    for (core::index4 i_last = i_first + t_lag; i_last < data.size(); ++i_last) {

      d = data[i_first].x - data[i_last].x;
      d2 += d * d;
      d = data[i_first].y - data[i_last].y;
      d2 += d * d;
      d = data[i_first].z - data[i_last].z;
      d2 += d * d;
      ++i_first;
    }
    d2 /= double(data.size() - t_lag);
    out.push_back(d2);
  }
}

template <typename T>
std::pair<T,T> regression_line(const std::vector<T> & x, const std::vector<T> & y,core::index4 from, core::index4 to) {

  if (to == 0) to = x.size();
  int n = to - from + 1;

  double sum_x = 0, sum_y = 0, sum_xy = 0, sum_x2 = 0;
  for (core::index4 i = from; i <= to; i++) {
    sum_x += x[i];
    sum_y += y[i];
    sum_xy += x[i] * y[i];
    sum_x2 += x[i] * x[i];
  }
  double avg_x = sum_x / n;
  double avg_y = sum_y / n;
  double den = sum_x2 - sum_x * avg_x;

  T a = (sum_xy - sum_x * avg_y) / den;
  T b = avg_y - a * avg_x;

  return std::pair<T,T>(a,b);
}

template std::pair<double, double> regression_line<double>(const std::vector<double> &x, const std::vector<double> &y,
                                                           core::index4, core::index4);

template std::pair<float, float> regression_line<float>(const std::vector<float> &x, const std::vector<float> &y,
                                                        core::index4, core::index4);

template<typename T>
std::pair<double, double> bootstrap_quantile(const std::vector<T> &data, double quantile_level, int n_samples) {

  std::vector<T> tmp(data.size());
  core::calc::statistics::Random &rnd = core::calc::statistics::Random::get();
  std::uniform_int_distribution<core::index4> rand_index(0, data.size() - 1);
  OnlineStatistics stats;
  for (int i = 0; i < n_samples; i++) {
    for (int j = 0; j < data.size(); j++) tmp[j] = data[rand_index(rnd)];
    stats(double(quantile(tmp, quantile_level)));
  }

  return std::pair<double, double>(stats.avg(), sqrt(stats.var()));
}

template std::pair<double, double> bootstrap_quantile<double>(const std::vector<double> &data,
    double quantile_level, int n_samples);

template std::pair<double, double> bootstrap_quantile<float>(const std::vector<float> &data,
                                                              double quantile_level, int n_samples);

}
}
}
