#include <core/calc/statistics/OnlineStatistics.hh>
#include <limits>

namespace core {
namespace calc {
namespace statistics {

OnlineStatistics::OnlineStatistics() {
  min_ = std::numeric_limits<double>::max();
  max_ = -std::numeric_limits<double>::max();
  cnt_ = 0;
}

void OnlineStatistics::operator()(double x) {

  double delta, delta_n, delta_n2, term1;

  long long n1 = cnt_;
  cnt_++;
  delta = x - M1;
  delta_n = delta / cnt_;
  delta_n2 = delta_n * delta_n;
  term1 = delta * delta_n * n1;
  M1 += delta_n;
  M4 += term1 * delta_n2 * (cnt_ * cnt_ - 3 * cnt_ + 3) + 6 * delta_n2 * M2 - 4 * delta_n * M3;
  M3 += term1 * delta_n * (cnt_ - 2) - 3 * delta_n * M2;
  M2 += term1;

  if (x > max_) max_ = x;
  if (x < min_) min_ = x;
}

double OnlineStatistics::bimodality_coefficient() const {

  double n = double(cnt_);
  double s = skewness() + 1.0;
  double l = 3.0 * (n - 1.0) * (n - 1.0) / (n - 1.0) / (n - 3.0) + kurtosis() - 3;

  return s / l;
}

}
}
}

