#ifndef CORE_CALC_STATISTICS_P2QuantileEstimation_HH
#define CORE_CALC_STATISTICS_P2QuantileEstimation_HH

#include <math.h>
#include <vector>

#include <core/index.hh>

namespace core {
namespace calc {
namespace statistics {

/** @brief Implements P-Square Algorithm for Dynamic Calculation of Quantiles.
 *
 * Object of this class accumulates data observations and provides on-the-fly estimation of a given quantile.
 * The quantile level must be defined at object construction stage
 *
 * @see http://www.cs.wustl.edu/~jain/papers/ftp/psqr.pdf
 */
class P2QuantileEstimation {
public:
  /** @brief Constructs the estimator for percentile 'p'
   *
   * @param p - the percentile
   */
  P2QuantileEstimation(const double p) : p_(p), n{0, 1, 2, 3, 4}, n_targets{0, 2 * p_, 4 * p_, 2 + 2 * p_, 4},
                                             dn{0, p_ / 2.0, p_, (1.0 + p_) / 2.0, 1.0} {}

  /** @brief Accumulate an observation
   *
   * @param x - an observed value
   * @return - current estimated value of the quantile
   */
  double operator()(const double x);

  /// Returns the current estimated value of the quantile
  double p_value() { return p_value_; }

private:
  const double p_; ///< percentile to find
  double p_value_; ///< last percentile value
  std::vector<double> initial; // Initial observations
  double q[5]; // Marker heights
  core::index4 n[5]; // Marker positions
  double n_targets[5]; //  desired marker positions
  double dn[5]; // Precalculated   marker increments

  double linear(int d, int i) const;
  double parabolic(double d, int i) const;
};

}
}
}

#endif // CORE_CALC_STATISTICS_P2QuantileEstimation_HH
