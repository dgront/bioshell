#include <core/calc/statistics/VonMisesDistribution.hh>
#include <core/calc/numeric/basic_math.hh>
#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/Interpolate1D.hh>

namespace core {
namespace calc {
namespace statistics {

VonMisesDistribution::VonMisesDistribution(const std::vector <double> &parameters)
  : PDF(parameters), logs("VonMisesDistribution") {

  norm = core::calc::numeric::mod_bessel_first_kind_zero(parameters[1]) * 2.0 * M_PI;
}

void VonMisesDistribution::copy_parameters_from(const std::vector<double> &source) {

  for(unsigned int i = 0; i < parameters_.size(); ++i) {
    parameters_[i] = source[i];
  }
  norm = core::calc::numeric::mod_bessel_first_kind_zero(parameters_[1]) * 2.0 * M_PI;
}

const std::vector<double> & VonMisesDistribution::estimate(const std::vector<std::vector<double>> & observations) {
  return estimate(observations, 0);
}

const std::vector<double> & VonMisesDistribution::estimate(const std::vector<std::vector<double>> & observations,
                                                                const std::vector<double> & weights) {
  return estimate(observations, weights, 0);
}

const std::vector<double> & VonMisesDistribution::estimate(const std::vector<std::vector<double>> & observations,
        const std::vector<double> & weights, const core::index1 data_column_index) {

  double s = 0, c = 0, w = 0;
  for (core::index4 i = 0; i < observations.size(); ++i) {
    s += sin(observations[i][data_column_index]);
    c += cos(observations[i][data_column_index]);
    w += weights[i];
  }

  s /= w;
  c /= w;
  double r2 = s * s + c * c;

  parameters_[0] = atan2(s,c); // mu
  parameters_[1] = sqrt(r2) * (2 - r2) / (1 - r2); // kappa
  norm = core::calc::numeric::mod_bessel_first_kind_zero(parameters_[1]) * 2.0 * M_PI;

  return parameters_;
}

const std::vector<double> & VonMisesDistribution::estimate(const std::vector<std::vector<double>> & observations,
    const core::index1 data_column_index) {

  double s = 0, c = 0;
  for (const auto & a:observations) {
    s += sin(a[data_column_index]);
    c += cos(a[data_column_index]);
  }

  s /= double(observations.size());
  c /= double(observations.size());
  double r2 = s * s + c * c;

  parameters_[0] = atan2(s,c); // mu
  parameters_[1] = sqrt(r2) * (2 - r2) / (1 - r2); // kappa
  norm = core::calc::numeric::mod_bessel_first_kind_zero(parameters_[1]) * 2.0 * M_PI;

  return parameters_;
}

double VonMisesDistribution::cdf(double x, double avg, double kappa) {

  double sum = 0, bterm = 1, tol = 1e-5;
  int j = 1;
  while (fabs(bterm) > tol) {
    bterm = core::calc::numeric::mod_bessel_first_kind_I(j, kappa, 0.00001) / j;
    sum += bterm * sin((x - avg) * j);
    j++;
  }

  return 1.0 / (2.0 * M_PI) * (x + 2.0 / core::calc::numeric::mod_bessel_first_kind_zero(kappa) * sum);
}

}
}
}
