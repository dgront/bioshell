#ifndef CORE_CALC_STATISTICS_TrivariateNormal_HH
#define CORE_CALC_STATISTICS_TrivariateNormal_HH

#include <vector>
#include <core/calc/statistics/PDF.hh>

namespace core {
namespace calc {
namespace statistics {

/** \brief Normal distribution function.
 */
class TrivariateNormal: public PDF {
public:

  /** @brief Constructor creates an instance of this distribution based on the given parameters.
   *
   * @param ave1 - expected value in the X dimension \f$\mu_x\f$
   * @param ave2 - expected value in the Y dimension \f$\mu_y\f$
   * @param ave3 - expected value in the Z dimension \f$\mu_z\f$
   * @param var1 - standard deviation in the X dimension  \f$\sigma_x\f$
   * @param var2 - standard deviation in the Y dimension  \f$\sigma_y\f$
   * @param var3 - standard deviation in the Z dimension  \f$\sigma_z\f$
   * @param cov1 - covariance between X and Y dimensions  \f$\sigma^2_{xy}\f$
   * @param cov2 - covariance between X and Z dimensions \f$\sigma^2_{xz}\f$
   * @param cov3 - covariance between Y and Z dimensions \f$\sigma^2_{yz}\f$
   */
  TrivariateNormal(const double ave1, const double ave2, const double ave3, const double var1, const double var2, const double var3, const double cov1, const double cov2, const double cov3) : TrivariateNormal(std::vector<double>({ave1,ave2,ave3,var1,var2,var3,cov1,cov2,cov3})) {}

  /** @brief Constructor creates an instance of this distribution based on the given parameters
   * @param parameters - parameters of the distribution in the following order:
   *   \f$\mu_x\f$, \f$\mu_y\f$, \f$\mu_z\f$, \f$\sigma_x\f$, \f$\sigma_y\f$, \f$\sigma_z\f$,
   *   \f$\sigma^2_{xy}\f$, \f$\sigma^2_{xz}\f$, \f$\sigma^2_{yz}\f$
   */
  TrivariateNormal(const std::vector<double> & parameters);

   /// Default virtual destructor does nothing
  virtual ~TrivariateNormal() {}

  /** @brief Estimates parameters of this distribution based on a given sample.
   *
   * @param observations - sample of 3D points generated from a trivariate normal distribution used to estimate its parameters
   */
  const virtual std::vector<double> & estimate(const std::vector<std::vector<double>> & observations);

  /** @brief Estimates parameters of this distribution based on a given sample of weighted observations.
   *
   * @param observations - sample of 3D points generated from a trivariate normal distribution used to estimate its parameters
   * @param weights - weight values
   */
  const virtual std::vector<double> & estimate(const std::vector<std::vector<double>> & observations,
      const std::vector<double> & weights);

  /** @brief Evaluate the probability of withdrawing a given vector of values from this distribution.
   *
   * @param random_value - vector of PDF function arguments : X, Y and Z for 3D normal
   */

  virtual inline double evaluate(const std::vector<double> & random_value) const  {
    return evaluate(random_value[0],random_value[1],random_value[2]);
  }

  /** @brief Evaluate the probability of withdrawing a given value from distribution.
   *
   * @param x_3d - x for the 3D normal term
   * @param y_3d - y for the 3D normal term
   * @param z_3d - z for the 3D normal term
   */
  double evaluate(double x_3d, double y_3d, double z_3d) const;

  virtual void copy_parameters_from(const std::vector<double> &source);

private:
  double detM;
  void set_up_constants();
};

}
}
}

#endif
