#ifndef CORE_CALC_STATISTICS_VonMissesDistribution_HH
#define CORE_CALC_STATISTICS_VonMissesDistribution_HH

#include <math.h>
#include <vector>
#include <core/calc/statistics/PDF.hh>
#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace statistics {

/** \brief The class represents a Von Misses probability distribution.
 * 
 * The von Mises probability density function for the argument \f$ x \f$ is given by:
 * \f[ f(x|\mu,\kappa)=\frac{e^{\kappa\cos(x-\mu)}}{2\pi I_0(\kappa)} \f]
 * where \f$ I_0(x) \f$ is the modified Bessel function of order 0. The parameters of the distribution are:
 *   - the mean direction \f$ \mu \in [0,2\pi) \f$
 *   - a concentration parameter \f$ \kappa > 0 \f$
 *   
 * The distribution may be used as a kernel function for a kernel density estimator when the estimated density function is periodic
 * @see core::calc::statistics::vonmises_kernel
 */
class VonMisesDistribution : public PDF {
public:

  /** \brief Creates a new Von Misses probability distribution.
   *
   * @param mu - the mean direction \f$ \mu \in [-\pi,\pi) \f$
   * @param kappa - a concentration parameter \f$ \kappa > 0 \f$
   */
  VonMisesDistribution(double mu, double kappa) : VonMisesDistribution(std::vector<double>({mu,kappa})) {}

  /** @brief Creates a new Von Misses probability distribution based on the given vector of parameters
   * @param parameters - parameters of the distribution: \f$ \mu \in [-\pi, \pi) \f$  and \f$ \kappa > 0 \f$
   */
  VonMisesDistribution(const std::vector <double> &parameters);

  /// Default virtual destructor does nothing
  virtual ~VonMisesDistribution() {}

  /** \brief Returns the value of \f$ \mu \f$ parameter.
   * @return \f$ \mu \in [-\pi, \pi) \f$ which is the mean direction
   */
  double mu() const { return parameters_[0]; }

  /** \brief Returns the value of \f$ \kappa \f$ parameter.
   * @return \f$ \kappa > 0 \f$ which is a concentration parameter
   */
  double kappa() const { return parameters_[1]; }

  void copy_parameters_from(const std::vector<double> &source);

  /** @brief Evaluate the probability of withdrawing a given value from distribution.
   *
   * @param random_value - PDF function argument
   */
  double evaluate(double x) const { return exp(parameters_[1] * cos(x - parameters_[0])) / norm; }

  /** @brief Evaluate the probability of withdrawing a given value from distribution.
   *
   * @param x - PDF function argument in the range \f$ [-\pi, \pi) \f$
   */
  double operator()(const double x) const { return evaluate(x); }

  /** @brief Evaluate the probability of withdrawing a given vector of values from this distribution.
   *
   * @param random_value - vector of PDF function arguments - only the first element of this vector is used
   */
  virtual double evaluate(const std::vector<double> & random_value) const { return evaluate(random_value[0]); }

  /** @brief Robust estimation of the parameters of this distribution based on a given sample.
   *
   * @param observations - a sample of random observations used for the estimation
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations);

  /** @brief Estimates parameters of this distribution based on given weighted observations.
   *
   * @param observations - sample of points generated from a normal distribution used to estimate its parameters
   * @param weights - real values to weight the observations
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations,
                                                   const std::vector<double> & weights);

  /** @brief Estimates parameters of this distribution based on given weighted observations.
   *
   * @param observations - sample of points generated from a normal distribution used to estimate its parameters
   * @param weights - real values to weight the observations
   * @param data_column_index - index in <code>observations</code> vectors pointing to the data scalars
   *    to be used by this method
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations,
          const std::vector<double> & weights, const core::index1 data_column_index);

  /** @brief Robust estimation of the parameters of this distribution based on a given sample.
   *
   * @param observations - a sample of random observations used for the estimation
   * @param data_column_index - index in <code>observations</code> vectors pointing to the data scalars
   *    to be used by this method
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations,
    const core::index1 data_column_index);

  /** @brief Returns the probability of \f$ [-\inf,x] \f$ of a Von Mises distribution
   *
   * @param x - function argument
   * @param avg - expected value (distribution mean)
   * @param kappa - the value of kappa
   * @return probability of withdrawing  a value <strong>smaller</strong> than <code>x</code> from a given Von Mises distribution
   */
  static double cdf(double x, double avg, double kappa);

private:
  double norm;
  utils::Logger logs;
};

}
}
}

#endif
