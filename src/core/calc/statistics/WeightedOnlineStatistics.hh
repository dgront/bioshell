#ifndef CORE_CALC_STATISTICS_WeightedOnlineStatistics_HH
#define CORE_CALC_STATISTICS_WeightedOnlineStatistics_HH

#include <math.h>

#include <core/index.hh>

namespace core {
namespace calc {
namespace statistics {

/** @brief Calculates basic weighted statistics on the fly.
 * Just toss some double data in and ask the object for average and variance.
 *
 * The following example application calculates basic statistics from data read from a file; if no input file
 * is given, a random sample will be used for demonstration:
 *
 * @include ap_WeightedOnlineStatistics.cc
 */
class WeightedOnlineStatistics {
public:

  /// Starts the calculator
  WeightedOnlineStatistics();

  /** @brief Adds a weighted observation
   * @param d - observed data
   * @param w - weight of this observation
   */
  void operator()(double d, double w);

  /// Returns the number of observed samples
  unsigned long long cnt() const { return cnt_; }

  /// Returns the number of observed samples
  long double sum_of_weights() const { return W_; }

  /// Returns the minimum value observed so far
  double min() const { return min_; }

  /// Returns the maximum value observed so far
  double max() const { return max_; }

  /// Returns the average of the values observed so far
  double avg() const { return M1; }

  /// Returns the variance of the values observed so far
  double var() const { return double(M2 / W_); }

private:
  unsigned long long cnt_ = 0;
  long double W_ = 0;
  double M1 = 0.0, M2 = 0.0;
  double min_;
  double max_;
};

}
}
}

#endif // ~ CORE_CALC_STATISTICS_WeightedOnlineStatistics_HH
/*
 * @example ap_WeightedOnlineStatistics.cc
 */