#ifndef CORE_CALC_STATISTICS_WeightedOnlineMultivariateStatistics_HH
#define CORE_CALC_STATISTICS_WeightedOnlineMultivariateStatistics_HH

#include <core/index.hh>
#include <core/data/basic/Array2D.hh>

namespace core {
namespace calc {
namespace statistics {

/** @brief Calculates  weighted covariance statistics on the fly.
 * Just toss some double 2D data in and ask the object for covariance.
 *
 */
class WeightedOnlineMultivariateStatistics {
public:

  /** @brief Starts the calculator
   * @param dim - dimension of the observed data
   */
  WeightedOnlineMultivariateStatistics(const core::index2 dim);

  /** @brief Adds a single weighted observation
   * @param dx - observed data - an observed vector
   * @param w - weight of this observation
   */
  void operator()(std::vector<double> d, double w);

  /// Returns the number of observed samples
  unsigned long long cnt() const { return cnt_; }

  /// Returns the number of observed samples
  long double sum_of_weights() const { return W_; }

  /** @brief Returns the average of the values observed so far.
   * @param id - index of the coordinate
   * @return average value of a given dimension
   */
  double avg(const core::index2 id) const { return M1[id]; }

  /** @brief Returns the variance of the values observed so far.
   * @param id - index of the coordinate
   * @return variance value of a given dimension
   */
  double var(const core::index2 id) const { return M2[id] / W_; }

  /** @brief Returns the covariance between i-th and j-th columns of the data observed so far
   * @param id_i - index of the i-th coordinate
   * @param id_j - index of the j-th coordinate
   * @return covariance value
   */
  double covar(const core::index2 id_i, const core::index2 id_j) const { return cov(id_i,id_j)/W_; }

private:
  std::vector<double> M1;
  std::vector<double> M2;
  core::data::basic::Array2D<double> cov;
  const core::index2 dim;
  unsigned long long cnt_ = 0;
  long double W_ = 0;
};

}
}
}

#endif // ~ CORE_CALC_STATISTICS_WeightedOnlineMultivariateStatistics_HH
