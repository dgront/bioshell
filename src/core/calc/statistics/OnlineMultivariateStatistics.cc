#include <limits>
#include <iostream>

#include <core/calc/statistics/OnlineMultivariateStatistics.hh>

namespace core {
namespace calc {
namespace statistics {

OnlineMultivariateStatistics::OnlineMultivariateStatistics(const core::index2 dim)
    : M1(dim, 0), M2(dim, 0), cov(dim, dim), dim(dim) {}

void OnlineMultivariateStatistics::operator()(std::vector<double> d) {

  cnt_++;
  for (core::index2 i = 0; i < dim; ++i) {
    double delta_x = d[i] - M1[i];
    M1[i] += delta_x / double(cnt_);  // --- M1[i] is now the new average for i-th dimension
    M2[i] += delta_x * (d[i] - M1[i]);

    for (core::index2 j = i+1; j < dim; ++j) {
      double delta_y = d[j] - M1[j];
      cov(i, j) += delta_y * (d[i] - M1[i]);
      cov(j, i) = cov(i, j);
    }
  }
}

}
}
}

