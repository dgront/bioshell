#ifndef CORE_CALC_STATISTICS_PDF_HH
#define CORE_CALC_STATISTICS_PDF_HH

#include <ostream>
#include <vector>
#include <memory>

#include <core/index.hh>

namespace core {
namespace calc {
namespace statistics {

/** \brief A virtual base class for a probability distribution function (pdf).
 */
class PDF {
public:

  /** @brief Constructor creates an instance of this distribution based on the given parameters
   * @param parameters - parameters of the distribution, e.g. expectation, variance, moments, etc.
   */
  PDF(const std::vector<double> & parameters) : parameters_(parameters) {}

   /// Default virtual destructor does nothing
  virtual ~PDF() {}

  /** @brief Estimates parameters of this distribution based on a given sample.
   *
   * @param observations - a sample of random observations used for the estimation
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations) = 0;

  /** @brief Estimates parameters of this distribution based on given weighted observations.
   *
   * @param observations - sample of points generated from a normal distribution used to estimate its parameters
   * @param weights - real values to weight the observations
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations,
                                                                 const std::vector<double> & weights) = 0;
  /** @brief Evaluate the probability of withdrawing a given value from distribution.
   *
   * @param random_value - PDF function argument
   */
  virtual double evaluate(const std::vector<double> & random_value) const = 0;

  /** @brief Evaluate the probability of withdrawing a given value from distribution.
   *
   * This operator simply calls <code>evaluate()</code>
   * @param random_value - PDF function argument
   */
  double operator()(const std::vector<double> & random_value) const { return evaluate(random_value); }

  /** @brief Provide read-only access to parameters of this distribution
   *
   * @return vector of distribution's parameters, such as mean, standard deviation, etc.
   */
  const std::vector<double> & parameters() const { return parameters_; }

  /** @brief Writes parameters of a distribution into a stream
   *
   * @param stream - output stream
   * @param pdf - distribution to be written
   * @return reference to the output stream
   */
  friend std::ostream &operator<<(std::ostream &stream, const PDF &pdf) {

    for (double p : pdf.parameters_) stream << p << " ";

    return stream;
  }

  /** @brief Copies parameters of this distribution to the given <code>destination</code> vector.
   * <code>destination</code> vector must be at least of the same size as the number of parameters of this distribution
   * @param destination - vector where parameters are copied to
   */
  void copy_parameters_into(std::vector<double> &destination) const {

    for (index1 i = 0; i < parameters_.size(); ++i) destination[i] = parameters_[i];
  }

  /** @brief Substitutes parameters of this distribution with the provided values
   *
   * @param source - container with new distribution parameters to be used
   */
  virtual void copy_parameters_from(const std::vector<double> &source) = 0;

protected:
  ///  parameters of the distribution, e.g. expectation, variance, moments, etc.
  std::vector<double> parameters_;
};


}
}
}

#endif
