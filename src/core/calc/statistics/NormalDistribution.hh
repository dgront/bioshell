#ifndef CORE_CALC_STATISTICS_NormalDistribution_HH
#define CORE_CALC_STATISTICS_NormalDistribution_HH

#include <math.h>
#include <algorithm>    // std::sort
#include <vector>
#include <core/calc/statistics/PDF.hh>
#include <core/calc/statistics/simple_statistics.hh>
#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace statistics {

/** \brief Normal distribution function.
 *
 * \include ex_NormalDistribution.cc
 */
class NormalDistribution: public PDF {
public:

  /** @brief Constructor creates an instance of this distribution based on the given parameters : mean and sdev
   * @param parameters - parameters of the distribution, e.g. expectation, variance, moments, etc.
   */
  NormalDistribution(const double avg, const double sdev) : NormalDistribution(std::vector<double>({avg,sdev})) {}

  /** @brief Constructor creates an instance of this distribution based on the given parameters
   * @param parameters - parameters of the distribution, e.g. expectation, variance, moments, etc.
   */
  NormalDistribution(const std::vector<double> & parameters);

   /// Default virtual destructor does nothing
  virtual ~NormalDistribution() {}

  /** @brief Estimates parameters of this distribution based on a given sample.
   *
   * @param observations - sample of points generated from a normal distribution used to estimate its parameters
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations);

  /** @brief Estimates parameters of this distribution based on given weighted observations.
   *
   * @param observations - sample of points generated from a normal distribution used to estimate its parameters
   * @param weights - real values to weight the observations
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations,
      const std::vector<double> & weights);

  /** @brief Estimates parameters of this distribution based on a given sample.
   *
   * @param observations - PDF function argument
   * @param data_column_index - index in <code>observations</code> vectors pointing to the data scalars
   *    to be used by this method
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations,
                                             const core::index1 data_column_index);

  /** @brief Estimates parameters of this distribution based on given weighted observations.
   *
   * @param observations - sample of points generated from a normal distribution used to estimate its parameters
   * @param weights - real values to weight the observations
   * @param data_column_index - index in <code>observations</code> vectors pointing to the data scalars
   *    to be used by this method
   */
  const std::vector<double> &estimate(const std::vector<std::vector<double>> &observations,
        const std::vector<double> & weights, const core::index1 data_column_index);

  /** @brief Evaluate the probability of withdrawing a given vector of values from this distribution.
   *
   * @param random_value - vector of PDF function arguments - only the first element of this vector is used
   */
  virtual inline double evaluate(const std::vector<double> & random_value) const  { return evaluate(random_value[0]); }

  /** @brief Evaluate the probability of withdrawing a given value from distribution.
   *
   * @param x - argument value
   */
  double evaluate(double x) const;

  /** @brief Substitutes parameters of this distribution with the provided values
   *
   * @param source - container with new distribution parameters to be used
   */
  virtual void copy_parameters_from(const std::vector<double> &source);

  /** @brief Substitutes parameters of this distribution with the provided values
   *
   * @param avg - new expected value for this normal distribution
   * @param stdev - new standard deviation for this normal distribution
   */
  void copy_parameters_from(const double avg, const double stdev) { parameters_[0] = avg; parameters_[1] = stdev; }

  /** @brief Returns the probability of \f$ [-\inf,x] \f$ of a normal distribution
   *
   * @param x - function argument (e.g. statistics value to be tested)
   * @param avg - expected value (distribution mean)
   * @param sdev - standard deviation (first central moment of the distribution)
   * @return probability of withdrawing  a value <strong>smaller</strong> than <code>x</code> from a given normal distribution
   */
  static double cdf(double x, double avg, double sdev) { return 0.5 * (1 + erf((x - avg) / (sdev * sqrt(2.)))); }

private:
  double const_1; ///< normalization of the 1D normal distribution
  utils::Logger logs;
  void set_up_constants();
};

/**
 * \example ex_NormalDistribution.cc
 */

}
}
}

#endif
