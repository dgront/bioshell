#include <core/calc/numeric/basic_math.hh>
#include <core/calc/statistics/P2QuantileEstimation.hh>
#include <iostream>
#include <algorithm>

namespace core {
namespace calc {
namespace statistics {

double P2QuantileEstimation::operator()(const double x) {

  if (initial.size() < 5) {
    initial.push_back(x);
    std::sort(initial.begin(), initial.end());

    if (initial.size() == 5)
      for (int i = 0; i < 5; i++) q[i] = initial[i];
    else {
      p_value_ = initial[(core::index1) (p_ * double(initial.size()))];
      return p_value_;
    }
  }

  int k = -1;
  if (x < q[0]) { // Update minimum value
    q[0] = x;
    k = 0;
  } else if (q[0] <= x && x < q[1])
    k = 0;
  else if (q[1] <= x && x < q[2])
    k = 1;
  else if (q[2] <= x && x < q[3])
    k = 2;
  else if (q[3] <= x && x <= q[4])
    k = 3;
  else if (q[4] < x) { // Update maximum value
    q[4] = x;
    k = 3;
  }

  // Increment all positions starting at marker k+1
  for (int i = k + 1; i < 5; i++) n[i]++;

  // Update desired marker positions
  for (int i = 0; i < 5; i++) n_targets[i] += dn[i];

  // Adjust marker heights 2-4 if necessary
  for (int i = 1; i < 4; i++) {
    float d = n_targets[i] - n[i];

    if ((d >= 1 && (n[i + 1] - n[i]) > 1) || (d <= -1 && (n[i - 1] - n[i]) < -1)) {
      int ds = core::calc::numeric::sgn(d);

      // Try adjusting q using P-squared formula
      float tmp = parabolic(ds, i);
      if (q[i - 1] < tmp && tmp < q[i + 1]) {
        q[i] = tmp;
      } else {
        q[i] = linear(ds, i);
      }
      n[i] += ds;
    }
  }

  p_value_ = q[2];
  return q[2];
}

double P2QuantileEstimation::linear(int d, int i) const {
  return q[i] + d * (q[i + d] - q[i]) / (n[i + d] - n[i]);
}

double P2QuantileEstimation::parabolic(double d, int i) const {
  float a = (float) d / (float) (n[i + 1] - n[i - 1]);

  float b = (double) (n[i] - n[i - 1] + d) * (q[i + 1] - q[i]) / (double) (n[i + 1] - n[i])
            + (double) (n[i + 1] - n[i] - d) * (q[i] - q[i - 1]) / (double) (n[i] - n[i - 1]);

  return (float) q[i] + a * b;
}


}
}
}
