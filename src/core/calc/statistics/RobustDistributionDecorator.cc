#include <core/calc/statistics/RobustDistributionDecorator.hh>
#include <core/calc/statistics/VonMisesDistribution.hh>
#include <core/calc/numeric/basic_math.hh>
#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/Interpolate1D.hh>

namespace core {
namespace calc {
namespace statistics {

/// Specialised version to handle robust estimation of VonMisesDistribution which is periodic and cannot be sorted
template<>
const std::vector<double> & RobustDistributionDecorator<VonMisesDistribution>::estimate(const std::vector<std::vector<double>> & observations) {

  // ---------- Approximate CDF of this distribution with a spline
  std::vector<double> cdfx;  // --- tabulated arguments (X) to approximate VonMises cumulative distribution function with a spline
  std::vector<double> cdfy;  // --- tabulated CDF values
  double step = 0.5 / sqrt(parameters_[1]);
  for(int i=-5;i<=5;++i) {
    cdfx.push_back(parameters_[0] + i * step); // --- CDF function for initial parametrs
    double v = cdf(parameters_[0] + i * step, parameters_[0], parameters_[1]);
    if (v > 0.5) v = 1.0 - v;
    cdfy.push_back(v);
  }
  core::calc::numeric::CatmullRomInterpolator<double> cri;
  core::calc::numeric::Interpolate1D<std::vector<double>, double, core::calc::numeric::CatmullRomInterpolator<double> > cdf_interpolated(
    cdfx, cdfy, cri);

  // ---------- copy observations to a new vector
  std::vector<std::vector<double>> data_copy(observations.size());
  for(auto const& v : observations) data_copy.push_back(v);
  std::sort(data_copy.begin(), data_copy.end(), // --- sort the data by their CDF value
    [&cdf_interpolated](const std::vector<double> & p1, const std::vector<double> & p2) {
        return cdf_interpolated(p1[0]) < cdf_interpolated(p2[0]); });

  core::index4 n_removed = observations.size() * fraction_to_be_removed_;
  data_copy.erase(data_copy.end() - n_removed, data_copy.end());

  logs << utils::LogLevel::INFO << "Robust estimation reduced data set to " << data_copy.size() << " points\n";
  VonMisesDistribution::estimate(data_copy);

  return parameters_;
}

}
}
}

