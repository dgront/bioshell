#ifndef CORE_CALC_STATISTICS_Robustdistribution_Decorator_HH
#define CORE_CALC_STATISTICS_Robustdistribution_Decorator_HH

#include <algorithm>

#include <core/calc/statistics/PDF.hh>
#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace statistics {

/** \brief A decorator which provides robust estimation of distribution's parameters.
 * 
 * This decorator redefines <code>estimate()</code> method so is uses trimmed statistics
 * during estimation process. This allows removal of outliers and makes the procedure more stable.
 *
 * All the other method of this class are just delegations to the underlying distribution instance
 * used to construct this object.
 *
 * The example below uses robust estimation to find parameters of a normal distribution in the presence of random noise:
 * @include ex_RobustDistributionDecorator.cc
 */
template <class T>  
class RobustDistributionDecorator : public T {
public:

  /** @brief Constructor creates an instance of this distribution based on the given parameters
   * @param parameters - parameters of the distribution, e.g. expectation, variance, moments, etc.
   */
  RobustDistributionDecorator(const std::vector<double> & parameters, const double fraction_to_be_removed = 0.1) :
   T(parameters),  fraction_to_be_removed_(fraction_to_be_removed),
   logs("RobustDistributionDecorator") {}

   /// Default virtual destructor does nothing
  virtual ~RobustDistributionDecorator() {}

  /** @brief Redefine estimation of distribution parameters to make it robust.
   *
   * @param observations - a sample of random observations used for the estimation
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations) {

    core::index1 n_col = observations[0].size(); // --- number of dimensions of the distribution
    core::index4 n_skip = fraction_to_be_removed_ * observations.size(); // --- number of points to be skipped (not exact for more than 1D)

    // --- Make a deep copy of the given sample
    std::vector<std::vector<double>> data_copy;
    data_copy.reserve(observations.size());
    std::copy(observations.begin(), observations.end(), std::back_inserter(data_copy));

    // --- Find a n_col-dimensional polyhedron to trim the data
    std::vector<double> min = observations[0], max = observations[1];
    for(core::index1 icol = 0; icol < n_col; ++icol) {
      // --- sort the data by icol column
      std::sort(data_copy.begin(), data_copy.end(), 
        [icol](std::vector<double> & vi, std::vector<double> & vj){ return vi[icol]< vj[icol]; });
        
      min[icol] = data_copy[n_skip][icol];
      max[icol] = data_copy[data_copy.size() - n_skip][icol];
      logs << utils::LogLevel::FINE << "range in dimension " << icol<< ": ["
          << min[icol] << "," << min[icol] << "]\n";
    }

    data_copy.clear();
    for(const std::vector<double> & o : observations) {
      bool is_ok = true;
      for(core::index1 icol = 0; icol < n_col; ++icol) {
        if(o[icol] < min[icol]) { is_ok = false; break; }
        if(o[icol] > max[icol]) { is_ok = false; break; }
      }
      if(is_ok)  data_copy.push_back(o);
    }
    logs << utils::LogLevel::INFO << "Robust estimation reduced data set to " << data_copy.size() << " from "
         << observations.size() << " points\n";
    T::estimate(data_copy);

    return T::parameters();
  }

private:
  double fraction_to_be_removed_;
  utils::Logger logs;
};

}
}
}

#endif
/*
 * @example ex_RobustDistributionDecorator
 */