#include <cmath>

#include <core/index.hh>
#include <core/calc/statistics/TrivariateNormal.hh>
#include <core/calc/statistics/OnlineMultivariateStatistics.hh>
#include <core/calc/statistics/WeightedOnlineMultivariateStatistics.hh>

namespace core {
namespace calc {
namespace statistics {

TrivariateNormal::TrivariateNormal(const std::vector<double> &parameters) : PDF(parameters) {

  if (parameters_.size() == 0) {
    parameters_.resize(9, 0.0);
    parameters_[3] = parameters_[4] = parameters_[5] = 0.1;
  }
  set_up_constants();
}

void TrivariateNormal::set_up_constants() {

  detM = (parameters_[3] * parameters_[4] * parameters_[5]) + 2 * (parameters_[6] * parameters_[7] * parameters_[8]) -
         (parameters_[7] * parameters_[7] * parameters_[4]) - (parameters_[6] * parameters_[6] * parameters_[5]) -
         (parameters_[8] * parameters_[8] * parameters_[3]);
}

void TrivariateNormal::copy_parameters_from(const std::vector<double> &source) {

  for(unsigned int i = 0; i < parameters_.size(); ++i) {
    parameters_[i] = source[i];
  }
  set_up_constants();
}

const std::vector<double> &TrivariateNormal::estimate(const std::vector<std::vector<double>> &observations) {

  OnlineMultivariateStatistics stats(3);
  for (core::index4 i = 0; i < observations.size(); ++i) stats(observations[i]);

  for(core::index1 i=0;i<3;++i) {
    parameters_[i] = stats.avg(i);
    parameters_[i+3] = sqrt(stats.var(i));
  }
  parameters_[6] = stats.covar(0,1);
  parameters_[7] = stats.covar(0,2);
  parameters_[8] = stats.covar(1,2);

  set_up_constants();							 //determinant of covariance matrix

  return parameters_;
}

const std::vector<double> & TrivariateNormal::estimate(const std::vector<std::vector<double>> & observations,
                                                 const std::vector<double> & weights) {

  WeightedOnlineMultivariateStatistics stats(3);
  for (core::index4 i = 0; i < observations.size(); ++i) stats(observations[i],weights[i]);

  for(core::index1 i=0;i<3;++i) {
    parameters_[i] = stats.avg(i);
    parameters_[i+3] = sqrt(stats.var(i));
  }
  parameters_[6] = stats.covar(0,1);
  parameters_[7] = stats.covar(0,2);
  parameters_[8] = stats.covar(1,2);

  set_up_constants();

  return parameters_;
}

/** @brief Evaluate the probability of withdrawing a given value from distribution.
 *
 * @param random_value - PDF function argument
 */
double TrivariateNormal::evaluate(double x, double y, double z) const {

  x -= parameters_[0];
  y -= parameters_[1];
  z -= parameters_[2];
  std::vector<double> M(6);                                                         //reverse matrix
  M[0] = (parameters_[4] * parameters_[5] - parameters_[8] * parameters_[8]) / detM;    //element [0][0]
  M[1] = (parameters_[7] * parameters_[8] - parameters_[6] * parameters_[5]) / detM;    //element [0][1] and [1][0]
  M[2] = (parameters_[6] * parameters_[8] - parameters_[7] * parameters_[4]) / detM;    //element [0][2] and [2][0]
  M[3] = (parameters_[3] * parameters_[5] - parameters_[7] * parameters_[7]) / detM;    //element [1][1]
  M[4] = (parameters_[6] * parameters_[7] - parameters_[3] * parameters_[8]) / detM;    //element [1][2] and [2][1]
  M[5] = (parameters_[3] * parameters_[4] - parameters_[6] * parameters_[6]) / detM;    //element [2][2]
  double pdf = (1.0 / (sqrt((2 * M_PI) * (2 * M_PI) * (2 * M_PI) * detM))) * exp(
    -((x * (x * M[0] + y * M[1] + z * M[2])) + (y * (x * M[1] + y * M[3] + z * M[4])) +
      (z * (x * M[2] + y * M[4] + z * M[5]))) / 2);
  return pdf;
}

}
}
}
