#ifndef CORE_CALC_STATISTICS_BayesianClassifier2D_HH
#define CORE_CALC_STATISTICS_BayesianClassifier2D_HH

#include <math.h>

#include <string>
#include <utility>
#include <map>
#include <set>

#include <core/index.hh>
#include <core/calc/statistics/Hexbins.hh>
#include <core/data/io/DataTable.hh>

#include <utils/Logger.hh>
#include <utils/io_utils.hh>

namespace core {
namespace calc {
namespace statistics {

/** \brief Bayesian classification for 2D data based on Hexbins histograms.
 *
 */
template <typename K, typename D, typename C>
class BayesianClassifier2D  {
public :

  /** \brief Creates empty classifier
   */
  BayesianClassifier2D() : logger("BayesianClassifier2D") { }

  /** \brief Creates empty classifier for given classes
 *
 */
  BayesianClassifier2D(const std::vector<K> klasses, D bin_side) : BayesianClassifier2D() {

    bin_width = bin_side;
    for(const K & k : klasses)  {
      klasses_.insert (std::pair<K,Hexbins<D,C>>(k,Hexbins<D,C>(bin_side)));
    }
  }

  inline void insert(const K & type, const D x,const D y) {

    std::pair<short, short> key;
    klasses_.at(type).insert(x, y, key);
    if(y<0) std::cerr << x<<" "<<y<<" "<<key.first<<" "<<key.second<<"\n";
    valid_keys.insert(key);
  }

  /** @brief Stores the classifier in a file.
   *
   * The file may be further loaded with load(const std::string &) method
   *
   * @param fname - output file name.
   */
  void write(const std::string &fname, bool if_append = false) {

    logger << utils::LogLevel::INFO << "Writing predictor to " << fname << "\n";
    if (if_append) {
      std::ofstream out(fname, std::ios_base::app);
      write(out);
      out.close();
    } else {
      std::ofstream out(fname);
      write(out);
      out.close();
    }
  }

  /** @brief Writes the classifier to a stream
   *
   * @param out - output stream
   */
  void write(std::ostream &out) {

    out << "# BIN_WIDTH " << bin_width << "\n";
    out << "#  id_x  id_y ";
    for (const auto &p :klasses_) out << utils::string_format("%6s ", p.first.c_str());
    out << "\n";
    for (const auto &k : valid_keys) {
      out << utils::string_format("%5d %5d   ", k.first, k.second);
      for (const auto &p :klasses_)
        out << utils::string_format("%6d ", p.second(k.first, k.second));
      out << "\n";
    }
  }

  void load(const std::string & fname) {

    std::string tmp;
    std::vector<std::string> tokens;
    std::string txt = utils::load_text_file(fname);
    std::stringstream in(txt);
    std::getline(in, tmp); // the first line of the file provides bin width
    utils::split(tmp, tokens);
    bin_width = utils::from_string<double>(tokens[2]);
    std::getline(in, tmp); // the second line of the file provides predictor classes
    tokens.clear();
    utils::split(tmp, tokens);

    valid_keys.clear();
    klasses_.clear();
    std::vector<std::string> pred_names;
    for (index1 ic = 3; ic < tokens.size(); ++ic) {
      logger << utils::LogLevel::INFO << "Predictor registered " << tokens[ic] << " class\n";
      klasses_.insert (std::pair<K,Hexbins<D,C>>(tokens[ic],Hexbins<D,C>(bin_width)));
      pred_names.push_back(tokens[ic]);
    }
    core::data::io::DataTable dt;
    dt.load(in);
    for(const auto & d : dt) {
      short ix = d.get<short>(0);
      short iy = d.get<short>(1);
      valid_keys.insert(std::pair<short, short>(ix, iy));
      for (index2 ic = 0; ic < pred_names.size(); ++ic) {
        klasses_.at(pred_names[ic]).insert(ix, iy, d.get<index4>(2 + ic));
      }
    }

  }

  /** Returns the most probable class based on given observations \f$ (x,y) \f$
   *
   * @param x - predictor value (first dimension)
   * @param y - predictor value (second dimension)
   * @param probability - probability value for the most probable class
   * @return most probable class
   */
  K  predict(const D x, const D y, double &probability) {

    C sum = 0;
    C max = 0;
    K best;
    for (const auto &p : klasses_) {
      auto b = p.second.bin_index(x,y);

      C c = p.second(x, y);
      sum += c;
      if (c > max) {
        max = c;
        best = p.first;
      }
    }
    probability = double(max) / double(sum);

    return best;
  }

  /** \brief Return number of observations for a certain data class where a certain pair of values belongs to.
   * @param type - predicted or true data class
   * @param x - is the value pointing for a demanded bin (X direction)
   * @param y - is the value pointing for a demanded bin (Y direction)
   * @return bin value or 0 if the value is outside the range covered by this histogram.
   */
  inline C operator()(const K & type, const D x,const D y) const {

    if (klasses_.find(type) == klasses_.end()) return 0;

    const Hexbins<D,C> & h = klasses_.find(type);
    return h.operator()(x,y);
  }

  /** \brief Return number of observations  where a certain pair of values belongs to - summed over all classes
   * @param x - is the value pointing for a demanded bin (X direction)
   * @param y - is the value pointing for a demanded bin (Y direction)
   * @return bin value or 0 if the value is outside the range covered by this histogram.
   */
  inline C operator()(const D x,const D y) const {

    C sum = 0;
    for(const auto & p : klasses_) sum += p.second(x,y);

    return sum;
  }

  C count_observations() {

    C sum = 0;
    for(const auto & k : klasses_) sum += k.second.count_entries();

    return sum;
  }

private:
  D bin_width;
  std::map<K, Hexbins<D,C>> klasses_;
  std::set<std::pair<short, short>> valid_keys;
  utils::Logger logger;
};

}
}
}

#endif
