#ifndef CORE_CALC_STATISTICS_Combined_1D_2D_Normal_HH
#define CORE_CALC_STATISTICS_Combined_1D_2D_Normal_HH

#include <vector>
#include <core/calc/statistics/PDF.hh>
#include <core/calc/statistics/NormalDistribution.hh>
#include <core/calc/statistics/BivariateNormal.hh>

namespace core {
namespace calc {
namespace statistics {

/** \brief Combine 1D and 2D normal distribution function.
 *
 */
class Combined_1D_2D_Normal: public PDF {
public:

  /** @brief  Constructor creates an instance of this distribution based on the given parameters : mean and variance and covariance.
   *
   * @param ave1 - expected value for 1D normal distribution
   * @param ave2 - expected value for 2D normal distribution - the X (first) dimension
   * @param ave3 - expected value for 2D normal distribution - the Y (second) dimension
   * @param sdev1 - standard deviation for 1D normal distribution
   * @param sdev2 - standard deviation \f$ \sigma_X \f$ for 2D normal distribution
   * @param sdev3 - standard deviation \f$ \sigma_Y \f$ for 2D normal distribution
   * @param cov23 - covariance \f$ \sigma^2_{XY} \f$  for 2D normal distribution
   */
  Combined_1D_2D_Normal(const double ave1, const double ave2, const double ave3, const double sdev1, const double sdev2,
                        const double sdev3, const double cov23) :
      Combined_1D_2D_Normal(std::vector<double>({ave1, ave2, ave3, sdev1, sdev2, sdev3, cov23})) {}

  /** @brief Constructor creates an instance of this distribution based on the given parameters
   * @param parameters - parameters of the joint distribution given in the following order:
   *   - expected value for 1D normal distribution
   *   - expected value for 2D normal distribution - the X (first) dimension
   *   - expected value for 2D normal distribution - the Y (second) dimension
   *   - standard deviation for 1D normal distribution
   *   - standard deviation \f$ \sigma_X \f$ for 2D normal distribution
   *   - standard deviation \f$ \sigma_Y \f$ for 2D normal distribution
   *   - covariance \f$ \sigma^2_{XY} \f$  for 2D normal distribution
   */
  Combined_1D_2D_Normal(const std::vector<double> & parameters);

   /// Default virtual destructor does nothing
  virtual ~Combined_1D_2D_Normal() {}

  /** @brief Estimates parameters of this distribution based on a given sample.
   *
   * @param observations - PDF function argument
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations);

  /** @brief Estimates parameters of this distribution based on a given sample of weighted observations.
   *
   * @param observations - sample of 3D points generated from a trivariate normal distribution used to estimate its parameters
   * @param weights - weight values
   */
  const virtual std::vector<double> & estimate(const std::vector<std::vector<double>> & observations,
                                                   const std::vector<double> & weights);

  /** @brief Evaluate the probability of withdrawing a given vector of values from this distribution.
   *
   * @param random_value - vector of PDF function arguments : X for 1D normal followed by X and Y for 2D normal
   */
  virtual inline double evaluate(const std::vector<double> & random_value) const  {
    return evaluate(random_value[0],random_value[1],random_value[2]);
  }

  /** @brief Evaluate the probability of withdrawing a given value from distribution.
   *
   * @param x_1d - x for the 1D normal term
   * @param x_2d - x for the 2D normal term
   * @param y_2d - y for the 2D normal term
   */
  double evaluate(double x_1d, double x_2d, double y_2d) const;

  virtual void copy_parameters_from(const std::vector<double> &source);

private:
  NormalDistribution d1;
  BivariateNormal d2;
};

}
}
}

#endif
