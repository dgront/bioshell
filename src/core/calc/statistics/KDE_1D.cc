#include <math.h>

#include <vector>
#include <algorithm>

#include <core/calc/statistics/KDE_1D.hh>
#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/Interpolate1D.hh>

namespace core {
namespace calc {
namespace statistics {

using namespace core::calc::numeric;

core::calc::numeric::Interpolate1D<std::vector<float>, float, CubicInterpolator<float> > KDE_1D::create_interpolator(
    double knots_per_bandwidth) {

  float min_spline = min_value() - h * knots_per_bandwidth;
  float max_spline = max_value() + h * knots_per_bandwidth;
  float n_spline_points = knots_per_bandwidth * (max_spline - min_spline) / h;
  float spline_step = (max_spline - min_spline) / n_spline_points;
  std::vector<float> spline_x(size_t(n_spline_points + 1));
  std::vector<float> spline_y(size_t(n_spline_points + 1));

  for (size_t i = 0; i < n_spline_points; i++) {
    spline_x[i] = min_spline + spline_step * i;
    spline_y[i] = operator()(spline_x[i]);
  }

  core::calc::numeric::CubicInterpolator<float> ci;

  Interpolate1D<std::vector<float>, float, CubicInterpolator<float> > i1d2(spline_x, spline_y, ci);
  return i1d2;
}

void KDE_1D::adaptive_bandwidth() {

  auto interpolator = create_interpolator(10);

  double avg = 0.0;
  for (size_t i = 0; i < observ.size(); i++) {
    band[i] = interpolator(observ[i]);
    avg += log(band[i]);
  }
  avg = exp(avg / double(observ.size()));
  for (size_t i = 0; i < observ.size(); i++)
    band[i] = pow(band[i] / avg, -C_PARAM);
}
}
}
}
