#ifndef CORE_CALC_STATISTICS_BivariateNormal_HH
#define CORE_CALC_STATISTICS_BivariateNormal_HH

#include <math.h>
#include <vector>
#include <core/calc/statistics/PDF.hh>

namespace core {
namespace calc {
namespace statistics {

/** \brief Bivariate normal distribution function (i.e. 2D Gaussian).
 *
 * \include ex_BivariateNormal.cc
 */
class BivariateNormal: public PDF {
public:

  /** @brief Creates an instance of this distribution based on the given parameters
   * @param ave1 - expected value (mean) of the first dimension
   * @param ave2 - expected value (mean) of the second dimension
   * @param sdev1 - standard deviation of the first dimension
   * @param sdev2 - standard deviation of the second dimension
   * @param cov12 - covariance between X and Y dimensions
   */
  BivariateNormal(const double ave1, const double ave2, const double sdev1, const double sdev2, const double cov12) :
    BivariateNormal(std::vector<double>({ave1,ave2,sdev1,sdev2,cov12})) {}

  /** @brief Constructor creates an instance of this distribution based on the given parameters
   * @param parameters - five parameters to define the distribution: two expected values, two standard deviations and covariance
   */
  BivariateNormal(const std::vector<double> & parameters);

   /// Default virtual destructor does nothing
  virtual ~BivariateNormal() {}

  /** @brief Estimates parameters of this distribution based on a given sample.
   *
   * @param observations - sample of 2D points generated from a bivariate normal distribution used to estimate its parameters
   */
  const virtual std::vector<double> & estimate(const std::vector<std::vector<double>> & observations);


  /** @brief Estimates parameters of this distribution based on given weighted observations.
   *
   * @param observations - sample of points generated from a normal distribution used to estimate its parameters
   * @param weights - real values to weight the observations
   */
  virtual const std::vector<double> & estimate(const std::vector<std::vector<double>> & observations,
                                                   const std::vector<double> & weights);

  /** @brief Evaluate the probability of withdrawing a given vector of values from this distribution.
   *
   * @param random_value - vector of PDF function arguments : X and Y for 2D normal
   */
  virtual inline double evaluate(const std::vector<double> & random_value) const  {
    return evaluate(random_value[0],random_value[1]);
  }

  virtual void copy_parameters_from(const std::vector<double> &source);

  /** @brief Evaluate the probability of withdrawing a given value from distribution.
   *
   * @param x_2d - x for the 2D normal term
   * @param y_2d - y for the 2D normal term
   */
  double evaluate(double x_2d, double y_2d) const;

private:
  double corr;
  double const_;  ///< 1 - square( correlation ), necessary for the normalization
  double const_1; ///<  normalization of the 2D normal distribution
  double const_2; ///< used by 2D normal, denominator for the exponential term

  void set_up_constants();
};

}
}
}

/**
 * \example ex_BivariateNormal.cc
 */

#endif
