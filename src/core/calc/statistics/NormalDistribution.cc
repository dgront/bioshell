#include <math.h>
#include <vector>
#include <core/index.hh>
#include <core/calc/statistics/NormalDistribution.hh>
#include <core/calc/statistics/OnlineStatistics.hh>
#include <core/calc/statistics/WeightedOnlineStatistics.hh>

namespace core {
namespace calc {
namespace statistics {

NormalDistribution::NormalDistribution(const std::vector<double> &parameters) : PDF(parameters), logs("NormalDistribution") {

  if (parameters_.size() == 0) {
    parameters_.push_back(0.0);
    parameters_.push_back(1.0);
  }
  set_up_constants();
}

void NormalDistribution::copy_parameters_from(const std::vector<double> &source) {

  for(unsigned int i = 0; i < parameters_.size(); ++i) {
    parameters_[i] = source[i];
  }
  set_up_constants();
}

void NormalDistribution::set_up_constants() { const_1 = (1.0 / (parameters_[1] * (sqrt(M_PI * 2.0)))); }

const std::vector<double> & NormalDistribution::estimate(const std::vector<std::vector<double>> &observations) {
  return estimate(observations, 0);
}

const std::vector<double> & NormalDistribution::estimate(const std::vector<std::vector<double>> &observations,
    const core::index1 data_column_index) {

  OnlineStatistics stats;
  for(const std::vector<double> & d : observations) stats(d[data_column_index]);
  parameters_[0] = stats.avg();                       // average
  parameters_[1] = sqrt(stats.var());                // standard deviation x
  set_up_constants();

  return parameters_;
}

const std::vector<double> & NormalDistribution::estimate(const std::vector<std::vector<double>> & observations,
                                                 const std::vector<double> & weights) {
  return estimate(observations, weights,0);
}

const std::vector<double> & NormalDistribution::estimate(const std::vector<std::vector<double>> & observations,
      const std::vector<double> & weights, const core::index1 data_column_index) {

  WeightedOnlineStatistics stats;
  for (size_t i = 0; i < observations.size(); ++i)
    stats(observations[i][data_column_index], weights[i]);
  parameters_[0] = stats.avg();                       // average
  parameters_[1] = sqrt(stats.var());                 // standard deviation x
  set_up_constants();

  return parameters_;
}

double NormalDistribution::evaluate(double x) const {

  x -= parameters_[0];

  double pdf = const_1 * exp(-(x * x) / (2 * parameters_[1] * parameters_[1]));

  return pdf;
}

}
}
}

