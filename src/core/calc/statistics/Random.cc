#include <core/calc/statistics/Random.hh>

namespace core {
namespace calc {
namespace statistics {

utils::Logger Random::logger("Random");

Random & rnd = Random::get();
std::uniform_real_distribution<> r(0.0,1.0);

core::data::basic::Vec3 & random_unit_vector(core::data::basic::Vec3 & v) {

  double phi = r(rnd) * M_PI * 2.0;
  double theta = acos(r(rnd) * 2 - 1);
  v.x = sin(theta) * cos(phi);
  v.y = sin(theta) * sin(phi);
  v.z = cos(theta);

  return v;
}

core::data::basic::Vec3 random_unit_vector() {

  core::data::basic::Vec3 out;

  return random_unit_vector(out);
}

}
}
}
