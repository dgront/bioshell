#include <limits>
#include <iostream>

#include <core/calc/statistics/WeightedOnlineStatistics.hh>

namespace core {
namespace calc {
namespace statistics {

WeightedOnlineStatistics::WeightedOnlineStatistics() {
  min_ = std::numeric_limits<double>::max();
  max_ = -std::numeric_limits<double>::max();
  W_ = 0.0;
  cnt_ = 0;
}

void WeightedOnlineStatistics::operator()(double x, double w) {

  W_ += w;
  cnt_++;
  double delta = x - M1;
  M1 += delta * w / W_;
  M2 += w * delta * (x - M1);
  if (x > max_) max_ = x;
  if (x < min_) min_ = x;
}

}
}
}

