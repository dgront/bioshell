#include <math.h>
#include <algorithm>    // std::sort
#include <vector>
#include <core/calc/statistics/BivariateNormal.hh>
#include <core/calc/statistics/WeightedOnlineMultivariateStatistics.hh>
#include <core/calc/statistics/OnlineMultivariateStatistics.hh>
#include <core/index.hh>

#include <iostream>

namespace core {
namespace calc {
namespace statistics {

BivariateNormal::BivariateNormal(const std::vector<double> & parameters) : PDF(parameters) {

  if (parameters_.size() == 0) {
    parameters_.resize(5, 0.0);
    parameters_[2] = parameters_[3] = 0.1;
  }
  set_up_constants();
}


void BivariateNormal::copy_parameters_from(const std::vector<double> &source) {

  for(unsigned int i = 0; i < parameters_.size(); ++i) {
    parameters_[i] = source[i];
  }
  set_up_constants();
}

void BivariateNormal::set_up_constants() {
  corr = parameters_[4] / (parameters_[2] * parameters_[3]);
  const_ = 1.0 - (corr * corr);
  const_1 = 1.0 / (2.0 * M_PI * parameters_[2] * parameters_[3] * sqrt(const_));
  const_2 = -1.0 / (2.0 * const_);
}

/** @brief Estimates parameters of this distribution based on a given sample.
 *
 * @param observations - PDF function argument
 */
const std::vector<double> & BivariateNormal::estimate(const std::vector<std::vector<double>> & observations) {

  OnlineMultivariateStatistics stats(2);
  for (core::index4 i = 0; i < observations.size(); ++i) stats(observations[i]);

  parameters_[0] = stats.avg(0);                        //average x
  parameters_[1] = stats.avg(1);                        //average y
  parameters_[2] = sqrt(stats.var(0));                  //standard deviation x
  parameters_[3] = sqrt(stats.var(1));                  //standard deviation y
  parameters_[4] = stats.covar(0,1);                    //covariance xy

  set_up_constants();

  return parameters_;
}

const std::vector<double> & BivariateNormal::estimate(const std::vector<std::vector<double>> & observations,
                                                 const std::vector<double> & weights) {

  WeightedOnlineMultivariateStatistics stats(2);
  for (core::index4 i = 0; i < observations.size(); ++i) stats(observations[i],weights[i]);

  parameters_[0] = stats.avg(0);                        //average x
  parameters_[1] = stats.avg(1);                        //average y
  parameters_[2] = sqrt(stats.var(0));                  //standard deviation x
  parameters_[3] = sqrt(stats.var(1));                  //standard deviation y
  parameters_[4] = stats.covar(0,1);                    //covariance xy

  set_up_constants();

  return parameters_;
}

double BivariateNormal::evaluate(double x, double y) const {

  x -= parameters_[0];
  y -= parameters_[1];
  double pdf = const_1 * exp(const_2 * (((x * x) / (parameters_[2] * parameters_[2])) -
                                               (2 * corr * ((x * y) / (parameters_[2] * parameters_[3]))) +
                                               ((y * y) / (parameters_[3] * parameters_[3]))));

  return pdf;
}

}
}
}
