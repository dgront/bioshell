#ifndef CORE_CALC_STATISTICS_Histogram_HH
#define CORE_CALC_STATISTICS_Histogram_HH

#include <string>
#include <cmath>
#include <core/index.hh>
#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace statistics {

/** \brief Histogram.
 *
 * @tparam D - type of the observed data
 * @tparam C - type representing histogram counts, also used for data weights. So if this is an integer type,
 *    the data weights must also be integer numbers
 */
template <typename D,typename C>
class Histogram  {
public :

  /** \brief Creates histogram of desired bin's width
   *
   * All bins will have equal width, given as:
   * \f[ n = \lceil (x_{max} - x_{min} / n)\rceil  \f]
   * where \f$ n \f$ is the number of bins and \f$\lceil x \rceil\f$ denotes <code>Math.ceil(x)</code>.
   * @param bin_size - width of each bin
   * @param min - the minimum of the range covered by the histogram bins
   * @param max - the maximum value of the range covered by the histogram bins
   */
  Histogram(D bin_size, D min, D max) : logger("Histogram") {

    n_bins = (core::index4) (ceil((max - min) / bin_size));
    min_val = min;
    max_val = max;
    histogram_data.resize(n_bins);
    n_underflow = 0;
    n_overflow = 0;
    bin_width = bin_size;
    output_value_format_ = "%8.3f";
    output_counts_format_ = "%8f";
    output_separator_ = " ";
  }

  /** \brief Constructor  that sets  number of bins and range.
   *
   * All bins will have equal width, given as:
   * \f[ \delta = (x_{max} - x_{min}) / n \f]
   * where \f$ n \f$ is the number of bins.
   * @param nbins - the number of bins the histogram should have. The
   * range specified by min and max will be divided up into this
   * many bins.
   * @param min - the minimum of the range covered by the histogram bins
   * @param max - the maximum value of the range covered by the histogram bins
   */
  Histogram(index4 nbins, D min, D max) : logger("Histogram") {

    n_bins = nbins;
    min_val = min;
    max_val = max;
    histogram_data.resize(n_bins);
    n_underflow = 0;
    n_overflow = 0;
    bin_width = (max_val - min_val) /  n_bins;
  }

  /** \brief Constructor that automatically assigns the range of a histogram and inserts given data.
   *
   * All bins will have equal width, given as:
   * \f[ \delta = (x_{max} - x_{min}) / n \f]
   * where \f$ n \f$ is the number of bins and <code>min</code>,<code>max</code>
   * are the minimum and maximum value in the given sample, respectively.
   * @param data - values to be inserted into the histogram
   * @param nbins - the number of bins the histogram should have. The
   * range specified by min and max will be divided up into this
   * many bins.
   */
  Histogram(std::vector<D> data, int nbins) : logger("Histogram") {

    n_bins = nbins;
    min_val = *std::min_element(data.cbegin(),data.cend());
    max_val = *std::max_element(data.cbegin(),data.cend());
    histogram_data.resize(n_bins);
    n_underflow = 0;
    n_overflow = 0;
    bin_width = (max_val - min_val) /  n_bins;
    insert(data);
  }

  /** \brief Returns the smallest value that falls into a given bin
   * @param bin - bin number
   * @return the smallest value that may be stored in that bin
   */
  inline D bin_min_val(const index4 bin) const { return (bin * bin_width + min_val); }

  /** \brief Returns the middle value that falls into a given bin
   *
   * @param bin - bin number
   * @return the value in the middle of this bin
   */
  inline double bin_middle_val(const index4 bin) const { return (bin + bin + 1) / 2.0 * bin_width + min_val; }

  /** \brief Returns the biggest value that falls into a given bin
   *
   * @param bin - bin number
   * @return the biggest value that may be stored in that bin
   */
  inline D bin_max_val(const index4  bin) const { return ((bin+1) * bin_width + min_val); }

  /** \brief Finds the highest bin in this histogram.
   *
   * The highest bin features the greatest number of observations. The actual number of counts
   * may be obtained by <code>checkBin()</code> method:
   * \code
   * double maxCounts = histogram.checkBin(histogram.highestBin());
   * \endcode
   *
   * @return zero-related bin number which features the greatest number of observations.
   */
  core::index4 highest_bin() const {

    int maxbin = 0;
    C maxval = histogram_data[0];
    for(core::index4 i = 1;i < n_bins;i++) {
      if (histogram_data[i] > maxval) {
        maxval = histogram_data[i];
        maxbin = i;
      }
    }
    return maxbin;
  }

  /** \brief Inserts an observation into this histogram
   *
   * Enter data into the histogram. The fill method takes the given
   * value, works out which bin this corresponds to, and increments
   * this bin by one.
   * @param x is the value to add in to the histogram
   */
  inline void insert(const D x) {
    if(std::isnan(x)) return;            // --- don't record NaNs
    if (x < min_val) n_underflow++;
    else {
      if (x >= max_val) n_overflow++;
      else {
        histogram_data[find_bin(x)]++;
        // count the number of entries made by the fill method
        n_entries++;
      }
    }
  }

  /** \brief Inserts an array of observations into this histogram
   *
   * Enter data into the histogram. The method works as insert(D val)
   * but takes an array as an argument
   * @param data is the array of values to insert into this histogram
   */
  inline void insert(const std::vector<D> data) { for(D d : data) insert(d); }

  /** \brief Inserts a weighted observation into this histogram
   *
   * Enter data into the histogram. The insert  method takes the given
   * value, works out which bin this corresponds to, and increments
   * this bin by the part value.
   * @param x - the value to add in to the histogram
   * @param part - what part to add in to the histogram (i.e. its weight)
   */
  inline void insert(const D x, const C part) {

    if (x < min_val) {
      n_underflow += part;
      return;
    }
    if (x > max_val) {
      n_overflow += part;
      return;
    }
    histogram_data[find_bin(x)] += part;

    // count the number of entries made by the fill method
    n_entries += part;
  }

  /** \brief Divide each bin by the area under this histogram.
   *
   * <strong>Note</strong>, that this operation makes sense only if the histogram bins were declared as a double type
   */
  inline void normalize() {

    for (int i = 0; i < n_bins; i++)
      histogram_data[i] /= n_entries;
  }

  /** \brief Give number of observations for a bin for which a certain value belongs.
   * @param x - is the value pointing for a demanded bin
   * @return bin value or -1 if the value is outside the range covered by this histogram.
   */
  inline C get_bin(const D x) const {

    if (x < min_val) return C(0);
    if (x > max_val) return C(0);

    return histogram_data[find_bin(x)];
  }

  /** \brief Give number of observations for a certain bin .
   * @param bin_index - number of bin
   * @return bin value
   */
  inline C get_bin(const core::index4 bin_index) const { return histogram_data[bin_index]; }

  /** \brief Get number of entries in the histogram.
   *
   * The result should correspond to the number of times the <code>insert()</code> method has been used.
   * @return number of entries
   */
  C count_entries() const { return n_entries;}

  /** \brief Get the number of bins in the histogram.
   *
   * The range of the histogram defined by min and max is divided into this many bins.
   * @return number of bins
   */
  core::index4 count_bins() const { return n_bins; }

  /** \brief Get lower end of histogram range
   * @return minimum x value covered by histogram
   */
  inline C min() const { return min_val; }

  /** \brief Get upper end of histogram range
   * @return maximum x value covered by histogram
   */
  inline C max() const { return max_val; }

  /** \brief Get the height of the overflow bin.
   *
   * Any value passed to the fill method which falls above the range of the histogram will be counted in the overflow bin.
   * @return number of overflows
   */
  inline C overflow() const { return n_overflow; }

  /** \brief Get the height of the underflow bin.
   * Any value passed to the fill method which falls below the range of the histogram will be counted in the underflow bin.
   * @return number of underflows
   */
  inline C underflow() const { return n_underflow; }

  /**
   * \brief Shows this histogram as an 'asci art' - a vertical plot is sent to a stream.
   *
   * Three characters are used to make this plot:
   *   - # is a single unit
   *   - . denotes \f$ 1/4 \f$
   *   - : denotes \f$ 1/2 \f$
   *   - * denotes \f$ 3/4 \f$
   * @param screenWidth - console width
   * @param stream - output stream
   */
  void show_graph(core::index2 screen_width, std::ostream & stream) {

    int counts_per_char;
    std::string longest_line(screen_width,' ');
    if (histogram_data[highest_bin()] < screen_width)
      counts_per_char = 1;
    else
      counts_per_char = (int) (ceil(histogram_data[highest_bin()] / ((double) screen_width)));
    logger << utils::LogLevel::FINE<<"Showing histogram with " << counts_per_char << " per each '#' character\n";
    for(int i = 0;i < n_bins;i++) {
      double nc = histogram_data[i] / counts_per_char;

      std::fill(longest_line.begin(),longest_line.end(),' ');

      std::fill_n(longest_line.begin(), (int) floor(nc), '#');

      std::string s(longest_line, (int) floor(nc));
      int r = (int) (floor((histogram_data[i] - floor(nc) * counts_per_char) * 4 / counts_per_char));
      switch (r) {
      case 1:
        s += ".";
        break;
      case 2:
        s += ":";
        break;
      case 3:
        s += "*";
        break;
      }
      stream << longest_line << "\n";
    }
  }

  /** @brief Defines a separator used to print bin range values
   * @param format - printf-style string, e.g. "%8.2f"
   * @return refeence to <code>this</code> object to facilitate chaining
   */
  Histogram<D, C> &output_value_format(const std::string &format) { output_value_format_ = format; return *this; }

  /** @brief Defines a separator used to print histogram counts
   * @param format - printf-style string, e.g. "%8d"
   * @return refeence to <code>this</code> object to facilitate chaining
   */
  Histogram<D, C> &output_counts_format(const std::string &format) { output_counts_format_ = format; return *this; }


  /** @brief Defines a separator string
   * @param format - e.g. " " or "\t"
   * @return refeence to <code>this</code> object to facilitate chaining
   */
  Histogram<D, C> &output_separator(const std::string &format) { output_separator_ = format; return *this; }

  /** @brief Returns a separator string
   * @param format - e.g. " " or "\t"
   * @return refeence to <code>this</code> object to facilitate chaining
   */
  const std::string &output_separator() const { return output_separator_; }

  /** @brief Defines a separator used to print bin range values
   * @param format - printf-style string, e.g. "%8.2f"
   * @return refeence to <code>this</code> object to facilitate chaining
   */
  const std::string &output_value_format() const { return output_value_format_; }

  /** @brief Defines a separator used to print histogram counts
   * @param format - printf-style string, e.g. "%8d"
   * @return refeence to <code>this</code> object to facilitate chaining
   */
  const std::string &output_counts_format() const { return output_counts_format_; }

protected:
  std::vector<C> histogram_data;
  D min_val;
  D max_val;
  core::index4 n_bins;
  C n_entries = 0;
  C n_overflow;
  C n_underflow;
  D bin_width;
  std::string output_value_format_; ///< Output format that is used to write bin values
  std::string output_counts_format_; ///< Output format that is used to write counts for each bin
  std::string output_separator_; ///< Separator between a bin value and the number of counts in this bin


private:
	utils::Logger logger;

  /** \brief Private internal utility method to figure out which bin of the histogram a number falls in.
   * @return info on which bin x falls in.
   */
	inline core::index4 find_bin(const double x) const { return (int) ((x - min_val) / bin_width); }
};

template  <typename D,typename C>
std::ostream & operator<<(std::ostream & out, const Histogram<D,C> & h) {

  std::string format = h.output_value_format() + h.output_separator() + h.output_value_format() + h.output_separator() +
                       h.output_counts_format();
  for (core::index4 i = 0; i < h.count_bins(); i++)
    out << utils::string_format(format, h.bin_min_val(i), h.bin_max_val(i), h.get_bin(i)) << "\n";

  return out;
}

/** @brief  Histogram that can be used in Python, specialised with <code>double</code> data</code>  and <code>index4</code>  indexes
 */
class HistogramD4 : public Histogram<double, index4> {
public:

  /// Calls Histogram(D bin_size, D min, D max) constructor with <code>D = double</code>  and <code> I = core::index4</code>
  HistogramD4(double bin_size, double min, double max) : Histogram<double, index4>(bin_size, min, max) {
    output_counts_format("%d");
    output_value_format("%8.3f");
  }

  /// Calls Histogram(index4 nbins, D min, D max)  constructor with <code>D = double</code>  and <code> I = core::index4</code>
  HistogramD4(index4 n_bins, double min, double max) : Histogram<double, index4>(n_bins, min, max) {
    output_counts_format("%d");
    output_value_format("%8.3f");
  }

  /// Calls Histogram(std::vector<D> data, int n_bins)  constructor with <code>D = double</code>  and <code> I = core::index4</code>
  HistogramD4(std::vector<double> data, core::index4 n_bins) : Histogram<double, index4>(data, n_bins) {
    output_counts_format("%d");
    output_value_format("%8.3f");
  }
};

/** @brief Histogram that can be used in Python, specialised with <code>int</code> data</code>  and <code>index4</code>  indexes
 */
class HistogramI4 : public Histogram<int, index4> {
public:

  /// Calls Histogram(D bin_size, D min, D max) constructor with <code> D = int</code>
  HistogramI4(int bin_size, int min, int max) : Histogram<int, index4>(bin_size, min, max) {
    output_counts_format("%d");
    output_value_format("%d");
  }

  /// Calls Histogram(index4 nbins, D min, D max)  constructor with <code>D = int</code>  and <code> I = core::index4</code>
  HistogramI4(index4 n_bins, int min, int max) : Histogram<int, index4>(n_bins, min, max) {
    output_counts_format("%d");
    output_value_format("%d");
  }

  /// Calls Histogram(std::vector<D> data, index4 n_bins)  constructor with <code>D = int</code>
  HistogramI4(std::vector<int> data, core::index4 n_bins) : Histogram<int, index4>(data, n_bins) {
    output_counts_format("%d");
    output_value_format("%d");
  }
};

/** @brief Histogram that can be used in Python, specialised with <code>double</code> data</code>  and <code>doule</code>  indexes
 */
class HistogramDD : public Histogram<double, double> {
public:

  /// Calls Histogram(D bin_size, D min, D max) constructor with <code>D = double</code>  and <code> I = core::index4</code>
  HistogramDD(double bin_size, double min, double max) : Histogram<double, double>(bin_size, min, max) {
    output_counts_format("%f");
    output_value_format("%8.3f");
  }

  /// Calls Histogram(D nbins, D min, D max)  constructor with <code>D = double</code>  and <code> I = core::index4</code>
  HistogramDD(index4 n_bins, double min, double max) : Histogram<double, double>(n_bins, min, max) {
    output_counts_format("%f");
    output_value_format("%8.3f");
  }

  /// Calls Histogram(std::vector<D> data, D n_bins)  constructor with <code>D = double</code>  and <code> I = core::index4</code>
  HistogramDD(std::vector<double> data, index4 n_bins) : Histogram<double, double>(data, n_bins) {
    output_counts_format("%f");
    output_value_format("%8.3f");
  }
};

}
}
}

#endif
