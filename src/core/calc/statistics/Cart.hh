#ifndef CORE_CALC_STATISTICS_Cart_HH
#define CORE_CALC_STATISTICS_Cart_HH

#include <math.h>
#include <vector>
#include <memory>

#include <core/algorithms/trees/BinaryTreeNode.hh>
#include <core/data/basic/Array2D.hh>
#include <core/data/io/DataTable.hh>

#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace statistics {

/** @brief Data type used to represent a single data row i.e. a vector of independent features
 *
 * Real data is stored first, followed by integers, then by strings
 */
class ObservationVector {
public:
  /** @brief Constructor takes data from a TableRow object
   * This facilitates easy loading of data from a file using DataTable class.
   * @param r - TableRow object provides the data
   * @param first_column - where the column with independent features start. The constructor reads all the columns
   * starting from <code>first_column</code>
   */
  ObservationVector(const core::data::io::TableRow & r, index2 first_column = 0);

  /** @brief Constructor takes data from a TableRow object
   * This facilitates easy loading of data from a file using DataTable class
   * @param r - TableRow object provides the data
   * @param first_column - where the column with independent features start
   * @param last_column - where the column with independent features eds
   */
  ObservationVector(const core::data::io::TableRow &r, index2 first_column, index2 last_column);

  /// Returns the total number of columns in this observation vector
  index2 size() { return real_data.size() + int_data.size() + string_data.size(); }

  /// Returns the total number of columns that holds real values in this observation vector
  index2 count_real_columns() const { return real_data.size(); }

  /// Returns the total number of columns that holds integers in this observation vector
  index2 count_int_columns() const { return int_data.size(); }

  /// Returns the total number of columns that holds strings in this observation vector
  index2 count_string_columns() const { return string_data.size(); }

  /** @brief Returns a given real value from this observation vector
   *
   * @param r_column_index - index of a real value, not the column index!
   * @return a real value
   */
  double get_real(index2 r_column_index) const { return real_data[r_column_index]; }

  /** @brief Returns a given integer value from this observation vector
   *
   * @param r_column_index - index of an integer value, not the column index!
   * @return an integer value
   */
  int get_int(index2 i_column_index) const { return int_data[i_column_index]; }

  /** @brief Returns a given string value from this observation vector
   *
   * @param r_column_index - index of a string value, not the column index!
   * @return a string value
   */
  std::string get_string(index2 s_column_index) const { return string_data[s_column_index]; }

  /// Inserts a real value to this vector of observations
  void push_back(double v) { real_data.push_back(v);}

  /// Inserts an integer value to this vector of observations
  void push_back(int v) { int_data.push_back(v);}

  /// Inserts a string value to this vector of observations
  void push_back(const std::string & v) { string_data.push_back(v);}

private:
  std::vector<double> real_data;
  std::vector<int> int_data;
  std::vector<std::string> string_data;
};

typedef std::shared_ptr<ObservationVector> ObservationVector_SP;

/** @brief Data type used to represent a single data row i.e. a vector of independent features
 *
 */
class LabelledObservationVector : public ObservationVector {
public:
  /** @brief Constructor takes data from a TableRow object
   * This facilitates easy loading of data from a file using DataTable class.
   * @param r - TableRow object provides the data
   * @param first_column - where the column with independent features start. The constructor reads all the columns
   * starting from <code>first_column</code> unless the very last column is used for labels
   * @param label_column - where the true label is stored; say '-1' to fetch the very last column in the data row
   */
  LabelledObservationVector(const core::data::io::TableRow &r,index2 label, index2 first_data_column = 0);

  /** @brief Constructor takes data from a TableRow object
   * This facilitates easy loading of data from a file using DataTable class
   * @param r - TableRow object provides the data
   * @param first_column - where the column with independent features start
   * @param last_column - where the column with independent features eds
   * @param label_column - where the true label is stored; say '-1' to fetch the very last column in the data row
   */
  LabelledObservationVector(const core::data::io::TableRow &r, index2 label, index2 first_column, index2 last_column);

  /// Returns the (true) label assigned to this data
  index2 label() const { return label_; }

  /// Returns the predicted label assigned to this data
  index2 predicted_label() const { return p_label_; }

  /// Returns the predicted label assigned to this data
  void predicted_label(index2 label) {  p_label_ = label; }

private:
  index2 label_;
  index2 p_label_;
};

typedef std::shared_ptr<LabelledObservationVector> LabelledObservationVector_SP;


/** @brief Calculates by-label-purity of a given set of data
 *
 * @param class_ids - all known labels (class IDs)
 * @param true_class_assigned - true labels assigned to data
 * @return purity fraction i.e. the fraction of the data comprised by the most popular class. E.g. data has 4 labels
 * "A", "B", "C" and "D". Given vector comprises 6, 3, 1 and 0 each of them respectively. Data purity is the fraction of "A"
 * (which is the most frequent label) and equals to 6/10 = 0.6
 */
double data_purity(const std::vector<index2> &class_ids, const std::vector<LabelledObservationVector_SP> &training_data);


class GiniImpuritySplit {
public:

  /** @brief Calculates Gini impurity value for a given segregation of data
   *
   * @param counts
   * @return
   */
  static double gini_imputiry(data::basic::Array2D<index4> & counts);

  GiniImpuritySplit(index4 n_class, index2 min_branch_size = 1) :
    n_class(n_class), min_branch_size_(min_branch_size), last_split_counts(2, n_class), logs("GiniImpuritySplit") {}

  double optimal_split(std::vector<LabelledObservationVector_SP> & vv, std::vector<index1> &assigned_groups);

  /// Returns the index of the column used to split data into two branches
  index2 split_column() const { return split_column_; }

  /// Returns the value that divides between the left and the right branch of this tree
  double split_point() const { return split_point_; }

  inline index2 best_left_class() const { return best_lr_class(0); }

  inline index2 best_right_class() const { return best_lr_class(1); }

  inline double split_cost() const { return split_cost_; }

  inline index4 left_counts_for_class(core::index2 which_class) const { return last_split_counts(0,which_class);}

  inline index4 right_counts_for_class(core::index2 which_class) const { return last_split_counts(1,which_class);}

  inline index2 class_cnt() const { return  n_class;}

  index1 classify(const ObservationVector & v) const;

private:
  index2 n_class;
  double split_point_;
  std::string split_string_;
  index2 split_column_;
  double split_cost_;
  index2 min_branch_size_;
  data::basic::Array2D<index4> last_split_counts;
  utils::Logger logs;

  inline index2 best_lr_class(const index1 lr_index) const {
    index4 best_cnt = 0;
    index2 best_idx = 0;
    for (index2 i = 0; i < n_class; ++i) {
      if (last_split_counts(lr_index, i) > best_cnt) {
        best_cnt = last_split_counts(lr_index, i);
        best_idx = i;
      }
    }

    return best_idx;
  }

};

typedef core::algorithms::trees::BinaryTreeNode<GiniImpuritySplit> CartNode;
typedef std::shared_ptr<core::algorithms::trees::BinaryTreeNode<GiniImpuritySplit>> CartNode_SP;

std::ostream & operator<<(std::ostream & out, const GiniImpuritySplit & splitting_point);


/** \brief Classification and regression tree
 *
 *
 */
class Cart  {
public:
  Cart(const std::vector<index2> &classes, int node_id = 0);

  index2 count_data_columns() const { return column_names.size(); }

  index2 count_classes() const { return classes.size(); }

  void train(std::vector<LabelledObservationVector_SP> & vv) { train(vv,root_sp); }

  index2 classify(const LabelledObservationVector_SP v);

  index2 classify(const ObservationVector_SP v);

  void branch_purity_ratio(double purity_ratio) { purity_ratio_ = purity_ratio; }

  const CartNode_SP root() const { return root_sp; }

  static void sort(std::vector<LabelledObservationVector_SP> & data, const index2 by_which_column);

private:
  index4 tree_nodes_index_;
  utils::Logger logs;
  std::vector<std::string> column_names;
  const std::vector<index2> &classes;
  std::vector<index1> left_right_assignment;
  double purity_ratio_; // --- stopping criterion
  index2 min_branch_size_; // --- stopping criterion

  CartNode_SP root_sp;


  bool stop_branch(const std::vector<LabelledObservationVector_SP>& data) const;

  void train(std::vector<LabelledObservationVector_SP> & vv, CartNode_SP node);
};

std::ostream & operator<<(std::ostream & out, const Cart & cart_tree);

}
}
}

#endif
