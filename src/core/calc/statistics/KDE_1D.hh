#ifndef CORE_CALC_STATISTICS_KDE_1D_HH
#define CORE_CALC_STATISTICS_KDE_1D_HH

#include <math.h>

#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>

#include <core/calc/numeric/interpolators.hh>
#include <core/calc/numeric/Interpolate1D.hh>
#include <core/calc/numeric/basic_math.hh>
#include <core/calc/structural/angles.hh>
#include <core/calc/statistics/simple_statistics.hh>

namespace core {
namespace calc {
namespace statistics {

using namespace core::calc::numeric;

/// The kernel type
typedef std::function<double(const double)> KDE_Kernel;

/// Normalization constant for the normal kernel;
const double C_NORMAL_KERNEL = 1.0 / sqrt(M_PI * 2.0);

/// Normalization constant for the von Mises kernel;
const double C_VON_MISES_KERNEL = core::calc::numeric::mod_bessel_first_kind_zero(1.0) * 2.0 * M_PI;

/** @brief Normal kernel used by Kernel Density Estimators (KDE)
 */
static const KDE_Kernel normal_kernel = [](const double x) { return exp(-x * x / 2.0) * C_NORMAL_KERNEL; };

/** @brief Von Mises kernel used by Kernel Density Estimators (KDE)
 */
static const KDE_Kernel vonmises_kernel = [](const double x) { return exp(cos(x)) * C_VON_MISES_KERNEL; };

/** @brief Simple rule to figure out the value of a normal kernel bandwidth.
 *
 * Not much worse than the plug-in rule, but way much faster.
 * @param begin - points to the first data element in a range that will be further used to construct a KDE estimator
 * @param end - points to behind the last data element in a range
 */
template<typename It>
double normal_kernel_bandwidth(It begin, It end) {
  return stdev(begin,end) * 1.05 * pow(std::distance(begin,end),-0.2);
}

/** \brief  Kernel Density Estimator in one dimension estimates a probability density function
 * of a 1D random variable.
 *
 * <p>Kernel estimator of an unknown probability density function \f$ f(x) \f$
 * for a given vector of N observations \f$ x_i, i \in [1,N] \f$ is defined as follows:</p>
 * \f[ \hat{f}_h(x) = \frac{1}{N} \sum_{1=0}^{N} \frac{1}{h\lambda_i} K(\frac{(x-x_i)}{h\lambda_i}) \f]
 * where K is some kernel and h is the bandwidth (smoothing parameter).
 * A function \f$ K(x) \f$ (kernel of the estimator) can be a standard Gaussian function with mean zero and variance 1: </p>
 * \f[ K(x) = \frac{1}{\sqrt{2\pi}} e^{-\frac{1}{2}x^2} \f]
 *
 * By default the KDE_1D::operator()(x) evaluates kernel function only for points in the range
 * \f$ [x - 5 h, x + 5 h]\f$.
 * Call KDE_1D::operator()(x, true) to use all data points in KDE evaluation.
 *
 * The following example read data points from a file (or withdraws them from a normal distribution if no file is given)
 * Then it tabulates a KDE function in a given range:
 * \include ex_KDE_1D.cc
 */
class KDE_1D  {
public:
   static constexpr double C_PARAM = 0.5; ///< Parameter used for bandwidth local adaptation (denoted as \f$\alpha \f$ in the formulas on this page)

  /** \brief Constructor creates a KDE object.
   *
   * It makes a local deep copy of data points.
   * @param data - observations used to estimate the unknown density
   * @param bandwidth - kernel bandwidth parameter  \f$h\f$
   * @param kernel - an instance of a kernel operator
   * @param is_periodic - if true, the estimated function is treated as periodic over \f$ [-\pi, \pi] \f$ range
   */
  KDE_1D(std::vector<double> &data, const double bandwidth, const KDE_Kernel kernel, const bool is_periodic = false)
    : observ(data), band(data.size(), bandwidth), h(bandwidth), kernel_func(kernel), is_periodic(is_periodic) {

    std::sort(observ.begin(),observ.end());
  }

  /** \brief Constructor creates a KDE object.
   *
   * It makes a local deep copy of data points.
   * @param begin - points to the first data element in a range that will be further used to construct a KDE estimator
   * @param end - points to behind the last data element in a range
   * @param bandwidth - kernel bandwidth parameter \f$h\f$
   * @param kernel - an instance of a kernel operator
   */
  template<typename It>
  KDE_1D(It begin, It end, const double bandwidth, const KDE_Kernel kernel, const bool use_all_points = false) :
      observ(std::distance(begin, end)), band(std::distance(begin, end), bandwidth), h(bandwidth), kernel_func(kernel) {

    std::copy(begin, end, observ.begin());
    std::sort(observ.begin(), observ.end());
  }

  /// Returns the smallest data value used to create the estimator
  double min_value() { return observ[0]; }

  /// Returns the smallest data value used to create the estimator
  double max_value() { return observ.back(); }

  /**\brief Assign a locally adapted bandwidth to each observation.
   *
   * For each point \f$x_i\f$ a separate bandwidth value \f$h_i = h\lambda_i\f$ is calculated as follows:
   * \f[
   * \lambda_i = \{ f_0(x_i) / g\}^\alpha
   * \f]
   *
   * \f[
   * \ln g = \frac{1}{N}\sum_i^N ln f_0(x_i)
   * \f]
   * where \f$h\f$ is the fixed width bandwidth, \f$f_0(x_i)\f$ is a pilot density constructed based on \f$h\f$ and
   * \f$g\f$ is a geometric mean of the \f$f_0(x_i)\f$
   * <p>Assigns a different values of bandwidth parameters for each observation.</p>
   * @see S Sain <em>Adaptive Kernel Density Estimation</em> Stata Journal,  <b>3</b> (2003) 148-156.
   */
  void adaptive_bandwidth();

  /** \brief Gives estimation of the probability density function (PDF) for a given argument.
   *
   * For the sake of speed, this method uses only these data points which are located at least
   * \f$ 3\times h_i \f$ from the argument \f$ x \f$, where \f$ h_i \f$ is the bandwidth used for i-th data point.
   * For an exact calculations, see evaluateAllKernels(double) method.
   * @param x - the x argument
   * @return PDF value
   */
  double operator()(const double x) const {

    double y = 0;
    auto i_start = std::lower_bound(observ.begin(), observ.end(), x - 5.0 * h);
    auto i_stop = std::lower_bound(observ.begin(), observ.end(), x + 5.0 * h);
    auto i_band = band.begin() + std::distance(observ.begin(),i_start);
    for (auto it = i_start; it != i_stop; ++it) {
      y += (is_periodic) ? kernel_func(structural::distance_in_radians(x, *it) / *i_band) / *i_band :
           kernel_func((x - *it) / *i_band) / *i_band;
      ++i_band;
    }

    return y / observ.size();
  }

  /** \brief Gives estimation of the probability density function (PDF) for a given argument.
   *
   * If the <code>full_range</code> argument is set to true, this method uses all the data points
   * to compute the estimate, even these which are so far from the given argument x that do not
   * affect the result. For an approximate (and thus much faster) estimation,
   *
   * @param x - the x argument
   * @param full_range - if true, evaluates the PDF using all the data points; otherwise calls <code>operator()(const double)</code>
   * @return PDF value
   * @see operator()(const double)
   */
  double operator()(const double x, const bool full_range) const {

    if (full_range) {
      double y = 0;
      for (size_t i = 0; i < observ.size(); i++) {
        y += (is_periodic) ? kernel_func(structural::distance_in_radians(x, observ[i]) / band[i]) / band[i] :
             kernel_func((x - observ[i]) / band[i]) / band[i];
      }

      return y / double(observ.size());
    } else return operator()(x);
  }

  /// Creates a spline interpolator object that interpolates this 1D KDE
  /**
   * The method constructs a spline polynomial in the range x range
   * covered by KDE data using <code>knots_per_bandwidth</code>
   * knots in a single bandwidth range.
   * In most applications such a spline is much faster that the call operator of this object
   *
   *
   * @param knots_per_bandwidth how many knots to be used i.e. how accurate the spline should be
   * @return an object that interpolates a KDE estimator
   */
  core::calc::numeric::Interpolate1D<std::vector<float>, float, CubicInterpolator<float> > create_interpolator(double knots_per_bandwidth);

private:
  std::vector<double> observ;
  std::vector<double> band; ///< local bandwidth smoothing
  double h = 1.0; ///< bandwidth
  const KDE_Kernel kernel_func;
  bool is_periodic;
};

}
}
}
/**
 * \example ex_KDE_1D.cc
 */
#endif
