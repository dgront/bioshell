/** \file expectation_maximization.hh
 * @brief fit a mixture of distribution to data by Expectation-Maximization algorithm.
 */
#ifndef CORE_CALC_STATISTICS_expectation_maximization_HH
#define CORE_CALC_STATISTICS_expectation_maximization_HH

#include <cmath>
#include <algorithm>
#include <iostream>
#include <random>
#include <vector>
#include <stdexcept>

#include <core/index.hh>
#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace statistics {

/** @brief fit a mixture of distribution to data by Expectation-Maximization algorithm.
 *
 * This method takes a vector of N-dimensional observations and fits a mixture of N-dimensional distributions to the sample. The following example
 * shows how to use this method to classify data coming from a mixture of 1D-normal and 2D-normal distributions
 *
 * Example application that fits a mixture of Von Mises distributions to angular observations (e.g. dihedral angles):
 * @include ap_fit_VonMisses_mixture.cc
 *
 * Demo testing this method:
 * @include ex_expectation_maximization.cc
 *
 * @param data -  \f$ N\f$ data points to be used for estimation
 * @param distributions -  \f$ n_f\f$ distributions to be estimated. <strong>Note</strong>, that in this implementation
 * the number of distributions \f$ n_f\f$ must be smaller than 256 (fitting greater number of distributions to the data seems ridiculous anyways)
 * @param best_distributions - provides assignment of every point to one of the estimated distributions; once the call is ended,
 *  this vector contains integers \f$ [0,n_f)\f$ referencing to one of \f$ n_f\f$ distributions.
 * @param tolerance - stopping criteria for the algorithm; at every iteration this function evaluates
 * <code> (new_likelihood - old_likelihood) / n_points </code> If this drops below <code>tolerance</code> the iterations stops
 * @param max_iterations - maximum number of iterations; the functions stops after that number of cycles even
 *  if the convergence criteria is not met
 * @tparam D - the type of distribution class
 * @return log-likelihood value, which says how well input data fits to the recovered distribution (the higher, the better);
 *  Note, that the returned value strongly depends on the number of sample size as well as on the number of distributions
 *  being fit
 */
template<typename D>
double expectation_maximization(const std::vector<std::vector<double>> &data, std::vector<D> &distributions,
                                std::vector<core::index1> &best_distributions,
                                const double tolerance = 0.01, const index2 max_iterations = 100) {

  using namespace core::calc::statistics;

  utils::Logger logs("expectation_maximization");

  if(distributions.size()>255) {
    logs << utils::LogLevel::CRITICAL << "Attempt to fit more than 255 distributions, which is not available in the current implementation!\n";
    throw std::out_of_range("Attempt to fit more than 255 distributions, which is not available in the current implementation!");
  }
  if (best_distributions.size() == 0) best_distributions.resize(data.size(), core::index1(distributions.size()));

  std::vector<std::vector<double>> backup_params;
  for (auto &d : distributions) {
    backup_params.emplace_back(d.parameters().size());
    d.copy_parameters_into(backup_params.back());
  }

  const core::index2 n_distributions = distributions.size();
  std::vector<std::vector<std::vector<double>>> BEST; // i-th inner vector contains points assigned to i-th distribution
  for (core::index4 i = 0; i < n_distributions; ++i) {
    std::vector<std::vector<double>> v;
    BEST.push_back(v);
  }
  double best_pdf_value, pdf, new_score = 0.0, score = 0.0, tmp_pdf = 0.0;
  core::index2 best_distribution_index;
  for (core::index4 z = 0; z < max_iterations; ++z) {                                //number of iterations
// --- Maximisation step ---
    for (core::index4 i = 0; i < n_distributions; ++i) BEST[i].clear();
    for (const std::vector<double> d : data) {
      best_pdf_value = - std::numeric_limits<double>::max();
      best_distribution_index = std::numeric_limits<core::index2>::max();
      for (core::index2 j = 0; j < n_distributions; ++j) {
        tmp_pdf = distributions[j].evaluate(d);
        if (best_pdf_value <= tmp_pdf) {
          best_pdf_value = tmp_pdf;
          best_distribution_index = j;
        }
      }
      if (best_distribution_index > n_distributions) logs << utils::LogLevel::WARNING << "missing point\n";
      BEST[best_distribution_index].push_back(d);
    }
// --- Expectation step ---
    new_score = 0.0;
    for (core::index4 which_distr = 0; which_distr < n_distributions; ++which_distr) {     //'which_distr' is the index of a distribution
      distributions[which_distr].estimate(BEST[which_distr]);
      for (const std::vector<double> di : BEST[which_distr]) {
        pdf = distributions[which_distr].evaluate(di);
        new_score += log(pdf);                                    //suma logarytów prawodpodobieństwa
      }
    }

    logs << utils::LogLevel::FINE << "Best score at iteration " << int(z) << " : " << new_score << "\n";
    if ((z == 0) && (new_score < 0)) score = 2 * new_score;
    if (tolerance < ((new_score - score) / data.size())) {
      score = new_score;
      for (core::index4 d = 0; d < distributions.size(); ++d) distributions[d].copy_parameters_into(backup_params[d]);
    } else {
      double best_pdf, tmp;
      core::index4 best_index, ix = 0;
      for (core::index4 j = 0; j < n_distributions; ++j) distributions[j].copy_parameters_from(backup_params[j]);
      for (const std::vector<double> d : data) {
        best_pdf = 0.0;
        best_index = 500;
        for (core::index4 j = 0; j < n_distributions; ++j) {
          tmp = distributions[j].evaluate(d);
          if (best_pdf <= tmp) {
            best_pdf = tmp;
            best_index = j;
          }
        }
        best_distributions[ix] = best_index;
        ++ix;
      }
      break;
    }
  }

  return new_score;
}

}
}
}

/**
 * @example ex_expectation_maximization.cc
 * @example ap_fit_VonMisses_mixture.cc
 */
#endif
