#ifndef CORE_CALC_STATISTICS_Hexbins_HH
#define CORE_CALC_STATISTICS_Hexbins_HH

#include <math.h>
#include <string>
#include <cmath>
#include <utility>
#include <map>

#include <core/index.hh>
#include <utils/Logger.hh>

namespace core {
namespace calc {
namespace statistics {

/** \brief Hexbins.
 *
 * @tparam D - type of the observed data
 * @tparam C - type representing histogram counts, also used for data weights. So if this is an integer type,
 *    the data weights must also be integer numbers
 */
template <typename D,typename C>
class Hexbins  {
public :
  static const std::string OUTPUT_VALUE_FORMAT; ///< Output format that is used to write bin values
  static const std::string OUTPUT_COUNTS_FORMAT; ///< Output format that is used to write counts for each bin
  static const std::string OUTPUT_SEPARATOR; ///< Separator between a bin value and the number of counts in this bin

  /** \brief Creates histogram of  desired bin's width
   *
   * All bins will have equal width, given as:
   * \f[ n = \lceil (x_{max} - x_{min} / n)\rceil  \f]
   * where \f$ n \f$ is the number of bins and \f$\lceil x \rceil\f$ denotes <code>ceil(x)</code>.
   * @param bin_side - width of each bin
   * @param min - the minimum of the range covered by the histogram bins
   * @param max - the maximum value of the range covered by the histogram bins
   */
  Hexbins(D bin_side) : logger("HexaHistogram") {

    bin_width_y = bin_width_x = bin_side;
    n_entries = 0;
    n_outside = 0;
  }

  /** \brief Constructor that automatically assigns the range of a histogram and inserts given data.
   *
   * All bins will have equal width, given as:
   * \f[ \delta = (x_{max} - x_{min}) / n \f]
   * where \f$ n \f$ is the number of bins and <code>min</code>,<code>max</code>
   * are the minimum and maximum value in the given sample, respectively.
   * @param data - values to be inserted into the histogram
   * @param nbins - the number of bins the histogram should have. The
   * range specified by min and max will be divided up into this
   * many bins.
   */
  Hexbins(std::vector<D> data, const D bin_size) : Hexbins(bin_size) { insert(data); }

  /** @brief Copy assignment operator
   *
   * @param src - a source object
   * @return reference to this object (after assignment)
   */
  Hexbins<D, C> &operator=(const Hexbins<D, C> &src) {

    bin_width_y = src.bin_width_y;
    bin_width_x = src.bin_width_x;
    n_entries = src.n_entries;
    min_bin_x = src.min_bin_x;
    min_bin_y = src.min_bin_y;
    max_bin_x = src.max_bin_x;
    max_bin_y = src.max_bin_y;
    histogram_data.clear();
    histogram_data.insert(src.histogram_data.begin(), src.histogram_data.end());
  }

  /// Clear the histogram.
  void clear() { histogram_data.clear(); }

  /** @brief Set the side length of each hexagon bin
   * @param bin_side - length of a bin side
   */
  void bin_side(const D bin_side) { bin_width_y = bin_width_x = bin_side; }

  /** @brief Returns the side length of each hexagon bin
   * @param bin_side - length of a bin side
   */
  D bin_side(void) const { return bin_width_x; }

  /** @brief Set the minimum bin index allowed in X direction
   * @param min_x_bin - minimum bin index allowed in X direction
   */
  void min_x_bin(const int min_x_bin) { min_bin_x = min_x_bin; }

  /** @brief Set the minimum bin index allowed in Y direction
   * @param min_y_bin - minimum bin index allowed in Y direction
   */
  void min_y_bin(const int min_y_bin) { min_bin_y = min_y_bin; }

  /** @brief Set the maximum bin index allowed in X direction
   * @param max_x_bin - maximum bin index allowed in X direction
   */
  void max_x_bin(const int max_x_bin) { max_bin_x = max_x_bin; }

  /** @brief Set the maximum bin index allowed in Y direction
   * @param max_y_bin - maximum bin index allowed in Y direction
   */
  void max_y_bin(const int max_y_bin) { max_bin_y = max_y_bin; }

  /** \brief Finds the highest bin in this histogram.
   *
   * The highest bin features the greatest number of observations. The actual number of counts
   * may be obtained by <code>checkBin()</code> method:
   * \code
   * double maxCounts = histogram.checkBin(histogram.highestBin());
   * \endcode
   *
   * @return zero-related bin number which features the greatest number of observations.
   */
  std::pair<short,short> highest_bin() const {

    std::pair<short,short> maxbin;
    C maxval = 0;

    for(const auto & bin:histogram_data) {
      if (bin.second > maxval) {
        maxbin = bin.first;
        maxval = bin.second;
      }
    }
    return maxbin;
  }

  /** \brief Inserts an observation into this histogram
   *
   * Enter data into the histogram. The fill method takes the given \f$ (x,y) \f$ pair of values,
   * finds out which bin this corresponds to, and increments this bin by one.
   * @param x is the value to add in to the histogram
   * @param y is the value to add in to the histogram
   * @return a bin key where the value belong to
   */
  inline void insert(const D x,const D y) {

    bin_index(x, y, tmp_bin_index);
    if ((tmp_bin_index.first < min_bin_x) || (tmp_bin_index.first > max_bin_x)) {
      ++n_outside;
      return;
    }
    if ((tmp_bin_index.second < min_bin_y) || (tmp_bin_index.second > max_bin_y)) {
      ++n_outside;
      return;
    }
    if (histogram_data.find(tmp_bin_index) == histogram_data.end()) histogram_data[tmp_bin_index] = 1;
    else histogram_data[tmp_bin_index]++;
    n_entries++;
  }

  /** \brief Inserts an observation into this histogram.
   *
   * Enter data into the histogram. The fill method takes the given \f$ (x,y) \f$ pair of values,
   * finds out which bin this corresponds to, and increments this bin by one.
   * The bin_id is stored in a given tuple.
   *
   * @param x is the value to add in to the histogram
   * @param y is the value to add in to the histogram
   * @param bin_id - points to the histogram bin a given observation was inserted
   * @return a bin key where the value belong to
   */
  inline void insert(const D x,const D y, std::pair<short, short> & bin_id) {

    bin_index(x, y, bin_id);
    if ((bin_id.first < min_bin_x) || (bin_id.first > max_bin_x)) {
      ++n_outside;
      return;
    }
    if ((bin_id.second < min_bin_y) || (bin_id.second > max_bin_y)) {
      ++n_outside;
      return;
    }
    if (histogram_data.find(bin_id) == histogram_data.end()) histogram_data[bin_id] = 1;
    else histogram_data[bin_id]++;
    n_entries++;
  }

  /** \brief Inserts an array of observations into this histogram
   *
   * Enter data into the histogram. The method works as insert(D val)
   * but takes an array as an argument
   * @param data is the array of values to insert into this histogram
   */
  inline void insert(const std::vector<D> data) { for(D d : data) insert(d); }

  /** \brief Inserts a weighted observation into this histogram
   *
   * Enter data into the histogram. The insert  method takes the given
   * value, works out which bin this corresponds to, and increments
   * this bin by the part value.
   * @param x - the value to add in to the histogram
   * @param part - what part to add in to the histogram (i.e. its weight)
   */
  inline void insert(const D x, const D y, const C part) {

    bin_index(x, y, tmp_bin_index);
    if ((tmp_bin_index.first < min_bin_x) || (tmp_bin_index.first > max_bin_x)) {
      ++n_outside;
      return;
    }
    if ((tmp_bin_index.second < min_bin_y) || (tmp_bin_index.second > max_bin_y)) {
      ++n_outside;
      return;
    }
    if (histogram_data.find(tmp_bin_index) == histogram_data.end()) histogram_data[tmp_bin_index] = part;
    else histogram_data[tmp_bin_index] += part;

    // count the number of entries made by the fill method
    n_entries += part;
  }

  /** \brief Inserts a weighted observation into this histogram
   *
   * Enter data into the histogram. The insert  method takes the given
   * value, works out which bin this corresponds to, and increments
   * this bin by the part value.
   * @param x - the value to add in to the histogram
   * @param part - what part to add in to the histogram (i.e. its weight)
   */
  inline void insert(const D x, const D y, const C part, std::pair<short, short> & bin_id) {

    bin_index(x, y, bin_id);
    if ((bin_id.first < min_bin_x) || (bin_id.first > max_bin_x)) {
      ++n_outside;
      return;
    }
    if ((bin_id.second < min_bin_y) || (bin_id.second > max_bin_y)) {
      ++n_outside;
      return;
    }
    if (histogram_data.find(bin_id) == histogram_data.end()) histogram_data[bin_id] = part;
    else histogram_data[bin_id] += part;

    // count the number of entries made by the fill method
    n_entries += part;
  }

  /** \brief Inserts a weighted observation into this histogram
   *
   * Enter data into the histogram. The insert  method takes the given
   * value, works out which bin this corresponds to, and increments
   * this bin by the part value.
   * @param x - the value to add in to the histogram
   * @param part - what part to add in to the histogram (i.e. its weight)
   */
  inline void insert(const short ix, const short iy, const C part) {

    if ((ix < min_bin_x) || (ix > max_bin_x)) {
      ++n_outside;
      return;
    }
    if ((iy < min_bin_y) || (iy> max_bin_y)) {
      ++n_outside;
      return;
    }
    tmp_bin_index.first = ix;
    tmp_bin_index.second = iy;
    if (histogram_data.find(tmp_bin_index) == histogram_data.end()) histogram_data[tmp_bin_index] = part;
    else histogram_data[tmp_bin_index] += part;

    // count the number of entries made by the fill method
    n_entries += part;
  }

  /** \brief Divide each bin by the area under this histogram.
   *
   * <strong>Note</strong>, that this operation makes sense only if the histogram bins were declared as a double type
   */
  inline void normalize() { for (auto & bin : histogram_data) histogram_data.second /= n_entries; }

  std::pair<D, D> bin_center(const std::pair<short, short> & bin) const {

    if (int(abs(bin.second)) % 2 != 0) return std::make_pair((bin.first + 0.5) * bin_width_x * sqrt(3.0),
        bin_width_y * bin.second * 1.5);
    else return std::make_pair(bin.first * bin_width_x * sqrt(3.0), bin_width_y * bin.second * 1.5);
  }

  /** \brief Return number of observations for a bin where a certain pair of values belongs to.
   * @param x - is the value pointing for a demanded bin (X direction)
   * @param y - is the value pointing for a demanded bin (Y direction)
   * @return bin value or 0 if the value is outside the range covered by this histogram.
   */
  inline C operator()(const D x,const D y) const {

    std::pair<short, short> i_bin = bin_index(x, y);
    if (histogram_data.find(i_bin) != histogram_data.end()) return histogram_data.at(i_bin);

    return 0;
  }

  /** \brief Return number of observations for a certain bin.
   * @param i - index of a bin - X direction
   * @param j - index of a bin - Y direction
   * @return bin value
   */
  inline C operator()(const short i, const short j) const {

    std::pair<short, short> i_bin = std::make_pair(i,j);
    if (histogram_data.find(i_bin) != histogram_data.end()) return histogram_data.at(i_bin);

    return 0;
  }

  /** \brief Return number of observations for a certain bin .
   * @param ij - a pair indexing  a bin
   * @return bin value
   */
  inline C operator()(const std::pair<short,short> & ij) const {

    if (histogram_data.find(ij) != histogram_data.end()) return histogram_data.at(ij);

    return 0;
  }

  /** \brief Returns const-iterator pointing to the first bin in this histogram.
   * @return iterator pointing to the first bin.
   */
  typename std::map<std::pair<short,short>,C>::const_iterator cbegin() const { return histogram_data.cbegin(); }

  /** \brief Returns pass-the-end const-iterator that marks the end of all bins of this histogram.
   * @return  pass-the-end iterator for bin.
   */
  typename std::map<std::pair<short,short>,C>::const_iterator cend() const { return histogram_data.cend(); }

  /** \brief Returns bin index for a given data.
   *
   * @param x - is the value pointing for a demanded bin (X direction)
   * @param y - is the value pointing for a demanded bin (Y direction)
   * @return pair containing two bin indexes (ix,iy)
   */
  std::pair<short,short> bin_index(const D x, const D y) const {

    std::pair<short, short> hash = std::make_pair<short, short>(0, 0);
    bin_index(x, y, hash);
    return hash;
  }

  /** \brief Finds bin index for a given data.
   *
   * Resulted integer bin coordinates (i,j) are stored in the given <code>bin_index</code> object
   *
   * @param x - is the value pointing for a demanded bin (X direction)
   * @param y - is the value pointing for a demanded bin (Y direction)
   * @param bin_index - points to the histogram bin a given observation was inserted
   */
  void bin_index(const D x, const D y, std::pair<short, short> & bin_index) const {

    std::pair<D, D> px_nearest = nearest_bins_centerpoint(x, bin_width_x * sqrt(3.0)/2);
    std::pair<D, D> py_nearest = nearest_bins_centerpoint(y, bin_width_y*1.5);

    D d1 = distance(x, y, px_nearest.first, py_nearest.first);
    D d2 = distance(x, y, px_nearest.second, py_nearest.second);

    D z1,z2;
    if (d1 < d2) {
      z1 = px_nearest.first/ sqrt(3.0)/bin_width_x;
      z2 = py_nearest.first/1.5/bin_width_y;
    }
    else {
      z1 = px_nearest.second/ sqrt(3.0)/bin_width_x;
      z2 = py_nearest.second/1.5/bin_width_y;
    }

    D part_i,part_f;
    part_f = modf(z1 + ( (z1 > 0) ? 0.0001 : -0.0001), &part_i);
    if (fabs(part_f) > 0.01) z1 -= 0.5;
    short i1 = (z1 < 0) ? short(z1 - 0.01) : short(z1 + 0.01);
    short i2 = (z2 < 0) ? short(z2 - 0.01) : short(z2 + 0.01);

    bin_index.first = i1;
    bin_index.second = i2;
  }

  /** \brief Returns coordinates of six vertices for a given hexagon.
   *
   * @param bin - pair containing two bin indexes (ix,iy)
   * @param coordinates - vector where the coordinates will be stored
   */
  void bin_vertices(const std::pair<short,short> & bin, std::vector<std::pair<double,double>> & coordinates) {

    static const double s32 = sqrt(3.0) / 2.0;
    short i = bin.first;
    short j = bin.second;
    double px = (j % 2 == 0) ? i + 0.5 : i;
    double x0 = px * bin_width_x * 2 * s32;
    double y0 = j * bin_width_y * 1.5;
    coordinates.clear();
    coordinates.emplace_back(0 + x0, bin_width_y + y0);
    coordinates.emplace_back(bin_width_x * s32 + x0, bin_width_y /2 + y0);
    coordinates.emplace_back(bin_width_x * s32 + x0, -bin_width_y /2 + y0);
    coordinates.emplace_back(0+ x0, -bin_width_y  + y0);

    coordinates.emplace_back(-bin_width_x * s32 + x0, -bin_width_y /2 + y0);
    coordinates.emplace_back(-bin_width_x * s32 + x0, bin_width_y/2+ y0);
  }

  /** \brief Get number of entries in the histogram.
   *
   * The result should correspond to the number of times the <code>insert()</code> method has been used.
   * @return number of entries
   */
  C count_entries() const { return n_entries; }

  /** \brief Get the number of bins in the histogram.
   *
   * The range of the histogram defined by min and max is divided into this many bins.
   * @return number of bins
   */
  core::index4 count_bins() const { return histogram_data.size(); }

  /** \brief Get number of entries that felt out of the histogram.
   *
   * @return number of unsuccessfully inserted entries (data points out of range)
   */
  core::index4 count_outside() const { return n_outside; }

protected:
  D bin_width_x;
  D bin_width_y;
  C n_entries;
  C n_outside;

private:
  utils::Logger logger;
  int min_bin_x = std::numeric_limits<short>::min();
  int max_bin_x = std::numeric_limits<short>::max();
  int min_bin_y = std::numeric_limits<short>::min();
  int max_bin_y = std::numeric_limits<short>::max();
  std::pair<short, short> tmp_bin_index;
  std::map <std::pair<short, short>, C> histogram_data;

  template<typename T>
  int sgn(const T val) const { return (T(0) < val) - (val < T(0)); }

  /// Detect the half-stripe a point falls to
  std::pair <D, D> nearest_bins_centerpoint(const D value, const D width) const {

    const D w = width;
    int sign = sgn(value);
    int div_ = int(fabs(value) / w);
    if (sign < 0) ++div_;
    D rounded = w * (sign * div_ + ((div_ % 2 == 1) ? 1 : 0));
    D rounded_scaled = w * (sign * div_ + ((div_ % 2 == 1) ? 0 : 1));

    return std::make_pair(rounded, rounded_scaled);
  }

	inline D distance(const D xa,const D ya,const D xb,const D yb) const { return (xa-xb) * (xa-xb) + (ya-yb) * (ya-yb); };
};

template <typename D,typename C>
const std::string Hexbins<D,C>::OUTPUT_VALUE_FORMAT = "%8.3f";

template <typename D,typename C>
const std::string Hexbins<D,C>::OUTPUT_COUNTS_FORMAT = "%8.3f";

template <typename D,typename C>
const std::string Hexbins<D,C>::OUTPUT_SEPARATOR = " ";

}
}
}

#endif
