#ifndef CORE_CALC_NUMERIC_DerivableFunction_HH
#define CORE_CALC_NUMERIC_DerivableFunction_HH

#include <core/calc/numeric/Function.hh>

namespace core {
namespace calc {
namespace numeric {

/** @brief Pure virtual definition of a generic multivariate function with gradient
 */
template <typename E>
class DerivableFunction : public Function<E> {
public:

  /** @brief Call a multivariate function.
   *
   * @param x function's argument
   * @return function value \f$ f(\vec{x}) \f$
   */
  virtual E operator()(const std::vector<E> & x) = 0;

  /** @brief Call a multivariate function and evaluate its gradient as well as function value.
   * @param x function's argument
   * @param gradient - evaluated gradient of this function will be stored in that vector
   * @return function value \f$ f(\vec{x}) \f$
   */
  virtual E operator()(const std::vector<E> & x, std::vector<E> & gradient) = 0;
};

}
}
}

#endif
