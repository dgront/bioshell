/** @file Quaternion.hh provides Quaternion data type with mathematical operations for it
 *
 */
#ifndef CORE_CALC_NUMERIC_QUATERNION_H
#define CORE_CALC_NUMERIC_QUATERNION_H

#include <cmath>
#include <core/data/basic/Vec3.hh>

namespace core {
namespace calc {
namespace numeric {

/** @brief Quaternion data structure with its basic operations
 */
struct __attribute__ (( aligned (16))) Quaternion {  // warning: gcc specific alignment flag
  double a = 0.0; ///< the first component of this quaternion
  double i = 0.0;///< the second component of this quaternion
  double j = 0.0;///< the third component of this quaternion
  double k = 0.0;///< the fourth component of this quaternion

  /// Default constructor
  Quaternion() {}

  // get_atom_coords relies on this constructor NOT normalizing the quaternion
  /** @brief Initialising constructor
   *
   * @param a - the first component of this quaternion
   * @param i - the second component of this quaternion
   * @param j - the third component of this quaternion
   * @param k - the fourth component of this quaternion
   */
  Quaternion(double a, double i, double j, double k) : a(a), i(i), j(j), k(k) {}

  /** @brief Initialising constructor
   *
   * @param v - vector used to initialise i, j and k components; a is set to 0
   */
  Quaternion(const core::data::basic::Vec3 &v) : a(0), i(v.x), j(v.y), k(v.z) {}

  /// Normalize this quaternion
  inline void norm();

  /// Find conjugated quaternion
  inline Quaternion conjugate() const;

  /// add a quaternion to this one
  inline Quaternion operator+(Quaternion r);

  /// multiply this quaternion by another one
  inline Quaternion &operator*=(const Quaternion &r);


  /// subtract a quaternion from this one
  inline Quaternion operator-(Quaternion r);

  // warning: treats this quaternion as a vector
  // WARNING: do NOT normalize the quaternion before applying this
  inline void rotate_by(const Quaternion &rot);

  /// create a random quaternion
  void random(double max_angle);

  /// create a random quaternion
  void random();

  /// Apply rotation of this quaternion to a given vector
  void apply(core::data::basic::Vec3 &v) {
    
    using core::data::basic::Vec3;
    Vec3 u{float(i), float(j), float(k)};
    Vec3 ou{u}, ov{v}, oc;
    ou *= 2.0f * u.dot_product(v); 
    ov *= a*a - u.dot_product(u);
    cross_product(u,v,oc);
    oc *= 2.0f * a;
    ou += ov;
    ou += oc;
    v = ou;
  }
  
  /// create a quaternion that rotates around a given axis by a given angle
  static Quaternion create_from_axis_angle(const double &xx, const double &yy, const double &zz, const double &a);
};

inline Quaternion &Quaternion::operator*=(const Quaternion &r) {
  double aa = a * r.a - i * r.i - j * r.j - k * r.k;
  double ii = a * r.i + i * r.a + j * r.k - k * r.j;
  double jj = a * r.j - i * r.k + j * r.a + k * r.i;
  k = a * r.k + i * r.j - j * r.i + k * r.a;
  a = aa;
  i = ii;
  j = jj;
  return *this;
}


inline Quaternion operator*(Quaternion l, const Quaternion &r) {
  l *= r;
  return l;
}

inline void Quaternion::norm() {
  double d = a * a + i * i + j * j + k * k;
  d = sqrt(d);
  a /= d;
  i /= d;
  j /= d;
  k /= d;
}

inline Quaternion Quaternion::operator+(Quaternion r) {
  Quaternion ret;
  ret.a = a + r.a;
  ret.i = i + r.i;
  ret.j = j + r.j;
  ret.k = k + r.k;
  return ret;
}

inline Quaternion Quaternion::operator-(Quaternion r) {
  Quaternion ret;
  ret.a = a - r.a;
  ret.i = i - r.i;
  ret.j = j - r.j;
  ret.k = k - r.k;
  return ret;
}

inline Quaternion Quaternion::conjugate() const {
  Quaternion ret(a, -i, -j, -k);
  return ret;
}

inline void Quaternion::rotate_by(const Quaternion &rot) {
  a = 0.; // it should be 0 anyway, just making sure
  *this = rot * (*this) * rot.conjugate();
}

} // ~ numeric
} // ~ calc
} // ~ core

#endif
