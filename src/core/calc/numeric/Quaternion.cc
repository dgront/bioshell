#include <core/calc/statistics/Random.hh>
#include <core/calc/numeric/Quaternion.hh>

namespace core {
namespace calc {
namespace numeric {

Quaternion Quaternion::create_from_axis_angle(const double &xx, const double &yy, const double &zz, const double &a) {

  // Here we calculate the sin( theta / 2) once for optimization
  double factor = sin( a / 2.0 );

  // Calculate the x, y and z of the quaternion
  double x = xx * factor;
  double y = yy * factor;
  double z = zz * factor;

  // Calcualte the w value by cos( theta / 2 )
  double w = cos( a / 2.0 );

  Quaternion q(w, x, y, z);
  q.norm();
  return q;
}


void Quaternion::random(double max_angle) {

  core::calc::statistics::Random & rd = core::calc::statistics::Random::get();
  core::calc::statistics::UniformRealRandomDistribution<double> r(0,1.0);
  //random spherical coordinates
  double theta = 2 * M_PI * (double) r(rd);
  double phi = acos(2 * (double) r(rd));

  //conversion to Cartesian coordinates
  i = cos(theta) * sin(phi);
  j = sin(theta) * sin(phi);
  k = cos(phi);
  a = max_angle * (2 * (double) r(rd));

  //creating a Quaternion with previously randomized vector and angle
  double sina = sin(a);
  i *= sina;
  j *= sina;
  k *= sina;
  a = cos(a);
};

void Quaternion::random() {

  core::calc::statistics::Random & rd = core::calc::statistics::Random::get();
  core::calc::statistics::UniformRealRandomDistribution<double> r(0,1.0);

  double u1 = (double) r(rd);
  double u2 = (double) r(rd);
  double u3 = (double) r(rd);
  double sqrt1 = sqrt(1 - u1);
  double sqrt2 = sqrt(u1);
  a = sqrt1 * sin(2 * M_PI * u2);
  i = sqrt1 * cos(2 * M_PI * u2);
  j = sqrt2 * sin(2 * M_PI * u3);
  k = sqrt2 * cos(2 * M_PI * u3);
  norm();
}

} // ~ numeric
} // ~ calc
} // ~ core

