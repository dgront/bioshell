#include <iostream>
#include <fstream>
#include <vector>

#include <core/calc/numeric/LBFGS.hh>
#include <core/calc/numeric/basic_algebra.hh>

namespace core {
namespace calc {
namespace numeric {

template<typename T>
T column_times_vector(const core::data::basic::Array2D<T> &m, const index2 col, const std::vector<T> &v) {

  T out = 0.0;
  for (index2 ii = 0; ii < v.size(); ++ii) out += m(ii, col) * v[ii];

  return out;
}

template double column_times_vector(const core::data::basic::Array2D<double> &m, const index2 col, const std::vector<double> &v);

template float column_times_vector(const core::data::basic::Array2D<float> &m, const index2 col, const std::vector<float> &v);

template<typename T>
void LBFGS<T>::line_search(DerivableFunction<T> &f, T &fx, std::vector<T> &x, std::vector<T> &grad,
                          T &step, const std::vector<T> &drt, const std::vector<T> &xp) {

  // Decreasing and increasing factors
  const T dec = 0.5;
  const T inc = 2.1;

  const T fx_init = fx; // --- Copy the function value at the current x
  const T dg_init = dot(grad, drt); // --- // Projection of gradient on the search direction

  const T dg_test = ftol * dg_init;
  T width;

  for (index2 iter = 0; iter < max_linesearch; iter++) {

    for (size_t i = 0; i < x.size(); ++i) x[i] = xp[i] + step * drt[i];

    fx = f(x, grad);

    if (fx > fx_init + step * dg_test) {
      width = dec;
    } else {
      // Armijo condition is met
      if (linesearch == LBFGS_LINESEARCH_BACKTRACKING_ARMIJO) break;

      const T dg = dot(grad, drt);
      if (dg < wolfe * dg_init) {
        width = inc;
      } else {
        // Regular Wolfe condition is met
        if (linesearch == LBFGS_LINESEARCH_BACKTRACKING_WOLFE)break;
        if (dg > -wolfe * dg_init) width = dec;
        else break;
      }
    }

    if (iter >= max_linesearch)
      throw std::runtime_error("the line search routine reached the maximum number of iterations");

    if (step < min_step)
      throw std::runtime_error("the line search step became smaller than the minimum value allowed");

    if (step > max_step)
      throw std::runtime_error("the line search step became larger than the maximum value allowed");

    step *= width;
  }
}

template<typename T>
LBFGS<T>::LBFGS(index2 n) : n(n), m(6),  m_s(n, m), m_y(n, m),logs("LBFGS") {

  m_ys.resize(m, 0.0);
  m_alpha.resize(m, 0.0);
  m_xp.resize(n, 0.0);
  m_grad.resize(n, 0.0);
  m_gradp.resize(n, 0.0);
  m_drt.resize(n, 0.0);
  m = 6;
  epsilon = T(1e-5);
  past = 0;
  delta = T(0);
  max_iterations = 0;
  linesearch = LBFGS_LINESEARCH_BACKTRACKING_ARMIJO;
  max_linesearch = 20;
  min_step = T(1e-20);
  max_step = T(1e+20);
  ftol = T(1e-4);
  wolfe = T(0.9);
}

template<typename T>
int LBFGS<T>::minimize(core::calc::numeric::DerivableFunction<T> &f, std::vector<T> &x, T &fx) {

  const int fpast = past;

  // Evaluate function and compute gradient
  fx = f(x, m_grad);
  T xnorm = norm(x);
  T gnorm = norm(m_grad);
  if (fpast > 0) m_fx[0] = fx;

  // Early exit if the initial x is already a minimizer
  if (gnorm <= epsilon * std::max(xnorm, T(1.0))) return 1;

  // Initial direction
  for (index2 i = 0; i < m_drt.size(); ++i) m_drt[i] = -m_grad[i]; // m_drt.noalias() = -m_grad;
  // Initial step
  T step = T(1.0) / norm(m_drt);

  int k = 1;
  int end = 0;
  for (int n_iter = 0; n_iter < 50; ++n_iter) {
    // Save the curent x and gradient
    for (index2 i = 0; i < x.size(); ++i) {
      m_xp[i] = x[i];
      m_gradp[i] = m_grad[i];
    }

    // Line search to update x, fx and gradient
    line_search(f, fx, x, m_grad, step, m_drt, m_xp);
    logs << utils::LogLevel::FINE << "Current minimum: " << fx << "\n";
    // New x norm and gradient norm
    xnorm = norm(x);
    gnorm = norm(m_grad);

    // Convergence test -- gradient
    if (gnorm <= epsilon * std::max(xnorm, T(1.0))) {
      return k;
    }
    // Convergence test -- objective function value
    if (fpast > 0) {
      if (k >= fpast && std::abs((m_fx[k % fpast] - fx) / fx) < delta)
        return k;

      m_fx[k % fpast] = fx;
    }
    // Maximum number of iterations
    if (max_iterations != 0 && k >= max_iterations) return k;

    // Update s and y
    for (index2 ii = 0; ii < n; ++ii) {
      m_s(ii, end) = x[ii] - m_xp[ii];
      m_y(ii, end) = m_grad[ii] - m_gradp[ii];
    }
    T ys = 0.0;
    T yy = 0.0;
    for (index2 ii = 0; ii < n; ++ii) {
      ys += m_y(ii, end) * m_s(ii, end);
      yy += m_y(ii, end) * m_y(ii, end);
    }
    m_ys[end] = ys;

    // Recursive formula to compute d = -H * g
    for (index2 i = 0; i < m_drt.size(); ++i) m_drt[i] = -m_grad[i];

    int bound = std::min(m, k);
    end = (end + 1) % m;
    int j = end;
    for (int i = 0; i < bound; i++) {
      j = (j + m - 1) % m;
      m_alpha[j] = column_times_vector(m_s, j, m_drt) / m_ys[j];
      for (index2 ii = 0; ii < m_drt.size(); ++ii) m_drt[ii] -= m_alpha[j] * m_y(ii, j);
    }

    for (index2 ii = 0; ii < m_drt.size(); ++ii) m_drt[ii] *= (ys / yy);

    for (int i = 0; i < bound; i++) {
      T beta = column_times_vector(m_y, j, m_drt);
      beta /= m_ys[j];
      for (index2 ii = 0; ii < m_drt.size(); ++ii) {
        m_drt[ii] += (m_alpha[j] - beta) * m_s(ii, j);
      }
      j = (j + 1) % m;
    }

    step = T(1.0);
    k++;
  }

  return k;
}


template
class LBFGS<double>;

template
class LBFGS<float>;

}
}
}