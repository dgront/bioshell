#include <core/calc/numeric/Pca3.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/calc/numeric/basic_algebra.hh>

namespace core {
namespace calc {
namespace numeric {

Pca3::Pca3(const std::vector<core::data::structural::PdbAtom_SP> & input_atoms_sp) {

  Vec3 c; // --- Center the input points
  for (core::index2 i = 0; i < input_atoms_sp.size(); ++i) center += *input_atoms_sp[i];
  center /= (double(input_atoms_sp.size()));

  // --- calculate covariance matrix
  Vec3 p;
  double xx = 0, xy = 0, xz = 0, yy = 0, yz = 0, zz = 0;
  for (size_t k = 0; k < input_atoms_sp.size(); ++k) {
    p.set(*input_atoms_sp[k]);
    p -= center;

    xx += p.x * p.x;
    yy += p.y * p.y;
    zz += p.z * p.z;
    xy += p.x * p.y;
    xz += p.x * p.z;
    yz += p.y * p.z;
  }

  xx /= (input_atoms_sp.size() - 1.0);
  yy /= (input_atoms_sp.size() - 1.0);
  zz /= (input_atoms_sp.size() - 1.0);
  xy /= (input_atoms_sp.size() - 1.0);
  xz /= (input_atoms_sp.size() - 1.0);
  yz /= (input_atoms_sp.size() - 1.0);
  core::data::basic::Array2D<double> cov(3, 3, {xx, xy, xz, xy, yy, yz, xz, yz, zz});

  core::calc::numeric::eigenvalues3(cov, eigenval);
  std::sort(eigenval.begin(), eigenval.end(), [](double a, double b) { return a > b; });
  eigenvec = core::calc::numeric::eigenvectors3(cov, eigenval);
  eigenvec[0].norm();
  eigenvec[1].norm();
  eigenvec[2].norm();
}

Pca3::Pca3(const std::vector<core::data::basic::Vec3> & input_vectors) {

  Vec3 c; // --- Center the input points
  for (core::index2 i = 0; i < input_vectors.size(); ++i) center += input_vectors[i];
  center /= (double(input_vectors.size()));

  // --- calculate covariance matrix
  Vec3 p;
  double xx = 0, xy = 0, xz = 0, yy = 0, yz = 0, zz = 0;
  for (size_t k = 0; k < input_vectors.size(); ++k) {
    p.set(input_vectors[k]);
    p -= center;

    xx += p.x * p.x;
    yy += p.y * p.y;
    zz += p.z * p.z;
    xy += p.x * p.y;
    xz += p.x * p.z;
    yz += p.y * p.z;
  }

  xx /= (input_vectors.size() - 1.0);
  yy /= (input_vectors.size() - 1.0);
  zz /= (input_vectors.size() - 1.0);
  xy /= (input_vectors.size() - 1.0);
  xz /= (input_vectors.size() - 1.0);
  yz /= (input_vectors.size() - 1.0);
  core::data::basic::Array2D<double> cov(3, 3, {xx, xy, xz, xy, yy, yz, xz, yz, zz});

  core::calc::numeric::eigenvalues3(cov, eigenval);
  std::sort(eigenval.begin(), eigenval.end(), [](double a, double b) { return a > b; });
  eigenvec = core::calc::numeric::eigenvectors3(cov, eigenval);
  eigenvec[0].norm();
  eigenvec[1].norm();
  eigenvec[2].norm();
}

core::calc::structural::transformations::Rototranslation Pca3::create_transformation() const {

  core::calc::structural::transformations::Rototranslation rt;
  rt.rot_x(eigenvec[0].x,eigenvec[0].y,eigenvec[0].z);
  rt.rot_y(eigenvec[1].x,eigenvec[1].y,eigenvec[1].z);
  rt.rot_z(eigenvec[2].x,eigenvec[2].y,eigenvec[2].z);
  rt.tr_before(center);

  return rt;
}

}
}
}
