#ifndef CORE_CALC_NUMERIC_DerivableFunction1D_HH
#define CORE_CALC_NUMERIC_DerivableFunction1D_HH

#include <core/calc/numeric/Function1D.hh>

namespace core {
namespace calc {
namespace numeric {

/** @brief Pure virtual definition of a generic univariate function with derivative
 */
template <typename E>
class DerivableFunction1D : public Function1D<E> {
public:

  /** @brief Call an univariate function and evaluate its derivative as well as function value.
   * @param x function's argument
   * @param derivative - evaluated derivative of this function will be stored in that value
   * @return function value \f$ f(x) \f$
   */
  virtual E operator()(const E x, E & derivative) const = 0;
};

}
}
}

#endif
