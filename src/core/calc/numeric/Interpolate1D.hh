#ifndef CORE_CALC_NUMERIC_Interpolate1D_HH
#define CORE_CALC_NUMERIC_Interpolate1D_HH

#include <exception>
#include <core/index.hh>
#include <utils/string_utils.hh>
#include <core/calc/numeric/Function1D.hh>

namespace core {
namespace calc {
namespace numeric {

/** @brief Interpolates a function based on given points placed on a regular grid in 1D.
 *
 * @tparam D - the type of the data container that holds the points for interpolations, e.g. std::vector<E>
 * @tparam E - the type of data being interpolated (typically double or float)
 * @tparam I - the type of interpolator to be used, e.g. @code CatmullRomInterpolator<E> @endcode
 * @see available interpolators defined in @code interpolators.hh @endcode
 */
template<typename D, typename E, typename I>
class Interpolate1D : public Function1D<E> {
public:

  /** @brief Creates the interpolator based on the given points.
   *
   * @param x - a vector of x-coordinates of the points; this interpolator will make a deep copy of this data
   * @param y - a vector of y-coordinates of the points; this interpolator will make a deep copy of this data
   * @param interpolator - instance of the low-level interpolator engine to be used
   * @see CubicInterpolator LinearInterpolator CatmullRomInterpolator : currently implemented interpolators
   * defined on \f$[0,1]\f$ range that may be used by Interpolate1D
   */
  Interpolate1D(const D &x, const D &y, const I interpolator = I()) { set(x, y, interpolator); }

  /// Default virtual destructor
  virtual ~Interpolate1D() = default;

  /// Copy assignment operator
  Interpolate1D& operator=(const Interpolate1D & other) {
    set(other.x(), other.y(), other.interpolator);
    return *this;
  }

  /// the smallest argument value allowed for this interpolator
  E x_min() const { return x_[0]; }

  /// the smallest argument value allowed for this interpolator
  E x_max() const { return x_.back(); };

  E div() const { return div_; };

  /** @brief Spacing between the points used to create this interpolation.
   *
   * The spacing must be equal for the whole set of points. The maximum argument value covered by this interpolation
   * may be easily calculated as: <code>n_points * step + x_min</code>
   */
  E step() const { return step_; }

  /// const access to the data this interpolation is based on: independent argument (X)
  const D &x() const { return x_; }

  /// const access to the data this interpolation is based on: dependent values (Y)
  const D &y() const { return y_; }

  /** @brief Calculate an interpolated value at x = arg.
   *
   * @param arg - a point for which the interpolated value will be computed; in the range from Interpolate1D::x_min
   * to x_max
   * @return interpolated value
   */
  virtual E operator()(const E arg) const {
    if (arg < min_allowed_value_) return y_[0];
    if (arg > max_allowed_value_) return y_[n_points_ - 1];

    const int klo = (core::index4) ((arg - x_[0]) * div_);

    const E mu = ((arg) - x_[klo]) * div_;
    return interpolator(y_[klo - 1], y_[klo], y_[klo + 1], y_[klo + 2], mu);
  }

protected:
  D x_;                 ///< vector of X values used for interpolation
  D y_;                 ///< vector of Y values used for interpolation
  I interpolator;       ///< interpolator functional that actually computes the interpolated value based on nodes
  E step_;              ///< steps size - the distanc between interpolation nodes
  core::index2 n_points_; ///< the number of points used for interpolation
private:
  E div_;
  E min_allowed_value_;
  E max_allowed_value_;

  void set(const D &x, const D &y, const I &interpolator) {

    x_ = x;
    y_ = y;
    this->interpolator = interpolator;
    n_points_ = x.size();
    step_ = x_[1] - x_[0];
    div_ = 1.0 / step_;
    min_allowed_value_ = x_min() + step_ + step_;
    max_allowed_value_ = (n_points_ - 2) * step_ + x_min();
  }
};

}
}
}

#endif
