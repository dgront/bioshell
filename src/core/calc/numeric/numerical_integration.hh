/** @file numerical_integration.hh Provides method for numerical function integration (Simpson method)
 *
 */
#ifndef CORE_CALC_NUMERIC_numerical_integration_HH
#define CORE_CALC_NUMERIC_numerical_integration_HH

#include <vector>

#include <core/index.hh>

namespace core {
namespace calc {
namespace numeric {

/** @brief Simpson integration method.
 *
 * Calculates an integral of a given function (i.e. the area under a curve) using the Simpson method.
 * The area is divided into a narrow rectangular stripes with one end approximated by a parabolic fragment
 * Total area of these stripes is returned as the approximated integration result.
 *
 * @param f - a function object to be integrated
 * @param x_min - minimum x value
 * @param x_max - maximum x value
 * @param n - the number of segments used for area calculation
 */
template<typename F>
double simpson_integration(F & f, double x_min, double x_max, index4 n) {

  n = (n % 2 == 1) ? n + 1 : n; // n must be even
  double h = (x_max - x_min) / double(n);
  double s = f(x_min) + f(x_max);

  for (index4 i = 1; i < n; i += 2) s += 4 * f(x_min + i * h);
  for (index4 i = 2; i < n - 1; i += 2) s += 2 * f(x_min + i * h);

  return s * h / 3;
}

}
}
}

#endif
