#include <math.h>

#include <core/index.hh>
#include <core/calc/numeric/basic_algebra.hh>
#include <utils/exit.hh>

namespace core {
namespace calc {
namespace numeric {

template<typename T>
T norm_squared(const std::vector<T> vi) {

  T v = 0;
  for (size_t i = 0; i < vi.size(); ++i) v += vi[i] * vi[i];

  return v;
}

template<typename T>
T dot(const std::vector<T> vi, const std::vector<T> &vj) {
  T v = 0;
  for (size_t i = 0; i < vi.size(); ++i) v += vi[i] * vj[i];
  return v;
}

template  double norm(const std::vector<double> vi);
template  float norm(const std::vector<float> vi);
template  double norm_squared(const std::vector<double> vi);
template  float norm_squared(const std::vector<float> vi);
template  double dot(const std::vector<double> vi, const std::vector<double> &vj);
template  float dot(const std::vector<float> vi, const std::vector<float> &vj);


template <typename T>
T det3x3(const core::data::basic::Array2D<T> &A) {

  const std::vector<T> & matrix9 = A.expose_vector();
  return matrix9[0] * (matrix9[4] * matrix9[8] - matrix9[5] * matrix9[7]) -
         matrix9[1] * (matrix9[3] * matrix9[8] - matrix9[5] * matrix9[6]) +
         matrix9[2] * (matrix9[3] * matrix9[7] - matrix9[4] * matrix9[6]);
}

template <typename T>
void mul(const core::data::basic::Array2D<T> &A, const core::data::basic::Vec3 & v, core::data::basic::Vec3 & out) {

  const std::vector<T> & data = A.expose_vector();
  out.x = v.x * data[0] + v.y * data[1] + v.z * data[2];
  out.y = v.x * data[3] + v.y * data[4] + v.z * data[5];
  out.z = v.x * data[6] + v.y * data[7] + v.z * data[8];
}

template <typename T>
core::data::basic::Vec3 mul(const core::data::basic::Array2D<T> &A, const core::data::basic::Vec3 & v) {

  const std::vector<T> & data = A.expose_vector();
  core::data::basic::Vec3 out;
  out.x = v.x * data[0] + v.y * data[1] + v.z * data[2];
  out.y = v.x * data[3] + v.y * data[4] + v.z * data[5];
  out.z = v.x * data[6] + v.y * data[7] + v.z * data[8];

  return out;
}

template<typename T>
void eigenvalues3(const core::data::basic::Array2D<T> &A3x3, std::vector<T> &eigenvalues) {

  const std::vector<T> &a = A3x3.expose_vector();
  // −l^3+(a8+a4+a0)*l^2+((−a4−a0)*a8+a5*a7+a2*a6−a0*a4+a1*a3)*l+(a0*a4−a1*a3)*a8+(a2*a3−a0*a5)*a7+(a1*a5−a2*a4)*a6
  std::vector<T>  coeff(4);

  coeff[3] = -1; // −l ^ 3
  coeff[2] = (a[8]+a[4]+a[0]);
  coeff[1] = ((-a[4]-a[0])*a[8]+a[5]*a[7]+a[2]*a[6]-a[0]*a[4]+a[1]*a[3]);
  coeff[0] = (a[0]*a[4]-a[1]*a[3])*a[8]+(a[2]*a[3]-a[0]*a[5])*a[7]+(a[1]*a[5]-a[2]*a[4])*a[6];

  if(eigenvalues.size()!=0) eigenvalues.clear();
  find_cubic_roots(coeff, eigenvalues);
}


template<typename T>
std::vector<core::data::basic::Vec3> eigenvectors3(const core::data::basic::Array2D<T> &A3x3, const std::vector<T> &eigenvalues) {

  core::data::basic::Array2D<T> m0(3,3,A3x3.expose_vector());
  std::vector<T> b{0,0,0};
  std::vector<core::data::basic::Vec3> eigenvectors;

  m0(0,0) -= eigenvalues[0];
  m0(1,1) -= eigenvalues[0];
  m0(2,2) -= eigenvalues[0];
  auto solution = solve_by_gj_decomposition(m0,b);
  eigenvectors.emplace_back(solution[0],solution[1],solution[2]);

  m0(0,0) += eigenvalues[0] - eigenvalues[1];
  m0(1,1) += eigenvalues[0] - eigenvalues[1];
  m0(2,2) += eigenvalues[0] - eigenvalues[1];
  solution = solve_by_gj_decomposition(m0,b);
  eigenvectors.emplace_back(solution[0],solution[1],solution[2]);

  m0(0,0) += eigenvalues[1] - eigenvalues[2];
  m0(1,1) += eigenvalues[1] - eigenvalues[2];
  m0(2,2) += eigenvalues[1] - eigenvalues[2];
  solution = solve_by_gj_decomposition(m0,b);
  eigenvectors.emplace_back(solution[0],solution[1],solution[2]);

  return eigenvectors;
}

template<typename T>
std::vector<T> solve_by_gj_decomposition(core::data::basic::Array2D<T> &A, std::vector<T> &b) {

  core::index2 n = A.count_rows();
  core::index2 m = A.count_columns();

#ifdef DEBUG
  if (n !=m) utils::exit_OK_with_message("solve_by_gj_decomposition: the input must be a square matrix");
#endif

  core::data::basic::Array2D<T> Ab(n,m+1);
  int k = -1;
  for (core::index2 i = 0; i < n; ++i)
    for (core::index2 j = 0; j < m; ++j)
      Ab(i, j) = A[++k];

  for (core::index2 i = 0; i < n; ++i) Ab(i, m) = b[i];

  auto o = solve_by_gj_decomposition(Ab);

  k = -1;
  for (core::index2 i = 0; i < n; ++i)
    for (core::index2 j = 0; j < m; ++j)
      A[++k] = A(i, j);

  return o;
}


template<typename T>
std::vector<T> solve_by_gj_decomposition(const core::data::basic::Array2D<T> &A, std::vector<T> &b) {

  core::index2 n = A.count_rows();
  core::index2 m = A.count_columns();

#ifdef DEBUG
  if (n !=m) utils::exit_OK_with_message("solve_by_gj_decomposition: the input must be a square matrix");
#endif

  core::data::basic::Array2D<T> Ab(n,m+1);
  int k = -1;
  for (core::index2 i = 0; i < n; ++i)
    for (core::index2 j = 0; j < m; ++j) Ab(i, j) = A[++k];

  for (core::index2 i = 0; i < n; ++i) Ab(i, m) = b[i];

  return solve_by_gj_decomposition(Ab);
}

static const double EPSILON = 0.0000001;

template<typename T>
std::vector<T> solve_by_gj_decomposition(core::data::basic::Array2D<T> &A) {

#ifdef DEBUG
  if (A.count_columns() - 1 != A.count_rows())
    utils::exit_OK_with_message("solve_by_gj_decomposition: The number of columns must be greater by 1 then the number of rows");
#endif

  core::index2 n = A.count_rows();

  for (index2 i = 0; i < n; i++) {
    // Search for maximum in this column
    T maxEl = fabs(A(i,i));
    index2 maxRow = i;
    for (index2 k = i + 1; k < n; k++) {
      if (fabs(A(k,i)) > maxEl) {
        maxEl = fabs(A(k,i));
        maxRow = k;
      }
    }
    if (maxEl == 0) continue;
    // Swap maximum row with current row (column by column)
    for (index2 k = i; k <= n ; k++) {
      T tmp = A(maxRow,k);
      A(maxRow,k) = A(i,k);
      A(i,k) = tmp;
    }

    // Make all rows below this one 0 in current column
    for (index2 k = i + 1; k < n; k++) {
      T c = -A(k,i) / A(i,i);
      for (index2 j = i; j <= n; j++) {
        if (i == j) {
          A(k,j) = 0;
        } else {
          A(k,j) += c * A(i,j);
        }
      }
    }
  }

  // Solve equation Ax=b for an upper triangular matrix A
  std::vector<T> x(n);
  for (int i = n - 1; i >= 0; i--) {
    bool is_zero = true;
    for (int l = 0; l < n; ++l) {
      if (fabs(A(i, l)) > EPSILON) {// --- check if the row is all-zero - in such a case we have an eigenspace
        is_zero = false;
        break;
      };
    }
    if (is_zero)
      x[i] = 1.0; // --- if eigenspace, any value works
    else
      x[i] = A(i, n) / A(i, i);
    for (int k = i - 1; k >= 0; k--) {
      A(k, n) -= A(k, i) * x[i];
    }
  }
  return x;
}

template std::vector<double> solve_by_gj_decomposition(core::data::basic::Array2D<double> & A);

template std::vector<float> solve_by_gj_decomposition(core::data::basic::Array2D<float> & A);

template
std::vector<double> solve_by_gj_decomposition(core::data::basic::Array2D<double> &matrix9, std::vector<double> &b);

template
std::vector<float> solve_by_gj_decomposition(core::data::basic::Array2D<float> &matrix9, std::vector<float> &b);

template
std::vector<double> solve_by_gj_decomposition(const core::data::basic::Array2D<double> &matrix9, std::vector<double> &b);

template
std::vector<float> solve_by_gj_decomposition(const core::data::basic::Array2D<float> &matrix9, std::vector<float> &b);

template
std::vector<core::data::basic::Vec3> eigenvectors3(const core::data::basic::Array2D<float> &A3x3,
                                                   const std::vector<float> &eigenvalues);

template
std::vector<core::data::basic::Vec3> eigenvectors3(const core::data::basic::Array2D<double> &A3x3,
                                                   const std::vector<double> &eigenvalues);

template  void eigenvalues3(const core::data::basic::Array2D<float> &A3x3, std::vector<float> &eigenvalues);
template  void eigenvalues3(const core::data::basic::Array2D<double> &A3x3, std::vector<double> &eigenvalues);

template  double det3x3(const core::data::basic::Array2D<double> &A);
template  float det3x3(const core::data::basic::Array2D<float> &A);

template
void mul(const core::data::basic::Array2D<float> &A, const core::data::basic::Vec3 & v, core::data::basic::Vec3 & out);
template
void mul(const core::data::basic::Array2D<double> &A, const core::data::basic::Vec3 & v, core::data::basic::Vec3 & out);

template core::data::basic::Vec3 mul(const core::data::basic::Array2D<float> &A, const core::data::basic::Vec3 & v);
template core::data::basic::Vec3 mul(const core::data::basic::Array2D<double> &A, const core::data::basic::Vec3 & v);

}
}
}
