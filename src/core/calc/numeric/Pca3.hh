#ifndef CORE_CALC_NUMERIC_Pca3_H
#define CORE_CALC_NUMERIC_Pca3_H


#include <cmath>
#include <ctime>
#include <cstdlib>

#include <core/data/basic/Vec3.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>

namespace core {
namespace calc {
namespace numeric {

/** @brief Calculates PCA in three-dimensions.
 *
 * Principal Component Analysis (PCA) finds directions of the highest variance. After the transformation,
 * the largest spread of points is located on the X axis. In the second example, an \f$\alpha \f$ helix is transformed,
 * which places the helix along the X axis.
 *
 * @see https://en.wikipedia.org/wiki/Principal_component_analysis
 * Example which transforms a set of random points
 * @include ex_Pca3.cc
 *
 * Example which transforms a fragment of a protein structure (a single alpha helix)
 * @include ex_orient_pdb.cc
 */
class Pca3 {
public:

  /** @brief Calculates PCA of the given set of atoms.
   *
   * This effectively places atoms along axes of global coordinate system.
   *
   * @param input_atoms_sp - a vector of input atoms
   */
  Pca3(const std::vector<core::data::structural::PdbAtom_SP> & input_atoms_sp);

  /** @brief Calculates PCA of the given set of 3D vectors.
   *
   * This effectively places points along axes of global coordinate system.
   *
   * @param input_atoms_sp - a vector of input points in 3D
   */
  Pca3(const std::vector<core::data::basic::Vec3> & input_vectors);

  /** @brief Returns eigenvalues
   *
   * @return a vector of calculated eigenvalues
   */
  const std::vector<double> & get_eigenvalues() const { return eigenval; }

  /** @brief Returns eigenvectors
   *
   * @return a vector of calculated eigenvectors - corresponding to eigenvalues
   */
  const std::vector<core::data::basic::Vec3> & get_eigenvectors() const { return eigenvec; }

  /** @brief Creates a rototranslation transformation corresponding to the principal components.
   *
   * Any point can be rotated from a global coordinate system to the system defined by the principal components.
   *
   * @return transformation object
   */
  core::calc::structural::transformations::Rototranslation create_transformation() const;

private:
  core::data::basic::Vec3 center;
  std::vector<double> eigenval;
  std::vector<core::data::basic::Vec3> eigenvec;
};

}
}
}

/**
 * @include ex_orient_pdb.cc ex_Pca3.cc
 */
#endif