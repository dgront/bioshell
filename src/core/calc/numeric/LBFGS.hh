#ifndef CORE_CALC_NUMERIC_Bfgs_HH
#define CORE_CALC_NUMERIC_Bfgs_HH

#include <vector>
#include <core/index.hh>
#include <core/calc/numeric/DerivableFunction.hh>
#include <core/data/basic/Array2D.hh>

namespace core {
namespace calc {
namespace numeric {

template<typename T>
class LBFGS {
public:

  const index2 n; ///< Number of DOFs, i.e. number of arguments of the minimised function

  /** @brief Create a lBFGS function minimizer
   *
   * @param n
   */
  LBFGS(const index2 n);

  /** @brief Minimize a multivariate function using LBFGS algorithm.
   *
   * @param f - a function to be minimised
   * @param x - initial vector of function arguments; local minimum coordinates will be stored in thi vector after this call
   * @param fx - minimum value of the function
   * @return number of iterations
   */
  int minimize(core::calc::numeric::DerivableFunction<T> &f, std::vector<T> &x, T &fx);

private:
  typedef core::data::basic::Array2D<T> Matrix;

  enum LINE_SEARCH_ALGORITHM {
    LBFGS_LINESEARCH_BACKTRACKING_ARMIJO = 1,
    LBFGS_LINESEARCH_BACKTRACKING_WOLFE = 2,
  };

  int m = 6; ///< Number of most recent vectors to remember
  T epsilon = T(1e-5);
  int past = 0;
  T delta = T(0);
  int max_iterations = 0;
  LINE_SEARCH_ALGORITHM linesearch = LBFGS_LINESEARCH_BACKTRACKING_ARMIJO;
  int max_linesearch = 20;
  T min_step = T(1e-20);
  T max_step = T(1e+20);
  T ftol = T(1e-4);
  T wolfe = T(0.9);

  Matrix m_s;      // History of the s vectors
  Matrix m_y;      // History of the y vectors
  std::vector<T> m_ys;     // History of the s'y values
  std::vector<T> m_alpha;  // History of the step lengths
  std::vector<T> m_fx;     // History of the objective function values
  std::vector<T> m_xp;     // Old x
  std::vector<T> m_grad;   // New gradient
  std::vector<T> m_gradp;  // Old gradient
  std::vector<T> m_drt;    // Moving direction
  utils::Logger logs;

  void line_search(DerivableFunction<T> &f, T &fx, std::vector<T> &x, std::vector<T> &grad,
                   T &step, const std::vector<T> &drt, const std::vector<T> &xp);
};

}
}
}

#endif