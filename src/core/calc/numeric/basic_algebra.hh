/** @brief A few basic algebraic routines
 *
 */
#ifndef CORE_CALC_NUMERIC_basic_algebra_HH
#define CORE_CALC_NUMERIC_basic_algebra_HH

#include <vector>
#include <core/data/basic/Vec3.hh>
#include <core/data/basic/Array2D.hh>

namespace core {
namespace calc {
namespace numeric {

/** @brief Calculates a dot product between two vectors.
 *
 * This function does not check if the size of the two given vector is the same!
 *
 * @param vi - the first vector
 * @param vj - the second vector
 * @tparam T - <code>double</code> or <code>float</code>
 * @return dot product (scalar value)
 */
template<typename T>
T dot(const std::vector<T> vi, const std::vector<T> & vj);

/** @brief Calculates the squared norm of a given vector
 *
 *
 * @param vi - an input vector
 * @tparam T - <code>double</code> or <code>float</code>
 * @return squared norm
 */
template<typename T>
T norm_squared(const std::vector<T> vi);

/** @brief Calculates the norm of a given vector
 *
 * @param vi - an input vector
 * @tparam T - <code>double</code> or <code>float</code>
 * @return  norm value
 */
template<typename T>
T norm(const std::vector<T> vi) { return sqrt(norm_squared(vi)); }

/** @brief Calculates eigenvalues of a given 3x3 real matrix
 *
 *
 * @param A3x3 - an input matrix
 * @param eigenvalues - container for eigenvalues
 * @tparam T - <code>double</code> or <code>float</code>
 */
template<typename T>
void eigenvalues3(const core::data::basic::Array2D<T> &A3x3, std::vector<T> &eigenvalues);

/** @brief Calculates eigenvectors of a given 3x3 real matrix
 *
 *
 * @param A3x3 - an input matrix
 * @param eigenvalues - container for eigenvalues
 * @tparam T - <code>double</code> or <code>float</code>
 * @return a container of eigenvectors
 */
template<typename T>
std::vector<core::data::basic::Vec3> eigenvectors3(const core::data::basic::Array2D<T> &A3x3,
                                                   const std::vector<T> &eigenvalues);

/** @brief Calculates the determinant of a 3x3 matrix
 *
 * @param A - a 3x3 matrix
 * @tparam T - <code>double</code> or <code>float</code>
 * @return determinant value
 */
template <typename T>
T det3x3(const core::data::basic::Array2D<T> &A);

/** @brief Gauss elimination for a symmetric matrix.
 * Uses partial pivoting only. The input augmented matrix will be modified by this method;
 * it will contain the inverted matrix.
 *
 * @param Ab - input augmented matrix for a system of linear equations \f$ Ax = b\f$
 * @return a solution vector \f$ x \f$
 * @tparam T - <code>double</code> or <code>float</code>
 */
template<typename T>
std::vector<T> solve_by_gj_decomposition(core::data::basic::Array2D<T> &Ab);

/** @brief Gauss elimination for a symmetric matrix.
 *
 * Uses partial pivoting only.
 * @param A - input matrix
 * @param b - right-hand side vector
 * @return a solution vector \f$ x \f$
 * @tparam T - <code>double</code> or <code>float</code>
 */
template<typename T>
std::vector<T> solve_by_gj_decomposition(const core::data::basic::Array2D<T> &A, std::vector<T> &b);

/** @brief Gauss elimination for a symmetric matrix.
 *
 * Uses partial pivoting only. The input matrix will be modified by this method;
 * it will contain the inverted matrix.
 * @param A - input matrix
 * @param b - right-hand side vector
 * @return a solution vector \f$ x \f$
 * @tparam T - <code>double</code> or <code>float</code>
 */
template<typename T>
std::vector<T> solve_by_gj_decomposition(core::data::basic::Array2D<T> &A, std::vector<T> &b);


/** @brief Multiplies a  object by a 3x3 matrix.
 *
 * @param matrix9 - 3x3 matrix represented
 * @param v - input vector
 * @return  result of multiplication (a newly allocated object)
 */
template<typename T>
core::data::basic::Vec3 mul(const core::data::basic::Array2D<T> &A, const core::data::basic::Vec3 &v);

/** @brief Multiplies a  object by a 3x3 matrix.
 *
 * @param matrix9 - 3x3 matrix represented
 * @param v - input vector
 * @param out - output vector
 */
template<typename T>
void mul(const core::data::basic::Array2D<T> &A, const core::data::basic::Vec3 &v,
                            core::data::basic::Vec3 &out);

}
}
}

#endif