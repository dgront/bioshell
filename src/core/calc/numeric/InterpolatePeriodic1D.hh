#ifndef CORE_CALC_NUMERIC_InterpolatePeriodic1D_HH
#define CORE_CALC_NUMERIC_InterpolatePeriodic1D_HH

#include <exception>
#include <core/index.hh>
#include <utils/string_utils.hh>
#include <core/calc/numeric/Interpolate1D.hh>

namespace core {
namespace calc {
namespace numeric {

/** @brief Interpolates a function based on given points placed on a regular grid in 1D.
 *
 * @tparam D - the type od the data container that holds the points for interpolations, e.g. std::vector<E>
 * @tparam E - the type of data being interpolated, e.g. <code>float</code> or <code>double</code>
 * @tparam I - the type of interpolator to be used
 */
template<typename D, typename E, typename I>
class InterpolatePeriodic1D : public Interpolate1D<D, E, I> {
public:

  /** @brief Creates the interpolator based on the given points.
   *
   * @param x - a vector of x-coordinates of the points; this interpolator will make a deep copy of this data
   * @param y - a vector of y-coordinates of the points; this interpolator will make a deep copy of this data
   * @param interpolator - instance of the low-level interpolator engine to be used
   * @param period - period value, e.g. \f$ 2\pi \f$
   * @see CubicInterpolator LinearInterpolator CatmullRomInterpolator : currently implemented interpolators
   * defined on \f$[0,1]\f$ range that may be used by Interpolate1D
   */
  InterpolatePeriodic1D(const D & x, const D & y, const I & interpolator, E period) :
      Interpolate1D<D, E, I>(x, y, interpolator), period_(period) {}

  /** @brief Calculate an interpolated value at x = arg.
   *
   * @param arg - a point for which the interpolated value will be computed; in the range from Interpolate1D::x_min
   * to x_max
   * @return interpolated value
   */
  inline E operator()(E arg) {

    while (arg < Interpolate1D<D, E, I>::x_min()) arg += period_;
    while (arg > Interpolate1D<D, E, I>::x_max()) arg -= period_;
    int klo_p = ((arg - Interpolate1D<D, E, I>::x_min()) * Interpolate1D<D, E, I>::div()) - 1;
    int klo = (klo_p + 1) % Interpolate1D<D, E, I>::n_points_;
    int klo_n = (klo + 1) % Interpolate1D<D, E, I>::n_points_;
    int klo_nn = (klo_n + 1) % Interpolate1D<D, E, I>::n_points_;
    const E mu =
        (klo_p < klo) ?
            ((arg) - Interpolate1D<D, E, I>::x()[klo]) / Interpolate1D<D, E, I>::step() :
            ((arg) - period_ - Interpolate1D<D, E, I>::x()[klo]) / Interpolate1D<D, E, I>::step();

    return Interpolate1D<D, E, I>::interpolator(Interpolate1D<D, E, I>::y()[klo_p], Interpolate1D<D, E, I>::y()[klo],
        Interpolate1D<D, E, I>::y()[klo_n], Interpolate1D<D, E, I>::y()[klo_nn], mu);
  }

private:
  const E period_;
};

}
}
}

#endif
