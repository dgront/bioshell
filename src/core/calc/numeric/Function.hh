#ifndef CORE_CALC_NUMERIC_Function_HH
#define CORE_CALC_NUMERIC_Function_HH

#include <vector>

namespace core {
namespace calc {
namespace numeric {

/** @brief Pure virtual definition of a generic multivariate function
 */
template <typename E>
class Function {
public:

  /** @brief Call a multivariate function.
   *
   * @param x function's argument
   * @return function value \f$ f(\vec{x}) \f$
   */
  virtual E operator()(const std::vector<E> & x) = 0;
};

}
}
}

#endif
