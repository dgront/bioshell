#ifndef CORE_CALC_NUMERIC_Function1D_HH
#define CORE_CALC_NUMERIC_Function1D_HH

namespace core {
namespace calc {
namespace numeric {

/** @brief Pure virtual definition of a 1D function.
 *
 * This interface defines a one-argument call operator.
 * @tparam E - the type of values returned by a call operator (i.e. this function object)
 */
template <typename E>
class Function1D {
public:

  /// Virtual destructor (empty)
  virtual ~Function1D() {}

  /** @brief Call a 1D function
   * @param x function's argument
   * @return function value \f$ f(x) \f$
   */
  virtual E operator()(const E x) const = 0;
};

/** @brief 1D function that always returns the same constant.
 */
template <typename E>
class ConstFunction1D : public Function1D<E> {
public:

  /** @brief Creates a const-function for a given value of the constant
   * @param const_val - the  value returned by this function
   */
  ConstFunction1D(E const_val = 0) { const_val_ = const_val; }

  /** @brief 1D function that always returns the same constant
   * @param x function's argument
   * @return function value \f$ f(x) \f$ - disregarded by this function
   */
  virtual E operator()(const E x) const { return const_val_; }

  /// Default virtual destructor
  virtual ~ConstFunction1D() = default;

private:
  E const_val_;
};

}
}
}

#endif
