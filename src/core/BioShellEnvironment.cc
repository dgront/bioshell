#include <string>
#include <fstream>
#include <stdexcept>

#include <core/BioShellEnvironment.hh>
#include <utils/io_utils.hh>
#include <utils/options/OptionParser.hh>
#include <utils/options/input_options.hh>

namespace core {

std::string BioShellEnvironment::bioshell_db_path_ = "";
bool BioShellEnvironment::db_path_env_tested_ = false;
utils::Logger BioShellEnvironment::logger("BioShellEnvironment");

const std::string get_env_var(const std::string & key) {
  char * val;
  val = getenv( key.c_str() );
  std::string retval = "";
  if (val != NULL) {
    retval = val;
  }

  return retval;
}
    
void BioShellEnvironment::set_db_path_from_environment() {
  
  std::string path = core::get_env_var("BIOSHELL_DATA_DIR");
  BioShellEnvironment::db_path_env_tested_ = true;
  if(path.size()>1) {
    BioShellEnvironment::bioshell_db_path(path);
    logger << utils::LogLevel::INFO << "BioShell DB path extracted from a shell variable: "
        << core::BioShellEnvironment::bioshell_db_path() << "\n";
    }
  }

const std::string BioShellEnvironment::from_file_or_db(const std::string & fname) {

  using utils::options::option_value;
  bool if_found;
  std::string fname2;
  if(!db_path_env_tested_)
    set_db_path_from_environment();
  std::string the_path_now = bioshell_db_path_;
  if (utils::options::db_path.was_used())
    the_path_now = option_value<std::string>(utils::options::db_path);
  if (the_path_now.size()>0) {
    if_found = utils::find_file(fname, fname2, the_path_now);
    if (if_found) logger << utils::LogLevel::FILE << "file " << fname2 << " found in BioShell DB location: "
        << the_path_now << "\n";
    else logger << utils::LogLevel::FILE << "Can't find file " << fname2 << " in BioShell DB location: "
        << option_value<std::string>(utils::options::db_path) << "\n";
  } else {
    logger << utils::LogLevel::FILE << "DB path not set. Use a proper command line option or set the BIOSHELL_DATA_DIR environmenal (i.e. shell) variable.\nFile not found in database: " << fname<<"\n";
    if_found = utils::find_file(fname, fname2, ".");
    if (if_found) logger << utils::LogLevel::FILE << "file " << fname2 << " found in current directory\n";
  }
  if (!if_found) throw std::runtime_error("Can't locate " + fname);
  return fname2;
}

const std::string BioShellEnvironment::from_file_or_db(const std::string & fname, const std::string & db_location) {

  using utils::options::option_value;
  bool if_found;
  std::string fname2;
  if(!db_path_env_tested_)
    set_db_path_from_environment();
  std::string the_path_now = bioshell_db_path_;
  if (utils::options::db_path.was_used())
    the_path_now = option_value<std::string>(utils::options::db_path);

  if (the_path_now.size()>0) {
    const std::string db_path = utils::join_paths(the_path_now, db_location);
    if_found = utils::find_file(fname, fname2, db_path);
    if (if_found) logger << utils::LogLevel::FILE << "file " << fname2 << " found in BioShell DB location: " << db_path
        << "\n";
    else logger << utils::LogLevel::FILE << "Can't find file " << fname2 << " in BioShell DB location: " << db_path
        << "\n";
  } else {
      logger << utils::LogLevel::FILE << "DB path not set. Use a proper command line option or set the BIOSHELL_DATA_DIR environmenal (i.e. shell) variable.\nFile not found: " << fname<<"\n";
    if_found = utils::find_file(fname, fname2, ".");
  }
  if (!if_found)
    throw std::runtime_error("Can't locate " + fname);

  return fname2;
}

void BioShellEnvironment::bioshell_db_path(const std::string & new_path) {
  bioshell_db_path_ = new_path;
  logger << utils::LogLevel::FILE << "Setting the BioShell database path to " << bioshell_db_path_ << "\n";
}

const std::string & BioShellEnvironment::bioshell_db_path() {

  if((bioshell_db_path_=="")&&(!db_path_env_tested_))
    set_db_path_from_environment();

  return bioshell_db_path_;
}

}

