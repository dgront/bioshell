/** \file index.hh
 * @brief Defines bioshell-specific unsigned integer basic types
 *
 */

#ifndef CORE_index_H
#define CORE_index_H

namespace core {

typedef unsigned char index1; ///< one byte unsigned integer-type

typedef unsigned short int index2; ///< two bytes long unsigned integer

typedef unsigned int index4; ///< four bytes long unsigned integer
} // ~core

#endif
