#ifndef CORE_ALGORITHMS_SimpleGraph_HH
#define CORE_ALGORITHMS_SimpleGraph_HH

#include <vector>

#include <core/index.hh>
#include <utils/Logger.hh>
#include <utils/string_utils.hh>

namespace core {
namespace algorithms {

/** \brief Represents a simple graph.
 * <p>
 * A graph is a collection of points and lines connecting some (possibly empty) subset of them.
 * The points of a graph are referred as graph vertices.
 * Similarly, the lines connecting the vertices of a graph are most called edges.
 * A simple graph is an unweighted, undirected graph containing no graph loops or multiple edges.
 * </p>
 * <p>
 * This class represents a graph for which:</p>
 * <ul>
 * <li>  multiple edges are not allowed between vertices \f$\Longleftrightarrow \f$ (i.e. it is not a multigraph) </li>
 * <li>  edges do not support arrows \f$\Longleftrightarrow \f$ undirected graph </li>
 * <li>  Edge objects (of a generic type E) and vertices (of a generic type V) may be considered as
 * edge and vertex labels, respectively \f$\Longleftrightarrow \f$ labeled graph </li>
 * </ul>
 * </p>
 * In this implementation, SimpleGraph class holds explicitly only edges as an adjacency lists. Vertices are
 * implicitly defined by the edges
 * @see http://mathworld.wolfram.com/Graph.html for graph terminology and a brief description
 */
class SimpleGraph {
public:

  /// iterator for vertex neighbors
  typedef std::vector<core::index4>::iterator iterator;

  /// iterator for vertex neighbors
  typedef std::vector<core::index4>::const_iterator const_iterator;

  /// Default c'tor
  SimpleGraph() : SimpleGraph(0) {}

  /// Constructor that creates an empty graph
  SimpleGraph(const index4 n_vertices) : n_vertices_(n_vertices), flags_(n_vertices), logger("SimpleGraph") { }

  /** \brief Returns the number of vertices of this graph.
   * @return  the number of vertices of this graph.
   */
  index4 count_vertices() const { return n_vertices_; }

  /** \brief Returns the total number of edges of this graph.
   * @return  the number of edges of this graph.
   */
  index4 count_edges() const { return n_edges_; }

  /** \brief Returns the total number of edges attached to a given vertex
   * @return  the number of edges connected to a vertex of this graph
   */
  index4 count_edges(const core::index4 vi) const { return adjacency_lists_[vi].size(); }

  /** @brief Returns true if the two given vertexes are connected by an edge.
   * @param vi - index of the first vertex
   * @param vj - index of the second vertex
   * @return true if <code>vi</code> is connected to <code>vj</code>, false otherwise
   */
  bool are_connected(const core::index4 vi, const core::index4 vj) const {

    auto it = std::find(adjacency_lists_[vi].cbegin(),adjacency_lists_[vi].cend(),vj);
    return (it != adjacency_lists_[vi].cend());
  }

   /** \brief Adds an edge into this graph that binds two given vertices.
    *
    * <p>The method binds two vertices that belong to this graph with an edge. If any of the to
    * vertices is not found in the graph, it will be immediately inserted.
    * Since this graph is undirected, the order in which the two vertices
    * are given is unimportant</p>
    *
    * <p>This method forces the properties of a simple graph, i.e. it checks whether
    * the two given vertices are already connected. If so, a new edge is not added and
    * <strong>false</strong> value returned. Otherwise the method inserts the new edge and
    * returns <strong>true</strong>.
    *
    * @param first_vertex - where an edge begins (the order is unimportant)
    * @param second_vertex - where an edge ends (the order is unimportant)
    * @return false if the two vertices are already connected by an edge; true otherwise
    */
   bool add_edge(const core::index4 vi, const core::index4 vj) {

     if (vi >= n_vertices_) n_vertices_ = vi + 1;
     if (vj >= n_vertices_) n_vertices_ = vj + 1;
     adjacency_lists_.resize(n_vertices_);
     flags_.resize(n_vertices_);
     if (are_connected(vi, vj)) return false;
     adjacency_lists_[vi].push_back(vj);
     adjacency_lists_[vj].push_back(vi);
     ++n_edges_;
     return true;
   }

   /** @brief Removes an edge between two vertices if it exists
    *
    * @param vi - index of the first vertex
    * @param vj - index of the second vertex
    * @return if the edge existed and was removed - returns true; false if there was no such an edge
    */
  bool remove_edge(const core::index4 vi, const core::index4 vj) {

    int i = neighbor_index_pos(vi,vj);
    if (i < 0) return false;
    int j = neighbor_index_pos(vj,vi);
    adjacency_lists_[vi][i] = adjacency_lists_[vi][adjacency_lists_[vi].size()-1];
    adjacency_lists_[vi].pop_back();
    adjacency_lists_[vj][j] = adjacency_lists_[vj][adjacency_lists_[vj].size()-1];
    adjacency_lists_[vj].pop_back();
    --n_edges_;

    return true;
  }

  /** @brief Returns a begin iterator to vertices connected by edges to a given vertexes
   * @param v - a vertex index
   * @return iterator pointing at the first vertex index
   */
  iterator begin(const core::index4 v) { return adjacency_lists_[v].begin(); }

   /** @brief Returns an end iterator to vertices connected by edges to a given vertexes
    * @param v - a vertex index
    * @return iterator pointing behind the last vertex index
    */
  iterator end(const core::index4 v) { return adjacency_lists_[v].end(); }

   /** @brief Returns a begin const-iterator to vertices connected by edges to a given vertexes
    * @param v - a vertex index
    * @return iterator pointing at the first vertex index
    */
  const_iterator cbegin(const core::index4 v) const { return adjacency_lists_[v].cbegin(); }

   /** @brief Returns an end const-iterator to vertices connected by edges to a given vertexes
    * @param v - a vertex index
    * @return iterator pointing behind the last vertex index
    */
  const_iterator cend(const core::index4 v) const { return adjacency_lists_[v].cend(); }

  /** @brief Returns a vector holding indexes of all vertices connected to a given vertex by an edge
   * @param v - a vertex index
   * @return const std::vector of indexes
   */
  const std::vector<index4> & get_adjacent(const index4 vi) const { return adjacency_lists_[vi]; }

  /** @brief Return a flag assigned to a vertex identified by its internal index.
   * By default all flags are 0, but new values may be defined by @code vertex_flag(const index4, const index1)@endcode method
   * @param vertex - index of a vertex
   * @return flag assigned to a vertex
   */
  index1 vertex_flag(const core::index4 vertex) const { return flags_[vertex]; }

  /** @brief Return a flag assigned to a vertex identified by its internal index.
   * By default all flags are 0, but new values may be defined by @code vertex_flag(const index4, const index1)@endcode method
   * @param vertex - index of a vertex
   * @param flag - a the new flag to be assigned to a vertex
   */
  void vertex_flag(const core::index4 vertex, const core::index1 flag) { flags_[vertex] = flag; }

  /** @brief Prints adjacency matrix of this graph on ostream, e.g. on a screen
   * @param out - stream to print this graph
   */
  void print_adjacency_matrix(std::ostream &out) {

    for (core::index2 i = 0; i < count_vertices(); ++i) {
      out << utils::string_format("%4d ", i);
      for (core::index2 j = 0; j < count_vertices(); ++j)
        out << ((are_connected(i, j)) ? '#' : '.');
      out <<"\n";
    }
    out <<"\n";
  }

  /** @brief Clear a flag of every vertex of this graph by setting them to the given value
   * @param flag - value used to reset all the flags (0 by default)
   */
  void clear_flags(const core::index1 flag = 0) { for (size_t i = 0; i < flags_.size(); ++i) flags_[i] = flag; }

  /** @brief Increase the number of vertices of this graph by one.
   */
  void add_vertex() {
    ++n_vertices_;
    adjacency_lists_.resize(n_vertices_);
    flags_.resize(n_vertices_);
  }

private:
  index4 n_vertices_ = 0; ///< Counts the number of vertices of this graph, should be incremented wne a new vertex is added
  core::index4 n_edges_ = 0;
  std::vector<std::vector<core::index4>> adjacency_lists_;
  std::vector<index1> flags_;
  utils::Logger logger;

  /** @brief Returns order index of vertex <code>vj</code> as a neighbor of <code>vi</code>
   * @param vi - the index of interest
   * @param vj - its neighbor, whose index will be returned
   * @return returns index <code>k</code> such as <code>adjacency_lists_[vi][k] == vj</code> i.e.
   * <code>vi</code> has <code>vj</code> as its k-th neighbor
   */
  int neighbor_index_pos(const index4 vi, index4 vj) {
    for (int i = 0; i < adjacency_lists_[vi].size(); ++i)
      if (adjacency_lists_[vi][i] == vj) return i;

    return -1;
  }
};

}
}

#endif
