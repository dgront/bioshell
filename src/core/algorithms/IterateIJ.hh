#ifndef CORE_ALGORITHMS_IterateIJ_HH
#define CORE_ALGORITHMS_IterateIJ_HH

#include <utility>
#include <iterator>
#include <algorithm>
#include <set>
#include <vector>
#include <iostream>

#include <core/index.hh>

namespace core {
namespace algorithms {

/** @brief Provides iterator that can index two dimensional array.
 *
 * This class provides <em>iterator<em> functionality for any 2D array. It iterates over pairs
 * of integer indexes from (0,0) to (N-1, N-1) for a square array of size NxN. It's also possible to
 * iterate only over upper right triangle of the matrix
 *
 * \include ex_IterateIJ.cc
 */
class IterateIJ {
public:
  /// Type of values this iterator iterates over
  typedef std::pair<index4, index4> value_type;
  typedef std::ptrdiff_t difference_type;
  typedef std::pair<index4, index4> *pointer;
  typedef std::pair<index4, index4> &reference;
  typedef std::input_iterator_tag iterator_category;

  /** @brief Iterates over pairs (i,j) that may be used to index 2D array.
   *
   * @param n - dimension of the square array
   * @param if_triangular - if true, this iterator will go only through such (i,j) pairs where j>i (upper right triangle)
   */
  IterateIJ(index4 n, bool if_triangular = true) : n_(n), if_triangular_(if_triangular), allowed_pairs_() {
    i_ = 0;
    j_ = (if_triangular) ? 1 : 0;
    k_ = 0;
  }

  /** @brief Advance this iterator by one position.
   *
   * <code>++iterator</code> operation will move the iterator to the next element of a matrix. E.g. for a matrix
   * of size 5x5, element (3,3) is followed by (3,4), then followed by (4,0)
   *
   * @return reference to the updated iterator (self reference)
   */
  inline IterateIJ &operator++() {

    if(allowed_pairs_.size()>0) {
      if (k_ < allowed_pairs_.size()) {
        i_ = allowed_pairs_[k_].first;
        j_ = allowed_pairs_[k_].second;
        ++k_;
      } else
        set_to_end();
    } else {
      ++j_;
      if (j_ == n_) {
        ++i_;
        if (allowed_rows_.size() > 0) {
          while ((i_ < n_) && (allowed_rows_.find(i_)) == allowed_rows_.end()) ++i_;
          if (i_ == n_) {
            auto e = end();
            i_ = e.i_;
            j_ = e.j_;
          } else j_ = (if_triangular_) ? i_ + 1 : 0;
        } else
          j_ = (if_triangular_) ? i_ + 1 : 0;
      }
    }
    return *this;
  }

  index4 count() const {

    IterateIJ copy(n_, if_triangular_);
    std::copy( allowed_rows_.cbegin(), allowed_rows_.cend(),
               std::insert_iterator<std::set<index4>>( copy.allowed_rows_, copy.allowed_rows_.begin() ) );
    copy.allowed_pairs_.assign(allowed_pairs_.cbegin(),allowed_pairs_.cend());
    index4 total = 0;
    IterateIJ begin = copy.begin();
    IterateIJ end = copy.end();
    for (auto it = begin; it != end; ++it) {
      ++total;
    }

    return total;
  }

  /** @brief Returns a pair of indexes to the underlying 2D matrix
   * @return pair of indexes
   */
  value_type operator*() const { return std::pair<index4, index4>(i_, j_); }

  /** @brief Returns true if the two iterators point to the very same element (pair of indexes)
   * @param lhs - left-hand side operand
   * @param rhs - right-hand side operand
   * @return true or false
   */
  friend bool operator==(const IterateIJ &lhs, const IterateIJ &rhs) {
    return (lhs.i_ == rhs.i_) && (lhs.j_ == rhs.j_);
  }

  /** @brief Returns true if the two iterators point to a different same element (pair of indexes)
   * @param lhs - left-hand side operand
   * @param rhs - right-hand side operand
   * @return true or false
   */
  friend bool operator!=(const IterateIJ &lhs, const IterateIJ &rhs) { return !(lhs == rhs); }

  /** @brief Assignment operator
   * @param other - source IterateIJ object
   * @return reference to this
   */
  IterateIJ& operator=(const IterateIJ & other) {
    n_ = other.n_;
    i_ = other.i_;
    j_ = other.j_;
    k_ = other.k_;
    if_triangular_ = other.if_triangular_;
    for(index4 r:other.allowed_rows_) add_selected_row(r);
    allowed_pairs_.assign(other.allowed_pairs_.cbegin(),other.allowed_pairs_.cend());

    return *this;
  }

  /** @brief Returns an iterator that points to the very first element of an array.
   *
   * If no particular rows were selected, this iterator points to (0,0). Otherwise it points to
   * (k,0) where <code>k</code> is the lowest index of selected rows
   * @return iterator that points to the first element
   */
  IterateIJ begin() {

    IterateIJ out = IterateIJ{n_, if_triangular_};
    out.i_ = (allowed_rows_.size()!=0) ? *std::min_element(allowed_rows_.begin(), allowed_rows_.end()) : 0;
    out.j_ = (if_triangular_) ? out.i_ + 1 : 0; 
    
    for(index4 r:allowed_rows_) out.add_selected_row(r);
    out.allowed_pairs_.assign(allowed_pairs_.cbegin(),allowed_pairs_.cend());
    if (out.allowed_pairs_.size() > 0) {
      out.i_ = allowed_pairs_[0].first;
      out.j_ = allowed_pairs_[0].second;
      out.k_ = 1;
    }

    return out;
  }
  /** @brief Returns an iterator that points behind the very last element of a matrix
   * @return iterator that points to (N,0) or to (N-1, N_) if the iteration went over a triangle
   */
  IterateIJ end() {

    IterateIJ it(n_, if_triangular_);
    for(index4 r:allowed_rows_) it.add_selected_row(r);
    it.allowed_pairs_.assign(allowed_pairs_.cbegin(),allowed_pairs_.cend());
    it.set_to_end();

    return it;
  }

  /** @brief Selects rows for iteration.
   *
   * When at least one row is selected, this iterator will iterate only through the selected rows. Call this method
   * multiple times to select more than one row.
   * If no rows are selected, this object will iterate through the full matrix (or a triangle, if selected)
   *
   * @param i - index of a row (first index in a pair)
   * @return reference to <code>this</code> to facilitate chaining calls
   */
  IterateIJ & add_selected_pair(core::index4 i, core::index4 j) {
    allowed_pairs_.emplace_back(i,j);
    return *this;
  }

  /** @brief Selects rows for iteration.
   *
   * When at least one row is selected, this iterator will iterate only through the selected rows. Call this method
   * multiple times to select more than one row.
   * If no rows are selected, this object will iterate through the full matrix (or a triangle, if selected)
   *
   * @param i - index of a row (first index in a pair)
   * @return reference to <code>this</code> to facilitate chaining calls
   */
  IterateIJ & add_selected_row(core::index4 i) {
    allowed_rows_.insert(i);
    return *this;
  }

private:
  index4 n_;            ///< size of a matrix this iterator walks through
  index4 i_ = 0;        ///< index of the row this iterator currently points at
  index4 j_ = 0;        ///< index of the column this iterator currently points at
  index4 k_ = 0;        ///< index of the selected pair element to return
  bool if_triangular_;  ///< true if this iterator should iterate only over a triangular part of a matrix
  std::set<index4> allowed_rows_; ///< selection of rows to iterate
  std::vector<std::pair<index4, index4>> allowed_pairs_; ///< selection of pairs to iterate

  // Set this iterator so it points to end
  void set_to_end() {

//    if (allowed_pairs_.size() > 0) {
//      i_ = allowed_pairs_.back().first;
//      j_ = allowed_pairs_.back().second;
//      k_ = allowed_pairs_.size();
//      return;
//    }
    k_ = 0;
    if(if_triangular_) {
      i_ = n_ - 1;
      j_ = n_;
    } else {
      i_ = n_;
      j_ =  0;
    }
  }
};

}
}
#endif // ~ CORE_ALGORITHMS_IterateIJ_HH

/**
 * @example  ex_IterateIJ.cc
 */