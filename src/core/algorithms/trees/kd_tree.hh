/** @file kd_tree.hh
 * @brief Provides routine to build a KD-tree based on given points
 */
#ifndef CORE_ALGORITHMS_TREES_ThreeDTree_H
#define CORE_ALGORITHMS_TREES_ThreeDTree_H

#include <iostream>
#include <stdexcept>
#include <iterator>
#include <memory> // for shared_ptr
#include <core/data/basic/Vec3.hh>

#include <core/algorithms/trees/BinaryTreeNode.hh>

namespace core {
namespace algorithms {
namespace trees {

/** @brief Provides operator to compare point objects (e.g. Vec3 or PdbAtom instances) as pointers
 * This operator works with any indexable data type, i.e. objects of type <code>T</code>
 * must implement <code>operator[]()</code> function
 */
template<typename T>
struct CompareAsPointers {
  /** @brief Create a operator-object that compares requested coordinate.
   * @param which_coordinate - indexes coordinate to be compared, e.g. 0 for x, 1 for y etc.
   */
  CompareAsPointers(const index1 which_coordinate) : which_coordinate_(which_coordinate) {}

  /// Return true if <code>*left[c] < *right[c]</code> where <code>c</code> is the coordinate index
  bool operator()(const T left, const T right) const { return (*left)[which_coordinate_] < (*right)[which_coordinate_]; }

private:
  index1 which_coordinate_;
};

/** @brief Provides operator to compare point objects (e.g. Vec3 or PdbAtom instances) as references.
 * This operator works with any indexable data type, i.e. objects of type <code>T</code>
 * must implement <code>operator[]()</code> function
 */
template<typename T>
struct CompareAsReferences {
 /** @brief Create a operator-object that compares requested coordinate.
   * @param which_coordinate - indexes coordinate to be compared, e.g. 0 for x, 1 for y etc.
   */
  CompareAsReferences(const index1 which_coordinate) : which_coordinate_(which_coordinate) {}

  /// Return true if <code>left[c] < right[c]</code> where <code>c</code> is the coordinate index
  bool operator()(const T &left, const T &right) const { return left[which_coordinate_] < right[which_coordinate_]; }

private:
  index1 which_coordinate_;
};

template<typename E>
struct KDTreeNode {
  E element;
  core::index2 level;
  core::index2 split_coordinate;
};

/** @brief Create a KD-tree in a recursive manner
 * @param begin - iterator pointing to the first data point
 * @param end - iterator pointing behind the last data point
 * @param dimensionality - dimensionality of the data, e.g. 3 for 3D space
 * @param tree_level - level of the three (recursive argument); use <code>0</code> to build the whole tree,
 *  with the first split along the first dimension
 * @tparam T - type of the data stored on the tree (e.g. Vec3, PdbAtom, etc.)
 * @tparam IteratorType - type of the iterator used to provide data points
 * @tparam AxisSorterCriteria - operator used to sort points (atoms, etc) along each axis. Use
 * CompareAsPointers or CompareAsReferences class
 */
template <typename T, typename IteratorType, typename AxisSorterCriteria>
static std::shared_ptr<BinaryTreeNode<KDTreeNode<T>>> create_kd_tree(IteratorType begin,
      IteratorType end, const index2 dimensionality = 3, const index2 tree_level = 0) {

  // ---------- Handle special cases (0 or 1 point)
  if (begin == end) return nullptr;
  index4 n_points = std::distance(begin, end);
  if (n_points == 1)
    return std::make_shared<BinaryTreeNode<KDTreeNode<T>>>(tree_level + 1, KDTreeNode<T>{*(begin), index2(tree_level + 1), 0});

  // ---------- Sort by an appropriate axis, find the median element
  AxisSorterCriteria sorter(tree_level % dimensionality);
  std::sort(begin, end, sorter);
  index4 median = n_points / 2;

  // ---------- Median element is the new root (of a subtree)
  std::shared_ptr<BinaryTreeNode<KDTreeNode<T>> > root =
      std::make_shared<BinaryTreeNode<KDTreeNode<T>>>(tree_level,
                      KDTreeNode<T>{*(begin + median), tree_level, index2(tree_level % dimensionality)});

  // ---------- Assign the left and the right subtree
  std::shared_ptr<BinaryTreeNode<KDTreeNode<T>> > left =
      create_kd_tree<T, IteratorType, AxisSorterCriteria>(begin, begin + median, dimensionality, index2(tree_level + 1));
  if (left != nullptr) root->set_left(left);
  std::shared_ptr<BinaryTreeNode<KDTreeNode<T>> > right =
      create_kd_tree<T, IteratorType, AxisSorterCriteria>(begin + median + 1, end, dimensionality, index2(tree_level + 1));
  if (right != nullptr) root->set_right(right);

  return root;
}

/** @brief Searches a KD-tree looking for points that are close enough to a given query
 * @param tree_root - the root node of the kd-tree
 * @param q_lower - lower bound of the search range
 * @param q_upper - upper bound of the search range
 * @param dimensionality - dimensionality of the data, e.g. 3 for 3D space
 * @param hits - a vector where the results will be stored
 * @tparam T - type of the data stored on the tree (e.g. Vec3, PdbAtom, etc.)
 * @tparam Q - type of the query data point
 * @tparam DistanceOperator - functor class used to evaluate distance between two objects: the first  of type Q and the second of type T
 */
template <typename T, typename Q>
void search_kd_tree(const std::shared_ptr<BinaryTreeNode<KDTreeNode<T>>> tree_root,
                    const Q &q_lower, const Q &q_upper, core::index2 dimensionality, std::vector<T> &hits) {

  std::vector<std::shared_ptr<BinaryTreeNode<KDTreeNode<T>>>> stack;
  stack.push_back(tree_root);
  while (stack.size() > 0) {
    std::shared_ptr<BinaryTreeNode<KDTreeNode<T>>> n = stack.back();
    stack.pop_back();

    bool inside = true;
    const T &e = n->element.element;
    for (index2 k = 0; k < dimensionality; ++k) {
      if ((e[k] > q_upper[k]) || (e[k] < q_lower[k])) {
        inside = false;
        break;
      }
    }
    if (inside) hits.push_back(e);

    core::index2 k = n->element.split_coordinate;
    const std::shared_ptr<BinaryTreeNode<KDTreeNode<T>>> l = n->get_left();
    const std::shared_ptr<BinaryTreeNode<KDTreeNode<T>>> r = n->get_right();
    if (q_lower[k] < e[k])
      if (l != nullptr) stack.push_back(l);
    if (q_upper[k] >= e[k])
      if (r != nullptr) stack.push_back(r);
  }
}

/** @brief Searches a KD-tree looking for a point that is the closest to the given query
 * @param tree_root - the root node of the kd-tree
 * @param dist_function - operator used to evaluate distance between the given query
 *  and an object one of type T. The type of these two point may differ, e.g. one of them may be a reference and the other a pointer etc.
 * @param query - the query point
 * @param min_element_found - the closest point found on the tree
 * @tparam T - type of the data stored on the tree (e.g. Vec3, PdbAtom, etc.)
 * @tparam Q - type of the query data point
 * @tparam DistanceOperator - functor class used to evaluate distance between two objects: the first  of type Q and the second of type T
 * @return the distance between the query and the closest point on the tre
 */
template <typename T, typename Q, typename DistanceOperator>
double search_kd_tree(const std::shared_ptr<BinaryTreeNode<KDTreeNode<T>>> tree_root,
                      const DistanceOperator & dist_function, const Q & query, T & min_element_found) {

  std::vector<std::shared_ptr<BinaryTreeNode<KDTreeNode<T>>>> stack;
  stack.push_back(tree_root);
  double min_dist = std::numeric_limits<double>::max();
  min_element_found = tree_root->element.element;
  while (stack.size() > 0) {
    std::shared_ptr<BinaryTreeNode<KDTreeNode<T>>> n = stack.back();  // --- process the top element from the stack
    const T & ne = n->element.element;                                // --- data stored at that node
    stack.pop_back();                                                 // --- remove top-most point from the stack
    // --- calculate the distances between the query the current node, update minimum if found
    double d = dist_function(ne, query);
    if (d < min_dist) {
      min_dist = d;
      min_element_found = ne;
    }
    core::index2 k = n->element.split_coordinate;
    d = ne[k] - query[k];
    d = sqrt(d*d);

    if (d > min_dist) {
      if (ne[k] > query[k]) {
        if (n->get_left() != nullptr) stack.push_back(n->get_left());
      } else {
        if (n->get_right() != nullptr) stack.push_back(n->get_right());
      }
    } else {
      if (n->get_left() != nullptr) stack.push_back(n->get_left());
      if (n->get_right() != nullptr) stack.push_back(n->get_right());
    }
  }

  return min_dist;
}

}
}
}

#endif
