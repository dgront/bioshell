#ifndef CORE_ALGORITHMS_TREES_BinaryTreeNode_H
#define CORE_ALGORITHMS_TREES_BinaryTreeNode_H

#include <iostream>
#include <stdexcept>
#include <memory> // for shared_ptr

#include <utils/string_utils.hh>

namespace core {
namespace algorithms {
namespace trees {

/** @brief  A node in a binary tree with a left and a right branch
 *
 */
template<typename T>
class BinaryTreeNode : public std::enable_shared_from_this<BinaryTreeNode<T> > {
public:
  int id;     ///< ID of this node
  T element;  ///< Element stored at this node

  /// Creates an empty node
  BinaryTreeNode(int no, T elem) : id(no), element(elem) { }

  /** @brief Returns the left node of this tree.
   * nullptr may be returned by this method
   */
  inline std::shared_ptr<BinaryTreeNode<T> > get_left() const { return left; }

  /** @brief Returns the right node of this tree.
   * nullptr may be returned by this method
   */
  inline std::shared_ptr<BinaryTreeNode<T> > get_right() const { return right; }

  /** @brief Returns the root node of this tree.
   * If this node is the root of the whole tree, nullptr is returned
   */
  inline std::shared_ptr<BinaryTreeNode<T> > get_root() const { return root; }

  /// Sets both leaves for this tree node
  inline void set_left_right(std::shared_ptr<BinaryTreeNode<T> > left, std::shared_ptr<BinaryTreeNode<T> > right) {
    set_left(left);
    set_right(right);
  }

  /// Sets the left leaf for this tree node
  inline void set_left(std::shared_ptr<BinaryTreeNode<T> > left) {

    this->left = left;
    left->root = std::enable_shared_from_this<BinaryTreeNode<T> >::shared_from_this();
  }

  /// Sets the right leaf for this tree node
  inline void set_right(std::shared_ptr<BinaryTreeNode<T> > right) {

    this->right = right;
    right->root = std::enable_shared_from_this<BinaryTreeNode<T> >::shared_from_this();
  }

  /** \brief Sets a value for an flag in this node.
   *
   * This operation may be useful for marking visited nodes. <strong>Be careful when storing data </strong> in
   * node's flag since the flag value changes after every tree traversal.
   * @param new_flag a new value for the flag
   */
  inline void set_flag(const char new_flag) { flag = new_flag; }

  /** \brief Reads a node's flag.
   *
   * This operation may be useful for marking visited nodes. <strong>Be careful when storing data </strong> in
   * node's flag since the flag value changes after every tree traversal.
   * @return a flag's value
   */
  inline char get_flag() const { return flag; }

  /// Checks if this node has the left leaf
  inline bool has_left() const { return (left != 0); }

  /// Checks if this node has the right leaf
  inline bool has_right() const { return (right != 0); }

  /** \brief Return true if this is a leaf node. */
  inline bool is_leaf() const { return (left == 0) && (right == 0); }

  /** @brief Returns the number of branches of this node.
   * Since this is a binary node, returned value cannot be greater than 2.
   * To count all the nodes in a subtree rooted in this node, use size() method.
   *
   * To count all the leaves in this subtree, call countLeaves()
   *
   * @return number of non-null branches of this node
   */
  inline core::index2 count_branches() const {

    if (left == 0) if (right == 0) return 0;
    else return 1;
    else if (right == 0) return 1;
    else return 2;
  }

  /** @brief Returns the requested branch.
   * Since this is a binary node, the argument index may be either 0 or 1.
   * @param index - which branch; if <code>index == 0</code> the left branch is returned if exists. Otherwise, the right one is returned.
   * In the case when <code>index == 1</code> both branhces must exist and the right one is returned. In all other cases the method throws an exception.
   * @return the requested branch
   */
  std::shared_ptr<BinaryTreeNode<T> > get_branch(const core::index2 index) {

    if (left == nullptr) {
      if (right == nullptr) {
        throw std::invalid_argument(utils::string_format("requested branch for index %d but this node %d is a leaf!", index, id));
      } else {
        if (index == 0) return right;
        else throw std::invalid_argument(utils::string_format("requested branch for index %d but this node %d has only one branch at index = 0!", index, id));
      }
    } else {
      if (right == nullptr) {
        if (index == 0) return left;
        else throw std::invalid_argument(utils::string_format("requested branch for index %d but this node %d has only one branch at index = 0!", index, id));
      } else {
        if (index == 0) return left;
        if (index == 1) return right;
        throw std::invalid_argument(utils::string_format("requested branch for index %d is too high because this node %d is the binary one!", index, id));
      }
    }
  }

  /** @brief Provides access to the requested branch and sets the node's flag accordingly.
   *
   * This operation sets the flag of this node to <code>index+1</code> value to mark which branch has been visited recently
   * @param index - which branch; must be in the range of \f$[0,N)\f$ where \f$N\f$ is the number of branches returned by count_branches()
   * @return the requested branch.
   */
  inline std::shared_ptr<BinaryTreeNode<T> > visit_branch(const core::index2 index) {
    flag = index+1;
    return get_branch(index);
  }

  /** @brief Returns the first branch that hasn't been yet visited.
   *
   * This operation sets the flag of this node to <code>++flag</code> value to mark that the very next branch has been visited.
   * @return the first branch that hasn't been yet visited or nullptr if all branches of that node have been visited already.
   */
  inline std::shared_ptr<BinaryTreeNode<T> > visit_branch() {
    ++flag;
    return (flag > count_branches()) ? nullptr : get_branch(flag-1);
  }

  template<typename J>
  friend std::ostream& operator<<(std::ostream &out, const BinaryTreeNode<J> &e);

  template<typename J>
  friend std::ostream& operator<<(std::ostream &out, const BinaryTreeNode<J> *e);

private:
  char flag = 0;
  core::index4 n_leaves_ = 1; ///< The number of leaves in the tree rooted in this cluster
  std::shared_ptr<BinaryTreeNode<T> > left = nullptr;
  std::shared_ptr<BinaryTreeNode<T> > right = nullptr;
  std::shared_ptr<BinaryTreeNode<T> > root = nullptr;
};

template<typename T>
std::ostream& operator<<(std::ostream &out, const BinaryTreeNode<T> &e) {

  out << e.id << " : " << e.element;
  return out;
}

template<typename T>
std::ostream& operator<<(std::ostream &out, const BinaryTreeNode<T> *e) {

  if (e == 0) out << "NULL";
  else
    out << e->id << " : " << e->element;
  return out;
}

}
}
}
#endif
