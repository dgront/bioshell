#ifndef CORE_ALGORITHMS_TREES_TreeNode_H
#define CORE_ALGORITHMS_TREES_TreeNode_H

#include <iostream>
#include <stdexcept>
#include <memory> // for shared_ptr
#include <utils/string_utils.hh>

namespace core {
namespace algorithms {
namespace trees {

/** @brief  A node in a tree.
 *
 * It may be a root of the whole tree as well.
 * The following example reads a number of file paths found in a directory and builds a directory tree
 * \include ex_TreeNode.cc
 */
template<typename T>
class TreeNode : public std::enable_shared_from_this<TreeNode<T> > {
public:

  /// Define the type of the input-iterator returned by <code>begin()</code> and <code>end()</code>
  typedef typename std::vector<std::shared_ptr<TreeNode<T> > >::iterator TreeNodeIterator;

  /// Define type for const-iterators
  typedef typename std::vector<std::shared_ptr<TreeNode<T> > >::const_iterator TreeNodeConstIterator;

  int id;     ///< ID of this node
  T element;  ///< Element stored at this node

  /** @brief  Bare constructor creates an empty node.
   *
   * @param no - integer identifier assigned to this tree node
   */
  TreeNode(int no = 0) : id(no) { }

  /// default virtual destructor
  virtual ~TreeNode() = default;

  /** @brief Creates a node to store a given element.
   *
   * @param no - integer identifier assigned to this tree node
   * @param elem - data element stored at this node
   */
  TreeNode(T elem, int no = 0) : id(no), element(elem) { }

  /// Returns the left-most branch rooted in this node
  inline std::shared_ptr<TreeNode<T> > get_left() const { return branches[0]; }

  /// Returns the right-most branch rooted in this node
  inline std::shared_ptr<TreeNode<T> > get_right() const { return branches.back(); }

  /// Returns the root node in this node
  inline std::shared_ptr<TreeNode<T> > get_root() const { return root; }

  /** @brief  Adds a new branch to this node.
   *
   * This will automatically set the root definition for the node <code>b</code> to <code>this</code>.
   */
  inline void add_branch(std::shared_ptr<TreeNode<T>> b) {
    branches.push_back(b);
    b->root = std::enable_shared_from_this<TreeNode<T> >::shared_from_this();
  }

  /** \brief Sets a value for an flag in this node.
   *
   * This operation may be useful for marking visited nodes.
   * @param new_flag a new value for the flag
   */
  inline void set_flag(const char new_flag) { flag = new_flag; }

  /** \brief reads a node's flag
   *
   * This operation may be useful for marking visited nodes
   * @return a flag's value
   */
  inline char get_flag() const { return flag; }

  /** \brief Return true if this is a leaf node. */
  inline bool is_leaf() const { return (branches.size() == 0); }

  /** @brief Returns the number of branches of this node.
   * To count all the leaves in this subtree, call count_leaves()
   * @return number of branches of this node
   */
  inline core::index4 count_branches() const { return branches.size(); }

  /** @brief Returns the requested branch.
   * @param index - which branch; must be in the range of [0,n) where N is
   * the number of branches returned by count_branches()
   * @return the requested branch
   */
  inline std::shared_ptr<TreeNode<T> > get_branch(const core::index2 index) { return branches[index]; }

  /** @brief Returns an iterator to the first child of this node.
   *
   * The returned iterator may be used by STL algorithms.
   */
  TreeNodeIterator begin() { return branches.begin(); }

  /** @brief Returns a pass-the-end iterator to children of this node.
   *
   * The returned iterator may be used by STL algorithms
   */
  TreeNodeIterator end() { return branches.end(); }

  /** @brief Returns a const-iterator to the first child of this node.
   *
   * The returned iterator may be used by STL algorithms.
   */
  TreeNodeConstIterator begin() const { return branches.begin(); }

  /** @brief Returns a pass-the-end const-iterator to children of this node.
   *
   * The returned iterator may be used by STL algorithms
   */
  TreeNodeConstIterator end() const { return branches.end(); }

  /** @brief Provides const-access to the requested branch.
   * @param index - which branch; must be in the range of [0,n) where N is
   * the number of branches returned by count_branches()
   * @return the requested branch
   */
  inline const std::shared_ptr<TreeNode<T> > get_branch(const core::index2 index) const { return branches[index]; }

  /** @brief Provides access to the requested branch and sets the node's flag accordingly.
   *
   * This operation sets the flag of this node to <code>index+1</code> value to mark which branch has been visited recently
   * @param index - which branch; must be in the range of [0,N) where N is the number of branches returned by count_branches()
   * @return the requested branch.
   */
  inline std::shared_ptr<TreeNode<T>> visit_branch(const core::index2 index) {
    flag = index+1;
    return branches[index];
  }

  /** @brief Returns the first branch that hasn't been yet visited.
   *
   * This operation sets the flag of this node to <code>++flag</code> value to mark that the very next branch has been visited.
   * @return the first branch that hasn't been yet visited or nullptr if all branches of that node have been visited already.
   */
  inline std::shared_ptr<TreeNode<T>> visit_branch() {
    ++flag;
    return (flag > branches.size()) ? nullptr : branches[flag - 1];
  }

protected:
  int flag = 0;        ///< A flag for various needs
  core::index4 n_leaves_ = 1; ///< The number of leaves in the tree rooted in this cluster; by default it is = 1 because it just includes this one node
  std::shared_ptr<TreeNode<T>> root; ///< The root node
  std::vector<std::shared_ptr<TreeNode<T> > > branches; ///< Vector of all the branches rooted in this node
};


}
}
}
#endif
/**
 * \example ex_TreeNode.cc
 */
