#ifndef CORE_ALGORITHMS_TREES_algorithms_H
#define CORE_ALGORITHMS_TREES_algorithms_H

#include <vector>
#include <iostream>

#include <core/index.hh>
#include <core/algorithms/trees/TreeNode.hh>
#include <core/algorithms/trees/BinaryTreeNode.hh>

namespace core {
namespace algorithms {
namespace trees {

/** @brief Traverse the tree using depth-first method, execute the given node operation in pre-order.
 * This routine is intended to work with a generic tree node. To traverse a binary tree nodes, use
 * breath_first_preorder();
 */
template<typename T, typename Op>
void depth_first_preorder(const T trie_node, Op operation) {

  std::vector<T> s;
  s.push_back(trie_node);
  operation(trie_node);
  while (s.size() > 0) {
    T t = s.back();
    s.pop_back();
    if (!t->is_leaf()) {
      T tt = t->visit_branch();
      if (tt != nullptr) {
        operation(tt);
        s.push_back(t);
        s.push_back(tt);
      } else t->set_flag(0);
    }
  }
}

/** @brief Traverse a binary tree using breadth-first method, execute the given node operation in pre-order.
 */
template<typename E, typename Op>
void breadth_first_preorder(const std::shared_ptr<BinaryTreeNode<E>> trie_node, Op operation) {

  std::vector<std::shared_ptr<BinaryTreeNode<E>>> s;
  s.push_back(trie_node);
  operation(trie_node);
  while (s.size() > 0) {
    std::shared_ptr<BinaryTreeNode<E>> t = s.back();
    s.pop_back();
    if (t->has_left()) {
      operation(t->get_left());
      s.push_back(t->get_left());
    }
    if (t->has_right()) {
      operation(t->get_right());
      s.push_back(t->get_right());
    }
    t->set_flag(0);
  }
}

/** @brief Calculates the size of a given binary (sub)-tree.
 *
 * This function count all the nodes of the given tree, no matter if they are leaves or not
 * @see count_leaves(const std::shared_ptr<BinaryTreeNode<E>>) to count leaves only
 */
template<typename E>
static core::index4 size(const std::shared_ptr<BinaryTreeNode<E>> trie_node) {

  size_t size = 0;
  std::vector<std::shared_ptr<BinaryTreeNode<E>>> s;
  s.push_back(trie_node);
  while (s.size() > 0) {
    const std::shared_ptr<BinaryTreeNode<E>> t = s.back();
    s.pop_back();
    size++;
    if (t->has_left()) s.push_back((t->get_left()));
    if (t->has_right()) s.push_back((t->get_right()));
  }

  return size;
}

/** @brief Calculates the height of a given binary tree rooted in a node.
 */
template<typename E>
static core::index4 height(const std::shared_ptr<BinaryTreeNode<E>> root) {

  if (root == nullptr) return 0;
  int hl = height(root->get_left());
  int hr = height(root->get_right());

  return std::max(hl, hr) + 1;
}

/** @brief Collect all nodes that are exactly on a given level of the tree.
 *
 * This function recursively calls itself until it reaches the desired tree level
 *
 * @tparam E - type of data that is stored at every tree's node
 * @param root - root of a tree
 * @param tree_level - desired level to be extracted
 * @param storage - vector where the output elements are stored
 * @param current_level - current level (recursive parameter)
 */
template<typename E>
static void collect_given_level(const std::shared_ptr<BinaryTreeNode<E>> root, const core::index2 tree_level,
              std::vector<std::shared_ptr<BinaryTreeNode<E>>> & storage, const core::index2 current_level = 0) {

  if (root == nullptr and tree_level==0) {
    storage.push_back(root);
    return;
  }
  if(current_level==tree_level) {
    storage.push_back(root);
  } else {
    collect_given_level(root->get_left(), tree_level, storage, current_level + 1);
    collect_given_level(root->get_right(), tree_level, storage, current_level + 1);
  }
}

/** @brief Calculates the size of a given binary (sub)-tree.
 *
 * This function count only leaves of the given tree
 * @see size(const std::shared_ptr<BinaryTreeNode<E>>) to count all nodes
 */
template<typename E>
static core::index4 count_leaves(const std::shared_ptr<BinaryTreeNode<E>> trie_node) {

  size_t size = 0;
  std::vector<std::shared_ptr<BinaryTreeNode<E>>> s;
  s.push_back(trie_node);
  while (s.size() > 0) {
    const std::shared_ptr<BinaryTreeNode<E>> t = s.back();
    s.pop_back();
    if ((t->has_left()) || (t->has_right())) {
      if (t->has_left()) s.push_back((t->get_left()));
      if (t->has_right()) s.push_back((t->get_right()));
    } else
      size++;
  }

  return size;
}

/** @brief Calculates the size of a given generic (sub)-tree.
 */
template<typename E>
static core::index4 size(const std::shared_ptr<TreeNode<E>> trie_node) {

  size_t size = 1;
  std::vector<std::shared_ptr<TreeNode<E>>> s;
  if(! trie_node->is_leaf()) s.push_back(trie_node);
  std::shared_ptr<TreeNode<E>> tt;
  while (s.size() > 0) {
    const std::shared_ptr<TreeNode<E>> t = s.back();
    s.pop_back();

    while ((tt = t->visit_branch()) != nullptr) {
      ++size;
      if (!tt->is_leaf()) s.push_back(tt);
    }
  }

  return size;
}

/** @brief Calculates the height of a given tree rooted in a node.
 */
template<typename E>
static core::index4 height(const std::shared_ptr<TreeNode<E>> root) {

  if (root == nullptr) return -1;
  int h = 0;
  for (index2 i = 0; i < root->count_branches(); ++i) {
    h = std::max(h, height(root->get_branch(i)));
  }

  return h + 1;
}


/** @brief Calculates the depth of a given node of a (sub)-tree.
 */
template<typename T>
static core::index2 depth(const T trie_node) {

  index2 d = 0;
  T t = trie_node;
  while (t->get_root() != nullptr) {
    t = t->get_root();
    ++d;
  }
  return d;
}

/** @brief Collects all leaf nodes that are rooted in a given subtree.
 *
 * The resulting elements are pushed back to the given vector.
 * @param trie_node - a (sub)tree root
 * @param vec - all leaves  will be stored in this vector. The vector <strong>is not cleared</strong> prior the operation.
 */
template<typename E>
static void collect_leaf_nodes(const std::shared_ptr<BinaryTreeNode<E>> trie_node, std::vector<std::shared_ptr<BinaryTreeNode<E>>> & leaf_nodes) {

  std::vector<std::shared_ptr<BinaryTreeNode<E>>> s;
  s.push_back(trie_node);
  while (s.size() > 0) {
    const std::shared_ptr<BinaryTreeNode<E>> t = s.back();
    s.pop_back();

    if (t->is_leaf()) {
      leaf_nodes.push_back(t);
      continue;
    }
    if (t->has_left()) s.push_back((t->get_left()));
    if (t->has_right()) s.push_back((t->get_right()));
  }
}

/** @brief Collects all leaf elements that are stored on the subtree rooted at a given tree node.
 *
 * The resulting leaf elements are pushed back to the given vector.
 *
 * <strong>Note</strong>, that this method collects just the data stored in leaf nodes ot his subtree rather than tree nodes themselves.
 * @param trie_node - a (sub)tree root
 * @param vec - all leaf node elements will be stored in this vector. The vector <strong>is not cleared</strong> prior the operation.
 */
template<typename E>
static void collect_leaf_elements(const std::shared_ptr<BinaryTreeNode<E>>  trie_node, std::vector<E> & elements) {

  std::vector<std::shared_ptr<BinaryTreeNode<E>>> s;
  s.push_back(trie_node);
  while (s.size() > 0) {
    const std::shared_ptr<BinaryTreeNode<E>>  t = s.back();
    s.pop_back();

    if (t->is_leaf()) {
      elements.push_back(t->element);
      continue;
    }
    if (t->has_left()) s.push_back((t->get_left()));
    if (t->has_right()) s.push_back((t->get_right()));
  }
}

/** @brief Collects all elements that are stored on the subtree rooted in a given subtree.
 *
 * The resulting elements are pushed back to the given vector.
 * @param trie_node - a (sub)tree root
 * @param vec - all node elements will be stored in this vector. The vector <strong>is not cleared</strong> prior the operation.
 */
template<typename E>
static void collect_elements(const std::shared_ptr<BinaryTreeNode<E>> trie_node, std::vector<E> & elements) {

  std::vector<std::shared_ptr<BinaryTreeNode<E>>> s;
  s.push_back(trie_node);
  while (s.size() > 0) {
    const std::shared_ptr<BinaryTreeNode<E>>  t = s.back();
    s.pop_back();
    elements.push_back(t->element);
    if (t->has_left()) s.push_back((t->get_left()));
    if (t->has_right()) s.push_back((t->get_right()));
  }
}

}
}
}

#endif
