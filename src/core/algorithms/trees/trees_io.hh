/** \file trees_io.hh
 * @brief Provides I/O methods for trees, e.g. phylogenetic trees or clustering results
 *
 */

#ifndef CORE_ALGORITHMS_TREES_trees_io_H
#define CORE_ALGORITHMS_TREES_trees_io_H

#include <vector>
#include <functional>
#include <iostream>

#include <core/index.hh>
#include <utils/string_utils.hh>

namespace core {
namespace algorithms {
namespace trees {

/** @brief Formatter used to write a tree in XML format.
 *
 * The class bears the three methods that are necessary to print a tree (e.g. a clustering output)
 * in the XML format that may be loaded into TreeViz program.
 * @tparam N a tree node type, e.g. BinaryTreeNode
 * @see TreeViz http://www.randelshofer.ch/treeviz/
 */
template<typename N>
class XMLFormatters {
public:

  std::ostream & out_stream;
  int level = 0;
  std::function<void(const N)> start;
  std::function<void(const N)> stop;
  std::function<void(const N)> leaf;


  /** @brief Creates a formatter that writes the clustering tree into a given stream.
   *
   * @param sink - an output tree where the data is printed
   */
  XMLFormatters(std::ostream & sink) : out_stream(sink),

  start([this](N node_sp) {
    (this->level)++;
    for (int i = 0; i < this->level - 1; i++) this->out_stream << "  ";
    std::stringstream ss;
    ss << node_sp->element;
    this->out_stream << utils::string_format("<L%d name=\""+ss.str()+"\"  size=\"%u\">\n", this->level, size(node_sp));
  }),

  stop([this](N node_sp) {
    for (int i = 0; i < this->level - 1; i++) this->out_stream << "  ";
    this->out_stream << utils::string_format("</L%d>\n", this->level);
    (this->level)--;
  }),

  leaf([this](N node_sp) {
    for (int i = 0; i < this->level; i++) this->out_stream << "  ";
    std::stringstream ss;
    ss << node_sp->element;
    this->out_stream << utils::string_format("<L%d name=\""+ss.str()+"\"  size=\"%u\" />\n", this->level+1,  size(node_sp));
  }) {}

};


/** @brief Writes a tree into a file
 *
 * The method writes a tree object into a file in one of the following formats:
 *    - XML (which can be loaded into TreeViz program)
 *
 * @tparam T a tree node type, e.g. BinaryTreeNode
 * @tparam W functor type that will be called to print a tree node. Its signature has to be:
 * @code
 * std::function<void(const T)>
 * @endcode
 *
 * More specifically, this method traverses a given tree in a depth-first way and:
 *   - for each <strong>new</node> non-leaf node visited, calls <code>write_prolog</code> to write about the fact it starts
 *   - for each leaf calls <code>write_leaf</code> to write it
 *   - once all branches of a given node were visited, <code>write_epilog</code> is called to write the end of the node
 * @see TreeViz http://www.randelshofer.ch/treeviz/
 */
template<typename T, typename W>
static void write_tree(T root_trie_node, W write_prolog, W write_leaf, W write_epilog) {
  std::vector<T> s;
  write_prolog(root_trie_node);
  s.push_back(root_trie_node);
  while (s.size() > 0) {
    const T t = s.back();
    s.pop_back();
    if (t->count_branches() == t->get_flag()) { // we've already seen all the branches from this node
      write_epilog(t);
      continue;
    }
    auto f = t->get_flag();
    const T tn = t->get_branch((core::index2) f);
    ++f;
    t->set_flag(f);
    s.push_back(t);
    if (tn->is_leaf()) write_leaf(tn);
    else {
      s.push_back(tn);
      write_prolog(tn);
    }
  }
}

}
}
}

#endif
