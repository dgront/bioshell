#ifndef CORE_ALGORITHMS_GraphWithData_HH
#define CORE_ALGORITHMS_GraphWithData_HH

#include <vector>
#include <map>

#include <core/index.hh>
#include <core/algorithms/GivenOrderIterator.hh>
#include <utils/Logger.hh>
#include <utils/string_utils.hh>

namespace core {
namespace algorithms {

/** @brief Decorator for a graph type to store data values on edges and vertices.
 *
 * Both edges and vertices data is stored by this class
 * @tparam <G> - graph type, such a SimpleGraph
 * @tparam <V> - a type for vertices
 * @tparam <E> - a type for edges
 */
template <typename G, typename V, typename E>
class GraphWithData : public G {
public:

  /**  \brief Adds a vertex into this graph.
   *
   * The method does not check if the graph already has this vertex! If you are not sure,
   * use has_vertex() to check if you really  need to insert. The newly added vertex will not be connected to anything
   * @param vertex a new vertex to be inserted
   * @return integer index referring to the inserted vertex; the index may be further used to declare bonds within this molecule
   */
  index4 add_vertex(const V & v) {

    v_to_index_map_[v] = vertices_.size();
    vertices_.push_back(v);
    G::add_vertex();

    return vertices_.size() - 1;
  }

  /** \brief Adds an edge into this graph that binds two given vertices.
   *
   * <p>The method binds two vertices that belong to this graph with an edge.
   * Since this graph is undirected, the order in which the two vertices
   * are given is unimportant</p>
   *
   * <p>This method forces the properties of a simple graph, i.e. it checks whether
   * the two given vertices are already connected. If so, a new edge is not added and
   * <strong>false</strong> value returned. Otherwise the method inserts the new edge and
   * returns <strong>true</strong>.
   *
   * @param vi - the first vertexes object to be connected; if @code vi @endcode is not a vertex of this graph, it will be added
   * @param vj - the second vertexes object to be connected; if @code vi @endcode is not a vertex of this graph, it will be added
   * @param edge - the edge object
   * @return false if the two vertices are already connected by an edge; true otherwise
   * @see add_vertex(V)
   */
  bool add_edge(const V &vi, const V &vj, const E &edge) {

    index4 ii = index_or_add(vi);
    index4 ij = index_or_add(vj);

    return add_edge(ii, ij, edge);
  }

  /** \brief Adds an edge into this graph that binds two given vertices.
   *
   * <p>The method binds two vertices that belong to this graph with an edge.
   * Since this graph is undirected, the order in which the two vertices
   * are given is unimportant</p>
   *
   * <p>This method forces the properties of a simple graph, i.e. it checks whether
   * the two given vertices are already connected. If so, a new edge is not added and
   * <strong>false</strong> value returned. Otherwise the method inserts the new edge and
   * returns <strong>true</strong>.
   *
   * @param vi - index of the first of two vertexes to be connected
   * @param vj - index of the second of two vertexes to be connected (the order is unimportant)
   * @param edge - the edge object
   * @return false if the two vertices are already connected by an edge; true otherwise
   * @see add_vertex(V)
   */
  bool add_edge(const index4 & vi, const index4 & vj, const E & edge) {

    std::pair<index4, index4>  key = (vi<vj) ? std::make_pair(vi,vj) : std::make_pair(vj,vi);
    if(edges_.find(key)==edges_.end()) {
      edges_[key] = edge;
      G::add_edge(vi,vj);
      return true;
    }

    return false;
  }

  /** @brief Returns true if the two given vertices are connected by an edge
   * @param vi - the first vertex
   * @param vj - the second vertex
   * @return true if the two given vertices are connected by an edge
   */
  bool are_connected(const V & vi, const V & vj) const {

    if(v_to_index_map_.find(vi)==v_to_index_map_.end()) return false;
    if(v_to_index_map_.find(vj)==v_to_index_map_.end()) return false;
    return G::are_connected(v_to_index_map_.at(vi), v_to_index_map_.at(vj));
  }

  /** @brief Removes an edge between two vertices if it exists
   *
   * @param vi - the first vertex
   * @param vj - the second vertex
   * @return if the edge existed and was removed - returns true; false if there was no such an edge
   */
  bool remove_edge(const V &vi, const V &vj) {

    int ii = index(vi);
    int ij = index(vj);
    return G::remove_edge(ii, ij);
  }

  /** @brief Returns the internal index of a given vertex object
   * @param vi - the vertex object
   * @return internal integer index or -1 if v is not a vertex of this graph
   */
  int index(const V &vi) const {

    const auto it = v_to_index_map_.find(vi);
    if (it == v_to_index_map_.end()) return -1;
    return it->second;
  }

  /** @brief Returns the internal index of a given vertex object.
   *
   * If  v is not a vertex of this graph, it will be inserted
   * @param vi - the vertex object
   * @return internal integer index
   */
  int index_or_add(V vi) {

    const auto it = v_to_index_map_.find(vi);
    if (it == v_to_index_map_.end()) return add_vertex(vi);
    return it->second;
  }

  /** \brief Returns true if a given vertex belongs to this graph.
   * @param vi - a object that might be a vertex of this graph
   * @return true if possibleVertex really belongs to this graph
   */
  bool has_vertex(const V &vi) const { return (v_to_index_map_.find(vi) != v_to_index_map_.end()); }

  /** \brief Returns the number of edges attached to a given vertex
   * @return  the number of edges attached to a given vertex of this graph.
   */
  index4 count_edges(const V &vi) const { return G::count_edges(index(vi)); }

  /** \brief Returns the total number of edges of this graph.
   * @return  the total number of edges of this graph.
   */
  index4 count_edges() const { return G::count_edges(); }

  /// Provides begin iterator fot vertex objects
  typename std::vector<V>::iterator begin() { return vertices_.begin(); }

  /// Provides end iterator fot vertex objects
  typename std::vector<V>::iterator end() { return vertices_.end(); }

  /// Provides const-begin iterator fot vertex objects
  typename std::vector<V>::const_iterator cbegin() const { return vertices_.cbegin(); }

  /// Provides const-end iterator fot vertex objects
  typename std::vector<V>::const_iterator cend() const { return vertices_.cend(); }

  /** @brief Provides begin iterator fot vertex objects that are directly connected to a given vertex
   * @param vi - a given vertex
   * @return begin iterator to vertices attached to @code vi @endcode
   */
  core::algorithms::GivenOrderIterator<V,typename G::iterator> begin(const V &vi) {
    int i = index(vi);
    return GivenOrderIterator<V,typename G::iterator>::begin(vertices_,G::begin(i), G::end(i));
  }

  /** @brief Provides end iterator fot vertex objects that are directly connected to a given vertex
   * @param vi - a given vertex
   * @return begin iterator to vertices attached to @code vi @endcode
   */
  core::algorithms::GivenOrderIterator<V,typename G::iterator> end(const V &vi) {
    int i = index(vi);
    return GivenOrderIterator<V,typename G::iterator>::end(vertices_,G::begin(i), G::end(i));
  }

  /** @brief Provides begin const-iterator fot vertex objects that are directly connected to a given vertex
   * @param vi - a given vertex
   * @return begin iterator to vertices attached to @code vi @endcode
   */
  core::algorithms::GivenOrderConstIterator<V, typename G::const_iterator> cbegin(const V &vi) const {
    int i = index(vi);
    if (i < 0) throw std::invalid_argument("Invalid vertex");
    return GivenOrderConstIterator<V, typename G::const_iterator>::
           cbegin(vertices_, G::cbegin(index4(i)),G::cend(index4(i)));
  }

  /** @brief Provides end const-iterator fot vertex objects that are directly connected to a given vertex
   * @param vi - a given vertex
   * @return begin iterator to vertices attached to @code vi @endcode
   */
  core::algorithms::GivenOrderConstIterator<V, typename G::const_iterator> cend(const V &vi) const {
    int i = index(vi);
    if (i < 0) throw std::invalid_argument("Invalid vertex");
    return GivenOrderConstIterator<V, typename G::const_iterator>::cend(vertices_, G::cbegin(index4(i)),
                                                                         G::cend(index4(i)));
  }

  /// Returns a vertex object assigned to a given internal index of this graph
  V & vertex(const index4 id) { return vertices_[id]; }

  /// Returns a vertex object assigned to a given internal index of this graph
  const V & vertex(const index4 id) const { return vertices_[id]; }

  /** @brief Returns an object stored at a given edge of this graph.
   * @param vi - index of the first vertex
   * @param vj - index of the second vertex
   * @return edge object
   */
  E & edge(const index4 vi, const index4 vj) {

    std::pair<index4, index4>  key = (vi<vj) ? std::make_pair(vi,vj) : std::make_pair(vj,vi);
    if(edges_.find(key)==edges_.end())
      throw std::out_of_range(utils::string_format("Edge not found between  %d and %d", vi, vj));

    return edges_[key];
  }

  /** @brief Returns an object stored at a given edge of this graph.
   * @param vi - the first vertex
   * @param vj - the second vertex
   * @return edge object
   */
  E & edge(const V & vi, const V & vj) {

    int i_vi = index(vi);
    int i_vj = index(vj);
    std::pair<index4, index4>  key = (i_vi<i_vj) ? std::make_pair(i_vi,i_vj) : std::make_pair(i_vj,i_vi);
    if(edges_.find(key)==edges_.end())
      throw std::out_of_range(utils::string_format("Edge not found between  %d and %d", i_vi, i_vj));

    return edges_[key];
  }

  /** @brief Returns an object stored at a given edge of this graph.
   * @param vi - the first vertex
   * @param vj - the second vertex
   * @return edge const object
   */
  const E & edge(const V & vi, const V & vj) const {

    int i_vi = index(vi);
    int i_vj = index(vj);
    std::pair<index4, index4>  key = (i_vi<i_vj) ? std::make_pair(i_vi,i_vj) : std::make_pair(i_vj,i_vi);
    if(edges_.find(key)==edges_.end())
      throw std::out_of_range(utils::string_format("Edge not found between  %d and %d", i_vi, i_vj));

    return edges_.at(key);
  }

  /// Returns begin const-iterator to edges of this graph
  typename std::map<std::pair<index4, index4>,E>::const_iterator cbegin_edges() const { return edges_.cbegin(); }

  /// Returns end const-iterator to edges of this graph
  typename std::map<std::pair<index4, index4>,E>::const_iterator cend_edges() const { return edges_.cend(); }

private:
  std::vector<V> vertices_;
  std::map<V,index4> v_to_index_map_;
  std::map<std::pair<index4, index4>,E> edges_;
};

}
}

#endif
