/** @file predicates.hh
 * @brief provides some basic predicates to be used with the STL machinery
 */
#ifndef CORE_ALGORITHMS_predicates_H
#define CORE_ALGORITHMS_predicates_H

namespace core {
namespace algorithms {

/** @brief Negates a given predicate.
 *
 * @tparam Functor - functor type to be negated
 */
template<class Functor>
struct Not {

  /** @brief Constructor takes a functor and negates it
   *
   * @param f - functor to be negated
   */
  Not(const Functor & f) : func(f) { }

  /** @brief calls the underlying functor and negates its result
   *
   * @tparam ArgType - functor's argument type
   * @param arg - functor argument
   */
  template<typename ArgType>
  bool operator()(ArgType & arg) const { return !func(arg); }

private:
  const Functor & func;
};

/** @brief Negates a given predicate.
 *
 * @tparam Functor - functor type to be negated
 */
template<class Functor>
struct DereferencedNot {

  /** @brief Constructor takes a functor and negates it
   *
   * @param f - functor to be negated
   */
  DereferencedNot(const Functor & f) : func(f) { }

  /** @brief Calls the underlying functor with dereferenced argument and negates its result.
   *
   * @tparam ArgType - functor's argument type
   * @param arg - functor argument
   */
  template<typename ArgType>
  bool operator()(ArgType & arg) const { return !func(*arg); }

private:
  const Functor & func;
};


/** @brief Logic AND of two predicates.
 *
 * @tparam FunctorA - type of the first functor
 * @tparam FunctorB - type of the second functor; in general might be different than the first one, but must accept the very same argument type
 * @tparam ArgType - the type of argument a functor accepts
 */
template<class FunctorA, class FunctorB, class ArgType>
struct And {

  /** @brief Constructor takes two functors and combines them
   *
   * @param f1 - the first functor to be combined
   * @param f2 - the second functor to be combined
   */
  And(const FunctorA & f1,const FunctorB & f2) : func1(f1), func2(f2) { }

  /** @brief calls the underlying functors and returns true if and only if both are true
   *
   * @tparam ArgType - functor's argument type
   * @param arg - functor argument
   */
  bool operator()(const ArgType & arg) const { return (func1(arg) && func2(arg)); }

private:
  const FunctorA & func1;
  const FunctorB & func2;
};

}
}

#endif
