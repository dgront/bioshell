/** @file Combinations.hh
 * @brief Provides Combinations class that generates combinations
 *
 */
#ifndef CORE_ALGORITHMS_Combinations_H
#define CORE_ALGORITHMS_Combinations_H

#include <vector>
#include <iostream>
#include <algorithm>

#include <core/index.hh>

namespace core {
namespace algorithms {

/** @brief Generate \f$ {N}\choose{k} \f$ combinations of k elements from a set of N elements.
 *
 * The class generates combinations without repetitions. These you can get by creating permutations
 * of each combination (e.g. use  <code>std::next_permutation()</code>
 *
 * @tparam T - type of data
 */
template <typename T>
class Combinations {
public:

  /** @brief Create a combination generator.
   *
   * Generator will provide \f$ {N}\choose{k}\f$ combinations where \f$ k \f$ is the first argument of this constructor and
   * \f$ N \f$ is the size of the provided vector of elements.
   *
   * @param k - size of a combination
   * @param elements  - input elements to choose from
   */
  Combinations(const index1 k,const std::vector<T> & elements) : has_next_(true), elements_(elements), flags(elements_.size()) {
    std::fill(flags.end() - k, flags.end(), true);
  }

  /** Provides the next combination.
   *
   * @param a_combination - a reference of a vector where to combination will be stored
   * @return true if the combination was actually generated; false if the previous call of this method generated the last combination
   */
  bool next(std::vector<T> &a_combination) {

    if (!has_next_) return false;
    index4 j = 0;
    for (index4 i = 0; i < elements_.size(); ++i) {
      if (flags[i]) {
        a_combination[j] = elements_[i];
        ++j;
      }
    }
    has_next_ = std::next_permutation(flags.begin(), flags.end());

    return true;
  }

private:
  bool has_next_;
  const std::vector<T> & elements_;
  std::vector<bool> flags;
};
}
}

#endif
