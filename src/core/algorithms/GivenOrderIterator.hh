#ifndef CORE_ALGORITHMS_GivenOrderIterator_HH
#define CORE_ALGORITHMS_GivenOrderIterator_HH

#include <iterator>
#include <iostream>
#include <vector>

namespace core {
namespace algorithms {

/** @brief Iterator that access elements of an underlying vector in the given order
 *
 * This class provides <em>iterator<em> that effectively changes order of a given std::vector<T>
 *
 */
template<typename T, typename It>
class GivenOrderIterator {
public:
  /// Type of values this iterator iterates over
  typedef typename std::vector<T>::value_type value_type;
  typedef typename std::vector<T>::difference_type difference_type;
  typedef typename std::vector<T>::value_type *pointer;
  typedef typename std::vector<T>::value_type &reference;
  typedef std::input_iterator_tag iterator_category;

  /** @brief Iterates over pairs (i,j) that may be used to index 2D array.
   *
   */
  GivenOrderIterator(std::vector<T> &source, const It sequence_begin, const It sequence_end)
  : iter_(sequence_begin),begin_(sequence_begin), end_(sequence_end), source_(source) {}

  /** @brief Advance this iterator by one position.
   *
   * @return reference to the updated iterator (self reference)
   */
  inline GivenOrderIterator &operator++() {
    ++iter_;
    return *this;
  }

  /** @brief Returns a pair of indexes to the underlying 2D matrix
   * @return pair of indexes
   */
  value_type operator*() { return source_[*iter_]; }

  /** @brief Returns true if the two iterators point to the very same element
   * @param lhs - left-hand side operand
   * @param rhs - right-hand side operand
   * @return true or false
   */
  friend bool operator==(const GivenOrderIterator &lhs, const GivenOrderIterator &rhs) {
    return (lhs.iter_ == rhs.iter_);
  }

  /** @brief Returns true if the two iterators point to a different element
   * @param lhs - left-hand side operand
   * @param rhs - right-hand side operand
   * @return true or false
   */
  friend bool operator!=(const GivenOrderIterator &lhs, const GivenOrderIterator &rhs) { return !(lhs == rhs); }

  /** @brief Returns an iterator that points to the very first element of an array.
   *
   * @return iterator that points to the first element
   */
  static GivenOrderIterator begin(std::vector<T> &source, const It sequence_begin, const It sequence_end) {
    return GivenOrderIterator{source, sequence_begin, sequence_end};
  }

  /// Returns an iterator that points behind the very last element of a matrix
  static GivenOrderIterator end(std::vector<T> &source, const It sequence_begin, const It sequence_end) {

    GivenOrderIterator it{source, sequence_begin, sequence_end};
    it.iter_ = it.end_;
    return it;
  }

private:
  It iter_;
  It begin_;
  It end_;
  std::vector<T> &source_;
};

/** @brief const-iterator that access elements of an underlying vector in the given order
 *
 * This class provides <em>iterator<em> that effectively changes order of a given std::vector<T>
 *
 */
template<typename T, typename It>
class GivenOrderConstIterator {
public:
  /// Type of values this iterator iterates over
  typedef typename std::vector<T>::value_type value_type;
  typedef typename std::vector<T>::difference_type difference_type;
  typedef typename std::vector<T>::value_type *pointer;
  typedef typename std::vector<T>::value_type &reference;
  typedef std::input_iterator_tag iterator_category;

  /** @brief Iterates over pairs (i,j) that may be used to index 2D array.
   *
   * @param n - dimension of the square array
   * @param if_triangular - if true, this iterator will go only through such (i,j) pairs where j>i (upper right triangle)
   */
  GivenOrderConstIterator(const std::vector<T> &source, const It sequence_begin, const It sequence_end)
      : iter_(sequence_begin),begin_(sequence_begin), end_(sequence_end), source_(source) {}

  /** @brief Advance this iterator by one position.
   *
   * @return reference to the updated iterator (self reference)
   */
  inline GivenOrderConstIterator &operator++() {
    ++iter_;
    return *this;
  }

  /** @brief Returns a pair of indexes to the underlying 2D matrix
   * @return pair of indexes
   */
  const value_type operator*() const { return source_[*iter_]; }

  /** @brief Returns true if the two iterators point to the very same element
   * @param lhs - left-hand side operand
   * @param rhs - right-hand side operand
   * @return true or false
   */
  friend bool operator==(const GivenOrderConstIterator &lhs, const GivenOrderConstIterator &rhs) {
    return (lhs.iter_ == rhs.iter_);
  }

  /** @brief Returns true if the two iterators point to a different element
   * @param lhs - left-hand side operand
   * @param rhs - right-hand side operand
   * @return true or false
   */
  friend bool operator!=(const GivenOrderConstIterator &lhs, const GivenOrderConstIterator &rhs) { return !(lhs == rhs); }

  /** @brief Returns an iterator that points to the very first element of an array.
   *
   * @return iterator that points to the first element
   */
  static GivenOrderConstIterator cbegin(const std::vector<T> &source, const It sequence_begin, const It sequence_end) {
    return GivenOrderConstIterator{source, sequence_begin, sequence_end};
  }

  /// Returns an iterator that points behind the very last element of a matrix
  static GivenOrderConstIterator cend(const std::vector<T> &source, const It sequence_begin, const It sequence_end) {

    GivenOrderConstIterator it{source, sequence_begin, sequence_end};
    it.iter_ = it.end_;
    return it;
  }

private:
  It iter_;
  It begin_;
  It end_;
  const std::vector<T> &source_;
};

}
}
#endif // ~ CORE_ALGORITHMS_GivenOrderIterator_HH

