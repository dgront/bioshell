#ifndef CORE_ALGORITHMS_graph_algorithms_HH
#define CORE_ALGORITHMS_graph_algorithms_HH

#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <memory>

#include <core/index.hh>
#include <core/algorithms/SimpleGraph.hh>

#include <utils/exit.hh>

namespace core {
namespace algorithms {

/** @brief Decomposes a graph into connected graphs.
 *
 * @tparam G - class representing a graph, e.g. SimpleGraph. The type must provide the following operations:
 *   - begin(V&)
 *   - end(V&)
 *   - vertices()
 *   - add_vertex(V&)
 *   - add_edge(V&, V&, E&)
 *   - edge(V&, V&)
 * @tparam V - class used to represent graph vertices
 * @tparam E - class used to represent graph edges
 * @param original_graph - the input graph
 * @param min_size - minimum size of a graph to be included in the returned vector
 * @return a vector of pointers to newly created subgraphs
 */
template<class G, class V, class E>
std::vector<std::shared_ptr<G>> connected_components(G &original_graph, core::index2 min_size = 1) {

  std::set<V> resolved_vertices;
  std::vector<std::shared_ptr<G>> out;

  bool if_new_graph = true;
  std::vector<V> stack;
  V & v = *(original_graph.begin());
  if (v == nullptr)
    utils::exit_with_error("graph_algorithms.hh", 43, "input set of atoms is empty!!");
  stack.push_back(v);
  while (if_new_graph) {
    std::shared_ptr<G> g_sp = std::make_shared<G>();
    out.push_back(g_sp);
    while (!stack.empty()) {      // --- keep adding vertices and edges until the stack is empty
      v = stack.back();           // --- take the last from stack
      stack.pop_back();
      resolved_vertices.insert(v);
      for (auto it = original_graph.begin(v); it != original_graph.end(v); ++it) {
        if (resolved_vertices.find(*it) != resolved_vertices.end()) continue;
        V vi = *it;
        g_sp->add_vertex(vi);
        g_sp->add_edge(v, vi, original_graph.edge(*it, v));
        stack.push_back(*it);
        resolved_vertices.insert(*it);
      }
    } // --- stack is empty <=> connected component is finished
// --- look for a member of a new connected component
    if_new_graph = false;
    for (auto it = original_graph.begin(); it != original_graph.end(); ++it) {
      if (resolved_vertices.find(*it) == resolved_vertices.end()) {
        stack.push_back(*it);
        if_new_graph = true;
        break;
      }
    }
    if(out.back()->count_vertices() < min_size)
      out.pop_back();
  }

  return out;
}

/// A helper function for find_cycles()
template<class G, class V, class E>
void cycles_recursion(G &original_graph, core::index2 min_length, std::vector<index4> path, std::vector<std::vector<index4>> &cycles) {

  // --- Get the most recently visited note last_v
  index4 last_v = path.back();

  // --- for every neighbor of the last node
  for (index4 vi:original_graph.get_adjacent(last_v)) {
    // --- Check if we are going back
    if (path.size() > 1 && path[path.size() - 2] == vi)
      continue;
    // --- find the element on a path (i.e. was it visited or not?)
    auto it = std::find(path.cbegin(), path.cend(), vi);
    if (it == path.cend()) {   // --- visit if not yet visited
      path.push_back(vi);
      cycles_recursion<G, V, E>(original_graph, min_length, path, cycles);
      path.pop_back();
    } else { // --- visited <=> a cycle
      std::vector<index4> a_cycle;
      std::copy(it, path.cend(), std::back_inserter(a_cycle));
      cycles.push_back(a_cycle);
    }
  }
}

/** @brief Finds cycles in a connected graph
 *
 * @tparam G - class representing a graph, e.g. SimpleGraph. The type must provide the following operations:
 *   - begin_vertex(V&)
 *   - end_vertex(V&)
 *   - vertices()
 *   - add_vertex(V&)
 *   - add_edge(V&, V&, E&)
 *   - get_edge(V&, V&)
 * @tparam V - class used to represent graph vertices
 * @tparam E - class used to represent graph edges
 * @param original_graph - the input graph
 * @param min_size - minimum size of a graph to be included in the returned vector
 * @return a vector of pointers to newly created subgraphs
 */
template<class G, class V, class E>
std::vector<std::vector<index4>> find_cycles(G &original_graph) {

  // --- working path (empty so far)
  std::vector<index4> wpath;
  wpath.push_back(0); // --- put first node on the path
  std::vector<std::vector<index4>> cycles; // --- container for cycles to be found
  cycles_recursion<G,V,E>(original_graph, 3, wpath, cycles); // --- run the recursion

  if (cycles.size() == 0) return cycles;

  // --- here is the section where duplicate cycles are removed
  std::vector<std::vector<index4>> final_cycles;
  auto it_min = std::min_element(cycles[0].begin(),cycles[0].end());
  std::rotate(cycles[0].begin(),it_min,cycles[0].end());
  final_cycles.push_back(cycles[0]);
  cycles[0] = cycles.back();
  cycles.pop_back();
  std::vector<index4> inv_cycle;
  for(std::vector<index4> & cycle : cycles) {
    // --- make a copy of the cycle and invert it
    inv_cycle.clear();
    std::copy(cycle.begin(), cycle.end(), std::back_inserter(inv_cycle));
    std::reverse(inv_cycle.begin(),inv_cycle.end());
    // --- rotate the inverted cycle
    auto it_min = std::min_element(inv_cycle.begin(),inv_cycle.end());
    std::rotate(inv_cycle.begin(),it_min,inv_cycle.end());
    // --- rotate the original cycle
    it_min = std::min_element(cycle.begin(),cycle.end());
    std::rotate(cycle.begin(),it_min,cycle.end());
    bool already_found = false;
    for (std::vector<index4> &fcycle : final_cycles) {
      if (fcycle.size() != cycle.size()) continue;
      if (std::equal(cycle.begin(), cycle.end(), fcycle.begin()) ||
          std::equal(inv_cycle.begin(), inv_cycle.end(), fcycle.begin())) {
        already_found = true;
        break;
      }
    }
    if(!already_found)
      final_cycles.push_back(cycle);
  }

  return final_cycles;
}

template<class G, class V, class E>
std::vector<index4> find_clique(index4 minimal_size, G &original_graph, index4 first_node) {

  std::vector<index4> clique; // --- container for clique members to be found
  const std::vector<index4> & nbrs = original_graph.get_adjacent(first_node);
  // --- if the given starting node doesn't have enough edges to form a clique, return an empty vector
  if (nbrs.size() + 1 < minimal_size) return clique;
  clique.push_back(first_node);
  clique.push_back(nbrs[0]);
  for (index4 i = 1; i < nbrs.size(); ++i) {
    index4 vi = nbrs[i];
    bool is_vi_ok = true;
    for (core::index4 ci:clique) {
      if (!original_graph.SimpleGraph::are_connected(vi, ci)) {
        is_vi_ok = false;
        break;
      }
    }
    if (is_vi_ok) clique.push_back(vi);
  }

  if(clique.size()<minimal_size) clique.clear();
  else
    std::sort(clique.begin(),clique.end());

  return clique;
}

}
}

#endif
