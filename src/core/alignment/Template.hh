#ifndef CORE_ALIGNMENT_Template_H
#define CORE_ALIGNMENT_Template_H

#include <string>
#include <vector>
#include <memory>
#include <stdexcept>

#include <core/data/basic/Vec3.fwd.hh>
#include <core/data/io/ss2_io.hh>
#include <core/data/sequence/SequenceProfile.hh>
#include <core/data/sequence/SecondaryStructure.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>

#include <core/alignment/Template.fwd.hh>
#include <core/alignment/Target.hh>

#include <core/chemical/Monomer.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace core {
namespace alignment {

using namespace core::data::sequence;
using namespace core::data::structural;
using namespace core::data::io;

/// Represents a template protein for a threading search
class Template : public Target {
public:

  /** \brief Constructor that creates a Template object from the relevant data pieces.
   *
   * The constructor loads a sequence profile, secondary structure and tertiary structure and builds a Template object to be used for 1D and 3D threading searches
   */
  Template(const std::string & name, const SequenceProfile_SP template_profile, const SecondaryStructure_SP template_ss,
      Chain_SP & template_residues);

  inline bool has_coordinates(core::index2 position) const { return !((*template_residues)[position] == nullptr); }
  inline const Residue_SP get_residue(core::index2 position) const { return (*template_residues)[position]; }
  static const Template_SP from_stream(std::istream & in);
  static const Template_SP from_file(const std::string & filename);

private:
  core::data::structural::Structure_SP backbone;
  Chain_SP template_residues;
  utils::Logger logger;
};

std::ostream & operator<<(std::ostream & out, const Template & t);

void load_templates(const std::vector<std::string> & fnames, std::vector<Template_SP> & templates);

}
}

#endif
