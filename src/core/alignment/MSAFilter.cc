
#include <core/alignment/MSAFilter.hh>

namespace core {
namespace alignment {

MSAFilter::MSAFilter(const std::vector<core::data::sequence::Sequence_SP> &msa) : logs("MSAFilter") {

  for (core::data::sequence::Sequence_SP s:msa)
    msa_.push_back(s);
}

void MSAFilter::calc_highly_gapped_positions() {
  core::index2 nhg = msa_.size() - nng_;  // --- N highly-gaped
  for (core::index2 it = 0; it < msa_[0]->length(); it++) {
    core::index2 tmp_nhg = 0;           // --- number of gaps at a given MSA column
    for (auto & s_it : msa_)
      if ((*s_it)[it] == '-') tmp_nhg++;

    if (tmp_nhg >= nhg) {
      logs << utils::LogLevel::FINE << "position " << it << " with " << tmp_nhg << " gaps marked as HGC\n";
      highly_gapped_positions_.push_back(it);
      std::vector<core::index2> tmp_sequences_at_gap_col;
      for (core::index2 s_it = 0; s_it < msa_.size(); s_it++)
        if ((*msa_[s_it])[it] != '-') tmp_sequences_at_gap_col.push_back(s_it);
      sequences_at_gapped_columns_[it] = tmp_sequences_at_gap_col;
    }
  }
  highly_gapped_for_sequence_.resize(msa_.size());
  for (const auto& it : sequences_at_gapped_columns_) {   // --- count how many times each sequence has AA in a highly gapped columns
    for (core::index2 seq : it.second)
      highly_gapped_for_sequence_[seq]++;
  }

  logs << utils::LogLevel::INFO << highly_gapped_positions_.size() << " highly gapped positions found\n";
}

}
}

