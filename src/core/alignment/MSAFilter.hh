#ifndef CORE_ALIGNMENT_MSAFilter_HH
#define CORE_ALIGNMENT_MSAFilter_HH

#include <string>

#include <core/index.hh>
#include <core/data/sequence/Sequence.hh>
#include <utils/Logger.hh>

namespace core {
namespace alignment {

/** @brief Base class for filtering sequences out of a MSA
 */
class MSAFilter {
public:
  /** @brief Create a new filter for a given set of aligned sequences (MSA)
   * @param msa - input set of aligned sequences (Multiple Sequence Alignment)
   */
  MSAFilter(const std::vector<core::data::sequence::Sequence_SP> & msa);

  virtual void run(core::index2 parameter) = 0;

  /** @brief Sets the value of N-non-gapped parameter.
   *
   * The  N-non-gapped parameter controls whether a column is recognized as <b>highly gapped</b> (HG).
   * When a number of non-gapped sequences  in a column is equal or lower than the N-non-gapped number,
   * the column will be marked as <b>highly gapped</b>. In the following example:
<code><pre>
AAAAA--A
AAAAAA-A
AAAAA--A
AAAAAAAA
</pre></code>
   * when <code>n_non_gapped</code> is set to 1, there is only 1 HG column (the second from the right).
   * Also, when <code>n_non_gapped=2</code>, there are 3 HG columns.
   * @param nng - the new value of N-non-gapped parameter
   */
  void n_non_gapped(core::index2 nng) {
    nng_ = nng;
    logs << utils::LogLevel::INFO << "N-non-gapped set to " << nng << "\n";
  }

  /** @brief Returns the value of N-non-gapped parameter.
   *
   * @see n_non_gapped(core::index2) method documentation
   * @return N-non-gapped parameter value
   */
  core::index2 n_non_gapped() { return nng_; }

  /** @brief Sets the value of N-non-gapped parameter assigning it as a fraction.
   *
   * This method assigns the new value of the N-non-gapped parameter as the fraction of all sequences in the input MSA
   * this filter has been created for.
   * @see n_non_gapped(core::index2) method documentation
   * @param f - fraction of all the sequences in that MSA
   */
  void f_non_gapped(double f) { n_non_gapped((core::index2) (f * msa_.size())); }

  /** @brief Provides const-access to the MSA stored in this object.
   * After calling <code>run()</code> method the MSA holds only the sequences which remained after the filtering protocol
   * @return const reference to the MSA
   */
  const std::vector<core::data::sequence::Sequence_SP> & msa() const { return msa_; }

  /** @brief Provides a vector of highly gapped positions (HGP) in this MSA.
   *
   * The HGPs are given as integer indexes (0-referenced) pointing to MSA columns; columns on the returned vector
   * have <b>at most</b> <code>n_non_gapped()</code> letters, all other characters are gaps.
   * @return a vector of highly gapped positions
   */
  const std::vector<core::index2> & highly_gapped_positions() { return highly_gapped_positions_; }

  /** @brief How many HGC each sequence participates in
   *
   * @return a vector of HPC counts for each sequence
   */
  const std::vector<core::index2> & highly_gapped_for_sequence() { return highly_gapped_for_sequence_; }

protected:
  core::index2 nng_;
  std::vector<core::index2> highly_gapped_positions_; ///< Vector of positions that are "highly gapped"
  std::map<core::index2,std::vector<core::index2>> sequences_at_gapped_columns_;  ///< A vector of sequences for every "highly gapped" position
  std::vector<core::data::sequence::Sequence_SP> msa_; ///< Sequences for the protocol
  std::vector<core::index2> highly_gapped_for_sequence_; ///< counts how many HGC were found for each sequence

  /// Builds highly_gapped_positions_ and sequences_at_gapped_columns_ data structures
  void calc_highly_gapped_positions();

private:
  utils::Logger logs;
};

}
}

#endif
