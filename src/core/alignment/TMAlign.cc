#include <cmath>
#include <vector>
#include <limits.h>

#include <core/index.hh>

#include <core/data/basic/Vec3.hh>
#include <core/alignment/NWAligner.hh>
#include <core/alignment/TMAlign.hh>
#include <core/alignment/PairwiseAlignment.hh>
#include <core/alignment/scoring/AlignmentTMScoring.hh>
#include <core/alignment/scoring/LocalStructure7.hh>
#include <core/alignment/scoring/LocalStructureMatch.hh>


namespace core {
namespace alignment {

TMAlign::TMAlign(const Coordinates & query, const Coordinates & tmplt)  :

    tmplt_to_align(tmplt), query_to_align(query),tms(query_,tmplt_), LOCAL_SEED_CUTOFF(20.0),
    EXTENDING_RADIUS(2.0), EXTENDING_ITERATIONS(5), MAX_NW_ITERATIONS(3), l("TMAlign"),
    seeds([&] (const Seed & x, const Seed & y) {return x.first>y.first;},
        [&] (const Seed & x, const Seed & y) {return x.first==y.first;},
        10,50,std::make_pair<double,Rototranslation_SP>(0.0,0)) {

  nw_iterations_cnt = 0;
  tmplt_.resize(tmplt_to_align.size());
  for(index2 i=0;i<tmplt_to_align.size();++i)
    tmplt_[i].set( tmplt_to_align[i]);
  query_.resize(query_to_align.size());
  for(index2 i=0;i<query_to_align.size();++i)
    query_[i].set( query_to_align[i]);

  recent_raw_tmscore_ = -1.0;
  recent_alignment_ = nullptr;
}


double TMAlign::init_from_fragments() {

  using namespace core::alignment::scoring;
  using namespace core::calc::structural::transformations;

  LocalStructure7 query_data(query_);
  LocalStructure7 tmplt_data(tmplt_);
  LocalStructureMatch<LocalStructure7, 8> lm(query_data,tmplt_data);
  double best_tms = 0;
  std::pair<double,Rototranslation_SP> dummy_pair = std::make_pair(0.0,nullptr);
  double tmscore = 0;
  Rototranslation best_rt;
  for (index2 iq = 6; iq < query_.size() - 3; ++iq) {
    for (index2 it = 6; it < tmplt_.size() - 3; ++it) {
      if (lm(iq - 3, it - 3) < LOCAL_SEED_CUTOFF) {
        if (!tms.define_seed(iq - 3, it - 3, 7)) continue;
        tms.best_tmscore = 0;
        tmscore = tms.extend(tmplt_.size(), EXTENDING_ITERATIONS, EXTENDING_RADIUS, true);
        dummy_pair.first = tmscore;
        if (seeds.push_might_work(dummy_pair)) {
          Rototranslation_SP r = std::make_shared<Rototranslation>();
          r->set(tms.best_rt);
          Seed s = std::make_pair(tmscore, r);
          seeds.push(s);
        }
        if (tmscore > best_tms) {
          best_tms = tmscore;
          best_rt.set(tms.best_rt);
          l << utils::LogLevel::FINEST<<"New seed found (tmscore, alignment path, diagonal length):\n"<<
              utils::to_string(tms)<<"\n";
        }
      }
    }
  }

  tms.set(best_rt);
  return best_tms;
}

double TMAlign::align() {

  auto start = std::chrono::high_resolution_clock::now();
  tms.reset_crmsd_calls_counter();
  double tm_value = init_from_fragments();
  double best_tm_value = 0;
  double last_tm_value = 0;
  Rototranslation best_rt;
  PairwiseAlignment_SP best_alignment;
  for (auto seed_it = seeds.cbegin(); seed_it != seeds.cend(); ++seed_it) {
    tms.set(*(*seed_it).second);
    nw_iterations_cnt = 0;
    do {
      last_tm_value = tm_value;
      tm_value = nw_iteration();
      if (tm_value > best_tm_value) {
        best_rt.set(tms);
        best_tm_value = tm_value;
        best_alignment = recent_alignment_;
        recent_raw_tmscore_ = tms.recent_tmscore(1.0);
      }
    } while ((fabs(last_tm_value - tm_value) > 0.01) && (nw_iterations_cnt < MAX_NW_ITERATIONS));
  }
  auto end = std::chrono::high_resolution_clock::now();
  l << utils::LogLevel::FINE << "best tm-score found: " << best_tm_value << " "<<best_alignment->to_path()<<"\n";
  l <<  "running time " << (double) (std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()) << " [ms]\n";
  l << utils::LogLevel::FINER << "crmsd calculations called: " << size_t(tms.crmsd_calls_counter()) << "\n";

  tms.set(best_rt);
  recent_alignment_ = best_alignment;

  return recent_raw_tmscore_/double(tmplt_to_align.size());
}

double TMAlign::nw_iteration() {

  NWAligner<float,scoring::AlignmentTMScoring> nw_aligner(std::max(tmplt_to_align.size(), query_to_align.size()));
  scoring::AlignmentTMScoring score(query_to_align,tms,tmplt_to_align);

  nw_aligner.align(-0.6,0.0,score);
  recent_alignment_ = nw_aligner.backtrace();
  Vec3 gap;
  tmplt_.clear();
  query_.clear();
  recent_alignment_->get_aligned_query_template(query_to_align,tmplt_to_align,gap,query_,tmplt_);
  tms.tmscore(tmplt_to_align.size());
  ++nw_iterations_cnt;
  l << utils::LogLevel::FINER << "TMScore and alignment after "<<nw_iterations_cnt<<" NW iteration(s):\n"<<tms.recent_tmscore(tmplt_to_align.size())<<" "<<recent_alignment_->to_path()<<"\n";

  return tms.recent_tmscore(tmplt_to_align.size());
}

}
}
