#include <core/alignment/CoordinatesSet.hh>

namespace core {
namespace alignment {

CoordinatesSet::CoordinatesSet(const core::data::structural::Structure_SP s, const std::string &name,
    const core::data::structural::selectors::AtomSelector &select_superimposed) :
    name(name), structure(s), log("CoordinatesSet") {

  for (auto ires = s->first_residue(); ires != s->last_residue(); ++ires) {
    for (auto iatom : (**ires)) {
      if (select_superimposed(iatom)) {
        coordinates_.emplace_back(0, 0, 0);
        coordinates_.back().set(*iatom);
        if (residues().size() == 0)
          residues_.push_back(*ires);
        else {
          if (residues_.back() != *ires) residues_.push_back(*ires);
        }
      }
    }
  }
}

const core::data::sequence::Sequence_SP CoordinatesSet::create_superimposed_sequence() const {

  std::string seq;
  for (auto ires : residues_) {
    seq += ires->residue_type().code1;
  }

  return std::make_shared<core::data::sequence::Sequence>(name, seq, 1);
}

}
}

