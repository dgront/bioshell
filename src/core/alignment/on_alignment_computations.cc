#include <cmath>
#include <string>

#include <core/index.hh>
#include <core/alignment/on_alignment_computations.hh>

namespace core {
namespace alignment {

static utils::Logger on_alignment_computations_logger("on_alignment_computations");

double hamming_distance(const std::string &a, const std::string &b) {
  int disti = 0;
  size_t n = a.size();
  for (int i = 0; i < n; i++)
    disti += (a[i] != b[i]);

  return disti / double(n);
}

core::index2 sum_identical(const std::string &s1, const std::string &s2) {

  core::index2 match = 0;
  for (size_t i = 0; i < std::min(s1.size(), s2.size()); ++i)
    if ((s1[i] == s2[i]) && (s1[i] != '-') && (s1[i] != '_')) ++match;

  return match;
}

core::index2 sum_identical(const Sequence &s1, const Sequence &s2) { return sum_identical(s1.sequence, s2.sequence); }

core::index2 sum_identical(const PairwiseAlignment &alignment, const std::string &query_sequence,
                           const std::string &tmplate_sequence) {

  const std::string tq = alignment.get_aligned_query(query_sequence, '-');
  const std::string tt = alignment.get_aligned_template(tmplate_sequence, '-');

  return sum_identical(tq,tt);
}

core::index2 sum_identical(const PairwiseSequenceAlignment &alignment) {

  return sum_identical(*alignment.alignment, alignment.query_sequence->sequence, alignment.template_sequence->sequence);
}

double semiglobal_identity_ratio(const PairwiseAlignment &alignment, const std::string &query_sequence,
                           const std::string &tmplate_sequence) {

  const std::string tq = alignment.get_aligned_query(query_sequence, '-');
  const std::string tt = alignment.get_aligned_template(tmplate_sequence, '-');

  core::index4 n_id = sum_identical(tq,tt);
  core::index4 ali_len = alignment.length() - alignment.n_front_gaps() - alignment.n_end_gaps();

  return n_id / double(std::max(ali_len, core::index4(std::min(query_sequence.size(), tmplate_sequence.size()))));
}

double semiglobal_identity_ratio(const std::string &aligned_query_sequence,
                                 const std::string &aligned_tmplate_sequence) {

  core::index4 n_id = sum_identical(aligned_query_sequence, aligned_tmplate_sequence);
  core::index4 ali_len =
      aligned_query_sequence.length() - count_front_gaps(aligned_query_sequence, aligned_tmplate_sequence) -
      count_end_gaps(aligned_query_sequence, aligned_tmplate_sequence);
  core::index4 seq_len = std::min(core::data::sequence::length_without_gaps(aligned_query_sequence),
                                  core::data::sequence::length_without_gaps(aligned_tmplate_sequence));

  return n_id / double(std::max(ali_len, seq_len));
}

bool is_gap(const char c) { return (c == '-' || c == '_'); }

core::index2 count_front_gaps(const std::string &s1, const std::string &s2) {

  core::index2 i = 0;
  core::index2 n_gaps = 0;
  while (!(!is_gap(s1[i]) && !is_gap(s2[i]))) {
    ++n_gaps;
    ++i;
  }

  return n_gaps;
}

core::index2 count_end_gaps(const std::string &s1, const std::string &s2) {

  core::index2 i = s2.length() - 1;
  core::index2 n_gaps = 0;
  while (!(!is_gap(s1[i]) && !is_gap(s2[i]))) {
    ++n_gaps;
    --i;
  }

  return n_gaps;
}

core::index2 count_gaps(const std::string &s1, const std::string &s2, bool const include_terminal_gaps) {

  core::index2 n_gaps = 0;
  std::string::const_iterator it1 = s1.begin();
  std::string::const_iterator it2 = s2.begin();
  while (it1 != s1.end()) {
    if (is_gap(*it1) || (is_gap(*it2))) ++n_gaps;
    ++it1;
    ++it2;
  }
  if (include_terminal_gaps) return n_gaps;

  core::index2 i = 0;
  while (!(!is_gap(s1[i]) && !is_gap(s2[i]))) {
    --n_gaps;
    ++i;
  }
  i = s2.length() - 1;
  while (!(!is_gap(s1[i]) && !is_gap(s2[i]))) {
    --n_gaps;
    --i;
  }
  return n_gaps;
}

core::index2 count_gaps(const Sequence &s1, const Sequence &s2, bool const include_terminal_gaps) {
  return count_gaps(s1.sequence, s2.sequence, include_terminal_gaps);
}

core::index2 count_gaps(const PairwiseAlignment &alignment, const std::string &query_sequence,
                        const std::string &tmplate_sequence, bool const include_terminal_gaps) {
  const std::string tq = alignment.get_aligned_query(query_sequence, '-');
  const std::string tt = alignment.get_aligned_template(tmplate_sequence, '-');

  return count_gaps(tq, tt, include_terminal_gaps);
}

core::index2 count_gaps(const PairwiseSequenceAlignment &alignment, bool const include_terminal_gaps) {

  return count_gaps(*alignment.alignment, alignment.query_sequence->sequence, alignment.template_sequence->sequence,
    include_terminal_gaps);
}

core::index2 sum_aligned(const std::string &s1, const std::string &s2) {

  core::index2 match = 0;
  std::string::const_iterator it1 = s1.begin();
  std::string::const_iterator it2 = s2.begin();
  while ((it1 != s1.end()) && (it2 != s2.end())) {
    if ((*it1 != '-') && (*it1 != '_') && (*it2 != '-') && (*it2 != '_')) ++match;
    ++it1;
    ++it2;
  }

  return match;
}

core::index2 sum_aligned(const Sequence &s1, const Sequence &s2) { return sum_aligned(s1.sequence, s2.sequence); }

core::index2 sum_aligned(const PairwiseAlignment &alignment) {

  std::string p = alignment.to_path();
  return std::count(p.begin(), p.end(), '*');
}

core::index2 compute_aln_n(const PairwiseAlignment &reference_alignment, const core::index2 k,
                           const PairwiseAlignment &another_alignment) {

  if (another_alignment.query_length() != reference_alignment.query_length()) {
    on_alignment_computations_logger << utils::LogLevel::WARNING
                                     << "Cannot compare alignment to the reference because the query sequences differ in length\n";
    return 0;
  }

  size_t sum = 0;
  for (size_t i = 0; i < reference_alignment.query_length(); i++) {
    const int i_ref = reference_alignment.which_template_for_query(i);
    const int i_other = another_alignment.which_template_for_query(i);
    if ((i_ref < 0) || (i_other < 0))
      continue;
    if (abs(i_ref - i_other) <= k)
      ++sum;
  }

  return sum;
}


short int calculate_score(const core::data::sequence::Sequence &s1, const core::data::sequence::Sequence &s2,
                             const scoring::NcbiSimilarityMatrix & scoring, short int gap_open, short int gap_extend) {
  return calculate_score(s1.sequence, s2.sequence, scoring, gap_open, gap_extend);
}

short int calculate_score(const std::string &s1, const std::string &s2,
                             const scoring::NcbiSimilarityMatrix & scoring, short int gap_open, short int gap_extend) {

  short int score = 0;
  bool is_gap_open = false;
  for (core::index4 ipos = 0; ipos < std::min(s1.length(), s2.length()); ++ipos) {
    if ((s1[ipos] == '-') || (s1[ipos] == '_') || (s2[ipos] == '-') || (s2[ipos] == '_')) {
      if (is_gap_open) score += gap_extend;
      else {
        score += gap_open;
        is_gap_open = true;
      }
    } else {
      is_gap_open = false;
      score += scoring(scoring.symbol_to_index(s1[ipos]), scoring.symbol_to_index(s2[ipos]));
    }
  }

  return score;
}


}
}
