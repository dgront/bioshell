#ifndef CORE_ALIGNMENT_SWAligner_H
#define CORE_ALIGNMENT_SWAligner_H

#include <cmath>
#include <vector>
#include <iomanip>
#include <limits.h>

#include <core/index.hh>

#include <core/data/basic/Array2D.hh>
#include <core/alignment/AbstractAligner.hh>
#include <core/alignment/PairwiseAlignment.hh>

namespace core {
namespace alignment {

/** \brief Computes global alignment of two sequences with affine gap penalty.
 *
 * \include ap_SWAligner.cc
 *
 * @tparam T  - primitive data type used to represent alignment scores, e.g. <code>float</code> or <code>short</code>
 * @tparam Op - call operator used to calculate a score for a match between two positions
 */
template<typename T, typename Op>
class SWAligner : public AbstractAligner<T, Op> {
public:
  /** @brief Create an aligner object that will calculate a local pairwise sequence alignments.
   *
   * The longest sequence must be at most <code>max_size</code> residues long.
   * @param max_size - the length of the longest aligned sequence.
   */
  SWAligner(const unsigned  int max_size) : max_size(max_size), H(max_size+1), E(max_size+1), F(max_size+1) , H_prev(max_size+1), E_prev(max_size+1), F_prev(max_size+1) {
    query_size = tmplt_size = 0;
  }

  /// Default virtual destructor
  virtual ~SWAligner() = default;

  /** @brief Calculates global sequence alignment using Needleman-Wunsch algorithm.
   *
   * This is a very general method. The whole knowledge about the target and query proteins is encoded by the scoring operator type.
   * Once this call is finished, the resulting alignment may be obtained by calling backtrace() method.
   * @param gap_open - penalty for opening a new gap; must be negative.
   * @param gap_extend - penalty for extending an existing gap; must be negative.
   * @param scoring - instance of a class that provides three methods:
   *     - tmplt_length() - the number of positions in the template sequence
   *     - query_length() - the number of positions in the query sequence
   *     - call operator operating on two integer indexes, which provides a score for matching the two respective positions
   *     See \ref doc/web_examples/ex_SemiglobalAligner.cc "doc/web_examples/ex_SemiglobalAligner.cc"  demo program for an example similarity scoring function
   * @tparam T - type used to represents scores
   * @tparam Op - scoring system class
   * @return alignment score.
   */
  T align(const T gap_open,const  T gap_extend,const Op & scoring);

  /** @brief Calculates the score of the global optimal sequence alignment using Needleman-Wunsch algorithm.
   *
   * This method is very similar to align(const T,const  T,const Op &), but it <strong>does not store</strong>
   * information for backtracing; alignment will not be available. This method however is much faster that
   * align(const T,const  T,const Op &).
   */
  T align_for_score(const T gap_open,const  T gap_extend,const Op & scoring);

  /** @brief Backtraces the alignment based on information sored by the most recent align(const T,const  T,const Op &) call.
   *
   */
  PairwiseAlignment_SP backtrace();

  /// Returns the score found by the most recent call of align(const T,const  T,const Op &) or align_for_score(const T,const  T,const Op &)
  inline T recent_score() const { return recent_score_; }

  /// Read-only access to the matrix of alignment arrows
  const std::shared_ptr<core::data::basic::Array2D<unsigned char>> arrows() const { return arrows_; }

private:
  T max_score_ = 0;
  index2 max_i = 0, max_j = 0;
  T recent_score_ = 0;
  const unsigned  int max_size;
  index2 query_size;
  index2 tmplt_size;
  std::vector<T> H; ///< The alignment matrix - the row being calculated
  std::vector<T> E; ///< Gotoh's helper matrix for the alignments ending with a gap in the query - the row being calculated (horizontal move)
  std::vector<T> F; ///< Gotoh's helper matrix for the alignments ending with a gap in the template - the row being calculated (vertical move)
  std::vector<T> H_prev; ///< The alignment matrix - the very previous row
  std::vector<T> E_prev; ///< Gotoh's helper matrix for the alignments ending with a gap in the query - the very previous row
  std::vector<T> F_prev; ///< Gotoh's helper matrix for the alignments ending with a gap in the template - the very previous row
  std::shared_ptr<core::data::basic::Array2D<unsigned char>> arrows_ = nullptr;
  std::shared_ptr<core::data::basic::Array2D<unsigned char>> E_arrows_ = nullptr;
  std::shared_ptr<core::data::basic::Array2D<unsigned char>> F_arrows_ = nullptr;
  std::vector<char> back_trace;
  static const T not_an_option;
};

template<typename T, typename Op>
const T SWAligner<T,Op>::not_an_option = -std::numeric_limits<T>::max() /2;

template<typename T, typename Op>
T SWAligner<T,Op>::align(const T gap_open,const  T gap_extend,const Op & scoring) {

  query_size = scoring.query_length();
  tmplt_size = scoring.tmplt_length();

  if(arrows_ == nullptr) {
    arrows_ = std::make_shared<core::data::basic::Array2D<unsigned char>>(max_size + 1, max_size + 1);
    E_arrows_ = std::make_shared<core::data::basic::Array2D<unsigned char>>(max_size + 1, max_size + 1);
    F_arrows_ = std::make_shared<core::data::basic::Array2D<unsigned char>>(max_size + 1, max_size + 1);
  }
  arrows_->clear(8); // STOP symbol everywhere
  E_arrows_->clear(8); // STOP symbol everywhere
  F_arrows_->clear(8); // STOP symbol everywhere

  // ---------- Initialize for tail gaps penalized
  H[0] = 0;
  max_score_ = recent_score_ = not_an_option;
  E[0] = F[0] = not_an_option;
  for (index2 j = 1; j <= tmplt_size; ++j) {
    F[j] = not_an_option;
    H[j] = E[j] = 0;
    arrows_->set(0, j, 8); // STOP
  }
  for (index2 i = 1; i <= query_size; ++i) {
    H.swap(H_prev);
    E.swap(E_prev);
    F.swap(F_prev);
    arrows_->set(i, 0, 8); // STOP
    H[0] = F[0] = 0;
    E[0] = not_an_option;
    for (index2 j = 1; j <= tmplt_size; ++j) {
      E[j] = std::max(E[j - 1] + gap_extend, std::max(H[j - 1] + gap_open, F[j - 1] + gap_open));
      if (E[j] - E[j - 1] == gap_extend)
        E_arrows_->set(i, j, E_arrows_->get(i, j - 1)+1);
      else
        E_arrows_->set(i,j,1);

      F[j] = std::max(F_prev[j] + gap_extend, std::max(H_prev[j] + gap_open, E_prev[j] + gap_open));
      if (F[j] - F_prev[j] == gap_extend)
        F_arrows_->set(i, j, F_arrows_->get(i-1, j)+1);
      else
        F_arrows_->set(i,j,1);

      H[j] = std::max(T(H_prev[j - 1] + (scoring)(i - 1, j - 1)), std::max(T(0), std::max(E[j], F[j])));
      if (H[j] == 0) {
        arrows_->set(i, j, 8); // STOP alignment
        continue;
      }
      if (max_score_ < H[j]) {
        max_score_ = H[j];
        max_i = i;
        max_j = j;
      }
      const T h = T(H_prev[j-1]+(scoring)(i-1,j-1));
      index1 arrow_flag = 0;
      if (H[j] == E[j]) arrow_flag += 1;
      if (H[j] == h) arrow_flag += 2;
      if (H[j] == F[j]) arrow_flag += 4;
      arrows_->set(i, j, arrow_flag);
    }
  }

  recent_score_ = max_score_;

  return recent_score_;
}

template<typename T, typename Op>
T SWAligner<T,Op>::align_for_score(const T gap_open,const  T gap_extend,const Op & scoring) {

  query_size = scoring.query_length();
  tmplt_size = scoring.tmplt_length();
  // ---------- Initialize for tail gaps penalized
  H[0] = 0;
  E[0] = F[0] = not_an_option;
  for(index2 j=1;j<=scoring.tmplt_length();++j) {
    F[j] =  not_an_option;
    H[j] = E[j] = 0;
  }
  T tmp_max = not_an_option;
  for (index2 i = 1; i <= scoring.query_length(); ++i) {
    H.swap(H_prev);
    E.swap(E_prev);
    F.swap(F_prev);
    H[0] = F[0] = 0;
    E[0] = not_an_option;
    for(index2 j=1;j<=scoring.tmplt_length();++j) {
      E[j] = std::max(E[j - 1] + gap_extend, std::max(H[j - 1] + gap_open, F[j - 1] + gap_open));
      F[j] = std::max(F_prev[j] + gap_extend, std::max(H_prev[j] + gap_open, E_prev[j] + gap_open));
      H[j] = std::max(T(H_prev[j - 1] + (scoring)(i - 1, j - 1)), std::max(T(0), std::max(E[j], F[j])));
      if(tmp_max < H[j]) { // --- record maximum score value and its position
        tmp_max = H[j];
        max_i = i;
        max_j = j;
      }
    }
  }

  recent_score_ = tmp_max;

  return tmp_max;
}

template<typename T, typename Op>
PairwiseAlignment_SP SWAligner<T,Op>::backtrace() {

  if (back_trace.size() == 0) back_trace.resize(max_size + max_size);

  index2 i = max_i, j = max_j; // --- to backtrack the alignment, we start from its end and go towards its beginning
  int k = -1;

//  std::cerr <<int(arrows->operator()(i,j))<< " "<<i<<" "<<j<<" "<<k<<" "<<int(back_trace[k])<<"\n";
  while (i > 0 && j > 0 && arrows_->operator()(i, j) != 8) {
    if((arrows_->operator()(i, j) & 2) != 0) {
      back_trace[++k] = 2;
//      std::cerr << int(arrows->operator()(i, j)) << " " << i << " " << j << " " << k << " " << int(back_trace[k]) << "\n";
      i--;
      j--;
      continue;
    }
    if((arrows_->operator()(i, j) & 1) != 0) {
      back_trace[++k] = 1;
      j--;
      continue;
    }
    back_trace[++k] = 4;
    i--;
  }
  // --- i and j variables now hold the coordinates of the beginning of the alignment i.e. its starting point on the
  // --- i.e. its starting point on the alignment matrix
  PairwiseAlignment_SP a = std::make_shared<PairwiseAlignment>(i, j, double(recent_score_));
  while(k>=0) {
    if(back_trace[k]==2) {
      a->append_match();
      --k;
      continue;
    }
    if(back_trace[k]==1) {
      a->append_query_gap();
      --k;
      continue;
    }
    if(back_trace[k]==4) {
      a->append_template_gap();
      --k;
      continue;
    }
  }

  return a;
}

}
}
/**
 * \example ap_SWAligner.cc
 */
#endif

