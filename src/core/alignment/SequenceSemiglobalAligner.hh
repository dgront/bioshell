#ifndef CORE_ALIGNMENT_SequenceSemiglobalAligner_H
#define CORE_ALIGNMENT_SequenceSemiglobalAligner_H

#include <core/alignment/SemiglobalAligner.hh>
#include <core/alignment/PairwiseSequenceAlignment.fwd.hh>
#include <core/alignment/scoring/SimilarityMatrixScore.hh>
#include <core/alignment/scoring/NcbiSimilarityMatrixFactory.hh>
#include <core/data/sequence/Sequence.hh>

namespace core {
namespace alignment {

/** @brief Semiglobal variant of Needleman–Wunsch alignment for amino acid sequences.
 *
 * This class is just a concrete derivative of SemiglobalAligner template with SimilarityMatrixScore scoring.
 * It has been provided so creating a sequence aligner does not require any use of templates,
 * e.g. it can be used from PyBioShell scripts
 */
class SequenceSemiglobalAligner : public SemiglobalAligner<short, scoring::SimilarityMatrixScore<short>> {
public:
  explicit SequenceSemiglobalAligner(const unsigned  int max_size) :
      SemiglobalAligner<short, scoring::SimilarityMatrixScore<short>>(max_size) {}

  short align(const core::data::sequence::Sequence_SP query, const core::data::sequence::Sequence_SP tmplt,
              const short gap_open, const short gap_extend, const std::string &matrix_name);

  short align(const std::string &query, const std::string &tmplt,
              const short gap_open, const short gap_extend, const std::string &matrix_name);

  /** @brief Calculates the score of the global optimal sequence alignment using Needleman-Wunsch algorithm.
   *
   * This method is very similar to align(const T,const  T,const Op &), but it <strong>does not store</strong>
   * information for backtracing; alignment will not be available. This method however is much faster that
   * align().
   */
  short align_for_score(const core::data::sequence::Sequence_SP query, const core::data::sequence::Sequence_SP tmplt,
                        const short gap_open, const short gap_extend, const std::string &matrix_name);

  short align_for_score(const std::string &query, const std::string &tmplt,
                        const short gap_open, const short gap_extend, const std::string &matrix_name);

  /** @brief Backtraces the alignment based on information sored by the most recent align() call.
   * @return a pointer to a PairwiseSequenceAlignment object
   */
  PairwiseSequenceAlignment_SP backtrace_sequence_alignment();

private:
  core::data::sequence::Sequence_SP recent_query = nullptr;
  core::data::sequence::Sequence_SP recent_tmplt = nullptr;

  void create_sequences(const std::string &query, const std::string &tmplt);

  // ---------- These three statements below are to properly hide methods inherited from the base class
  using SemiglobalAligner<short, scoring::SimilarityMatrixScore<short>>::align;
  using SemiglobalAligner<short, scoring::SimilarityMatrixScore<short>>::align_for_score;
  using SemiglobalAligner<short, scoring::SimilarityMatrixScore<short>>::backtrace;
};

}
}

#endif

