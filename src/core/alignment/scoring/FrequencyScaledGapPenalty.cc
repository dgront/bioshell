#include <core/alignment/scoring/FrequencyScaledGapPenalty.hh>

namespace core {
namespace alignment {
namespace scoring {

FrequencyScaledGapPenalty::FrequencyScaledGapPenalty(const float gap_open, const float gap_extend,
      const std::vector<float> & query_open_profile, const std::vector<float> & query_extend_profile,
      const std::vector<float> & template_open_profile, const std::vector<float> & template_extend_profile):
            gap_open_(gap_open), gap_extend_(gap_extend),
            template_open_profile_(template_open_profile), template_extend_profile_(template_extend_profile),
            query_open_profile_(query_open_profile), query_extend_profile_(query_extend_profile) {

  template_partial_sum_.resize(template_extend_profile_.size());
  template_partial_sum_[0] = 1.0 - template_extend_profile_[0];
  for (size_t i = 1; i < template_extend_profile_.size(); ++i)
    template_partial_sum_[i] = (1.0 - template_extend_profile_[i]) + template_partial_sum_[i - 1];

  query_partial_sum_.resize(query_extend_profile.size());
  query_partial_sum_[0] = 1.0 - query_extend_profile_[0];
  for (size_t i = 1; i < query_extend_profile_.size(); ++i)
    query_partial_sum_[i] = (1.0 - query_extend_profile_[i]) + query_partial_sum_[i - 1];
}

float FrequencyScaledGapPenalty::query_gap_cost(index2 last_gapped_pos_template, const index2 gap_length) const {

  if(last_gapped_pos_template==0) return query_open_profile_[0] * gap_open_;

  return (template_partial_sum_[last_gapped_pos_template] -
          template_partial_sum_[last_gapped_pos_template + 1 - gap_length]) * gap_extend_ +
          template_open_profile_[last_gapped_pos_template + 1 - gap_length] * gap_open_;
}


float FrequencyScaledGapPenalty::template_gap_cost(index2 last_gapped_pos_query, const index2 gap_length) const {

  if(last_gapped_pos_query==0) return query_open_profile_[0] * gap_open_;

  return (query_partial_sum_[last_gapped_pos_query] -
          query_partial_sum_[last_gapped_pos_query + 1 - gap_length]) * gap_extend_ +
         query_open_profile_[last_gapped_pos_query + 1 - gap_length] * gap_open_;
}

}
}
}

