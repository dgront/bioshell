#ifndef CORE_ALIGNMENT_SCORING_NcbiSimilarityMatrixFactory_H
#define CORE_ALIGNMENT_SCORING_NcbiSimilarityMatrixFactory_H

#include <memory>
#include <iostream>
#include <string>
#include <sstream>
#include <map>

#include <core/alignment/scoring/SimilarityMatrix.hh>

#include <utils/io_utils.hh>

namespace core {
namespace alignment {
namespace scoring {

/** @brief a container for substitution matrices used in sequence alignment.
 */
class NcbiSimilarityMatrixFactory {
public:

  /** @brief Static method to access the factory singleton.
   * @return a singleton instance of a factory that provides NcbiSimilarityMatrix objects
   */
  static NcbiSimilarityMatrixFactory & get() {
    static NcbiSimilarityMatrixFactory manager;
    return manager;
  }

  /** @brief Returns a substitution matrix.
   *
   * If the name does not refer to any matrix known to this factory, a null pointer is returned.
   * @param matrix_name - identifies a matrix
   */
  const NcbiSimilarityMatrix_SP get_matrix(const std::string & matrix_name) const;

  /** @brief Creates a similarity (substitution) matrix either from data stored in a BioShell distribution or from a file.
   *
   *  If the given string is already registered as a matrix name, the relevant matrix is returned. Otherwise,
   * this method first attempts to create the matrix based on internal data stored in a BioShell distribution.
   * If such a file cannot be found, it attempts to read a local file.
   *
   * @param file_or_matrix_name - either a name of a matrix to be made or a file name to be loaded.
   */
  const NcbiSimilarityMatrix_SP load_matrix(const std::string & file_or_matrix_name);

  /** @brief Says what matrices are registered at this factory.
   * @param names - a vector of strings where the names of registered matrices will be stored. The vector is not cleared beforehand.
   */
  void matrix_names(std::vector<std::string> & names) const;

private:

  static std::map<std::string, NcbiSimilarityMatrix_SP> matrices;

  NcbiSimilarityMatrixFactory() { load_matrices(); }

  void load_matrices(void);
};

}
}
}

#endif

