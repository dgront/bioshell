#ifndef CORE_ALIGNMENT_SCORING_CombinedScore_HH
#define CORE_ALIGNMENT_SCORING_CombinedScore_HH

#include <cmath>
#include <string>
#include <vector>
#include <memory>

#include <core/index.hh>

#include <core/alignment/scoring/AlignmentScoreBase.hh>

namespace core {
namespace alignment {
namespace scoring {

/** @brief Alignment score that is a linear combination of scores
 *
 * @tparam T - data type used to represent a score value
 */
template<typename T>
class CombinedScore : public AlignmentScoreBase<T> {
public:

  /** \brief Calculates a linear combination of dynamic-programming scores.
   * @param i - position index in the query sequence
   * @param j - position index in the template sequence
   * @returns the score
   */
  virtual T operator()(const index2 i,const index2 j) const;

  /// Returns the length of the query sequence
  inline index2 query_length() const { return terms[0]->query_length(); }
  /// Returns the length of the template sequence
  inline index2 tmplt_length() const { return terms[0]->tmplt_length(); }

  /** \brief Adds a new score component to the combination
   * @param scoring_term - scoring object derived from AlignmentScoreBase class
   * @param weight - real weight
   * @returns the score
   */
  inline void add_score(const std::shared_ptr<AlignmentScoreBase<T>> scoring_term, const double weight) {

    weights.push_back(weight);
    terms.push_back(scoring_term);
  }

private:
  std::vector<std::shared_ptr<AlignmentScoreBase<T>>> terms;
  std::vector<double> weights;
};

template <typename T>
inline T CombinedScore<T>::operator()(const index2 i,const index2 j) const {

  double s1 = 0;
  int k=-1;
  for(const std::shared_ptr<AlignmentScoreBase<T>> si : terms)
    s1 += si->operator()(i, j) * weights[++k];

  return s1;
}

}
}
}

#endif

