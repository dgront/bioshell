#ifndef CORE_ALIGNMENT_SCORING_AlignmentConstraints_H
#define CORE_ALIGNMENT_SCORING_AlignmentConstraints_H

#include <memory>
#include <map>

#include <core/index.hh>

#include <core/data/basic/SparseMap2D.hh>
#include <core/alignment/scoring/AlignmentScoreBase.hh>

namespace core {
namespace alignment {
namespace scoring {

/** @brief Alignment constraint simply adds a very high score when two selected positions are aligned with one another
 *
 * @tparam T - data type of the score
 */
template<typename T>
class AlignmentConstraints : public AlignmentScoreBase<T> {
public:

  /** \brief Calculates a linear combination of dynamic-programming scores.
   * @param i - position index in the query sequence
   * @param j - position index in the template sequence
   * @returns the score
   */
  virtual T operator()(const index2 i,const index2 j) const;

  /// Returns the highest index of a residue restrained in the query sequence
  inline index2 query_length() const { return max_q; }

  /// Returns the highest index of a residue restrained in the template sequence
  inline index2 tmplt_length() const { return max_t; }

  /** \brief Adds a new alignment constraint
   * @param q_pos - index of a residue restrained in the a query sequence (starts from 0)
   * @param t_pos - index of a residue restrained in the a template sequence (starts from 0)
   * @param constraint_score - score value assigned when these two residues are aligned (should be large enough to ensure they will be aligned)
   */
  inline void add_constraints(const index2 q_pos, const index2 t_pos, const T constraint_score = 100000) {

    constr.insert(q_pos,t_pos,constraint_score);
    max_t = std::max(max_t,t_pos);
    max_q = std::max(max_q,q_pos);
  }

private:
  core::data::basic::SparseMap2D<core::index2,T> constr;
  core::index2 max_t = 0;
  core::index2 max_q = 0;
};

template <typename T>
T AlignmentConstraints<T>::operator()(const index2 i,const index2 j) const {

  double s1 = 0;
  if(constr.has_element(i,j)) return constr.at(i,j);
  else return 0;
}

}
}
}

#endif

