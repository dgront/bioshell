#include <core/BioShellEnvironment.hh>
#include <core/alignment/scoring/NcbiSimilarityMatrixFactory.hh>

#include <utils/Logger.hh>

namespace core {
namespace alignment {
namespace scoring {

utils::Logger ncbi_logger("NcbiSimilarityMatrixFactory");

std::map<std::string,NcbiSimilarityMatrix_SP> NcbiSimilarityMatrixFactory::matrices;


void NcbiSimilarityMatrixFactory::load_matrices() {

  std::vector<std::string> fnames = utils::glob(utils::join_paths(core::BioShellEnvironment::bioshell_db_path(),"alignments/*"));
  for(const std::string & fn:fnames) load_matrix(fn);
  ncbi_logger << utils::LogLevel::FILE << matrices.size()<<" matrices found";
}

const NcbiSimilarityMatrix_SP NcbiSimilarityMatrixFactory::load_matrix(const std::string & file_or_matrix_name) {

  if (matrices.find(file_or_matrix_name) != matrices.end())
    return matrices[file_or_matrix_name];
  auto m = SimilarityMatrix<short int>::from_ncbi_file(file_or_matrix_name);
  auto name_ext = utils::root_extension(utils::basename(file_or_matrix_name));
  matrices[name_ext.first] = m;
  return m;
}

void NcbiSimilarityMatrixFactory::matrix_names(std::vector<std::string> & names) const {

  for (auto const& element : matrices) names.push_back(element.first);
}

const NcbiSimilarityMatrix_SP NcbiSimilarityMatrixFactory::get_matrix(const std::string & matrix_name) const {

  if (matrices.find(matrix_name) != matrices.end()) {
    ncbi_logger << utils::LogLevel::INFO<<"Returning "<<matrix_name<<" matrix\n";
    return matrices[matrix_name];
  }
  ncbi_logger << utils::LogLevel::CRITICAL << "Can't find substitution matrix for a given name '" << matrix_name
              << "'\nKnown matrices: ";
  for (auto const &element : matrices) ncbi_logger << element.first << " ";
  ncbi_logger << "\n";

  return nullptr;
}


}
}
}
