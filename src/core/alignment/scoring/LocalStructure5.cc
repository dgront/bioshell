#include <vector>
#include <memory>
#include <iostream>

#include <core/index.hh>
#include <core/data/basic/Vec3.hh>
#include <core/alignment/scoring/LocalStructure5.hh>

#include <utils/string_utils.hh>

namespace core {
namespace alignment {
namespace scoring {

LocalStructure5::LocalStructure5(const data::basic::Coordinates_SP coordinates) : LocalStructure5(*coordinates) {}

LocalStructure5::LocalStructure5(const data::basic::Coordinates & coordinates) {

  data.resize(coordinates.size() * 4);
  // ---------- local distances for the coordinates
  fill_distances(coordinates,-2,2,data,0); // ---------- r15(-2,2)
  fill_distances(coordinates,-2,1,data,1); // ---------- r14(-2,1)
  fill_distances(coordinates,-1,2,data,2); // ---------- r14(-1,2)
  fill_distances(coordinates,-1,1,data,3); // ---------- r13(-1,1)
}

void LocalStructure5::fill_distances(const data::basic::Coordinates_SP xyz, const short from, const short to,
                                     std::vector<float> &where_to, const index1 offset) {

  for (index4 i = -from; i < xyz->size() - to; ++i)
    where_to[i * 4 + offset] = xyz->operator [](i + from).distance_to(xyz->operator [](i + to));
}

void LocalStructure5::fill_distances(const data::basic::Coordinates &xyz, const short from, const short to,
                                     std::vector<float> &where_to, const index1 offset) {

  for (index4 i = -from; i < xyz.size() - to; ++i)
    where_to[i * 4 + offset] = xyz.operator[](i + from).distance_to(xyz.operator[](i + to));
}

}
}
}

