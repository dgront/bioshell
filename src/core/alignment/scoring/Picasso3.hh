#ifndef CORE_ALIGNMENT_SCORING_Picasso3_H
#define CORE_ALIGNMENT_SCORING_Picasso3_H

#include <cmath>
#include <string>
#include <vector>
#include <map>

#include <core/index.hh>
#include <core/data/sequence/SequenceProfile.hh>
#include <core/alignment/scoring/AlignmentScoreBase.hh>

#include <utils/Logger.hh>

namespace core {
namespace alignment {
namespace scoring {

using core::data::sequence::SequenceProfile_SP;

/** @brief Scores similarity between positions in sequence profiles using Picasso3 formula
 *
 */
class Picasso3 : public AlignmentScoreBase<float> {
public:
  Picasso3(const SequenceProfile_SP query, const SequenceProfile_SP tmplt);

  float operator()(const index2 i,const index2 j) const;

  /// Returns the length of the query sequence
  inline index2 query_length() const { return query_->length(); }
  /// Returns the length of the template sequence
  inline index2 tmplt_length() const { return tmplt_->length(); }

private:
  SequenceProfile_SP query_;
  SequenceProfile_SP tmplt_;
  static const std::map<std::string, float> freq_map;
  std::vector<float> freq;
};

}
}
}

#endif

