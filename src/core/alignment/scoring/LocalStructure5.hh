#ifndef CORE_ALIGNMENT_SCORING_LocalStructure5_HH
#define CORE_ALIGNMENT_SCORING_LocalStructure5_HH

#include <vector>
#include <memory>
#include <iostream>

#include <core/index.hh>
#include <core/data/basic/Vec3.fwd.hh>

namespace core {
namespace alignment {
namespace scoring {

/** \brief Holds local distances for 5 amino acid residue fragments.
 *
 * This class holds four distances  for each fragment of 5 consecutive alpha-carbons:
 * \f$R_{15}\f$, \f$R_{14}\f$, \f$R_{25}\f$ and \f$R_{24}\f$ (in this order).
 *
 * @see LocalStructureMatch5 class involves two LocalStructure5 objects to calculate local structural similarity
 */
class LocalStructure5 {
public:

  /// LocalStructureMatch class must be able to read the length of a fragment
  static const int FRAGMENT_LENGTH = 5;

  /// Calculates the distances stored by this object from the given coordinates
  LocalStructure5(const data::basic::Coordinates_SP structure);

  /// Calculates the distances stored by this object from the given coordinates
  LocalStructure5(const data::basic::Coordinates & structure);

  /** \brief Returns iterator to local distances for a fragment starting an the given residue
   *
   * @poaram sequence_position - the first of five residues in the fragment of interest
   */
  inline std::vector<float>::const_iterator get_distances(const core::index4 sequence_position) const {
    return data.cbegin() + sequence_position * 4;
  }

  /** \brief Access to a distance value stored in this object.
   *
   * @poaram i - index of a distance value. For a fragment starting at a residue <code>i</code>, the four
   * distances \f$R_{15}\f$, \f$R_{14}\f$, \f$R_{25}\f$ and \f$R_{24}\f$  are stored in cells from
   * <code>i*4</code> to <code>i*4 + 3</code>, respectively
   */
  inline float operator[](const index4 i) const { return data[i]; }

  /// Returns the number of distances stored in this object, which is <code> N_RESID * 8 </code>
  inline size_t size() const { return data.size(); }

private:
  std::vector<float> data;
  void fill_distances(const data::basic::Coordinates_SP xyz,const short from,const short to,std::vector<float> & where_to,const index1 offset);
  void fill_distances(const data::basic::Coordinates & xyz,const short from,const short to,std::vector<float> & where_to,const index1 offset);
};

}
}
}

#endif

