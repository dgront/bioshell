#include <stdexcept>

#include <core/alignment/scoring/alignment_similarity.hh>

namespace core {
namespace alignment {
namespace scoring {

double aln_0_similarity(const PairwiseAlignment &ai, const PairwiseAlignment &aj) {

  if ((ai.query_length() != aj.query_length()) || (ai.template_length() != aj.template_length()))
    throw std::length_error("The two alignments do not refer to the same proteins");
  double cnt = 0.0;
  for (index2 iq = 0; iq < ai.query_length(); ++iq) {
    if ((ai.which_template_for_query(iq) == aj.which_template_for_query(iq)) && (aj.which_template_for_query(iq) != -1))
      ++cnt;
  }

  return cnt / double(std::min(ai.query_length(), ai.template_length()));
}

double aln_0_similarity(const PairwiseSequenceAlignment &ai, const PairwiseSequenceAlignment &aj) {

  return aln_0_similarity(*ai.alignment, *aj.alignment);
}

double aln_k_similarity(const PairwiseAlignment &ai, const PairwiseAlignment &aj, const short k) {

  if ((ai.query_length() != aj.query_length()) || (ai.template_length() != aj.template_length()))
    throw std::length_error("The two alignments do not refer to the same proteins");
  double cnt = 0.0;
  for (index2 iq = 0; iq < ai.query_length(); ++iq) {
    if ((aj.which_template_for_query(iq) == -1) || (ai.which_template_for_query(iq) == -1)) continue;
    if (aj.which_template_for_query(iq) > ai.which_template_for_query(iq)) {
      if (aj.which_template_for_query(iq) - ai.which_template_for_query(iq) <= k) ++cnt;
    } else {
      if (ai.which_template_for_query(iq) - aj.which_template_for_query(iq) <= k) ++cnt;
    }
  }

  return cnt / double(std::min(ai.query_length(), ai.template_length()));
}

double aln_k_similarity(const PairwiseSequenceAlignment &ai, const PairwiseSequenceAlignment &aj, const short k) {

  return aln_k_similarity(*ai.alignment, *aj.alignment, k);
}

}
}
}


