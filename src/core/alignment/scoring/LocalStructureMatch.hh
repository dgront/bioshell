#ifndef CORE_ALIGNMENT_SCORING_LocalStructureMatch_HH
#define CORE_ALIGNMENT_SCORING_LocalStructureMatch_HH

#include <vector>
#include <memory>
#include <iostream>

#include <core/index.hh>
#include <utils/string_utils.hh>

namespace core {
namespace alignment {
namespace scoring {

/** \brief Similarity matrix defined on local CA trace geometry.
 *
 *  Each LocalStructure5 object holds 4 inter-atomic distances within a 5 residue long segment while
 *  LocalStructure7 keeps 8 distances computed for a 7 residue long segment.
 *  This class calculate a distance value between two  LocalStructure5 or LocalStructure7 objects.
 *  The distance is defined as a sum of squared differences between these local distances.
 *
 * @tparam T - LocalStructure5 or LocalStructure7 data type
 * @tparam N - either 4 or 8 in the case of LocalStructure5 or LocalStructure7 data type, respectively
 */
template <typename T,unsigned int N>
class LocalStructureMatch {
public:

  /** \brief Creates a matching function that will evaluate local structural similarity between a given query and a template.
   *
   * @param query_data - local structure information about the query protein
   * @param tmplt_data - local structure information about the template protein
   */
  LocalStructureMatch(const T & query_data, const T & tmplt_data) :
    query_data(query_data), tmplt_data(tmplt_data) {}

  /** \brief Computes the distance between local conformations : the one starts at <code>i</code> in the query and the other starts at <code>j</code> in the template
   *
   * @param i - index of the first residue of the fragment of interest from the query structure
   * @param j - index of the first residue of the fragment of interest from the template structure
   */
  inline float operator()(const index4 i,const index4 j) const {


    index4 ii = i * N;
    index4 jj = j * N;
    float score = 0.0;
    for (index2 k = 0; k < N; ++k) {
      float t = query_data[ii] - tmplt_data[jj];
      score += t * t;
      ++ii;
      ++jj;
    }

    return score;
  }

  /** \brief Returns an iterator to the local distances calculated for fragment centered at the residue <code>query_residue_pos</code>
   *
   * @param query_residue_pos - index of the middle residue of the fragment of interest from the query sequence
   */
  inline std::vector<float>::const_iterator get_query_distances(const index4 query_residue_pos) const { return query_data.get_distances(query_residue_pos); }

  /** \brief Returns an iterator to the local distances calculated for fragment centered at the residue <code>query_residue_pos</code>
   *
   * @param tmplt_residue_pos - index of the middle residue of the fragment of interest from the template sequence
   */
  inline std::vector<float>::const_iterator get_tmplt_distances(const index4 tmplt_residue_pos) const { return tmplt_data.get_distances(tmplt_residue_pos); }

  /** @brief Prints pairwise local distances: each segment in query vs each segment in template.
   *
   * Onli distances shorter than <code>max_distance</code> are printed
   * @param out_stream - output stream to print the results
   * @param max_distance - maximum distance to be printed
   */
  void print(std::ostream &out_stream, const float max_distance = 1000.0)  const;

private:
  const T & query_data;
  const T & tmplt_data;
};

template <typename T,unsigned int N>
void LocalStructureMatch<T,N>::print( std::ostream &out_stream, const float max_distance) const {

  for (index4 i = 0; i < query_data.size() / N - T::FRAGMENT_LENGTH; ++i) {
    for (index4 j = 0; j < tmplt_data.size() / N - T::FRAGMENT_LENGTH; ++j) {
      if (operator()(i, j) < max_distance)
        out_stream << utils::string_format("%d %d %f\n", i, j, operator()(i, j));
    }
  }
}

/** \brief Computes the structural distance based on distances provided by two iterators.
 *
 * @param qd - distances for the query fragment
 * @param td - distances for the template fragment
 * @tparam T1 - type of the query data, e.g. <code>std::vector<float>::const_iterator</code> or <code>float*</code>
 * @tparam T2 - type of the template data, e.g. <code>std::vector<float>::const_iterator</code> or <code>float*</code>
 * @tparam N - length of the distance data : 4 or 8
 */
template <typename T1,typename T2, unsigned int N>
inline static float local_structure_match(T1 qd,T2 td) {

  float score = 0.0;
  for (index2 k = 0; k < N; ++k) {
    float t = *(qd) - *(td);
    score += t * t;
    ++qd;
    ++td;
  }

  return score;
}

}
}
}

#endif

