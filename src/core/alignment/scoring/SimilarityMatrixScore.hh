/** @brief Means to calculate a score based on a substitution matrix
 *
 */
#ifndef CORE_ALIGNMENT_SCORING_SimilarityMatrixScore_H
#define CORE_ALIGNMENT_SCORING_SimilarityMatrixScore_H

#include <cmath>
#include <string>
#include <vector>

#include <core/index.hh>
#include <core/data/structural/Residue.fwd.hh>
#include <core/alignment/scoring/SimilarityMatrix.hh>

namespace core {
namespace alignment {
namespace scoring {

/** @brief Provides an operator for scoring protein sequences based on a substitution matrix.
 *
 * @tparam T - type of data used to store substitution matrix values; for BLOSUM62 use <code>short int</code>
 */
template<typename T>
class SimilarityMatrixScore {
public:

  /** @brief Create a scoring object used to score matches between the two gives sequences.
   *
   * For every pair of sequences a separate scoring object must be created.
   * @param query - the first (query) sequence
   * @param tmplt - the second (template) sequence
   * @param scoring - a substitution matrix used to score matches
   */
  SimilarityMatrixScore(const std::string & query, const std::string & tmplt,const SimilarityMatrix<T> & scoring);

  /** @brief scoring operator returns score rewarded when two positions are aligned.
   *
   * @param i - position of the query sequence that will be aligned with j-th position in the template
   * @param j - position of the template sequence that will be aligned with i-th position in the query
   */
  inline T operator()(const index2 i,const index2 j) const { return scoring(query_i[i],tmplt_i[j]); }

  /// Returns the length of the query sequence
  inline index2 query_length() const { return query_i.size(); }
  /// Returns the length of the template sequence
  inline index2 tmplt_length() const { return tmplt_i.size(); }

private:
  std::vector<index1> query_i;
  std::vector<index1> tmplt_i;
  const SimilarityMatrix<T> & scoring;
};

template<typename T>
SimilarityMatrixScore<T>::SimilarityMatrixScore(const std::string & query, const std::string & tmplt,const SimilarityMatrix<T> & scoring)
 : query_i(query.size()),tmplt_i(tmplt.size()),scoring(scoring) {

  for(index2 i=0;i<query_i.size();++i)
    query_i[i] = scoring.symbol_to_index(query[i]);
  for(index2 i=0;i<tmplt_i.size();++i)
    tmplt_i[i] = scoring.symbol_to_index(tmplt[i]);
}


}
}
}

#endif

