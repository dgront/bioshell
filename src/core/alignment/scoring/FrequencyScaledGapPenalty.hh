#ifndef CORE_ALIGNMENT_SCORING_FrequencyScaledGapPenalty_H
#define CORE_ALIGNMENT_SCORING_FrequencyScaledGapPenalty_H

#include <vector>
#include <iostream>
#include <core/index.hh>

namespace core {
namespace alignment {
namespace scoring {

class FrequencyScaledGapPenalty {
public:
  FrequencyScaledGapPenalty(const float gap_open, const float gap_extend,
      const std::vector<float> & query_open_profile, const std::vector<float> & query_extend_profile,
      const std::vector<float> & template_open_profile, const std::vector<float> & template_extend_profile);

  /** @brief Penalty (negative value) paid for aligning template residues with a gap in a query
   *
   * Score function for the following scenario:
<code><pre>
   Q: BBBBB---BBBB
   T: BBBBBCCCBBBB
</pre><code>
   * @param last_gapped_pos_template - position in the template where the gap ends, it's 8 in the example above
   * @param gap_length - length of the gap, it's 3 in the example above
   * @return gap penalty (negative)
   */
  float query_gap_cost(index2 last_gapped_pos_template, const index2 gap_length) const;

  /** @brief Penalty (negative value) paid for aligning query residues with a gap in a template.
   *
   * Score function for the following scenario:
<code><pre>
   Q: AACCCAAAA
   T: AA---AAAA
</pre><code>
   *
   *
   * @param last_gapped_pos_template - position in the query where the gap ends, it's 4 in the example above
   * @param gap_length - length of the gap, it's 3 in the example above
   * @return gap penalty (negative)
   */
  float template_gap_cost(index2 last_gapped_pos_query, const index2 gap_length) const;

private:
  float gap_open_;
  float gap_extend_;
  const std::vector<float> &template_open_profile_;
  const std::vector<float> &template_extend_profile_;
  const std::vector<float> &query_open_profile_;
  const std::vector<float> &query_extend_profile_;
  std::vector<float> template_partial_sum_;
  std::vector<float> query_partial_sum_;
};


}
}
}

#endif //CORE_ALIGNMENT_GapPenaltyModel_H
