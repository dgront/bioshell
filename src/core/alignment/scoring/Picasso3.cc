#include <core/alignment/scoring/Picasso3.hh>

namespace core {
namespace alignment {
namespace scoring {

const std::map<std::string, float> Picasso3::freq_map = { { "ALA", 7.81 }, { "GLN", 3.94 }, { "LEU", 9.62 }, { "SER",
    6.88 }, { "ARG", 5.32 }, { "GLU", 6.61 }, { "LYS", 5.93 }, { "THR", 5.45 }, { "ASN", 4.20 }, { "GLY", 6.93 }, {
    "MET", 2.37 }, { "TRP", 1.15 }, { "ASP", 5.30 }, { "HIS", 2.28 }, { "PHE", 4.00 }, { "TYR", 3.07 }, { "CYS", 1.56 },
    { "ILE", 5.91 }, { "PRO", 4.84 }, { "VAL", 6.71 } };

Picasso3::Picasso3(const SequenceProfile_SP query, const SequenceProfile_SP tmplt) :
  query_(query), tmplt_(tmplt) {

  for (index1 i = 0; i < 20; ++i) freq.push_back(freq_map.at(query->monomer_type(i).code3) * 0.01);
}

float Picasso3::operator()(const index2 i,const index2 j) const {

  const std::vector<float> & q = query_->get_probabilities(i);
  const std::vector<float> & t = tmplt_->get_probabilities(j);
  float s1 = 0;
  for (index1 k = 0; k < 20; ++k) {
    s1 += q[k] * log((t[k]+0.0001) / freq[k]);
    s1 += t[k] * log((q[k]+0.0001) / freq[k]);
  }
#ifdef DEBUG
  if(std::isnan(s1)) {
    utils::Logger l("Picasso3");
    l << utils::LogLevel::SEVERE<<"Alignment score is NaN! The profile data was:\n";
    l <<"query: ";
    for (index1 k = 0; k < 20; ++k) l << utils::string_format(" %5.3f",q[k]);
    l <<"\tmplt: ";
    for (index1 k = 0; k < 20; ++k) l << utils::string_format(" %5.3f",t[k]);
    l << "\n";
  }
#endif
  return s1;
}

}
}
}
