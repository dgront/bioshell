#ifndef CORE_ALIGNMENT_SCORING_LocalStructure7_HH
#define CORE_ALIGNMENT_SCORING_LocalStructure7_HH

#include <vector>
#include <memory>
#include <iostream>

#include <core/index.hh>
#include <core/data/basic/Vec3.fwd.hh>

namespace core {
namespace alignment {
namespace scoring {

/** \brief Holds local distances for 7 amino acid residue fragments.
 *
 * This class holds eight distances  for each fragment of 7 consecutive alpha-carbons:
 * \f$R_{17}\f$, \f$R_{16}\f$, \f$R_{27}\f$, \f$R_{15}, \f$R_{26}, \f$R_{37}\f$, \f$R_{14} and \f$R_{47} (in this order).
 *
 * @see LocalStructureMatch7 class involves two LocalStructure7 objects to calculate local structural similarity
 */
class LocalStructure7 {
public:

  /// LocalStructureMatch class must be able to read the length of a fragment
  static const int FRAGMENT_LENGTH = 7;

  /// Calculates the distances from the given coordinates
  LocalStructure7(const data::basic::Coordinates_SP structure);

  /// Calculates the distances from the given coordinates
  LocalStructure7(const data::basic::Coordinates & structure);

  /** \brief Returns iterator to local distances for a fragment starting an the given residue
   *
   * @poaram sequence_position - the first of seven residues in the fragment of interest
   */
  inline std::vector<float>::const_iterator get_distances(const core::index4 sequence_position) const {
    return data.cbegin() + sequence_position * 8;
  }

  /** \brief Read-only access to raw distances stored in this object.
   *
   * @poaram i - index of a distance value. For a fragment starting at a residue <code>i+3</code>, the eight
   * distances \f$R_{17}\f$, \f$R_{16}\f$, \f$R_{27}\f$, \f$R_{15}, \f$R_{26}, \f$R_{37}\f$, \f$R_{14} and \f$R_{47}  are stored in cells from
   * <code>i*8</code> to <code>i*8 + 7</code>, respectively
   */
  inline float operator[](const index4 i) const { return data[i]; }

  /// Returns the number of distances stored in this object, which is <code> N_RESID * 8 </code>
  inline size_t size() const { return data.size(); }

private:
  std::vector<float> data;
  void fill_distances(const data::basic::Coordinates_SP xyz,const short from,const short to,std::vector<float> & where_to,const index1 offset);
  void fill_distances(const data::basic::Coordinates & xyz,const short from,const short to,std::vector<float> & where_to,const index1 offset);
};

}
}
}

#endif

