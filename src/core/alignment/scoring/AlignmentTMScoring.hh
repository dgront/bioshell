#ifndef CORE_ALIGNMENT_SCORING_AlignmentTMScoring_H
#define CORE_ALIGNMENT_SCORING_AlignmentTMScoring_H

#include <core/index.hh>

#include <core/data/basic/Vec3.hh>

#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/calc/structural/transformations/TMScore.hh>

#include <utils/Logger.hh>

namespace core {
namespace alignment {
namespace scoring {

using namespace core::data::basic;
using namespace core::calc::structural::transformations;

/** \brief Similarity score used by TMAlign algorithm.
 */
class AlignmentTMScoring {
public:

  /** \brief Creates the scoring object.
   *
   * @param query - query coordinates
   * @param rt - rototranslation used to superimpose query on the template
   * @param tmplt - template coordinates
   */
  AlignmentTMScoring(const Coordinates & query, const Rototranslation & rt, const Coordinates & tmplt) :
    d0_sq(TMScore::d0(std::min(tmplt.size(),query.size()))), tmplt(tmplt), query(query), rotated_query(query.size()) {

    for (index2 i = 0; i < query.size(); ++i)
      rt.apply(query[i], rotated_query[i]);
  }

  /** \brief Evaluates the score for aligning position <code>i</code> from the template with position <code>j</code> in the query.
   *
   */
  inline double operator()(const index2 i,const index2 j) const {

    const double d2 = tmplt[j].distance_square_to(rotated_query[i]);
    return 1.0/(1.0 + d2/d0_sq);
  }

  /** \brief Transforms the original query atoms by the given rototranslation.
   *
   * <strong>Note,</strong> that this method transforms the original coordinates, rather than the positions that have already resulted from a transformation
   * @param rt - rototranslation transformation
   */
  inline void update(const Rototranslation & rt) {

    for (index2 i = 0; i < query.size(); ++i)
      rt.apply(query[i], rotated_query[i]);
  }

  /// Returns the number of atoms in the first (query) sequence
  inline index2 query_length() const { return query.size(); }

  /// Returns the number of atoms in the second (template) sequence
  inline index2 tmplt_length() const { return tmplt.size(); }

  const double d0_sq;
  const core::data::basic::Coordinates & tmplt;
  const core::data::basic::Coordinates & query;
  core::data::basic::Coordinates rotated_query;
};

}
}
}

#endif
