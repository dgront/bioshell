/** \file alignment_similarity.hh
 * @brief  Functions to calculate similarity between alignments
 *
 */
#ifndef CORE_ALIGNMENT_SCORING_alignment_distance_H
#define CORE_ALIGNMENT_SCORING_alignment_distance_H

#include <core/alignment/PairwiseAlignment.hh>
#include <core/alignment/PairwiseSequenceAlignment.hh>

namespace core {
namespace alignment {
namespace scoring {

/** @brief Calculates the fraction of residue pairs that are aligned identically in the two given alignments.
 *
 * The two alignments must refer to the same pair of sequences.
 * @param ai - the first alignment
 * @param aj - the second alignment
 * @return the fraction of identically aligned pairs divided by the length of the shorter sequence.
 */
double aln_0_similarity(const PairwiseAlignment &ai, const PairwiseAlignment &aj);

/** @brief Calculates the fraction of residue pairs that are aligned identically in the two given alignments.
 *
 * The two alignments must refer to the same pair of sequences.
 * @param ai - the first alignment
 * @param aj - the second alignment
 * @return the fraction of identically aligned pairs divided by the length of the shorter sequence.
 */
double aln_0_similarity(const PairwiseSequenceAlignment &ai, const PairwiseSequenceAlignment &aj);

/** @brief Calculates the fraction of residue pairs that are aligned with +-k tolerance
 *
 * The two alignments must refer to the same pair of sequences.
 * @param ai - the first alignment
 * @param aj - the second alignment
 * @param k - allowed difference in aligned positions to still recognize the two alignments as identical
 * @return the fraction of correctly aligned pairs divided by the length of the shorter sequence.
 */
double aln_k_similarity(const PairwiseAlignment &ai, const PairwiseAlignment &aj, const short k);

/** @brief Calculates the fraction of residue pairs that are aligned with +-k tolerance
 *
 * The two alignments must refer to the same pair of sequences.
 * @param ai - the first alignment
 * @param aj - the second alignment
 * @param k - allowed difference in aligned positions to still recognize the two alignments as identical
 * @return the fraction of correctly aligned pairs divided by the length of the shorter sequence.
 */
double aln_k_similarity(const PairwiseSequenceAlignment &ai, const PairwiseSequenceAlignment &aj, const short k);


}
}
}

#endif

