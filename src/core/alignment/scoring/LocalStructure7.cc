#include <vector>
#include <memory>
#include <iostream>

#include <core/index.hh>
#include <core/data/basic/Vec3.hh>
#include <core/alignment/scoring/LocalStructure7.hh>

#include <utils/string_utils.hh>

namespace core {
namespace alignment {
namespace scoring {

LocalStructure7::LocalStructure7(const data::basic::Coordinates_SP coordinates) : LocalStructure7(*coordinates) {}

LocalStructure7::LocalStructure7(const data::basic::Coordinates & coordinates) {

  data.resize(coordinates.size() * 8);

  // ---------- local distances for the coordinates
  fill_distances(coordinates,-3,3,data,0); // ---------- r17(-3,3)
  fill_distances(coordinates,-3,2,data,1); // ---------- r16(-3,2)
  fill_distances(coordinates,-2,3,data,2); // ---------- r16(-2,3)
  fill_distances(coordinates,-3,1,data,3); // ---------- r15(-3,1)
  fill_distances(coordinates,-2,2,data,4); // ---------- r15(-2,2)
  fill_distances(coordinates,-1,3,data,5); // ---------- r15(-1,3)
  fill_distances(coordinates,-3,0,data,6); // ---------- r14(-3,0)
  fill_distances(coordinates, 0,3,data,7); // ---------- r14(0,3)
}

void LocalStructure7::fill_distances(const data::basic::Coordinates_SP xyz, const short from, const short to,
                                     std::vector<float> &where_to, const index1 offset) {

  for (index4 i = -from; i < xyz->size() - to; ++i)
    where_to[i * 8 + offset] = xyz->operator [](i + from).distance_to(xyz->operator [](i + to));
}

void LocalStructure7::fill_distances(const data::basic::Coordinates &xyz, const short from, const short to,
                                     std::vector<float> &where_to, const index1 offset) {

  for (index4 i = -from; i < xyz.size() - to; ++i)
    where_to[i * 8 + offset] = xyz.operator [](i + from).distance_to(xyz.operator [](i + to));
}

}
}
}

