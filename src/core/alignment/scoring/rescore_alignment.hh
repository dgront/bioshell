/** @brief Means to calculates the score of an alignment
 *
 */
#ifndef CORE_ALIGNMENT_SCORING_rescore_alignment_H
#define CORE_ALIGNMENT_SCORING_rescore_alignment_H

#include <string>
#include <vector>

#include <core/index.hh>

namespace core {
namespace alignment {
namespace scoring {

/** @brief Calculates a score of a given alignment with an affine gap penalty.
 *
 * @param scoring - object used to score a match in the alignment
 * @param gap_opening_penalty - penalty (a negative score value) assigned to each gap opened
 * @param gap_extending_penalty - penalty (a negative score value) assigned to each gap extended
 * @param aligned_query - aligned query sequence
 * @param aligned_template - aligned template sequence
 */
template<typename T, typename Op>
T score(Op scoring, const T gap_opening_penalty, const T gap_extending_penalty, const std::string aligned_query, const std::string aligned_template) {

  bool gap_opened=false;
  T score = 0;
  index2 qi = 0;
  index2 ti = 0;
  for (index2 i = 0; i < aligned_query.size(); ++i) {
    if((aligned_query[i] == '-') || (aligned_query[i] == '_')) { // --------- gap in a query sequence
      if(gap_opened)
        score += gap_extending_penalty;
      else {
        gap_opened=true;
        score += gap_opening_penalty;
      }
      ++ti;
      continue;
    }
    if((aligned_template[i] == '-') || (aligned_template[i] == '_')) { // --------- gap in a template sequence
      if(gap_opened)
        score += gap_extending_penalty;
      else {
        gap_opened=true;
        score += gap_opening_penalty;
      }
      ++qi;
      continue;
    }

    score += scoring(qi, ti);
    gap_opened = false;
    ++qi;
    ++ti;
  }

  return score;
}

}
}
}

#endif

