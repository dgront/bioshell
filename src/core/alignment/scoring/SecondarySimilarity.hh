#ifndef CORE_ALIGNMENT_SCORING_SecondarySimilarity_H
#define CORE_ALIGNMENT_SCORING_SecondarySimilarity_H

#include <cmath>
#include <string>
#include <vector>
#include <map>

#include <core/index.hh>
#include <core/data/sequence/SecondaryStructure.hh>

#include <core/alignment/scoring/AlignmentScoreBase.hh>

namespace core {
namespace alignment {
namespace scoring {

using core::data::sequence::SecondaryStructure_SP;

/** @brief Scores similarity between two secondary structure probability vectors.
 *
 */
class SecondarySimilarity : public AlignmentScoreBase<float> {
public:
  SecondarySimilarity(const SecondaryStructure_SP query, const SecondaryStructure_SP tmplt) :
    query_(query), tmplt_(tmplt) {}

  /** \brief Calculates the score.
   * @param i - position index in the query sequence
   * @param j - position index in the template sequence
   * @returns the score
   */
  inline float operator()(const index2 i,const index2 j) const;

  /// Returns the length of the query sequence
  inline index2 query_length() const { return query_->length(); }
  /// Returns the length of the template sequence
  inline index2 tmplt_length() const { return tmplt_->length(); }

private:
  SecondaryStructure_SP query_;
  SecondaryStructure_SP tmplt_;
  const float HH = 1.0;
  const float EE = 1.0;
  const float CC = 0.5;
  const float HE = -2.0;
  const float HC = 0.0;
  const float EC = 0.0;
};

inline float SecondarySimilarity::operator()(const index2 i,const index2 j) const {

  double s1 = query_->fraction_H(i)
      * (tmplt_->fraction_H(j) * HH + tmplt_->fraction_E(j) * HE + tmplt_->fraction_C(j) * HC);
  s1 += query_->fraction_E(i) * (tmplt_->fraction_E(j) * EE + tmplt_->fraction_C(j) * EC);
  s1 += query_->fraction_C(i) + tmplt_->fraction_C(j) * CC;

#ifdef DEBUG
  if(std::isnan(s1)) {
    utils::Logger l("SecondarySimilarity");
    l << utils::LogLevel::SEVERE<<"Alignment score is NaN! The secondary structure data was:\n";
    l <<"query: " << query_->fraction_H(i)<<" "<<query_->fraction_E(i)<<" "<<query_->fraction_C(i)<<"\n";
    l <<"tmply: " << tmplt_->fraction_H(j)<<" "<<tmplt_->fraction_E(j)<<" "<<tmplt_->fraction_C(j)<<"\n";
  }
#endif
  return s1;
}

}
}
}

#endif

