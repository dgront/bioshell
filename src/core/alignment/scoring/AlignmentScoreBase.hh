#ifndef CORE_ALIGNMENT_SCORING_AlignmentScoreBase_H
#define CORE_ALIGNMENT_SCORING_AlignmentScoreBase_H

#include <core/index.hh>

namespace core {
namespace alignment {
namespace scoring {

/** \brief Base class for dynamic-programming score types
 */
template<typename T>
class AlignmentScoreBase {
public:

  /// Virtual destructor is required but empty
  virtual ~AlignmentScoreBase() {}

  /** \brief Calculates the score.
   * @param i - position index in the query sequence
   * @param j - position index in the template sequence
   * @returns the score
   */
  virtual T operator()(const index2 i, const index2 j) const = 0;

  /// Returns the length of the query sequence
  virtual index2 query_length() const = 0;
  /// Returns the length of the template sequence
  virtual index2 tmplt_length() const = 0;

};

}
}
}

#endif

