/** \file k_tuples.hh
 * @brief  methods that operates on k-tuples extracted from a protein sequence.
 *
 * These methods are used in FASTA algorithm implementation and also to quickly estimate sequence identity
 */
#ifndef CORE_ALIGNMENT_SCORING_k_tuples_H
#define CORE_ALIGNMENT_SCORING_k_tuples_H

#include <string>
#include <bitset>
#include <vector>

#include <core/index.hh>
#include <core/data/sequence/ReduceSequenceAlphabet.hh>

#include <utils/Logger.hh>

namespace core {
namespace alignment {
namespace scoring {

/** @brief Creates all possible k-tuple strings from a given sequence by cutting it into k-mer oligomers.
 *
 * @param sequence - a sequence that will be cut into overlapping tuples
 * @param tuples - vector used to store the resulting tuples. The vector will be emptied before new tuples are stored
 */
template<unsigned char k>
void create_k_tuples(const std::string & sequence, std::vector<std::string> & tuples) {

  tuples.clear();
  for (size_t i = 0; i < sequence.length() - k + 1; ++i)
    tuples.push_back(sequence.substr(i, i + k));
}

/** @brief Creates all possible k-tuple strings from a given sequence by cutting it into k-mer oligomers.
 *
 * @param sequence - a sequence that will be cut into overlapping tuples
 * @param tuples - container used to store the resulting tuples. The vector will be emptied before new tuples are stored
 */
template<unsigned char k>
void create_k_tuples(const std::string &sequence, std::map<std::string, std::vector<index2>> &tuples) {

  tuples.clear();
  for (size_t i = 0; i < sequence.length() - k + 1; ++i) {
    std::string tuple = sequence.substr(i, k);
    if (tuples.find(tuple) == tuples.cend())
      tuples[tuple] = std::vector<core::index2>{};
    tuples[tuple].push_back(i);
  }
}

}
}
}

#endif

