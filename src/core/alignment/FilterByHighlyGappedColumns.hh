#ifndef CORE_ALIGNMENT_FilterByHighlyGappedColumns_HH
#define CORE_ALIGNMENT_FilterByHighlyGappedColumns_HH

#include <core/alignment/MSAFilter.hh>

namespace core {
namespace alignment {

class FilterByHighlyGappedColumns : public MSAFilter {
public:
  explicit FilterByHighlyGappedColumns(const std::vector<core::data::sequence::Sequence_SP> & msa) :
      MSAFilter(msa), logs("FilterByHighlyGappedColumns") {};

  /// Run this filter
  void run(core::index2 parameter) override;

private:
  utils::Logger logs;
};

}
}

#endif
