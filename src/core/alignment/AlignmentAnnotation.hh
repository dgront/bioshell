#ifndef CORE_ALIGNMENT_AlignmentAnnotation_H
#define CORE_ALIGNMENT_AlignmentAnnotation_H

#include <map>
#include <string>

namespace core {
namespace alignment {

/** @brief Keys used to store information about an alignment
 */
enum class AlignmentAnnotation {
  PDB_ID, ///< PDB code of a target structure
  CHAIN_ID, ///< ID of a target chain
  E_VALUE, ///< Alignment e-value
  SEQ_ID, ///< Alignment sequence identity as stored along the alignment
  FIRST_QUERY_POS, ///< First position in the query sequence covered by the annotated alignment
  FIRST_TMPLT_POS ///< First position in the template sequence covered by the annotated alignment
};

}
}

#endif