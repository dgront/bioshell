#ifndef CORE_ALIGNMENT_Target_FWD_H
#define CORE_ALIGNMENT_Target_FWD_H

#include <memory>

namespace core {
namespace alignment {

class Target;
typedef std::shared_ptr<Target> Target_SP;

}
}

#endif
