#ifndef CORE_DATA_STRUCTURAL_CoordinatesSet_H
#define CORE_DATA_STRUCTURAL_CoordinatesSet_H

#include <string>
#include <vector>

#include <core/index.hh>

#include <core/data/basic/Vec3.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

namespace core {
namespace alignment {

/** \brief Represents a single input structure subjected to structure alignment.
 *
 * Coordinates set provides access to coordinates of selected atoms and their residues.
 * By default, alpha carbons are selected.
 */
class CoordinatesSet {
public:

  const std::string name; ///< short string ID for each structure, typically its PDB code
  const core::data::structural::Structure_SP structure; ///< full structures, as loaded from a PDB file;

  /** @brief Provides const-access to the coordinates used during structure alignment.
   *
   * These are coordinates of the atoms which satisty the selector used to construct this object (C-alpha atoms by default)
   */
  const core::data::basic::Coordinates & coordinates() const { return coordinates_; }

  /** @brief Superimposed residues from the input structure repacked to a single array.
   *
   * Provides const-access to residues whose atoms were used in structure alignment
   */
  const std::vector<core::data::structural::Residue_SP> & residues() { return residues_; }

  /** @brief Create a set of coordinates that will be subjected to structure alignment
   *
   * @param s - source structure
   * @param name - anem of the aligned structure (to be used in output and to name relevant output files)
   * @param select_superimposed  - selector marking the atoms that will be aligned
   */
  CoordinatesSet(const core::data::structural::Structure_SP s,const std::string &name,
    const core::data::structural::selectors::AtomSelector & select_superimposed = core::data::structural::selectors::IsCA());

  /** @brief Creates an amino acid sequence comprising only these residues, whose atoms are subjected to structure alignment
   *
   * @return pointer to a newly created sequence object
   */
  const core::data::sequence::Sequence_SP create_superimposed_sequence() const ;

private:
  utils::Logger log;
  core::data::basic::Coordinates coordinates_; ///< Coordinates of atoms being aligned
  std::vector<core::data::structural::Residue_SP> residues_;
};

}
}

#endif

