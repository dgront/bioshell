#ifndef CORE_ALIGNMENT_NWAlignerAnyGap_H
#define CORE_ALIGNMENT_NWAlignerAnyGap_H

#include <cmath>
#include <vector>
#include <limits.h>

#include <core/index.hh>

#include <core/data/basic/Array2D.hh>
#include <core/alignment/PairwiseAlignment.hh>

namespace core {
namespace alignment {

/** \brief Computes global alignment of two sequences with arbitrary gap penalty.
 *
 * \include ap_NWAlignerAnyGap.cc
 *
 * @tparam S - call operator used to calculate a score for a match between two positions
 * @tparam G - call operator used to calculate a penalty for a gap for a given length
 */
template<typename S, typename G>
class NWAlignerAnyGap {
public:
  /** @brief Create an aligner object that will calculate global pairwise sequence alignments.
   *
   * The longest sequence must be at most <code>max_size</code> residues long.
   * @param max_size - the length of the longest aligned sequence.
   */
  NWAlignerAnyGap(const unsigned int max_size) : max_size(max_size), arrows(max_size+1,max_size+1), scores(max_size+1,max_size+1) {
    query_size = tmplt_size = 0;
  }

  /// Default virtual destructor
  virtual ~NWAlignerAnyGap() = default;

  /** @brief Calculates global sequence alignment using Needleman-Wunsch algorithm with arbitrary gap penalty.
   *
   * This is a very general method. The whole knowledge about the target and query proteins is encoded by the scoring operator type.
   * Once this call is finished, the resulting alignment may be obtained by calling backtrace() method.
   * @param scoring - instance of a class that provides three methods:
   *     - tmplt_length() - the number of positions in the template sequence
   *     - query_length() - the number of positions in the query sequence
   *     - call operator operating on two integer indexes, which provides a score for matching the two respective positions
   *     See \ref doc/web_examples/ex_SemiglobalAligner.cc "doc/web_examples/ex_SemiglobalAligner.cc"  demo program for an example similarity scoring function
   * @tparam S - type of an object used to score matches
   * @tparam G - type of an object used to assign gap penalties
   * @return alignment score.
   */
  float align(const S & scoring,const G & gap_penalty);

  /** @brief Calculates the score of the global optimal sequence alignment using Needleman-Wunsch algorithm with arbitrary gap penalty.
   *
   * This method is very similar to align(), but it <strong>does not store</strong>
   * information for backtracing; alignment will not be available. This method however is a bit faster that
   * align().
   */
  float align_for_score(const S & scoring,const G & gap_penalty);

  /** @brief Backtraces the alignment based on information sored by the most recent align(const T,const  T,const Op &) call.
   *
   */
  PairwiseAlignment_SP backtrace();

  /// Returns the score found by the most recent call of align(const T,const  T,const Op &) or align_for_score(const T,const  T,const Op &)
  inline float recent_score() const { return recent_score_; }

private:
  float recent_score_ = 0;
  const unsigned  int max_size;
  index2 query_size;
  index2 tmplt_size;

  core::data::basic::Array2D<short int> arrows;
  core::data::basic::Array2D<float> scores;
  std::vector<char> back_trace;

  static const float not_an_option;
};

template<typename S, typename G>
const float NWAlignerAnyGap<S, G>::not_an_option = -std::numeric_limits<float>::max() /2;

template<typename S, typename G>
float NWAlignerAnyGap<S, G>::align(const S & scoring,const G & gap_penalty) {

  query_size = scoring.query_length();
  tmplt_size = scoring.tmplt_length();

  scores(0,0) = 0;
  for (index2 j = 1; j <= query_size; ++j)  // gap in a template sequence spends letters of a query
    scores(j,0) = gap_penalty.template_gap_cost(j,j);
  for (index2 j = 1; j <= tmplt_size; ++j)  // gap in a query sequence spends letters of a template
    scores(0,j) = gap_penalty.query_gap_cost(j,j);

  for (index2 i = 1; i <= query_size; ++i) {
    for (index2 j = 1; j <= tmplt_size; ++j) {
      index2 max_k_q = 1, max_k_t = 1;
      const float h = float(scores(i - 1,j - 1) + (scoring)(i - 1, j - 1));
      float gap_q = scores(i,j - 1) + gap_penalty.query_gap_cost(j-1, 1);
      for (index2 k = 2; k < j; ++k) {
        float p = gap_penalty.query_gap_cost(j-1, k);
        if (scores(i,j - k) + p > gap_q) {
          gap_q = scores(i,j - k) + p;
          max_k_q = k;
        }
      }
      float gap_t = scores(i - 1,j) + gap_penalty.template_gap_cost(i-1, 1);
      for (index2 k = 2; k < i; ++k) {
        float p = gap_penalty.template_gap_cost(i, k);
        if (scores(i - k, j) + p > gap_t) {
          gap_t = scores(i - k,j) + p;
          max_k_t = k;
        }
      }
      if ((h > gap_q) && (h > gap_t)) {
        scores(i,j) = h;
        arrows(i,j) = 0;
      } else {
        if (gap_q > gap_t) {
          scores(i,j) = gap_q;
          arrows(i,j) = -max_k_q;
        } else {
          scores(i,j) = gap_t;
          arrows(i,j) = max_k_t;
        }
      }
    }
  }

  recent_score_ = scores(query_size,tmplt_size);

  return recent_score_;
}

template<typename S, typename G>
float NWAlignerAnyGap<S,G>::align_for_score(const S & scoring,const G & gap_penalty) {

  query_size = scoring.query_length();
  tmplt_size = scoring.tmplt_length();
  scores(0,0) = 0;
  for (index2 j = 1; j <= query_size; ++j)  // gap in a template sequence spends letters of a query
    scores(j,0) = gap_penalty.template_gap_cost(j,j);
  for (index2 j = 1; j <= tmplt_size; ++j)  // gap in a query sequence spends letters of a template
    scores(0,j) = gap_penalty.query_gap_cost(j,j);

  for (index2 i = 1; i <= query_size; ++i) {
    for (index2 j = 1; j <= tmplt_size; ++j) {
      const float h = T(scores(i - 1, j - 1) + (scoring)(i - 1, j - 1));
      float gap_q = scores(i,j-1) + gap_penalty.query_gap_cost(j-1, 1);
      for (index2 k = 2; k < j; ++k) {
        float p = gap_penalty.query_gap_cost(j-1, k);
        if (scores(i,j-k) + p < gap_q) gap_q = scores(i,j-k) + p;
      }
      float gap_t = scores(i-1,j) + gap_penalty.template_gap_cost(i-1, 1);
      for (index2 k = 2; k < i; ++k) {
        float p = gap_penalty.template_gap_cost(i-1, k);
        if (scores(i-k,j) + p < gap_t) gap_t = scores(i-k,j) + p;
      }
      if ((h > gap_q) && (h > gap_t)) {
        scores(i,j) = h;
      } else {
        if (gap_q > gap_t) scores(i,j) = gap_q;
         else scores(i,j) = gap_t;
      }
    }
  }

  recent_score_ = scores(query_size,tmplt_size);

  return recent_score_;
}

template<typename S, typename G>
PairwiseAlignment_SP NWAlignerAnyGap<S,G>::backtrace() {

  PairwiseAlignment_SP a = std::make_shared<PairwiseAlignment>(1, 1, double(recent_score_));

  if (back_trace.size() == 0) back_trace.resize(max_size + max_size);

  index2 i = query_size, j = tmplt_size;
  int k = -1;

  while (i > 0 || j > 0) {
    if((arrows.operator()(i,j) & 2) !=0) {
      back_trace[++k] = 2;
//      std::cerr <<int(arrows->operator()(i,j))<< " "<<i<<" "<<j<<" "<<k<<" "<<int(back_trace[k])<<"\n";
      i--;
      j--;
      continue;
    }
    if((arrows.operator()(i,j) & 1) !=0) {
      back_trace[++k] = 1;
      j--;
      continue;
    }
    back_trace[++k] = 4;
    i--;
  }
  while(k>=0) {
    if(back_trace[k]==2) {
      a->append_match();
      --k;
      continue;
    }
    if(back_trace[k]==1) {
      a->append_query_gap();
      --k;
      continue;
    }
    if(back_trace[k]==4) {
      a->append_template_gap();
      --k;
      continue;
    }
  }

  return a;
}

}
}
/**
 * \example ap_NWAligner.cc
 */
#endif

