/** \file alignment_utility_tasks.hh
 * @brief defines option objects (command line flags) related to sequence/structure alignment
 */
#ifndef CORE_ALIGNMENT_TASKS_alignment_utility_tasks_HH
#define CORE_ALIGNMENT_TASKS_alignment_utility_tasks_HH

#include <memory> // for shared ptr

#include <core/data/io/XMLElement.hh>
#include <ui/tasks/Task.hh>

namespace core {
namespace alignment {
namespace tasks {

static const std::string OutputTemplate("name"); ///< where the processed template PDB should be written


class TrimStructureByAlignment : public ui::tasks::Task {
public:

  static const std::string MyTagName; ///< XML tag name that defines this task

  /** @name XML tags that used to control AlignedSubstructureSelector task
   *
   * <strong>Note</strong> that some of them may also be used as attributes!
   */
  ///@{
  static const std::string InputAlignmentPIR;///< provides input alignment in the PIR format
  static const std::string InputAlignmentFASTA;///< provides input alignment in the FASTA format
  static const std::string InputAlignmentPath; ///< a string that encodes an alignment
  static const std::string InputQueryPDB;///< provides input query structure
  static const std::string InputTemplatePDB; ///< provides input template structure
  static const std::string OutputQueryPDB;///< where the processed query PDB should be written
  static const std::string OutputTemplatePDB; ///< where the processed template PDB should be written
  ///@}

  TrimStructureByAlignment(const std::string & xml_cfg_file);

  /** @brief Creates a new AlignedSubstructureSelector task based on the XML data structure.
   *
   * @param xml_cfg - root element of a XML structure describing this task
   */
  TrimStructureByAlignment(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);

  virtual ~TrimStructureByAlignment() {}

  /// Trims a structure by an alignment so only the un-aligned part is removed
  virtual void run();

private:
  std::string pir_input;
  std::string fasta_input;
  std::string input_target;
  std::string input_template;
  std::string output_target;
  std::string output_template;
  std::string alignment_path;
  utils::Logger l;
  bool load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg);
};

}
}
}

#endif
