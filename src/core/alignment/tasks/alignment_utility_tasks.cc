#include <core/data/io/pir_io.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/io/fasta_io.hh>
#include <core/data/io/XML.hh>
#include <core/data/io/XMLElement.hh>

#include <core/alignment/PairwiseAlignment.hh>
#include <core/alignment/tasks/alignment_utility_tasks.hh>

#include <ui/tasks/Task.hh>
#include <utils/io_utils.hh>

namespace core {
namespace alignment {
namespace tasks {

const std::string TrimStructureByAlignment::MyTagName = "trim_structure_by_alignment";
const std::string TrimStructureByAlignment::InputAlignmentPIR = "input_alignment_pir"; //
const std::string TrimStructureByAlignment::InputAlignmentFASTA = "input_alignment_fasta"; //
const std::string TrimStructureByAlignment::InputAlignmentPath = "input_alignment_path"; //
const std::string TrimStructureByAlignment::InputQueryPDB = "input_query_pdb"; //
const std::string TrimStructureByAlignment::InputTemplatePDB = "input_template_pdb"; //
const std::string TrimStructureByAlignment::OutputQueryPDB = "output_query_pdb";
const std::string TrimStructureByAlignment::OutputTemplatePDB = "output_template_pdb";

TrimStructureByAlignment::TrimStructureByAlignment(const std::string & xml_cfg_file) : l("TrimStructureByAlignment") {
  Task::task_name = "TrimStructureByAlignment";
  core::data::io::XML parser(xml_cfg_file);
  load_task(parser.document_root());
}


TrimStructureByAlignment::TrimStructureByAlignment(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) : l("TrimStructureByAlignment") {

  Task::task_name = "TrimStructureByAlignment";
  load_task(task_cfg);
}

bool TrimStructureByAlignment::load_task(const std::shared_ptr<core::data::io::XMLElement> & task_cfg) {

  if (task_cfg->name().compare(MyTagName) == 0) {
    // ---------- Extract the variable name and its associated value
    pir_input = task_cfg->find_value(InputAlignmentPIR);
    fasta_input = task_cfg->find_value(InputAlignmentFASTA);
    input_target = task_cfg->find_value(InputQueryPDB);
    input_template = task_cfg->find_value(InputTemplatePDB);
    output_target = task_cfg->find_value(OutputQueryPDB);
    output_template = task_cfg->find_value(OutputTemplatePDB);
    alignment_path = task_cfg->find_value(InputAlignmentPath);

    if ((pir_input.size() == 0) && (alignment_path.size() == 0)&& (fasta_input.size() == 0)) {
      l << utils::LogLevel::SEVERE << "TrimStructureByAlignment requires an input alignment!\n";
    }

  }

  return false;
}

void TrimStructureByAlignment::run() {

  std::vector<std::shared_ptr<core::data::sequence::Sequence>> input_sequences;   // Input sequences read from a FASTA or a PIR file
  PairwiseAlignment_SP ali;

  // ---------- Read FASTA
  if (fasta_input.size()>0) core::data::io::read_fasta_file(fasta_input, input_sequences);

  // ---------- Read PIR
  if (pir_input.size()>0) core::data::io::read_pir_file(pir_input, input_sequences);

  // ---------- Create alignment
  if (alignment_path.size() > 0) ali = std::make_shared<PairwiseAlignment>(alignment_path);
  else {
    if (input_sequences.size() > 1) ali = std::make_shared<PairwiseAlignment>(input_sequences[0]->sequence,
        input_sequences[0]->first_pos(), input_sequences[1]->sequence, input_sequences[1]->first_pos(), 0.0);
    else {
      l << utils::LogLevel::SEVERE << "There must be two sequences in the input alignment!\n";
    }
  }

  if(input_target.size()>0) {
    std::shared_ptr<std::ostream> out_sp = utils::out_stream(output_target);
    core::data::io::Pdb pdb(input_target);
    core::data::structural::Structure_SP q_str = pdb.create_structure(0);
    core::index2 cnt = 0;
    for (auto r_it = q_str->first_residue(); r_it != q_str->last_residue(); ++r_it) {
      if(ali->which_template_for_query(cnt)>=0) {
        for (auto q_atom_it = (*r_it)->begin(); q_atom_it != (*r_it)->end(); ++q_atom_it)
        *out_sp << (*q_atom_it)->to_pdb_line() << "\n";
      }
      ++cnt;
    }
  }

  if (input_template.size() > 0) {
    std::shared_ptr<std::ostream> out_sp = utils::out_stream(output_template);
    core::data::io::Pdb pdb(input_template);
    core::data::structural::Structure_SP t_str = pdb.create_structure(0);
    core::index2 cnt = 0;
    for (auto r_it = t_str->first_residue(); r_it != t_str->last_residue(); ++r_it) {
      if (ali->which_template_for_query(cnt) >= 0) {
        for (auto t_atom_it = (*r_it)->begin(); t_atom_it != (*r_it)->end(); ++t_atom_it)
          *out_sp << (*t_atom_it)->to_pdb_line() << "\n";
      }
      ++cnt;
    }
  }
}

}
}
}
