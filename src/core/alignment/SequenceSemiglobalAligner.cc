#include <memory>

#include <core/alignment/SequenceSemiglobalAligner.hh>
#include <core/alignment/PairwiseSequenceAlignment.hh>

namespace core {
namespace alignment {

using namespace core::data::sequence;

void SequenceSemiglobalAligner::create_sequences(const std::string &query, const std::string &tmplt) {

  bool create_sequences = false;
  if((recent_tmplt == nullptr) || (recent_query == nullptr)) {
    create_sequences = true;
  } else {
    if(recent_query->sequence != query) create_sequences = true;
    if(recent_tmplt->sequence != tmplt) create_sequences = true;
  }
  if (create_sequences) {
    recent_query = std::make_shared<Sequence>("query", query);
    recent_tmplt = std::make_shared<Sequence>("tmplt", tmplt);
  }
}

short SequenceSemiglobalAligner::align(const Sequence_SP query, const Sequence_SP tmplt,
                               const short gap_open, const short gap_extend, const std::string &matrix_name) {
  recent_query = query;
  recent_tmplt = tmplt;
  return align(query->sequence, tmplt->sequence, gap_open, gap_extend, matrix_name);
}

short SequenceSemiglobalAligner::align(const std::string &query, const std::string &tmplt,
                               const short gap_open, const short gap_extend, const std::string &matrix_name) {

  create_sequences(query, tmplt);

  scoring::NcbiSimilarityMatrix_SP sim_m = scoring::NcbiSimilarityMatrixFactory::get().get_matrix(matrix_name);
  scoring::SimilarityMatrixScore<short> score(query, tmplt, *sim_m);
  return SemiglobalAligner<short, scoring::SimilarityMatrixScore<short>>::align(gap_open, gap_extend, score);
}


short SequenceSemiglobalAligner::align_for_score(const std::string &query, const std::string &tmplt,
                                         const short gap_open, const short gap_extend, const std::string &matrix_name) {

  create_sequences(query, tmplt);

  scoring::NcbiSimilarityMatrix_SP sim_m = scoring::NcbiSimilarityMatrixFactory::get().get_matrix(matrix_name);
  scoring::SimilarityMatrixScore<short> score(query, tmplt, *sim_m);
  return SemiglobalAligner<short, scoring::SimilarityMatrixScore<short>>::align_for_score(gap_open, gap_extend, score);
}

short SequenceSemiglobalAligner::align_for_score(const core::data::sequence::Sequence_SP query, const core::data::sequence::Sequence_SP tmplt,
                                         const short gap_open, const short gap_extend, const std::string &matrix_name) {
  recent_query = query;
  recent_tmplt = tmplt;
  return align_for_score(query->sequence, tmplt->sequence, gap_open, gap_extend, matrix_name);
}

PairwiseSequenceAlignment_SP SequenceSemiglobalAligner::backtrace_sequence_alignment() {

  PairwiseAlignment_SP ali = SemiglobalAligner<short, scoring::SimilarityMatrixScore<short>>::backtrace();
  return std::make_shared<PairwiseSequenceAlignment>(ali, recent_query, recent_tmplt);
}


}
}


