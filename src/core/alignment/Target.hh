#ifndef CORE_ALIGNMENT_Target_H
#define CORE_ALIGNMENT_Target_H

#include <string>
#include <vector>
#include <memory>
#include <stdexcept>

#include <core/alignment/Target.fwd.hh>

#include <core/data/io/ss2_io.hh>
#include <core/data/sequence/SequenceProfile.hh>
#include <core/data/sequence/SecondaryStructure.hh>

#include <utils/Logger.hh>

namespace core {
namespace alignment {

using namespace core::data::sequence;
using namespace core::data::io;

/// Represents target for a threading search
class Target {
public:

  std::string name; ///< String ID for the target, e.g. the name of a sequence
  const SequenceProfile_SP profile; ///< Points to a sequence profile calculated for the target sequence
  const SecondaryStructure_SP ss; ///< Points to a secondary structure profile calculated for the target sequence

  Target(const std::string & name, const SequenceProfile_SP query_profile, const SecondaryStructure_SP query_ss) :
    name(name), profile(query_profile), ss(query_ss), logger("Target") {
    if(profile->sequence.compare(ss->sequence)!=0) {
      logger << utils::LogLevel::SEVERE<<"Sequence from a profile is different than the sequence from a SS prediction!\n";
      throw std::runtime_error("Can't create Target object because the two given sequences are different");
    }
  }

  Target(const std::string & name, const std::string & query_profile, const std::string & query_ss) :
      Target(name, read_binary_checkpoint(query_profile), read_ss2(query_ss)) {}

  /// Returns the number of residues in the target sequence
  index2 length() { return profile->length(); }

  /** @brief Read a Target object from a stream
   * @param in - input stream
   */
  static const Target_SP from_stream(std::istream & in);

  /** @brief Read a Target object from a file
   * @param filename - input file name
   */
  static const Target_SP from_file(const std::string & filename);

private:
  utils::Logger logger;
};

std::ostream & operator<<(std::ostream & out, const Target & t);

}
}

#endif
