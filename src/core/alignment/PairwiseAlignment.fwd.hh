#ifndef CORE_ALIGNMENT_PairwiseAlignment_FWD_H
#define CORE_ALIGNMENT_PairwiseAlignment_FWD_H

#include <memory>

namespace core {
namespace alignment {

class PairwiseAlignment;

typedef std::shared_ptr <PairwiseAlignment> PairwiseAlignment_SP;

}
}
#endif // CORE_ALIGNMENT_PairwiseAlignment_FWD_H
