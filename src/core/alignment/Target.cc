#include <iostream>
#include <regex>

#include <core/alignment/Target.hh>

namespace core {
namespace alignment {

std::ostream & operator<<(std::ostream & out, const Target & t) {

  out << "# " << t.name << " " << t.profile->length() << " residues\n";
  out << t.profile->sequence<<"\n";
  out << t.ss->str()<<"\n";
  for (index2 i = 0; i < t.profile->length(); ++i) {
    out << utils::string_format("%4d ", i + t.profile->first_pos()) << t.profile->operator [](i);
    const std::vector<float> & p = t.profile->get_probabilities(i);
    out << utils::string_format(" %5.3f", p);
    out << " " << t.ss->ss(i) << utils::string_format(" %5.3f", t.ss->fraction_H(i))
        << utils::string_format(" %5.3f", t.ss->fraction_E(i)) << utils::string_format(" %5.3f", t.ss->fraction_C(i))<<"\n";
  }
  return out;
}


const Target_SP Target::from_stream(std::istream & in) {

  static utils::Logger l("Target");
  if (!in) {
    l << utils::LogLevel::SEVERE << "Can't read template data from a stream!\n";
    return nullptr;
  }

  std::string line;
  std::getline(in, line);
  std::smatch matches;

  if(!std::regex_match(line,matches,std::regex("# (.+) : (\\d+) residues"))) {
    l << utils::LogLevel::SEVERE << "Incorrect file header!\n";
    return nullptr;
  }

  std::string target_name = matches[1];
  core::index2 size = utils::from_string<core::index2>(matches[2]);
  std::string seq, ss;
  std::getline(in, seq);
  std::getline(in, ss);

  core::data::sequence::SequenceProfile_SP p = std::make_shared<core::data::sequence::SequenceProfile>(target_name, seq,
      core::data::sequence::SequenceProfile::aaOrderByNCBI());
  core::data::sequence::SecondaryStructure_SP s = std::make_shared<core::data::sequence::SecondaryStructure>(target_name,
      seq, 0, ss);

  core::index2 i = 0;
  std::vector<std::string> tokens;
  while (std::getline(in, line)) {
    if (line[0] == '#' || line.length() < 100) continue;
    tokens.clear();
    utils::split(line, tokens, ' ');
    for (index2 j = 2; j < 22; ++j)
      p->set_probability(i, j-2, utils::from_string<double>(tokens[j]));
    s->fractions(i, utils::from_string<double>(tokens[24]), utils::from_string<double>(tokens[25]),
        utils::from_string<double>(tokens[26]));
    ++i;
  }
  if(p->length()!=size) {
    l << utils::LogLevel::SEVERE << "Declared template length doesn't match the actual number of residues\n";
    return nullptr;
  }

  return std::make_shared<Target>(target_name, p, s);
}

const Target_SP Target::from_file(const std::string & filename) {

  std::ifstream in(filename);
  static utils::Logger l("Target");
  if (!in) {
    l << utils::LogLevel::SEVERE << "Can't read target data from a file: " << filename
        << "\n\t\tnull pointer returned!\n";
    return nullptr;
  }
  l << utils::LogLevel::FINE << "Loading a threading target from a file: " << filename << "\n";
  return Target::from_stream(in);
}

}
}

