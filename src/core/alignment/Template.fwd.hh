#ifndef CORE_ALIGNMENT_Template_FWD_H
#define CORE_ALIGNMENT_Template_FWD_H

#include <memory>

namespace core {
namespace alignment {

class Template;
typedef std::shared_ptr<Template> Template_SP;

}
}

#endif
