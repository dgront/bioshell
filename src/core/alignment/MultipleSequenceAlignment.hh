#ifndef CORE_ALIGNMENT_MultipleSequenceAlignment_H
#define CORE_ALIGNMENT_MultipleSequenceAlignment_H

#include <vector>
#include <string>
#include <iostream>

#include <core/alignment/MultipleAlignment.hh>
#include <core/data/sequence/Sequence.hh>
#include <core/data/sequence/sequence_utils.hh>

namespace core {
namespace alignment {

/** @brief Multiple sequence alignment object.
 *
 * Holds multiple alignment along with the aligned sequences
 */
class MultipleSequenceAlignment {
public:

  /** @brief Returns the number of aligned objects (sequences, structures, etc.).
   */
  inline int count_rows() const { return aligned.count_rows(); }

  /** @brief Creates a new Sequence object and adds it pointer to this multiple alignment
   */
  inline void add_row(const std::string &id, const std::string &aligned_sequence, const core::index2 first_pos) {
    aligned.add_row(id, aligned_sequence, first_pos);
    core::data::sequence::Sequence_SP ungapped = std::make_shared<core::data::sequence::Sequence>(id, aligned_sequence, first_pos);
    aligned_seqences.emplace(id, aligned_sequence);
    just_seqences.emplace(id, ungapped);
  }

  /** @brief Adds a given aligned sequence to this multiple sequence alignment.
   *
   * Note that the inserted sequence must be aligned with all the other sequences already inserted.
   */
  inline void add_row(const std::string &id, const core::data::sequence::Sequence_SP &aligned_sequence) {
    aligned.add_row(id, aligned_sequence->sequence, aligned_sequence->first_pos());
    core::data::sequence::Sequence_SP ungapped = aligned_sequence->create_ungapped_sequence();
    just_seqences.emplace(id, ungapped);
    aligned_seqences.emplace(id, aligned_sequence->sequence);
  }

  /** @brief  Returns an alignment row identified by a given string.
 *
 * Exception will be thrown if the given string is not a valid key
 * @param row_id - identifies a row in the alignment
 */
  inline const AlignmentRow & get_row(const std::string & row_id) const { return aligned.get_row(row_id); }

  /** @brief Returns an alignment column as a string
   *
   * Exception will be thrown if the given string is not a valid key
   * @param row_id - identifies a row in the alignment
   */
  inline std::string get_column(const core::index4 & which_column) const {

    std::string col;
    for(const auto & key_val : aligned_seqences) col += key_val.second[which_column];

    return col;
  }

  /** @brief  Returns a sequence identified by a given string.
   *
   * Exception will be thrown if the given string is not a valid key
   * @param row_id - identifies a sequence in the alignment
   * @return an original (unaligned) sequence
   */
  inline const core::data::sequence::Sequence_SP get_sequence(const std::string & row_id) const { return just_seqences.at(row_id); }

  /** @brief  Returns an aligned sequence identified by a given string.
   *
   * Exception will be thrown if the given string is not a valid key
   * @param row_id - identifies a sequence in the alignment
   * @return an aligned sequence, as it appear in the alignment
   */
  inline const std::string & get_aligned_sequence(const std::string & row_id) const { return aligned_seqences.at(row_id); }

  /** @brief Begins iteration over Ids for alignment rows
   *
   * @return a const iterator pointing at the very first AlignmentRow object contained in this multiple sequence alignment
   */
  std::vector<std::string>::const_iterator cbegin() const { return aligned.cbegin(); }

  /** @brief Ends iteration over Ids for alignment rows
   *
   * @return a pass-the-end const iterator pointing behind the very last AlignmentRow object contained in this multiple sequence alignment
   */
  std::vector<std::string>::const_iterator cend() const { return aligned.cend(); }

private:
  MultipleAlignment aligned;
  std::map<std::string,core::data::sequence::Sequence_SP> just_seqences;
  std::map<std::string,std::string> aligned_seqences;
};


} // ~alignment
} // ~core

#endif
