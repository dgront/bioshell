#include <iostream>
#include <memory>
#include <regex>

#include <core/alignment/Template.hh>

namespace core {
namespace alignment {

std::ostream & operator<<(std::ostream & out, const Template & t) {

  out << "# " << t.name << " : " << t.profile->length() << " residues\n";
  out << t.profile->sequence<<"\n";
  out << t.ss->str()<<"\n";
  for (index2 i = 0; i < t.profile->length(); ++i) {
    out << utils::string_format("%4d ", i + t.profile->first_pos()) << t.profile->operator [](i);
    const  std::vector<float> & p = t.profile->get_probabilities(i);
    out << utils::string_format(" %5.3f", p);
    out << " " << t.ss->ss(i) << utils::string_format(" %5.3f", t.ss->fraction_H(i))
        << utils::string_format(" %5.3f", t.ss->fraction_E(i)) << utils::string_format(" %5.3f", t.ss->fraction_C(i));
    if (t.has_coordinates(i)) {
      core::data::structural::Residue_SP r = t.get_residue(i);
      core::data::structural::PdbAtom_SP n = r->find_atom(" N  ");
      core::data::structural::PdbAtom_SP ca = r->find_atom(" CA ");
      core::data::structural::PdbAtom_SP c = r->find_atom(" C  ");
      core::data::structural::PdbAtom_SP o = r->find_atom(" O  ");
      core::data::structural::PdbAtom_SP cb = r->find_atom(" CB ");
      const char icode = (r->icode()==' ') ? '_' : r->icode();
      out << utils::string_format(" %4s %3d %c", r->owner()->id().c_str(), r->id(), icode);
      out << utils::string_format(" %8.3f %8.3f %8.3f", n->x, n->y, n->z);
      out << utils::string_format(" %8.3f %8.3f %8.3f", ca->x, ca->y, ca->z);
      out << utils::string_format(" %8.3f %8.3f %8.3f", c->x, c->y, c->z);
      out << utils::string_format(" %8.3f %8.3f %8.3f", o->x, o->y, o->z);
      if (cb != nullptr) out << utils::string_format(" %8.3f %8.3f %8.3f", cb->x, cb->y, cb->z);
    }
    out<<"\n";
  }
  return out;
}

Template::Template(const std::string & name, const SequenceProfile_SP template_profile,
                   const SecondaryStructure_SP template_ss, Chain_SP & template_residues) :
  Target(name, template_profile,template_ss),template_residues(template_residues), logger("Template") {}

const Template_SP Template::from_file(const std::string & filename) {

  std::ifstream in(filename);
  static utils::Logger l("Template");
  if (!in) {
    l << utils::LogLevel::SEVERE << "Can't read template data from a file: " << filename << "\n\t\tnull pointer returned!\n";
    return nullptr;
  }
  l << utils::LogLevel::FINE << "Loading a threading template from a file: " << filename << "\n";
  return from_stream(in);
}

const Template_SP Template::from_stream(std::istream & in) {

  static utils::Logger l("Template");
  if (!in) {
    l << utils::LogLevel::SEVERE << "Can't read template data from a stream!\n";
    return nullptr;
  }

  std::string line;
  std::getline(in, line);
  std::smatch matches;

  if(!std::regex_match(line,matches,std::regex("# (.+) : (\\d+) residues"))) {
    l << utils::LogLevel::SEVERE << "Incorrect file header!\n";
    return nullptr;
  }

  std::string template_name = matches[1];
  core::index2 size = utils::from_string<core::index2>(matches[2]);
  std::string seq, ss;
  std::getline(in, seq);
  std::getline(in, ss);
  core::data::structural::Chain_SP structure_sp= std::make_shared<core::data::structural::Chain>("A");
  core::data::structural::Chain & structure = *structure_sp;

//  std::vector<Residue_SP> structure;
  core::data::sequence::SequenceProfile_SP p = std::make_shared<core::data::sequence::SequenceProfile>(template_name, seq,
      core::data::sequence::SequenceProfile::aaOrderByNCBI());
  core::data::sequence::SecondaryStructure_SP s = std::make_shared<core::data::sequence::SecondaryStructure>(template_name, seq,
      0, ss);

  core::index2 i = 0;

  core::index2 i_atom = 0;
  std::vector<std::string> tokens;
  core::data::structural::Chain_SP chn = nullptr;
  std::string last_chain_id = 0;
  while (std::getline(in, line)) {
    if (line[0] == '#' || line.length() < 100) continue;
    tokens.clear();
    utils::split(line, tokens, ' ');

    // ---------- extract amino acid probabilities
    for (index2 j = 2; j < 22; ++j)

      p->set_probability(i, j - 2, utils::from_string<double>(tokens[j]));

    // ---------- extract secondary probabilities
    s->fractions(i, utils::from_string<double>(tokens[24]), utils::from_string<double>(tokens[25]),
        utils::from_string<double>(tokens[26]));

    // ---------- extract chain id, residue id, icode
    if (tokens[27] != last_chain_id) {
      last_chain_id = tokens[27];
      chn = std::make_shared<core::data::structural::Chain>(last_chain_id);
    }
    short int res_id = utils::from_string<short int>(tokens[28]);
    // ---------- extract atom coordinates, if found
    core::data::structural::Residue_SP res = std::make_shared<core::data::structural::Residue>(res_id,
        core::chemical::Monomer::get(tokens[1][0]));
    if (tokens[29][0] != '_') res->icode(tokens[29][0]);
    res->owner(chn);
    structure.push_back(res);
    double x = utils::from_string<double>(tokens[30]);
    double y = utils::from_string<double>(tokens[31]);
    double z = utils::from_string<double>(tokens[32]);
    res->push_back(std::make_shared<PdbAtom>(++i_atom, " N  ", x, y, z));
    x = utils::from_string<double>(tokens[33]);
    y = utils::from_string<double>(tokens[34]);
    z = utils::from_string<double>(tokens[35]);
    res->push_back(std::make_shared<PdbAtom>(++i_atom, " CA ", x, y, z));
    x = utils::from_string<double>(tokens[36]);
    y = utils::from_string<double>(tokens[37]);
    z = utils::from_string<double>(tokens[38]);
    res->push_back(std::make_shared<PdbAtom>(++i_atom, " C  ", x, y, z));
    x = utils::from_string<double>(tokens[39]);
    y = utils::from_string<double>(tokens[40]);
    z = utils::from_string<double>(tokens[41]);
    res->push_back(std::make_shared<PdbAtom>(++i_atom, " O  ", x, y, z));
    if (tokens.size() == 45) {
      x = utils::from_string<double>(tokens[42]);
      y = utils::from_string<double>(tokens[43]);
      z = utils::from_string<double>(tokens[44]);
      res->push_back(std::make_shared<PdbAtom>(++i_atom, " CB ", x, y, z));
    }
    ++i;
  }

  if(p->length()!=size) {
    l << utils::LogLevel::SEVERE << "Declared template length doesn't match the actual number of residues\n";
    return nullptr;
  }
  return std::make_shared<Template>(template_name, p, s, structure_sp);
}

void load_templates(const std::vector<std::string> & fnames, std::vector<Template_SP> & templates) {

  static utils::Logger l("Template");

  for (const std::string & fname : fnames) {
    Template_SP t = Template::from_file(fname);
    if (t != nullptr) templates.push_back(t);
    else l << utils::LogLevel::WARNING << "Template " << fname << " rejected!\n";
  }
}

}
}
