#ifndef CORE_ALIGNMENT_NWAligner_H
#define CORE_ALIGNMENT_NWAligner_H

#include <cmath>
#include <vector>
#include <limits.h>

#include <core/index.hh>

#include <core/data/basic/Array2D.hh>
#include <core/alignment/AbstractAligner.hh>
#include <core/alignment/PairwiseAlignment.hh>

namespace core {
namespace alignment {

/** \brief Computes global alignment of two sequences with affine gap penalty.
 *
 * Templated Needleman–Wunsch algorithm implementation.
 * \include ap_NWAligner.cc
 *
 * @tparam T  - primitive data type used to represent alignment scores, e.g. <code>float</code> or <code>short</code>
 * @tparam Op - call operator used to calculate a score for a match between two positions
 * @see SequenceNWAligner class can be used to align amino acid sequences with a substitution matrix score
 */
template<typename T, typename Op>
class NWAligner : public AbstractAligner<T, Op> {
public:
  /** @brief Create an aligner object that will calculate global pairwise sequence alignments.
   *
   * The longest sequence must be at most <code>max_size</code> residues long.
   * @param max_size - the length of the longest aligned sequence.
   */
  NWAligner(const unsigned  int max_size) : max_size(max_size), H(max_size+1), E(max_size+1), F(max_size+1) , H_prev(max_size+1), E_prev(max_size+1), F_prev(max_size+1) {
    query_size = tmplt_size = 0;
  }

  /// Default virtual destructor
  virtual ~NWAligner() = default;

  /** @brief Calculates global sequence alignment using Needleman-Wunsch algorithm.
   *
   * This is a very general method. The whole knowledge about the target and query proteins is encoded by the scoring operator type.
   * Once this call is finished, the resulting alignment may be obtained by calling backtrace() method.
   * @param gap_open - penalty for opening a new gap; must be negative.
   * @param gap_extend - penalty for extending an existing gap; must be negative.
   * @param scoring - instance of a class that provides three methods:
   *     - tmplt_length() - the number of positions in the template sequence
   *     - query_length() - the number of positions in the query sequence
   *     - call operator operating on two integer indexes, which provides a score for matching the two respective positions
   *     See \ref doc/web_examples/ex_SemiglobalAligner.cc "doc/web_examples/ex_SemiglobalAligner.cc"  demo program for an example similarity scoring function
   * @tparam T - type used to represents scores
   * @tparam Op - scoring system class
   * @return alignment score.
   */
  T align(const T gap_open, const T gap_extend, const Op & scoring);

  /** @brief Calculates the score of the global optimal sequence alignment using Needleman-Wunsch algorithm.
   *
   * This method is very similar to align(const T,const  T,const Op &), but it <strong>does not store</strong>
   * information for backtracing; alignment will not be available. This method however is much faster that
   * align(const T,const  T,const Op &).
   */
  T align_for_score(const T gap_open,const  T gap_extend,const Op & scoring);

  /** @brief Backtraces the alignment based on information sored by the most recent align(const T,const  T,const Op &) call.
   */
  PairwiseAlignment_SP backtrace();

  /// Returns the score found by the most recent call of align(const T,const  T,const Op &) or align_for_score(const T,const  T,const Op &)
  inline T recent_score() const { return recent_score_; }

private:
  T recent_score_ = 0;
  const unsigned  int max_size;
  index2 query_size;
  index2 tmplt_size;
  std::vector<T> H; ///< The alignment matrix - the row being calculated
  std::vector<T> E; ///< Gotoh's helper matrix for the alignments ending with a gap in the query - the row being calculated (horizontal move)
  std::vector<T> F; ///< Gotoh's helper matrix for the alignments ending with a gap in the template - the row being calculated (vertical move)
  std::vector<T> H_prev; ///< The alignment matrix - the very previous row
  std::vector<T> E_prev; ///< Gotoh's helper matrix for the alignments ending with a gap in the query - the very previous row
  std::vector<T> F_prev; ///< Gotoh's helper matrix for the alignments ending with a gap in the template - the very previous row
  std::shared_ptr<core::data::basic::Array2D<unsigned char>> arrows_ = nullptr;
  std::shared_ptr<core::data::basic::Array2D<unsigned short>> E_arrows_ = nullptr;
  std::shared_ptr<core::data::basic::Array2D<unsigned short>> F_arrows_ = nullptr;
  std::vector<char> back_trace;
  static const T not_an_option;
};

template<typename T, typename Op>
const T NWAligner<T,Op>::not_an_option = -std::numeric_limits<T>::max() /2;

template<typename T, typename Op>
T NWAligner<T,Op>::align(const T gap_open,const  T gap_extend,const Op & scoring) {

  query_size = scoring.query_length();
  tmplt_size = scoring.tmplt_length();

  if(arrows_ == nullptr) {
    arrows_ = std::make_shared<core::data::basic::Array2D<unsigned char>>(max_size + 1, max_size + 1);
    E_arrows_ = std::make_shared<core::data::basic::Array2D<unsigned short>>(max_size + 1, max_size + 1);
    F_arrows_ = std::make_shared<core::data::basic::Array2D<unsigned short>>(max_size + 1, max_size + 1);
  }
  E_arrows_->clear(1); // open a new gap everywhere
  F_arrows_->clear(1); // open a new gap everywhere

  // ---------- Initialize for tail gaps penalized
  T tmp = gap_open;
  H[0] = 0;
  recent_score_ = not_an_option;
  E[0] = F[0] = not_an_option;
  for (index2 j = 1; j <= scoring.tmplt_length(); ++j) {
    F[j] = not_an_option;
    H[j] = E[j] = tmp;
    E_arrows_->set(0, j, j);
    tmp += gap_extend;
    arrows_->set(0, j, 1); // horizontal
  }
  T gap_started_in_q = gap_open;
  for (index2 i = 1; i <= scoring.query_length(); ++i) {
    H.swap(H_prev);
    E.swap(E_prev);
    F.swap(F_prev);
    arrows_->set(i, 0, 4); // vertical
    F_arrows_->set(i, 0, i);
    H[0] = F[0] = gap_started_in_q;
    E[0] = not_an_option;
    for (index2 j = 1; j <= scoring.tmplt_length(); ++j) {
      E[j] = std::max(E[j - 1] + gap_extend, std::max(H[j - 1] + gap_open, F[j - 1] + gap_open));
      if (E[j] - E[j - 1] == gap_extend)
        E_arrows_->set(i, j, E_arrows_->get(i, j - 1) + 1);

      F[j] = std::max(F_prev[j] + gap_extend, std::max(H_prev[j] + gap_open, E_prev[j] + gap_open));
      if (F[j] - F_prev[j] == gap_extend)
        F_arrows_->set(i, j, F_arrows_->get(i - 1, j) + 1);

      const T h = T(H_prev[j - 1] + (scoring)(i - 1, j - 1)); // --- the score of a match at i,j
      H[j] = std::max(h, std::max(E[j], F[j]));
      index1 arrow_flag = 0;
      if (H[j] == E[j]) arrow_flag += 1;
      if (H[j] == h) arrow_flag += 2;
      if (H[j] == F[j]) arrow_flag += 4;
      arrows_->set(i, j, arrow_flag);
    }
    gap_started_in_q += gap_extend;
  }

  recent_score_ = H[scoring.tmplt_length()];

  return recent_score_;
}

template<typename T, typename Op>
T NWAligner<T,Op>::align_for_score(const T gap_open,const  T gap_extend,const Op & scoring) {

  query_size = scoring.query_length();
  tmplt_size = scoring.tmplt_length();
  // ---------- Initialize for tail gaps penalized
  T tmp = gap_open;
  H[0] = 0;
  E[0] = F[0] = not_an_option;
  for(index2 j=1;j<=scoring.tmplt_length();++j) {
    F[j] =  not_an_option;
    H[j] = E[j] = tmp;
    tmp += gap_extend;
  }

  T gap_started_in_q = gap_open;
  for(index2 i=1;i<=scoring.query_length();++i) {
    H.swap(H_prev);
    E.swap(E_prev);
    F.swap(F_prev);
    H[0] = F[0] = gap_started_in_q;
    E[0] = not_an_option;
    for(index2 j=1;j<=scoring.tmplt_length();++j) {
      E[j] = std::max(E[j - 1] + gap_extend, std::max(H[j - 1] + gap_open, F[j - 1] + gap_open));
      F[j] = std::max(F_prev[j] + gap_extend, std::max(H_prev[j] + gap_open, E_prev[j] + gap_open));
      H[j] = std::max(T(H_prev[j - 1] + (scoring)(i - 1, j - 1)), std::max(E[j], F[j]));
    }
    gap_started_in_q += gap_extend;
  }

  recent_score_ = H[scoring.tmplt_length()];

  return H[scoring.tmplt_length()];
}

template<typename T, typename Op>
PairwiseAlignment_SP NWAligner<T,Op>::backtrace() {

  PairwiseAlignment_SP a = std::make_shared<PairwiseAlignment>(0, 0, double(recent_score_));

  if (back_trace.size() == 0) back_trace.resize(max_size + max_size);

  index2 i = query_size, j = tmplt_size;
  int k = -1;

  while (i > 0 || j > 0) {
    if((arrows_->operator()(i, j) & 2) != 0) {
      back_trace[++k] = 2;
//      std::cerr << int(arrows_->operator()(i, j)) << " " << i << " " << j << " " << k << " " << int(back_trace[k]) << "\n";
      i--;
      j--;
      continue;
    }
    if ((arrows_->operator()(i, j) & 1) != 0) {
      core::index2 n_gaps = E_arrows_->get(i, j);
      for (index2 kk = 0; kk < n_gaps; ++kk) {
        back_trace[++k] = 1;
        j--;
      }
      continue;
    }
    core::index2 n_gaps = F_arrows_->get(i, j);
    for (index2 kk = 0; kk < n_gaps; ++kk) {
      back_trace[++k] = 4;
      i--;
    }
  }
  while(k>=0) {
    if(back_trace[k]==2) {
      a->append_match();
      --k;
      continue;
    }
    if(back_trace[k]==1) {
      a->append_query_gap();
      --k;
      continue;
    }
    if(back_trace[k]==4) {
      a->append_template_gap();
      --k;
      continue;
    }
  }

  return a;
}

}
}
/**
 * \example ap_NWAligner.cc
 */
#endif

