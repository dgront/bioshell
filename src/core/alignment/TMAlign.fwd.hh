/** @file TMAlign.fwd.hh
 *  @brief Declares TMAlign and TMAlign_SP types
 */
#ifndef CORE_ALIGNMENT_TMalign_FWD_HH
#define CORE_ALIGNMENT_TMalign_FWD_HH

#include <memory>

namespace core {
namespace alignment {

class TMAlign;

/// Define a shared pointer to TMAlign object as a distinct type
typedef std::shared_ptr<TMAlign> TMAlign_SP;
}
}

#endif


