#include <core/alignment/FilterByHighlyGappedColumns.hh>

namespace core {
namespace alignment {

void FilterByHighlyGappedColumns::run(core::index2 parameter) {
  calc_highly_gapped_positions();

  std::vector<core::data::sequence::Sequence_SP> sequences_to_erase;
  core::index2 n_erased = 0;
  for (core::index2 it : highly_gapped_for_sequence_) {
    if (it >= parameter) {
      msa_.erase(std::remove(msa_.begin(), msa_.end(), msa_[it]), msa_.end());
      ++n_erased;
    }
  }
  logs << utils::LogLevel::INFO << n_erased << " sequences removed due to filtering\n";
}

}
}

