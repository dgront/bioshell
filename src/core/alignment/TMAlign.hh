#ifndef CORE_ALIGNMENT_TMalign_H
#define CORE_ALIGNMENT_TMalign_H

#include <utility>
#include <chrono>
#include <vector>
#include <limits.h>

#include <core/index.hh>

#include <core/data/basic/Vec3.hh>
#include <core/data/basic/BoundedPriorityQueue.hh>

#include <core/alignment/NWAligner.hh>
#include <core/alignment/PairwiseAlignment.hh>

#include <core/calc/structural/transformations/TMScore.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>

#include <utils/Logger.hh>

namespace core {
namespace alignment {

using namespace core::data::basic;
using namespace core::calc::structural::transformations;

/** \brief Aligns two structures (sets of atoms) in a way similar to tm-align algorithm.
 *
 * @see Zhang, Skolnick, "TM-align: a protein structure alignment algorithm based on the TM-score", NAR 33 2302-2309 2005
 */
class TMAlign {
public:

  /** \brief Creates the aligner for two sets of coordinates.
   *
   * The actual alignment is computed by <code>align()</code> method.
   *
   * @param query - coordinates of the query structure that will be superimposed on the template
   * @param tmplt - template coordinates
   */
  TMAlign(const Coordinates & query, const Coordinates & tmplt);

  /** \brief Computes the structural alignment.
   *
   */
  double align();

  /// Returns the optimal rotation-translation transformation found during the most recent <code>align()</code> call
  const Rototranslation & recent_tmscore_transformation() const { return tms; }

  /// Returns the tm-score value resulting from the most recent <code>align()</code> call
  double recent_tmscore() const { return recent_raw_tmscore_/double(tmplt_to_align.size()); }

  /// Returns the tm-score value resulting from the most recent <code>align()</code> call
  double recent_tmscore(const index2 reference_length) const { return recent_raw_tmscore_/(double(reference_length)); }

  /// Returns the structure alignment found during the most recent <code>align()</code> call
  PairwiseAlignment_SP recent_alignment() const { return recent_alignment_; }

  /// Returns the reference to the query coordinates being aligned
  const Coordinates & query_to_be_aligned() const { return query_to_align; }

  /// Returns the reference to the template coordinates being aligned
  const Coordinates & tmplt_to_be_aligned() const { return tmplt_to_align; }

private:
  typedef std::pair<double, Rototranslation_SP> Seed;
  typedef std::function<bool(const Seed &,const Seed &)> SeedComparatorType;

  const Coordinates & tmplt_to_align;
  const Coordinates & query_to_align;
  Coordinates tmplt_;  ///< coordinates of the aligned template atoms
  Coordinates query_;  ///< coordinates of the aligned query atoms
  PairwiseAlignment_SP recent_alignment_;
  double recent_raw_tmscore_;
  index2 nw_iterations_cnt;
  TMScore tms;        ///< Stores the most recent RT, but NOT the best tmscore value!
  const double LOCAL_SEED_CUTOFF;
  const double EXTENDING_RADIUS;
  const core::index2 EXTENDING_ITERATIONS;
  const core::index2 MAX_NW_ITERATIONS;
  utils::Logger l;
  core::data::basic::BoundedPriorityQueue<Seed, SeedComparatorType,SeedComparatorType> seeds;


  /** \brief Finds the initial alignment seed.
   *
   * This method goes through all matches of seven residues. Each high scoring match is extended as follows:
   *     - rotation matrix is established based on the match of seven residue pairs
   *     - all atoms are transformed by the above RT
   *     - new aligned pairs that are included if they are closer than a cutoff; tm-score is evaluated (this step is by TMScore::extend())
   * The best initial alignment (i.e. giving the highest tm-score for any seed of seven pairs) is stored in this object
   *
   * In the next step of alignment, <code>nw_iteration()</code> method is used to find the alignment
   */
  double init_from_fragments();

  /** \brief Performs one DP iteration starting from the most recent alignment matrix.
   *
   * The first iteration is to be initiated by  <code>init_from_fragments()</code>
   */
  double nw_iteration();
};

}
}

#endif


