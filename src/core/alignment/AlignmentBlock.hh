/** @file AlignmentBlock.hh
 * Provides data type representing a single contiguous block of a sequence alignment
 */
#ifndef CORE_ALIGNMENT_AlignmentBlock_H
#define CORE_ALIGNMENT_AlignmentBlock_H

#include <memory>   // for shared_ptr
#include <iostream>

#include <core/index.hh>
#include <utils/string_utils.hh>

namespace core {
namespace alignment {

/** @brief Represents a contiguous (i.e. gapless) block of an alignment.
 *
 * Any alignment may be written as a combination of gapless aligned blocks linked by stretches of sequence fragments aligned with gaps
 */
struct AlignmentBlock {

  core::index2 first_query_pos; ///< Index of the first residue of a query sequence included in this block
  core::index2 last_query_pos; ///< Index of the last residue of a query sequence included in this block
  core::index2 first_tmplt_pos; ///< Index of the first residue of a template sequence included in this block
  core::index2 last_tmplt_pos; ///< Index of the last residue of a template sequence included in this block

  /** @brief Creates a new block.
   *
   * @param first_q - index of the first query residue
   * @param last_q - index of the last query residue
   * @param first_t - index of the first query template
   * @param last_t - index of the last query template
   */
  AlignmentBlock(const core::index2 first_q, const core::index2 last_q, const core::index2 first_t,
                 const core::index2 last_t) {
    first_query_pos = first_q;
    first_tmplt_pos = first_t;
    last_query_pos = last_q;
    last_tmplt_pos = last_t;
  }

  /// Calculate the number of aligned residues in this block
  core::index2 size() const { return last_tmplt_pos - first_tmplt_pos + 1; }

  /** @brief Returns true if this block is located on the same diagonal of an alignment matrix as the given <code>another_block</code>
   * @param another_block - another block of the same alignment
   * @return true if the two blocks are located on the same alignment diagonal
   */
  bool are_same_diagonal(const AlignmentBlock &another_block) const;

  /** @brief Merges another block into this block.
   *
   * After this merge, this block will cover the two blocks: this and the given <code>another_block</code>
   * and all the residues in between the two blocks. The merge is executed if and only if the number of residues
   * separating the two blocks is not greater to the given value
   *
   * @param another_block - a block to be merged into this block
   * @param max_distance - the maximum number of residues that can separate two alignment blocks being merged
   * @return true if the two blocks were actualy merged
   */
  bool combine_with(const AlignmentBlock &another_block, const index2 max_distance = 65535);

  /** @brief Retrieves the fragment of a query sequence that is included in this block
   * @param full_query_sequence - the full, ungapped query sequence
   * @return the relevant fragment of a query sequence
   */
  std::string query_sequence(const std::string &full_query_sequence) const {
    return full_query_sequence.substr(first_query_pos, last_query_pos - first_query_pos + 1);
  }

  /** @brief Retrieves the fragment of a template sequence that is included in this block
   * @param full_tmplt_sequence - the full, ungapped template sequence
   * @return the relevant fragment of a template sequence
   */
  std::string template_sequence(const std::string &full_tmplt_sequence) const {
    return full_tmplt_sequence.substr(first_tmplt_pos, last_tmplt_pos - first_tmplt_pos + 1);
  }

  /** @brief Counts identical residues within this alignment block.
   *
   * This method extracts the respective fragments of the query and the template sequences that are contained
   * in this block of alignment and counts identical letters by calling <code>core::alignment::sum_identical()</code> function
   * @param full_query_sequence - the full query sequence
   * @param full_tmplt_sequence - the full template sequence
   * @return the number of identical positions (letters)
   */
  core::index2 sum_identical(const std::string &full_query_sequence, const std::string &full_tmplt_sequence) const;

  /** @brief Counts sequence identity fraction for this alignment block.
   *
   * This method counts the number of identical positions by calling <code>sum_identical()</code> method
   * and divides the result by the length of this block
   * @param full_query_sequence - the full query sequence
   * @param full_tmplt_sequence - the full template sequence
   * @return sequence identity fraction
   */
  double fraction_identical(const std::string &full_query_sequence, const std::string &full_tmplt_sequence) const;

};

/// shared pointer to AlignmentBlock object
typedef std::shared_ptr<AlignmentBlock> AlignmentBlock_SP;

/** @brief Writes an AlignmentBlock object into a stream
 *
 * @param out - output stream
 * @param selector -selector object
 * @return reference to the output stream
 */
std::ostream & operator<<(std::ostream & out, const AlignmentBlock & block);

/** @brief Writes an AlignmentBlock object into a stream
 *
 * @param out - output stream
 * @param selector -selector object
 * @return reference to the output stream
 */
std::ostream & operator<<(std::ostream & out, const AlignmentBlock_SP block);


}
}

#endif
