#include <core/index.hh>
#include <utils/string_utils.hh>
#include <core/alignment/AlignmentBlock.hh>
#include <core/alignment/on_alignment_computations.hh>

namespace core {
namespace alignment {

std::ostream &operator<<(std::ostream &out, const AlignmentBlock &block) {

  out << utils::string_format("%4d %4d %4d %4d\n", block.first_query_pos, block.last_query_pos, block.first_tmplt_pos,
                              block.last_tmplt_pos);

  return out;
}


std::ostream &operator<<(std::ostream &out, const AlignmentBlock_SP block) {

  out << utils::string_format("%4d %4d %4d %4d\n", block->first_query_pos, block->last_query_pos,
                              block->first_tmplt_pos, block->last_tmplt_pos);

  return out;
}

bool AlignmentBlock::combine_with(const AlignmentBlock & another_block, const index2 max_distance) {

  if(!are_same_diagonal(another_block)) return false;

  if (first_query_pos < another_block.first_query_pos) { // |this block| --- |another block|
    if ((another_block.first_query_pos > last_query_pos) &&
        (another_block.first_query_pos - last_query_pos > max_distance)) return false;
    last_query_pos = another_block.last_query_pos;
    last_tmplt_pos = another_block.last_tmplt_pos;
  } else { // |another block| -- |this block|
    if ((first_query_pos > another_block.last_query_pos) &&
        (first_query_pos - another_block.last_query_pos > max_distance)) return false;
    first_query_pos = another_block.first_query_pos;
    first_tmplt_pos = another_block.first_tmplt_pos;
  }

  return true;
}

bool AlignmentBlock::are_same_diagonal(const AlignmentBlock &another_block) const {

  return ((first_query_pos-first_tmplt_pos) == (another_block.first_query_pos-another_block.first_tmplt_pos));
}

core::index2
AlignmentBlock::sum_identical(const std::string &full_query_sequence, const std::string &full_tmplt_sequence) const {

  return core::alignment::sum_identical(query_sequence(full_query_sequence), template_sequence(full_tmplt_sequence));
}

double
AlignmentBlock::fraction_identical(const std::string &full_query_sequence, const std::string &full_tmplt_sequence) const {

  return core::alignment::sum_identical(query_sequence(full_query_sequence), template_sequence(full_tmplt_sequence)) /
         double(size());
}


}
}
