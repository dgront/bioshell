#ifndef CORE_ALIGNMENT_SemiglobalAligner_H
#define CORE_ALIGNMENT_SemiglobalAligner_H

#include <cmath>
#include <vector>
#include <limits.h>

#include <core/index.hh>

#include <core/data/basic/Array2D.hh>
#include <core/alignment/AbstractAligner.hh>
#include <core/alignment/PairwiseAlignment.hh>
#include <core/alignment/scoring/SimilarityMatrixScore.hh>

namespace core {
namespace alignment {

/** \brief Computes global alignment of two sequences with affine gap penalty and cost-free-ends.
 *
 * The following example aligns two sequences and prints the alignment on stdout. Note, now a sequence similarity scoring object is created:
 * \include ex_SemiglobalAligner.cc
 *
 * The resulting alignment should be:
 * <code><pre>
# score: 5 length: 15
>query
CATACGTCGACGGCT
>tmplt
---ACGACGT-----
</pre></code>
 * The score of this alignment is 5 because there are five matches and gaps come for free
 *
 * @tparam T  - primitive data type used to represent alignment scores, e.g. <code>float</code> or <code>short</code>
 * @tparam Op - call operator used to calculate a score for a match between two positions
 */
template<typename T, typename Op>
class SemiglobalAligner : public AbstractAligner<T, Op> {
public:

  /** @brief Create an aligner object that will calculate global pairwise sequence alignments with cost-free end gaps.
   *
   * This aligner works very similarly to NWAligner; the only difference is that this class does
   * not penalize for end gaps in an alignment. This might be useful when one of the two aligned sequences is much shorter than the other.
   *
   * The longest sequence must be at most <code>max_size</code> residues long.
   * @param max_size - the length of the longest aligned sequence.
   */
  SemiglobalAligner(const unsigned int max_size) :
      max_size(max_size), H(max_size + 1), E(max_size + 1), F(max_size + 1), H_prev(max_size + 1), E_prev(max_size + 1), F_prev(
          max_size + 1) { query_size = tmplt_size = 0; }

  /// Default virtual destructor
  virtual ~SemiglobalAligner() = default;

  /** @brief Calculates the score of the semiglobal sequence alignment.
   *
   * This is a very general method. The whole knowledge about the target and query proteins is encoded by the scoring operator type.
   * Once this call is finished, the resulting alignment may be obtained by calling backtrace() method.
   * @param gap_open - penalty for opening a new gap; must be negative.
   * @param gap_extend - penalty for extending an existing gap; must be negative.
   * @param scoring - instance of a class that provides three methods:
   *     - tmplt_length() - the number of positions in the template sequence
   *     - query_length() - the number of positions in the query sequence
   *     - call operator operating on two integer indexes, which provides a score for matching the two respective positions
   * @tparam T - type used to represents scores
   * @tparam Op - scoring system class
   * @return alignment score.
   */
  T align(const T gap_open,const  T gap_extend,const Op & scoring);

  /** @brief Calculates the score of the semiglobal sequence alignment
   *
   * This method is very similar to align(const T,const  T,const Op &), but it <strong>does not store</strong>
   * information for backtracing; alignment will not be available. This method however is much faster that
   * align(const T,const  T,const Op &).
   */
  T align_for_score(const T gap_open,const  T gap_extend,const Op & scoring);

  /** @brief Backtraces the alignment based on information sored by the most recent align(const T,const  T,const Op &) call.
   *
   */
  PairwiseAlignment_SP backtrace();

  /// Returns the score found by the most recent call of align(const T,const  T,const Op &) or align_for_score(const T,const  T,const Op &)
  inline T recent_score() const { return recent_score_; }

private:
  T recent_score_ = not_an_option;
  const unsigned  int max_size;
  unsigned int recent_i_ = 0;
  unsigned int  recent_j_ = 0;
  index2 query_size;
  index2 tmplt_size;
  std::vector<T> H; ///< The alignment matrix - the row being calculated
  std::vector<T> E; ///< Gotoh's helper matrix for the alignments ending with a gap in the query - the row being calculated (horizontal move)
  std::vector<T> F; ///< Gotoh's helper matrix for the alignments ending with a gap in the template - the row being calculated (vertical move)
  std::vector<T> H_prev; ///< The alignment matrix - the very previous row
  std::vector<T> E_prev; ///< Gotoh's helper matrix for the alignments ending with a gap in the query - the very previous row
  std::vector<T> F_prev; ///< Gotoh's helper matrix for the alignments ending with a gap in the template - the very previous row
  std::shared_ptr<core::data::basic::Array2D<unsigned char>> arrows_ = nullptr;
  std::shared_ptr<core::data::basic::Array2D<unsigned short>> E_arrows_ = nullptr;
  std::shared_ptr<core::data::basic::Array2D<unsigned short>> F_arrows_ = nullptr;
  std::vector<char> back_trace;
  static const T not_an_option;
};


template<typename T, typename Op>
const T SemiglobalAligner<T,Op>::not_an_option = -std::numeric_limits<T>::max() /2;

template<typename T, typename Op>
T SemiglobalAligner<T,Op>::align(const T gap_open,const  T gap_extend,const Op & scoring) {

  query_size = scoring.query_length();
  tmplt_size = scoring.tmplt_length();

  if(arrows_ == nullptr) {
    arrows_ = std::make_shared<core::data::basic::Array2D<unsigned char>>(max_size + 1, max_size + 1);
    E_arrows_ = std::make_shared<core::data::basic::Array2D<unsigned short>>(max_size + 1, max_size + 1);
    F_arrows_ = std::make_shared<core::data::basic::Array2D<unsigned short>>(max_size + 1, max_size + 1);
  }
  E_arrows_->clear(1); // open a new gap everywhere
  F_arrows_->clear(1); // open a new gap everywhere

  // ---------- Initialize for tail gaps penalized
  H[0] = 0;
  E[0] = F[0] = not_an_option;
  recent_score_ = not_an_option;

  for (index2 j = 1; j <= scoring.tmplt_length(); ++j) {
    F[j] = not_an_option;
    H[j] = E[j] = 0;
    arrows_->set(0, j, 1); // horizontal
    E_arrows_->set(0, j, j);
  }

  for (index2 i = 1; i <= scoring.query_length(); ++i) {
    H.swap(H_prev);
    E.swap(E_prev);
    F.swap(F_prev);
    arrows_->set(i, 0, 4); // vertical
    H[0] = F[0] = 0;
    E[0] = not_an_option;
    F_arrows_->set(i, 0, i);
//    size_t j_arrow = arrows->row_starts(i);
    for (index2 j = 1; j <= scoring.tmplt_length(); ++j) {
      // ---------- Exact, but a bit slower version
       E[j] = std::max(E[j - 1] + gap_extend, std::max(H[j - 1] + gap_open, F[j - 1] + gap_open));
      if (E[j] - E[j - 1] == gap_extend)
        E_arrows_->set(i, j, E_arrows_->get(i, j - 1)+1);

      F[j] = std::max(F_prev[j] + gap_extend, std::max(H_prev[j] + gap_open, E_prev[j] + gap_open));
      if (F[j] - F_prev[j] == gap_extend)
        F_arrows_->set(i, j, F_arrows_->get(i-1, j)+1);

      const T h = T(H_prev[j - 1] + (scoring)(i - 1, j - 1));

      H[j] = std::max(h, std::max(E[j], F[j]));
      index1 arrow_flag = 0;
      if (H[j] == E[j]) arrow_flag += 1;
      if (H[j] == h) arrow_flag += 2;
      if (H[j] == F[j]) arrow_flag += 4;
      arrows_->set(i, j, arrow_flag);
    }
    if (recent_score_ < H[scoring.tmplt_length()]) {
      recent_score_ = H[scoring.tmplt_length()];
      recent_j_ = scoring.tmplt_length();
      recent_i_ = i;
    }
  }
  for (index2 j = 1; j <= scoring.tmplt_length(); ++j) {
    if (recent_score_ < H[j]) {
      recent_score_ = H[j];
      recent_j_ = j;
      recent_i_ = scoring.query_length();
    }
  }

  return recent_score_;
}

template<typename T, typename Op>
T SemiglobalAligner<T,Op>::align_for_score(const T gap_open,const  T gap_extend,const Op & scoring) {

  query_size = scoring.query_length();
  tmplt_size = scoring.tmplt_length();
  // ---------- Initialize for tail gaps penalized
  H[0] = 0;
  E[0] = F[0] = not_an_option;
  for(index2 j=1;j<=scoring.tmplt_length();++j) {
    F[j] =  not_an_option;
    H[j] = E[j] = 0;
  }

  recent_score_ = not_an_option;
  for(index2 i=1;i<=scoring.query_length();++i) {
    H.swap(H_prev);
    E.swap(E_prev);
    F.swap(F_prev);
    H[0] = F[0] = 0;
    E[0] = not_an_option;
    for(index2 j=1;j<=scoring.tmplt_length();++j) {
      E[j] = std::max(E[j - 1] + gap_extend, std::max(H[j - 1] + gap_open, F[j - 1] + gap_open));
      F[j] = std::max(F_prev[j] + gap_extend, std::max(H_prev[j] + gap_open, E_prev[j] + gap_open));
//      E[j] = std::max(E[j - 1] + gap_extend, H[j - 1] + gap_open);
//      F[j] = std::max(F_prev[j] + gap_extend, H_prev[j] + gap_open);
      H[j] = std::max(T(H_prev[j - 1] + (scoring)(i - 1, j - 1)), std::max(E[j], F[j]));
    }
    if (recent_score_ < H[scoring.tmplt_length()]) recent_score_ = H[scoring.tmplt_length()];
  }
  for (index2 j = 1; j <= scoring.tmplt_length(); ++j)
    if (recent_score_ < H[j]) recent_score_ = H[j];

  return recent_score_;
}

template<typename T, typename Op>
PairwiseAlignment_SP SemiglobalAligner<T,Op>::backtrace() {

  PairwiseAlignment_SP a = std::make_shared<PairwiseAlignment>(0, 0, double(recent_score_));

  if (back_trace.size() == 0) back_trace.resize(max_size + max_size);
  index2 i = recent_i_, j = recent_j_;
  int k = -1;
  if(i==query_size) {
    for (index2 ii = tmplt_size; ii > recent_j_; --ii) {
      back_trace[++k] = 1;
    }
  }
  if(j==tmplt_size) {
    for (index2 ii = query_size; ii > recent_i_; --ii) {
      back_trace[++k] = 4;
    }
  }

  while (i > 0 || j > 0) {
    if((arrows_->operator()(i, j) & 2) != 0) {
      back_trace[++k] = 2;
      i--;
      j--;
      continue;
    }
    if ((arrows_->operator()(i, j) & 1) != 0) {
      core::index2 n_gaps = E_arrows_->get(i, j);
      for (index2 kk = 0; kk < n_gaps; ++kk) {
        back_trace[++k] = 1;
        j--;
      }
      continue;
    }
    core::index2 n_gaps = F_arrows_->get(i, j);
    for (index2 kk = 0; kk < n_gaps; ++kk) {
      back_trace[++k] = 4;
      i--;
    }
  }
  while(k>=0) {
    if(back_trace[k]==2) {
      a->append_match();
      --k;
      continue;
    }
    if(back_trace[k]==1) {
      a->append_query_gap();
      --k;
      continue;
    }
    if(back_trace[k]==4) {
      a->append_template_gap();
      --k;
      continue;
    }
  }

  return a;
}

}
}
/**
 * \example ex_SemiglobalAligner.cc
 */
#endif

