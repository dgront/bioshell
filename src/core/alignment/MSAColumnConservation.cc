#include <numeric>
#include <core/alignment/MSAColumnConservation.hh>
#include <core/protocols/SequenceWeightingProtocol.hh>


namespace core {
namespace alignment {

MSAColumnConservation::MSAColumnConservation(const MultipleSequenceAlignment &msa, const std::string & matrix_name) :
  background_counts(20), sim_m(core::alignment::scoring::NcbiSimilarityMatrixFactory::get().get_matrix(matrix_name)),
  tmp_cnts(20), logs("MSAColumnConservation") {

  for (auto seq_it = msa.cbegin(); seq_it != msa.cend(); ++seq_it) {
    weights.push_back(1.0);
    msa_.push_back( *seq_it);
  }

  initialize();
}

void MSAColumnConservation::initialize() {

  // --- Calculate sequence weights
  core::protocols::InverseIdentitySequenceWeights protocol;
  protocol.add_input_sequences(msa_);
  protocol.run();
  for (size_t i = 0; i < protocol.count_sequences(); ++i) weights[i] = protocol.get_weight(i);

  // --- Precalculate background total for each column
  for (size_t i_seq = 0; i_seq < msa_.size(); ++i_seq) {
    const std::string & s = msa_[i_seq];
    for (size_t ires = 0; ires < s.size(); ++ires) {
      index2 id = core::chemical::Monomer::get(s[ires]).id;
      if (id < 20) {
        background_counts[id] += weights[i_seq];
        tmp_total_weight += weights[i_seq];
      }
    }
  }
  for (size_t i = 0; i < background_counts.size(); ++i) background_counts[i] /= tmp_total_weight;

  logs << utils::LogLevel::INFO << "background amino acid frequency:\n"
       << "\t   A     R     N     D     C     Q     E     G     H     I     L     K     M     F     P     S     T     W     Y     V\n\t";

  double total = 0;
  for (index2 i = 0; i < 20; ++i) {
    logs << utils::string_format(" %5.3f", background_counts[i]);
    total += background_counts[i];
  }
  logs << "\n\tTotal = " << total << "\n";
}

MSAColumnConservation::MSAColumnConservation(const std::vector<std::string> & msa, const std::string & matrix_name) :
  background_counts(20), sim_m(core::alignment::scoring::NcbiSimilarityMatrixFactory::get().get_matrix(matrix_name)),
  tmp_cnts(20), logs("MSAColumnConservation") {

  for(const std::string & s : msa) {
    msa_.push_back(s);
    weights.push_back(1.0);
  }

  initialize();
}

MSAColumnConservation::MSAColumnConservation(const std::vector<core::data::sequence::Sequence_SP> & msa, const std::string & matrix_name) :
  background_counts(20), sim_m(core::alignment::scoring::NcbiSimilarityMatrixFactory::get().get_matrix(matrix_name)),
  tmp_cnts(20), logs("MSAColumnConservation") {

  for(const core::data::sequence::Sequence_SP s : msa) {
    msa_.push_back(s->sequence);
    weights.push_back(1.0);
  }

  initialize();
}


std::map<std::string,ColumnConservationScores> scoring_method_names = {
  {"SumOfPairs", ColumnConservationScores::SumOfPairs},
  {"Variation", ColumnConservationScores::Variation},
  {"ShannonEntropy", ColumnConservationScores::ShannonEntropy},
  {"RelativeEntropy", ColumnConservationScores::RelativeEntropy},
  {"JensenShannonDivergence", ColumnConservationScores::JensenShannonDivergence}
};

ColumnConservationScores MSAColumnConservation::score_by_name(const std::string &enum_name) {

  if(scoring_method_names.find(enum_name)!=scoring_method_names.end())
    return scoring_method_names[enum_name];

  throw std::invalid_argument("Unknown MSA score name: " + enum_name);
}

std::map<ColumnConservationScores, MSAColumnConservation::ScoreFunction> MSAColumnConservation::create_map() {

  std::map<ColumnConservationScores, ScoreFunction> tmp;
  tmp.insert(std::pair<ColumnConservationScores, ScoreFunction>(GapPercent, MSAColumnConservation::gap_percent));
  tmp.insert(std::pair<ColumnConservationScores, ScoreFunction>(ShannonEntropy, MSAColumnConservation::shannon_entropy));
  tmp.insert(std::pair<ColumnConservationScores, ScoreFunction>(RelativeEntropy, MSAColumnConservation::relative_entropy));
  tmp.insert(std::pair<ColumnConservationScores, ScoreFunction>(Variation, MSAColumnConservation::variation));
  tmp.insert(std::pair<ColumnConservationScores, ScoreFunction>(SumOfPairs, MSAColumnConservation::sum_of_pairs));
  tmp.insert(std::pair<ColumnConservationScores, ScoreFunction>(JensenShannonDivergence, MSAColumnConservation::jensen_shannon_divergence));

  return tmp;
};

void MSAColumnConservation::update_statistics(const index2 which_column) {


  tmp_total_weight = 0.0;
  tmp_rejected_cnt = 0;
  for (index2 i = 0; i < 20; ++i) tmp_cnts[i] = 0.0;
  for (size_t i = 0; i < msa_.size(); ++i) {
    index2 id = core::chemical::Monomer::get(msa_[i][which_column]).id;
    if (id < 20) {
      tmp_cnts[id] += weights[i];
      tmp_total_weight += weights[i];
    } else ++tmp_rejected_cnt;
  }
  for (index2 i = 0; i < 20; ++i) tmp_cnts[i] /= tmp_total_weight;
}

MSAColumnConservation::ScoreFunction MSAColumnConservation::shannon_entropy = [](MSAColumnConservation & my_instance,const index2 which_column) {

  my_instance.update_statistics(which_column);

  double out = 0.0;
  for (double p:my_instance.tmp_cnts)
    if (p > 0.0) out += p * log(p);

  return -out;
};

MSAColumnConservation::ScoreFunction MSAColumnConservation::relative_entropy = [](MSAColumnConservation & my_instance,const index2 which_column) {

  my_instance.update_statistics(which_column);

  double out = 0.0;
  for (core::index2 i=0;i<20;++i) {
    if (my_instance.tmp_cnts[i] > 0) {
      out += my_instance.tmp_cnts[i] * log(my_instance.tmp_cnts[i] / my_instance.background_counts[i]);
    }
  }
  return out;
};

MSAColumnConservation::ScoreFunction MSAColumnConservation::variation = [](MSAColumnConservation & my_instance,const index2 which_column) {

  my_instance.update_statistics(which_column);

  double out = 0.0;
  for (core::index2 i = 0; i < 20; ++i) {
    double p = my_instance.tmp_cnts[i] - my_instance.background_counts[i];
    out += p * p;
  }

  return sqrt(out);
};

MSAColumnConservation::ScoreFunction MSAColumnConservation::sum_of_pairs = [](MSAColumnConservation & my_instance,const index2 which_column) {

  my_instance.update_statistics(which_column);

  double out = 0.0;
  for (size_t i=1;i<20; ++i) {
    for (size_t j=0;j<i; ++j) {
      out += my_instance.tmp_cnts[i] * my_instance.tmp_cnts[j] * (*my_instance.sim_m)(i, j);
    }
  }

  return out;
};

MSAColumnConservation::ScoreFunction MSAColumnConservation::jensen_shannon_divergence = [](MSAColumnConservation & my_instance,const index2 which_column) {

  my_instance.update_statistics(which_column);

  double out = 0.0;
  for (core::index2 i = 0; i < 20; ++i) {
    if (my_instance.tmp_cnts[i] > 0) {
      double avg = (my_instance.tmp_cnts[i] + my_instance.background_counts[i]) / 2.0;
      out += my_instance.tmp_cnts[i] * log(my_instance.tmp_cnts[i] / avg) +
             my_instance.background_counts[i] * log(my_instance.background_counts[i] / avg);
    }
  }
  return out / 2.0;
};

MSAColumnConservation::ScoreFunction MSAColumnConservation::gap_percent = [](MSAColumnConservation & my_instance,const index2 which_column) {

  my_instance.update_statistics(which_column);

  return my_instance.tmp_rejected_cnt/double(my_instance.msa_.size());
};
std::map<ColumnConservationScores, MSAColumnConservation::ScoreFunction> MSAColumnConservation::scoring_methods = MSAColumnConservation::create_map();


} // ~alignment
} // ~core
