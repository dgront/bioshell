#ifndef CORE_ALIGNMENT_AbstractAligner_H
#define CORE_ALIGNMENT_AbstractAligner_H

#include <cmath>
#include <vector>
#include <limits.h>

#include <core/index.hh>

#include <core/data/basic/Array2D.hh>
#include <core/alignment/PairwiseAlignment.hh>
#include <iomanip>

namespace core {
namespace alignment {

/** \brief A base class for all the alignment methods implemented in BioShell
 *
 * @tparam T  - primitive data type used to represent alignment scores, e.g. <code>float</code> or <code>short</code>
 * @tparam Op - call operator used to calculate a score for a match between two positions
 */
template<typename T, typename Op>
class AbstractAligner {
public:

  /// Virtual destructor (empty)
  virtual ~AbstractAligner() {}

  /** @brief Calculates a sequence alignment.
   *
   * @param gap_open - penalty for opening a new gap; must be negative.
   * @param gap_extend - penalty for extending an existing gap; must be negative.
   * @param scoring - instance of a class that provides three methods:
   *     - tmplt_length() - the number of positions in the template sequence
   *     - query_length() - the number of positions in the query sequence
   *     - call operator operating on two integer indexes, which provides a score for matching the two respective positions
   * @tparam T - type used to represents scores
   * @tparam Op - scoring system class
   * @return alignment score.
   */
  virtual T align(const T gap_open,const  T gap_extend,const Op & scoring) = 0;

  /** @brief Calculates the score of the optimal sequence alignment.
   *
   * This method is very similar to align(const T,const  T,const Op &), but it <strong>does not store</strong>
   * information for backtracing; alignment will not be available. This method however is much faster that
   * align(const T,const  T,const Op &).
   */
  virtual T align_for_score(const T gap_open,const  T gap_extend,const Op & scoring) = 0;

  /** @brief Backtraces the alignment based on information sored by the most recent align(const T,const  T,const Op &) call.
   */
  virtual PairwiseAlignment_SP backtrace() = 0;

  /// Returns the score found by the most recent call of align(const T,const  T,const Op &) or align_for_score(const T,const  T,const Op &)
  virtual T recent_score() const = 0;
};

}
}

#endif

