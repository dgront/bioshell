#ifndef CORE_ALIGNMENT_MSAColumnConservation_H
#define CORE_ALIGNMENT_MSAColumnConservation_H

#include <vector>
#include <string>
#include <utility>
#include <map>

#include <core/index.hh>
#include <core/alignment/MultipleSequenceAlignment.hh>
#include <core/alignment/scoring/NcbiSimilarityMatrixFactory.hh>

namespace core {
namespace alignment {

enum ColumnConservationScores {
  GapPercent, ///< Count fraction of gaps at a given position (when the fraction of gaps gets higher, the column become irrelevant)
  SumOfPairs, ///< Sum-of-pairs score, the higher the more conserved the column is
  Variation, ///< measures mean-square difference between a column's distribution and a reference distribution (the higher the more conserved)
  ShannonEntropy, ///< Evaluates Shannon entropy of a MSA column  (the lower the more conserved)
  RelativeEntropy, ///< entropy relative to a reference distribution, also known as Kullback-Leibler divergence (the more diverged from reference, the higher the value)
  JensenShannonDivergence ///< Calculates Jensen-Shannon Divergence; the higher the more conserved, always in the range (0,1)
};

/** @brief Class that calculates various measures of conservation for MSA columns
 *
 */
class MSAColumnConservation {
  typedef std::function<double(MSAColumnConservation &, const index2)> ScoreFunction;
public:

  MSAColumnConservation(const MultipleSequenceAlignment & msa, const std::string & matrix_name = "BLOSUM62");

  MSAColumnConservation(const std::vector<std::string> & msa, const std::string & matrix_name = "BLOSUM62");

  MSAColumnConservation(const std::vector<core::data::sequence::Sequence_SP> & msa, const std::string & matrix_name = "BLOSUM62");

  double evaluate(ColumnConservationScores what_score, const index2 which_pos) { return scoring_methods[what_score](*this, which_pos); }

  static ColumnConservationScores score_by_name(const std::string &enum_name);

  /** @brief Creates all LocalBackbonePropertyDef objects necessary to calculate every property.
   * The objects are associated with their enum identifiers in a map
   * @return map holding all the property calculators
   */
  static std::map<ColumnConservationScores, ScoreFunction> create_map();

private:
  std::vector<double> background_counts;
  std::vector<std::string> msa_;
  std::vector<double> weights;
  core::alignment::scoring::NcbiSimilarityMatrix_SP sim_m;

  /// Stuff updated at every update_statistics() call; different for each column
  std::vector<double> tmp_cnts;
  double tmp_total_weight;
  index4 tmp_rejected_cnt;

  utils::Logger logs;

  std::string get_column(const index4 which_column);
  void update_statistics(const index2 which_column);
  void initialize();

  static ScoreFunction gap_percent; ///< count gaps
  static ScoreFunction variation; ///< Evaluates variation of a column from a reference distr
  static ScoreFunction sum_of_pairs; ///< sum-of-pairs score
  static ScoreFunction shannon_entropy; ///< Evaluates Shannon entropy of a MSA column
  static ScoreFunction relative_entropy; ///< Evaluates relative entropy of a MSA column (relative to the background distribution)
  static ScoreFunction jensen_shannon_divergence;

  static std::map<ColumnConservationScores,ScoreFunction> scoring_methods;
};


} // ~alignment
} // ~core

#endif
