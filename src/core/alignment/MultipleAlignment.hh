#ifndef CORE_ALIGNMENT_MultipleAlignment_H
#define CORE_ALIGNMENT_MultipleAlignment_H

#include <vector>
#include <string>
#include <utility>
#include <map>

#include <core/index.hh>
#include <core/alignment/AlignmentRow.hh>

namespace core {
namespace alignment {

/** @brief Multiple alignment of sequence-type data.
 *
 * Multiple alinment is implemented as a container of AlignmentRow objects
 * Typically, a sequence alignment of sequences, structures, secondary structures, etc.
 */
class MultipleAlignment {
public:

  /** @brief Returns the number of aligned objects (sequences, structures, etc.).
   */
  int count_rows() const { return aligned.size(); }

  /** @brief Adds a new alignment row to this multiple alignment.
   * @param id - a string identifier for an alignment row
   * @param row - a new row for this alignment
   */
  inline void add_row(const std::string & id, const AlignmentRow & row) {
    keys_.push_back(id);
    aligned.insert(std::pair<std::string,AlignmentRow>(id, row));
  }

  /** @brief  Returns an alignment row identified by a given string.
   *
   * Exception will be thrown if the given string is not a valid key
   * @param row_id - identifies a row in the alignment
   */
  const AlignmentRow & get_row(const std::string & row_id) const { return aligned.at(row_id); }

  /** @brief Creates a  a new alignment row and adds it to this multiple alignment
   */
  void add_row(const std::string &id, const std::string &aligned_sequence, const core::index2 first_pos) {
    aligned.emplace(id,AlignmentRow(first_pos,aligned_sequence));
    keys_.push_back(id);
  }

  /** @brief Begins iteration over Ids for alignment rows
   *
   * @return a const iterator pointing at the very first AlignmentRow object contained in this multiple alignment
   */
  std::vector<std::string>::const_iterator cbegin() const { return keys_.cbegin(); }

  /** @brief Ends iteration over Ids for alignment rows
   *
   * @return a pass-the-end const iterator pointing behind the very last AlignmentRow object contained in this multiple alignment
   */
  std::vector<std::string>::const_iterator cend() const { return keys_.cend(); }

protected:
  std::vector<std::string> keys_;

private:
  std::map<std::string,AlignmentRow> aligned;
};


} // ~alignment
} // ~core

#endif
