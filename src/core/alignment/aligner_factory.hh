#ifndef CORE_ALIGNMENT_AlignerFactory_H
#define CORE_ALIGNMENT_AlignerFactory_H

#include <core/alignment/AbstractAligner.hh>
#include <core/alignment/NWAligner.hh>
#include <core/alignment/SWAligner.hh>
#include <core/alignment/SemiglobalAligner.hh>

namespace core {
namespace alignment {

/** @brief Defines the alignment algorithms available in BioShell.
 */
enum AlignmentType {

  LOCAL_ALIGNMENT, ///< Smith-Waterman pairwise local sequence alignment
  GLOBAL_ALIGNMENT, ///< Needleman-Wunsh pairwise global sequence alignment
  SEMIGLOBAL_ALIGNMENT ///< Pairwise global sequence alignment without penalising terminal gaps
};

/** @brief Creates an aligner object
 *
 * The longest sequence must be at most <code>max_size</code> residues long.
 * @param which_aligner - defines the alignment method
 * @param max_size - the length of the longest aligned sequence.
 */
template<typename T, typename Op>
std::shared_ptr<AbstractAligner<T, Op>> create_aligner(AlignmentType which_aligner, const unsigned int max_size) {

  switch (which_aligner) {
    case LOCAL_ALIGNMENT :
      return std::make_shared<SWAligner<T, Op >>(max_size);
    case GLOBAL_ALIGNMENT :
      return std::make_shared<NWAligner<T, Op >>(max_size);
    default : // SEMIGLOBAL_ALIGNMENT is the default
      return std::make_shared<SemiglobalAligner<T, Op >>(max_size);
  }
}

}
}

#endif

