#include <core/data/structural/Strand.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>

namespace core {
namespace data {
namespace structural {

std::map<StrandTypes, std::string> Strand::create_map() {

  std::map<StrandTypes, std::string> m;
  m[StrandTypes::Parallel] = std::string("Parallel");
  m[StrandTypes::Antiparallel] = std::string("Antiparallel");
  m[StrandTypes::Other] = std::string("Other");

  return m;
};

std::map<StrandTypes, std::string> Strand::type_names = Strand::create_map();

const std::string & Strand::strand_type_name(const StrandTypes type) { return type_names[type]; }

std::string Strand::info() const {
  return utils::string_format("%3s : %c %3s %4s %4d - %3s %4s %4d %s",name().c_str(),hec_code(),
    front()->residue_type().code3.c_str(), front()->owner()->id().c_str(), front()->id(),
    back()->residue_type().code3.c_str(), back()->owner()->id().c_str(), back()->id(),
    strand_type_name(type).c_str());
}

}
}
}
