#ifndef CORE_DATA_STRUCTURAL_Strand_HH
#define CORE_DATA_STRUCTURAL_Strand_HH

#include <map>
#include <core/index.hh>

#include <core/data/structural/Residue.hh>
#include <core/data/structural/SecondaryStructureElement.hh>

namespace core {
namespace data {
namespace structural {

/// Defines all allowed types of a strand
enum StrandTypes { Parallel, Antiparallel, Other };

/// Represents a strand - secondary structure element
class Strand : public SecondaryStructureElement {
public:
  int strand_index_in_sheet; ///< Integer index denoting position of this Strand in its sheet
  StrandTypes type; ///< parallel, antiparallel or other

  static const std::string & strand_type_name(const StrandTypes type);

  /** \brief Creates a strand containing given residues.
   * @param residues - all residues that belong to this strand
   * @param type - type of this strand: one of StrandTypes members
   */
  Strand(const std::vector<Residue_SP> &residues, const StrandTypes type) :
    SecondaryStructureElement(residues), type(type), logger("Strand") {
    logger << utils::LogLevel::FINE << "Creating a new strand of " << residues.size() << " residues : "
           << info() << "\n";
  }

  /**\brief Creates a strand containing given residues.
 * @param residues_begin - iterator pointing to the first residue in this strand
 * @param residues_end - passed-the-end iterator referring to the last residue in this strand
 * @param type - type of this helix: one of StrandTypes members
 */
  template<typename It>
  Strand(const It & residues_begin, const It & residues_end, const StrandTypes type) :
    SecondaryStructureElement(residues_begin,residues_end), type(type), logger("Strand") {
    logger << utils::LogLevel::FINE << "Creating a new strand of " << int(std::distance(residues_begin, residues_end))
           << " residues : " << info() << "\n";
  }

  virtual char hec_code() const { return 'E'; };

  virtual std::string info() const;

  virtual ~Strand() {}
private:
  utils::Logger logger;
  static std::map<StrandTypes, std::string> type_names;
  static std::map<StrandTypes, std::string> create_map();
};

}
}
}

#endif // --- CORE_DATA_STRUCTURAL_Strand_HH
