/** @file Residue.hh
 *  @brief Defines Residue data structure and less-than operators for it.
 */
#ifndef CORE_DATA_STRUCTURAL_Residue_H
#define CORE_DATA_STRUCTURAL_Residue_H

#include <string>
#include <memory>
#include <algorithm>

#include <core/index.hh>

#include <utils/exceptions/AtomNotFound.hh>
#include <utils/string_utils.hh> // for string_format()

#include <core/data/basic/Vec3.hh>

#include <core/data/structural/Residue.fwd.hh>
#include <core/data/structural/Chain.fwd.hh>
#include <core/data/structural/PdbAtom.fwd.hh>
#include <core/data/structural/selectors/structure_selectors.fwd.hh>

#include <core/chemical/Monomer.hh>

namespace core {
namespace data {
namespace structural {

/** \brief Single residue : an amino acid, nucleic base, ligand, ion, etc.
 *
 * Residue is a container for atoms.
 *
 * The following example reads a PDB file and tests if each of the amino acid residues has all the five backbone heavy atoms:
 * \include ex_Residue.cc
 *
 * \include ex_LigandNeighbors.cc
 */
class Residue : public std::enable_shared_from_this<Residue>, private std::vector<std::shared_ptr<PdbAtom>> {

friend std::ostream& operator<<(std::ostream &out, const Residue & r);
friend utils::Logger &operator <<(utils::Logger &logger, const Residue & r);

public:
  using std::vector<std::shared_ptr<PdbAtom>>::assign;
  using std::vector<std::shared_ptr<PdbAtom>>::at;
  using std::vector<std::shared_ptr<PdbAtom>>::back;
  using std::vector<std::shared_ptr<PdbAtom>>::begin;
  using std::vector<std::shared_ptr<PdbAtom>>::capacity;
  using std::vector<std::shared_ptr<PdbAtom>>::cbegin;
  using std::vector<std::shared_ptr<PdbAtom>>::cend;
  using std::vector<std::shared_ptr<PdbAtom>>::crbegin;
  using std::vector<std::shared_ptr<PdbAtom>>::crend;
  using std::vector<std::shared_ptr<PdbAtom>>::clear;
  using std::vector<std::shared_ptr<PdbAtom>>::empty;
  using std::vector<std::shared_ptr<PdbAtom>>::end;
  using std::vector<std::shared_ptr<PdbAtom>>::erase;
  using std::vector<std::shared_ptr<PdbAtom>>::front;
  using std::vector<std::shared_ptr<PdbAtom>>::get_allocator;
  using std::vector<std::shared_ptr<PdbAtom>>::insert;
  using std::vector<std::shared_ptr<PdbAtom>>::max_size;
  using std::vector<std::shared_ptr<PdbAtom>>::operator [];
  using std::vector<std::shared_ptr<PdbAtom>>::pop_back;
  using std::vector<std::shared_ptr<PdbAtom>>::rbegin;
  using std::vector<std::shared_ptr<PdbAtom>>::rend;
  using std::vector<std::shared_ptr<PdbAtom>>::reserve;
  using std::vector<std::shared_ptr<PdbAtom>>::resize;
  using std::vector<std::shared_ptr<PdbAtom>>::size;
  using std::vector<std::shared_ptr<PdbAtom>>::swap;
  using std::vector<std::shared_ptr<PdbAtom>>::shrink_to_fit;
  typedef  typename std::vector<std::shared_ptr<PdbAtom>>::value_type  value_type;
  typedef  typename std::vector<std::shared_ptr<PdbAtom>>::reference  reference;
  typedef  typename std::vector<std::shared_ptr<PdbAtom>>::const_reference  const_reference;
  typedef  typename std::vector<std::shared_ptr<PdbAtom>>::pointer  pointer;
  typedef  typename std::vector<std::shared_ptr<PdbAtom>>::const_pointer  const_pointer;
  typedef  typename std::vector<std::shared_ptr<PdbAtom>>::iterator  iterator;
  typedef  typename std::vector<std::shared_ptr<PdbAtom>>::const_iterator  const_iterator;
  typedef  typename std::vector<std::shared_ptr<PdbAtom>>::reverse_iterator  reverse_iterator;
  typedef  typename std::vector<std::shared_ptr<PdbAtom>>::const_reverse_iterator  const_reverse_iterator;
  typedef  typename std::vector<std::shared_ptr<PdbAtom>>::size_type  size_type;
  typedef  typename std::vector<std::shared_ptr<PdbAtom>>::difference_type  difference_type;
  typedef  typename std::vector<std::shared_ptr<PdbAtom>>::allocator_type  allocator_type;

public:
  // ---------- C-tors ----------
  /// Constructor creates  a new residue of a given type
  Residue(const int id, const core::chemical::Monomer &residue_type) : id_(id), residue_type_(residue_type) {
	  reserve(residue_type.n_atoms);
	  insertion_code_ = ' ';
	  ss_type_ = 'C';
  }

  /// Constructor creates  a new residue of a given type defined by its 3-letter code
  Residue(const int id, const std::string &residue_type) : id_(id), residue_type_(core::chemical::Monomer::get(residue_type)) {
	  reserve(residue_type_.n_atoms);
	  insertion_code_ = ' ';
	  ss_type_ = 'C';
  }

  /// Constructor creates  a new residue of a given type defined by its one-letter code
  Residue(const int id, const char &residue_type) : id_(id), residue_type_(core::chemical::Monomer::get(residue_type)) {
	  reserve(residue_type_.n_atoms);
	  insertion_code_ = ' ';
	  ss_type_ = 'C';
  }

  /** @brief Creates a new Residue as a deep copy of this object.
   *
   * This method makes also a deep copy of atoms that belong to this residue.
   */
  Residue_SP clone() const;

  /** @brief Creates a new Residue as a deep copy of this object.
   *
   * This method makes also a deep copy of atoms that belong to this residue providing that
   * they satisfy a given selector.
   *
   * @param which_atoms - clone also atoms which satisfy the given selector
   */
  Residue_SP clone(const selectors::AtomSelector &which_atoms) const;

  // ---------- Getters ----------
  /// Returns and integer identifying this residue
  inline int id() const { return id_; }

  /// Returns an insertion code character
  inline char icode() const { return insertion_code_; }

  /// Returns a character denoting secondary structure assigned to this residue.
  inline char ss() const { return ss_type_; }

  /// Returns a PDB-convention string identifying this residue (id + icode)
  inline std::string &residue_id() { return residue_id_; }

  /// Returns the chemical residue type, i.e. the Monomer object assigned to this residue
  inline const core::chemical::Monomer &residue_type() const { return residue_type_; }

  // ---------- Misc operations ----------
  /// Returns the number of atoms that belong to this residue
  inline core::index2 count_atoms() const { return size(); }

  /// Returns the number of heavy (i.e. non-hydrogen) atoms that belong to this residue
  core::index2 count_heavy_atoms() const;

  // ---------- Setters ----------
  /// Sets the new integer index identifying this residue
  inline void id(const int new_id) {
    id_ = new_id;
    residue_id_ = utils::string_format("%d%c", id_, insertion_code_);
  }

  /// Sets the new insertion code character
  inline void icode(const char new_icode) {
    insertion_code_ = new_icode;
    residue_id_ = utils::string_format("%d%c", id_, insertion_code_);
  }

	/// Sets secondary structure type for this residue
  inline void ss(const char new_ss) { ss_type_ = new_ss; }

  // ---------- Atom tree operations ----------
  /// Returns a const-pointer to the chain owning this residue
  const std::shared_ptr<Chain> owner() const {
    std::shared_ptr<Chain> c = owner_.lock();
    if (c) return c;
    else return nullptr;
  }

  /// Returns a pointer to the chain owning this residue
  std::shared_ptr<Chain> owner() {
    std::shared_ptr<Chain> c = owner_.lock();
    if (c) return c;
    else return nullptr;
  }

  /** @brief Returns a pointer to the residue that directly precedes this residue in the chain.
   *
   * If this residue is N-terminal (5' terminal), <code>nullptr</code> is returned
   */
  Residue_SP previous() {
    std::shared_ptr<Residue> c = previous_.lock();
    if (c) return c;
    else return nullptr;
  }

  /** @brief Returns a const-pointer to the residue that directly precedes this residue in the chain.
   *
   * If this residue is N-terminal, <code>nullptr</code> is returned
   */
  const Residue_SP previous() const {
    std::shared_ptr<Residue> c = previous_.lock();
    if (c) return c;
    else return nullptr;
  }

  /** @brief Defines a residue that precedes this residue in a chain.
   * @param previous_residue - points to a residue that precedes this residue in a biomolecular chain
   */
  void previous(Residue_SP previous_residue) { previous_ = previous_residue; }

  /** @brief Returns a pointer to the residue that directly follows this residue in the chain.
   *
   * If this residue is C-terminal, <code>nullptr</code> is returned
   */
  Residue_SP next() {
    std::shared_ptr<Residue> c = next_.lock();
    if (c) return c;
    else return nullptr;
  }

  /** @brief Defines a residue that follows this residue in a chain.
   * @param next_residue - points to a residue that follows this residue in a biomolecular chain
   */
  void next(Residue_SP next_residue) { next_ = next_residue; }

  /** @brief Returns a const-pointer to the residue that directly follows this residue in the chain.
   *
   * If this residue is C-terminal, <code>nullptr</code> is returned
   */
  const Residue_SP next() const {
    Residue_SP c = next_.lock();
    if (c) return c;
    else return nullptr;
  }

  /// Sets the new owner (i.e. a chain) that owns this residue
  void owner(std::shared_ptr<Chain> new_owner) { owner_ = new_owner; }

  /// Adds an atom to this residue
  void push_back(PdbAtom_SP a);

  friend Chain;

  // ---------- Calculations ----------

  /** @brief Computes the closest distance between any atom of this residue and any atom from another residue
   *
   * @param another_residue - the second residue for the distance calculation
   * @param min_val_cutoff - the method returns the minimal distance only if it is shorter than that given cutoff
   * @return a distance value
   *
   * The example program reads a PDB file and looks for a specific ligand. Then it finds
   * all residues from a structure whose <code>min_distance()</code> is shorter than a given cutoff.
   *
   * \include ./ex_LigandNeighbors.cc
   */
  double min_distance(const Residue & another_residue,
                          const double min_val_cutoff = std::numeric_limits<double>::max()) const;

  /** @brief Computes the closest distance between any atom of this residue and any atom from another residue
   *
   * @param another_residue - the second residue for the distance calculation
   * @param min_val_cutoff - the method returns the minimal distance only if it is shorter than that given cutoff
   * @return a distance value
   */
  double min_distance(const Residue_SP another_residue,
    const double min_val_cutoff = std::numeric_limits<double>::max()) const { return min_distance(*another_residue); }

  /** @brief Computes the closest distance between any valid atom of this residue and any valid atom of another residue
   *
   * @param another_residue - the second residue for the distance calculation
   * @param valid_atoms - selector (call operator or a lambda function) used to say which atoms are included
   * in distance evaluations (same for both residues)
   * @param min_val_cutoff - the method returns the minimal distance only if it is shorter than that given cutoff
   * @tparam S - a lambda function or a structure providing a call operator to select atoms
   * @return a distance value
   */
  double min_distance(const Residue & another_residue, selectors::AtomSelector_SP & valid_atoms,
                          double min_val_cutoff = std::numeric_limits<double>::max()) const;

  /** @brief Calculates the center of mass for the selected atoms of this residue
   * @param which_atoms - selects atoms for the CM calculations, e.g. all heavy atoms, atoms of the side chain etc.
   * @return CM vector
   */
  core::data::basic::Vec3 cm(const selectors::AtomSelector &which_atoms) const;

  /** @brief Calculates the center of mass of this residue
   * @return CM vector
   */
  core::data::basic::Vec3 cm() const;

  /** @brief Finds atom by name; throws an exception if not found
   *
   * @param atom_name - the name of an atom to look for
   * @return a pointer to an atom
   */
  const PdbAtom_SP find_atom_safe(const std::string & atom_name) const {
    const PdbAtom_SP a = find_atom(atom_name, nullptr);
    if (a != nullptr) return a;
    throw utils::exceptions::AtomNotFound(atom_name, residue_id_);
  }

  /** @brief Finds atom by name.
   *
   * @param atom_name - the name of an atom to look for
   * @param fallback_item - object returned when there is no atom named <code>atom_name</code> in this residue;
   * by default <code>nullptr</code> is returned
   * @return a pointer to an atom; <code>fallback_item</code> (<code>nullptr</code> by default) when not found
   */
  PdbAtom_SP find_atom(const std::string & atom_name, PdbAtom_SP fallback_item = nullptr);

  /** @brief Finds atom by name.
   *
   * @param atom_name - the name of an atom to look for
   * @param fallback_item - object returned when there is no atom named <code>atom_name</code> in this residue;
   * by default <code>nullptr</code> is returned
   * @return a pointer to an atom; <code>fallback_item</code> (<code>nullptr</code> by default) when not found
   */
  PdbAtom_SP find_atom(const char* atom_name, PdbAtom_SP fallback_item = nullptr)  { return find_atom(std::string(atom_name)); }

  /** @brief Finds atom by name.
   *
   * @param atom_name - the name of an atom to look for
   * @param fallback_item - object returned when there is no atom named <code>atom_name</code> in this residue;
   * by default <code>nullptr</code> is returned
   * @return a pointer to an atom; <code>fallback_item</code> (<code>nullptr</code> by default) when not found
   */
  const PdbAtom_SP find_atom(const std::string & atom_name, const PdbAtom_SP fallback_item = nullptr) const;

  /** @brief Finds atom by name.
   *
   * @param atom_name - the name of an atom to look for
   * @param fallback_item - object returned when there is no atom named <code>atom_name</code> in this residue;
   * by default <code>nullptr</code> is returned
   * @return a pointer to an atom; <code>fallback_item</code> (<code>nullptr</code> by default) when not found
   */
  const PdbAtom_SP find_atom(const char* atom_name, const PdbAtom_SP fallback_item = nullptr) const  { return find_atom(std::string(atom_name)); }

  /// Sort atoms of this residue by their ID
  void sort();

private:
  int id_;    ///< Integer identifying this residue
  const core::chemical::Monomer &residue_type_; ///< The chemical type of this residue
  char insertion_code_;    ///< PDB-style insertion code
  char ss_type_; ///< secondary structure type
  std::string residue_id_;
  std::weak_ptr<Chain> owner_;
  std::weak_ptr<Residue> previous_;
  std::weak_ptr<Residue> next_;
};

/** @brief less-then operator provides PDB-like ordering.
 *
 * A given residue \f$r_i\f$ should go before residue \f$r_j\f$ if:
 *     - the ID of \f$r_i\f$ is less than the ID of \f$r_j\f$
 *     - IDs of the two residues are equal and the insertion code of \f$r_i\f$ is lexically before the insertion code of \f$r_j\f$
 */
bool operator<(const Residue & ri, const Residue & rj);

/** @brief less-then operator provides PDB-like ordering.
 *
 * A given residue \f$r_i\f$ should go before residue \f$r_j\f$ if:
 *     - the ID of \f$r_i\f$ is less than the ID of \f$r_j\f$
 *     - IDs of the two residues are equal and the insertion code of \f$r_i\f$  lexicographically precedes the insertion code of \f$r_j\f$
 */
bool operator<(const Residue_SP ri, const Residue_SP rj);

}
}
}

#endif
/**
 * \example ex_Residue.cc
 * \example ex_LigandNeighbors.cc
 */
