#ifndef CORE_DATA_STRUCTURAL_StrandPairing_HH
#define CORE_DATA_STRUCTURAL_StrandPairing_HH

#include <core/data/structural/Strand.hh>
#include <core/calc/structural/interactions/BackboneHBondInteraction.hh>
#include <core/data/structural/StrandPairing.fwd.hh>

namespace core {
namespace data {
namespace structural {

/** @brief Says which two strands form a \f$ \beta \f$ -sheet
 */
class StrandPairing {
public:

  const Strand_SP first_strand;    ///< First (the one closer to N terminus) strand in this pairing
  const Strand_SP second_strand;   ///< Second (the one closer to C terminus) strand in this pairing
  const StrandTypes pairing_type;  ///< Defines the type of this strand pairing

  /** @brief constructor fills the public const member fields
   * @param first_strand - the first (N-terminal) of the two strands
   * @param second_strand - the second (C-terminal) of the two strands
   * @param pairing_type - defines the strand pairing
   */
  StrandPairing(const Strand_SP first_strand,const Strand_SP second_strand, const StrandTypes pairing_type) :
    first_strand(first_strand), second_strand(second_strand), pairing_type(pairing_type) {}

  const std::vector<calc::structural::interactions::BackboneHBondInteraction_SP> & hydrogen_bonds() const { return hbonds; }

  void add_hbond(calc::structural::interactions::BackboneHBondInteraction_SP hbond) { hbonds.push_back(hbond); }

private:
  std::vector<calc::structural::interactions::BackboneHBondInteraction_SP> hbonds;
};

}
}
}

#endif // --- CORE_DATA_STRUCTURAL_StrandPairing_HH
