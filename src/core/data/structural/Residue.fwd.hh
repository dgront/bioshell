#ifndef CORE_DATA_STRUCTURAL_Residue_FWD_HH
#define CORE_DATA_STRUCTURAL_Residue_FWD_HH

#include <memory>

namespace core {
namespace data {
namespace structural {

/// Forwarded declaration of the Residue class
class Residue;
/// Declaration of a shared pointer to the Residue class
typedef std::shared_ptr<Residue> Residue_SP;

}
}
}

#endif
