/** @file ResiduePairCollector.hh
 *  @brief Provides ResiduePairCollector base class that collects ResiduePair objects
 */
#ifndef CORE_DATA_STRUCTURAL_ResiduePairCollector_HH
#define CORE_DATA_STRUCTURAL_ResiduePairCollector_HH

#include <memory>

#include <core/data/structural/Structure.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/ResiduePair.hh>

namespace core {
namespace data {
namespace structural {

/** @brief A base class for any collector that collects  residue pairs.
 *
 * ResiduePairCollector is a virtual class, do not instantiate. Use other collector instead.
 */
class ResiduePairCollector {
public:

  /** @brief Collect residue pairs from a Structure object
   *
   * Pointers to newly created ResiduePair instances will be appended to the given vector. The vector is not cleared
   * by this call, so its content will not be lost
   * @param strctr - a structure  ResiduePair will be taken from
   * @param sink - destinations, where the ResiduePair objects will be stored
   * @return number of new ResiduePairs created
   */
  virtual core::index4 collect(const core::data::structural::Structure &strctr,
                                            std::vector<ResiduePair_SP> &sink) = 0;

  /** @brief Collect residue pairs from a Chain object
   *
   * Pointers to newly created ResiduePair instances will be appended to the given vector. The vector is not cleared
   * by this call, so its content will not be lost
   * @param chain - a chain  ResiduePair will be taken from
   * @param sink - destinations, where the ResiduePair objects will be stored
   * @return number of new ResiduePairs created
   */
  virtual core::index4 collect(const core::data::structural::Chain &chain,
                                            std::vector<ResiduePair_SP> &sink) = 0;

  /** @brief Create a ResiduePair based on the two given Residues providing they satisfy the selection criteria
   *
   * A pointer to the newly created ResiduePair instance will be appended to the given vector.
   * The vector is not cleared by this call, so its content will not be lost.
   * @param i_residue - the first residue in a pair
   * @param j_residue - the second residue in a pair
   * @param sink - destinations, where the ResiduePair objects will be stored
   * @return number of new ResiduePairs created
   */
  virtual core::index4 collect(const core::data::structural::Residue &i_residue,
                                            const core::data::structural::Residue &j_residue,
                                            std::vector<ResiduePair_SP> &sink) = 0;
  /// Bare default destructor
  virtual ~ResiduePairCollector() = default;
};

}
}
}


#endif
