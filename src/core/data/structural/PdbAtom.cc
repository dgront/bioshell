#include <string>

#include <utils/string_utils.hh> // for string_format()

#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.hh>

namespace core {
namespace data {
namespace structural {

std::string format_pdb_atom_name(const std::string &atom_name,bool is_hetero) {

  if (atom_name.size() == 1) return " " + atom_name + "  ";
  if (atom_name.size() == 2 ) {
      if (is_hetero) return atom_name + "  "; else return " " + atom_name + " ";}
  if (atom_name.size() == 3) return " " + atom_name;
  return atom_name;
}

PdbAtom::PdbAtom(const core::index4 id, const std::string &atom_name, const core::index2 element_index) :
  id_(id), atom_name_(atom_name), element_index_(element_index) {
  alt_locator_ = ' ';
  occupancy_ = 1.0;
  b_factor_ = 99.99;
  is_heteroatom_ = false;
}

PdbAtom::PdbAtom(const core::index4 id, const std::string &atom_name,
                 const double cx, const double cy, const double cz, const double occupancy,
                 const double b_factor, const core::index2 element_index) :
  Vec3(cx, cy, cz), id_(id), atom_name_(atom_name), element_index_(element_index), alt_locator_(' '),
  occupancy_(occupancy), b_factor_(b_factor) {
  is_heteroatom_ = false;
}

PdbAtom_SP PdbAtom::clone() const {
  PdbAtom_SP ret = std::make_shared<PdbAtom>(id_,atom_name_,x,y,z,occupancy_,b_factor_,element_index_);
  ret->is_heteroatom_ = is_heteroatom_;
  return ret;
}

std::string PdbAtom::to_pdb_line() const {

  std::string out;

  const std::string &fmt = (is_heteroatom_) ? data::io::Atom::hetatm_format_uncharged
                                            : data::io::Atom::atom_format_uncharged;
  if (Residue_SP rr = owner_.lock()) {
    const Residue &r = *rr;
    const auto c = r.owner();
    out = utils::string_format(fmt, id_, atom_name_.c_str(), ' ',
                               r.residue_type().code3.c_str(), r.owner()->char_id(), r.id(), r.icode(), x, y, z,
                               occupancy_, b_factor_,
                               core::chemical::AtomicElement::periodic_table[element_index_].symbol.c_str());
    if ((*(c->terminal_residue()) == rr) && (*(rr->back()) == *this)) {
      std::string ter = utils::string_format("\nTER   %5d      %3s %c%4d%c", id_, r.residue_type().code3.c_str(),
                                             r.owner()->char_id(), r.id(), r.icode());
      out += ter;
      return out;
    } else { return out; }
  } else {
    return utils::string_format(fmt, id_, atom_name_.c_str(), ' ',
                                "ALA", 'A', 0, ' ', x, y, z, occupancy_, b_factor_,
                                core::chemical::AtomicElement::periodic_table[element_index_].symbol.c_str());
  }
}

std::ostream& operator<<(std::ostream &out, const PdbAtom &v) {

  out << utils::string_format("%4d %4s %8.3f %8.3f %8.3f", v.id(), v.atom_name().c_str(), v.x, v.y, v.z);
  return out;
}

}
}
}
