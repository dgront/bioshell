/** @file ResidueIndex.hh
 *  @brief Provides ResidueIndex data structure that binds all the data necessary to identify a residue
 */
#ifndef CORE_DATA_STRUCTURAL_ResidueIndex_H
#define CORE_DATA_STRUCTURAL_ResidueIndex_H

#include <map>
#include <memory>
#include <vector>
#include <ostream>

#include <core/index.hh>
#include <core/data/structural/Structure.fwd.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

namespace core {
namespace data {
namespace structural {

/** @brief A simple data structure to unambiguously define a residue.
 *
 * This structure is used as an index pointing to residues in aggregated types.
 * @see PairwiseResidueMap
 */
struct ResidueIndex {
  std::string chain_id; ///< single character denoting the chain this residue belong to
  int residue_id;       ///< Index of this residue, as given in a PDB file; may be negative
  char i_code;          ///< Insertion code of this residue (a space in most cases)
};

/** @brief ostream operator for ResidueIndex
 *
 * @param out - output stream
 * @param ri - printed object
 * @return reference to the stream
 */
std::ostream & operator<<(std::ostream & out, const ResidueIndex & ri);

/** @brief less-then operator provides PDB-like ordering.
 *
 * A given residue \f$r_i\f$ should go before residue \f$r_j\f$ if:
 *     - residue \f$r_i\f$ belongs to a chain of a code lexically lower than \f$r_j\f$ chain code
 *     - the ID of \f$r_i\f$ is less than the ID of \f$r_j\f$
 *     - IDs of the two residues are equal and the insertion code of \f$r_i\f$ is lexically before the insertion code of \f$r_j\f$
 */
bool operator<(const ResidueIndex & ri, const ResidueIndex & rj);

/** @brief less-then operator provides PDB-like ordering.
 *
 * A given residue \f$r_i\f$ should go before residue \f$r_j\f$ if:
 *     - residue \f$r_i\f$ belongs to a chain of a code lexically lower than \f$r_j\f$ chain code
 *     - the ID of \f$r_i\f$ is less than the ID of \f$r_j\f$
 *     - IDs of the two residues are equal and the insertion code of \f$r_i\f$  lexicographically precedes the insertion code of \f$r_j\f$
 */
bool operator<(const std::shared_ptr<ResidueIndex> ri, const std::shared_ptr<ResidueIndex> rj);


/** @brief Provides a map that bind residue indexes (ResidueIndex instances) to integer indexes from a given structure.
 */
class ResidueIndexer {
public:

  /** @brief Creates a residue indexer for a given structure.
   * @param strctr - a structure whose residues will be indexed by this object
   */
  ResidueIndexer(const core::data::structural::Structure & strctr);

  /** @brief Creates a residue indexer for a given structure.
   * @param strctr - a structure whose residues will be indexed by this object
   * @param only_selected_residues - this ResidueIndexer will cover only these residues who satisfy a given selector.
   * For example, to create a ResidueIndexer for protein chains (but to skip ligands etc) use ResidueSelector::IsAA selctor
   */
  ResidueIndexer(const Structure & strctr, const selectors::ResidueSelector & only_selected_residues);

  template<typename It>
  ResidueIndexer(It &residue_begin, It &residue_end);

  /** @brief Return an integer index assigned to a given ResidueIndex instance.
   *
   * This method provides  bounds checking. If an argument refers to a residue unknown to this indexer, an exception will be thrown.
   * @param ri - specifies a residue
   * @return integer index assigned to that residue
   */
  core::index2 at(const ResidueIndex & ri) const { return index.at(ri); }

  /** @brief Return an integer index assigned to a given Residue instance.
   *
   * This method provides  bounds checking. If an argument refers to a residue unknown to this indexer, an exception will be thrown.
   * @param r - a residue
   * @return integer index assigned to that residue
   */
  core::index2 at(const Residue & r) const { return index.at(ResidueIndex( { r.owner()->id(), r.id(), r.icode() })); }

  /** @brief Return an integer index assigned to a given Residue instance.
   *
   * This method does provide  bounds checking. If arguments refer to a residue unknown to this indexer, an exception will be thrown.
   * @param chain_id - a chain owning that residue
   * @param res_id - residue index
   * @param i_code - insertion code of that residue
   * @return integer index assigned to that residue
   */
  core::index2 at(const std::string & chain_id, const int res_id, const char i_code) const;

  /// Returns the highest value of a residue index.
  core::index2 max_index() const { return max_index_; }

  /** @brief Returns a ResidueIndex describing a residue at a given position
   *
   * @param residue_index - index (position) of a residue, starts from 0
   * @return ResidueIndex of a residue
   */
  const ResidueIndex & residue_index(core::index2 residue_index);

  /** @brief Returns a residue for a given index.
   *
   * This method allows reverse indexing: returns residue for its index
   *
   * @param residue_index - index of a residue
   * @return a pointer to a residue for a given index
   */
  const Residue_SP residue(core::index2 residue_index) { return resids[residue_index]; }

  /** @brief Returns a residue for a given index.
   *
   * This method allows reverse indexing: returns residue for its index
   *
   * @param residue_index - index of a residue
   * @return a pointer to a residue for a given index
   */
  const Residue_SP residue(ResidueIndex residue_index) { return resids[index[residue_index]]; }

private:
  std::map<ResidueIndex,core::index2> index;
  core::index2 max_index_;
  std::vector<Residue_SP> resids;
};

}
}
}

#endif
