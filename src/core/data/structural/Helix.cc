#include <core/data/structural/Helix.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>

namespace core {
namespace data {
namespace structural {

std::map<HelixTypes, std::string> Helix::create_map() {

  std::map<HelixTypes, std::string> m;

  m[HelixTypes::RightAlpha] = std::string("RightAlpha");
  m[HelixTypes::RightOmega] = std::string("RightOmega");
  m[HelixTypes::RightPi] = std::string("RightPi");
  m[HelixTypes::RightGamma] = std::string("RightGamma");
  m[HelixTypes::Right310] = std::string("Right310");
  m[HelixTypes::LeftAlpha] = std::string("LeftAlpha");
  m[HelixTypes::LeftOmega] = std::string("LeftOmega");
  m[HelixTypes::RibbonHelix] = std::string("RibbonHelix");
  m[HelixTypes::LeftGamma] = std::string("LeftGamma");
  m[HelixTypes::Polyproline] = std::string("Polyproline");
  return m;
};

std::map<HelixTypes, std::string> Helix::type_names = Helix::create_map();

const std::string & Helix::helix_type_name(const HelixTypes type) { return type_names[type]; }

std::string Helix::info() const {
  return utils::string_format("%3s : %c %3s %4s %4d - %3s %4s %4d %s",name().c_str(),hec_code(),
     front()->residue_type().code3.c_str(), front()->owner()->id().c_str(), front()->id(),
     back()->residue_type().code3.c_str(),back()->owner()->id().c_str(), back()->id(),
     helix_type_name(type).c_str());
}

}
}
}
