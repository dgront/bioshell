#include <core/data/structural/BetaStructuresGraph.hh>


namespace core {
namespace data {
namespace structural {

struct CompareStrands{

  bool operator()(Strand_SP lhs, Strand_SP rhs) { return lhs->name() < rhs->name(); }
};

std::vector<Strand_SP> BetaStructuresGraph::get_strands_copy() {

  std::vector<Strand_SP> out;
  for (auto it = Base::begin(); it != Base::end(); ++it) out.push_back(*it);
  std::sort(out.begin(), out.end(), CompareStrands());

  return out;
}

std::vector<Strand_SP> BetaStructuresGraph::get_strands_copy(const Strand_SP v) {

  std::vector<Strand_SP> out;

  for(auto it = Base::begin(v);it!=Base::end(v);++it) out.push_back(*it);
  std::sort(out.begin(), out.end(), CompareStrands());

  return out;
}

/** @brief Prints adjacency matrix of this graph on ostream, e.g. on a screen
 * @param out - stream to print this graph
 */
void BetaStructuresGraph::print_adjacency_matrix(std::ostream &out) {

  for (core::index2 i = 0; i < count_vertices(); ++i) {
    out << utils::string_format("%5s ", vertex(i)->name().c_str());
    for (core::index2 j = 0; j < count_vertices(); ++j)
      out << ((core::algorithms::SimpleGraph::are_connected(i, j)) ? '#' : '.');
    out <<"\n";
  }
}

bool BetaStructuresGraph::are_connected(const Strand_SP si,const  Strand_SP sj) const {
  return core::algorithms::SimpleGraph::are_connected(Base::index(si),Base::index(sj));
}


}
}
}