/** \file ResiduePair.hh
 * @brief Defines a ResiduePair data type
 */
#ifndef CORE_DATA_STRUCTURAL_ResiduePair_HH
#define CORE_DATA_STRUCTURAL_ResiduePair_HH

#include <memory>

#include <core/data/structural/Residue.fwd.hh>

namespace core {
namespace data {
namespace structural {

/** \brief Defines a data type that holds pointers to two residues
 */
class ResiduePair {
public:

  /** @brief Base class constructor to define an abstract interaction between two residues.
   *
   * @param first_residue - first residue
   * @param second_residue - second residue
   */
  ResiduePair(const core::data::structural::Residue_SP first_residue,
                     const core::data::structural::Residue_SP second_residue) :
      first_residue_(first_residue), second_residue_(second_residue) {}

  /// Default virtual destructor
  virtual ~ResiduePair() {}

  /// Returns the first atom of this residue pair
  const core::data::structural::Residue_SP first_residue() const { return first_residue_; }

  /// Returns the second atom of this residue pair
  const core::data::structural::Residue_SP second_residue() const { return second_residue_; }

protected:
  const core::data::structural::Residue_SP first_residue_; ///< the first interacting residue
  const core::data::structural::Residue_SP second_residue_; ///< the second interacting residue
};

/// A shared pointer to ResiduePair type - for polymorphic behavior
typedef std::shared_ptr<ResiduePair> ResiduePair_SP;

}
}
}

#endif
