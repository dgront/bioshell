#ifndef CORE_DATA_STRUCTURAL_Structure_H
#define CORE_DATA_STRUCTURAL_Structure_H

#include <stdexcept>

#include <core/index.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/io/Pdb.fwd.hh>
#include <core/data/io/PdbField.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Structure.fwd.hh>
#include <core/data/structural/selectors/structure_selectors.fwd.hh>

namespace core {
namespace data {
namespace structural {

/** \brief Represents a biomacromolecular structure.
 *
 * To read a Structure from a PDB file, use Pdb class.
 * Structures may be conveniently loaded at the command line level from a PDB file by using
 * <code>utils::options::structures_from_cmdline()</code> declared in input_utils.hh
 *
 * To write a structure to a PDB-formatted file, use <code>core::data::io::write_pdb()</code> declared in Pdb.hh file
 *
 * The following example reads a PDB file and creates a Structure object
 * \include ex_Pdb.cc
 *
 * The following example iterates over chains, residues and atom; it prints all atoms of a residue in a single line, one line per residue
 * \include ex_Structure.cc
 */
class Structure :  public std::enable_shared_from_this<Structure>, private std::vector<std::shared_ptr<Chain> >{
public:
  using std::vector<std::shared_ptr<Chain>>::assign;
  using std::vector<std::shared_ptr<Chain>>::at;
  using std::vector<std::shared_ptr<Chain>>::back;
  using std::vector<std::shared_ptr<Chain>>::begin;
  using std::vector<std::shared_ptr<Chain>>::capacity;
  using std::vector<std::shared_ptr<Chain>>::cbegin;
  using std::vector<std::shared_ptr<Chain>>::cend;
  using std::vector<std::shared_ptr<Chain>>::crbegin;
  using std::vector<std::shared_ptr<Chain>>::crend;
  using std::vector<std::shared_ptr<Chain>>::clear;
  using std::vector<std::shared_ptr<Chain>>::empty;
  using std::vector<std::shared_ptr<Chain>>::end;
  using std::vector<std::shared_ptr<Chain>>::erase;
  using std::vector<std::shared_ptr<Chain>>::front;
  using std::vector<std::shared_ptr<Chain>>::get_allocator;
  using std::vector<std::shared_ptr<Chain>>::insert;
  using std::vector<std::shared_ptr<Chain>>::max_size;
  using std::vector<std::shared_ptr<Chain>>::operator [];
  using std::vector<std::shared_ptr<Chain>>::pop_back;
  using std::vector<std::shared_ptr<Chain>>::rbegin;
  using std::vector<std::shared_ptr<Chain>>::rend;
  using std::vector<std::shared_ptr<Chain>>::reserve;
  using std::vector<std::shared_ptr<Chain>>::resize;
  using std::vector<std::shared_ptr<Chain>>::size;
  using std::vector<std::shared_ptr<Chain>>::swap;
  using std::vector<std::shared_ptr<Chain>>::shrink_to_fit;
  typedef  typename std::vector<std::shared_ptr<Chain>>::value_type  value_type;
  typedef  typename std::vector<std::shared_ptr<Chain>>::reference  reference;
  typedef  typename std::vector<std::shared_ptr<Chain>>::const_reference  const_reference;
  typedef  typename std::vector<std::shared_ptr<Chain>>::pointer  pointer;
  typedef  typename std::vector<std::shared_ptr<Chain>>::const_pointer  const_pointer;
  typedef  typename std::vector<std::shared_ptr<Chain>>::iterator  iterator;
  typedef  typename std::vector<std::shared_ptr<Chain>>::const_iterator  const_iterator;
  typedef  typename std::vector<std::shared_ptr<Chain>>::reverse_iterator  reverse_iterator;
  typedef  typename std::vector<std::shared_ptr<Chain>>::const_reverse_iterator  const_reverse_iterator;
  typedef  typename std::vector<std::shared_ptr<Chain>>::size_type  size_type;
  typedef  typename std::vector<std::shared_ptr<Chain>>::difference_type  difference_type;
  typedef  typename std::vector<std::shared_ptr<Chain>>::allocator_type  allocator_type;

public:

  /// const-iterator to iterate over atoms of this structure
  class atom_const_iterator {
  public:

      typedef std::vector<std::shared_ptr<Chain> >::const_iterator outer_iterator;
      typedef Chain::atom_iterator inner_iterator;

      typedef std::forward_iterator_tag iterator_category;
      typedef inner_iterator::value_type value_type;
      typedef inner_iterator::difference_type difference_type;
      typedef inner_iterator::pointer pointer;
      typedef inner_iterator::reference reference;

      atom_const_iterator() { }
      atom_const_iterator(outer_iterator it) : outer_it_(it), outer_end_(it) { }
      atom_const_iterator(outer_iterator it, outer_iterator end) : outer_it_(it), outer_end_(end) {

        if (outer_it_ == outer_end_) return; inner_it_ = (*outer_it_)->first_atom();
        advance_past_empty_inner_containers();
      }

      reference operator*()  const { return *inner_it_;  }
      pointer operator->() const { return &*inner_it_; }

      atom_const_iterator& operator++ () {
        ++inner_it_;
        if (inner_it_ == (*outer_it_)->last_atom()) advance_past_empty_inner_containers();

        return *this;
      }

      atom_const_iterator operator++ (int) {
        atom_const_iterator it(*this);
        ++*this;

        return it;
      }

      atom_const_iterator& operator+=(const unsigned int rhs){

        for(unsigned int i=0;i<rhs;++i)  ++*this;
        return *this;
      }

      bool operator==(const atom_const_iterator &b) const {
        if (outer_it_ != b.outer_it_) return false;
        if (outer_it_ != outer_end_ && b.outer_it_ != b.outer_end_ && inner_it_ != b.inner_it_)
          return false;

        return true;
      }

      bool operator!=(const atom_const_iterator &b) const { return !operator==(b); }

  private:

      void advance_past_empty_inner_containers() {
        while (outer_it_ != outer_end_ && inner_it_ == (*outer_it_)->last_atom()) {
          ++outer_it_;
          if (outer_it_ != outer_end_) inner_it_ = (*outer_it_)->first_atom();
        }
      }

      outer_iterator outer_it_;
      outer_iterator outer_end_;
      inner_iterator inner_it_;
    }; // ~ atom_iterator

  /// Iterator (non-const) to iterate over atoms of this structure
  class atom_iterator {
	public:

	    typedef std::vector<std::shared_ptr<Chain> >::iterator outer_iterator;
	    typedef Chain::atom_iterator inner_iterator;

	    typedef std::forward_iterator_tag iterator_category;
	    typedef inner_iterator::value_type value_type;
	    typedef inner_iterator::difference_type difference_type;
	    typedef inner_iterator::pointer pointer;
	    typedef inner_iterator::reference reference;

	    atom_iterator() { }
	    atom_iterator(outer_iterator it) : outer_it_(it), outer_end_(it) { }
	    atom_iterator(outer_iterator it, outer_iterator end) : outer_it_(it), outer_end_(end) {

	      if (outer_it_ == outer_end_) return; inner_it_ = (*outer_it_)->first_atom();
	      advance_past_empty_inner_containers();
	    }

	    reference operator*()  const { return *inner_it_;  }
	    pointer operator->() const { return &*inner_it_; }

    atom_iterator& operator+= (const unsigned int rhs) {

        for(unsigned int i=0;i<rhs;++i)  ++*this;
        return *this;
      }

    atom_iterator& operator++ () {
	      ++inner_it_;
	      if (inner_it_ == (*outer_it_)->last_atom()) advance_past_empty_inner_containers();

	      return *this;
	    }

	    atom_iterator operator++ (int) {
	      atom_iterator it(*this);
	      ++*this;

	      return it;
	    }

      bool operator==(const atom_iterator &b) const {
        if (outer_it_ != b.outer_it_) return false;
        if (outer_it_ != outer_end_ && b.outer_it_ != b.outer_end_ && inner_it_ != b.inner_it_)
          return false;

        return true;
      }

      bool operator!=(const atom_iterator &b) const { return !operator==(b); }

  private:

	    void advance_past_empty_inner_containers() {
	      while (outer_it_ != outer_end_ && inner_it_ == (*outer_it_)->last_atom()) {
	        ++outer_it_;
	        if (outer_it_ != outer_end_) inner_it_ = (*outer_it_)->first_atom();
	      }
	    }

	    outer_iterator outer_it_;
	    outer_iterator outer_end_;
	    inner_iterator inner_it_;
	  }; // ~ atom_iterator

  /// const-iterator to iterate over residues of this structure
  class residue_const_iterator {
  public:

      typedef std::vector<std::shared_ptr<Chain> >::const_iterator outer_iterator;
      typedef std::vector<std::shared_ptr<Residue> >::const_iterator inner_iterator;

      typedef std::forward_iterator_tag iterator_category;
      typedef inner_iterator::value_type value_type;
      typedef inner_iterator::difference_type difference_type;
      typedef inner_iterator::pointer pointer;
      typedef inner_iterator::reference reference;

      residue_const_iterator() { }
      residue_const_iterator(outer_iterator it) : outer_it_(it), outer_end_(it) { }
      residue_const_iterator(outer_iterator it, outer_iterator end) : outer_it_(it), outer_end_(end) {

        if (outer_it_ == outer_end_) return; inner_it_ = (*outer_it_)->begin();
        advance_past_empty_inner_containers();
      }

      reference operator*()  const { return *inner_it_;  }
      pointer operator->() const { return &*inner_it_; }

      residue_const_iterator& operator++ () {
        ++inner_it_;
        if (inner_it_ == (*outer_it_)->cend()) advance_past_empty_inner_containers();

        return *this;
      }

    residue_const_iterator operator++ (int) {
      residue_const_iterator it(*this);
      ++*this;

      return it;
    }

    residue_const_iterator& operator+=(const unsigned int rhs){

      for(unsigned int  i=0;i<rhs;++i)  ++*this;
      return *this;
    }

      friend bool operator==(const residue_const_iterator &a, const residue_const_iterator &b) {
        if (a.outer_it_ != b.outer_it_) return false;

        if (a.outer_it_ != a.outer_end_ && b.outer_it_ != b.outer_end_ && a.inner_it_ != b.inner_it_)
          return false;

        return true;
      }

      friend bool operator!=(const residue_const_iterator &a,const residue_const_iterator &b) { return !(a == b); }

  private:

      void advance_past_empty_inner_containers() {
        while (outer_it_ != outer_end_ && inner_it_ == (*outer_it_)->end()) {
          ++outer_it_;
          if (outer_it_ != outer_end_) inner_it_ = (*outer_it_)->begin();
        }
      }

      outer_iterator outer_it_;
      outer_iterator outer_end_;
      inner_iterator inner_it_;
    }; // ~ residue_const_iterator

  /// Iterator (non-const) to iterate over residues of this structure
  class residue_iterator {
  public:

      typedef std::vector<std::shared_ptr<Chain> >::iterator outer_iterator;
      typedef std::vector<std::shared_ptr<Residue> >::iterator inner_iterator;

      typedef std::forward_iterator_tag iterator_category;
      typedef inner_iterator::value_type value_type;
      typedef inner_iterator::difference_type difference_type;
      typedef inner_iterator::pointer pointer;
      typedef inner_iterator::reference reference;

      residue_iterator() { }
      residue_iterator(outer_iterator it) : outer_it_(it), outer_end_(it) { }
      residue_iterator(outer_iterator it, outer_iterator end) : outer_it_(it), outer_end_(end) {

        if (outer_it_ == outer_end_) return; inner_it_ = (*outer_it_)->begin();
        advance_past_empty_inner_containers();
      }

      reference operator*()  const { return *inner_it_;  }
      pointer operator->() const { return &*inner_it_; }

    friend residue_iterator operator+(residue_iterator lhs, const unsigned int rhs) {
      lhs += rhs;
      return lhs;
    }

    residue_iterator& operator+=(const unsigned int rhs){

      for(unsigned int  i=0;i<rhs;++i)  ++*this;
      return *this;
    }

    residue_iterator& operator++ () {
        ++inner_it_;
        if (inner_it_ == (*outer_it_)->last_atom()) advance_past_empty_inner_containers();

        return *this;
      }

      residue_iterator operator++ (int) {
        residue_iterator it(*this);
        ++*this;

        return it;
      }

      friend bool operator==(const residue_iterator &a, const residue_iterator &b) {
        if (a.outer_it_ != b.outer_it_) return false;

        if (a.outer_it_ != a.outer_end_ && b.outer_it_ != b.outer_end_ && a.inner_it_ != b.inner_it_)
          return false;

        return true;
      }

      friend bool operator!=(const residue_iterator &a,const residue_iterator &b) { return !(a == b); }

  private:

      void advance_past_empty_inner_containers() {
        while (outer_it_ != outer_end_ && inner_it_ == (*outer_it_)->end()) {
          ++outer_it_;
          if (outer_it_ != outer_end_) inner_it_ = (*outer_it_)->begin();
        }
      }

      outer_iterator outer_it_;
      outer_iterator outer_end_;
      inner_iterator inner_it_;
    }; // ~ residue_iterator

  // public data
  std::multimap<std::string, std::shared_ptr<core::data::io::PdbField>> pdb_header;

  // ---------- C-tor ----------
  /** \brief Creates a structure with no atoms.
   *
   * @param code - a structure ID, preferably 4-character string so it looks like a PDB code
   */
  Structure(const std::string &code) : code_(code), logger("Structure") { }

  /** @brief Creates a new Structure as a deep copy of this object.
   *
   * This method makes also a deep copy of chains  that belong to this structure (with their residues and atoms, accordingly)
   */
  Structure_SP clone() const;

  /** @brief Creates a new Structure as a deep copy of this object.
   *
   * This method makes also a deep copy of chains  that belong to this structure (with their residues and atoms, accordingly)
   * providing that they satisfy a given selector.
   *
   * @param which_chains - clone also chains, residues and atoms which satisfy the given selector
   */
  Structure_SP clone(const selectors::ChainSelector &which_chains) const;

  // ---------- Getters ----------
  /** @brief Returns true if this is a crystal structure.
   *
   * This method simply checks what the data parsed from PDB header; remember to set header parsing flag
   * to <code>true</code> while reading a PDB file. Otherwise this method will always return false!
   *
   * @return true if this structure has been determined by crystallography
   */
  bool is_xray() const;

  /** @brief Returns true if this is a NMR structure.
   *
   * This method simply checks what the data parsed from PDB header; remember to set header parsing flag
   * to <code>true</code> while reading a PDB file. Otherwise this method will always return false!
   *
   * @return true if this structure has been determined by NMR
   */
  bool is_nmr() const;

  /** @brief Returns true if this is an EM structure.
   *
   * This method simply checks what the data parsed from PDB header; remember to set header parsing flag
   * to <code>true</code> while reading a PDB file. Otherwise this method will always return false!
   *
   * @return true if this structure has been determined by EM
   */
  bool is_em() const;

  /// Returns the PDB code of this deposit
  inline const std::string & code() const { return code_; }

  /// Returns resolution of this deposit, if applicable
  double resolution() const;

  /// Returns R-value of this deposit, if applicable
  double r_value() const;

  /// Returns free R-value of this deposit, if applicable
  double r_free() const;

  /// Returns classification of this biomolecule, as stored in the PDB header
  std::string classification() const;

  /// Returns the title of this deposit, as stored in the PDB header
  std::string title() const;

  /// Returns deposition date of this PDB deposit
  std::string deposition_date() const;

  /** @brief Returns definition of this compound, as stored in PDB file header
   * @return COMPND as a string
   */
  std::string compound() const;

  /// Returns a list of keywords assigned to this PDB deposit
  const std::vector<std::string> & keywords() const;

  /** @brief Returns a chemical formula of a heteroatom group, as stored in this PDB file header (FORMUL field)
   * @param code3 - three-lette code of a monomer
   * @return string holding the formula, e.g. <code>C10 H17 N2 O14 P3</code> tor TTP residue
   */
  std::string formula(const std::string & code3) const;

  /** @brief Returns a chemical name of a heteroatom group, as stored in this PDB file header (FORMUL field)
   * @param code3 - three-lette code of a monomer
   * @return string holding the chemical name, e.g. <code>THYMIDINE-5'-TRIPHOSPHATE</code> tor TTP residue
   */
  std::string hetname(const std::string & code3) const;

  /** @brief Returns true if this Structure has a chain identified by a given string.
   * @param code - chainId
   * @return true if the chain was found in this structure; false otherwise
   */
  bool has_chain(const std::string & code) const;

  /** @brief Returns true if this Structure has a chain identified by a given character.
   * @param code - chainId
   * @return true if the chain was found in this structure; false otherwise
   */
  bool has_chain(const char code) const;

  /** @brief Returns the chain for the given index.
   * Throws an exception, if the chain index is too high
   * @param index - index of the requested chain
   * @return pointer to the relevant chain
   */
  std::shared_ptr<Chain> get_chain(const core::index2 index) { return at(index); }

  /** @brief Returns the const-pointer to a chain for the given index.
   * Throws an exception, if the chainId is invalid
   * @param code - chainId
   * @return pointer to the relevant chain
   */
  const std::shared_ptr<Chain> get_chain(const std::string & code) const;

  /** @brief Returns the chain for the given code.
   * Throws an exception, if the chain code is invalid i.e. there is no such chain in this Structure object
   * @param code - chainId
   * @return pointer to the relevant chain
   */
  std::shared_ptr<Chain> get_chain(const std::string & code);

  /** @brief Returns a string that contains chain code string
   * The chain IDs are separated by a comma, e.g. "A", "A,B,C", "AB,AC,AD"
   * @return codes of all chains of this structure as a single string
   */
  std::vector<std::string> chain_codes() const;

  /** @brief Returns a pointer to a residue from this Structure
   * Throws an exception, if the chain code is invalid i.e. there is no such chain in this Structure object
   * @param chain_code - chainId the residue belongs to
   * @param residue_id - resId of the residue
   * @param icode - insertion code for the requested residue
   * @return pointer to the relevant residue
   */
  Residue_SP get_residue(const std::string & chain_code, const core::index2 residue_id, const char icode);

  /** @brief Returns a const-pointer to a residue from this Structure
   * Throws an exception, if the chain code is invalid i.e. there is no such chain in this Structure object
   * @param chain_code - chainId the residue belongs to
   * @param residue_id - resId of the residue
   * @param icode - insertion code for the requested residue
   * @return const-pointer to the relevant residue
   */
  const Residue_SP get_residue(const std::string & chain_code, const core::index2 residue_id, const char icode) const;

  /** @brief Returns a const-pointer to a residue requested by its index.
   *
   * @param residue_index - index of the residue of interest; It starts from zero and goes continuously through all chains
   * @return a const-pointer to the requested residue
   */
  const Residue_SP get_residue(const core::index2 residue_index) const;

  /** @brief Returns a pointer to a residue requested by its index.
   *
   * @param residue_index - index of the residue of interest; It starts from zero and goes continuously through all chains
   * @return a pointer to the requested residue
   */
  Residue_SP get_residue(const core::index2 residue_index);

  /** @brief Returns a vector of residues that form a contiguous range in this structure.
   * @param first_residue - the first residue of a range
   * @param last_residue  - the last residue of a range
   * @return a vector of all residues starting from <code>first_residue</code>, ending with <code>last_residue</code>;
   *    both <code>first_residue</code> and <code>last_residue</code> are returned, the order of residues
   *    in the returned vector is the same as the order of residues in this structure
   */
  std::vector<Residue_SP> get_residue_range(const Residue_SP first_residue, const Residue_SP last_residue) const;

  /** @brief Finds all residues that satisfy the given selector
   * @param selector - what to look for
   * @return a vector that holds results of the search
   */
  std::vector<Residue_SP> find_residues(selectors::ResidueSelector & selector);

  /** @brief Finds all residues that satisfy the given selector
   * @param selector - what to look for
   * @param out - pointers to residues that satisfy the given selector will be appended to this vector;
   *    the vector is not cleared by this method
   * @return reference to the vector that holds results of the search
   */
  std::vector<Residue_SP> & find_residues(selectors::ResidueSelector & selector, std::vector<Residue_SP> & out);

  /** @brief Finds all atoms that satisfy the given selector
   * @param selector - what to look for
   * @param out - pointers to atoms that satisfy the given selector will be appended to this vector;
   *    the vector is not cleared by this method
   * @return reference to the vector that holds results of the search
   */
  std::vector<PdbAtom_SP> & find_atoms(selectors::AtomSelector & selector, std::vector<PdbAtom_SP> & out);

  /** @brief Finds all atoms that satisfy the given selector
   * @param selector - what to look for
   * @return a vector that holds results of the search
   */
  std::vector<PdbAtom_SP> find_atoms(selectors::AtomSelector & selector);

  /// Returns the polymer sequence as stored in SEQRES field in the source PDB file
  std::vector<core::chemical::Monomer> & original_sequence(const char chain_code);

  // ---------- Setters ----------
  inline void code(const std::string & new_code) { code_ = new_code; }

	// ---------- Chain tree operations ----------
  /// begin() iterator for residues
	inline Structure::residue_iterator  first_residue() { return residue_iterator(begin(),end()); }

  /// end() iterator for residues
	inline Structure::residue_iterator  last_residue() { return residue_iterator(end()); }

  /// begin() const iterator for residues
  inline Structure::residue_const_iterator  first_const_residue() const { return residue_const_iterator(begin(),end()); }

  /// end() const iterator for residues
  inline Structure::residue_const_iterator  last_const_residue() const { return residue_const_iterator(end()); }

  /// begin() iterator for atoms
  inline Structure::atom_iterator  first_atom() { return atom_iterator(begin(),end()); }

  /// end() const iterator for atoms
  inline Structure::atom_iterator  last_atom() { return atom_iterator(end()); }

  /// begin() const iterator for atoms
  inline Structure::atom_const_iterator  first_const_atom() const { return atom_const_iterator(begin(),end()); }

  /// end() iterator for atoms
  inline Structure::atom_const_iterator  last_const_atom() const { return atom_const_iterator(end()); }

  /// Append a new chain to this structure
  void push_back(std::shared_ptr<Chain> c);

  // ---------- Misc operations ----------
	/// Returns the number of atoms that belong to this structure
	inline core::index4 count_atoms() const {
		core::index4 sum=0;
		for(std::vector<std::shared_ptr<Chain> >::const_iterator it=begin();it!=end();++it)
			sum +=(*it)->count_atoms();
		return sum;
	}

	/// Returns the number of residues that belong to this structure
	inline core::index2 count_residues() const {
		core::index2 sum=0;
		for(std::vector<std::shared_ptr<Chain> >::const_iterator it=begin();it!=end();++it)
			sum +=(*it)->count_residues();
		return sum;
	}

	/// Returns the number of chains that belong to this structure
	inline core::index2 count_chains() const { return size(); }

  /** @brief Returns true if any Chain of this Structure contain secondary structure information.
   * @return true if at least one chain of this structure contains any residue annotated as helix or strand
   */
  bool has_secondary_structure() const;

  /** @brief Sort chains, residues and atoms of this structure.
   *
   * Chains are sorted lexicographically. Atoms are sorted by their <code>id</code>. Residues are sorted by the means of
   * <code>bool operator<(const Residue & ri, const Residue & rj)</code> operator.
   */
  void sort();

  /** @brief Creates a vector of secondary structure objects: helices and strands.
   *
   * The objects are created based on secondary structure assignment read from a PDB file header
   * @return a newly allocated vector of all secondary structure elements in this structure
   */
  std::vector<SecondaryStructureElement_SP> create_sse();

private:
  std::string code_;
  utils::Logger logger;
  std::vector<std::string> empty_vector_;

  /** @brief Extracts a requested PdbField element from this structure's header; returns nullptr if not present
   *
   * Note, that only the first PdbField element is returned, event if more than one are present. Other may be directly
   * accessed from <code>pdb_header<code> std::multimap object
   * @tparam T - a type derived from PdbField
   * @param field_name - the name of a PDB field, e.g. HELIX or HEAD
   * @return pointer to the field or nullptr if not found
   */
  template<typename T>
  const std::shared_ptr<T> get_pdb_field(const std::string & field_name) const;
};

template<typename T>
const std::shared_ptr<T> Structure::get_pdb_field(const std::string & field_name) const {

  auto range = pdb_header.equal_range(field_name);
  if (range.first == range.second) return nullptr; // --- nothing found!
  return std::static_pointer_cast<T>(range.first->second);
}

/** \brief Copy coordinates of all atoms of a Coordinates object (which in fact is a std::vector<Vec3>) into a given Structure.
 *
 * This method does not check if the destination vector has sufficient size.
 * @param coordinates - source vector of coordinates
 * @param structure - destination structure
 */
size_t coordinates_to_structure(const core::data::basic::Coordinates & coordinates, Structure & structure);

/** \brief Copy coordinates of all atoms of this structure into a given Coordinates object (which in fact is  std::vector<Vec3>).
 *
 * This method does not check if the destination vector has sufficient size.
 * @param structure - the source of atoms
 * @param coordinates - destination vector
 * @return the number of atoms copied
 */
size_t structure_to_coordinates(const Structure_SP structure, core::data::basic::Coordinates & coordinates);

/** \brief Copy coordinates of the matching atoms of this structure into a given Coordinates object (which in fact is  std::vector<Vec3>).
 *
 * This method does not check if the destination vector has sufficient size.
 * @param structure - the source of atoms
 * @param coordinates - destination vector
 * @param op - atom selector (boolean operator) used to pick the atoms of interest. E.g. use core::data::structural::IsCA to copy coordinates of alpha-carbons only.
 * @return the number of atoms copied
 */
size_t structure_to_coordinates(const Structure_SP structure, core::data::basic::Coordinates & coordinates,
    const selectors::AtomSelector_SP op);

/** \brief Copy coordinates of the matching atoms of this structure into a given Coordinates object (which in fact is  std::vector<Vec3>).
 *
 * This method does not check if the destination vector has sufficient size.
 * @param structure - the source of atoms
 * @param coordinates - destination vector
 * @param op - atom selector (boolean operator) used to pick the atoms of interest. E.g. use core::data::structural::IsCA to copy coordinates of alpha-carbons only.
 * @return the number of atoms copied
 */
size_t structure_to_coordinates(const Structure_SP structure, core::data::basic::Coordinates & coordinates,
    const selectors::AtomSelector & op);
}
}
}

#endif

/**
 * \example ex_Structure.cc
 */
