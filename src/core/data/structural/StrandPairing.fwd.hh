#ifndef CORE_DATA_STRUCTURAL_StrandPairing_FWD_HH
#define CORE_DATA_STRUCTURAL_StrandPairing_FWD_HH

#include <memory>

#include <core/data/structural/Strand.hh>
#include <core/data/structural/SecondaryStructureElement.fwd.hh>
#include <core/calc/structural/interactions/BackboneHBondInteraction.hh>

namespace core {
namespace data {
namespace structural {

/// Declares StrandPairing type
class StrandPairing;

/// Declares a shared pointer to StrandPairing
typedef std::shared_ptr<StrandPairing> StrandPairing_SP;

}
}
}

#endif // --- CORE_DATA_STRUCTURAL_StrandPairing_FWD_HH
