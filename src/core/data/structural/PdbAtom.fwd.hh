#ifndef CORE_DATA_STRUCTURAL_PdbAtom_FWD_HH
#define CORE_DATA_STRUCTURAL_PdbAtom_FWD_HH

#include <memory>

namespace core {
namespace data {
namespace structural {

/// Forwarded declaration of the PdbAtom class
class PdbAtom;
/// Declaration of a shared pointer to the PdbAtom class
typedef std::shared_ptr<PdbAtom> PdbAtom_SP;

}
}
}

#endif
