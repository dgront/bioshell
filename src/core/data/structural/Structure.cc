#include <memory>
#include <stdexcept>
#include <algorithm>

#include <core/index.hh>

#include <core/data/io/Pdb.hh>

#include <core/data/basic/Vec3Cubic.hh>

#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

namespace core {
namespace data {
namespace structural {

bool Structure::is_xray() const {

  const auto & e = get_pdb_field<core::data::io::Expdata>("EXPDTA");
  return (e == nullptr) ? false : e->is_xray();
}

bool Structure::is_nmr() const {

  const auto & e = get_pdb_field<core::data::io::Expdata>("EXPDTA");
  return (e == nullptr) ? false : e->is_nmr();
}

bool Structure::is_em() const {

  const auto & e = get_pdb_field<core::data::io::Expdata>("EXPDTA");
  return (e == nullptr) ? false : e->is_em();
}

double Structure::resolution() const {
  const auto & e = get_pdb_field<core::data::io::Remark2>("REMARK2");
  return (e == nullptr) ? -1.0 : e->resolution;
}

double Structure::r_value() const {
  const auto & e = get_pdb_field<core::data::io::Remark3>("REMARK3");
  return (e == nullptr) ? -1.0 : e->r_value;
}

double Structure::r_free() const {
  const auto & e = get_pdb_field<core::data::io::Remark3>("REMARK3");
  return (e == nullptr) ? -1.0 : e->r_free;
}

std::string  Structure::classification() const {
  const auto & e = get_pdb_field<core::data::io::Header>("HEADER");
  return (e == nullptr) ? "UNKNOWN" : e->classification;
}

std::string  Structure::deposition_date() const {
  const auto & e = get_pdb_field<core::data::io::Header>("HEADER");
  return (e == nullptr) ? "UNKNOWN" : e->date;
}

std::string Structure::title() const {
  const auto & e = get_pdb_field<core::data::io::Title>("TITLE");
  return (e == nullptr) ? "UNKNOWN" : e->title();
}

std::string Structure::compound() const {

  const auto & e = get_pdb_field<core::data::io::Compnd>("COMPND");
  return (e == nullptr) ? "UNKNOWN" : e->compound_data();
}

const std::vector<std::string> & Structure::keywords() const {

  auto kwds = get_pdb_field<core::data::io::Keywords>("KEYWRDS");
  if(kwds == nullptr)
    return empty_vector_;
  else
    return kwds->keywords();
}

std::string Structure::formula(const std::string &code3) const {

  const auto it_pair = pdb_header.equal_range("FORMUL");
  for (auto it = it_pair.first; it != it_pair.second; ++it) {
    std::shared_ptr<core::data::io::Formula> f = std::static_pointer_cast<io::Formula>(it->second);
    if (f->code3 == code3) return f->formula;
  }
  return "UNKNOWN";
}

std::string Structure::hetname(const std::string &code3) const {

  const auto it_pair = pdb_header.equal_range("HETNAM");
  for (auto it = it_pair.first; it != it_pair.second; ++it) {
    std::shared_ptr<core::data::io::Formula> f = std::static_pointer_cast<io::Formula>(it->second);
    if (f->code3 == code3) return f->formula;
  }
  return "UNKNOWN";
}

Structure_SP Structure::clone() const {

  return clone(selectors::ChainSelector{});
}

Structure_SP Structure::clone(const selectors::ChainSelector &which_chains) const {

  Structure_SP ret = std::make_shared<Structure>(code_);

  for (const Chain_SP &ci : (*this))
    if (which_chains(*ci))
      ret->push_back(ci->clone(which_chains));

  return ret;
}

void Structure::push_back(std::shared_ptr<Chain> c) { c->owner_ = shared_from_this(); std::vector<Chain_SP>::push_back(c);}

void Structure::sort() {

  std::sort(begin(),end(),[](const Chain_SP & ai,const Chain_SP & aj){ return ai->id() < aj->id();});
  for(Chain_SP c : *this) c->sort();
}

bool Structure::has_chain(const std::string & code) const {
  for (const std::shared_ptr<Chain> & c : *this)
    if (c->id() == code) return true;

  return false;
}

bool Structure::has_chain(const char code) const {
  for (const std::shared_ptr<Chain> & c : *this)
    if ((c->id().size()==1) && (c->id()[0] == code)) return true;

  return false;
}

const std::shared_ptr<Chain> Structure::get_chain(const std::string & code) const {

  for(const std::shared_ptr<Chain> & c :*this) {
    if(c->id()==code) return c;
  }
  std::string codes;
  for(const Chain_SP & c:*this) codes +=c->id();
  std::string msg = std::string("Invalid chain code: '") + code + std::string("'; Registered chain codes: " + codes) + std::string("\n");
  throw std::invalid_argument(msg);
}

std::shared_ptr<Chain> Structure::get_chain(const std::string& code) {

  for (std::shared_ptr<Chain> &c :*this) {
    if (c->id() == code) return c;
  }
  std::string codes;
  for(const Chain_SP & c:*this) codes +=c->id();
  std::string msg = std::string("Invalid chain code: '") + code + std::string("'; Registered chain codes: " + codes)
      + std::string("\n");
  logger << utils::LogLevel::CRITICAL << msg;
  throw std::invalid_argument(msg);
}

std::vector<std::string> Structure::chain_codes() const {

  std::vector<std::string> out;
  for (size_t i = 0; i < size(); ++i)
    out.push_back(this->operator[](i)->id());
  return out;

}

Residue_SP Structure::get_residue(const std::string & chain_code, const core::index2 residue_id, const char icode) {

  for(auto & r : *get_chain(chain_code))
    if((r->id()==residue_id)&&(r->icode()==icode)) return r;
  throw std::invalid_argument("Invalid residue request");
}

const Residue_SP Structure::get_residue(const std::string & chain_code, const core::index2 residue_id, const char icode) const {

  for(const auto & r : *get_chain(chain_code))
    if((r->id()==residue_id)&&(r->icode()==icode)) return r;
  throw std::invalid_argument("Invalid residue request");
}

Residue_SP Structure::get_residue(const core::index2 residue_index) {

  core::index2 total_res = 0;
  short ichain=-1;
  do {
    total_res = operator[](++ichain)->count_residues();
  } while(total_res<residue_index);

  return operator[](ichain)->operator[](residue_index - total_res + operator[](ichain)->count_residues());
}

std::vector<Residue_SP> Structure::get_residue_range(const Residue_SP first_residue,
                                                     const Residue_SP last_residue) const {
  std::vector<Residue_SP> residues;       // --- the vector to be returned
  residues.push_back(first_residue);      // --- push the very first
  Residue_SP first = first_residue;
  while (first != last_residue) {         // --- iterate to the end of the range
    Residue_SP first_tmp = first->next(); // --- get the next residue
    if (first_tmp == nullptr) {           // --- in might be null when we hit the C-terminus
      const Chain_SP nc = first->owner()->next(); // --- if so, get the next chain
      if (nc != nullptr)                  // --- if we have the next chain, get its first residue
        first_tmp = (*nc)[0];
    }
    if (first_tmp != nullptr) {           // --- append the next residue
      first = first_tmp;
      residues.push_back(first);
    }
    else break;                           // --- next residue not found, stop iteration
  }

  return residues;
}

const Residue_SP Structure::get_residue(const core::index2 residue_index) const {

  core::index2 total_res = 0;
  short ichain=-1;
  do {
    total_res += operator[](++ichain)->count_residues();
  } while(total_res<=residue_index);

  core::index4 ipos = operator[](ichain)->count_residues() + residue_index - total_res;
  auto ret = operator[](ichain)->operator[](ipos);
  return ret;
}

std::vector<Residue_SP> & Structure::find_residues(selectors::ResidueSelector & selector, std::vector<Residue_SP> & out) {

  for (auto r_it = first_residue(); r_it != last_residue(); ++r_it)
    if (selector(**r_it)) out.push_back(*r_it);

  return out;
}

std::vector<Residue_SP> Structure::find_residues(selectors::ResidueSelector & selector) {

  std::vector<Residue_SP> out;
  return find_residues(selector,out);
}

std::vector<PdbAtom_SP> & Structure::find_atoms(selectors::AtomSelector & selector, std::vector<PdbAtom_SP> & out) {

  for (auto a_it = first_atom(); a_it != last_atom(); ++a_it)
    if (selector(**a_it)) out.push_back(*a_it);

  return out;
}

std::vector<PdbAtom_SP> Structure::find_atoms(selectors::AtomSelector & selector) {

  std::vector<PdbAtom_SP> out;
  return find_atoms(selector,out);
}

std::vector<core::chemical::Monomer> & Structure::original_sequence(const char chain_code) {

  static const std::string key("SEQRES");
  auto it = pdb_header.find(key);
  if (it != pdb_header.end()) {
    const std::shared_ptr<io::Seqres> & seqr_sp = std::dynamic_pointer_cast<io::Seqres>((it->second));
    return seqr_sp->sequences[chain_code];
  } else throw std::runtime_error("Input PDB file has no SEQRES data");
}

bool Structure::has_secondary_structure() const {

  for(const Chain_SP ci : *this)
    if (ci->has_secondary_structure()) return true;
  return false;
}

std::vector<SecondaryStructureElement_SP> Structure::create_sse() {

  std::vector<SecondaryStructureElement_SP> sse;
  for (const Chain_SP &c:*this) c->create_sse(sse);

  return sse;
}

size_t coordinates_to_structure(const core::data::basic::Coordinates & coordinates, Structure & structure) {

  int i = -1;
  std::for_each(structure.first_atom(), structure.last_atom(),
    [&](core::data::structural::PdbAtom_SP a) {(*a).set(coordinates[++i]);});

  return (++i);
}

size_t structure_to_coordinates(const Structure_SP structure, core::data::basic::Coordinates & coordinates) {

  if (coordinates.size() != structure->count_atoms()) coordinates.resize(structure->count_atoms());
  int i = -1;
  std::for_each(structure->first_atom(), structure->last_atom(),
    [&](core::data::structural::PdbAtom_SP a) {coordinates[++i].set(*a);});

  return (++i);
}

template <typename C>
size_t structure_to_coordinates(const Structure_SP structure, std::unique_ptr<C[]>  & coordinates) {

  int i = -1;
  std::for_each(structure->first_atom(), structure->last_atom(),
    [&](core::data::structural::PdbAtom_SP a) {coordinates[++i].set(*a);});

  return (++i);
}

template
size_t structure_to_coordinates(const Structure_SP structure, std::unique_ptr<basic::Vec3[]>  & coordinates);
template
size_t structure_to_coordinates(const Structure_SP structure, std::unique_ptr<basic::Vec3Cubic[]>  & coordinates);


template <typename C>
size_t coordinates_to_structure(const std::unique_ptr<C[]> &coordinates, Structure & structure) {

  int i = -1;
  std::for_each(structure.first_atom(), structure.last_atom(), [&](core::data::structural::PdbAtom_SP a) {a->set(coordinates[++i]);});

  return (++i);
}

template
size_t coordinates_to_structure(const std::unique_ptr<basic::Vec3[]> &coordinates, Structure & structure);

template
size_t coordinates_to_structure(const std::unique_ptr<basic::Vec3Cubic[]> &coordinates, Structure & structure);


size_t structure_to_coordinates(const Structure_SP structure, core::data::basic::Coordinates & coordinates,
    const selectors::AtomSelector_SP op) {

  if (op == nullptr) return structure_to_coordinates(structure, coordinates);

  return structure_to_coordinates(structure,coordinates,*op);
}

size_t structure_to_coordinates(const Structure_SP structure, core::data::basic::Coordinates & coordinates,
    const selectors::AtomSelector & op) {

  utils::Logger l("structure_to_coordinates");

  size_t cnt = 0;
  for (auto it = structure->first_atom(); it != structure->last_atom(); ++it)
    if ((op)(**it)) cnt++;

  if (cnt != coordinates.size()) {
    l << utils::LogLevel::INFO << "Atomic coordinates for structure " << structure->code() << " " << cnt
        << " atoms found, vector resized from " << coordinates.size() << "\n";
    coordinates.resize(cnt);
  } else l << utils::LogLevel::FINE << cnt << " atomic coordinates for structure " << structure->code() << "\n";

  int i = -1;
  l << utils::LogLevel::FINE << "Selecting atoms:\n\t";
  for (auto it = structure->first_atom(); it != structure->last_atom(); ++it) {
    if ((op)(**it)) {
      coordinates[++i].set(**it);
      l << (*it)->atom_name() << " ";
    }
  }
  l << utils::LogLevel::FINE << "\n";

  return (++i);
}

}
}
}
