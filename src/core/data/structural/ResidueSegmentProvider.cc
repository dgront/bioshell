#include <algorithm>
#include <vector>

#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/ResidueSegmentProvider.hh>

namespace core {
namespace data {
namespace structural {

bool ResidueSegmentProvider::find_next() {

  Chain_SP c = strctr_->get_chain(current_chain_);
  if (current_residue_ + length <= c->size())
    return true;

  ++current_chain_;
  while(current_chain_ < strctr_->count_chains() && strctr_->get_chain(current_chain_)->size() < length) ++current_chain_;

  if (current_chain_ < strctr_->count_chains()) {
    logger << utils::LogLevel::INFO << "Segments of chain " << strctr_->get_chain(current_chain_)->id() << "\n";
    current_residue_ = 0;
    return true;
  }
  else return false;
}

ResidueSegment_SP ResidueSegmentProvider::next() {

  Chain_SP c = strctr_->get_chain(current_chain_);
  ResidueSegment_SP rs = std::make_shared<ResidueSegment>(c, current_residue_, current_residue_ + length - 1);
  ++current_residue_;
  has_next_ = find_next();

  return rs;
}

ResidueSegment_SP ResidueSegmentProvider::peek() {

    Chain_SP c = strctr_->get_chain(current_chain_);
    ResidueSegment_SP rs = std::make_shared<ResidueSegment>(c, current_residue_, current_residue_ + length - 1);
    has_next_ = find_next();

    return rs;
}

}
}
}
