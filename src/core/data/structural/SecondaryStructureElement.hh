#ifndef CORE_DATA_STRUCTURAL_SecondaryStructureElement_HH
#define CORE_DATA_STRUCTURAL_SecondaryStructureElement_HH

#include <algorithm>
#include <vector>

#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/ResidueSegment.hh>
#include <core/data/structural/SecondaryStructureElement.fwd.hh>

namespace core {
namespace data {
namespace structural {

/** @brief Base class for secondary structure elements.
 * @see Helix Strand classes
 */
class SecondaryStructureElement : public ResidueSegment {
public:

  /** @brief Constructor makes a deep copy of the given residue pointers.
   * @param residues - residues that constitute this secondary structure element
   */
  SecondaryStructureElement(const std::vector<Residue_SP> &residues) : ResidueSegment(residues) {}

  /** @brief Constructor makes a deep copy of the given residue pointers.
   * @param residues_begin - iterator pointing to the first residue in this secondary structure element
   * @param residues_end - passed-the-end iterator referring to the last residue in this secondary structure element
   */
  template<typename It>
  SecondaryStructureElement(const It &residues_begin, const It &residues_end) : ResidueSegment(residues_begin, residues_end) {}

  /// Virtual destructor (empty)
  virtual ~SecondaryStructureElement() = default;

  /// Returns the character encoding the type of this secondary structure element: either 'H', 'E' or 'C'
  virtual char hec_code() const = 0;

  /// Returns a string information about this element, e.g. "H : A 23ALA - A 35ASN RightAlpha" for a helix
  virtual std::string info() const = 0;

  /** @brief Sets a new name of this SSE
   *
   * @param new_name - a string used to identify this SSE in outputs
   */
  void name(const std::string & new_name) { name_ = new_name; }

  /** @brief Provides the  name of this SSE
   *
   * @return a string used to identify this SSE in outputs
   */
  const std::string & name() const { return name_; }

  /** @brief  Prints information about this secondary structure element
   *
   * @param str - output stream
   * @param sse - object to be printed
   * @return the reference to the output stream
   */
  friend std::ostream& operator<<(std::ostream& str, SecondaryStructureElement const& sse) {

    str << sse.info();
    return str;
  }

protected:
  /// Name of this secondary structure element (used for output)
  std::string name_;
};

/** @brief Define order of Strands by the order they appear in the sequence of a protein
 *
 * @param lhs - a SecondaryStructureElement to be compared
 * @param rhs - a SecondaryStructureElement to be compared
 * @return true if the lhs SSE is closer to N-termini of a protein than rhs SSE
 */
inline bool operator <(const SecondaryStructureElement& lhs, const SecondaryStructureElement& rhs) {
  if (lhs.front()->owner()->id() != rhs.front()->owner()->id())
    return lhs.front()->owner()->id() < rhs.front()->owner()->id();
  if (lhs.back()->owner()->id() != rhs.back()->owner()->id())
    return lhs.back()->owner()->id() < rhs.back()->owner()->id();

  return lhs[0]->id() < rhs[0]->id();
}

/** @brief Define order of Strands by the order they appear in the sequence of a protein
 *
 * @param lhs - a SecondaryStructureElement to be compared
 * @param rhs - a SecondaryStructureElement to be compared
 * @return true if the lhs SSE is closer to N-termini of a protein than rhs SSE
 */
inline bool operator <(const SecondaryStructureElement_SP lhs, const SecondaryStructureElement_SP rhs) {

  return (*lhs) < (*rhs);
}

}
}
}

#endif // --- CORE_DATA_STRUCTURAL_SecondaryStructureElement_HH
