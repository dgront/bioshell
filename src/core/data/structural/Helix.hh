#ifndef CORE_DATA_STRUCTURAL_Helix_HH
#define CORE_DATA_STRUCTURAL_Helix_HH

#include <core/data/structural/Residue.hh>
#include <core/data/structural/SecondaryStructureElement.hh>

namespace core {
namespace data {
namespace structural {

/// Defines all allowed types of a helix
enum class HelixTypes : unsigned char {
  RightAlpha = 1, ///<    Right-handed alpha (default), PDB code       1, DSSP code H
  RightOmega = 2, //<    Right-handed omega, PDB code                 2
  RightPi = 3, //<    Right-handed pi, PDB code                    3 DSSP code I
  RightGamma = 4, //<    Right-handed gamma, PDB code                 4
  Right310 = 5, //<    Right-handed 310, PDB code                   5, DSSP code G
  LeftAlpha = 6, //<    Left-handed alpha, PDB code                  6,  DSSP code H
  LeftOmega = 7, //<    Left-handed omega, PDB code                  7
  LeftGamma = 8, //<    Left-handed gamma, PDB code                  8
  RibbonHelix = 9, //<    27 ribbon/helix, PDB code                    9
  Polyproline = 10 //<    Polyproline, PDB code                       10
};

/** @brief represents a single heliz of a protein structure
 *
 */
class Helix : public SecondaryStructureElement {
public:
  HelixTypes type; ///< Defines the type of this helix

  static const std::string & helix_type_name(const HelixTypes type);

  /**\brief Creates a helix containing given residues.
   * @param residues - all residues that belong to this helix
   * @param type - type of this helix: one of HelixTypes members
   */
  Helix(const std::vector<Residue_SP> &residues, const HelixTypes type) :
    SecondaryStructureElement(residues), type(type), logger("Helix") {
    logger << utils::LogLevel::FINE << "Creating a new helix of " << residues.size() << " residues : "
           << info() << "\n";
  }

  /**\brief Creates a helix containing given residues.
   * @param residues_begin - iterator pointing to the first residue in this helix
   * @param residues_end - passed-the-end iterator referring to the last residue in this helix
   * @param type - type of this helix: one of HelixTypes members
   */
  template<typename It>
  Helix(const It & residues_begin, const It & residues_end, const HelixTypes type) :
    SecondaryStructureElement(residues_begin,residues_end), type(type), logger("Helix") {
    logger << utils::LogLevel::FINE << "Creating a new helix of " << int(std::distance(residues_begin, residues_end))
           << " residues : " << info() << "\n";
  }

  virtual char hec_code() const { return 'H'; };

  virtual std::string info() const;

  virtual ~Helix() {}

private:
  utils::Logger logger;
  static std::map<HelixTypes, std::string> type_names;
  static std::map<HelixTypes, std::string> create_map();
};

}
}
}

#endif // --- CORE_DATA_STRUCTURAL_Helix_HH
