/** \file structure_selectors.fwd.hh
 * @brief Provides forward declarations for types declared in structure_selectors.hh
 */

#ifndef CORE_DATA_STRUCTURAL_SELECTORS_structure_selectors_FWD_HH
#define CORE_DATA_STRUCTURAL_SELECTORS_structure_selectors_FWD_HH

#include <memory>

namespace core {
namespace data {
namespace structural {
namespace selectors {

class AtomSelector;

class SelectAllAtoms;

/// shared pointer to AtomSelector type
typedef std::shared_ptr<core::data::structural::selectors::AtomSelector> AtomSelector_SP;

class IsCA;

class IsCB;

class IsBB;

class IsBBCB;

class IsNamedAtom;

class ResidueSelector;

/// shared pointer to ResidueSelector type
typedef std::shared_ptr<ResidueSelector> ResidueSelector_SP;

class SelectResidueRange;

class ChainSelector;

/// shared pointer to ChainSelector type
typedef std::shared_ptr<ChainSelector> ChainSelector_SP;

class SelectChainResidues;

class SelectChainResidueAtom;

class LogicalANDSelector;
}
}
}
}
#endif
