#include <memory>

#include <core/data/structural/Residue.hh>
#include <core/data/structural/selectors/SelectChainBreaks.hh>

namespace core {
namespace data {
namespace structural {
namespace selectors {

bool SelectChainBreaks::operator()(const Residue &r) const {

  const_cast<SelectChainBreaks*>(this)->left_side_distance = 0.0;
  const_cast<SelectChainBreaks*>(this)->right_side_distance = 0.0;

  if (r.residue_type().type != 'P') return false;
  if ((r.next() == nullptr) && (r.previous() == nullptr)) {
    const_cast<SelectChainBreaks*>(this)->last_chainbreak_type = BOTH;
    return true;
  }
  if (r.next() == nullptr) {
    const_cast<SelectChainBreaks*>(this)->last_chainbreak_type = RIGHT;
    return true;
  }
  if (r.previous() == nullptr) {
    const_cast<SelectChainBreaks*>(this)->last_chainbreak_type = LEFT;
    return true;
  }
  const auto ca = r.find_atom(" CA ");
  if (ca == nullptr) {
    const_cast<SelectChainBreaks*>(this)->last_chainbreak_type = BOTH;
    return true;
  }

  const auto prev_ca = r.previous()->find_atom(" CA ");
  const auto next_ca = r.next()->find_atom(" CA ");
  if ((next_ca == nullptr) && (prev_ca == nullptr)) {
    const_cast<SelectChainBreaks*>(this)->last_chainbreak_type = BOTH;
    return true;
  }
  if (next_ca == nullptr) {
    const_cast<SelectChainBreaks*>(this)->last_chainbreak_type = RIGHT;
    return true;
  }
  if (prev_ca == nullptr) {
    const_cast<SelectChainBreaks*>(this)->last_chainbreak_type = LEFT;
    return true;
  }

  if ((prev_ca->distance_to(*ca) > MAXIMUM_CA_CA_DISTANCE) && (next_ca->distance_to(*ca) > MAXIMUM_CA_CA_DISTANCE)) {
    const_cast<SelectChainBreaks*>(this)->left_side_distance = prev_ca->distance_to(*ca);
    const_cast<SelectChainBreaks*>(this)->right_side_distance = next_ca->distance_to(*ca);

    const_cast<SelectChainBreaks *>(this)->last_chainbreak_type = BOTH;
    return true;
  }

  if (prev_ca->distance_to(*ca) > MAXIMUM_CA_CA_DISTANCE) {
    const_cast<SelectChainBreaks*>(this)->last_chainbreak_type = LEFT;
    const_cast<SelectChainBreaks*>(this)->left_side_distance = prev_ca->distance_to(*ca);
    return true;
  }
  if (next_ca->distance_to(*ca) > MAXIMUM_CA_CA_DISTANCE) {
    const_cast<SelectChainBreaks*>(this)->last_chainbreak_type = RIGHT;
    const_cast<SelectChainBreaks*>(this)->right_side_distance = next_ca->distance_to(*ca);
    return true;
  }
  return false;
}

}
}
}
}
