/** @file SelectPlanarCAGeometry.hh
 *  @brief Provides SelectPlanarCAGeometry selector that selects residues where geometry at C\f$\alpha\f$ atom deviates from tetragonal
 */
#ifndef CORE_DATA_STRUCTURAL_SELECTORS_SelectPlanarCAGeometry_H
#define CORE_DATA_STRUCTURAL_SELECTORS_SelectPlanarCAGeometry_H

#include <core/data/structural/Residue.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

namespace core {
namespace data {
namespace structural {
namespace selectors {

/** @brief Selects residues where geometry at C\f$\alpha\f$ atom deviates from tetragonal.
 *
 * The selector calculates dihedral angle between N, C\f$\alpha\f$ , C plane and C\f$\beta\f$ atom
 * which ideally should be
 */
class SelectPlanarCAGeometry  : public ResidueSelector {
public:

  constexpr static const double IDEAL_VALUE_RADIANS = -2.12;
  constexpr static const double ALLOWED_DEVIATION = 0.2;
  /// For proline, the distribution is bimodal: one peak for cis and the other for trans conformation; here in case
  constexpr static const double IDEAL_VALUE_RADIANS_PRO = -2.0;
  constexpr static const double ALLOWED_DEVIATION_PRO = 0.2;

  /** @brief Tests whether C\f$\alpha\f$  at a given residue is actually flat
  *
  * @param r - refers to the tested residue
   * @return true if a given residue has incorrect geometry, false if the geometry is OK
  */
  virtual bool operator()(const Residue & r) const;

  /** @brief Tests whether C\f$\alpha\f$  at a given residue is actually flat
  *
  * @param r - points to the tested residue
   * @return true if a given residue has incorrect geometry, false if the geometry is OK
  */
  virtual bool operator()(const Residue_SP r) const { return operator()(*r); }

  /** @brief Tests whether C\f$\alpha\f$  at a given residue is actually flat
   *
   * @param a - points to a tested atom
   * @return true if a given residue has incorrect geometry, false if the geometry is OK
   */
  virtual bool operator()(const PdbAtom & a) const { return operator()(a.owner()); }

  /// Evaluates the dihedral angle that is used to test the planarity
  double evaluate_angle(const Residue &r) const;

  /// This method has empty implementation so far
  virtual void set(const std::string & new_selection) { }

  virtual ~SelectPlanarCAGeometry() {}
};

}
}
}
}
#endif
