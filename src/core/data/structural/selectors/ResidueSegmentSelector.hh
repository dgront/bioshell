#ifndef CORE_DATA_STRUCTURAL_ResidueSegmentSelector_H
#define CORE_DATA_STRUCTURAL_ResidueSegmentSelector_H

#include <string>

#include <core/index.hh>
#include <core/data/structural/selectors/structure_selectors.fwd.hh>
#include <core/data/structural/ResidueSegment.hh>

namespace core {
namespace data {
namespace structural {
namespace selectors {

class ResidueSegmentSelector {
public:

  /** @brief Virtual function to select or unselect a residue segment.
   *
   * The base class method always returns true
   * @param rs - a reference to a segment
   * @return true if this segment satisfy this selector's criteria
   */
  virtual bool operator()(const core::data::structural::ResidueSegment &rs) const  { return true; }

  /// Default destructor
  virtual ~ResidueSegmentSelector() = default;
};

class DecoratedResidueSegmentSelector : public ResidueSegmentSelector {
public:

  DecoratedResidueSegmentSelector(const ResidueSegmentSelector &rs) : rs_(rs) { }

  /** @brief Virtual function to select or unselect a residue segment
   * @param rs - a reference to a segment
   * @return true if this segment satisfy this selector's criteria
   */
  virtual bool operator()(const core::data::structural::ResidueSegment &rs) const { return rs_(rs); }

  /// Default destructor
  virtual ~DecoratedResidueSegmentSelector() = default;

protected:
  const ResidueSegmentSelector & rs_;
};


class HasProperlyConnectedCA : public ResidueSegmentSelector {
public:
  bool operator()(const core::data::structural::ResidueSegment &rs) const override;
};


/** @brief Checks whether all residues are in a given secondary structure type.
 *
 * To enforce proper CA connections, use HasProperlyConnectedCA object as the constructor argument
 */
class IsSSType : public DecoratedResidueSegmentSelector {
public:

  explicit IsSSType(const ResidueSegmentSelector & rs, const char ss) : DecoratedResidueSegmentSelector(rs) { ss_ = ss; }

  virtual bool operator()(const core::data::structural::ResidueSegment &rs) const;

private:
  char ss_;
};

/** @brief Checks whether all residues satisfy a given ResidueSelector rule.
 *
 * To enforce other rule befor this condition is tested (e.g. proper CA connections),
 * pass the relevant ResidueSegmentSelector  object as the constructor argument.
 */
class AllResiduesSelected : public DecoratedResidueSegmentSelector {
public:

  explicit AllResiduesSelected(const ResidueSegmentSelector & rs, ResidueSelector_SP sel) :
    DecoratedResidueSegmentSelector(rs) { sel_ = sel; }

  virtual bool operator()(const core::data::structural::ResidueSegment &rs) const;

private:
  ResidueSelector_SP sel_;
};

/** @brief Checks whether sequence of this fragment matches a given pattern
 *
 */
class HasSequence : public DecoratedResidueSegmentSelector {
public:

  HasSequence(const ResidueSegmentSelector & rs, const std::string & seq) : DecoratedResidueSegmentSelector(rs) { sequence_ = seq; }

  virtual bool operator()(const core::data::structural::ResidueSegment &rs) const;

private:
  std::string sequence_;
};

/** @brief Checks whether all residues are in a given secondary structure type.
 *
 * To enforce proper CA connections, use HasProperlyConnectedCA object as the constructor argument
 */
class HasSecondary : public DecoratedResidueSegmentSelector {
public:

  HasSecondary(const ResidueSegmentSelector & rs, const std::string & sec) : DecoratedResidueSegmentSelector(rs) { secondary_ = sec; }

  virtual bool operator()(const core::data::structural::ResidueSegment &rs) const;

private:
  std::string secondary_;
};

}
}
}
}
#endif
