/** \file SelectClashingResidues.hh
 * @brief A base class for selectors that use kd-tree to select objects by spatial neighborhood
 */

#ifndef CORE_DATA_STRUCTURAL_SELECTORS_SelectClashingResidues_H
#define CORE_DATA_STRUCTURAL_SELECTORS_SelectClashingResidues_H

#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Structure.fwd.hh>
#include <core/algorithms/trees/kd_tree.hh>

#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/interactions/PairwiseResidueMap.hh>

namespace core {
namespace data {
namespace structural {
namespace selectors {

/** @brief Selector that detects clashes between residues
 */
class SelectClashingResidues : public ResidueSelector {
public:

  /** @brief Look for clashes in a given structure for a given clashing distance.
   *
   *  Any two atoms that are closer to each other than the given distance cutoff are said to be in a clash
   *  \todo_code clash detection should be improved! (hydrogen bonds distapched separately)
   */
  SelectClashingResidues(const Structure_SP s, const double clash_distance);

  virtual ~SelectClashingResidues() {}

  double clash_distance() const { return clash_distance_; }

  void clash_distance(const double new_distance) { clash_distance_ = new_distance; }

  virtual bool operator()(const PdbAtom &c) const;

  virtual bool operator()(const PdbAtom_SP c) const;

  virtual bool operator()(const Residue &r) const;

  virtual bool operator()(const Residue_SP r) const;

  virtual const std::string &selector_string() const { return selection_string_; }

  virtual void set(const std::string &new_selection) {}


private:
  utils::Logger logs;
  double clash_distance_;
  std::string selection_string_;
  core::calc::structural::interactions::PairwiseResidueMap<double> clashes;
};

}
}
}
}
#endif
