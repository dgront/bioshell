#include <core/data/structural/Structure.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/algorithms/trees/kd_tree.hh>

#include <core/data/structural/selectors/SelectClashingResidues.hh>

namespace core {
namespace data {
namespace structural {
namespace selectors {

SelectClashingResidues::SelectClashingResidues(const Structure_SP s, const double clash_distance) :
    logs("SelectClashingResidues"), clash_distance_(clash_distance), clashes(*s) {

  auto it_i_prev = s->first_residue();
  auto it_i = s->first_residue();
  ++it_i;
  for (; it_i != s->last_residue(); ++it_i, ++it_i_prev) {
    if ((*it_i)->residue_type().type != 'P') continue;
    for (auto it_j = s->first_residue(); it_j != it_i_prev; ++it_j) {
      if ((*it_j)->residue_type().type != 'P') continue;
      for (const auto &ai : **it_i) {
        if (ai->element_index() == 1) continue;
        for (const auto &aj : **it_j) {
          if (aj->element_index() == 1) continue;
          double d = aj->distance_to(*ai);
          if (d < clash_distance_) {
            if ((aj->atom_name() == " SG ") && (ai->atom_name() == " SG ")) continue;
            clashes.add(**it_i, **it_j, d);
            clashes.add(**it_j, **it_i, d);
            logs << utils::LogLevel::FINE << "Clash detected bewteen " << (**it_i) << " " << ai->atom_name() << " and "
                 <<
                 (**it_j) << " " << aj->atom_name() << " d = " << d << "\n";
          }
        }
      }
    }
  }
}

bool SelectClashingResidues::operator()(const PdbAtom &c) const {

  return clashes.has_row(clashes.ResidueIndexer::at(*(c.owner())));
}

bool SelectClashingResidues::operator()(const PdbAtom_SP c) const {
  return clashes.has_row(clashes.ResidueIndexer::at(*(c->owner())));
}

bool SelectClashingResidues::operator()(const Residue &c) const {

  return clashes.has_row(clashes.ResidueIndexer::at(c));
}

bool SelectClashingResidues::operator()(const Residue_SP c) const {
  return clashes.has_row(clashes.ResidueIndexer::at(*c));
}


}
}
}
}