#include <core/index.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/structural/selectors/ResidueSegmentSelector.hh>


namespace core {
namespace data {
namespace structural {
namespace selectors {


bool HasProperlyConnectedCA::operator()(const core::data::structural::ResidueSegment &rs) const {

  auto prev_ca = rs[0]->find_atom(" CA ", nullptr);
  if (prev_ca == nullptr) return false;
  for (core::index2 ir = 1; ir < rs.length(); ++ir) {
    auto ca = rs[ir]->find_atom(" CA ", nullptr);
    if (ca == nullptr) return false;
    if (prev_ca->distance_square_to(*ca) > 18.5) return false; //under 4.3 A is allowed
    prev_ca = ca;
  }
  return true;
}

bool AllResiduesSelected::operator()(const core::data::structural::ResidueSegment &rs) const {

  for (const auto r_sp: rs) {
    if (!sel_->operator()(r_sp)) return false;
  }
  return true;
}

bool IsSSType::operator()(const core::data::structural::ResidueSegment &rs) const {

  if (!DecoratedResidueSegmentSelector::operator()(rs)) return false;

  for (core::index2 ir = 0; ir < rs.length(); ++ir) {
    if (rs[ir]->ss() != ss_) return false;
  }
  return true;
}


bool HasSequence::operator()(const core::data::structural::ResidueSegment &rs) const {

  if (!DecoratedResidueSegmentSelector::operator()(rs)) return false;

  core::index2 n = rs.length();
  for (core::index2 ir = 0; ir < n; ++ir) {
    char c = sequence_[ir % n];
    if ((c != '?') && (c != 'X')) {
      if (c != rs[ir]->residue_type().code1)
        return false;
    }
  }
  return true;
}

bool HasSecondary::operator()(const core::data::structural::ResidueSegment &rs) const {

  if (!DecoratedResidueSegmentSelector::operator()(rs)) return false;

  core::index2 n = rs.length();
  for (core::index2 ir = 0; ir < n; ++ir) {
    char c = secondary_[ir % n];
    if ((c != '?') && (c != 'X')) {
      if (c != rs[ir]->ss())
        return false;
    }
  }
  return true;
}

//bool CombinedResidueSelectors::operator()(const core::data::structural::ResidueSegment &rs) const {
//
//  for (core::index2 ir = 0; ir <rs.length(); ++ir) {
//    for (auto sel: selectors_) {
//      if (!(*sel)(rs[ir])) return false;
//    }
//  }
//  return true;
//}
//
//ContiguousSSSegment::ContiguousSSSegment(const char ss) {
//  add_selector(std::make_shared<SelectResidueBySecondaryStructure>(ss));
//  std::make_shared<ProperlyConnectedCA>();
//}

//NearlySSSegment::NearlySSSegment(const char ss, const std::vector<core::index2> & ss_positions) {
//  for(core::index2 p: ss_positions) ss_pos_.push_back(p);
//  ss_ = ss;
//}
//
//bool NearlySSSegment::operator()(const core::data::structural::ResidueSegment &rs) const {
//
//  ProperlyConnectedCA ca_sel;
//  for (core::index2 ir = 0; ir <rs.length(); ++ir)
//    if (!ca_sel(rs[ir])) return false;
//
//  SelectResidueBySecondaryStructure ss_sel(ss_);
//  for (core::index2 pos: ss_pos_)
//    if (!ss_sel(*rs[pos])) return false;
//
//  return true;
//}

}
}
}
}

