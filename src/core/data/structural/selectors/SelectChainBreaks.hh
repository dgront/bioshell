/** @file SelectChainBreaks.hh
 *  @brief Provides SelectChainBreaks selector that selects residues at chain ends and chain breaks.
 */
#ifndef CORE_DATA_STRUCTURAL_SELECTORS_SelectChainBreaks_H
#define CORE_DATA_STRUCTURAL_SELECTORS_SelectChainBreaks_H

#include <memory>

#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

namespace core {
namespace data {
namespace structural {
namespace selectors {

enum CHAIN_BREAK_TYPE {
  LEFT,
  RIGHT,
  BOTH
};

/** @brief Selects residues that are located at chain breaks.
 *
 * This includes bot natural termini (N, C, 3', 5' etc.) as well as regions with undefined structures
 * (e.g. missing density in crystal structures)
 */
class SelectChainBreaks : public ResidueSelector {
public:

  /// This is the cutoff distance between subsequent CA atoms to detect a chain break
  constexpr static const double MAXIMUM_CA_CA_DISTANCE = 4.5;

  /// Type of the most recent chain break
  CHAIN_BREAK_TYPE last_chainbreak_type;

  /** @brief distance between this residue's CA  and the CA atom of the upstream (N-terminal) residue.
   *
   * The value is set by the most recent <code>operator()</code> call. The value of 0.0
   * means the chain break was caused by the missing CA atom or incorrect upstream residue rather than
   * by violating the distance criteria
   */
  double left_side_distance;

  /** @brief distance between this residue's CA  and the CA atom of the downstream (C-terminal) residue.
   *
   * The value is set by the most recent <code>operator()</code> call. The value of 0.0
   * means the chain break was caused by the missing CA atom or incorrect downstream residue rather than
   * by violating the distance criteria
   */
  double right_side_distance;

  /** @brief Tests whether a given residue is located at a chain break.
  *
  * @param r - refers to the tested residue
  * @return true if the chain is broken on either side of this residue
  */
  virtual bool operator()(const Residue &r) const;

  /** @brief Tests whether a given residue is located at a chain break.
  *
  * @param r - points to the tested residue
  * @return true if the chain is broken on either side of this residue
  */
  virtual bool operator()(const Residue_SP r) const { return operator()(*r); }

  /** @brief Tests whether a given atom belongs to a selected residue
   *
   * @param a - points to a tested atom
   * @return true if the chain is broken on either side of this residue
   */
  virtual bool operator()(const PdbAtom &a) const { return operator()(a.owner()); }

  /// This method has empty implementation so far
  virtual void set(const std::string &new_selection) {}

  virtual ~SelectChainBreaks() {}
};

}
}
}
}

#endif
