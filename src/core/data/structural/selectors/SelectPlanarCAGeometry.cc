#include <core/data/structural/selectors/SelectPlanarCAGeometry.hh>
#include <core/calc/structural/angles.hh>

namespace core {
namespace data {
namespace structural {
namespace selectors {

bool SelectPlanarCAGeometry::operator()(const Residue &r) const {

  if (r.residue_type().parent_id == core::chemical::Monomer::GLY.id)
    return false;

  if (r.residue_type().parent_id == core::chemical::Monomer::PRO.id) {
    if (fabs(evaluate_angle(r) - IDEAL_VALUE_RADIANS_PRO) > ALLOWED_DEVIATION_PRO) return true;
  } else {
    if (fabs(evaluate_angle(r) - IDEAL_VALUE_RADIANS) > ALLOWED_DEVIATION) return true;
  }
  return false;
}


double SelectPlanarCAGeometry::evaluate_angle(const Residue &r) const {

  const core::data::structural::PdbAtom &N = *(r.find_atom(" N  "));
  const core::data::structural::PdbAtom &CA = *(r.find_atom(" CA "));
  const core::data::structural::PdbAtom &C = *(r.find_atom(" C  "));
  const core::data::structural::PdbAtom &CB = *(r.find_atom(" CB "));

  return core::calc::structural::evaluate_dihedral_angle(N, CA, C, CB);
}

}
}
}
}