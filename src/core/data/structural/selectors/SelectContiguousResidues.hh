/** @file SelectContiguousResidues.hh
 *  @brief Provides SelectContiguousResidues selector that checks if a residue is contiguous
 */
#ifndef CORE_DATA_STRUCTURAL_SELECTORS_SelectContiguousResidues_H
#define CORE_DATA_STRUCTURAL_SELECTORS_SelectContiguousResidues_H

#include <memory>

#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

namespace core {
namespace data {
namespace structural {
namespace selectors {

/** @brief Selects residues which have all their atoms connected with bonds.
 *
 * The selector does not check if a residue is chemically correct; just looks if molecular graph is disjoint or not.
 * UnionFind is used for the test
 */
class SelectContiguousResidues : public ResidueSelector {
public:

  /** @brief Tests whether a given residue is contiguous (i.e. all its atoms are connected by bonds)
  *
  * @param r - refers to the tested residue
  * @return true if at least bond is broken
  */
  virtual bool operator()(const Residue &r) const;

  /** @brief Tests whether a given residue is contiguous (i.e. all its atoms are connected by bonds)
  *
  * @param r - refers to the tested residue
  * @return true if at least bond is broken
  */
  virtual bool operator()(const Residue_SP r) const { return operator()(*r); }

  /** @brief Tests whether a given atom belongs to a selected residue
   *
   * @param a - points to a tested atom
   * @return true if the chain is broken on either side of this residue
   */
  virtual bool operator()(const PdbAtom &a) const { return operator()(a.owner()); }

  /// This method has empty implementation so far
  virtual void set(const std::string &new_selection) {}

  virtual ~SelectContiguousResidues() {}
};

}
}
}
}

#endif
