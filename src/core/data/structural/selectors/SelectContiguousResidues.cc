#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/chemical/molecule_utils.hh>

#include <core/algorithms/UnionFind.hh>

#include <core/data/structural/selectors/SelectContiguousResidues.hh>

namespace core {
namespace data {
namespace structural {
namespace selectors {

bool SelectContiguousResidues::operator()(const Residue &r) const {

  core::algorithms::UnionFind<index2, index2> union_find(r.count_atoms());

  for (index2 i = 0; i < r.count_atoms(); ++i) {
    union_find.add_element(i);
    for (index2 j = 0; j < i; ++j) {
      if (core::chemical::bond_length(r[i]->atom_type, r[j]->atom_type) <= r[i]->distance_to(*r[j]))
        union_find.union_set(i, j);
    }
  }
  if (union_find.count_sets() > 1) return false;
  return true;
}


}
}
}
}
