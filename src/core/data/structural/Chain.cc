#include <memory>
#include <algorithm>
#include <stdexcept>

#include <core/index.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/structural/Chain.fwd.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/data/sequence/Sequence.hh>
#include <core/data/sequence/SecondaryStructure.hh>

#include <core/data/structural/Helix.hh>
#include <core/data/structural/Strand.hh>

namespace core {
namespace data {
namespace structural {


const Chain_SP Chain::previous() const {
  index2 i = 0;
  for (const Chain_SP ci:*owner())
    if (ci->id() != id()) ++i;
  if (i == 0) return nullptr;
  else return (*owner())[i - 1];
}

const Chain_SP Chain::next() const {
  index2 i = 0;
  for (const Chain_SP ci:*owner())
    if (ci->id() != id()) ++i;
  if (i == owner()->size() - 1) return nullptr;
  else return (*owner())[i + 1];
}

Chain_SP Chain::clone() const {
  return clone(selectors::ResidueSelector{});
}

Chain_SP Chain::clone(const selectors::ResidueSelector &which_residues = selectors::ResidueSelector{})  const {

  Chain_SP ret = std::make_shared<Chain>(id_);
  ret->terminal_index_ = terminal_index_;
  for (const Residue_SP &ri : (*this))
    if (which_residues(*ri))
      ret->push_back(ri->clone(which_residues));

  return ret;
}

void Chain::push_back(Residue_SP r) {

  if (std::vector<Residue_SP>::size() != 0) {
    Residue_SP b = std::vector<Residue_SP>::back();
    if ((b->residue_type().type == 'P') && (r->residue_type().type == 'P')) {
      b->next(r);
      r->previous(b);
    }
    if ((b->residue_type().type == 'N') && (r->residue_type().type == 'N')) {
      b->next(r);
      r->previous(b);
    }
  }
  r->owner_ = shared_from_this();
  std::vector<Residue_SP>::push_back(r);
}

core::index2 Chain::count_aa_residues() const {

  return std::count_if(begin(),end(),[](const Residue_SP r){ return r->residue_type().type=='P'; });
}

core::index2 Chain::count_na_residues() const {

  return std::count_if(begin(),end(),[](const Residue_SP r){ return r->residue_type().type=='N'; });
}

Chain_SP Chain::create_ca_chain(const std::string & sequence, const std::string & chain_id) {

  double dx = 3.67;
  double dy = 0.5;
  double sig = -1.0;
  Chain_SP c = std::make_shared<Chain>(chain_id);
  for (core::index2 i = 0; i < sequence.size(); ++i) {
    PdbAtom_SP a = std::make_shared<PdbAtom>(i + 1, std::string(" CA "));
    a->x = i * dx;
    a->y = i % 2 * dy * sig;
    sig *= -1;
    a->z = 0.0;
    Residue_SP r = std::make_shared<Residue>(i + 1, core::chemical::Monomer::get(sequence[i]));
    r->push_back(a);
    c->push_back(r);
  }

  return c;
}

Chain_SP Chain::create_ca_chain(const core::data::sequence::SecondaryStructure & secondary, const std::string & chain_id) {

  auto chain = create_ca_chain(secondary.sequence, chain_id);
  for (index2 ires = 0; ires < chain->size(); ++ires)
    (*chain)[ires]->ss(secondary.ss(ires));

  return chain;
}

size_t chain_to_coordinates(const Chain_SP structure, core::data::basic::Coordinates & coordinates) {

  if (coordinates.size() != structure->size()) coordinates.resize(structure->size());
  int i = -1;
  for(auto a_it=structure->first_atom();a_it!=structure->last_atom();++a_it)
    coordinates[++i].set(*(*a_it));

  return (++i);
}

size_t chain_to_coordinates(const Chain_SP chain, core::data::basic::Coordinates & coordinates,
                            selectors::AtomSelector_SP op) {

  if (op == nullptr) chain_to_coordinates(chain, coordinates);

  int i = -1;
  for (auto it = chain->first_atom(); it != chain->last_atom(); ++it)
    if ((*op)(**it)) coordinates[++i].set(**it);

  return (++i);
}

Residue_SP Chain::get_residue(const core::index2 residue_index) {

  for(auto & ri : *this)
    if(ri->id() == residue_index) return ri;
  return nullptr;
}

void Chain::remove_ligands() {

  utils::Logger logs("Chain");
  size_type s = size();
  for (int i = 0; i < s - terminal_index_ - 1; ++i) {
    logs << utils::LogLevel::FINE << "Residue " << *back() << " removed from chain " << id_ << " as a ligand\n";
    pop_back();
  }
}

bool Chain::has_secondary_structure() const {

  for (const Residue_SP ri: *this)
    if (ri->ss() != 'C') return true;

  return false;
}

core::data::sequence::SecondaryStructure_SP Chain::create_sequence(bool if_exclude_ligands) const {
  utils::Logger logs("Chain");
  std::string s;
  std::string ss;


  if (if_exclude_ligands && count_aa_residues() == 0 && count_na_residues() == 0) {
    logs << utils::LogLevel::WARNING << "Creating an empty sequence for chain " << id_ << "\n";
    return std::make_shared<core::data::sequence::SecondaryStructure>(owner()->code() + id_, s, (*begin())->id(), ss);
  }
  s.reserve(count_residues());
  ss.reserve(count_residues());
  const auto end = (if_exclude_ligands) ? ((terminal_residue() == cend()) ? cend() : terminal_residue() + 1) : cend();
  for (auto r_it = cbegin(); r_it != end; ++r_it) {
    if ((*r_it)->residue_type().type == 'P' || (*r_it)->residue_type().type == 'N') {
      s += (*r_it)->residue_type().code1;
      ss += (*r_it)->ss();
    }
  }
  logs << utils::LogLevel::INFO << "Creating sequence for chain " << id_;
  if(!if_exclude_ligands) logs << " trimmed to TER residue\n";
  else logs << " with all residues, including possible ligands\n";
  sequence::SecondaryStructure_SP seq_str = std::make_shared<core::data::sequence::SecondaryStructure>(owner()->code() + id_, s, (*begin())->id(), ss);
    for (auto r_it = begin(); r_it != end; ++r_it) {
        if ((*r_it)->residue_type().type == 'P' || (*r_it)->residue_type().type == 'N') {
            seq_str->indexes_.push_back( (*r_it)->id());
            seq_str->icodes_.push_back( (*r_it)->icode());
        }
    }
    seq_str->chain_id_=id();
    return seq_str;
}

std::vector<SecondaryStructureElement_SP> & Chain::create_sse(std::vector<SecondaryStructureElement_SP> &sse_vect) const {

  std::vector<Residue_SP> sse_residues;
  sse_residues.push_back(operator[](0));
  std::string ss = create_sequence(false)->str();
  char current_sse = ss[0];
  for (index4 i = 1; i < ss.size(); ++i) {
    // --- go on with the current SSE
    if (ss[i] == current_sse) {
      sse_residues.push_back(operator[](i));
      continue;
    }
    // --- new SSE has been started, the previous one was a helix
    if (current_sse == 'H') {
      SecondaryStructureElement_SP helix = std::make_shared<Helix>(sse_residues, core::data::structural::HelixTypes::RightAlpha);
      sse_vect.push_back(helix);
    } else if (current_sse == 'E') { // --- new SSE has been started, the previous one was a strand
      SecondaryStructureElement_SP strand = std::make_shared<Strand>(sse_residues, core::data::structural::StrandTypes::Other);
      sse_vect.push_back(strand);
    }

    sse_residues.clear();
    current_sse = ss[i];
  }
  // --- new SSE has been started, the previous one was a helix
  if (current_sse == 'H') {
    SecondaryStructureElement_SP helix = std::make_shared<Helix>(sse_residues, core::data::structural::HelixTypes::RightAlpha);
    sse_vect.push_back(helix);
  } else if (current_sse == 'E') { // --- new SSE has been started, the previous one was a strand
    SecondaryStructureElement_SP strand = std::make_shared<Strand>(sse_residues, core::data::structural::StrandTypes::Other);
    sse_vect.push_back(strand);
  }

  return sse_vect;
}

std::vector<SecondaryStructureElement_SP> Chain::create_sse() const {

  std::vector<SecondaryStructureElement_SP> sse;

  return create_sse(sse);
}

void Chain::annotate_ss(const std::string & secondary_structure) {

  if (secondary_structure.size() != size())
    throw std::length_error(
        "Length of this chain differs from the length of a given secondary structure string:\n" + secondary_structure);

  for(int i=0;i<size();i++)
    (*this)[i]->ss(secondary_structure[i]);
}

void Chain::sort() {

  for(Residue_SP & r : *this)
    r->sort();
  std::sort(this->begin(),this->end());
}

std::ostream& operator<<(std::ostream &out, const Chain &c) {

  out << c.id();
  return  out;
}


Chain_SP chain_for_sequence(const std::string & id, const std::string & sequence) {

  Chain_SP chain = std::make_shared<Chain>(id);
  core::index2 i_res = 0;
  for(const char c : sequence) chain->push_back( std::make_shared<Residue>(++i_res,c));

  return chain;
}

Chain_SP chain_for_sequence(const std::string & id, const core::data::sequence::Sequence & sequence) {

  Chain_SP chain = std::make_shared<Chain>(id);
  for(core::index2 i_res=0;i_res<sequence.length();++i_res)
    chain->push_back(std::make_shared<Residue>(sequence.first_pos() + i_res, sequence.get_monomer(i_res)));

  return chain;
}


Chain_SP chain_for_sequence(const std::string & id, const core::data::sequence::SecondaryStructure & sequence) {

  Chain_SP chain = std::make_shared<Chain>(id);
  for (core::index2 i_res = 0; i_res < sequence.length(); ++i_res) {
    chain->push_back(std::make_shared<Residue>(sequence.first_pos() + i_res, sequence.get_monomer(i_res)));
    chain->back()->ss(sequence.ss(i_res));
  }

  return chain;
}

}
}
}

