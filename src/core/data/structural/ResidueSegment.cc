#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/ResidueSegment.hh>

#include <utils/string_utils.hh>

namespace core {
namespace data {
namespace structural {

ResidueSegment::ResidueSegment(Chain_SP chain, index2 first, index2 last) {

  for (index2 i = first; i <= last; ++i) {
    push_back((*chain)[i]);
  }
}

std::ostream& operator<<(std::ostream& str, ResidueSegment const& segm) {

  const Chain & c = *segm.front()->owner();
  const Residue & ri = *segm.front();
  const Residue & rj = *segm.back();
  const auto & ss = segm.sequence();
  str << utils::string_format("%4s %2s %3s%d - %3s%d %s %s", c.owner()->code().c_str(), c.id().c_str(),
      ri.residue_type().code3.c_str(), ri.id(), rj.residue_type().code3.c_str(), rj.id(), ss->sequence.c_str(),
      ss->str().c_str());

  return str;
}

core::data::sequence::SecondaryStructure_SP ResidueSegment::sequence() const {
  std::string seq, ss;
  for(Residue_SP ri:*this) {
    seq += ri->residue_type().code1;
    ss +=  ri->ss();
  }
  const Chain & c = *(front()->owner());
  return std::make_shared<core::data::sequence::SecondaryStructure>(c.owner()->code(), seq, front()->id(), ss);
}

}
}
}
