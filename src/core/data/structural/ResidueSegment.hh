#ifndef CORE_DATA_STRUCTURAL_ResidueSegment_HH
#define CORE_DATA_STRUCTURAL_ResidueSegment_HH

#include <algorithm>
#include <vector>
#include <memory>

#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/sequence/SecondaryStructure.hh>

namespace core {
namespace data {
namespace structural {

/** @brief A residue segment is a chunk of a protein (od nucleic) chain
 */
class ResidueSegment : private std::vector<Residue_SP> {
public:
  using std::vector<std::shared_ptr<Residue>>::assign;
  using std::vector<std::shared_ptr<Residue>>::at;
  using std::vector<std::shared_ptr<Residue>>::back;
  using std::vector<std::shared_ptr<Residue>>::begin;
  using std::vector<std::shared_ptr<Residue>>::capacity;
  using std::vector<std::shared_ptr<Residue>>::cbegin;
  using std::vector<std::shared_ptr<Residue>>::cend;
  using std::vector<std::shared_ptr<Residue>>::crbegin;
  using std::vector<std::shared_ptr<Residue>>::crend;
  using std::vector<std::shared_ptr<Residue>>::clear;
  using std::vector<std::shared_ptr<Residue>>::empty;
  using std::vector<std::shared_ptr<Residue>>::end;
  using std::vector<std::shared_ptr<Residue>>::erase;
  using std::vector<std::shared_ptr<Residue>>::front;
  using std::vector<std::shared_ptr<Residue>>::get_allocator;
  using std::vector<std::shared_ptr<Residue>>::insert;
  using std::vector<std::shared_ptr<Residue>>::max_size;
  using std::vector<std::shared_ptr<Residue>>::operator [];
  using std::vector<std::shared_ptr<Residue>>::pop_back;
  using std::vector<std::shared_ptr<Residue>>::push_back;
  using std::vector<std::shared_ptr<Residue>>::rbegin;
  using std::vector<std::shared_ptr<Residue>>::rend;
  using std::vector<std::shared_ptr<Residue>>::reserve;
  using std::vector<std::shared_ptr<Residue>>::resize;
  using std::vector<std::shared_ptr<Residue>>::size;
  using std::vector<std::shared_ptr<Residue>>::swap;
  using std::vector<std::shared_ptr<Residue>>::shrink_to_fit;
  typedef  typename std::vector<std::shared_ptr<Residue>>::value_type  value_type;
  typedef  typename std::vector<std::shared_ptr<Residue>>::reference  reference;
  typedef  typename std::vector<std::shared_ptr<Residue>>::const_reference  const_reference;
  typedef  typename std::vector<std::shared_ptr<Residue>>::pointer  pointer;
  typedef  typename std::vector<std::shared_ptr<Residue>>::const_pointer  const_pointer;
  typedef  typename std::vector<std::shared_ptr<Residue>>::iterator  iterator;
  typedef  typename std::vector<std::shared_ptr<Residue>>::const_iterator  const_iterator;
  typedef  typename std::vector<std::shared_ptr<Residue>>::reverse_iterator  reverse_iterator;
  typedef  typename std::vector<std::shared_ptr<Residue>>::const_reverse_iterator  const_reverse_iterator;
  typedef  typename std::vector<std::shared_ptr<Residue>>::size_type  size_type;
  typedef  typename std::vector<std::shared_ptr<Residue>>::difference_type  difference_type;
  typedef  typename std::vector<std::shared_ptr<Residue>>::allocator_type  allocator_type;

  /** @brief Constructor makes a segment that refers to a particular fragment of a given chain
   * @param chain - a chain this segment cones from
   * @param first - the first residue of this segment
   * @param last - the last residue of this segment (inclusive)
   */
  ResidueSegment(Chain_SP chain, index2 first, index2 last);

  /** @brief Constructor makes a deep copy of the given residue pointers.
   * @param residues - residues that constitute this secondary structure element
   */
  ResidueSegment(const std::vector<Residue_SP> &residues) {
    std::copy(residues.begin(), residues.end(), std::back_inserter(*this));
  }

  /** @brief Constructor makes a deep copy of the given residue pointers.
   * @param residues_begin - iterator pointing to the first residue in this secondary structure element
   * @param residues_end - passed-the-end iterator referring to the last residue in this secondary structure element
   */
  template<typename It>
  ResidueSegment(const It &residues_begin, const It &residues_end) {
    std::copy(residues_begin, residues_end, std::back_inserter(*this));
  }

  /// Default destructor (empty)
  ~ResidueSegment() = default;

  /** @brief Returns a SecondaryStructure object for this segment
   * @return object providing the sequence and secondary structure data for this segment
   */
  core::data::sequence::SecondaryStructure_SP sequence() const;

  /// Returns the ID string of the chain the very first residue of this segment belongs to
  const std::string & chain_id() const { return front()->owner()->id(); }

  /** @brief Returns true if this segment contains a given residue.
   *
   * Each residue of this segment is compared to <code>r</code> by their residueId, insertion code and chainId. If these three
   * are identical, the method returns <code>true</code>
   * @param r another residue
   * @return true if this segment contains residue identified identically as the given <code>r</code>
   */
  bool contains(const Residue &r) const {

    for (const Residue_SP ri : *this) {
      if ((ri->id() == r.id()) && (r.icode() == ri->icode()) && (ri->owner()->id() == r.owner()->id())) return true;
    }

    return false;
  }

  const Structure_SP structure() const { return front()->owner()->owner(); }

  /// Return the number of residues contained in this segment - alias to std::vector<>::size()
  core::index2 length() const { return size(); }

  /** @brief  Prints information about this secondary structure element
   *
   * @param str - output stream
   * @param segm - object to be printed
   * @return the reference to the output stream
   */
  friend std::ostream& operator<<(std::ostream& str, ResidueSegment const& segm);

};

/** @brief Define order of segments by the order they appear in the sequence of a protein
 *
 * @param lhs - a segments to be compared
 * @param rhs - a segments to be compared
 * @return true if the lhs segments is closer to N-termini of a protein than rhs segments
 */
inline bool operator <(const ResidueSegment& lhs, const ResidueSegment& rhs) {
  return lhs[0]->id() < rhs[0]->id();
}

/// Define the pointer type of a ResidueSegment class
typedef typename std::shared_ptr<ResidueSegment> ResidueSegment_SP;

}
}
}

#endif // --- CORE_DATA_STRUCTURAL_ResidueSegment_HH
