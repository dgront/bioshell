#ifndef CORE_DATA_STRUCTURAL_BetaStructuresGraph_HH
#define CORE_DATA_STRUCTURAL_BetaStructuresGraph_HH

#include <core/algorithms/SimpleGraph.hh>
#include <core/algorithms/GraphWithData.hh>
#include <core/data/structural/Strand.hh>
#include <core/data/structural/StrandPairing.hh>

namespace core {
namespace data {
namespace structural {

/** @brief Graph of beta strands connected into sheets.
 *
 * Use ProteinArchitecture::create_strand_graph() method to create an object of this class
 * Note, that the graph may be disconnected if the analysed protein contains more than one sheet
 */
class BetaStructuresGraph : private core::algorithms::GraphWithData<core::algorithms::SimpleGraph, Strand_SP, StrandPairing_SP> {

  typedef core::algorithms::GraphWithData<core::algorithms::SimpleGraph, Strand_SP, StrandPairing_SP> Base;

public:
  /// Constructor that creates an empty graph of beta strands
  BetaStructuresGraph() {}

  /** @brief Adds a new strand to this graph.
   *
   * Note, that the newly added strand will not be attached to any sheet. Use <code>add_strand_pairing()</code>
   * to change it.
   *
   * @param si - a strands
   */
  void add_strand(Strand_SP si) { Base::add_vertex(si); }

  /** @brief Alias to @code add_strand() @endcode
   *
   * The method is necessary to apply graph algorithms to BetaStructuresGraph type
   * @param si - a strands
   */
  void add_vertex(Strand_SP si) { Base::add_vertex(si); }

  /** @brief Adds two strands to this graph and connects them by a given strand pairing data.
   *
   * The method takes care not to insert the same strand twice. It also checks if the strands already exists
   *
   * @param si - the first of the two strands
   * @param sj  - the second of the two strands
   * @param pairing - object describing how the two strands are connected
   */
  void add_strand_pairing(Strand_SP si, Strand_SP sj, StrandPairing_SP pairing) { Base::add_edge(si, sj, pairing); }

  /** @brief Alias to @code add_strand_pairing() @endcode
   *
   * The method is necessary to apply graph algorithms to BetaStructuresGraph type
   * @param si - the first of the two strands
   * @param sj  - the second of the two strands
   * @param pairing - object describing how the two strands are connected
   */
  void add_edge(Strand_SP si, Strand_SP sj, StrandPairing_SP pairing) { Base::add_edge(si, sj, pairing); }

  /** @brief Removes a strand pairing
   *
   * @param si - the first of the two strands
   * @param sj  - the second of the two strands
   *
   * @return true if the pairing was actually removed, false if the two strands were not connected
   */
  bool remove_strand_pairing(Strand_SP si, Strand_SP sj) { return remove_edge(si, sj); }

  /** @brief Counts strands in this protein structure
   *
   * @return the number of beta-strands in the protein structure for which this graph was created
   */
  core::index4 count_strands() const { return Base::count_vertices(); }

  /** @brief Counts strands in this protein structure
   *
   * This is just an alias for <code>count_strands()</code>
   * @return the number of beta-strands in the protein structure for which this graph was created
   */
  core::index4 count_vertices() const { return Base::count_vertices(); }

  /**  @brief Counts how many strands are hydrogen-bonded to a given strand
   * @param v - the query strand
   * @return the number of strands connected to v
   */
  core::index4 count_partners(const Strand_SP v) const { return Base::count_edges(v); }

  /** \brief Returns the number of edges attached to a given vertex (alias to count_partners(V&) )
   * @return  the number of edges attached to a given vertex of this graph.
   */
  inline index4 count_edges(const Strand_SP v) const { return count_partners(v); }

  /** \brief Returns the total number of edges of this graph.
   * @return  the total number of edges of this graph.
   */
  inline index4 count_edges() const { return Base::count_edges(); }

  /** \brief Returns a being iterator for strands.
   * @return iterator that points prior the first strand in this structure
   */
  typename std::vector<Strand_SP>::iterator begin_strand() { return Base::begin(); }

  /**  \brief Returns an end iterator for strands.
   * @return iterator that points past the last strand in this structure
   */
  typename std::vector<Strand_SP>::iterator end_strand() { return Base::end(); }


  /** \brief Returns a being iterator for strands.
   * @return iterator that points prior the first strand in this structure
   */
  typename std::vector<Strand_SP>::iterator begin() { return Base::begin(); }

  /**  \brief Returns an end iterator for strands.
   * @return iterator that points past the last strand in this structure
   */
  typename std::vector<Strand_SP>::iterator end() { return Base::end(); }

  /** \brief Returns a being const-iterator for strands.
   * @return iterator that points prior the first strand in this structure
   */
  typename std::vector<Strand_SP>::const_iterator cbegin_strand() const { return Base::cbegin(); }

  /**  \brief Returns an end const-iterator for strands.
   * @return iterator that points past the last strand in this structure
   */
  typename std::vector<Strand_SP>::const_iterator cend_strand() const { return Base::cend(); }

  /** \brief Returns a being iterator for vertexes that are connected to <code>a_strand</code>.
   * @return iterator that points prior the first strand connected to <code>a_strand</code>.
   */
  core::algorithms::GivenOrderIterator<Strand_SP,SimpleGraph::iterator> begin_strand(const Strand_SP & a_strand) {
    return Base::begin(a_strand);
  }

  /** \brief Returns an end iterator for strands that are connected to another strand <code>a_strand</code>.
   * @return iterator that points past the last strand connected to <code>a_strand</code>.
   */
  core::algorithms::GivenOrderIterator<Strand_SP,SimpleGraph::iterator> end_strand(const Strand_SP & a_strand) {
    return Base::end(a_strand);
  }

  /** \brief Returns a being iterator for vertexes that are connected to <code>a_strand</code>.
   * @return iterator that points prior the first strand connected to <code>a_strand</code>.
   */
  core::algorithms::GivenOrderIterator<Strand_SP,SimpleGraph::iterator> begin(const Strand_SP & a_strand) {
    return Base::begin(a_strand);
  }

  /** \brief Returns an end iterator for strands that are connected to another strand <code>a_strand</code>.
   * @return iterator that points past the last strand connected to <code>a_strand</code>.
   */
  core::algorithms::GivenOrderIterator<Strand_SP,SimpleGraph::iterator> end(const Strand_SP & a_strand) {
    return Base::end(a_strand);
  }

  /** \brief Returns a being iterator for vertexes that are connected to <code>a_strand</code>.
   * @return iterator that points prior the first strand connected to <code>a_strand</code>.
   */
  core::algorithms::GivenOrderConstIterator<Strand_SP,SimpleGraph::const_iterator> cbegin_strand(const Strand_SP & a_strand) const {
    return Base::cbegin(a_strand);
  }

  /** \brief Returns an end iterator for strands that are connected to another strand <code>a_strand</code>.
   * @return iterator that points past the last strand connected to <code>a_strand</code>.
   */
  core::algorithms::GivenOrderConstIterator<Strand_SP,SimpleGraph::const_iterator> cend_strand(const Strand_SP & a_strand) const {
    return Base::cend(a_strand);
  }

  /** @brief Creates a new vector of all strands in this structure.
   *
   * The newly created vector contains a copy of pointers to all strands of a structure
   * @return a newly allocated vector containing all strands of this graph
   */
  std::vector<Strand_SP> get_strands_copy();

  /** @brief Creates a vector of strands that are hydrogen-bonded to a given strand
   * @return a newly allocated vector containing all strands connected to v
   */
  std::vector<Strand_SP> get_strands_copy(const Strand_SP v);

  /** @brief Returns a StrandPairing object that connects the two given strands.
   *
   * Exception is thrown if the two strands are not paired into a sheet.
   * @param si - the first sheet partner
   * @param sj - the second sheet partner
   * @return strand pairing data
   */
  const StrandPairing_SP get_strand_pairing(const Strand_SP si,const  Strand_SP sj) const { return Base::edge(si,sj); }

  /// Alias to <code>get_strand_pairing()</code>
  const StrandPairing_SP edge(const Strand_SP si,const  Strand_SP sj) const { return get_strand_pairing(si,sj); }

  bool are_connected(const Strand_SP si,const  Strand_SP sj) const;

  /// Returns begin const-iterator to edges of this graph
  typename std::map<std::pair<index4, index4>,StrandPairing_SP>::const_iterator cbegin_pairings() const { return Base::cbegin_edges(); }

  /// Returns end const-iterator to edges of this graph
  typename std::map<std::pair<index4, index4>,StrandPairing_SP>::const_iterator cend_pairings() const { return Base::cend_edges(); }

  /** @brief Prints adjacency matrix of this graph on ostream, e.g. on a screen
   * @param out - stream to print this graph
   */
  void print_adjacency_matrix(std::ostream &out);

};

typedef std::shared_ptr<BetaStructuresGraph> BetaStructuresGraph_SP;

}
}
}

#endif // --- CORE_DATA_STRUCTURAL_BetaStructuresGraph_HH
