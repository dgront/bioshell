#include <core/data/structural/ResidueIndex.hh>
#include <utils/string_utils.hh>

namespace core {
namespace data {
namespace structural {

std::ostream & operator<<(std::ostream & out, const ResidueIndex & ri) {
  out << ri.chain_id << ' ' << ri.residue_id << ri.i_code;

  return out;
}

bool operator<(const ResidueIndex & ri, const ResidueIndex & rj) {

  if (ri.chain_id != rj.chain_id) return ri.chain_id < rj.chain_id;
  if (ri.residue_id != rj.residue_id) return ri.residue_id < rj.residue_id;
  if (ri.residue_id == rj.residue_id) return ri.i_code < rj.i_code;
  return false;
}

bool operator<(const std::shared_ptr<ResidueIndex> ri, const std::shared_ptr<ResidueIndex> rj) {

  if (ri->chain_id != rj->chain_id) return ri->chain_id < rj->chain_id;
  if (ri->residue_id != rj->residue_id) return ri->residue_id < rj->residue_id;
  if (ri->residue_id == rj->residue_id) return ri->i_code < rj->i_code;
  return false;
}

ResidueIndexer::ResidueIndexer(const core::data::structural::Structure & strctr) {

  core::index2 i = 0;
  for (const auto & ci : strctr)
    for (auto ri : *ci) {
      ResidueIndex id( { ri->owner()->id(), ri->id(), ri->icode() });
      index[id] = i;
      max_index_ = i;
      ++i;
      resids.push_back(ri);
    }
}

ResidueIndexer::ResidueIndexer(const Structure & strctr, const selectors::ResidueSelector & only_selected_residues) {

  core::index2 i = 0;
  for (const auto & ci : strctr)
    for (auto ri : *ci) {
      if(only_selected_residues(*ri)) {
        ResidueIndex id({ri->owner()->id(), ri->id(), ri->icode()});
        index[id] = i;
        max_index_ = i;
        ++i;
        resids.push_back(ri);
      }
    }
}


template<typename It>
ResidueIndexer::ResidueIndexer(It &residue_begin, It &residue_end) {

  core::index2 i = 0;
  for (auto ri = residue_begin; ri != residue_end; ++ri) {
    ResidueIndex id({ri->owner()->id(), ri->id(), ri->icode()});
    index[id] = i;
    max_index_ = i;
    ++i;
    resids.push_back(ri);
  }
}

core::index2 ResidueIndexer::at(const std::string & chain_id, const int res_id, const char i_code) const {
  const ResidueIndex ri( { chain_id, res_id, i_code });
  return index.at(ri);
}

const ResidueIndex & ResidueIndexer::residue_index(core::index2 residue_index) {

  for (auto it = index.cbegin(); it != index.cend(); ++it )
    if (it->second == residue_index) return it->first;

  throw std::range_error(utils::string_format("Invalid residue index: %d",residue_index));
}

}
}
}
