#ifndef CORE_DATA_STRUCTURAL_ResidueSegmentProvider_HH
#define CORE_DATA_STRUCTURAL_ResidueSegmentProvider_HH

#include <algorithm>
#include <vector>

#include <core/data/structural/Structure.hh>
#include <core/data/structural/ResidueSegment.hh>

#include <utils/Logger.hh>
#include <ui/pipelines/GenericProvider.hh>

namespace core {
namespace data {
namespace structural {

/** @brief A residue segment is a chunk of a protein (or a nucleic) chain
 */
class ResidueSegmentProvider : public ui::pipelines::GenericProvider<ResidueSegment_SP> {
public:

  /// Length of this segment - number of its residues
  const core::index2 length;

  /** @brief Creates an object that iterates over all residue segments of a given structure
   * @param strctr - the source of residue segments
   */
  ResidueSegmentProvider(Structure_SP strctr, core::index2 segment_length) : length(segment_length),
      strctr_(strctr), logger("ResidueSegmentProvider") {

    current_chain_ = 0;
    current_residue_ = 0;
    has_next_ = find_next();
    if(has_next_) {
      logger << utils::LogLevel::INFO << "Processing " << strctr->code() << "\n";
      logger << utils::LogLevel::INFO << "Segments of chain " << strctr_->get_chain(current_chain_)->id() << "\n";
    } else {
      logger << utils::LogLevel::INFO << "Structure " << strctr->code() << " has no suitable chains!\n";
    }
  }

  /// Virtual destructor (empty)
  virtual ~ResidueSegmentProvider() = default;

  /// Returns true if there are any segments left
  bool has_next() const override { return has_next_; }

  /// returns the next segment
  ResidueSegment_SP next() override;

  ResidueSegment_SP peek();
protected:
  Structure_SP strctr_;      ///< Reference to structure which is the source of the structural data for this segment

private:
  index2 current_chain_ = 0;
  index4 current_residue_ = 0;
  bool has_next_ = true;
  utils::Logger logger;

  bool find_next();
};

class SecondaryStructureElementProvider : public ui::pipelines::GenericProvider<SecondaryStructureElement_SP> {
public:

  /** @brief Creates an object that iterates over all secondary structure elements of a given structure.
   *
   * The given structure object must be properly annotated with secondary structure information
   * @param strctr - the source of residue segments
   */
  SecondaryStructureElementProvider(Structure_SP strctr) : strctr_(strctr), logger("SecondaryStructureElementProvider") {
    logger << utils::LogLevel::INFO << "Processing " << strctr->code() << "\n";
    sse_ = strctr_->create_sse();
    pointer_ = sse_.begin();
  }

  /// Virtual destructor (empty)
  virtual ~SecondaryStructureElementProvider() = default;

  /// Returns true if there are any segments left
  bool has_next() const override { return pointer_ != sse_.end(); }

  /// returns the next segment
  SecondaryStructureElement_SP next() override {
    SecondaryStructureElement_SP sse = *(pointer_);
    ++pointer_;
    return sse;
  }

protected:
  /// Reference to structure which is the source of the structural data for this segment
  Structure_SP strctr_;
  /// Holds secondary structure elements provided by this Provider
  std::vector<SecondaryStructureElement_SP> sse_;

private:
  utils::Logger logger;
  std::vector<SecondaryStructureElement_SP>::iterator pointer_;
};

}
}
}

#endif // --- CORE_DATA_STRUCTURAL_ResidueSegmentProvider_HH
