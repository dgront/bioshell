#include <iostream>
#include <string>
#include <memory>

#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <utils/string_utils.hh> // for string_format()

namespace core {
namespace data {
namespace structural {

bool operator<(const Residue & ri, const Residue & rj) {

  if (ri.id() != rj.id()) return ri.id() < rj.id();
  if (ri.id() == rj.id()) return ri.icode() < rj.icode();
  return false;
}

bool operator<(const Residue_SP ri, const Residue_SP rj) {

  if (ri->id() != rj->id()) return ri->id() < rj->id();
  if (ri->id() == rj->id()) return ri->icode() < rj->icode();
  return false;
}

Residue_SP Residue::clone() const { return clone(selectors::AtomSelector{}); }

Residue_SP Residue::clone(const selectors::AtomSelector &which_atoms) const {

  Residue_SP ret = std::make_shared<Residue>(id_,residue_type_);
  ret->insertion_code_ = insertion_code_;
  ret->ss_type_ = ss_type_;
  ret->residue_id_ = residue_id_;

  for (const PdbAtom_SP &ai : *(this))
    if (which_atoms(*ai))
      ret->push_back(ai->clone());

  return ret;
}

core::data::basic::Vec3 Residue::cm(const selectors::AtomSelector &which_atoms) const {

  Vec3 cm;
  double n = 0;
  for (const PdbAtom_SP a:*this) {
    if (which_atoms(a)) {
      cm += *a;
      ++n;
    }
  }
  cm /= n;

  return cm;

}

core::data::basic::Vec3 Residue::cm() const {

  Vec3 cm;
  for (const core::data::structural::PdbAtom_SP a:*this) cm += *a;
  cm /= count_atoms();

  return cm;
}

void Residue::push_back(PdbAtom_SP a) { a->owner_ = shared_from_this(); std::vector<PdbAtom_SP>::push_back(a); }

std::ostream& operator<<(std::ostream &out, const Residue & r) {
	out << utils::string_format("%c%s %4d", r.insertion_code_, r.residue_type_.code3.c_str(), r.id_);
	return out;
}

utils::Logger &operator <<(utils::Logger &logger, const Residue & r) {

  logger << utils::string_format("%c%s %4d", r.insertion_code_, r.residue_type_.code3.c_str(), r.id_);
  return logger;
}


core::index2 Residue::count_heavy_atoms() const {

  core::index2 nnH = 0;
  for (size_t i = 0; i < size(); ++i)
    if (operator[](i)->element_index() != 1) ++nnH;
  return nnH;
}


void Residue::sort() {
  std::sort(begin(),end(),[](const PdbAtom_SP & ai,const PdbAtom_SP & aj){ return ai->id() < aj->id();});
}

double Residue::min_distance(const Residue & another_residue, const double min_val_cutoff) const {

  double min_val2 = (min_val_cutoff == std::numeric_limits<double>::max()) ? min_val_cutoff :
                        min_val_cutoff * min_val_cutoff;

  for (core::index2 i = 0; i < size(); ++i) {
    for (core::index2 j = 0; j < another_residue.size(); ++j) {
      double d = operator[](i)->x - another_residue[j]->x;
      double d2 = d * d;
      if (d2 >= min_val2) continue;
      d = operator[](i)->y - another_residue[j]->y;
      d2 += d * d;
      if (d2 >= min_val2) continue;
      d = operator[](i)->z - another_residue[j]->z;
      d2 += d * d;
      if (d2 > min_val2) continue;
      min_val2 = d2;
    }
  }

  return sqrt(min_val2);
}

double Residue::min_distance(const Residue & another_residue, selectors::AtomSelector_SP & valid_atoms,
                                 double min_val_cutoff) const {

  double min_val2 = (min_val_cutoff == std::numeric_limits<double>::max()) ? min_val_cutoff :
                        min_val_cutoff * min_val_cutoff;
  for (core::index2 i = 0; i < size(); ++i) {
    if (!(*valid_atoms)(operator[](i))) continue;
    for (core::index2 j = 0; j < another_residue.size(); ++j) {
      if (!(*valid_atoms)(another_residue[j])) continue;
      double d = operator[](i)->x - another_residue[j]->x;
      double d2 = d * d;
      if (d2 >= min_val2) continue;
      d = operator[](i)->y - another_residue[j]->y;
      d2 += d * d;
      if (d2 >= min_val2) continue;
      d = operator[](i)->z - another_residue[j]->z;
      d2 += d * d;
      if (d2 >= min_val2) continue;
      min_val2 = d2;
    }
  }

  return sqrt(min_val2);
}
PdbAtom_SP Residue::find_atom(const std::string & atom_name, PdbAtom_SP fallback_item)  {

  for(auto it=begin();it!=end();++it) {
    if ((*it)->atom_name().compare(atom_name) == 0)
      return *it;
  }

  return fallback_item;
}

const PdbAtom_SP Residue::find_atom(const std::string & atom_name, const PdbAtom_SP fallback_item)  const {

  for(auto it=begin();it!=end();++it)
    if((*it)->atom_name().compare(atom_name)==0)
      return *it;

  return fallback_item;
}

}
}
}
