/** @file Chain.hh Provides Chain class and some related utility functions
 */
#ifndef CORE_DATA_STRUCTURAL_Chain_H
#define CORE_DATA_STRUCTURAL_Chain_H

#include <string>
#include <memory>
#include <iterator>

#include <core/index.hh>
#include <core/data/io/Pdb.fwd.hh>

#include <core/data/basic/Vec3.hh>
#include <core/data/sequence/Sequence.hh>
#include <core/data/structural/Chain.fwd.hh>
#include <core/data/structural/Residue.fwd.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/PdbAtom.fwd.hh>
#include <core/data/structural/Structure.fwd.hh>
#include <core/data/structural/selectors/structure_selectors.fwd.hh>
#include <core/data/sequence/Sequence.hh>
#include <core/data/sequence/SecondaryStructure.hh>
#include <core/data/structural/SecondaryStructureElement.fwd.hh>

#include <core/chemical/Monomer.hh>

namespace core {
namespace data {
namespace structural {

/** \brief Single biopolymer chain, either a protein or nucleic.
 *
 * This Chain class is derived from <code>std::vector<std::shared_ptr<Residue> > </code>, which allows one
 * use algorithms defined in STL. For example, one can easily remove from a chain these residues which do
 * not have the <code>CA</code> atom:
 *
 * <code>
 * chain.erase(std::remove_if(chain.begin(),chain.end(),[](Residue_SP r){ return r->find_atom(" CA ")==nullptr;}),chain.end());
 * </code>
 */
class Chain :  public std::enable_shared_from_this<Chain>, private std::vector<std::shared_ptr<Residue> > {

  friend Structure;

  friend std::ostream& operator<<(std::ostream &out, const Chain &r);
  friend core::data::io::Pdb;
public:
  using std::vector<std::shared_ptr<Residue>>::assign;
  using std::vector<std::shared_ptr<Residue>>::at;
  using std::vector<std::shared_ptr<Residue>>::back;
  using std::vector<std::shared_ptr<Residue>>::begin;
  using std::vector<std::shared_ptr<Residue>>::capacity;
  using std::vector<std::shared_ptr<Residue>>::cbegin;
  using std::vector<std::shared_ptr<Residue>>::cend;
  using std::vector<std::shared_ptr<Residue>>::crbegin;
  using std::vector<std::shared_ptr<Residue>>::crend;
  using std::vector<std::shared_ptr<Residue>>::clear;
  using std::vector<std::shared_ptr<Residue>>::empty;
  using std::vector<std::shared_ptr<Residue>>::end;
  using std::vector<std::shared_ptr<Residue>>::erase;
  using std::vector<std::shared_ptr<Residue>>::front;
  using std::vector<std::shared_ptr<Residue>>::get_allocator;
  using std::vector<std::shared_ptr<Residue>>::insert;
  using std::vector<std::shared_ptr<Residue>>::max_size;
  using std::vector<std::shared_ptr<Residue>>::operator [];
  using std::vector<std::shared_ptr<Residue>>::pop_back;
  using std::vector<std::shared_ptr<Residue>>::rbegin;
  using std::vector<std::shared_ptr<Residue>>::rend;
  using std::vector<std::shared_ptr<Residue>>::reserve;
  using std::vector<std::shared_ptr<Residue>>::resize;
  using std::vector<std::shared_ptr<Residue>>::size;
  using std::vector<std::shared_ptr<Residue>>::swap;
  using std::vector<std::shared_ptr<Residue>>::shrink_to_fit;
  typedef  typename std::vector<std::shared_ptr<Residue>>::value_type  value_type;
  typedef  typename std::vector<std::shared_ptr<Residue>>::reference  reference;
  typedef  typename std::vector<std::shared_ptr<Residue>>::const_reference  const_reference;
  typedef  typename std::vector<std::shared_ptr<Residue>>::pointer  pointer;
  typedef  typename std::vector<std::shared_ptr<Residue>>::const_pointer  const_pointer;
  typedef  typename std::vector<std::shared_ptr<Residue>>::iterator  iterator;
  typedef  typename std::vector<std::shared_ptr<Residue>>::const_iterator  const_iterator;
  typedef  typename std::vector<std::shared_ptr<Residue>>::reverse_iterator  reverse_iterator;
  typedef  typename std::vector<std::shared_ptr<Residue>>::const_reverse_iterator  const_reverse_iterator;
  typedef  typename std::vector<std::shared_ptr<Residue>>::size_type  size_type;
  typedef  typename std::vector<std::shared_ptr<Residue>>::difference_type  difference_type;
  typedef  typename std::vector<std::shared_ptr<Residue>>::allocator_type  allocator_type;

public:

  class atom_const_iterator {
public:

    typedef std::vector<std::shared_ptr<Residue> >::const_iterator outer_iterator;
    typedef std::vector<std::shared_ptr<PdbAtom> >::const_iterator inner_iterator;

    typedef std::forward_iterator_tag iterator_category;
    typedef inner_iterator::value_type value_type;
    typedef inner_iterator::difference_type difference_type;
    typedef inner_iterator::pointer pointer;
    typedef inner_iterator::reference reference;

    atom_const_iterator() { }
    atom_const_iterator(outer_iterator it) : outer_it_(it), outer_end_(it) { }
    atom_const_iterator(outer_iterator it, outer_iterator end) : outer_it_(it), outer_end_(end) {

      if (outer_it_ == outer_end_) return; inner_it_ = (*outer_it_)->begin();
      advance_past_empty_inner_containers();
    }

    reference operator*()  const { return *inner_it_;  }
    pointer operator->() const { return &*inner_it_; }

    atom_const_iterator& operator++ () {
      ++inner_it_;
      if (inner_it_ == (*outer_it_)->end()) advance_past_empty_inner_containers();

      return *this;
    }

    atom_const_iterator operator++ (int) {
      atom_const_iterator it(*this);
      ++*this;

      return it;
    }

    friend bool operator==(const atom_const_iterator &a, const atom_const_iterator &b) {
      if (a.outer_it_ != b.outer_it_) return false;

      if (a.outer_it_ != a.outer_end_ && b.outer_it_ != b.outer_end_ && a.inner_it_ != b.inner_it_)
        return false;

      return true;
    }

    friend bool operator!=(const atom_const_iterator &a,const atom_const_iterator &b) { return !(a == b); }

private:

    void advance_past_empty_inner_containers() {
      while (outer_it_ != outer_end_ && inner_it_ == (*outer_it_)->end()) {
        ++outer_it_;
        if (outer_it_ != outer_end_) inner_it_ = (*outer_it_)->begin();
      }
    }

    outer_iterator outer_it_;
    outer_iterator outer_end_;
    inner_iterator inner_it_;
  };  // ~ atom_const_iterator

  class atom_iterator {
public:

    typedef std::vector<std::shared_ptr<Residue> >::iterator outer_iterator;
    typedef std::vector<std::shared_ptr<PdbAtom> >::iterator inner_iterator;

    typedef std::forward_iterator_tag iterator_category;
    typedef inner_iterator::value_type value_type;
    typedef inner_iterator::difference_type difference_type;
    typedef inner_iterator::pointer pointer;
    typedef inner_iterator::reference reference;

    atom_iterator() { }
    atom_iterator(outer_iterator it) : outer_it_(it), outer_end_(it) { }
    atom_iterator(outer_iterator it, outer_iterator end) : outer_it_(it), outer_end_(end) {

      if (outer_it_ == outer_end_) return; inner_it_ = (*outer_it_)->begin();
      advance_past_empty_inner_containers();
    }

    reference operator*()  const { return *inner_it_;  }
    pointer operator->() const { return &*inner_it_; }

    atom_iterator& operator+= (const unsigned int rhs) {

      for (unsigned int i = 0; i < rhs; ++i) {
        ++inner_it_;
        if (inner_it_ == (*outer_it_)->end()) advance_past_empty_inner_containers();
      }
      return *this;
    }

    atom_iterator& operator++ () {
      ++inner_it_;
      if (inner_it_ == (*outer_it_)->end()) advance_past_empty_inner_containers();

      return *this;
    }

    atom_iterator operator++ (int) {
      atom_iterator it(*this);
      ++*this;

      return it;
    }

    friend bool operator==(const atom_iterator &a, const atom_iterator &b) {
      if (a.outer_it_ != b.outer_it_) return false;

      if (a.outer_it_ != a.outer_end_ && b.outer_it_ != b.outer_end_ && a.inner_it_ != b.inner_it_)
    	  return false;

      return true;
    }

    friend bool operator!=(const atom_iterator &a,const atom_iterator &b) { return !(a == b); }

private:

    void advance_past_empty_inner_containers() {
      while (outer_it_ != outer_end_ && inner_it_ == (*outer_it_)->end()) {
        ++outer_it_;
        if (outer_it_ != outer_end_) inner_it_ = (*outer_it_)->begin();
      }
    }

    outer_iterator outer_it_;
    outer_iterator outer_end_;
    inner_iterator inner_it_;
  };  // ~ atom_iterator

public:
  // ---------- C-tors ----------
  /// Constructor creates  a new chain with a given ID
  Chain(const std::string& id) : id_(id) {}

  /** @brief Creates a new chains as a deep copy of this object.
   *
   * This method makes also a deep copy of all residues that belong to this structure (with their atoms, accordingly)
   */
  Chain_SP clone() const;

  /** @brief Creates a new chains as a deep copy of this object.
   *
   * This method makes also a deep copy of residues that belong to this structure
   * (with their atoms, accordingly) providing that they satisfy a given selector.
   *
   * @param which_residues - clone also residues and atoms which satisfy the given selector; the default value is
   *    selectors::ResidueSelector instance which selects every residue and every atom
   *
   * The following example shows how to clone a fragment of a structure or chain:
   * \include ex_StructureSelector.cc
   */
  Chain_SP clone(const selectors::ResidueSelector &which_residues) const;

  /** @brief Returns a const-pointer to the chain that directly precedes this chain in this structure.
   *
   * If this chain is the very first one, <code>nullptr</code> is returned
   */
  const Chain_SP previous() const;

  /** @brief Returns a const-pointer to the chain that directly follows this chain in this structure.
   *
   * If this chain is the very last one, <code>nullptr</code> is returned
   */
  const Chain_SP next() const;

  // ---------- Getters ----------
  /// Returns the string identifying this chain, e.g. "A" or "AC"
  inline const std::string & id() const { return id_; }

  /** @brief Returns the single character identifying this chain.
   * @return a single character ID of this string, which may be redundant for the structure this string belong to
   */
  inline char char_id() const { return id_[0]; }

  // ---------- Setters ----------
  /// Sets the new string identifying this residue, e.g. "A" or "AC"
  inline void id(const std::string & new_id)  { id_ = new_id; }

  // ---------- Atom tree operations ----------
  /// Returns a const-pointer to the structure owning this chain
  const std::shared_ptr<Structure> owner() const {
    std::shared_ptr<Structure> r = owner_.lock();
    if (r) return r;
    else return nullptr;
  }

  /// Sets the new owner (i.e. a structure) that owns this chain
  void owner(std::shared_ptr<Structure> new_owner) { owner_ = new_owner; }

  /// begin() iterator for atoms
  inline Chain::atom_const_iterator  first_const_atom() const { return atom_const_iterator(begin(),end()); };

  /// end() iterator for atoms
  inline Chain::atom_const_iterator  last_const_atom() const { return atom_const_iterator(end()); };

  /// begin() iterator for atoms
  inline Chain::atom_iterator  first_atom() { return atom_iterator(begin(), end()); };

  /// end() iterator for atoms
  inline Chain::atom_iterator  last_atom() { return atom_iterator(end()); };

  /** @brief Returns an iterator that points to the terminal residue of this chain.
   * Any residue that belongs to this chain but is located behind the terminal residue, should be treated as a ligand
   * If this chain has no ligands, this method returns the <code>this->end() - 1</code> pointer
   * @return pointer to the C terminal (3' terminal) residue of this chain
   */
  inline std::vector<std::shared_ptr<Residue> >::iterator terminal_residue() {

    if (terminal_index_ >= size()) return begin() + size() - 1;
    return begin() + terminal_index_;
  }

  /** @brief Returns a const-iterator that points to the terminal residue of this chain.
   * Any residue that belongs to this chain but is located behind the terminal residue, should be treated as a ligand.
   * If this chain has no ligands, this method returns the <code>this->cend() - 1</code> pointer
   * @return pointer to the C terminal (3' terminal) residue of this chain
   */
  inline std::vector<std::shared_ptr<Residue> >::const_iterator terminal_residue() const {

    if (terminal_index_ >= size()) return begin() + size() - 1;
    return begin() + terminal_index_;
  }

  /** @brief Sets given residue r as terminal_residue if r belong to this chain as. If no, throws an exception.
   * @param res - shared pointer to residue ti be set as terminal. 
   */
  void terminal_residue(Residue_SP res) {

    terminal_index_ = 0;
    for (Residue_SP i_res: *this) {
      if (i_res == res) return;
      ++terminal_index_;
    }
    std::string log("Terminal residue for chain is not in the chain");
    throw log;
  }
  
  /** @brief Returns the index of the terminal residue of this chain.
   * Any residue that belongs to this chain but is located behind the terminal residue, should be treated as a ligand
   * If this chain has no ligands, this method returns <code>this->size() - 1</code> which is the index
   * of the very last residue of this chain
   * @return pointer to the C terminal (3' terminal) residue of this chain
   */
  inline core::index2 terminal_residue_index() const { return terminal_index_; }

  /// end() iterator for atoms
  void push_back(Residue_SP r);

  /// Returns residue for a given residue_id number
  Residue_SP get_residue(const core::index2 residue_index);

  // ---------- Misc tree operations ----------
  /// Returns the number of atoms that belong to this chain
  inline core::index4 count_atoms() const {
    core::index4 sum=0;
    for(std::vector<std::shared_ptr<Residue> >::const_iterator it=begin(); it!=end(); ++it) sum +=(*it)->count_atoms();

    return sum;
  }

  /** @brief sorts atoms, residues and chains of this chain according to the PDB ordering.
   *
   * Atoms are sorted by their <code>id</code>. Residues are sorted by the means of
   * <code>bool operator<(const Residue & ri, const Residue & rj)</code> operator
   */
  void sort();

  /// Returns the number of residues that belong to this chain
  inline core::index2 count_residues() const { return size(); }

  /** @brief Returns true if Residues of this chain contain secondary structure information.
   * This method tests if any residue has secondary structure other than "coil"; chains consisting of loops only
   * will return <code>false</code>
   * @return true if this chain contains any residues annotated as helix or strand
   */
  bool has_secondary_structure() const;

  /** @brief Creates a Sequence object for this Chain.
   * The index of the first residue of the returned sequence will be set according to the index
   * of the very first residue in this chain. Sequence's comment will hold code of the structure this chain belongs to
   *
   * @param if_exclude_ligands - if false, the returned sequence will contain all small molecules
   * logically assigned to this chain (ligands, solvent, cofactors, etc.); if true - all ligands will be removed
   * @return a shared pointer to the newly created Sequence object
   */
  core::data::sequence::SecondaryStructure_SP create_sequence(bool if_exclude_ligands = true) const;

  /** @brief Creates a vector of secondary structure objects: helices and strands.
   *
   * The objects are created based on secondary structure assignment read from a PDB file header
   * @return a newly allocated vector of all secondary structure elements in this chain
   */
  std::vector<SecondaryStructureElement_SP> create_sse() const;

  /** @brief Creates  secondary structure objects: helices and strands and stores them in a given vectore.
   *
   * The objects are created based on secondary structure assignment read from a PDB file header
   * @return a vector of all secondary structure elements in this chain
   */
  std::vector<SecondaryStructureElement_SP> &create_sse(std::vector<SecondaryStructureElement_SP> &sse_vect) const;

  /** @brief Adds secondary structure information to this chain.
   * The <code>ss</code> field of every residue of this chain will be set to an appropriate character from a given string
   * @param secondary_structure - a string that holds secondary structure in HEC code
   */
  void annotate_ss(const std::string & secondary_structure);

  /** @brief Counts how many amino acid residues belong to this chain
   * @return the number of amino acid residues in this chain. In the case of a nucleic chain (e.g. a piece of DNA)
   * this method returns 0
   */
  core::index2 count_aa_residues() const;

  /** @brief Counts how many nucleic acid residues belong to this chain
   * @return the number of nucleic acid residues in this chain. In the case of a protein chain
   * this method returns 0
   */
  core::index2 count_na_residues() const;

  /** @brief Removes all ligands from this chain.
   */
  void remove_ligands();

  /** @brief Creates a C-\f$\alpha\f$ - only chain for a given amino acid sequence.
   * @param sequence - requested protein sequence
   * @param chain_id - id to be assigned to the newly created chain
   * @return a shared pointer to the newly created chain object
   */
  static Chain_SP create_ca_chain(const std::string & sequence, const std::string & chain_id = "A");

  /** @brief Creates a C-\f$\alpha\f$ - only chain for a given amino acid sequence and secondary structure
   * @param sequence - requested protein sequence
   * @param chain_id - id to be assigned to the newly created chain
   * @return a shared pointer to the newly created chain object
   */
  static Chain_SP create_ca_chain(const core::data::sequence::SecondaryStructure & secondary, const std::string & chain_id);

private:
  core::index2 terminal_index_ = 0;
  std::string id_;                                     ///< Id for this chain (a string of up to four characters)
  std::weak_ptr<Structure> owner_;
};

/** \brief Copy coordinates of all the atoms of the given chain into a given Coordinates object (which in fact is  std::vector<Vec3>).
 *
 * This method does not check if the destination vector has sufficient size.
 * @param chain - the source of atoms
 * @param coordinates - destination vector
 */
size_t chain_to_coordinates(const Chain_SP structure, core::data::basic::Coordinates & coordinates);

/** \brief Copy coordinates of the matching atoms of the given chain into a given Coordinates object (which in fact is  std::vector<Vec3>).
 *
 * This method does not check if the destination vector has sufficient size.
 * @param chain - the source of atoms
 * @param coordinates - destination vector
 * @param op - atom selector (boolean operator)
 */
size_t chain_to_coordinates(const Chain_SP chain, basic::Coordinates & coordinates, selectors::AtomSelector_SP op);

/// ostream operator just prints the chain code
std::ostream& operator<<(std::ostream &out, const Chain &c  );


/** @brief Creates a new chain of a given sequence.
 *
 * The new chain will contain all the Residue objects according to the provided sequence;
 * the residues however will contain no atoms
 *
 * @param id - ID of the new chain
 * @param sequence - amino acid or nucleic sequence of this chain
 */
Chain_SP chain_for_sequence(const std::string & id, const std::string & sequence);

/** @brief Creates a new chain of a given sequence.
 *
 * The new chain will contain all the Residue objects according to the provided sequence;
 * the residues however will contain no atoms
 *
 * @param id - ID of the new chain
 * @param sequence - amino acid or nucleic sequence of this chain
 */
Chain_SP chain_for_sequence(const std::string & id, const core::data::sequence::Sequence & sequence);

/** @brief Creates a new chain of a given sequence and secondary structure.
 *
 * The new chain will contain all the Residue objects according to the provided sequence;
 * the residues however will contain no atoms
 *
 * @param id - ID of the new chain
 * @param sequence - amino acid or nucleic sequence of this chain
 */
Chain_SP chain_for_sequence(const std::string & id, const core::data::sequence::SecondaryStructure & sequence);

}
}
}

#endif
/**
 * \example ex_StructureSelector.cc
 */
