/** @file SecondaryStructureElement.fwd.hh
 * @brief Provide definitions for shared pointer types to secondary structure elements (Helix, Strand, etc)
 */
#ifndef CORE_DATA_STRUCTURAL_SecondaryStructureElement_FWD_HH
#define CORE_DATA_STRUCTURAL_SecondaryStructureElement_FWD_HH

#include <memory>

namespace core {
namespace data {
namespace structural {

// Forwarded declaration of the SecondaryStructureElement class
class SecondaryStructureElement;
/// Declaration of a shared pointer to the SecondaryStructureElement class
typedef std::shared_ptr<SecondaryStructureElement> SecondaryStructureElement_SP;

// Forwarded declaration of the Strand class
class Strand;
/// Declaration of a shared pointer to the Strand class
typedef std::shared_ptr<Strand> Strand_SP;

// Forwarded declaration of the Helix class
class Helix;
/// Declaration of a shared pointer to the Helix class
typedef std::shared_ptr<Helix> Helix_SP;

// Forwarded declaration of the Sheet class
class Sheet;
/// Declaration of a shared pointer to the Sheet class
typedef std::shared_ptr<Sheet> Sheet_SP;

}
}
}

#endif // --- CORE_DATA_STRUCTURAL_SecondaryStructureElement_HH
