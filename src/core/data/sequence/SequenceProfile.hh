#ifndef CORE_DATA_SEQUENCE_SequenceProfile_H
#define CORE_DATA_SEQUENCE_SequenceProfile_H

#include <string>
#include <vector>
#include <map>
#include <utility>  // for std::make_pair()

#include <core/data/sequence/Sequence.hh>
#include <core/data/sequence/SequenceProfile.fwd.hh>

#include <core/chemical/Monomer.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace core {
namespace data {
namespace sequence {

using namespace core::chemical;

/** @brief Represents sequence profile.
 *
 * A sequence profile is a matrix of 20 or so probabilities defined for every position in an amino acid sequence. These
 * probabilities reflect how likely is to find any amino acid type (say, GLY or TYR) at that position.
 *
 * This class can create a sequence profile from a multiple sequence alignment, as in the example below:
 * @include ex_SequenceProfile
 * The sequence profile created from MSA data in the example has 23 columns: 20 columns for "regular" amino acids,
 * column for  the UNK residues ('X') and two columns for gap probabilities: gap opening (GAP residue type)
 * and gap extension (GPE residue type)
 *
 * The following example reads a multiple sequence alignment and writes a sequence profile
 * @example ap_SequenceProfile.cc
 */
class SequenceProfile : public Sequence {
public:

  static const std::vector<Monomer> & aaOrderByNCBI();
  static const std::vector<Monomer> & aaOrderByProperties();
  static const std::vector<Monomer> & aaOrderByNCBIGapped();
  static const std::vector<Monomer> & aaOrderByPropertiesGapped();

  /** @brief Creates a SequenceProfile object that holds just one sequence.
   *
   * @param header - header - description of the sequence
   * @param seq - the sequence from which this profile this created
   * @param column_types - define the order of column in this profile
   * @param first_pos - integer index of the first position in the input sequence
   */
  SequenceProfile(const std::string & header, const std::string & seq,
                  const std::vector<Monomer> & column_types, core::index2 first_pos = 0);

  /** @brief Creates a SequenceProfile object by averaging aligned sequence data, i.e. a multiple sequence alignment (MSA)
   *
   * @param header - header - description of the sequence
   * @param seq - the sequence from which this profile this created; The sequence should also be repeated in the sequence set,
   * otherwise it will not be included in the profile
   * @param column_types - define the order of column in this profile
   * @param sequence_set - the set of aligned sequences (as FASTA strings)
   * @param sequence_weights - fractional weights assigned to each sequence; these values will be used to weight
   *   sequences in this profile
   * @param first_pos - integer index of the first position in the input sequence
   */
  SequenceProfile(const std::string & header, const std::string & seq, const std::vector<Monomer> & column_types,
      const std::vector<std::string> & sequence_set,const std::vector<double> & sequence_weights = std::vector<double>{1, 1.0},
      core::index2 first_pos = 0);

  /** @brief Creates a SequenceProfile object by averaging aligned sequence data, i.e. a multiple sequence alignment (MSA)
   *
   * @param seq - the sequence from which this profile this created; The sequence should also be repeated in the sequence set,
   * otherwise it will not be included in the profile. Note, that this method creates a new, separate sequence object
   * @param column_types - define the order of column in this profile
   * @param sequence_set - the set of aligned sequences
   * @param sequence_weights - fractional weights assigned to each sequence; these values will be used to weight
   *   sequences in this profile
   */
  SequenceProfile(const Sequence & seq, const std::vector<Monomer> & column_types,
      const std::vector<Sequence_SP> & sequence_set, const std::vector<double> & sequence_weights = std::vector<double>{1, 1.0});

  /** @brief Create a copy of this profile, with a new column order
   *
   * @param column_types - the new order of columns
   * @return a newly created sequence profile
   */
  SequenceProfile_SP create_reordered(const std::vector<Monomer> & column_types);

  /** @brief Create a copy of this profile, with a new column order
   *
   * @param column_types - the new order of columns - as a string, each letter defines a residue type
   * @return a newly created sequence profile
   */
  SequenceProfile_SP create_reordered(const std::string & column_types);

  /** \brief Says which monomer type occupies given profile's column.
   * @param id - colum  id (from 0)
   * @return monomer type
   */
  inline const Monomer & monomer_type(const index2 id) { return Monomer::get(internal_column_types[id]); }

  /** \brief Says which profile column stores probability for the given amino acid type.
   *
   * <strong>Note</strong>, that this method can distinguish between gap opening and gap continuation, they must be
   * just properly identified by Monomer types.
   *
   * @param aaType - the amino acid type of interest (one of the twenty)
   * @return -1 when the column index could not be identified; otherwise returns
   *  column index in the range [0,19] (when profiles has no information about gaps) up to [0,21] when
   *    both gap opening and gap extension probabilities are stored
   */
  int column_index(const Monomer &aaType) const {
    if (internal_column_map.find(aaType.id) == internal_column_map.cend()) return -1;
    return internal_column_map.at(aaType.id);
  }

  /** \brief Says which profile column stores probability for the given amino acid type.
   *
   * <strong>Note</strong>, that this method does not distinguish between gap opening and gap continuation!
   *
   * @param aa_code1 - character denoting an amino acod type; use the value of <code>Sequence::gap_symbol</code>
   *    static variable to denote a gap
   * @return -1 when the column index could not be identified; otherwise returns
   *    a column index in the range [0,19] (when profiles has no information about gaps) up to [0,21] when
   *    both gap opening and gap extension probabilities are stored
   */
  int column_index(const char aa_code1) const {

    if (internal_column_map.find(Monomer::get(aa_code1).id) == internal_column_map.end()) return -1;
    return internal_column_map.at(Monomer::get(aa_code1).id);
  }

  /** \brief Says which profile column stores probability for the given amino acid type.
   * @param aa_code1 - the amino acid type of interest (one of the twenty)
   * @param prev_aa_code1 - amino acid type on the preceding position. If both aa_code1 and  prev_aa_code1
   *    are gaps, than Monomer::GPE column (gap extension) index is returned.
   * @return column index in the range [0,19] (when profiles has no information about gaps) up to [0,21] when
   *    both gap opening and gap extension probabilities are stored
   */
  int column_index(const char prev_aa_code1, const char aa_code1) const {

    if (internal_column_map.find(Monomer::get(aa_code1).id) == internal_column_map.end()) return -1;

    if (aa_code1 != Sequence::gap_symbol)
      return internal_column_map.at(Monomer::get(aa_code1).id);
    else {
      if (prev_aa_code1 != Sequence::gap_symbol) return Monomer::GAP.id;
      else return Monomer::GPE.id;
    }
  }

  /** \brief Sets new probability vector at a given position in a sequence.
   * @param sequence_position - zero-related index of a residue of interest
   * @param column_index - zero-related index of a profile column
   * @param probability - a value of probability
   * The column_index must be given in the same order as defined in the construction of this object
   */
  void set_probability(const index2 sequence_position, const index1 column_index, const float & probability) {

    data[sequence_position][column_index] = probability;
  }

  /** \brief Sets new probability vector at a given position in a sequence.
   *
   * @param sequence_position - zero-related index of a residue of interest
   * @param column_order - says what is the order of residue types in a given input vector of probabilities.
   * @param probabilities - a vector of probabilities;
   */
  void set_probabilities(const index2 sequence_position, const std::vector<Monomer> & column_order, const std::vector<float> & probabilities) {

    for(index2 i = 0;i < column_order.size();i++)
      data[sequence_position][internal_column_map.at(column_order[i].id)] = probabilities[i];
  }

  /** \brief Sets new probability vector at a given position in a sequence.
   * @param sequence_position - zero-related index of a residue of interest
   * @param probabilities - a vector of probabilities;
   * @tparam T array-like type that holds 20 real values, i.e. std::vector<float>
   * The values must be given in the same order as defined in the construction of this object
   */
  template<typename T>
  void set_probabilities(const index2 sequence_position, const T & probabilities) {

    for (index2 i = 0; i < internal_column_types.size(); ++i)
      data[sequence_position][i] = probabilities[i];
  }

  /** \brief Returns a probability for a given sequence position and a residue type.
   * @param sequence_position - zero-related index of a residue of interest
   * @param residue_type - residue type.
   * @return probability value
   */
  inline float get_probability(const index2 sequence_position, const Monomer & residue_type) const {
    return data[sequence_position][internal_column_map.at(residue_type.id)];
  }

  /** \brief Returns a probability for a given sequence position and a residue type.
   * @param sequence_position - zero-related index of a residue of interest
   * @param residue_index - residue type index, according to the ordering specified by the constructor.
   * @return probability value
   */
  inline float get_probability(const index2 sequence_position, const int residue_index) const {

    return data[sequence_position][residue_index];
  }

  /** \brief Returns a probability for a given sequence position and a residue type.
   * @param sequence_position - zero-related index of a residue of interest
   * @param residue_code_1 - one-letter code definition of a residue type
   * @return probability value
   */
  inline float get_probability(const index2 sequence_position, const char residue_code_1) const {

    return data[sequence_position][internal_column_map.at(Monomer::get(residue_code_1).id)];
  }

  /** @brief Counts distinct residue types that have been observed in a given column
   * This method does not take gaps and UNK into account
   * @param sequence_position - index of an alignment column number
   * @return distinct residue types count
   */
  index1 observed_residue_types(const index2 sequence_position) const;

  /** \brief Returns a row of probabilities measured for a given sequence position.
   * @param sequence_position - zero-related index of a residue of interest
   * @return a reference to a vector of 20 (21 or 22) probabilities observed at a given sequence position
   */
  inline const std::vector<float> & get_probabilities(const index2 sequence_position) const {

    return data[sequence_position];
  }

  /** \brief Returns a column of probabilities measured for a given residue type.
   * @param res_type - zero-related index of a residue of interest
   * @param out - a vector where the data will be stored; the vector will be cleared before copying the data
   */
  void get_probabilities(const Monomer res_type, std::vector<float> & out) const;

  /** \brief Writes all data related to a single row i.e. to a single position in a protein sequence
   *
   * @param which_row - index of a row to print (from 0)
   * @param out - output stream
   */
  void write_row(const index2 which_row,std::ostream & out) const;

  /** \brief Writes all profile's probabilities to a stream in a form of a nice table.
   * @param out - output stream
   */
  void write_table(std::ostream & out) const;

  /** \brief Writes a header line for the table of probabilities.
   *
   * The line says which aa type is in each column
   * @param out - output stream
   */
  void write_table_header(std::ostream & out) const;

  /** \brief Writes all profile's probabilities to standard output (stdout) in a form of a nice table.
   * @param out - output stream
   */
  void write_table() const;

  /** \brief Writes a header line for the table of probabilities  to standard output (stdout) 
   *
   * The line says which aa type is in each column
   * @param out - output stream
   */
  void write_table_header() const;

  /** @brief Convert this profile to PSSM.
   *
   * A PSSM - Position Specific Scoring Matrix is just a logarithm of a sequence profile
   * @param logarithm_base - the base of logarithm to be used - by default it's 2
   */
  void to_pssm(double logarithm_base = 2);

private:
  std::map<index2, index2> internal_column_map;
  std::vector<index2> internal_column_types;
  std::vector<std::vector<float>> data;
  utils::Logger logger;
  bool is_pssm_;

  void init(const std::vector<Monomer> & column_types);

  friend SequenceProfile_SP read_profile_table(const std::string & file_name);
};

/** \brief Reads a sequence profile from a checkpoint file produced by legacy Blast
 *
 * @param file_name - input file - *.chk binary format; its no longer supported by NCBI;
 * this BioShell function  allows reading old legacy files
 * @returns shared pointer to the sequence profile object
 */
SequenceProfile_SP read_binary_checkpoint(const std::string & file_name);

/** \brief Reads a sequence profile from a checkpoint file produced by new Blast+
 *
 * @param file_name - input file (ASN.1 format)
 * @returns shared pointer to the sequence profile object
 */
SequenceProfile_SP read_ASN1_checkpoint(const std::string & file_name);

/** \brief Reads a sequence profile from a checkpoint file produced by new Blast+
 *
 * @param file_name - input file (ASN.1 format)
 * @returns shared pointer to the sequence profile object
 */
SequenceProfile_SP read_ASN1_checkpoint(const std::string & file_name);

/** \brief Reads a sequence profile in from flat text file (BioShell format)
 *
 * @param file_name - input file (BioShell format)
 * @returns shared pointer to the sequence profile object
 */
SequenceProfile_SP read_profile_table(const std::string & file_name);

}
}
}

/**
 * @example ap_SequenceProfile.cc
 */

#endif
