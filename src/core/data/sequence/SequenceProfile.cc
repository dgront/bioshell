#include <map>
#include <cmath>
#include <vector>
#include <iomanip>
#include <algorithm>
#include <stdio.h>
#include <iostream>
#include <fstream>

#include <core/data/sequence/SequenceProfile.hh>
#include <core/chemical/Monomer.hh>
#include <utils/io_utils.hh>

namespace core {
namespace data {
namespace sequence {

using namespace core::chemical;
const std::vector<Monomer> & SequenceProfile::aaOrderByNCBI() {

  static const std::vector<Monomer> aaOrderByNCBI( { Monomer::ALA, Monomer::ARG, Monomer::ASN, Monomer::ASP,
    Monomer::CYS, Monomer::GLN, Monomer::GLU, Monomer::GLY, Monomer::HIS, Monomer::ILE, Monomer::LEU, Monomer::LYS,
    Monomer::MET, Monomer::PHE, Monomer::PRO, Monomer::SER, Monomer::THR, Monomer::TRP, Monomer::TYR, Monomer::VAL, Monomer::UNK });

  return aaOrderByNCBI;
}

const std::vector<Monomer> & SequenceProfile::aaOrderByNCBIGapped() {

  static const std::vector<Monomer> aaOrderByNCBI( { Monomer::ALA, Monomer::ARG, Monomer::ASN, Monomer::ASP,
    Monomer::CYS, Monomer::GLN, Monomer::GLU, Monomer::GLY, Monomer::HIS, Monomer::ILE, Monomer::LEU, Monomer::LYS,
    Monomer::MET, Monomer::PHE, Monomer::PRO, Monomer::SER, Monomer::THR, Monomer::TRP, Monomer::TYR, Monomer::VAL,
    Monomer::UNK, Monomer::GAP, Monomer::GPE});

  return aaOrderByNCBI;
}

const std::vector<Monomer> & SequenceProfile::aaOrderByProperties() {
  static const std::vector<Monomer> aaOrderByProperties({Monomer::ALA, Monomer::VAL, Monomer::PRO, Monomer::ILE, Monomer::LEU, Monomer::MET, // hydrophobic
  Monomer::GLU, Monomer::ASP, // acidic
  Monomer::ARG, Monomer::LYS, // basic
  Monomer::THR, Monomer::SER, Monomer::HIS, Monomer::CYS, Monomer::GLN, Monomer::ASN, // other polar
  Monomer::PHE, Monomer::TYR, Monomer::TRP, // aromatic
  Monomer::GLY, Monomer::UNK});

  return aaOrderByProperties;
}

const std::vector<Monomer> & SequenceProfile::aaOrderByPropertiesGapped() {
  static const std::vector<Monomer> aaOrderByProperties({Monomer::ALA, Monomer::VAL, Monomer::PRO, Monomer::ILE, Monomer::LEU, Monomer::MET, // hydrophobic
    Monomer::GLU, Monomer::ASP, // acidic
    Monomer::ARG, Monomer::LYS, // basic
    Monomer::THR, Monomer::SER, Monomer::HIS, Monomer::CYS, Monomer::GLN, Monomer::ASN, // other polar
    Monomer::PHE, Monomer::TYR, Monomer::TRP, // aromatic
    Monomer::GLY, Monomer::UNK, Monomer::GAP, Monomer::GPE});

  return aaOrderByProperties;
}

void SequenceProfile::init(const std::vector<Monomer> & column_types) {

  for (index2 i = 0; i < column_types.size(); ++i) internal_column_types.push_back(column_types[i].id);

  for (index2 i = 0; i < sequence.size(); ++i) data[i].resize(internal_column_types.size());
  for (index2 i = 0; i < column_types.size(); ++i)
    internal_column_map.insert(std::pair<index2, index2>(column_types[i].id, i));

  if (logger.is_logable(utils::LogLevel::FINER)) {
    logger << utils::LogLevel::FINER << " ";
    for (const auto &m:internal_column_map) logger << " (" << Monomer::get(m.first).code3 << "=>" << m.second << ")";
  }
}

SequenceProfile::SequenceProfile(const std::string & header, const std::string & seq,
                const std::vector<Monomer> & column_types, core::index2 first_pos) :
  Sequence(header, seq, first_pos), data(seq.length()),
  logger("SequenceProfile") {

  is_pssm_ = false;
  init(column_types);
  for (index2 i = 0; i < length(); ++i)
    data[i][internal_column_map[get_monomer(i).id]] = 1.0;
}

SequenceProfile::SequenceProfile(const std::string & header, const std::string & seq,
                const std::vector<Monomer> & column_types, const std::vector<std::string> & sequence_set,
                const std::vector<double> & sequence_weights, core::index2 first_pos) :
                Sequence(header, seq, first_pos), data(seq.length()), logger("SequenceProfile") {

  is_pssm_ = false;
  if (sequence_set.size() < sequence_weights.size())
    throw std::invalid_argument{"Number of input sequences is smaller than the number of weight values!"};

  init(column_types);

  int i = 0;
  core::index4 seq_idx = 0;
  for (const std::string &i_seq : sequence_set) {
    double w = sequence_weights[seq_idx % sequence_weights.size()];
    if ((i = column_index(i_seq[0])) > -1) data[0][i] += w;
    for (index4 i_pos = 1; i_pos < i_seq.length(); ++i_pos) {
      if ((i = column_index(i_seq[i_pos - 1], i_seq[i_pos])) > -1) data[i_pos][i] += w;
    }
    ++seq_idx;
  }
  for (index4 i_pos = 0; i_pos < data.size(); ++i_pos) {
    double total = 0.0;
    for (index4 i_col = 0; i_col < data[0].size(); ++i_col) total += data[i_pos][i_col];
    for (index4 i_col = 0; i_col < data[0].size(); ++i_col) data[i_pos][i_col] /= total;
  }
}

SequenceProfile::SequenceProfile(const Sequence &seq, const std::vector<Monomer> &column_types,
    const std::vector<Sequence_SP> &sequence_set, const std::vector<double> & sequence_weights)
    : Sequence(seq), data(seq.length()), logger("SequenceProfile") {

  is_pssm_ = false;
  if (sequence_set.size() < sequence_weights.size())
    throw std::invalid_argument{"Number of input sequences is smaller than the number of weight values!"};

  init(column_types);

  core::index4 seq_idx = 0;
  for (const Sequence_SP i_seq : sequence_set) {
    double w = sequence_weights[seq_idx % sequence_weights.size()];
    for (index4 i_pos = 0; i_pos < data.size(); ++i_pos) {
      const core::chemical::Monomer & m = i_seq->get_monomer(i_pos);
      if (internal_column_map.find(m.id) != internal_column_map.end()) {
        data[i_pos][internal_column_map[m.id]]+=w;
      }
    }
    ++seq_idx;
  }
  for (index4 i_pos = 0; i_pos < data.size(); ++i_pos) {
    double total = 0.0;
    for (index4 i_col = 0; i_col < data[0].size(); ++i_col) total += data[i_pos][i_col];
    for (index4 i_col = 0; i_col < data[0].size(); ++i_col) data[i_pos][i_col] /= total;
  }
}

index1 SequenceProfile::observed_residue_types(const index2 sequence_position) const {
  return std::count_if(data[sequence_position].cbegin(), data[sequence_position].cbegin() + 20,
                       [](float p) { return p > 0.000000000001; });
}

SequenceProfile_SP SequenceProfile::create_reordered(const std::vector<Monomer> & column_types) {

  SequenceProfile_SP sp = std::make_shared<SequenceProfile>("", sequence, column_types, first_pos());

  for (core::index2 i = 0; i < length(); ++i) {
    for(core::index1 j=0;j<column_types.size();++j)
      sp->set_probability(i,j,get_probability(i, column_types[j]));
  }

  return sp;
}

SequenceProfile_SP SequenceProfile::create_reordered(const std::string & column_types) {

  std::vector<core::chemical::Monomer> column_order;
  for(const char c: column_types) column_order.push_back( core::chemical::Monomer::get(c));

  return create_reordered(column_order);
}

void SequenceProfile::get_probabilities(const Monomer res_type, std::vector<float> & out) const {

  out.clear();
  index2 id = internal_column_map.at(res_type.id);
  for(const std::vector<float> & row : data) out.push_back(row[id]);
}

void SequenceProfile::write_row(const index2 which_row, std::ostream &out) const {

  out << utils::string_format("%4d %c", first_pos() + which_row, get_monomer(which_row).code1);

  for (index2 j = 0; j < internal_column_types.size(); j++) {
    if (is_pssm_)
      out << utils::string_format(" %4.1f", get_probability(which_row, j));
    else
      out << utils::string_format(" %5.3f", get_probability(which_row, j));
  }
}

void SequenceProfile::write_table(std::ostream & out) const {

  write_table_header(out);
  for (core::index2 i = 0; i < length(); ++i) {
    write_row(i, out);
    out << "\n";
  }
}

void SequenceProfile::write_table_header(std::ostream &out) const {

  out << "#id aa";
  for (index2 j = 0; j < internal_column_types.size(); j++)
    out << "  " << Monomer::get(internal_column_types[j]).code3 << " ";
  out << "\n";
}

void SequenceProfile::write_table() const { write_table(std::cout); }

void SequenceProfile::write_table_header() const { write_table_header(std::cout); }

void SequenceProfile::to_pssm(double logarithm_base) {

  is_pssm_ = true;
  double f = log(logarithm_base);
  for(core::index2 i=0;i<sequence.size();++i) {
    for (index2 j = 0; j < internal_column_types.size(); j++) {
      data[i][j] = log(data[i][j]) / f;
    }
  }
}

SequenceProfile_SP read_binary_checkpoint(const std::string & file_name) {

  double x = 0;
  char bb4[4];
  std::ifstream myfile (file_name, std::ios::in|std::ios::binary);
  myfile.read(bb4,4);

  int seqLength = bb4[0];
  int b2 = ((int) bb4[1]) << 8;
  int b3 = ((int) bb4[2]) << 16;
  int b4 = ((int) bb4[3]) << 24;

  seqLength = (seqLength < 0) ? 256 + seqLength + b2 + b3 + b4 : seqLength + b2 + b3 + b4;

  char seq[seqLength+1];
  for (int i = 0; i < seqLength; ++i) seq[i] = myfile.get();
  seq[seqLength] = 0;
  std::string strSeq(seq);

  SequenceProfile_SP sp = std::make_shared<SequenceProfile>("",strSeq,SequenceProfile::aaOrderByNCBI(),1);
  char bb8[8];
  double row[20];
  for(int i = 0;i < seqLength;i++) {
    for(int j = 0;j < 20;j++) {
      myfile.read(bb8,8);
      std::copy(bb8, bb8 + sizeof(double), reinterpret_cast<char*>(&x));
//        x = swap(x);
      row[j] = x;
    }
    sp->set_probabilities(i,row);
  }

  return sp;
}


SequenceProfile_SP read_ASN1_checkpoint(const std::string & file_name) {

  static utils::Logger l("read_ASN1_checkpoint");
  static const char *ncbicodes = "XAXCDEFGHIKLMNPQRSTVWXYXXXXX";
  std::string data = utils::load_text_file(file_name);
  data.erase(std::remove_if( data.begin(), data.end(),
       [](char c){ return (c ==',' || c =='}' || c == '{');}), data.end() );
  std::replace(data.begin(), data.end(), '\n', ' ');
  std::vector<std::string> tokens;
  utils::split(data,tokens,' ');

  auto it = std::find(tokens.begin(), tokens.end(), std::string("length"));
  core::index2 length = 0;
  if (it == tokens.end()) {
    l<<utils::LogLevel::SEVERE << "Bad sequence record in checkpoint file (can't find sequence's length)!\n";
  } else length = utils::from_string<core::index2>(*(it + 1));

  it = std::find(tokens.begin(), tokens.end(), std::string("ncbistdaa"));
  if (it == tokens.end()) {
    l << utils::LogLevel::SEVERE << "Bad sequence record in checkpoint file (can't find sequence)!\n";
  }
  std::string seq = *(++it);
  while (!utils::ends_with(*(it), std::string("'H"))) {
    ++it;
    seq += (*it);
  }
  seq = seq.substr(1,seq.length()-3);

  std::string seq_str;
  for (core::index2 i = 0; i < seq.length(); ++i) {
    char ch = seq[i];

    if (!isxdigit(ch)) std::cerr << "Bad sequence record in checkpoint file!\n";
    char aa;
    if (ch >= 'A') aa = 16 * (10 + ch - 'A');
    else aa = 16 * (ch - '0');

    ch = seq[++i];
    if (!isxdigit(ch)) std::cerr << "Bad sequence record in checkpoint file!\n";
    if (ch >= 'A') aa += 10 + ch - 'A';
    else aa += ch - '0';

    seq_str += ncbicodes[int(aa)];
  }
  l << utils::LogLevel::INFO << "Loaded sequence: " << seq_str << "\n";

  SequenceProfile_SP sp = std::make_shared<SequenceProfile>("", seq_str, SequenceProfile::aaOrderByNCBI(), 1);

  it = std::find(tokens.begin(), tokens.end(), std::string("weightedResFreqsPerPos"));
  if (it == tokens.end()) {
    std::cerr << "Can't read probabilities from a checkpoint file!\n";
  } else {
    std::vector<core::index1> ids(28, 0);
    for (core::index1 j = 0; j < 28; j++) {
      ids[j] = sp->column_index(core::data::sequence::Monomer::get(ncbicodes[j]));
    }
    for (core::index2 i = 0; i < length; i++)
      for (core::index1 j = 0; j < 28; j++) {
        double val = utils::from_string<double>(*(++it));
        double base = utils::from_string<double>(*(++it));
        double power = utils::from_string<double>(*(++it));
        val = val * pow(base, power);
        sp->set_probability(i, ids[j], val);
      }
  }

  return sp;
}

std::string format_20 = "%d %c %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f";
std::string format_21 = "%d %c %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f";
std::string format_22 = "%d %c %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f";
std::string format_23 = "%d %c %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f";

SequenceProfile_SP read_profile_table(const std::string & file_name) {

  static utils::Logger logs("read_profile_table");
  std::ifstream inp(file_name);
  std::string line;
  index4 cnt = 0;

  std::vector<std::string> lines;
  utils::split(utils::load_text_file(file_name), lines, {'\n'}, false);
  logs << utils::LogLevel::FILE << lines.size()<< " lines loaded from "<< file_name << "\n";

  std::vector<std::string> header_tokens = utils::split(lines[0], {' '});
  const index1 n_pos = header_tokens.size();
  logs << utils::LogLevel::INFO << int(n_pos)<< " columns found\n";
  if (n_pos < 22 || n_pos > 25) { // --- wrong file format
    logs << utils::LogLevel::SEVERE << "Wrong profile format - incorrect number of columns! Input file given: "<< file_name << "\n";
    return nullptr;
  }
  std::vector<Monomer> order;
  for (int i = 2; i < n_pos; ++i) order.push_back(Monomer::get(header_tokens[i]));
  std::string sequence = "";
  for (core::index2 i_line = 1; i_line < lines.size(); ++i_line) sequence += lines[i_line][5];

  SequenceProfile_SP sp = std::make_shared<SequenceProfile>("", sequence, order, 1);
  int id;
  char aa;
  for (core::index2 i_line = 1; i_line < lines.size(); ++i_line) {
    ++cnt;
    if(n_pos==22) std::sscanf(lines[i_line].c_str(),format_20.c_str(),&id, &aa,
        &(sp->data)[i_line-1][0],&(sp->data)[i_line-1][1],&(sp->data)[i_line-1][2],&(sp->data)[i_line-1][3],&(sp->data)[i_line-1][4],
        &(sp->data)[i_line-1][5],&(sp->data)[i_line-1][6],&(sp->data)[i_line-1][7],&(sp->data)[i_line-1][8],&(sp->data)[i_line-1][9],
        &(sp->data)[i_line-1][10],&(sp->data)[i_line-1][11],&(sp->data)[i_line-1][12],&(sp->data)[i_line-1][13],&(sp->data)[i_line-1][14],
        &(sp->data)[i_line-1][15],&(sp->data)[i_line-1][16],&(sp->data)[i_line-1][17],&(sp->data)[i_line-1][18],&(sp->data)[i_line-1][19]);
    if(n_pos==23) std::sscanf(lines[i_line].c_str(),format_21.c_str(),&id, &aa,
        &(sp->data)[i_line-1][0],&(sp->data)[i_line-1][1],&(sp->data)[i_line-1][2],&(sp->data)[i_line-1][3],&(sp->data)[i_line-1][4],
        &(sp->data)[i_line-1][5],&(sp->data)[i_line-1][6],&(sp->data)[i_line-1][7],&(sp->data)[i_line-1][8],&(sp->data)[i_line-1][9],
        &(sp->data)[i_line-1][10],&(sp->data)[i_line-1][11],&(sp->data)[i_line-1][12],&(sp->data)[i_line-1][13],&(sp->data)[i_line-1][14],
        &(sp->data)[i_line-1][15],&(sp->data)[i_line-1][16],&(sp->data)[i_line-1][17],&(sp->data)[i_line-1][18],&(sp->data)[i_line-1][19],
        &(sp->data)[i_line-1][20]);
    if(n_pos==24) std::sscanf(lines[i_line].c_str(),format_22.c_str(),&id, &aa,
        &(sp->data)[i_line-1][0],&(sp->data)[i_line-1][1],&(sp->data)[i_line-1][2],&(sp->data)[i_line-1][3],&(sp->data)[i_line-1][4],
        &(sp->data)[i_line-1][5],&(sp->data)[i_line-1][6],&(sp->data)[i_line-1][7],&(sp->data)[i_line-1][8],&(sp->data)[i_line-1][9],
        &(sp->data)[i_line-1][10],&(sp->data)[i_line-1][11],&(sp->data)[i_line-1][12],&(sp->data)[i_line-1][13],&(sp->data)[i_line-1][14],
        &(sp->data)[i_line-1][15],&(sp->data)[i_line-1][16],&(sp->data)[i_line-1][17],&(sp->data)[i_line-1][18],&(sp->data)[i_line-1][19],
        &(sp->data)[i_line-1][20],&(sp->data)[i_line-1][21]);
    if(n_pos==25) std::sscanf(lines[i_line].c_str(),format_23.c_str(),&id, &aa,
        &(sp->data)[i_line-1][0],&(sp->data)[i_line-1][1],&(sp->data)[i_line-1][2],&(sp->data)[i_line-1][3],&(sp->data)[i_line-1][4],
        &(sp->data)[i_line-1][5],&(sp->data)[i_line-1][6],&(sp->data)[i_line-1][7],&(sp->data)[i_line-1][8],&(sp->data)[i_line-1][9],
        &(sp->data)[i_line-1][10],&(sp->data)[i_line-1][11],&(sp->data)[i_line-1][12],&(sp->data)[i_line-1][13],&(sp->data)[i_line-1][14],
        &(sp->data)[i_line-1][15],&(sp->data)[i_line-1][16],&(sp->data)[i_line-1][17],&(sp->data)[i_line-1][18],&(sp->data)[i_line-1][19],
        &(sp->data)[i_line-1][20],&(sp->data)[i_line-1][21],&(sp->data)[i_line-1][22]);
  }

  logs.log(utils::LogLevel::FILE, "found ", int(cnt), " positions in ", file_name, " profile\n");

  return sp;
}

}
}
}
