/** \file sequence_utils.hh
 * @brief Some utility methods that operate on protein and nucleic sequences.
 *
 */
#ifndef CORE_DATA_SEQUENCE_sequence_utils_H
#define CORE_DATA_SEQUENCE_sequence_utils_H

#include <string>
#include <cstdlib>
#include <regex>

#include <core/index.hh>
#include <core/data/sequence/Sequence.hh>
#include <core/chemical/Monomer.hh>

namespace core {
namespace data {
namespace sequence {

/// Extracts gi-number form a sequence header string.
/**
 * @param sequence_header - a sequence header which contains a gi number, formatted as gi|123456|
 * @return gi number if it was found in the given sequence header; 0 otherwise
 */
core::index4 extract_gid(const std::string & sequence_header);

/** @brief Returns true if a given string contains <strong>only</strong> characters corresponding to amino acids (one-letter code)
 * @param sequence - a putative sequence
 * @return true if this is an amino acid sequence
 */
bool contains_aa_only(const std::string &sequence);

/** @brief Returns true if a given string  actually encodes secondary structure.
 *
 * If this secondary structure string is a part of an alignment, use <code>remove_gaps(const std::string &)</code>
 * method before calling this function
 *
 * The string must contain only 'H', 'E', 'C' letters
 * @param sequence - a putative secondary structure
 * @return true if this is secondary structure string
 */
bool is_secondary_structure(const std::string &sequence);

/** @brief Returns the number of residues in a sequence that are not gaps.
 * @param sequence - an input sequence
 * @return the number of non-gap symbols in that sequence
 */
core::index2 length_without_gaps(const std::string & sequence);

/** @brief Removes all gap characters ('_' and '-') from a given sequence string.
 *
 * This method does modify the argument sequence.
 * @param sequence_string - an input sequence
 */
void remove_gaps(std::string & sequence_string);

/** @brief Removes all gap characters ('_' and '-') from a given sequence string.
 *
 * This method doesn't modify the argument sequence.
 * @param sequence_string - an input sequence
 * @return the newly allocated sequence string - with no gaps
 */
std::string remove_gaps(const std::string & sequence_string);

/** @brief Counts amino acid characters in the given sequence
 * @param seq - an input sequence
 * @return the number of characters corresponding to amino acids
 */
core::index2 count_aa(const std::string &seq);

/** @brief Counts amino acid residues in the given sequence
 * @param seq - an input sequence
 * @return the number of residues recognized as amino acid monomers
 */
core::index2 count_aa(const Sequence &seq);

/** @brief Counts nucleic acid residues in the given sequence
 * @param seq - an input sequence
 * @return the number of characters corresponding to nucleic acids (A, C, T, G, U, a, c, t, g or u)
 */
core::index2 count_nt(const std::string &seq);

/** @brief Counts amino acid residues in the given sequence
 * @param seq - an input sequence
 * @return the number of residues recognized as nucleic acid monomers
 */
core::index2 count_nt(const Sequence &seq);

/** @brief Count residues by type and accumulates these counts to a given vector
 *
 * @param sequence - an amino acid sequence as a string
 * @param counts - vector to accumulate counts from the given sequence; the sise of the vector should be 30 since there are
 * 30 basic (non-modified) residue types defined in Monomer.hh
 */
void count_residues_by_type(const std::string &sequence, std::vector<core::index4> &counts);

/** @brief Counts how many times each amino acid type appears in a given sequence
 * @param seq - an input sequence
 * @param counts - vector of size 21 to store the statistics. This method does not clear the vector, but if its shorter than 21,
 *      it will be resized
 * @return the vector with (updated) statistics
 */
std::map<core::chemical::Monomer,index4> count_residues_by_type(const Sequence &seq);

/** @brief Counts how many times each residue type appears in a given sequence.
 *
 * @param seq - an input sequence, as <code>std::string</code>
 * @return the map using  <code>Monomer</code> objects as keys that holds a non-zero count of how many times a given Monomer type
 * was found in an input sequence string
 */
std::map<core::chemical::Monomer,index4> count_residues_by_type(const std::string &seq);

/** @brief Counts how many times each residue type appears in a given sequence.
 *
 * @param seq - an input sequence
 * @return the map using  <code>Monomer</code> objects as keys that holds a non-zero count of how many times a given Monomer type
 * was found in an input sequence string
 */
std::map<core::chemical::Monomer,index4> count_residues_by_type(const std::vector<Sequence> &seq);

/** @brief Counts how many times each residue type appears in a given set of sequences.
 *
 * @param seq - an input vector of sequences
 * @return the map using  <code>Monomer</code> objects as keys that holds a non-zero count of how many times a given Monomer type
 * was found in an input sequence string
 */
std::map<core::chemical::Monomer,index4> count_residues_by_type(const std::vector<Sequence_SP> &seq);

/** @brief Counts how many times each residue type appears in a given set of sequences.
 *
 * @param seq - an input vector of sequence strings
 * @return the map using  <code>Monomer</code> objects as keys that holds a non-zero count of how many times a given Monomer type
 * was found in an input sequence string
 */
std::map<core::chemical::Monomer,index4> count_residues_by_type(const std::vector<std::string> &seq);

/** @brief Counts how many different amino acid types show up in a given amino acid sequence.
 * @param seq - an input sequence
 * @return the number of different amino acid types, e.g. for the sequence "GPGPGPGPGP" this method will return 2
 */
core::index1 count_aa_types(const std::string &seq);

/** @brief Counts how many different amino acid types show up in a given amino acid sequence.
 * @param seq - an input sequence
 * @return the number of different amino acid types, e.g. for the sequence "GPGPGPGPGP" this method will return 2
 */
core::index1 count_aa_types(const Sequence &seq) ;

/** @brief Make sure all characters representing residues of a sequence are compatible with Monomers
 *
 * @param sequence - input sequence, which may be modified to fix non-canonical amino acids such as 'B' or 'Z'
 */
std::string& fix_code1(std::string & sequence);

/** @brief Counts helices ('H') and strands ('E') in a given secondary structure string (HEC code)
 * @param hec_secondary - a string with H, E, and C characters describing a secondary structure of a protein
 * @return  counts of helices and strands
 */
std::pair<index2, index2> count_helices_strands(const std::string & hec_secondary);

}
}
}

#endif
