#ifndef CORE_DATA_SEQUENCE_ReduceSequenceAlphabet_H
#define CORE_DATA_SEQUENCE_ReduceSequenceAlphabet_H

#include <string>
#include <memory>
#include <map>
#include <iostream>

#include <core/data/sequence/Sequence.fwd.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace core {
namespace data {
namespace sequence {

class ReduceSequenceAlphabet;
typedef std::shared_ptr<ReduceSequenceAlphabet> ReduceSequenceAlphabet_SP;

/** @brief  Represents amino acid or nucleic acid sequence.
 *
 * The class provides a number of reduced amino acid alphabets published in the literature, extracted from this work:
 * Peterson, Kondev, Theriot and Phillips. “Reduced amino acid alphabets exhibit an improved sensitivity and selectivity in fold assignment”.  Bioinformatics 2009 25:1356-1362
 *
 * In the following example a ReduceSequenceAlphabet instance was used to create a reduced sequence
 * and to make sequence k-tuples.
 * \include ex_k_tuples.cc
 * The second example shows what alphabets are implemented in BioShell; it also prints the conversion rules for a requested alphabets
 * \include ex_ReduceSequenceAlphabet.cc
 */
class ReduceSequenceAlphabet {
public:

  static const ReduceSequenceAlphabet_SP get_alphabet(const std::string & alphabet_name);

  static bool has_alphabet(const std::string & alphabet_name) {

    return reduced_sequence_alphabets.find(alphabet_name)!=reduced_sequence_alphabets.end();
  }

  static std::map<std::string,ReduceSequenceAlphabet_SP>::const_iterator cbegin();

  static std::map<std::string,ReduceSequenceAlphabet_SP>::const_iterator cend();

  /// Prints an alphabet nicely formatted
  friend std::ostream & operator<<(std::ostream &out, const ReduceSequenceAlphabet &mapping);

  /// Returns a letter from this alphabet
  char operator[](const unsigned char index) const { return map_to_reduced[index]; }

  /// Says how many letters has the reduced alphabet
  index1 alphabet_size() const { return alphabet_size_; }

  /// Returns a character from a reduced alphabet that corresponds to a given original character
  char reduced_character(const char original_character) const { return map_to_original[map_to_reduced[original_character]]; }

  /// Returns the index of a character from a reduced alphabet that corresponds to a given original character
  char reduced_index(const char original_character) const { return map_to_reduced[original_character]; }

  /** @brief Encodes a given amino acid sequence.
   *
   * Returned sequence contains only letters from this alphabet. The actual characters (letters)
   * may be obtained by calling the <code>operator[](const unsigned char)</code> of this class.
   * @param aa_sequence - a sequence to be reduced
   * @return a corresponding sequence expressed in this reduced alphabet
   */
  std::string encode_indexes(const std::string & aa_sequence) const {

    std::string out(aa_sequence.size(),0);

    size_t i = 0;
    while(i<aa_sequence.size()) {
      out[i] = map_to_reduced[aa_sequence[i]];
      ++i;
    }

    return out;
  }

  /** @brief Encodes a given amino acid sequence.
   *
   * Returned string contains <strong>indexes</strong> to characters from this alphabet.
   * @param aa_sequence - a sequence to be reduced
   * @return a string of unsigned char indexes pointing to letters of this alphabet
   */
  std::string encode_sequence(const std::string & aa_sequence) const {

    std::string out(aa_sequence.size(), 'X');

    size_t i = 0;
    while(i<aa_sequence.size()) {
      out[i] = map_to_original[map_to_reduced[aa_sequence[i]]];
      ++i;
    }

    return out;
  }

private:
  utils::Logger logs;
  index1 alphabet_size_;
  std::vector<char> map_to_original; // provides a representative residue symbol for each group, e.g. map_to_original[3] returns a character of an amino acid that belongs to the grup indexed by 3
  std::vector<core::index1> map_to_reduced; // provides reduced index for each original character, e.g. map_to_reduced['A'] return index that encodes a group alanine belongs to

  ReduceSequenceAlphabet(const std::string &alphabet_name, const std::vector<std::string> &aa_groups);

  static bool is_loaded;
  static std::map<std::string,ReduceSequenceAlphabet_SP> reduced_sequence_alphabets;
  static void load_alphabets();
  static std::map<std::string,ReduceSequenceAlphabet_SP> create_map();
};


}
}
}

/*
 * @include ex_ReduceSequenceAlphabet.cc
 * @include ex_k_tuples.cc
 */
#endif
