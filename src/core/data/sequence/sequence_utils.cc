#include <string>
#include <cstdlib>
#include <regex>
#include <unordered_set>

#include <core/index.hh>
#include <core/data/sequence/sequence_utils.hh>
#include <core/algorithms/basic_algorithms.hh>

namespace core {
namespace data {
namespace sequence {

core::index4 extract_gid(const std::string &sequence_header) {

  static const std::regex find_gi_regex("gi\\|([0-9]+)\\|");

  std::smatch base_match;
  if (std::regex_search(sequence_header, base_match, find_gi_regex)) {
    if (base_match.size() >= 2) {
      return (core::index4) atoi(base_match[1].str().c_str());
    }
  }

  return 0;
}

bool is_secondary_structure(const std::string &sequence) {

  for (const char &c : sequence) {
    if ((c != 'E') && (c != 'H') && (c != 'C')) return false;
  }

  return true;
}

bool contains_aa_only(const std::string &sequence) {

  static const std::string accepted_letters = "ARNDCEQGHILKMFPSTWYVX";
  for (const char &c : sequence)
    if (accepted_letters.find(c) == std::string::npos) return false;
  return true;
}

core::index2 length_without_gaps(const std::string &sequence) {
  return std::count_if(sequence.begin(), sequence.end(), [](char c) { return (c != '-') && (c != '_'); });
}

void remove_gaps(std::string &sequence_string) {

  sequence_string.erase(std::remove(sequence_string.begin(), sequence_string.end(), '-'), sequence_string.end());
  sequence_string.erase(std::remove(sequence_string.begin(), sequence_string.end(), '_'), sequence_string.end());
}

std::string remove_gaps(const std::string &sequence_string) {

  std::string out(sequence_string);
  out.erase(std::remove(out.begin(), out.end(), '-'), out.end());
  out.erase(std::remove(out.begin(), out.end(), '_'), out.end());
  return out;
}

core::index2 count_aa(const std::string &seq) {
  static const std::string aa = "ARNDCEQGHILKMFPSTWYVX";
  return count_if(seq.begin(), seq.end(), [](const char c) { return aa.find(c) != std::string::npos; });
}

core::index2 count_aa(const Sequence &seq) {
  core::index2 cnt = 0;
  for (core::index2 i = 0; i < seq.length(); ++i)
    cnt += seq.get_monomer(i).type == 'P';
  return cnt;
}

core::index2 count_nt(const std::string &seq) {
  static const std::string nt = "actguACTGU";
  return count_if(seq.begin(), seq.end(), [](const char c) { return nt.find(c) != std::string::npos; });
}

core::index2 count_nt(const Sequence &seq) {
  core::index2 cnt = 0;
  for (core::index2 i = 0; i < seq.length(); ++i) cnt += seq.get_monomer(i).type == 'N';
  return cnt;
}

void count_residues_by_type(const std::string &sequence, std::vector<core::index4> &counts) {

  for (const char c : sequence) ++counts[core::chemical::Monomer::get(c).id];
}

/// --- internal helper function to repack a vector of counts into a map
std::map<core::chemical::Monomer, index4> internal_res_counters_to_map(std::vector<core::index4> &counts) {

  std::map<core::chemical::Monomer, index4> out_cnt;
  for (index2 i = 0;
       i < std::distance(core::chemical::Monomer::standard_cbegin(), core::chemical::Monomer::standard_cend()); ++i)
    if (counts[i] > 0) out_cnt[core::chemical::Monomer::get(i)] = counts[i];

  return out_cnt;
}

std::map<core::chemical::Monomer, index4> count_residues_by_type(const std::string &seq) {

  std::vector<index4> counts(30); // --- scratch memory
  count_residues_by_type(seq, counts); // --- count residues by type

  return internal_res_counters_to_map(counts);
}

std::map<core::chemical::Monomer, index4> count_residues_by_type(const Sequence &seq) {
  return count_residues_by_type(seq.sequence);
}


std::map<core::chemical::Monomer, index4> count_residues_by_type(const std::vector<Sequence> &seq) {

  std::vector<index4> counts(30); // --- scratch memory
  for (const Sequence &s:seq) count_residues_by_type(s.sequence, counts); // --- count residues by type

  return internal_res_counters_to_map(counts);
}

std::map<core::chemical::Monomer, index4> count_residues_by_type(const std::vector<Sequence_SP> &seq) {

  std::vector<index4> counts(30); // --- scratch memory
  for (const Sequence_SP &s:seq) count_residues_by_type(s->sequence, counts); // --- count residues by type

  return internal_res_counters_to_map(counts);
}

std::map<core::chemical::Monomer, index4> count_residues_by_type(const std::vector<std::string> &seq) {

  std::vector<index4> counts(30); // --- scratch memory
  for (const std::string &s:seq) count_residues_by_type(s, counts); // --- count residues by type

  return internal_res_counters_to_map(counts);

}

core::index1 count_aa_types(const std::string &seq) {

  std::unordered_set<char> s;
  for (const char c:seq) s.insert(c);

  return s.size();
}

core::index1 count_aa_types(const Sequence &seq) {

  return count_aa_types(seq.sequence);
}

std::string &fix_code1(std::string &sequence) {

  for (size_t i = 0; i < sequence.size(); ++i) {
    if (sequence[i] == 'B') sequence[i] = 'X';
    else if (sequence[i] == 'Z') sequence[i] = 'X';
  }

  return sequence;
}

struct is_same_ss{ bool operator()(char ci, char cj) { return ci==cj; } };

std::pair<index2, index2> count_helices_strands(const std::string & hec_secondary) {

  std::vector<std::pair<core::index2, core::index2>> islands;
  core::algorithms::consecutive_find(hec_secondary.begin(), hec_secondary.end(), is_same_ss{}, islands);
  std::pair<index2, index2> out {0,0};
  for(const std::pair<index2, index2> & i: islands) {
    if(hec_secondary[i.first]=='H') ++out.first;
    if(hec_secondary[i.first]=='E') ++out.second;
  }
  return out;
}

}
}
}
