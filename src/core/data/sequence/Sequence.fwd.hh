#ifndef CORE_DATA_SEQUENCE_Sequence_FWD_H
#define CORE_DATA_SEQUENCE_Sequence_FWD_H

#include <memory>

namespace core {
namespace data {
namespace sequence {

/// Forwarded declaration of the Sequence class
class Sequence;

/// Declaration of a shared pointer to the Sequence class
typedef std::shared_ptr<Sequence> Sequence_SP;

}
}
}

#endif
