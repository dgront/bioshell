#ifndef CORE_DATA_SEQUENCE_PirEntry_H
#define CORE_DATA_SEQUENCE_PirEntry_H

#include <string>
#include <vector>
#include <memory>

#include <core/index.hh>
#include <core/data/sequence/Sequence.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace core {
namespace data {
namespace sequence {

enum PirEntryType {
  STRUCTURE,
  STRUCTURE_X,
  STRUCTURE_N,
  SEQUENCE,
  SECONDARY,
  UNDEFINED
};

/// Represents a sequence annotated with secondary structure probabilities.
class PirEntry : public Sequence {
public:

  core::index2 PIR_LINE_WIDTH = 65535; // Virtually unlimited sequence length

  /** @brief Creates a PIR entry
   *
   * @param header - sequence header <strong>in the PIR format<strong>. All the relevant data will be extracted from it.
   *   Alternatively, the fields may be set later by using setters methods
   * @param seq - amino acid or nucleic sequence (required by Sequence base class constructor)
   * Example header (note that it consist of two lines!):
<pre>
>P1;1bgxt
structureX:1bgx:   1 :T: 285 :T:TAQ DNA polymerase:Thermus aquaticus: 2.30: 18.9
</pre>
   */
  PirEntry(const std::string &header, const std::string &seq) : Sequence(header, seq, 1), logs("PirEntry") {

    last_chain_ = 0;
    first_chain_ = 0;
    last_residue_ = seq.size();

    if ((header.length() > 0) && (header.find(">P1;") != 0))
      logs << utils::LogLevel::WARNING << "Incorrect PIR header:\n" << header << "\nPIR header must start with: >P1;\n";
    else {
      parse_pir_header(header);
      format_pir_header();
    }
  }

  /** @brief Creates a new entry based on a contiguous fragment of a source sequence
   * @param sequence - original sequence
   * @param start_pos - zero-related index indicates the first position of the new sequence
   * @param end_pos - zero-related index indicates the last position of the new sequence <strong>inclusive!</strong>
   */
  PirEntry(const PirEntry &seq, const core::index2 start_pos, const core::index2 end_pos) :
    PirEntry(seq.header(), seq.sequence.substr(start_pos-seq.first_pos(), end_pos-seq.first_pos()+1)) {
    first_residue_id(start_pos);
    last_residue_id(end_pos);
  }

  /// Bare virtual destructor to satisfy a compiler
  virtual ~PirEntry() {}

  /** @brief Defines the type of this entry.
   * Possible entry types are defined by PirEntryType enum.
   */
  void type(const PirEntryType entry_type)  {  type_ = entry_type; format_pir_header(); }

  /** @brief Checks the type of this entry
   */
  PirEntryType type(void) const {  return type_; }

  /** @brief Says whether this object bears information about secondary structure for this sequence.
   * @return always false
   */
  virtual bool has_ss() const { return false; }

  /** @brief Defines a new header string for this sequence.
   *
   * This method will also change the header defined in the base class <code>Sequence</code>.
   */
  virtual void header(const std::string &new_header) { parse_pir_header(new_header); format_pir_header(); }

  /** @brief Returns the header of this sequence.
   *
   * This method is not <code>virtual</code> so one can get a FASTA-style header by upper-casting
   * the object to <code>Sequence</code>.
   */
  const std::string &header() const { return header_; }

  /** @brief Returns the code of this sequence.
   * According to PIR format, this should be the PDB code of the deposit
   * If it has not been defined, an empty string will be returned)
   */
  const std::string & code() const { return code_; }

  /// Defines the new code for this sequence
  void code(const std::string & new_code) {  code_ = new_code; format_pir_header(); }

  /** @brief Returns the name of this sequence
   * If it has not been defined, an empty string will be returned)
   */
  const std::string & name() const { return name_; }

  /// Defines the new name of this sequence
  void name(const std::string & new_name) {  name_ = new_name; format_pir_header(); }

  /** @brief Returns the name of organism this sequence comes from
   * If it has not been defined, an empty string will be returned)
   */
  const std::string & organism() const { return organism_; }

  /// Defines the name of the organism this sequence comes from
  void organism(const std::string & organism_name) {  organism_ = organism_name; format_pir_header(); }

  /** @brief Sets the ID of the first residue of this sequence.
   *
   * This method is identical to <code>first_pos(const core::index2)</code> inherited from the base class. It has been
   * introduce for consistency with <code>last_residue_id()</code>
   */
  void first_residue_id(const core::index2 new_first_pos) { first_pos(new_first_pos); format_pir_header(); }

  /// Sets the ID of the chain the first residue of this sequence belongs to.
  void first_chain_id(const char new_first_chain) { first_chain_ = new_first_chain; format_pir_header(); }

  /// Sets the ID of the last residue of this sequence.
  void last_residue_id(const core::index2 new_last_pos) { last_residue_ = new_last_pos; format_pir_header(); }

  /// Sets the ID of the chain the last residue of this sequence belongs to.
  void last_chain_id(const char new_last_chain) { last_chain_ = new_last_chain; format_pir_header(); }

  /** @brief Returns ID of the first residue of this sequence.
   *
   * This method is identical to <code>first_pos()</code> inherited from the base class. It has been
   * introduce for consistency with <code>last_residue_id()</code>
   */
  core::index2 first_residue_id() const { return first_pos(); }

  /// Returns ID of the chain the first residue of this sequence belongs to.
  char first_chain_id() const { return first_chain_; }

  /// Returns ID of the last residue of this sequence.
  core::index2 last_residue_id() const { return last_residue_; }

  /// Returns ID of the chain the last residue of this sequence belongs to.
  char last_chain_id() const { return last_chain_; }

  /// Creates and returns a new PirEntry by removing gaps from this sequence
  virtual std::shared_ptr<Sequence> create_ungapped_sequence() const {

    std::string tmp_seq = sequence;
    tmp_seq.erase(std::remove(tmp_seq.begin(), tmp_seq.end(), '-'), tmp_seq.end());
    tmp_seq.erase(std::remove(tmp_seq.begin(), tmp_seq.end(), '-'), tmp_seq.end());
    return std::make_shared<PirEntry>(header_, tmp_seq);
  }

private:
  std::string code_;
  char first_chain_;
  core::index2 last_residue_;
  char last_chain_;
  std::string name_;
  std::string organism_;
  utils::Logger logs;
  PirEntryType type_ = PirEntryType::UNDEFINED; ///< The type of this entry
  std::string header_;

  void format_pir_header();

  bool parse_pir_header(const std::string &header);
};

///  Operator to print a PirEntry to a stream
std::ostream & operator<<(std::ostream & out, const PirEntry & entry);

/// PirEntry_SP is a short name for a shared pointer to PirEntry
typedef std::shared_ptr<PirEntry> PirEntry_SP;

}
}
}

#endif
