#include <map>
#include <iostream>
#include <iomanip>

#include <core/BioShellEnvironment.hh>
#include <core/data/sequence/ReduceSequenceAlphabet.hh>
#include <utils/io_utils.hh>

namespace core {
namespace data {
namespace sequence {

bool ReduceSequenceAlphabet::is_loaded = false;

std::map<std::string,ReduceSequenceAlphabet_SP>::const_iterator ReduceSequenceAlphabet::cbegin() {

  if(!is_loaded) load_alphabets();
  return reduced_sequence_alphabets.cbegin();
}

std::map<std::string,ReduceSequenceAlphabet_SP>::const_iterator ReduceSequenceAlphabet::cend() {
  if(!is_loaded) load_alphabets();
  return reduced_sequence_alphabets.cend();
}

const ReduceSequenceAlphabet_SP ReduceSequenceAlphabet::get_alphabet(const std::string &alphabet_name) {

  static utils::Logger st_logs("get_alphabet");

  if (!is_loaded) load_alphabets();
  if (reduced_sequence_alphabets.find(alphabet_name) == reduced_sequence_alphabets.end()) {
    st_logs << utils::LogLevel::SEVERE << "Unknown sequence alphabet: " << alphabet_name << "\n";
    return nullptr;
  }
  return reduced_sequence_alphabets[alphabet_name];
}

void ReduceSequenceAlphabet::load_alphabets() {

  std::ifstream listfile(core::BioShellEnvironment::from_file_or_db("alphabets/Alphabet_list.txt"));
  if (!listfile.is_open()) {
    static utils::Logger st_logs("get_alphabet");
    st_logs << utils::LogLevel::SEVERE << "Can't find "
            << core::BioShellEnvironment::from_file_or_db("alphabets/Alphabet_list.txt") << "\n";
  }
  std::string alphabet_name;
  while (getline(listfile, alphabet_name)) {
    utils::trim(alphabet_name);
    if(alphabet_name[0]=='#') continue;
//    std::cout << line << '\n';
    std::string alp = utils::load_text_file(core::BioShellEnvironment::from_file_or_db("alphabets/" + alphabet_name));
    std::vector<std::string> groups;
    utils::split(alp,groups,'\n');
    auto a = std::shared_ptr<ReduceSequenceAlphabet>( new ReduceSequenceAlphabet(alphabet_name, groups) );
    reduced_sequence_alphabets[alphabet_name] = a;

  }
  listfile.close();
  is_loaded = true;
}

std::map<std::string, ReduceSequenceAlphabet_SP> ReduceSequenceAlphabet::reduced_sequence_alphabets;

ReduceSequenceAlphabet::ReduceSequenceAlphabet(const std::string &alphabet_name, const std::vector<std::string> &aa_groups) :
  logs("ReduceSequenceAlphabet"), alphabet_size_(aa_groups.size()), map_to_original(aa_groups.size(), aa_groups[0][0]), map_to_reduced(256, 255) {

  map_to_original.resize(alphabet_size_);
  for (core::index2 j = 0; j < aa_groups.size(); ++j) {
    const std::string &group = aa_groups[j];
    for (const char c : group) {
      map_to_reduced[int(c)] = j;
      map_to_original[j] = c;
    }
  }

  logs << utils::LogLevel::FINE << "Created alphabet " << alphabet_name << " of " << aa_groups.size() << " letters\n";
}

std::ostream & operator<<(std::ostream &out, const ReduceSequenceAlphabet &mapping) {

  int row_cnt = 0;
  for (int i = 0; i < 255; ++i) {
    if (mapping.map_to_reduced[i] == 255) continue;
    out << char(i) << " => " << std::setw(2) << ((char) mapping.map_to_original[mapping.map_to_reduced[i]]);
    if ((++row_cnt) > 3) {
      row_cnt = 0;
      out << "\n";
    } else
      out << " | ";
  }
  return out;
}

}
}
}
