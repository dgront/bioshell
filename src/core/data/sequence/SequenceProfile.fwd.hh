#ifndef CORE_DATA_SEQUENCE_SequenceProfile_FWD_H
#define CORE_DATA_SEQUENCE_SequenceProfile_FWD_H

#include <memory>

namespace core {
namespace data {
namespace sequence {

/// Forwarded declaration of the SequenceProfile class
class SequenceProfile;

/// Declaration of a shared pointer to the SequenceProfile class
typedef std::shared_ptr<SequenceProfile> SequenceProfile_SP;

}
}
}

#endif
