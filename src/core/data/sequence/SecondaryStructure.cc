#include <core/data/sequence/SecondaryStructure.hh>

namespace core {
namespace data {
namespace sequence {

std::shared_ptr<Sequence> SecondaryStructure::create_ungapped_sequence() const {

  std::vector<float> h, e, c;
  std::string seq;
  core::index4 i = 0;
  for (core::index4 j = 0; j < sequence.size(); ++j) {
    if ((sequence[j] != '-') && (sequence[j] != '_')) {
      h.push_back(fraction_H(j));
      e.push_back(fraction_E(j));
      c.push_back(fraction_C(j));
      seq += sequence[j];
      ++i;
    }
  }
  std::shared_ptr<SecondaryStructure> s = std::make_shared<SecondaryStructure>(header(), seq, first_pos());
  for (index4 i = 0; i < h.size(); ++i) s->fractions(i, h[i], e[i], c[i]);

  return s;
}
}
}
}

