#include <string>
#include <vector>

#include <core/data/sequence/PirEntry.hh>

namespace core {
namespace data {
namespace sequence {

///  Operator to print a PirEntry to a stream
std::ostream & operator<<(std::ostream & out, const PirEntry & entry) {

  out << entry.header()<<"\n";
  core::index2 len = entry.sequence.length();
  core::index2 start = 0;
  do {
    core::index2 n = std::min(entry.PIR_LINE_WIDTH, (core::index2) (len - start));
    out << entry.sequence.substr(start, n);
    start += n;
    if(start<len) out << "\n";
  } while (start < len);
  if(entry.sequence.back()!='*') out << "*";
  return out;
}

static const std::vector<std::string> pir_entry_type_names({"structure","structureX","structureN","sequence","secondary","unknown"});


void PirEntry::format_pir_header() {

  std::string chain_from = (first_chain_ == 0) ? "" : std::string{first_chain_};
  std::string chain_to = (last_chain_ == 0) ? "" : std::string{last_chain_};
  bool is_strctr = ((type_ == PirEntryType::STRUCTURE) || (type_ == PirEntryType::STRUCTURE_N) ||
                    (type_ == PirEntryType::STRUCTURE_X));
  std::string first_res_idx = (is_strctr) ? " " : utils::to_string(first_residue_id());
  std::string last_res_idx =  (is_strctr) ? " " : utils::to_string(last_residue_id());
  std::string code = (code_.size() > 0) ? code_ : "query";
  header_ = utils::string_format(">P1;%s\n%s:%s:%4s:%s:%4s:%s%s:%s:::", code.c_str(), pir_entry_type_names[type_].c_str(),
    code_.c_str(), first_res_idx.c_str(), chain_from.c_str(), last_res_idx.c_str(), chain_to.c_str(), name_.c_str(), organism_.c_str());
  std::string h = "";
  if (code_.size() == 4) h += "pdb|" + code_ + " chain " + first_chain_id() + "|";
  else if (code_.size() == 5) h += "pdb|" + code_ + "|";
  if (name_.size() > 0) h += name_;
  Sequence::header(h);
}

bool PirEntry::parse_pir_header(const std::string &header) {

  std::vector<std::string> tokens;
  utils::split_into_strings(header, tokens, ':', false);
  if (tokens.size() < 6) return false;

  if (tokens.size() > 1) {
    if ((tokens[2].length() == 0) || (tokens[2] == "FIRST")) first_pos(0);
    else first_pos(utils::from_string<core::index2>(tokens[2]));
    if ((tokens[3].length() == 0) || (tokens[3][0] == ' ')) first_chain_id('A');
    else first_chain_id(tokens[3][0]);
    if (first_chain_id() == ' ') first_chain_id('A');
    if ((tokens[4].length() == 0) || (tokens[4] == "LAST")) last_residue_id(first_pos() + sequence.length() - 1);
    else last_residue_id(utils::from_string<core::index2>(tokens[4]));
    if ((tokens[5].length() == 0) || (tokens[5][0] == ' ')) last_chain_id('A');
    else last_chain_id(tokens[5][0]);
    if (tokens.size() > 6) name(tokens[6]);
    if (tokens.size() > 7) organism(tokens[6]);
  }
  std::string first{tokens[0]};
  tokens.clear();
  utils::split(first, tokens, '\n');
  utils::to_lower(tokens.back());
  type_ = PirEntryType::UNDEFINED;
  if (tokens.back() == "structure") type_ = PirEntryType::STRUCTURE;
  if (tokens.back() == "structurex") type_ = PirEntryType::STRUCTURE_X;
  if (tokens.back() == "structuren") type_ = PirEntryType::STRUCTURE_N;
  if (tokens.back() == "secondary") type_ = PirEntryType::SECONDARY;
  if (tokens.back() == "sequence") type_ = PirEntryType::SEQUENCE;
  if (type_ == PirEntryType::UNDEFINED)
    logs << "PIR entry type undefined after parsing header:\n" << header << "\n";
  utils::split(tokens[0], tokens, ';');
  if (tokens.back() != ">P1") code_ = tokens.back();

  return true;
}

}
}
}

