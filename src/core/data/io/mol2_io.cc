#include <iostream>
#include <regex>

#include <core/data/io/mol2_io.hh>
#include <utils/io_utils.hh>

namespace core {
namespace data {
namespace io {

const std::string MOLECULE_HEADER = "@<TRIPOS>MOLECULE";
const std::string ATOM_HEADER = "@<TRIPOS>ATOM";
const std::string BOND_HEADER = "@<TRIPOS>BOND";
const std::string SUBSTR_HEADER = "@<TRIPOS>SUBSTRUCTURE";

utils::Logger mol2_io_logs("mol2_io");

core::chemical::PdbMolecule_SP read_mol2(const std::string &file_name) {

  std::ifstream ifs;
  utils::in_stream(file_name, ifs);
  return read_mol2(ifs);
}

core::chemical::PdbMolecule_SP read_mol2(std::istream &input) {

  std::string name;
  core::index2 n_atoms, n_bonds;
  std::string mol2_text = utils::load_text_file(input);
  std::regex find_molecule(MOLECULE_HEADER + "(.+?)@");
  std::regex find_bonds(BOND_HEADER + "(.+?)@");
  std::regex find_atoms(ATOM_HEADER + "(.+?)@");
  std::smatch match;

  std::replace( mol2_text.begin(), mol2_text.end(), '\n', '%');
  if(std::regex_search(mol2_text, match, find_molecule)) {
    std::string str = match.str(1);
    std::replace( str.begin(), str.end(), '%', '\n');
    std::stringstream inp(str);
    inp >> name >> n_atoms >> n_bonds;
    mol2_io_logs << utils::LogLevel::FINE << "molecule " << name << " found with " << n_atoms << " atoms\n";
  } else
    return nullptr;

  core::chemical::PdbMolecule_SP mol = std::make_shared<core::chemical::PdbMolecule>();
  std::vector<std::string> lines;
  std::vector<std::string> tokens;
  if(std::regex_search(mol2_text, match, find_atoms)) {
    std::string str = match.str(1);
    utils::split_into_strings(str,lines,'%',true);
    for(const std::string &line:lines) {
      tokens.clear();
      utils::split_into_strings(line, tokens);
      index2 id = utils::from_string<index2>(tokens[0]);
      double x = utils::from_string<double>(tokens[2]);
      double y = utils::from_string<double>(tokens[3]);
      double z = utils::from_string<double>(tokens[4]);
      core::data::structural::PdbAtom_SP atom =
          std::make_shared<core::data::structural::PdbAtom>(id, core::data::structural::format_pdb_atom_name(tokens[1]), x, y, z);
      mol->add_atom(atom);
    }
    if(mol->count_atoms()!=n_atoms)
      mol2_io_logs << utils::LogLevel::WARNING << n_atoms << " expected, " << mol->count_atoms() << " atoms created\n";
  }
  if(std::regex_search(mol2_text, match, find_bonds)) {
    std::string str = match.str(1);
    lines.clear();
    utils::split_into_strings(str,lines,'%',true);
    for(const std::string &line:lines) {
      tokens.clear();
      utils::split_into_strings(line, tokens);
      index2 id1 = utils::from_string<index2>(tokens[1]);
      index2 id2 = utils::from_string<index2>(tokens[2]);
      mol->bind_atoms(id1-1,id2-1,core::chemical::bond_type_from_code(tokens[3]));
    }
    if(mol->count_atoms()!=n_atoms)
      mol2_io_logs << utils::LogLevel::WARNING << n_bonds << " expected, " << mol->count_bonds() << " bonds created\n";
  }
  mol->molecule_name = name;

  return mol;
}

typedef std::pair<std::pair<index4, index4>, core::chemical::BondType> BondDef;

struct SortBonds {
  bool operator()(const BondDef & lhs, const BondDef & rhs) {
    if (lhs.first.first == rhs.first.first)
      return lhs.first.second < rhs.first.second;
    else
      return lhs.first.first < rhs.first.first;
  }
};

void write_mol2(const core::chemical::PdbMolecule_SP molecule, const std::string &file_name) {

  std::shared_ptr<std::ostream> out = utils::out_stream(file_name);
  write_mol2(molecule, *out);
  out->flush();
}

void write_mol2(const core::chemical::PdbMolecule_SP molecule, std::ostream & out) {

  out << MOLECULE_HEADER<<"\n";
  out << molecule->molecule_name<<"\n";
  out << molecule->count_atoms()<<" "<<molecule->count_bonds()<<"\n";
  out << ATOM_HEADER<<"\n";
  int substr_id = 1; // --- All atoms belong to the same substructure; should be fixed in the future
  for(auto ai=molecule->cbegin_atom();ai!=molecule->cend_atom();++ai) {
    const core::data::structural::PdbAtom a = **ai;
    std::string atom_type = core::chemical::AtomicElement::periodic_table[a.element_index()].symbol;
    out << utils::string_format("%10d%10s%10.3f%10.3f%10.3f %5s%5d %12s 0.000\n",
        a.id(), a.atom_name().c_str(), a.x, a.y, a.z, atom_type.c_str(), substr_id, molecule->molecule_name.c_str());
  }
  out << BOND_HEADER<<"\n";
  core::index4 ibond = 0;
  std::vector<BondDef> bonds;
  for (auto ai = molecule->cbegin_bonds(); ai != molecule->cend_bonds(); ++ai)
    bonds.push_back(*ai);
  std::sort(bonds.begin(), bonds.end(),SortBonds());
  for (const BondDef &b:bonds) {
    out << utils::string_format("%10d%10d%10d%10s\n",
                                ++ibond, b.first.first + 1, b.first.second + 1,
                                core::chemical::mol2_bond_code_from_type(b.second).c_str());
  }
}

}
}
}