#include <string>
#include <utility>
#include <vector>
#include <algorithm>

#include <core/index.hh>
#include <core/data/io/hssp_io.hh>
#include <core/data/sequence/Sequence.hh>

#include <utils/Logger.hh>
#include <utils/string_utils.hh>
#include <utils/io_utils.hh>

namespace core {
namespace data {
namespace io {

using core::data::sequence::Sequence;
using core::data::sequence::Sequence_SP;

utils::Logger hssp_io_logs("hssp_io");

std::vector<std::shared_ptr<Sequence>> & read_hssp_file(const std::string & file_name,
      std::vector<std::shared_ptr<Sequence>> & sink, bool to_uppercase, bool fix_aa) {

  std::ifstream inp;
  utils::in_stream(file_name, inp);
  return read_hssp_file(inp, sink, to_uppercase, fix_aa);
}

std::vector<std::shared_ptr<Sequence>> & read_hssp_file(std::istream & input,
      std::vector<std::shared_ptr<Sequence>> & sink, bool to_uppercase, bool fix_aa) {

  std::vector<std::pair<std::string,std::string>> headers;  // --- stores data from sequence headers
  std::vector<std::string> sequences;                       // --- stores the sequences themselves
  std::vector<index2> first_pos;                            // --- stores the index of the first residue for each sequence
  index2 seq_length = 0;
  index2 n_seq = 0;
  std::string junk;

  std::vector<std::string> lines;                           // --- the file split into lines
  std::vector<std::string> tokens;                          // --- tokens of a single line
  utils::load_text_lines(input, lines);               // --- Load the whole file
  auto it_line = lines.begin();
  while (it_line->rfind("## PROTEINS") != 0) {
    if (it_line->rfind("NALIGN") == 0) {
      std::istringstream iss(*it_line);
      iss >> junk>> n_seq;
    }
    if (it_line->rfind("SEQLENGTH") == 0) {
      std::istringstream iss(*it_line);
      iss >> junk>> seq_length;
    }
    ++it_line;
  }
  ++it_line;                                                // --- skip section separator (## PROTEINS line)
  ++it_line;                                                // --- skip table header line
  std::string id, header;
  while(it_line->rfind("## ALIGNMENTS") !=0 ) {          // --- Read protein headers until reach alignment section
    tokens.clear();
    utils::split_into_strings(*it_line, tokens);
    hssp_io_logs << utils::LogLevel::FINE << "new sequence detected: " << tokens[2] << " : " << tokens[14] << "\n";
    headers.emplace_back(tokens[2],it_line->substr(91));
    first_pos.push_back(utils::from_string<index2>(tokens[7]));
    ++it_line;
  }
  ++it_line;                                                // --- skip section separator (## ALIGNMENTS line)
  ++it_line;                                                // --- skip table header line
  sequences.reserve(headers.size());
  for (int i = 0; i < headers.size(); ++i)                  // --- create empty strings for sequences
    sequences.emplace_back("");
  index2 i_sect = 0;
  while ((it_line->rfind("## SEQUENCE") != 0) && (it_line != lines.end())) {
    while ((it_line->rfind("## ALIGNMENTS") != 0) && (it_line->rfind("## SEQUENCE") != 0)) {
      for (index2 i = 51; i < 121; ++i) {
        index2 which_seq = i_sect * 70 + i - 51;            // --- there are 70 sequences in each section
        if (which_seq >= n_seq) break;
        char c = (*it_line)[i];
        if ((c == '.') || (c == ' ')) c = '-';
        if(to_uppercase) c = toupper(c);
        sequences[which_seq] += c;
      }
      ++it_line;
    }
    if (it_line->rfind("## ALIGNMENTS") == 0) {
      it_line += 2;
      ++i_sect;
    }
    if (it_line->rfind("## SEQUENCE") == 0) break;
  }

  // --- Insertions are parsed but actually not returned ...
  while (it_line->rfind("## INSERTION") != 0) {           // --- scroll down to INSERTION section
    ++it_line;
    // --- no insertions, just the end of that file:
    if ((it_line == lines.end() || it_line->rfind("//") == 0)) break;
  }
  if ((it_line != lines.end()) && (it_line->rfind("## INSERTION") == 0)) {
    it_line+=2;
    index2  ali_no,  i_pos,  j_pos,   len;
    std::string sequence_fragment;
    while ((it_line->rfind("//") != 0) && (it_line != lines.end())) {
      std::istringstream iss(*it_line);
      iss >> ali_no >> i_pos >> j_pos >> len >> sequence_fragment;
      ++it_line;
    }
  }

  for (index2 i_seq = 0; i_seq < n_seq; ++i_seq) {
    if(fix_aa) core::data::sequence::fix_code1(sequences[i_seq]);
    Sequence_SP seq = std::make_shared<Sequence>(headers[i_seq].second, sequences[i_seq], first_pos[i_seq]);
    seq->id(headers[i_seq].first);
    sink.push_back(seq);
  }

  return sink;
}

}
}
}