#include <core/data/io/mmCif.hh>
#include <core/data/io/Cif.hh>

#include <core/data/sequence/sequence_utils.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>
#include <core/chemical/AtomicElement.hh>

namespace core {
namespace data {
namespace io {

using namespace core::data::structural;

mmCif::mmCif(const std::string & fname, const bool first_model_only):reader_(fname),fname_(fname) {}

std::string mmCif::pdb_code() const {return reader_.begin()->first.substr(5,4);}

core::index2 mmCif::count_models() const {
    const std::shared_ptr<CifBlock> block = reader_.begin()->second;
    for (auto sec = block->begin(); sec != block->end(); ++sec) {
        if ((*sec)->is_loop() && (*sec)->loop_labels()[0].compare(0, 11, "_atom_site.") == 0) {
            auto vec = (*sec)->loop().at("_atom_site.pdbx_PDB_model_num");
            return utils::from_string<index2>(vec[vec.size() - 1]);
        }
    }
}

void mmCif::create_structures(std::vector<core::data::structural::Structure_SP> & structures){
    for(core::index4 i=0;i<count_models();++i) structures.push_back( create_structure(i) );
}


core::data::structural::Structure_SP mmCif::create_structure(const core::index2 which_model) {
    const std::shared_ptr<CifBlock> block = reader_.begin()->second;

    Structure_SP strc = std::make_shared<Structure>(pdb_code());
    std::string current_chain_nr="";
    std::string current_res_nr="";

    for (auto sec = block->begin(); sec != block->end(); ++sec) {
        if ((*sec)->is_loop() && (*sec)->loop_labels()[0].compare(0, 11, "_atom_site.") == 0) {
            auto atom_type_col = (*sec)->loop_labels()[0];
            auto id_col = (*sec)->loop_labels()[1];
            auto element_col = (*sec)->loop_labels()[2];
            auto name_col = (*sec)->loop_labels()[3];
            auto res_type_col = (*sec)->loop_labels()[5];
            auto chain_nr_col = (*sec)->loop_labels()[7];
            auto owner_nr_col = (*sec)->loop_labels()[8];
            auto x_col = (*sec)->loop_labels()[10];
            auto y_col = (*sec)->loop_labels()[11];
            auto z_col = (*sec)->loop_labels()[12];
            auto occupancy_col = (*sec)->loop_labels()[13];
            auto b_factor_col = (*sec)->loop_labels()[14];
            auto res_id_col = (*sec)->loop_labels()[16];
            auto chain_id_col = (*sec)->loop_labels()[18];

            for (auto i = 0; i < (*sec)->loop().at((*sec)->loop_labels()[0]).size(); ++i) {
                std::string res_id = (*sec)->loop().at(res_id_col)[i];
                std::string chain_id = (*sec)->loop().at(chain_id_col)[i];
                std::string res_type = (*sec)->loop().at(res_type_col)[i];
                if (res_id!=current_res_nr) {
                    if (chain_id != current_chain_nr) {
                        current_chain_nr = chain_id;
                        if (!strc->has_chain(chain_id)) {
                            Chain_SP chain = std::make_shared<Chain>(chain_id);
                            strc->push_back(chain);
                        }
                    }
                    current_res_nr=res_id;
                    Residue_SP res = std::make_shared<Residue>(utils::from_string<index4>(res_id), res_type);
                    strc->get_chain(chain_id)->push_back(res);
                }

                if (utils::from_string<index4>((*sec)->loop().at((*sec)->loop_labels()[20])[i])!=which_model+1) break;
                    core::index4 id = utils::from_string<index4>((*sec)->loop().at(id_col)[i]);
                    std::string element = (*sec)->loop().at(element_col)[i];
                    std::string name = (*sec)->loop().at(name_col)[i];
                    core::index4 owner = utils::from_string<core::index4>((*sec)->loop().at(owner_nr_col)[i]);
                    core::index4 chain_nr = utils::from_string<core::index4>((*sec)->loop().at(chain_nr_col)[i]);
                    double x = utils::from_string<double>((*sec)->loop().at(x_col)[i]);
                    double y = utils::from_string<double>((*sec)->loop().at(y_col)[i]);
                    double z = utils::from_string<double>((*sec)->loop().at(z_col)[i]);
                    double occupancy = utils::from_string<double>((*sec)->loop().at(occupancy_col)[i]);
                    double b_factor = utils::from_string<double>((*sec)->loop().at(b_factor_col)[i]);
                    PdbAtom_SP atom = std::make_shared<PdbAtom>(id, name, x, y, z,
                                                                occupancy,
                                                                b_factor,
                                                                core::chemical::AtomicElement::by_symbol(element).z);
                if ((*sec)->loop().at((*sec)->loop_labels()[0])[i]=="HETATM") atom->is_heteroatom(true);
                    strc->get_chain(chain_id)->get_residue(utils::from_string<index4>(res_id))->push_back(atom);
                }
            }
        }

    return strc;
}

}
}
}

