#include <iostream>
#include <iomanip>
#include <string>

#include <core/alignment/PairwiseAlignment.hh>
#include <core/alignment/PairwiseSequenceAlignment.hh>
#include <core/data/sequence/Sequence.hh>
#include <core/data/sequence/sequence_utils.hh>
#include <core/data/sequence/SecondaryStructure.hh>
#include <core/data/io/alignment_io.hh>
#include <core/data/io/DsspData.hh>

#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace core {
namespace data {
namespace io {

using namespace core::data::sequence;

const std::string AlignmentOutputLines::create_sequence_marks(const std::string & seq1, const std::string & seq2) {

  std::string sequence_marks = "";
  for (core::index2 i = 0; i < seq1.size(); ++i) {
    if (seq1[i] == seq2[i] && seq2[i] != '_' && seq2[i] != '-') sequence_marks += '*';
    else sequence_marks += ' ';
  }
  return sequence_marks;
}

std::string AlignmentOutputLines::format_name(const std::string & sequence_name) {

  std::string q_name = sequence_name.substr(0,NAME_MAX_LENGTH);
  q_name += std::string(NAME_MAX_LENGTH+1 - q_name.size(), ' ');

  return q_name;
}

std::vector<AlignmentOutputLines> & AlignmentOutputLines::create_blocks(const Sequence & query_sequence, const core::alignment::PairwiseAlignment & alignment,
  const Sequence & templt_sequence, const index2 width, std::vector<AlignmentOutputLines> & storage) {

  std::string q_name = format_name(query_sequence.header());
  std::string t_name = format_name(templt_sequence.header());
  std::string path = alignment.to_path();

  // --- figure out the number of output blocks and create the output data structure
  core::index1 n_blocks = path.size() / width + 1;
  storage.clear();
  storage.resize(n_blocks);

  std::string aligned_q = alignment.get_aligned_query(query_sequence.sequence);
  std::string aligned_t = alignment.get_aligned_template(templt_sequence.sequence);

  std::vector<std::string> marks;
  utils::split(create_sequence_marks(aligned_q,aligned_t), marks, width);

  // --- store query sequence data in the structures
  std::vector<std::string> tokens;
  utils::split(aligned_q, tokens, width);
  int start = query_sequence.first_pos() + alignment.first_query_position();
  for (core::index2 i = 0; i < tokens.size(); ++i) {
    storage[i].query_name = q_name;
    storage[i].update_sequence<QuerySeq>(tokens[i], start);
    storage[i].sequence_marks = marks[i];
    start = storage[i].query_end + 1;
  }

  // --- store template sequence data in the structures
  tokens.clear();
  utils::split(aligned_t, tokens, width);
  start = templt_sequence.first_pos() + alignment.first_template_position();
  for (core::index2 i = 0; i < tokens.size(); ++i) {
    storage[i].tmplt_name = t_name;
    storage[i].update_sequence<TmpltSeq>(tokens[i], start);
    start = storage[i].tmplt_end + 1;
  }

  return storage;
}

std::vector<AlignmentOutputLines> & AlignmentOutputLines::create_blocks(const SecondaryStructure & query_sequence, const core::alignment::PairwiseAlignment & alignment,
  const SecondaryStructure & templt_sequence, const index2 width, std::vector<AlignmentOutputLines> & storage) {

  std::string q_name = format_name(query_sequence.header());
  std::string t_name = format_name(templt_sequence.header());
  std::string path = alignment.to_path();

  // --- figure out the number of output blocks and create the output data structure
  core::index1 n_blocks = path.size() / width + 1;
  storage.clear();
  storage.resize(n_blocks);

  std::string aligned_q = alignment.get_aligned_query(query_sequence.sequence);
  std::string aligned_t = alignment.get_aligned_template(templt_sequence.sequence);

  std::vector<std::string> marks;
  utils::split(create_sequence_marks(aligned_q,aligned_t), marks, width);

  // --- store query sequence data in the structures
  std::vector<std::string> tokens;
  utils::split(aligned_q, tokens, width);
  int start = query_sequence.first_pos();
  for (core::index2 i = 0; i < tokens.size(); ++i) {
    storage[i].query_name = q_name;
    storage[i].update_sequence<QuerySeq>(tokens[i], start);
    storage[i].sequence_marks = marks[i];
    start = storage[i].query_end + 1;
  }

  // --- store template sequence data in the structures
  tokens.clear();
  utils::split(aligned_t, tokens, width);
  start = templt_sequence.first_pos();
  for (core::index2 i = 0; i < tokens.size(); ++i) {
    storage[i].tmplt_name = t_name;
    storage[i].update_sequence<TmpltSeq>(tokens[i], start);
    start = storage[i].tmplt_end + 1;
  }

  // --- store query and template secondary structure data in the structures
  tokens.clear();
  utils::split(alignment.get_aligned_query(query_sequence.str()), tokens, width);
  for (core::index2 i = 0; i < tokens.size(); ++i)
    storage[i].update_sequence<QuerySS>(tokens[i], start);

  tokens.clear();
  utils::split(alignment.get_aligned_template(templt_sequence.str()), tokens, width);
  for (core::index2 i = 0; i < tokens.size(); ++i)
    storage[i].update_sequence<TmpltSS>(tokens[i], start);

  return storage;
}


void write_edinburgh(const Sequence & query_sequence, const core::alignment::PairwiseAlignment & alignment,
    const Sequence & templt_sequence, std::ostream & out, const index2 width) {

  std::vector<AlignmentOutputLines> blocks = AlignmentOutputLines::create_blocks(query_sequence,alignment,templt_sequence,width);

  for(const AlignmentOutputLines & b: blocks) {
    out << b.empty_string() << b.sequence_marks << "\n";
    out << b.query_name << std::setw(3) << b.query_start << ' ' << b.query_sequence << std::setw(4) << b.query_end << "\n";
    out << b.tmplt_name << std::setw(3) << b.tmplt_start << ' ' << b.tmplt_sequence << std::setw(4) << b.tmplt_end << "\n\n";
  }
}

void write_edinburgh(const SecondaryStructure & query_ss, const core::alignment::PairwiseAlignment & alignment,
    const SecondaryStructure & templt_ss, std::ostream & out, const index2 width) {

  std::vector<AlignmentOutputLines> blocks = AlignmentOutputLines::create_blocks(query_ss,alignment,templt_ss,width);

  for(const AlignmentOutputLines & b: blocks) {
    std::string s(b.sequence_marks);
    out << b.empty_string() << utils::replace_substring(s," ",".") << "\n";
    out << b.query_name << std::setw(3) << b.query_start << ' ' << b.query_sequence << std::setw(4) << b.query_end << "\n";
    out << b.empty_string()   << b.query_secondary << "\n";
    out << b.empty_string()   << b.tmplt_secondary << "\n";
    out << b.tmplt_name << std::setw(3) << b.tmplt_start << ' ' << b.tmplt_sequence << std::setw(4) << b.tmplt_end << "\n\n";
  }
}

core::alignment::PairwiseSequenceAlignment_SP read_edinburgh(const std::string & fname) {

  std::ifstream in(fname);
  return read_edinburgh(in);
}

bool is_ss(const std::string & s) {

  for(const char ch : s) if ((ch != 'H') && (ch != 'E') && (ch != 'C') && (ch != '-') && (ch != '_')) return false;
  return true;
}

core::alignment::PairwiseSequenceAlignment_SP read_edinburgh(std::istream & buffer) {

  static utils::Logger log("read_edinburgh");
  std::vector<std::string> lines;
  std::string line;
  long begin = buffer.tellg();
  while (std::getline(buffer, line)) {
    if (line.length() < 5) continue;
    if (line[0] == '#') {
      if (lines.size() == 0) continue; // this is a comment - header for the alignment we gonna read
      else {// this comment is for the next alignment, so we should stop here
        buffer.seekg(begin);
        break;
      }
    }
    lines.push_back(line);
    begin = buffer.tellg();
  }
  if (lines.size() < 3) {
    log << utils::LogLevel::FILE << "Found less than three lines in the input alignment (format empty?)\n";
    return nullptr;
  }

  int q_first = -9999, t_first = -9999;
  std::string q_name, t_name;
  std::string q_seq, t_seq;
  std::vector<std::string> tmp;
  if (is_ss(lines[3])) { // the third line in the block is just a single ungapped string => we got SS, so 5 lines per block
    if (lines.size() % 5 != 0) {
      log << utils::LogLevel::SEVERE << "Found secondary structure but the number of lines is not a multiplicity of 5 (format incorrect!)\n";
      return nullptr;
    }
    std::string q_ss, t_ss;
    for (size_t i = 0; i < lines.size(); i+=5) {
      tmp.clear();
      utils::split(utils::trim(lines[i + 1]), tmp, ' ');
      if (tmp.size() < 2) {    // --- wrong sequence format !!
        log << utils::LogLevel::SEVERE << "incorrect template sequence line:\n\t" << lines[i + 1] << "\n";
        return nullptr;
      }
      q_name = tmp[0];
      if (tmp.size() == 2) q_seq += tmp[1]; // --- just the sequence name and the sequence itself
      else {
        q_seq += tmp[2];
        if (q_first == -9999) q_first = utils::from_string<int>(tmp[1]);
      }
      q_ss += utils::trim(lines[i + 2], " ");
      t_ss += utils::trim(lines[i + 3], " ");

      tmp.clear();
      utils::split(utils::trim(lines[i + 4]), tmp, ' ');
      if (tmp.size() < 2) {    // --- wrong sequence format !!
        log << utils::LogLevel::SEVERE << "incorrect template sequence line:\n\t" << lines[i + 2] << "\n";
        return nullptr;
      }
      t_name = tmp[0];
      if (tmp.size() == 2) t_seq += tmp[1]; // --- just the sequence name and the sequence itself
      else {
        t_seq += tmp[2];
        if (t_first == -9999) t_first = utils::from_string<int>(tmp[1]);
      }
    }
    core::alignment::PairwiseAlignment_SP ali = std::make_shared<core::alignment::PairwiseAlignment>(q_seq, q_first,
        t_seq, t_first, 0);
    core::data::sequence::remove_gaps(q_ss);
    core::data::sequence::remove_gaps(t_ss);
    core::data::sequence::remove_gaps(q_seq);
    core::data::sequence::remove_gaps(t_seq);
    SecondaryStructure_SP q_s = std::make_shared<SecondaryStructure>(q_name, q_seq, (q_first == -9999) ? 0 : q_first - 1, q_ss);
    SecondaryStructure_SP t_s = std::make_shared<SecondaryStructure>(t_name, t_seq, (t_first == -9999) ? 0 : t_first - 1, t_ss);
    return std::make_shared<core::alignment::PairwiseSequenceAlignment>(ali, q_s, t_s);
  } else {
    // no SS, so 3 lines per block
    if (lines.size() % 3 != 0) {
      log << utils::LogLevel::SEVERE << "The number of lines is not a multiplicity of 3 (format incorrect!)";
      return nullptr;
    }
    for (size_t i = 0; i < lines.size(); i+=3) {
      tmp.clear();
      utils::split(utils::trim(lines[i + 1]), tmp, ' ');
      if (tmp.size() < 2) {    // --- wrong sequence format !!
        log << utils::LogLevel::SEVERE << "incorrect template sequence line:\n\t" << lines[i + 1] << "\n";
        return nullptr;
      }
      q_name = tmp[0];
      if (tmp.size() == 2) q_seq += tmp[1]; // --- just the sequence name and the sequence itself
      else {
        q_seq += tmp[2];
        if (q_first == -9999) q_first = utils::from_string<int>(tmp[1]);
      }

      tmp.clear();
      utils::split(utils::trim(lines[i + 2]), tmp, ' ');
      if (tmp.size() < 2) {    // --- wrong sequence format !!
        log << utils::LogLevel::SEVERE << "incorrect template sequence line:\n\t" << lines[i + 2] << "\n";
        return nullptr;
      }
      t_name = tmp[0];
      if (tmp.size() == 2) t_seq += tmp[1]; // --- just the sequence name and the sequence itself
      else {
        t_seq += tmp[2];
        if (t_first == -9999) t_first = utils::from_string<int>(tmp[1]);
      }
    }
    core::alignment::PairwiseAlignment_SP ali = std::make_shared<core::alignment::PairwiseAlignment>(q_seq, q_first,
        t_seq, t_first, 0);
    core::data::sequence::remove_gaps(q_seq);
    core::data::sequence::remove_gaps(t_seq);
    Sequence_SP q_s = std::make_shared<Sequence>(q_name, q_seq, (q_first == -9999) ? 0 : q_first - 1);
    Sequence_SP t_s = std::make_shared<Sequence>(t_name, t_seq, (t_first == -9999) ? 0 : t_first - 1);

    return std::make_shared<core::alignment::PairwiseSequenceAlignment>(ali, q_s, t_s);;
  }
}

std::vector<core::alignment::PairwiseSequenceAlignment_SP> & read_edinburgh(const std::string & file_name,std::vector<core::alignment::PairwiseSequenceAlignment_SP> & container) {

  std::ifstream in(file_name);
  read_edinburgh(in, container);

  return container;
}

std::vector<core::alignment::PairwiseSequenceAlignment_SP> & read_edinburgh(std::istream & buffer,std::vector<core::alignment::PairwiseSequenceAlignment_SP> & container) {

  core::alignment::PairwiseSequenceAlignment_SP a = read_edinburgh(buffer);
  while (a != nullptr) {
    container.push_back(a);
    a = read_edinburgh(buffer);
  }

  return container;
}

std::pair<std::string, char> extract_pdb_chain(const std::string &hhpred_header) {

  std::vector<std::string> tokens;
  if (hhpred_header[0] == '>')
    utils::split(hhpred_header.substr(1), tokens);
  else
    utils::split(hhpred_header, tokens);

  if (tokens[0].size() == 4)
    return std::make_pair(tokens[0], tokens[1][0]);
  if (tokens[0].size() == 5)
    return std::make_pair(tokens[0].substr(0,4), tokens[0][4]);
  if (tokens[0].size() == 6)
    return std::make_pair(tokens[0].substr(0,4), tokens[0][5]);

  return std::make_pair("",'A');
}


core::alignment::PairwiseSequenceAlignment_SP read_hhpred(std::istream & buffer) {

  using namespace core::alignment; // for  AlignmentAnnotation, PairwiseSequenceAlignment

  std::string q_str, t_str, t_seq, q_seq, header, scores;
  std::string line = " ";
  // --- scroll down until header
  while (std::getline(buffer, line))
    if (line[0] == '>') break;
  header = line.substr(1);
  std::getline(buffer, line);
  scores = line;

  // --- Load the actual alignment
  std::vector<std::string> lines;
  while ((std::getline(buffer, line))) {
    if(line.size() > 10) lines.push_back(line);
    if (buffer.peek() == '>') break;
  }

  // --- Wrong number of lines: must be multiplicity of 8!
  if ((lines.size() == 0) || (lines.size() % 8 != 0)) return nullptr;

  for (size_t il = 0; il < lines.size(); il += 8) {
    q_str += lines[il].substr(22, 80);
    q_seq += lines[il + 1].substr(22, 80);
    t_seq += lines[il + 5].substr(22, 80);
    t_str += lines[il + 6].substr(22, 80);
  }
  q_seq = q_seq.substr(0, q_str.size());
  t_seq = t_seq.substr(0, t_str.size());

  std::string frag = lines[1].substr(17,5);
  index2 first_q_pos = utils::to_double(utils::trim(frag).c_str());
  frag = lines[4].substr(17,5);
  index2 first_t_pos = utils::to_double(utils::trim(frag).c_str());

  for (size_t ic = 0; ic < q_str.size(); ++ic) q_str[ic] = std::toupper(q_str[ic]);
  for (size_t ic = 0; ic < t_str.size(); ++ic)
    if (t_str[ic] != '-')
      t_str[ic] = core::data::io::DsspData::dssp_to_hec(std::toupper(t_str[ic]));

  core::data::sequence::SecondaryStructure_SP q_aligned =
      std::make_shared<core::data::sequence::SecondaryStructure>("query", q_seq, 1, q_str);
  core::data::sequence::SecondaryStructure_SP t_aligned =
      std::make_shared<core::data::sequence::SecondaryStructure>(header, t_seq, 1, t_str);
  core::alignment::PairwiseAlignment_SP ali =
      std::make_shared<core::alignment::PairwiseAlignment>(*q_aligned, *t_aligned,0.0);

  core::data::sequence::SecondaryStructure_SP qq = std::static_pointer_cast<core::data::sequence::SecondaryStructure>(t_aligned->create_ungapped_sequence());

  PairwiseSequenceAlignment_SP alignment =
      std::make_shared<PairwiseSequenceAlignment>(ali, q_aligned->create_ungapped_sequence(), t_aligned->create_ungapped_sequence());

  // --- Parse header and try to extract as much data from it as possible
  const auto pdb_chain = extract_pdb_chain(header);
  if (pdb_chain.first.size() == 4) {
    alignment->alignment_data()[AlignmentAnnotation::PDB_ID] = pdb_chain.first;
    alignment->alignment_data()[AlignmentAnnotation::CHAIN_ID] = std::string{pdb_chain.second};
  }
  alignment->alignment_data()[AlignmentAnnotation::FIRST_TMPLT_POS] = std::to_string(first_t_pos);
  alignment->alignment_data()[AlignmentAnnotation::FIRST_QUERY_POS] = std::to_string(first_q_pos);

  return alignment;
}

std::vector<core::alignment::PairwiseSequenceAlignment_SP> read_hhpred(const std::string & file_name) {

  std::vector<core::alignment::PairwiseSequenceAlignment_SP> alignments;
  std::ifstream in(file_name);
  read_hhpred(in, alignments);

  return alignments;
}

std::vector<core::alignment::PairwiseSequenceAlignment_SP> & read_hhpred(std::istream & buffer,
          std::vector<core::alignment::PairwiseSequenceAlignment_SP> & container) {

  core::alignment::PairwiseSequenceAlignment_SP a = read_hhpred(buffer);
  while (a != nullptr) {
    container.push_back(a);
    a = read_hhpred(buffer);
  }

  return container;
}

std::vector<core::alignment::PairwiseSequenceAlignment_SP> & read_hhpred(const std::string & file_name,
          std::vector<core::alignment::PairwiseSequenceAlignment_SP> & container) {

  std::ifstream in(file_name);
  read_hhpred(in,container);

  return container;
}


}
}
}
