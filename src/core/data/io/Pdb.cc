#include <string>
#include <sstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <chrono>
#include <fstream>
#include <vector>
#include <set>
#include <regex>

#include <core/data/io/Pdb.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/basic/Vec3I.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>
#include <core/chemical/AtomicElement.hh>
#include <core/chemical/monomer_io.hh>

#include <utils/Logger.hh>
#include <utils/exit.hh>
#include <utils/string_utils.hh>
#include <utils/io_utils.hh>
#include <utils/exceptions/NoSuchFile.hh>
#include <core/data/basic/Vec3Cubic.hh>

namespace core {
namespace data {
namespace io {

using namespace core::data::structural;

utils::Logger Pdb::logger = utils::Logger("Pdb");

const PdbLineFilter invert_filter(const PdbLineFilter f1) {

  auto f = [=](const std::string line) {

    return !f1(line);
  };
  return f;
}

const PdbLineFilter all_true(const PdbLineFilter f1, const PdbLineFilter f2) {

  auto f = [=](const std::string line) {

    return f1(line)&&f2(line);
  };
  return f;
}

const PdbLineFilter one_true(const PdbLineFilter f1, const PdbLineFilter f2) {

    auto f = [=](const std::string line) {

        return f1(line)||f2(line);
    };
    return f;
}


const PdbLineFilter all_true(const PdbLineFilter f1, const PdbLineFilter f2, const PdbLineFilter f3) {

  auto f = [=](const std::string & line) {

    return f1(line)&&f2(line)&&f3(line);
  };
  return f;
}

const PdbLineFilter all_true(const PdbLineFilter f1, const PdbLineFilter f2, const PdbLineFilter f3, const PdbLineFilter f4) {

  auto f = [=](const std::string line) {

    return f1(line)&&f2(line)&&f3(line)&&f4(line);
  };
  return f;
}

const PdbLineFilter all_true(const PdbLineFilter f1, const PdbLineFilter f2, const PdbLineFilter f3, const PdbLineFilter f4, const PdbLineFilter f5) {

  auto f = [=](const std::string line) {

    return f1(line)&&f2(line)&&f3(line)&&f4(line)&&f5(line);
  };
  return f;
}

const PdbLineFilter all_true(const std::vector<PdbLineFilter> & filters) {

  auto f = [=](const std::string line) {
      bool out = true;
      for(const auto & fi : filters) out = out & fi(line);
      return out;
  };
  return f;
}

std::string Remark290::to_pdb_line() const {

  std::stringstream sb;
  for (const auto & rec : records)
    for (const auto &l : rec) sb << l << "\n";
  return sb.str();
}

void Remark290::add_line(const std::string &pdb_line) {

  core::index2 n_op = utils::from_string<int>(pdb_line, 21, 23, 1);
  core::index2 n_l = utils::from_string<int>(pdb_line, 18, 19, 1);
  if (n_l == 1) records.push_back(std::vector<std::string>());
  records[n_op - 1].push_back(pdb_line);
  if (n_l == 3) {
    rt_.emplace_back();
    double tx = utils::from_string<double>(records[n_op - 1][0], 60, 68, 0);
    double x = utils::from_string<double>(records[n_op - 1][0], 24, 33, 1);
    double y = utils::from_string<double>(records[n_op - 1][0], 34, 43, 0);
    double z = utils::from_string<double>(records[n_op - 1][0], 44, 53, 0);
    rt_.back().rot_x(x, y, z);
    double ty = utils::from_string<double>(records[n_op - 1][1], 60, 68, 0);
    x = utils::from_string<double>(records[n_op - 1][1], 24, 33, 1);
    y = utils::from_string<double>(records[n_op - 1][1], 34, 43, 0);
    z = utils::from_string<double>(records[n_op - 1][1], 44, 53, 0);
    rt_.back().rot_y(x, y, z);
    double tz = utils::from_string<double>(records[n_op - 1][2], 60, 68, 0);
    x = utils::from_string<double>(records[n_op - 1][2], 24, 33, 1);
    y = utils::from_string<double>(records[n_op - 1][2], 34, 43, 0);
    z = utils::from_string<double>(records[n_op - 1][2], 44, 53, 0);
    rt_.back().rot_z(x, y, z);
    rt_.back().tr_after(tx, ty, tz);
  }
}

std::string Remark350::to_pdb_line() const {

    std::stringstream sb;
    std::vector<std::string> rts;
    core::index1 bio_cnt=1;

    for (auto i=rt_map.cbegin();i!=rt_map.cend();++i) {
        sb << "REMARK 350 BIOMOLECULE: "<<utils::string_format("%d",bio_cnt++)<<"\n";
        sb << "REMARK 350 APPLY THE FOLLOWING TO CHAINS:";
        for (auto chain:i->first) sb <<" "<<chain;
        sb<<"\n";
        rts.clear();
        rototranslation_to_pdb(i->second,rts);
        core::index1 xyz_cnt=0;
        core::index1 rt_cnt=1;
        for (auto l:rts) {
            sb<<"REMARK 350   BIOMT"<<(xyz_cnt++)%3+1<<utils::string_format(" %3d ",rt_cnt)<< l<<"\n";
            if ((xyz_cnt)%3==0) rt_cnt++;
        }
      }
    return sb.str();
    }

void Remark350::add_line(const std::string &pdb_line) {


    if (pdb_line.compare(0, 16, "REMARK 350 APPLY") == 0){
        chains_codes.clear();
        std::vector<std::string> two_parts;
        utils::split(pdb_line,two_parts, {':'},true);
        utils::split(two_parts[1],chains_codes, {','},true);
        rt_.clear();
    }

    if (pdb_line.compare(0, 18, "REMARK 350   BIOMT") == 0) {

        core::index2 n_l = utils::from_string<int>(pdb_line, 18, 19, 1);
        if (n_l == 1) records.push_back(std::vector<std::string>());
        records[n_op - 1].push_back(pdb_line);
        if (n_l == 3) {

            rt_.emplace_back();
            double tx = utils::from_string<double>(records[n_op - 1][0], 60, 68, 0);
            double x = utils::from_string<double>(records[n_op - 1][0], 24, 33, 1);
            double y = utils::from_string<double>(records[n_op - 1][0], 34, 43, 0);
            double z = utils::from_string<double>(records[n_op - 1][0], 44, 53, 0);
            rt_.back().rot_x(x, y, z);
            double ty = utils::from_string<double>(records[n_op - 1][1], 60, 68, 0);
            x = utils::from_string<double>(records[n_op - 1][1], 24, 33, 1);
            y = utils::from_string<double>(records[n_op - 1][1], 34, 43, 0);
            z = utils::from_string<double>(records[n_op - 1][1], 44, 53, 0);
            rt_.back().rot_y(x, y, z);
            double tz = utils::from_string<double>(records[n_op - 1][2], 60, 68, 0);
            x = utils::from_string<double>(records[n_op - 1][2], 24, 33, 1);
            y = utils::from_string<double>(records[n_op - 1][2], 34, 43, 0);
            z = utils::from_string<double>(records[n_op - 1][2], 44, 53, 0);
            rt_.back().rot_z(x, y, z);
            rt_.back().tr_after(tx, ty, tz);
            rt_map[chains_codes]=rt_;
            n_op+=1;
        }
    }

}


const std::vector<std::string> Pdb::pdb_filter_names({"is_ca", "is_bb", "is_cb", "is_hydrogen",
                                                      "is_hetero_atom", "is_standard_atom", "is_not_hydrogen", "is_not_alternative",
                                                      "only_ss_from_header", "is_not_water", "is_water"});

std::map<std::string const, PdbLineFilter> atom_filters_map = {{"is_ca", is_ca},
                                                               {"is_cb", is_cb},
                                                               {"is_not_hydrogen", is_not_hydrogen},
                                                               {"is_hydrogen", is_hydrogen},
                                                               {"is_not_water", is_not_water},
                                                               {"is_water", is_water},
                                                               {"is_hetero_atom", is_hetero_atom},
                                                               {"is_standard_atom", is_standard_atom},
                                                               {"is_not_alternative", is_not_alternative},
                                                               {"is_bb", is_bb}};

std::map<std::string const, PdbLineFilter> header_filters_map = {{"only_ss_from_header", only_ss_from_header}};


Pdb::Pdb(const std::string & fname, const PdbLineFilter & atom_predicate,
    const PdbLineFilter & header_predicate, const bool if_parse_header, const bool first_model_only) {

  std::shared_ptr<std::istream> in;
  if (if_parse_header)
    logger << utils::LogLevel::FINER << "PDB file header will be parsed\n";

  if(fname.size() > 256) { // --- most likely its not PDB data but a file name
    fname_ = "";
    in = std::make_shared<std::stringstream>(fname);
    read_pdb(*in, atom_predicate, header_predicate, if_parse_header, first_model_only);
    return;
  }

  logger << utils::LogLevel::FINER << "Attempting " << fname << "\n";

  if(!utils::if_file_exists(fname)) {
    std::string msg = "Can't find PDB file: " + fname + "!\nAttempt to parse the string as raw PDB data\n";
    logger<<utils::LogLevel::SEVERE<<msg;
  }

  if (utils::has_suffix(fname, ".gz")) {
    std::string tmp;
    utils::load_binary_file(fname, tmp);
    if (tmp.size() == 0) EXIT("Zero bytes loaded from a gzip'ed file: " + fname + "\n");

    std::shared_ptr<std::stringstream> ssp = std::make_shared<std::stringstream>();
    utils::ungzip_string(tmp,*ssp);
    in = std::static_pointer_cast<std::istream>(ssp);
    logger << utils::LogLevel::FILE << "Reading PDB from a gzip'ed file: " << fname << "\n";
  } else {
    in = std::static_pointer_cast<std::istream>(std::make_shared<std::ifstream>(fname));
    if (!*in) logger << utils::LogLevel::SEVERE << "Can't open file: " << fname << "\n";
    logger << utils::LogLevel::FILE << "Reading PDB from: " << fname << "\n";
  }
  fname_ = fname;
  read_pdb(*in, atom_predicate, header_predicate, if_parse_header, first_model_only);
}

Pdb::Pdb(const std::string &fname, const std::string &pdb_line_filters,
         const bool if_parse_header, const bool first_model_only) :
         Pdb(fname, pdb_line_filters_factory(pdb_line_filters, atom_filters_map),
             keep_all, if_parse_header, first_model_only) {
}



void Pdb::read_pdb(std::istream & infile, const PdbLineFilter & atom_predicate,
    const PdbLineFilter & header_predicate, const bool if_parse_header, const bool first_model_only) {

  std::string line;
  std::vector<Atom> v;
  atoms.push_back(std::make_shared<std::vector<Atom>>());
  termini.push_back(std::make_shared<std::vector<Ter>>());
  std::shared_ptr<std::vector<Atom>> current_model = atoms.back();
  std::shared_ptr<std::vector<Ter>> current_termini = termini.back();
  auto start = std::chrono::high_resolution_clock::now();
  std::shared_ptr<Seqres> seqres_sp = std::make_shared<Seqres>();

  std::shared_ptr<Remark290> remark290 = std::make_shared<Remark290>();
  std::shared_ptr<Remark350> remark350 = std::make_shared<Remark350>();
  std::shared_ptr<Expdata> expdata = std::make_shared<Expdata>();
  std::shared_ptr<Title> title = std::make_shared<Title>();
  std::shared_ptr<Compnd> compnd = std::make_shared<Compnd>();
  std::shared_ptr<Remark2> remark2 = std::make_shared<Remark2>();
  std::shared_ptr<Remark3> remark3 = std::make_shared<Remark3>();
  std::shared_ptr<Keywords> keywords = std::make_shared<Keywords>();

  std::set<std::string> distinct_residue_types; // --- Residue types that were found in the PDB being loaded
  std::set<std::string> distinct_ligand_types; // --- Ligand types that were found in the PDB being loaded
  bool after_ter = false;
  bool header_done = false;
  while (std::getline(infile, line)) {
    if (line.size() < 3) continue;   // Silently skip very short (or empty) lines

    if (line.compare(0, 6, "ENDMDL") == 0) {
      atoms.emplace_back(std::make_shared<std::vector<Atom>>());
      current_model = atoms.back();
      termini.emplace_back(std::make_shared<std::vector<Ter>>());
      current_termini = termini.back();
      after_ter = false;
      if(first_model_only) break;
    }
    // --- attempt parsing ATOM/HETATM lines first
    if ((line.compare(0, 4, "ATOM") == 0)||(line.compare(0, 6, "HETATM") == 0)) {
      header_done = true;
      if (!atom_predicate(line)) {
        logger << utils::LogLevel::FINE << "Skipping atom line due to predicate: " << line << "\n";
        continue;
      }
      if (line.size() < 53) {
        logger << utils::LogLevel::WARNING << "Skipping incomplete ATOM line: " << line << "\n";
        continue;
      }
      current_model->emplace_back(line);
      if (after_ter) // --- Here we collect the distinct 3-letter codes
        distinct_ligand_types.insert(current_model->back().res_name);
      else
        distinct_residue_types.insert(current_model->back().res_name);

      continue;
    }
    // --- TER line marks the end of a current chain
    if (line.compare(0, 4, "TER ") == 0) {  // TER must be parsed regardless header predicates
      current_termini->emplace_back(line);
      after_ter = true;
      continue;
    }
    // --- Load also CONECT lines to define bonds
    if (line.compare(0, 6, "CONECT") == 0) {
      std::shared_ptr<PdbField> p = std::static_pointer_cast<PdbField>(std::make_shared<Conect>(line));
      header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("CONECT", p));
      continue;
    }

    // --- if not any of the above, it's probably header

    if (if_parse_header && (!header_done)) {

      if (!header_predicate(line)) {
        logger << utils::LogLevel::FINE << "Skipping header line due to predicate: " << line << "\n";
        continue;
      }

      if (line.compare(0, 6, "SEQRES") == 0) {
        seqres_sp->add_line(line);
        continue;
      }

      if (line.compare(0, 6, "TITLE ") == 0) {
        title->add_line(line);
        continue;
      }

      if (line.compare(0, 6, "COMPND") == 0) {
        compnd->add_line(line);
        continue;
      }

      if (line.compare(0, 6, "EXPDTA") == 0) {
        expdata->add_line(line);
        continue;
      }

      if(line.compare(0, 18, "REMARK 290   SMTRY") == 0) {
        remark290->add_line(line);
        continue;
      }
      if(line.compare(0, 18, "REMARK 350   BIOMT") == 0 || line.compare(0, 16, "REMARK 350 APPLY") == 0) {
          remark350->add_line(line);
          continue;
      }

      if(line.compare(0, 11, "REMARK   2 ") == 0) {
        remark2->add_line(line);
        continue;
      }

      if(line.compare(0, 11, "REMARK   3 ") == 0) {
        remark3->add_line(line);
        continue;
      }

      if (line.compare(0, 6, "MODRES") == 0) {
        std::shared_ptr<PdbField> p = std::static_pointer_cast<PdbField>(std::make_shared<Modres>(line));
        header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("MODRES", p));
        continue;
      }

      if (line.compare(0, 6, "FORMUL") == 0) {
        std::shared_ptr<PdbField> p = std::static_pointer_cast<PdbField>(std::make_shared<Formula>(line));
        header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("FORMUL", p));
        continue;
      }

      if (line.compare(0, 6, "HETNAM") == 0) {
        std::shared_ptr<PdbField> p = std::static_pointer_cast<PdbField>(std::make_shared<Hetnam>(line));
        header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("HETNAM", p));
        continue;
      }

      if (line.compare(0, 6, "HEADER") == 0) {
        std::shared_ptr<PdbField> p = std::static_pointer_cast<PdbField>(std::make_shared<Header>(line));
        header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("HEADER", p));
        continue;
      }

      if (line.compare(0, 6, "KEYWDS") == 0) {
        keywords->add_line(line);
        continue;
      }

      if (line.compare(0, 6, "DBREF ") == 0) {
        std::shared_ptr<PdbField> p = std::static_pointer_cast<PdbField>(std::make_shared<DBRef>(line));
        header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("DBREF", p));
        continue;
      }

      if (line.compare(0, 6, "HELIX ") == 0) {
        std::shared_ptr<PdbField> p = std::static_pointer_cast<PdbField>(std::make_shared<HelixField>(line));
        header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("HELIX", p));
        continue;
      }

      if (line.compare(0, 6, "SHEET ") == 0) {
        std::shared_ptr<PdbField> p = std::static_pointer_cast<PdbField>(std::make_shared<SheetField>(line));
        header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("SHEET", p));
        continue;
      }
    }
  }

  if(seqres_sp!=nullptr)
    header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("SEQRES", seqres_sp));

  if(compnd->compound_data().size() > 12)
    header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("COMPND", compnd));
  if(title->title().size() > 10)
    header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("TITLE", title));

  header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("EXPDTA", expdata));
  header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("KEYWRDS", keywords));
  header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("REMARK2", remark2));
  header.insert(std::pair<std::string, std::shared_ptr<PdbField>>("REMARK3", remark3));

  if (current_model->size() == 0) {
    if (atoms.size() == 1)
      logger << utils::LogLevel::WARNING << "The input file stream contains no valid ATOM lines !\n";
    atoms.pop_back();
    termini.pop_back();
  }
  if (remark290->count_operators() > 0) remark290_ = remark290;
  if (remark350->count_biomolecules() > 0) remark350_ = remark350;

  // ---------- Here we check if all monomers found in ATOM lines had been defined in BioShell database (monomers.txt file)
  // ---------- In not, the unknown ones will be converted to UNK or UNL
  core::index2 n_created = 30000;
  if (distinct_ligand_types.size() > 0) core::chemical::load_monomers_from_db();
  else {
    bool must_load = false;
    for (const std::string &code3 : distinct_residue_types)
      if (core::chemical::Monomer::is_standard_code(code3)) {
        must_load = true;
        break;
      }
    if(must_load) core::chemical::load_monomers_from_db();
  }
  for(const std::string & code3 : distinct_residue_types) {
    if(!core::chemical::Monomer::is_known_monomer(code3)) {
      logger << utils::LogLevel::INFO << "Creating new monomer for code " << code3 << " derived from UNK\n";
      core::chemical::Monomer m{n_created,'X',code3,'P',0,0,false,0,core::chemical::Monomer::UNK.id};
      core::chemical::Monomer::register_monomer(m);
      ++n_created;
    }
  }
  for(const std::string & code3 : distinct_ligand_types) {
    if(!core::chemical::Monomer::is_known_monomer(code3)) {
      logger << utils::LogLevel::INFO << "Creating new monomer for code " << code3 << " derived from UNL\n";
      core::chemical::Monomer m{n_created,'X',code3,'U',0,0,false,0,core::chemical::Monomer::UNL.id};
      core::chemical::Monomer::register_monomer(m);
      ++n_created;
    }
  }

  //---------- show timer and stats
  auto end = std::chrono::high_resolution_clock::now();
  logger << utils::LogLevel::INFO << "Found " << atoms.size() << " models, " << atoms[0]->size() << " atoms each after "
      << (double) (std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()) << " [ms]\n";
}

Structure_SP Pdb::create_structure(const core::index2 which_model) {

  Structure_SP structure = std::make_shared<Structure>(pdb_code());
  if(structure->code().size()==0) structure->code(pdb_code_from_file_name(fname_));
  char last_chain_code = 0;
  Chain_SP last_chain;
  int last_resid_id = -65535;
  char last_icode = ' ';
  Residue_SP last_residue;
  bool new_chain_flag = false;
  for (Atom aline : *atoms[which_model]) {
// ---------- do we have a new chain?
    if (aline.chain != last_chain_code) {
      last_chain_code = aline.chain;
      new_chain_flag = true; // --- we got a new chain
      if (!structure->has_chain(aline.chain)) {
        last_chain = std::make_shared<Chain>(std::string{last_chain_code});
        structure->push_back(last_chain);
        logger << utils::LogLevel::FINE << "Creating a new chain: " << aline.chain << "\n";
      } else last_chain = structure->get_chain(std::string{aline.chain});
    }
    // ---------- do we have a new residue?
    if (new_chain_flag || (aline.residue_id != last_resid_id) || (aline.i_code != last_icode)) {
      last_icode = aline.i_code;
      last_resid_id = aline.residue_id;
      last_residue = std::make_shared<Residue>(aline.residue_id, aline.res_name);
      last_residue->icode(last_icode);
      last_chain->push_back(last_residue );
      logger << utils::LogLevel::FINER << "Creating a new residue: " << aline.residue_id <<" "<< aline.res_name << "\n";
    }

    PdbAtom_SP a =
        ((aline.element[0] != ' ') || (aline.element[1] != ' ')) ?
            std::make_shared<PdbAtom>(aline.serial, aline.name, aline.x, aline.y, aline.z, aline.occupancy,
                aline.temp_factor, core::chemical::AtomicElement::by_symbol(utils::trim(aline.element)).z) :
            std::make_shared<PdbAtom>(aline.serial, aline.name, aline.x, aline.y, aline.z, aline.occupancy,
                aline.temp_factor, core::chemical::AtomicElement::DUMMY.z);
    a->alt_locator(aline.alt_loc);
    a->is_heteroatom(aline.is_heteroatom);
    last_residue->push_back(a);
    if(logger.is_logable(utils::LogLevel::FINEST))
      logger << utils::LogLevel::FINEST << "Creating a new atom: " << *a << "\n";
    new_chain_flag = false; // --- clear the flag, will wait for the next new chain
  }

  // ---------- copy header items ----------
  structure->pdb_header.insert(header.begin(), header.end());

  // ---------- annotate residues with secondary structure ----------
  auto range = header.equal_range("HELIX");
  for (Pdb::HeaderIterator it = range.first; it != range.second; ++it) {
    std::shared_ptr<HelixField> h = std::static_pointer_cast<HelixField>((*it).second);
    for (auto rit = structure->first_residue(); rit != structure->last_residue(); ++rit)
      if (h->is_my_residue(**rit)) {
        (*rit)->ss('H');
        logger << utils::LogLevel::FINE << "Residue " << (**rit).owner()->id() << " " << (**rit)
               << " marked as H as it belongs to " << h->to_pdb_line() << "\n";
      }
  }

  range = header.equal_range("SHEET");
  for (Pdb::HeaderIterator it = range.first; it != range.second; ++it) {
    std::shared_ptr<SheetField> h = std::static_pointer_cast<SheetField>((*it).second);
    for (auto rit = structure->first_residue(); rit != structure->last_residue(); ++rit)
      if (h->is_my_residue(**rit)) {
        (*rit)->ss('E');
        logger << utils::LogLevel::FINE << "Residue " << (**rit).owner()->id() << " " << (**rit)
               << " marked as E as it belongs to " << h->to_pdb_line() << "\n";
      }
  }

  // ---------- assign chain termini ----------
  for(Chain_SP ci : *structure)             // --- first assign all termini to the last residue of a given chain
    ci->terminal_index_ = ci->size() - 1;   // --- this will be overriden by a TER field, is found

  for(const auto & ter : (*termini[which_model])) {
    if(!ter.is_complete) {
      logger << utils::LogLevel::WARNING << "Incomplete TER field, assigned to the very last residue in the chain\n";
      continue;
    }
    Chain_SP terminated_chain = nullptr;
    try {
      terminated_chain = structure->get_chain(ter.chain);
    } catch (const std::invalid_argument & e) {
      logger << utils::LogLevel::INFO << "Can't find chain : " << ter.chain << " to be closed with a TER field\n";
    }
    if (terminated_chain == nullptr) continue;
    try {
      index2 i_res = 0;
      for(;i_res<terminated_chain->size();++i_res) {
        Residue_SP r = (*terminated_chain)[i_res];
        if((r->id() == ter.residue_id) && (r->icode() == ter.i_code)) break;
      }
      if (i_res == terminated_chain->size()) {
        logger << utils::LogLevel::SEVERE << "TER field: " << ter.to_pdb_line() <<
               " can't be assigned to any residue of the chain " << ter.chain << " model " << which_model << "\n";
        terminated_chain->terminal_index_ = terminated_chain->size() - 1;
        continue;
      }
      terminated_chain->terminal_index_ = i_res;
    } catch (const std::invalid_argument &e) {
      logger << utils::LogLevel::SEVERE << "TER field: " << ter.to_pdb_line() <<
             " can't be assigned to any residue of the chain " << ter.chain << " model " << which_model << "\n";
      terminated_chain->terminal_index_ = terminated_chain->size() - 1;
    }
  }

  return structure;
}

void Pdb::create_structures(std::vector<core::data::structural::Structure_SP> & structures) {

  for(core::index4 i=0;i<count_models();++i) structures.push_back( create_structure(i) );
}

void Pdb::fill_structure(const core::index2 which_model, core::data::structural::Structure &structure) {

  const std::vector<Atom> &atms = *atoms[which_model];
  // ---------- Check whether the number of atoms is the same
  if (structure.count_atoms() != atms.size())
    throw std::length_error(
      utils::string_format("Wrong number of atoms! source lines: %d, destination structure: %d", atms.size(),
        structure.count_atoms()));

  core::index4 i_atom = 0;
  for (auto chain_sp : structure) {
    for (auto res_sp : *chain_sp) {
      for (auto atom_sp: *res_sp) {
        atom_sp->x = atms[i_atom].x;
        atom_sp->y = atms[i_atom].y;
        atom_sp->z = atms[i_atom].z;
        ++i_atom;
      }
    }
  }
}


PdbLineFilter Pdb::pdb_line_filters_factory(const std::string &pdb_filter_names_string,
    std::map<std::string const, PdbLineFilter> & filters_map) {

  std::vector<PdbLineFilter> filters;
  std::vector<std::string> names;
  utils::split(pdb_filter_names_string, names, {' '}, true);
  for (const std::string &n : names) {
    if (filters_map.find(n) == filters_map.cend()) {
      logger << utils::LogLevel::CRITICAL << "Unknown PdbLineFilter name: " << n << "\n";
      throw std::out_of_range("Unknown PdbLineFilter name: " + n);
    }
    filters.push_back(filters_map.at(n));
  }

  return all_true(filters);
}

void Pdb::fill_structure(const core::index2 which_model, std::vector<core::data::basic::Vec3> & xyz) {

  const std::vector<Atom> & atms = *atoms[which_model];
  // ---------- Check whether the number of atoms is the same
  if (xyz.size() != atms.size())
    throw std::length_error(
      utils::string_format("Wrong number of atoms! source lines: %d, destination structure: %d", atms.size(), xyz.size()));

  for(size_t i_atom=0;i_atom<xyz.size();++i_atom) {
    xyz[i_atom].x = atms[i_atom].x;
    xyz[i_atom].y = atms[i_atom].y;
    xyz[i_atom].z = atms[i_atom].z;
  }
}

void Pdb::fill_structure(const core::index2 which_model, std::vector<core::data::basic::Vec3I> & xyz) {

  const std::vector<Atom> & atms = *atoms[which_model];
  // ---------- Check whether the number of atoms is the same
  if (xyz.size() != atms.size())
    throw std::length_error(
        utils::string_format("Wrong number of atoms! source lines: %d, destination structure: %d", atms.size(), xyz.size()));

  for(size_t i_atom=0;i_atom<xyz.size();++i_atom) {
    xyz[i_atom].x(atms[i_atom].x);
    xyz[i_atom].y(atms[i_atom].y);
    xyz[i_atom].z(atms[i_atom].z);
  }
}

std::string Pdb::pdb_code() const {

  auto it = header.find("HEADER");
  if (it == header.end()) return "";
  else {
    std::shared_ptr<Header> h = std::static_pointer_cast<Header>(it->second);
    return h->pdb_id;
  }
}

std::string Header::to_pdb_line() const {

  return "HEADER    " + classification + date + "   " + pdb_id;
}

Header::Header(const std::string & pdb_line) : classification(pdb_line.substr(10, 40)),
  date(pdb_line.substr(50, 9)), pdb_id(pdb_line.substr(62, 4)) {

}

std::string Formula::to_pdb_line() const {

  return "FORMUL    " + code3 + "   " + formula;
}

Formula::Formula(const std::string & pdb_line) : code3(pdb_line.substr(12, 3)),
                                               formula(pdb_line.substr(18)) {

}

std::string Hetnam::to_pdb_line() const {

  return "HETNAM     " + code3 + " " + name;
}

void Keywords::add_line(const std::string &pdb_line) {
  utils::split(pdb_line.substr(10), keywords_, ',');
}

std::string Keywords::to_pdb_line() const {

  std::string line = "KEYWDS   ";
  std::string full_line = "";
  if (keywords_.size() < 1) return line;
  line += keywords_[0];
  std::cout << "\n" << full_line << "\n";
  int cnt = 1;
  for (size_t i = 1; i < keywords_.size(); i++) {
    const std::string &s = keywords_[i];
    if (line.length() + s.length() >= 76) {
      cnt++;
      full_line += line;
      line = utils::string_format("\nKEYWORDS  %2d", cnt) + " " + s;
    } else line += ", " + s;
  }

  full_line += line;

  return full_line;
}

void Compnd::add_line(const std::string & pdb_line) {

  compound_data_.push_back(pdb_line.substr(10));
}

std::string Compnd::to_pdb_line() const {

  std::string line = "COMPND    " + compound_data_[0];
  for(index2 i=1;i<compound_data_.size();++i)
    line += utils::string_format("\nCOMPND %3d %s",i+1,compound_data_[i].c_str());
  return line;
}

std::string Compnd::compound_data() const {

  std::string line = "";
  for (const std::string &s:compound_data_) {
    std::string s_copy{s};
    utils::trim(s_copy);
    line += s_copy + " ";
  }
  return line;
}

void Title::add_line(const std::string &pdb_line) {

  compound_data_.push_back(pdb_line.substr(10));
}

std::string Title::to_pdb_line() const {

  std::string line = "TITLE     " + compound_data_[0];
  for (index2 i = 1; i < compound_data_.size(); ++i)
    line += utils::string_format("\nTITLE  %3d %s", i+1, compound_data_[i].c_str());
  return line;
}

std::string Title::title() const {

  std::string line = "";
  for (const std::string &s:compound_data_)
    line += s + " ";
  return line;
}

void Expdata::add_line(const std::string & pdb_line) {

  std::string m = pdb_line.substr(10);
  utils::split_into_strings(m, methods_, ';', true);
}

std::string Expdata::to_pdb_line() const {

  std::string out = "EXPDATA    " + methods_[0];
  for (int i = 1; i < methods_.size(); ++i) out += "; " + methods_[i];

  return out;
}

bool Expdata::is_xray() const {

  for (const std::string &m: methods_)
    if (m.find("X-RAY") != std::string::npos) return true;

  return false;
}

bool Expdata::is_nmr() const {

  for (const std::string &m: methods_)
    if (m.find("NMR") != std::string::npos) return true;

  return false;
}

bool Expdata::is_em() const {

  for (const std::string &m: methods_)
    if (m.find("ELECTRON MICROSCOPY") != std::string::npos) return true;

  return false;
}

void Remark2::add_line(const std::string & pdb_line) {

  if (pdb_line.find("NOT APPLICABLE") != std::string::npos) resolution = -1;
  else {
    std::vector<std::string> tokens;
    utils::split_into_strings(pdb_line,tokens,' ',true);
    if(tokens.size()>3)
      resolution = utils::to_double(tokens[3].c_str());
  }
}

std::string Remark2::to_pdb_line() const {

  if (resolution > 0)
    return utils::string_format("REMARK   2\nREMARK   2 RESOLUTION.   %5.2f  ANGSTROMS.", resolution);
  else return "REMARK   2\nREMARK   2 RESOLUTION.    NOT APPLICABLE.";
}


void Remark3::add_line(const std::string & pdb_line) {

  std::smatch r;
  if (std::regex_search(pdb_line, r, std::regex(r_value_1)))
    if (r.size() == 2) r_value = utils::to_double(r[1].str().c_str());
  if (std::regex_search(pdb_line, r, std::regex(r_value_2)))
    if (r.size() == 2) r_value = utils::to_double(r[1].str().c_str());
  if (std::regex_search(pdb_line, r, std::regex(r_value_3)))
    if (r.size() == 2) r_value = utils::to_double(r[1].str().c_str());
  if (std::regex_search(pdb_line, r, std::regex(r_value_4)))
    if (r.size() == 2) r_value = utils::to_double(r[1].str().c_str());

  if (std::regex_search(pdb_line, r, std::regex(free_r_value_1)))
    if (r.size() == 2) r_free = utils::to_double(r[1].str().c_str());
  if (std::regex_search(pdb_line, r, std::regex(free_r_value_2)))
    if (r.size() == 2) r_free = utils::to_double(r[1].str().c_str());
}

std::string Remark3::to_pdb_line() const {

  std::string out = (r_value > 0) ?
      utils::string_format("REMARK   3\nREMARK   3   R VALUE                          : %.3f", r_value) :
      "REMARK   3\nREMARK   3   R VALUE                          : NONE";
  out += (r_free > 0) ?
                    utils::string_format("REMARK   3   FREE R VALUE                     : %.3f", r_value) :
                    "REMARK   3   FREE R VALUE                     : NONE";

  return out;
}

const std::regex Remark3::r_value_1("REMARK   3\\s+R VALUE\\s+: (\\d+\\.\\d+)");
const std::regex Remark3::r_value_2("REMARK   3\\s+R VALUE\\s+\\(WORKING SET\\)\\s+: (\\d+\\.\\d+)");
const std::regex Remark3::r_value_3("REMARK   3\\s+R VALUE\\s+\\(WORKING \\+ TEST SET\\)\\s+: (\\d+\\.\\d+)");
const std::regex Remark3::r_value_4("REMARK   3\\s+R VALUE\\s+\\(WORKING \\+ TEST SET, NO CUTOFF\\)\\s+: (\\d+\\.\\d+)");

const std::regex Remark3::free_r_value_1("REMARK   3\\s+FREE R VALUE\\s+: (\\d+\\.\\d+)");
const std::regex Remark3::free_r_value_2("REMARK   3\\s+FREE R VALUE\\s+\\(NO CUTOFF\\)\\s+: (\\d+\\.\\d+)");

DBRef::DBRef(const std::string &pdb_line) {

  pdb_code = pdb_line.substr(7, 4);
  chain = pdb_line[12];
  residue_from = utils::from_string<int>(pdb_line, 14, 17, -1);
  insert_from = pdb_line[18];
  residue_to = utils::from_string<int>(pdb_line, 20, 23, -1);
  insert_to = pdb_line[24];
  db_name = pdb_line.substr(26, 6);
  db_accession = pdb_line.substr(33, 8);
  db_code = pdb_line.substr(42, 12);
  db_residue_from = utils::from_string<int>(pdb_line, 55, 59, -1);
  db_insert_from = pdb_line[60];
  db_residue_to = utils::from_string<int>(pdb_line, 62, 66, -1);
  db_insert_to = pdb_line[67];
}

std::string DBRef::to_pdb_line() const {

  return utils::string_format("DBREF   %s %c %4d%c %4d%c %6s %8s %12s %4d%c %4d%c", pdb_code.c_str(), chain,
      residue_from, insert_from, residue_to, insert_to, db_name.c_str(), db_accession.c_str(), db_code.c_str(),
      db_residue_from, db_insert_from, db_residue_to, db_insert_to);
}

TVect::TVect(const std::string &pdb_line) {

  serial = utils::from_string<index2>(pdb_line, 7, 10, 1);
  x = utils::from_string<double>(pdb_line, 10, 20, 0.0);
  y = utils::from_string<double>(pdb_line, 20, 30, 0.0);
  z = utils::from_string<double>(pdb_line, 30, 40, 0.0);
}

std::string TVect::to_pdb_line() const {

  return utils::string_format("TVECT %4d%10.3f%10.3f%10.3f", serial, x, y, z);
}

Remark465::Remark465(const std::string &pdb_line) : residue_type(core::chemical::Monomer::UNK) {

  chain_id = pdb_line[18];
  residue_id = utils::from_string<int>(pdb_line, 20, 35, 0);
  residue_type = core::chemical::Monomer::get(pdb_line.substr(14, 3));
  model_id = utils::from_string<index2>(pdb_line, 9, 14, 0);
  if (pdb_line.size() > 25) i_code = pdb_line[25];
  else i_code = ' ';
}

std::string Remark465::to_pdb_line() const {

  if (model_id == 0)
    return utils::string_format("REMARK 465     %3s %c %5d%c", residue_type.code3.c_str(), chain_id, residue_id, i_code);
  else
    return utils::string_format("REMARK 465%4d %3s %c %5d%c", model_id,residue_type.code3.c_str(), chain_id, residue_id, i_code);
}

HelixField::HelixField(const std::string &pdb_line) {

  serial = utils::from_string<int>(pdb_line, 7, 10, -1);
  helix_id = pdb_line.substr(11, 3);
  residue_name_from = pdb_line.substr(15, 3);
  chain_from = pdb_line[19];
  residue_id_from = utils::from_string<int>(pdb_line, 21, 25, -1);
  insert_from = pdb_line[25];

  residue_name_to = pdb_line.substr(27, 3);
  residue_id_to = utils::from_string<int>(pdb_line,33, 36,-1);
  chain_to = pdb_line[31];
  insert_to = pdb_line[37];
  helix_class = utils::from_string<int>(pdb_line,39, 41,-1);
  length =  utils::from_string<int>(pdb_line,72, 77,-1);
  comment = pdb_line.substr(40, 30);
}

std::string HelixField::to_pdb_line() const {

  return utils::string_format("HELIX  %3d %s %s %c %4d%c %3s %c %4d%c%2d %s %4d", serial, helix_id.c_str(),
      residue_name_from.c_str(), chain_from[0], residue_id_from, insert_from, residue_name_to.c_str(), chain_to[0],
      residue_id_to, insert_to, helix_class, comment.c_str(), length);
}

bool HelixField::is_my_residue(const core::data::structural::Residue & r) {

  if ((r.owner()->id() != chain_from) && (r.owner()->id() != chain_to)) return false;
  if ((r.id() > residue_id_from) && (r.id() < residue_id_to)) return true;
  if ((r.id() == residue_id_from) && (r.icode() >= insert_from)) return true;
  if ((r.id() == residue_id_to) && (r.icode() <= insert_to)) return true;
  return false;
}

SheetField::SheetField(const std::string &pdb_line) {

  strand_id = utils::from_string<int>(pdb_line, 7, 10, -1);
  sheet_id = pdb_line.substr(11, 3);
  n_strands = utils::from_string<int>(pdb_line, 14, 16, -1);

  residue_name_from = pdb_line.substr(17, 3);
  chain_from = pdb_line[21];
  residue_id_from = utils::from_string<int>(pdb_line, 22, 26, -1);
  insert_from = pdb_line[26];

  residue_name_to = pdb_line.substr(28, 3);
  chain_to = pdb_line[32];
  residue_id_to = utils::from_string<int>(pdb_line, 33, 37, -1);
  insert_to = pdb_line[37];

  sense = utils::from_string<int>(pdb_line, 38, 40, -1);

  if (sense != 0) {
    register_my_atom = pdb_line.substr(41, 4);
    register_my_residue_name = pdb_line.substr(45, 3);
    register_my_chain = pdb_line[49];
    register_my_residue_id = utils::from_string<int>(pdb_line, 51, 55, -1);
    register_my_insert = pdb_line[54];

    register_previous_atom = pdb_line.substr(56, 4);
    register_previous_residue_name = pdb_line.substr(60, 3);
    register_previous_chain = pdb_line[64];
    register_previous_residue_id = utils::from_string<int>(pdb_line, 65, 69, -1);
    register_previous_insert = pdb_line[69];
  } else {
    register_my_atom = "    ";
    register_my_residue_name = "   ";
    register_my_chain = ' ';
    register_my_residue_id = -1;
    register_my_insert = ' ';

    register_previous_atom = "    ";
    register_previous_residue_name = "   ";
    register_previous_chain = ' ';
    register_previous_residue_id = -1;
    register_previous_insert = ' ';
  }
}

std::string SheetField::to_pdb_line() const {

  if (sense != 0) {
    return utils::string_format("SHEET  %3d %3s%2d %3s %c%4d%c %3s %c%4d%c%2d %4s%3s %c%4d%c %4s%3s %c%4d%c", strand_id,
        sheet_id.c_str(), n_strands, residue_name_from.c_str(), chain_from[0], residue_id_from, insert_from,
        residue_name_to.c_str(), chain_to[0], residue_id_to, insert_to, sense, register_my_atom.c_str(),
        register_my_residue_name.c_str(), register_my_chain[0], register_my_residue_id, register_my_insert, register_previous_atom.c_str(),
        register_previous_residue_name.c_str(), register_previous_chain[0], register_previous_residue_id,
        register_previous_insert);
  } else {
    return utils::string_format("SHEET  %3d %3s%2d %3s %c%4d%c %3s %c%4d%c%2d", strand_id, sheet_id.c_str(),
        n_strands, residue_name_from.c_str(), chain_from[0], residue_id_from, insert_from, residue_name_to.c_str(),
        chain_to[0], residue_id_to, insert_to, sense);
  }
}

bool SheetField::is_my_residue(const core::data::structural::Residue & r) {

  if ((r.owner()->id() != chain_from) && (r.owner()->id() != chain_to)) return false;
  if ((r.id() > residue_id_from) && (r.id() < residue_id_to)) return true;
  if ((r.id() == residue_id_from) && (r.icode() >= insert_from)) return true;
  if ((r.id() == residue_id_to) && (r.icode() <= insert_to)) return true;
  return false;
}

Modres::Modres(const std::string &pdb_line) {

  pdb_id = pdb_line.substr(7, 4);
  res_name = pdb_line.substr(12, 3);
  chain = pdb_line[16];
  residue_id = utils::from_string<int>(pdb_line, 18, 21, -1);
  i_code = pdb_line[23];
  std_res_name = pdb_line.substr(24, 3);
  comment = pdb_line.substr(29, 40);
}

void Seqres::add_line(const std::string & seqres_line) {

  char chain = seqres_line[11];
  core::index2 n = utils::from_string<core::index2>(seqres_line, 12, 17, 0);
  std::vector<std::string> aa = utils::split(seqres_line.substr(17), {' '});
  if(sequences.find(chain)==sequences.end()) {
    std::vector<core::chemical::Monomer> v;
    sequences.insert(std::pair<char,std::vector<core::chemical::Monomer>>(chain,v));
  }
  std::vector<core::chemical::Monomer> & v = sequences.at(chain);
  for(const std::string & s:aa) {
    if(s.length() == 3) v.push_back(core::chemical::Monomer::get(s));
    else {
      if(s.length() == 2) v.push_back(core::chemical::Monomer::get(" " + s));
      else v.push_back(core::chemical::Monomer::get("  " + s));
    }
  }
  if(v.size()>n) {
    logger<<utils::LogLevel::SEVERE<<"Corrupted SEQRES data in a PDB file!\n";
  }
}

std::string Modres::to_pdb_line() const {

  return utils::string_format("MODRES %4s %3s %c %4d%c %3s %s", pdb_id.c_str(), res_name.c_str(), chain, residue_id,
      i_code, std_res_name.c_str(), comment.c_str());
}

const std::string Conect::conect_format_2 = std::string("CONECT%5d%5d");
const std::string Conect::conect_format_3 = std::string("CONECT%5d%5d%5d");
const std::string Conect::conect_format_4 = std::string("CONECT%5d%5d%5d%5d");
const std::string Conect::conect_format_5 = std::string("CONECT%5d%5d%5d%5d%5d");
const std::string Conect::conect_format_6 = std::string("CONECT%5d%5d%5d%5d%5d%5d");

Conect::Conect(const std::string &pdb_line) {

  std::string copy{pdb_line.substr(6)};
  atom_ids_.push_back(utils::from_string<core::index4>(pdb_line, 6, 10, 0));
  atom_ids_.push_back(utils::from_string<core::index4>(pdb_line, 11, 15, 0));
  if (pdb_line.size() > 20) {
    atom_ids_.push_back(utils::from_string<core::index4>(pdb_line, 16, 20, 0));
    if (pdb_line.size() > 25) {
      atom_ids_.push_back(utils::from_string<core::index4>(pdb_line, 21, 25, 0));
      if (pdb_line.size() > 30)
        atom_ids_.push_back(utils::from_string<core::index4>(pdb_line, 26, 30, 0));
    }
  }
}

Conect::Conect(const core::index4 this_atom_id, const core::index4 bonded_atom_id) {
  atom_ids_.push_back(this_atom_id);
  atom_ids_.push_back(bonded_atom_id);
}

Conect::Conect(const core::index4 this_atom_id, const core::index4 bonded_atom_id,
               const core::index4 bonded_atom_id_2) {
  atom_ids_.push_back(this_atom_id);
  atom_ids_.push_back(bonded_atom_id);
  atom_ids_.push_back(bonded_atom_id_2);
}

Conect::Conect(const core::index4 this_atom_id, const core::index4 bonded_atom_id, const core::index4 bonded_atom_id_2,
               const core::index4 bonded_atom_id_3) {
  atom_ids_.push_back(this_atom_id);
  atom_ids_.push_back(bonded_atom_id);
  atom_ids_.push_back(bonded_atom_id_2);
  atom_ids_.push_back(bonded_atom_id_3);
}

Conect::Conect(const core::index4 this_atom_id, const core::index4 bonded_atom_id, const core::index4 bonded_atom_id_2,
               const core::index4 bonded_atom_id_3, const core::index4 bonded_atom_id_4) {
  atom_ids_.push_back(this_atom_id);
  atom_ids_.push_back(bonded_atom_id);
  atom_ids_.push_back(bonded_atom_id_2);
  atom_ids_.push_back(bonded_atom_id_3);
  atom_ids_.push_back(bonded_atom_id_4);
}

Conect::Conect(const core::index4 this_atom_id, const core::index4 bonded_atom_id, const core::index4 bonded_atom_id_2,
               const core::index4 bonded_atom_id_3, const core::index4 bonded_atom_id_4,
               const core::index4 bonded_atom_id_5) {
  atom_ids_.push_back(this_atom_id);
  atom_ids_.push_back(bonded_atom_id);
  atom_ids_.push_back(bonded_atom_id_2);
  atom_ids_.push_back(bonded_atom_id_3);
  atom_ids_.push_back(bonded_atom_id_4);
  atom_ids_.push_back(bonded_atom_id_5);
}

std::string Conect::to_pdb_line() const {

  if (atom_ids_.size() == 2) return utils::string_format(conect_format_2, atom_ids_[0], atom_ids_[1]);
  if (atom_ids_.size() == 3) return utils::string_format(conect_format_3, atom_ids_[0], atom_ids_[1], atom_ids_[2]);
  if (atom_ids_.size() == 4)
    return utils::string_format(conect_format_4, atom_ids_[0], atom_ids_[1], atom_ids_[2], atom_ids_[3]);
  if (atom_ids_.size() == 5)
    return utils::string_format(conect_format_5, atom_ids_[0], atom_ids_[1], atom_ids_[2], atom_ids_[3], atom_ids_[4]);
  return utils::string_format(conect_format_6, atom_ids_[0], atom_ids_[1], atom_ids_[2], atom_ids_[3], atom_ids_[4],
      atom_ids_[5]);
}

Ter::Ter(const std::string &pdb_line) {

  std::string copy(pdb_line);
  utils::trim(copy);
  if(copy.size() < 24) {
    is_complete = false;
  } else {
    serial = utils::from_string<int>(copy, 6, 10, -1);
    res_name = copy.substr(17, 3);
    chain = std::string{copy[21]};
    residue_id = utils::from_string<int>(copy, 22, 25, -1);
    i_code = (copy.size() > 26) ? copy[26] : ' ';
    is_complete = true;
  }
}

std::string Ter::to_pdb_line() const {

  return utils::string_format("TER   %5d      %3s %c%4d%c", serial, res_name.c_str(), chain[0], residue_id, i_code);
}

Atom::Atom(const std::string &pdb_line) {

  serial = utils::from_string<int>(pdb_line, 6, 10, -1);
  is_heteroatom = (pdb_line[0]=='H');
  name = pdb_line.substr(12, 4);
  alt_loc = pdb_line[16];
  res_name = pdb_line.substr(17, 3);
  chain = pdb_line[21];
  residue_id = utils::from_string<int>(pdb_line, 22, 25, -1);
  i_code = pdb_line[26];

  x = utils::from_string<double>(pdb_line, 30, 37, 0.0);
  y = utils::from_string<double>(pdb_line, 38, 45, 0.0);
  z = utils::from_string<double>(pdb_line, 46, 53, 0.0);
  if (pdb_line.length() >= 59) {
    occupancy = utils::from_string<double>(pdb_line, 54, 59, 1.0);
    if (pdb_line.length() >= 65) {
      temp_factor = utils::from_string<double>(pdb_line, 60, 65, 0.0);
      if (pdb_line.length() >= 78) {
        element = pdb_line.substr(76, 2);
        charge = pdb_line.substr(78, 2);
      }
    }
  }
}

std::string Atom::to_pdb_line() const {

  if(is_heteroatom)
    return utils::string_format(hetatm_format, serial, name.c_str(), alt_loc, res_name.c_str(), chain, residue_id, i_code,
                                x, y, z, occupancy, temp_factor, element.c_str(), charge.c_str());
  else
    return utils::string_format(atom_format, serial, name.c_str(), alt_loc, res_name.c_str(), chain, residue_id, i_code,
                                x, y, z, occupancy, temp_factor, element.c_str(), charge.c_str());
}

const std::string Atom::atom_format = std::string(
    "ATOM  %5d %4s%c%3s %c%4d%c   %8.3f%8.3f%8.3f%6.2f%6.2f          %2s%2s");
const std::string Atom::atom_format_uncharged = std::string(
    "ATOM  %5d %4s%c%3s %c%4d%c   %8.3f%8.3f%8.3f%6.2f%6.2f          %2s  ");

const std::string Atom::hetatm_format = std::string(
    "HETATM%5d %4s%c%3s %c%4d%c   %8.3f%8.3f%8.3f%6.2f%6.2f          %2s%2s");
const std::string Atom::hetatm_format_uncharged = std::string(
    "HETATM%5d %4s%c%3s %c%4d%c   %8.3f%8.3f%8.3f%6.2f%6.2f          %2s  ");

std::string Hetatm::to_pdb_line() const {

  return utils::string_format(hetatm_format, serial,
      name.c_str(), alt_loc, res_name.c_str(), chain, residue_id, i_code, x, y, z, occupancy, temp_factor,
      element.c_str(), charge.c_str());
}

core::index4 Pdb::read_coordinates(const std::string & fname, std::vector<core::data::basic::Vec3> & destination,
    const bool if_push_back, const PdbLineFilter & predicate) {

  std::ifstream infile;
  infile.open(fname);
  if (!infile) logger << utils::LogLevel::SEVERE << "Can't open file: " << fname << "\n";
  logger << utils::LogLevel::FILE << "Reading PDB coordinates from: " << fname << "\n";

  return read_coordinates(infile,destination, if_push_back, predicate);
}

core::index4 Pdb::read_coordinates(std::istream &infile, std::vector<core::data::basic::Vec3> &destination,
                                   const bool if_push_back, const PdbLineFilter &predicate) {

  core::index4 nc = 0;
  std::string line;
  if (if_push_back) destination.clear();
  while (std::getline(infile, line)) {
    if ((line.compare(0, 4, "ATOM") != 0) && (line.compare(0, 6, "HETATM") != 0)) continue;  // --- not an atom
    if (!predicate(line)) continue;

    if (!if_push_back) {
      if (nc >= destination.size()) return nc; // --- receiver is full
      destination[nc].x = utils::to_double(line.substr(30, 8).c_str());
      destination[nc].y = utils::to_double(line.substr(38, 8).c_str());
      destination[nc].z = utils::to_double(line.substr(46, 8).c_str());
    } else {
      Vec3 v(utils::to_double(line.substr(30, 8).c_str()),
        utils::to_double(line.substr(38, 8).c_str()),
        utils::to_double(line.substr(46, 8).c_str()));
      destination.push_back(v);
    }
    nc++;
  }

  return nc;
}

std::string Seqres::to_pdb_line() const {

  std::string lines;
  for(const auto & p :sequences) {
    const char chain_id = p.first;
    const std::vector<core::chemical::Monomer> & seq = p.second;
    core::index2 row=1;
    lines += utils::string_format("\nSEQRES %3d %c%5d ",row,chain_id,seq.size());
    core::index2 n=0;
    for(const core::chemical::Monomer & m : seq) {
        lines += " " + m.code3;
        ++n;
        if (n == 13) {
            ++row;
            lines += utils::string_format("\nSEQRES %3d %c%5d ", row, chain_id, seq.size());
            n = 0;
        }
    }
  }
  return lines;
}

static std::string prefixes[] = { "pdb", "PDB", "pdb", "" };
static std::string  sufixes[] = { ".ent", ".ent.gz", ".gz", ".pdb", ".PDB", ".pdb.gz", "" };

std::string find_pdb_file_name(const std::string &pdbCode, const std::string &pdbPath) {

  // --- Firstly, let's check if the PDB code is actually the file we are looking for
  if (utils::if_file_exists(pdbCode)) return pdbCode;

  std::string path(pdbPath);
  if (path.back() != utils::dir_separator) path += utils::dir_separator;

  // --- Also check if the PDB code is the file located at the given path
  if (utils::if_file_exists(path+pdbCode)) return path+pdbCode;

  std::string code_lo(pdbCode);
  utils::to_lower(code_lo);
  std::string code_up(pdbCode);
  utils::to_upper(code_up);

  const std::string subdir = code_lo.substr(1, 3) + utils::dir_separator;

  std::vector<std::string> tested;
  for (const std::string & p : prefixes) {
    for (const std::string & s : sufixes) {
      std::string name = path + p + code_lo + s;
      if (utils::if_file_exists(name)) return name;
      else tested.push_back(name);
      name = path + p + code_up + s;
      if (utils::if_file_exists(name)) return name;
      else tested.push_back(name);
      name = path + subdir + p + code_lo + s;
      if (utils::if_file_exists(name)) return name;
      else tested.push_back(name);
      name = path + subdir + p + code_up + s;
      if (utils::if_file_exists(name)) return name;
      else tested.push_back(name);
    }
  }

  static utils::Logger logger("find_pdb_file_name");
  logger << utils::LogLevel::WARNING
      << "Cannot find a pdb file for a given code: " + pdbCode + " at path: " + path
          + "\n\tThe following possibilities were tested:\n\t" + utils::to_string(tested," ");

  return "";
}

std::shared_ptr<Pdb> find_pdb(const std::string &pdb_code, const std::string &pdb_path,
    const bool if_parse_header,const bool if_first_model_only) {

  utils::Logger logs("find_pdb");
  std::string fname = find_pdb_file_name(pdb_code, pdb_path);
  logs << utils::LogLevel::INFO << pdb_code << " located in " << fname << "\n";

  return std::make_shared<Pdb>(fname, is_not_water, keep_all, if_parse_header, if_first_model_only);
}

std::string pdb_code_from_file_name(const std::string & code) {

  std::string code_(utils::basename(code));
  for (const std::string & s : sufixes) utils::replace_substring(code_, s, "");
  for (const std::string & p : prefixes) {
    if (code_.find(p, 0) == 0) utils::replace_substring(code_, p, "");
  }

  return code_;
}

bool is_pdb_code(const std::string & code) {

  if (code.length() != 4) return false;
  if (!isdigit(code[0])) return false;
  return true;
}

void write_pdb(const core::data::structural::Structure_SP structure, std::ostream & out) {

  for (auto a_it = structure->first_atom(); a_it != structure->last_atom(); ++a_it)
    out << (*a_it)->to_pdb_line() << "\n";
}

void write_pdb(const core::data::structural::Structure_SP structure, const std::string &file_name,
               const index4 model_id) {

  if (model_id == 0) {
    std::ofstream out(file_name);
    write_pdb(structure, out);
    out.close();
  } else {
    std::ofstream out(file_name, std::fstream::out | std::fstream::app);
    out << utils::string_format("MODEL   %6d\n", model_id);
    write_pdb(structure, out);
    out << "ENDMDL\n";
    out.close();
  }
}

void rototranslation_to_pdb(
        std::vector<calc::structural::transformations::Rototranslation, std::allocator<calc::structural::transformations::Rototranslation>> input, std::vector<std::string> & out_lines){
    std::string tmplt{"%9.6f %9.6f %9.6f       %8.5f"};
    for (const auto &rt :input){
        out_lines.push_back(utils::string_format(tmplt,rt.rot_x().x,rt.rot_x().y,rt.rot_x().z,rt.tr_after().x));
        out_lines.push_back(utils::string_format(tmplt,rt.rot_y().x,rt.rot_y().y,rt.rot_y().z,rt.tr_after().y));
        out_lines.push_back(utils::string_format(tmplt,rt.rot_z().x,rt.rot_z().y,rt.rot_z().z,rt.tr_after().z));
    }

}


} // ~ io
} // ~ data
} // ~ core

