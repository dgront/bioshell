/** \file seq_io.hh
 * @brief Provides I/O methods for the SEQ file format.
 *
 * The following example reads a SEQ file and prints the secondary structure in FASTA format.
 * \include ex_seq_io.cc
 */
#ifndef CORE_DATA_IO_seq_io_H
#define CORE_DATA_IO_seq_io_H

#include <string>
#include <memory>
#include <iostream>
#include <core/data/sequence/Sequence.hh>
#include <core/data/sequence/SecondaryStructure.hh>

namespace core {
namespace data {
namespace io {

/** @brief Reads in secondary structure from a file in SEQ format
 *
 * @param in_stream - input stream
 * @param header - header string of the sequence that will be created. The header will be used e.g. when the sequence
 * is printed in the FASTA format
 * @return newly created SecondaryStructure instance, containing the data obtained from the given stream
 */
core::data::sequence::SecondaryStructure_SP read_seq(std::istream & in_stream, const std::string & header = "");

/** @brief Reads in secondary structure from a file in SEQ format
 *
 * @param in_fname - input file name
 * @param header - header string of the sequence that will be created. The header will be used e.g. when the sequence
 * is printed in the FASTA format
 * @return newly created SecondaryStructure instance, containing the data obtained from the given file
 */
core::data::sequence::SecondaryStructure_SP read_seq(const std::string & in_fname, const std::string & header = "");

/** @brief Writes a secondary structure in SEQ format
 *
 * @param seq - SecondaryStructure object to write
 * @return string that represents SecondaryStructure object that can be write to file in SEQ format
 */
const std::string create_seq_string(const core::data::sequence::SecondaryStructure & seq);

/** @brief Writes a secondary structure in to file in a SEQ format
 *
 * @param seq - SecondaryStructure object to write to a file
 */
const void write_seq(const core::data::sequence::SecondaryStructure & ss, std::ostream & out);

}
}
}

#endif
