#include <core/index.hh>

#include <core/algorithms/trees/algorithms.hh>
#include <core/data/io/BlastXMLReader.hh>
#include <core/data/io/XML.hh>
#include <core/data/io/Hsp.hh>
#include <core/data/io/XMLElement.hh>

#include <utils/io_utils.hh>

namespace core {
namespace data {
namespace io {

/** @brief Visit a Hsp node of XML tree and appends it to hits vector
 */
struct Visit_node {

  std::vector<Hsp> & hits_; // --- reference to a vector where hits will be stored

  Visit_node(std::vector<Hsp> & hits) : hits_(hits) {}

  void operator()(std::shared_ptr<core::algorithms::trees::TreeNode<XMLElementData>> n) {

    if (n->element.name() == "Hsp") {
      auto xmlel = std::static_pointer_cast<XMLElement>(n);
      auto xmlel_root = std::static_pointer_cast<XMLElement>(n->get_root()->get_root());

      //--- Reads XML tags and put it into variables
      int len = xmlel_root->find_value<int>("Hit_len",-1);
      const std::string &hit_accession = xmlel_root->find_value("Hit_accession");
      const std::string &description = xmlel_root->find_value("Hit_def");
      int score = xmlel->find_value<int>("Hsp_score",-1);
      double expect = xmlel->find_value<double>("Hsp_evalue",1);
      int identities = xmlel->find_value<int>("Hsp_identity",-1);
      int positives = xmlel->find_value<int>("Hsp_positive",-1);
      int gaps = xmlel->find_value<int>("Hsp_gaps",-1);
      int query_start = xmlel->find_value<int>("Hsp_query-from",-1);
      int query_end = xmlel->find_value<int>("Hsp_hit-to",-1);
      int sbjct_start = xmlel->find_value<int>("Hsp_hit-from",-1);
      int sbjct_end = xmlel->find_value<int>("Hsp_hit-to",-1);
      const std::string & query = xmlel->find_value("Hsp_qseq");
      const std::string & sbjct = xmlel->find_value("Hsp_hseq");

      // --- Creating Hsp element
      Hsp hsp_el(len, description, hit_accession, score, expect, identities,
                 positives, gaps, query_start, query_end, sbjct_start, sbjct_end, query, sbjct);

      // --- Appending to hits vector
      hits_.push_back(hsp_el);
    }
  }
};

std::vector<std::vector<Hsp>> & BlastXMLReader::parse(const std::string & fname) {

  XML xxx;
  std::shared_ptr<XMLElement> root = xxx.load_data(fname);

  auto it = root->begin();
  while ((*it)->element.name() != "BlastOutput_iterations")
    ++it; // --- Visit branches until you find BlastOutput_iterations

  core::index2 iteration_counter = 0;
  for (const auto &v : **it) {
    if (v->element.name() == "Iteration") {
      std::vector<Hsp> iteration_hits;
      ++iteration_counter;
      core::algorithms::trees::depth_first_preorder((*it)->get_right(), Visit_node(iteration_hits));
      hits_.push_back(iteration_hits);
    }
  }

  return hits_;
}

}
}
}