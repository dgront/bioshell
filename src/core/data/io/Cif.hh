#ifndef CORE_DATA_IO_CIF_H
#define CORE_DATA_IO_CIF_H

#include <fstream>
#include <map>
#include <memory>
#include <string>
#include <vector>
#include <exception>

#include <core/index.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace core {
namespace data {
namespace io {

/// Exception thrown by a CIF parser
class CifParseError : public std::exception {
public:
  explicit CifParseError(const std::string &msg) : msg_("Parser error: " + msg) {}

  virtual const char *what() const throw() { return msg_.data(); }

private:
  std::string msg_;
};

/// Exception thrown by a CIF reader
class CifSectionTypeException : public std::exception {
public:
  virtual const char *what() const throw() { return "Bad CifSectionType."; }
};

/** @brief Represents a single section of a CIF block - either key - value or loop
 */
 class CifSection {
public:

  /// Constructor creates an empty block
  CifSection();

  /// Constructor creates an empty loop-type block
  CifSection(std::vector<std::string> loopLabels);

  /// Returns true if this block is the loop-type
  bool is_loop() const { return is_loop_; }

 /** @brief Access to a std::map that stores key-value pairs found in a block
  */
  const std::map<std::string, std::string> &values() const;

 /** @brief Access to a std::map that stores key-value pairs found in a loop-type block
  */
  const std::map<std::string, std::vector<std::string>> &loop() const;

  /** @brief Returns a value associated with a given key.
   *
   * If <code>key</code> does not match the key of any element in the container,
   * the function throws an <code>out_of_range</code>  exception.
   *
   * @param key - the key
   * @return associated value
   */
  const std::string & at(const std::string & key) const { return values_.at(key); }

  /** @brief Returns true if this block contains a given key associated with a value.
   *
   * @param key - the key
   * @return true if this block contains that key, false otherwise
   */
  bool has_key(const std::string & key) const { return values_.find(key) != values_.end(); }

  /** @brief Returns a vector of column labels if this block is a loop-type one.
   *
   * Otherwise, throws an exception
   * @return  column names
   */
  const std::vector<std::string> & loop_labels() const { return loop_labels_; }

  /** @brief Adds a key-value pair to this block
   * @param key - the key
   * @param value - the associated value
   * @return true if inserted
   */
  bool add_key_value(std::string key, std::string value);

  /** @brief Adds a table row to this block (only if this block is of loop-type)
   * @param values - a row of values
   * @return true if inserted
   */
  bool add_loop_row(const std::vector<std::string> & values);

private:
  bool is_loop_;
  std::map<std::string, std::string> values_;
  std::vector<std::string> loop_labels_;
  std::map<std::string, std::vector<std::string>> loopData_;
};

/// Prints a given CIF <code>data</code> block  nicely in CIF format
std::ostream & operator<<(std::ostream & out, const CifSection & sec);


/** @brief Represents a named  block of a CIF file
 *
 * @include ex_Cif.cc
 */
class CifBlock {
public:
  const std::string name; ///< Name of the data block

  /// Creates a new named block
  CifBlock(const std::string & name) : name(name) {}

  /// Returns the number of data blocks stored in the given CIF file
  core::index4 size() const { return sections_.size(); }

  /// Returns a block requested by its index
  std::shared_ptr<CifSection> get_block(core::index4 idx) const { return sections_.at(idx); }

  /// Returns const-iterator that points to the first block
  std::vector<std::shared_ptr<CifSection>>::const_iterator begin() const { return sections_.cbegin(); }

  /// Returns const-iterator that points behind the last block
  std::vector<std::shared_ptr<CifSection>>::const_iterator end() const { return sections_.cend(); }

  /// Adds a section to this block
  void push_back(std::shared_ptr<CifSection> sec) { sections_.push_back(sec); }

  /** @brief Returns a value associated with a given key.
   *
   * If <code>key</code> does not match the key of any element in the container,
   * the function throws an <code>out_of_range</code>  exception.
   *
   * @param key - the key
   * @return associated value
   */
  const std::string & at(const std::string & key) const;

  /** @brief Returns true if this block contains a given key associated with a value.
   *
   * @param key - the key
   * @return true if this block contains that key, false otherwise
   */
  bool has_key(const std::string &key) const;

private:
  std::vector<std::shared_ptr<CifSection>> sections_;
};

/** @brief Prints the content of this CIF data block to a stream (in CIF format, of course)
 * @param out - output stream
 * @param cif - CIF data
 * @return output stream
 */
std::ostream & operator<<(std::ostream & out, const CifBlock & data_block);

/** @brief Simple CIF file reader.
 *
 * @include ex_Cif.cc
 */
class Cif {
public:

  /** @brief Constructor opens CIF file and reads its content
   *
   * @param filename - input file name
   */
  Cif(const std::string & filename);

  /// Returns the number of data blocks stored in the given CIF file
  core::index4 size() const { return blocks_.size(); }

  /// Returns a block requested by its index
  std::shared_ptr<CifBlock> get_block(const std::string & block_name) const { return blocks_.at(block_name); }

  /// Returns const-iterator that points to the first block
  std::map<std::string, std::shared_ptr<CifBlock>>::const_iterator begin() const { return blocks_.cbegin(); }

  /// Returns const-iterator that points behind the last block
  std::map<std::string, std::shared_ptr<CifBlock>>::const_iterator end() const { return blocks_.cend(); }

private:
  std::ifstream ifs_;
  std::map<std::string, std::shared_ptr<CifBlock>> blocks_;
  utils::Logger logger;

  void read_root();
  void handle_semicolon(std::string & token);
  bool read_section(std::shared_ptr<CifBlock> data_block);
  bool read_loop(std::shared_ptr<CifBlock> block);
  void finish_value(std::string &value);
};

typedef std::shared_ptr<CifSection> CifSection_SP;

/** @brief Prints the content of this CIF data to a stream (in CIF format, of course)
 * @param out - output stream
 * @param cif - CIF data
 * @return output stream
 */
std::ostream & operator<<(std::ostream & out, const Cif & cif);

}
}
}

/**
 * @example  ex_Cif.cc
 */

#endif
