/** \file mmCif.hh
 * @brief
 */
#ifndef CORE_DATA_IO_mmCif_H
#define CORE_DATA_IO_mmCif_H

#include <string>
#include <regex>
#include <algorithm>
#include <string>
#include <vector>
#include <memory>
#include <map>
#include <functional>
#include <iostream>

#include <core/data/basic/Vec3.hh>
#include <core/data/basic/Vec3Cubic.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/Residue.fwd.hh>
#include <core/data/sequence/Sequence.hh>
#include <core/data/io/PdbField.hh>

#include <utils/Logger.hh>
#include <core/index.hh>
#include <utils/string_utils.hh>

#include <utils/Logger.hh>
#include <core/data/io/Cif.hh>

namespace core {
namespace data {
namespace io {

class mmCif {
public:
    /** \brief Constructor reads and parses mmCIF file but does not create any core::data::structural::Structure object
     * Uses class Cif to read the whole file and stores the info as a object of this class inside the mmCif object
     *
     * Now we can ask the reader to create a structure (i.e. an object that represents a protein) from the first (indexed by 0) model stored in the PDB file.
     * @code
     * core::data::structural::Structure_SP backbone = reader.create_structure(0);
     * @endcode
     *
     * @param fname - input file name or PDB data itself - as a multiline string
     * @param first_model_only - load only first model from a file and stop reading
     */
    mmCif(const std::string & fname, const bool first_model_only = false);

    /** @brief Returns the PDB code of a deposit : four-character string.
     *
     * @returns four-character string, e.g. 2AZA, 2GB1, 1HLB etc
     */
    std::string pdb_code() const;

    /// Returns the number of models in the PDB deposit that has been loaded
    core::index2 count_models() const ;

    /** @brief Creates all Structure objects that are available in this PDB stream
      *
      * @param structures - vector to hold created structures
      */
    void create_structures(std::vector<core::data::structural::Structure_SP> & structures);
    /** @brief Creates a Structure object from a model stored in the PDB data structure
     *
     * @param which_model - model index; starts from 0
     * @returns pointer to the newly created Structure object
     */
    core::data::structural::Structure_SP create_structure(const core::index2 which_model);

private:
    Cif reader_;
    static utils::Logger logger;
    std::string fname_;

};

}
}
}

#endif
