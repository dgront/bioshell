#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <memory>

#include <core/data/sequence/SecondaryStructure.hh>
#include <utils/Logger.hh>
#include <utils/string_utils.hh>

#include <core/data/io/seq_io.hh>

namespace core {
namespace data {
namespace io {

using core::data::sequence::Sequence;

static utils::Logger seq_io_logs("seq_io");


core::data::sequence::SecondaryStructure_SP read_seq(std::istream & in_stream, const std::string & header) {

    std::string line;
    std::string ss_str, icodes, seq_str;
    std::vector<core::index2> indexes;
    core::index4 resnum;
    char icode;
    std::string code3;
    std::string chain;
    char ss;
//    double occ;
    while (std::getline(in_stream, line)) {
        if (line.length() < 4) continue;
        if (line[0] == '#') continue;
        resnum = utils::from_string<core::index4>(line.substr(0, 5));
        indexes.push_back(resnum);
        icode = line[6];
        icodes.push_back(icode);
        code3 = line.substr(8, 3);
        chain = line[12];
        ss = line[15];
        //     occ=utils::from_string<double>(line.substr(18,4));
        seq_str += core::chemical::Monomer::get(code3).code1;
        switch (ss) {
            case '1' :
                ss_str += "C";
                break;
            case '2' :
                ss_str += "H";
                break;
            case '3' :
                ss_str += "C";
                break;
            case '4' :
                ss_str += "E";
                break;
            default:
                seq_io_logs << utils::LogLevel::SEVERE << "Unknown secondary structure type: " << ss
                            << " switched to coil!\n";
                ss_str += "C";
        }
    }

    core::data::sequence::SecondaryStructure_SP seq = std::make_shared<core::data::sequence::SecondaryStructure>(header,
                                                                                                                 seq_str,
                                                                                                                 indexes[0],
                                                                                                                 ss_str);
    seq->set_chain_id(chain);
    for (core::index2 i = 0; i < indexes.size(); ++i) {
    seq->set_icode(i, icodes[i]);
    seq->set_index(i,indexes[i]);
}
return seq;

}

core::data::sequence::SecondaryStructure_SP read_seq(const std::string & in_file, const std::string & header) {
        std::ifstream inf(in_file);
        return read_seq(inf, header);
    }

const std::string create_seq_string(const core::data::sequence::SecondaryStructure & seq){
        std::stringstream out;
        for (core::index4 i =0;i<seq.length();i++) {
            out << utils::string_format("%5d ",seq.indexes(i));
            out << seq.icodes(i)<< " "<<seq.get_monomer(i).code3;
            char ss = seq.ss(i);

            core::index1 ss_num=5;
            switch(ss) {
                case 'C' :
                    ss_num = 1;
                    break;
                case 'H' :
                    ss_num = 2;
                    break;
                case 'E' :
                    ss_num = 4;
                    break;
            }
            out << utils::string_format("%5d %s" ,ss_num,seq.chain_id().c_str()) <<"\n";

        }
        return out.str();
}

const void write_seq(const core::data::sequence::SecondaryStructure & ss, std::ostream & out){
    out<< create_seq_string(ss);
}

}
}
}
