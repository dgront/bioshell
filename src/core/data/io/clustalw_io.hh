/** \file clustalw_io.hh
 * @brief Provides various I/O methods for the ClustalW file format
 */
#ifndef CORE_DATA_IO_clustalw_io_H
#define CORE_DATA_IO_clustalw_io_H

#include <string>
#include <vector>
#include <memory>

#include <core/data/sequence/Sequence.hh>

namespace core {
namespace data {
namespace io {

/** @brief Reads a file in the ClustalW format.
 *
 * The method creates Sequence objects from the data and places them on a given vector
 *
 * The following example reads a multiple sequence alignment and writes a sequence profile
 * @example ap_SequenceProfile.cc
 *
 * @param file_name - name of the input file
 * @param sink - where to store the newly created sequences
 * @param fix_aa - if true, this function automatically convert all 'B' and 'Z' characters to 'X' (unknown amino acid)
 * @return a reference to the <code>sink</code> vector
 */
std::vector<core::data::sequence::Sequence_SP> &read_clustalw_file(const std::string &file_name,
          std::vector<core::data::sequence::Sequence_SP> &sink, bool fix_aa = false);

/** @brief Reads a file in the ClustalOmega format.
 *
 * The method creates Sequence objects from the data and places them on a given vector
 *
 * @param file_name - name of the input file
 * @param sink - where to store the newly created sequences
 * @param fix_aa - if true, this function automatically convert all 'B' and 'Z' characters to 'X' (unknown amino acid)
 * @return a reference to the <code>sink</code> vector
 */
std::vector<core::data::sequence::Sequence_SP> &read_clustalo_file(const std::string &file_name,
            std::vector<core::data::sequence::Sequence_SP> &sink, bool fix_aa = false);

/** @brief Writes a Multiple Sequence Alignment in the ClustalOmega format.
 *
 * @param data - a vector of aligned sequences
 * @param file_name - name of the input file
 * @param max_header_length maximum length of a sequence header; longer string will be truncated
 */
void write_clustalo_file(const std::vector<core::data::sequence::Sequence_SP> &data, const std::string &out_fname,
                         core::index2 max_header_length = 120);

/** @brief Writes a Multiple Sequence Alignment in the ClustalOmega format.
 *
 * @param data - a vector of aligned sequences
 * @param out - output stream
 * @param max_header_length maximum length of a sequence header; longer string will be truncated
 */
void write_clustalo_file(const std::vector<core::data::sequence::Sequence_SP> &data, std::ostream &out,
                         core::index2 max_header_length = 120);
}
}
}
/**
 * @include ap_SequenceProfile.cc
 */
#endif
