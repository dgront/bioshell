#ifndef CORE_DATA_IO_BXMLR_H
#define CORE_DATA_IO_BXMLR_H

#include <vector>
#include <string>

#include <core/data/io/Hsp.hh>

namespace core {
namespace data {
namespace io {

/** @brief Parses a XML file produced by blast or psiblast program and provides a vector of hits
 */
class BlastXMLReader {
public:

  /// Default constructor
  BlastXMLReader() {};

  /** @brief Parse data XML file and returns vector of Blast iterations.
   * Every iteration is a vector of hits for that iteration
   */
  std::vector<std::vector<Hsp>> & parse(const std::string & fname);

private:
  std::vector<std::vector<Hsp>> hits_;
};


}
}
}
#endif

