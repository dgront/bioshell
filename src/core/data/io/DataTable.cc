#include <string>
#include <iostream>
#include <vector>
#include <stdexcept>

#include <core/algorithms/basic_algorithms.hh>
#include <utils/string_utils.hh>
#include <core/data/io/DataTable.hh>
#include <utils/Logger.hh>
#include <utils/io_utils.hh>

namespace core {
namespace data {
namespace io {

/// Appends a string to this table row
template <>
inline TableRow & TableRow::push_back(const std::string & s) { data.push_back(s); return *this; }


std::ostream &operator<<(std::ostream &out, const TableRow &row) {

  for (core::index2 i = 0; i < row.size() - 1; ++i) out << row[i] << " ";
  out << row.back();

  return out;
}

void DataTable::to_json(const std::vector<bool> &need_quotes, std::ostream &out) const {

  out << "[\n";
  for(index4 i_row=0; i_row<size();++i_row) {
    const TableRow & row = (*this)[i_row];
    out << "\t[";
    if (need_quotes[0]) out << "'" << row[0] << '"';
    else out  << row[0];

    for (index2 i_entry = 1; i_entry < row.size(); ++i_entry)
      if (need_quotes[i_entry])
        out << ",\"" << row[i_entry] << '"';
      else
        out << ',' << row[i_entry];
    out << ((i_row<size()-1) ? "],\n" : "]\n");
  }
  out << "]\n";
}

void DataTable::load(std::istream & source) {

  std::string line;
  while (std::getline(source, line)) {
    if (line.length() < 1) continue;
    utils::trim(line);
    if (line[0] == '#') continue;
    emplace_back(TableRow{});
    utils::split(line, back().data);
  }
  logger << utils::LogLevel::FILE << "found " << size() << " rows\n";
}

void DataTable::load(const std::string & fname) {

  if(!utils::if_file_exists(fname)) {
    std::string msg = "Can't find a file: " + fname + "!\n";
    logger<<utils::LogLevel::SEVERE<<msg;
    throw std::runtime_error(msg);
  }

  logger << utils::LogLevel::FILE << "loading tabular data from " << fname << "\n";
  std::ifstream in(fname);
  if (!in) std::runtime_error("Can't read from the file: "+fname);
  load(in);
  in.close();
}

}
}
}
