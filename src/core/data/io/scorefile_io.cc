#include <core/data/io/scorefile_io.hh>
#include <utils/string_utils.hh>
#include <utils/io_utils.hh>

namespace core {
namespace data {
namespace io {

utils::Logger logger("scorefile_io");

bool is_header_line(const std::vector<std::string> & tokens) {

  index2 n_val = 0;
  for(const std::string & s:tokens)
    if ((utils::is_integer(s))||(utils::is_integer(s)))
      ++n_val;

  return n_val < 3;
}

std::shared_ptr<NamedDataTable> read_scorefile(std::istream &in_stream) {

  std::shared_ptr<NamedDataTable> out = std::make_shared<NamedDataTable>();
  bool got_header = false; // --- flag to parse a header line only once
  std::string line;
  std::vector<std::string> storage;
  while (std::getline(in_stream, line)) {
    storage.clear();
    if (line.length() < 1) continue;
    utils::trim(line);
    if (!utils::has_prefix(line,"SCORE:")) continue;
    utils::split_into_strings(line, storage);
    // --- first we check if this is a header line
    if((!got_header) && (is_header_line(storage))) {
      for (index1 i = 1; i < storage.size(); ++i) out->column_name(i - 1, storage[i]);
      got_header = true;
    }
    else {
      TableRow row;
      for (index1 i = 1; i < storage.size(); ++i) row.push_back(storage[i]);
      out->push_back(row);
    }
  }

  return out;
}

std::shared_ptr<NamedDataTable> read_scorefile(const std::string & in_fname) {

  if(!utils::if_file_exists(in_fname)) {
    std::string msg = "Can't find a file: " + in_fname + "!\n";
    logger<<utils::LogLevel::SEVERE<<msg;
    throw std::runtime_error(msg);
  }

  logger << utils::LogLevel::FILE << "loading tabular data from " << in_fname << "\n";
  std::ifstream in(in_fname);
  if (!in) std::runtime_error("Can't read from the file: "+in_fname);
  auto out = read_scorefile(in);
  in.close();

  return out;
}

}
}
}

