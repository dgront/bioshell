/** @brief Provides methods to read a number of models (aka decoys) from PDB files
 *
 * All models must be composed of the same number of atoms. By default the methods read only C-alpha atoms.
 * This can be changed by passing an appropriate PDB-predicate as an argument
 */
#ifndef CORE_DATA_IO_load_decoys_H
#define CORE_DATA_IO_load_decoys_H

#include <string>
#include <vector>
#include <memory>
#include <map>
#include <iostream>
#include <vector>

#include <core/data/basic/Vec3.hh>
#include <core/data/io/Pdb.hh>

namespace core {
namespace data {
namespace io {

void read_decoys(const std::vector<std::string> & fnames, const core::index4 n_atoms,
    std::vector<core::data::basic::Coordinates_SP> & destination, std::vector<std::string> & model_names,
    const PdbLineFilter & predicate = is_ca);

void read_decoys(const std::vector<std::string> & fnames, const core::index4 n_atoms,
     std::vector<core::data::basic::Coordinates_SP> & destination, std::vector<std::string> & model_names,
     const PdbLineFilter & predicate, std::vector<core::index2> & decoys);

void read_decoys(const std::string & listfile, const core::index4 n_atoms,
     std::vector<core::data::basic::Coordinates_SP> & destination, std::vector<std::string> & model_names,
     const PdbLineFilter & predicate = is_ca);
}
}
}

#endif
