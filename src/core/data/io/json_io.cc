#include <regex>

#include <core/data/io/json_io.hh>
#include <core/algorithms/trees/algorithms.hh>

namespace core {
namespace data {
namespace io {

utils::Logger json_io_logs("json_io");

std::ostream & operator<<(std::ostream & out, const JsonValue & value) {

  out << '"' << value.key() << '"' << " : ";
  if (value.need_quotes()) {
    if (value.string_value().front() != '"') out << '"';
    out << value.string_value();
    if (value.string_value().back() != '"') out << '"';
  }
  else out << value.string_value();
  return out;
}

std::ostream & operator<<(std::ostream & out, const JsonArray & value) {

  if(value.key().size()>0)
    out << '"' << value.key() << '"' << " : " << value.string_value();
  else
    out << value.string_value();

  return out;
}

std::ostream &operator<<(std::ostream &out, const JsonValue_SP &value) {

  out << *value;
  return out;
}


std::ostream &operator<<(std::ostream &out, const JsonNode_SP node) {

  if (node->count_branches() == 0) out << std::string(core::algorithms::trees::depth(node) * 2, ' ') << node->element;
  else {
    std::string padding(core::algorithms::trees::depth(node) * 2, ' ');
    if (node->get_root() != nullptr)
      out << padding << '"' << node->element->key() << '"' << " : {\n";
    else out << " {\n";
    out << std::string(core::algorithms::trees::depth(node->get_branch(0)) * 2, ' ') << node->get_branch(0);
    for (index2 i = 1; i < node->count_branches(); ++i) out << "," << node->get_branch(i);
    out << "\n" << padding << "}\n";
  }
  return out;
}

JsonNode_SP read_json(const std::string & json) {

  size_t begin = 0,end = json.size()-1;
  while (json[begin] == ' ') ++begin; // --- trim white characters on both ends
  while (json[end] == ' ') --end;
  if (json[begin] != '{') { // --- trim starting '}'
    json_io_logs << utils::LogLevel::WARNING << "JSON expression must begin with '{' character";
    return nullptr;
  } else begin++;

  const std::regex level("\\\"([^\\\"]+?)\\\"\\s*?:\\s*?\\{");
  const std::regex token("\\\"(.+?)\\\"\\s*?:\\s*?(\\S.+?)[,|\\}]");
  std::smatch m, m2;
  auto i_pos = json.cbegin() + begin;
  auto i_end = json.cend(); // - json.size() + end;

  bool go_on = true;
  index2 i_node = 0;
  std::vector<JsonNode_SP> roots;
  roots.push_back(std::make_shared<JsonNode>(std::make_shared<JsonValue>(""), ++i_node));
  while (go_on) {
    regex_search(i_pos, i_end, m, level);
    regex_search(i_pos, i_end, m2, token);
    if (m2.empty()) break;
    if ((!m.empty()) && m.position(0) <= m2.position(0)) {  // here we open a new level
      JsonNode_SP n = std::make_shared<JsonNode>(std::make_shared<JsonValue>(m[1]), ++i_node);
      roots.back()->add_branch(n);
      roots.push_back(n);
      i_pos = m.suffix().first - 1;
    } else {
      JsonNode_SP n = std::make_shared<JsonNode>(std::make_shared<JsonValue>(m2[1], m2[2]), ++i_node);
      roots.back()->add_branch(n);
      i_pos = m2.suffix().first - 1;
      if (*(m2.suffix().first - 1) == '}') { // --- here we should close the most recent level
        roots.pop_back();
      }
    }
  }

  return roots.front();
}

const std::string JsonArray::string_value() const {

  std::stringstream out;
  if(values_.size()==0) return "[]";
  out << "[" << values_[0];
  for (core::index4 i = 1; i < values_.size(); ++i) out << ", " << values_[i];
  out << "]";

  return out.str();
}

void JsonArray::push_back(const bool need_quotes, const std::string &val) {
  if (need_quotes) values_.push_back("\"" + utils::to_string(val) + "\"");
  else values_.push_back(utils::to_string(val));
}


JsonNode_SP create_json_node(const JsonValue_SP & value) { return std::make_shared<JsonNode>(value,0); }

JsonNode_SP create_json_node() { return std::make_shared<JsonNode>(0); }

template <>
JsonNode_SP create_json_node<std::string>(const std::string & key, const std::string & value) {

  return std::make_shared<JsonNode>(std::make_shared<JsonValue>(key,value),0);
}

}
}
}

