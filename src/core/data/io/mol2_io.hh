/**@file mol2_io.hh Provides I/O methods for MOL2 file format
 *
 */
#ifndef CORE_DATA_IO_mol2_io_H
#define CORE_DATA_IO_mol2_io_H

#include <string>
#include <iostream>
#include <memory>

#include <core/chemical/PdbMolecule.hh>
#include <utils/Logger.hh>

namespace core {
namespace data {
namespace io {

/** @brief Reads a molecule from a file in the TRIPOS' MOL2 format
 * @param file_name - the name of an input file
 * @return a pointer to a newly created molecule object
 */
core::chemical::PdbMolecule_SP read_mol2(const std::string & file_name);

/** @brief Reads a molecule from a file in the TRIPOS' MOL2 format
 * @param input - the input stream
 * @return a pointer to a newly created molecule object
 */
core::chemical::PdbMolecule_SP read_mol2(std::istream & input);

/** @brief Writes a molecule into a file in the TRIPOS' MOL2 format
 * @param molecule - molecule to be written
 * @param file_name - the output file name
 * @return a pointer to a newly created molecule object
 */
void write_mol2(const core::chemical::PdbMolecule_SP molecule, const std::string & file_name);

/** @brief Writes a molecule into a file in the TRIPOS' MOL2 format
 * @param molecule - molecule to be written
 * @param out - the output stream
 */
void write_mol2(const core::chemical::PdbMolecule_SP molecule, std::ostream & out);

}
}
}
#endif
