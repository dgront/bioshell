/** \file seq_io.hh
 * @brief Provides I/O methods for Rosetta's score file
 *
 */
#ifndef CORE_DATA_IO_scorefile_io_HH
#define CORE_DATA_IO_scorefile_io_HH

#include <string>
#include <memory>
#include <iostream>
#include <core/data/io/NamedDataTable.hh>

namespace core {
namespace data {
namespace io {

/** @brief Reads a scorefile
 *
 */
std::shared_ptr<NamedDataTable> read_scorefile(std::istream &in_stream);

/** @brief Reads a Rosetta's score file.
 *
 * The first column, which according to the score file format is aways "SCORE:", is neglected
 *
 * @param in_fname - input file name
 * @return newly created NamedDataTable instance, containing the scores from a given file
 */
std::shared_ptr<NamedDataTable> read_scorefile(const std::string & in_fname);

}
}
}

/**
 * \example ex_seq_io.cc
 */
#endif
