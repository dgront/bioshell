/** \file hssp_io.hh
 * @brief provides a HSSP file format reader
 */
#ifndef CORE_DATA_IO_Hssp_H
#define CORE_DATA_IO_Hssp_H

#include <core/index.hh>
#include <utils/string_utils.hh>

#include <core/data/sequence/Sequence.hh>

namespace core {
namespace data {
namespace io {

/** @brief Defines an insertion that may occur in a multiple sequence alignment, stored in a HSSP file.
 */
struct HsspInsertion {
  core::index2 i_sequence;          ///< index of a sequence where this insertion occurred  (from 0)
  core::index2 i_pos;               ///< index of a sequence position - insertion location (from 0)
  std::string sequence_fragment;    ///< inserted sequence fragment with two flanking residues given in small-caps
};

/** @brief Reads a file in the HSSP format.
 *
 * The method creates Sequence objects from the data and places them on a given vector
 *
 * The following example reads a multiple sequence alignment (MSA) and writes a sequence profile:
 * @example ap_SequenceProfile.cc
 *
 * Note: this example cn process also MSA in FASTA and in ClustalW formats
 *
 * @param file_name - name of the input file
 * @param sink - where to store the newly created sequences
 * @param to_uppercase - if true, this function automatically uppercase all lower-case characters;
 *  such lower-case letters are used by the HSSP file format to denote insertion spots
 * @param fix_aa - if true, this function automatically convert all 'B' and 'Z' characters to 'X' (unknown amino acid)
 * @return a reference to the <code>sink</code> vector
 */
std::vector<std::shared_ptr<core::data::sequence::Sequence>> & read_hssp_file(const std::string & file_name,
    std::vector<std::shared_ptr<core::data::sequence::Sequence>> & sink, bool to_uppercase = true, bool fix_aa = false);

/** @brief Reads a file in the HSSP format.
 *
 * The method creates Sequence objects from the data and places them on a given vector
 *
 * @param input -  the input stream of HSSP-formatted data
 * @param sink - where to store the newly created sequences
 * @param to_uppercase - if true, this function automatically uppercase all lower-case characters;
 *  such lower-case letters are used by the HSSP file format to denote insertion spots
 * @param fix_aa - if true, this function automatically convert all 'B' and 'Z' characters to 'X' (unknown amino acid)
 * @return a reference to the <code>sink</code> vector
 */
std::vector<std::shared_ptr<core::data::sequence::Sequence>> & read_hssp_file(std::istream & input,
    std::vector<std::shared_ptr<core::data::sequence::Sequence>> & sink, bool to_uppercase = true, bool fix_aa = false);
}
}
}

#endif