#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <utils/string_utils.hh>
#include <core/data/io/Pdb.hh>
#include <core/data/io/load_decoys.hh>
#include <core/data/basic/Vec3.hh>

#include <utils/Logger.hh>
#include <utils/io_utils.hh>

namespace core {
namespace data {
namespace io {

utils::Logger load_decoys_logger("load_decoys");

void read_decoys(const std::vector<std::string> & fnames, const core::index4 n_atoms,
    std::vector<core::data::basic::Coordinates_SP> & destination,
    std::vector<std::string> & model_names, const PdbLineFilter & predicate) {

  load_decoys_logger << utils::LogLevel::FILE << "Reading from " << fnames.size() << " files\n";
  for (const std::string & fname : fnames) {
    load_decoys_logger << utils::LogLevel::FILE << "Reading " << fname << "\n";
    std::ifstream in(fname);
    model_names.push_back(fname+"-1"); // --- insert the name of the first model

    // --- Read the very first frame
    core::data::basic::Coordinates_SP frame = std::make_shared<core::data::basic::Coordinates>();
    frame->resize(n_atoms);
    index4 nc = core::data::io::Pdb::read_coordinates(in, *frame, false, predicate);
    destination.push_back(frame);

    // --- Read all the other frames
    index4 iframe = 1;
    while (nc != 0) {
      ++iframe;
      core::data::basic::Coordinates_SP frame = std::make_shared<core::data::basic::Coordinates>();
      frame->resize(n_atoms);
      nc = core::data::io::Pdb::read_coordinates(in, *frame, false, predicate);
      if (nc == n_atoms) {
        destination.push_back(frame);
        model_names.push_back(fname + "-" + utils::to_string(iframe));
      }
    }
    if (iframe == 1) { // --- if there is just one model from a file, remove the index from the end of its name
      model_names.pop_back();
      model_names.push_back(fname);
    }
  }

  load_decoys_logger << utils::LogLevel::FILE << destination.size() << " models found in total " << "\n";
}

void read_decoys(const std::vector<std::string> & fnames, const core::index4 n_atoms,
     std::vector<core::data::basic::Coordinates_SP> & destination, std::vector<std::string> & model_names,
     const PdbLineFilter & predicate, std::vector<core::index2> & decoys) {

  load_decoys_logger << utils::LogLevel::FILE << "Reading from " << fnames.size() << " files\n";
  for (const std::string & fname : fnames) {
    load_decoys_logger << utils::LogLevel::FILE << "Reading " << fname << "\n";
    std::ifstream in(fname);
    std::string line;
    core::index4 iframe = 1;
    core::index4 nc = n_atoms;
    
    // --- Read all frames and save only chosen
    if (decoys.size() != 0) {
      for (unsigned int i = 0; i < decoys.size(); ++i) {
        nc = n_atoms;
        while (nc != 0) {
	  if (iframe != decoys[i]) {
            nc = 0;
    	    std::getline(in, line);
	    while(line.compare(0, 6, "ENDMDL")!=0) {
		std::getline(in, line);
		++nc;
	    }
	  } else {
            core::data::basic::Coordinates_SP frame = std::make_shared<core::data::basic::Coordinates>();
	    frame->resize(n_atoms);
    	    nc = core::data::io::Pdb::read_coordinates(in, *frame, false, predicate);		//nc=n_atoms
            destination.push_back(frame);
            model_names.push_back(utils::to_string(iframe));
            nc = 0;
	  }
	  ++iframe;
        }
      }
    }
  }
  load_decoys_logger << utils::LogLevel::FILE << destination.size() << " models printed in total " << "\n";
}

void read_decoys(const std::string & listfile, const core::index4 n_atoms,
    std::vector<core::data::basic::Coordinates_SP> & destination,
    std::vector<std::string> & model_names, const PdbLineFilter & predicate) {

  read_decoys(utils::read_listfile(listfile), n_atoms, destination, model_names, predicate);
}

}
}
}





