#include <core/data/io/Hsp.hh>
#include <core/data/sequence/sequence_utils.hh>

namespace core {
namespace data {
namespace io {

const std::string Hsp::output_header = "       hit sequence ID    len score gaps  gap% ident ident%    evalue  qpos tpos sequence";

Hsp::Hsp() {

  length_ = 0;
  description_ = "";
  hit_accession_ = "";
  score_ = 0;
  expect_ = 0.0;
  identities_ = 0;
  positives_ = 0;
  gaps_ = 0;
  query_start_ = 0;
  query_end_ = 0;
  sbjct_start_ = 0;
  sbjct_end_ = 0;
  query_ = "";
  sbjct_ = "";
}

Hsp::Hsp(const Hsp &h) {

  length_ = h.length_;
  description_ = h.description_;
  hit_accession_ = h.hit_accession_;

  score_ = h.score_;
  expect_ = h.expect_;
  identities_ = h.identities_;
  positives_ = h.positives_;
  gaps_ = h.gaps_;
  query_start_ = h.query_start_;
  query_end_ = h.query_end_;
  sbjct_start_ = h.sbjct_start_;
  sbjct_end_ = h.sbjct_end_;
  query_ = h.query_;
  sbjct_ = h.sbjct_;
}

Hsp::Hsp(index2 len, const std::string &hit_description, const std::string &hit_accession,
         index2 score, double expect, index2 identities,
         index2 positives, index2 gaps, index2 query_start, index2 query_end, index2 sbjct_start,
         index2 sbjct_end, const std::string &query, const std::string &sbjct) {


  length_ = len;
  description_ = hit_description;
  hit_accession_ = hit_accession;

  score_ = score;
  expect_ = expect;
  identities_ = identities;
  positives_ = positives;
  gaps_ = gaps;
  query_start_ = query_start;
  query_end_ = query_end;
  sbjct_start_ = sbjct_start;
  sbjct_end_ = sbjct_end;
  query_ = query;
  sbjct_ = sbjct;
}

index2 Hsp::n_aligned() const {

  index2 cnt = 0;
  for (int i = 0; i < query_.size(); ++i)
    if ((query_[i] != '-') && (query_[i] != '_') && (sbjct_[i] != '-') && (sbjct_[i] != '-')) ++cnt;

  return cnt;
}

std::ostream &operator<<(std::ostream &out, const Hsp &hsp) {

  std::string line = utils::string_format("[%27s] %5d %5d %4d (%3d%%) %4d (%3d%%) %10.2e %4d %4d ",
      hsp.hit_accession().substr(0, 27).c_str(), hsp.length(), hsp.score(), hsp.gaps(),
      int(hsp.gaps() * 100.0 / hsp.query().size()), hsp.n_identical(),
      int(hsp.n_identical() * 100.0 / hsp.query().size()), hsp.expect(), hsp.query_start(), hsp.sbjct_start());
  std::string g(hsp.query_start() - 1, '-');
  out << line << g << hsp.sbjct();

  return out;
}

}
}
}

