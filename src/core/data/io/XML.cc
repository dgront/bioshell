#include <string>
#include <vector>
#include <memory>
#include <stack>
#include <regex>
#include <iostream>
#include <stdexcept>

#include <utils/Logger.hh>
#include <core/data/io/XML.hh>
#include <core/data/io/XMLElement.hh>
#include <utils/exit.hh>

namespace core {
namespace data {
namespace io {

static const std::regex extract_tag_name_open("< *([a-zA-Z0-9_\\-]+)[ >]");
static const std::regex extract_tag_name_close("<. *([a-zA-Z0-9_\\-]+)[ >]");
static const std::regex extract_key_value(" +([a-zA-Z0-9_\\-]+)=\"(.*?)\"");

std::shared_ptr<XMLElement> XML::load_data(const std::string & fname) {

  utils::load_text_file(fname, text);
  logger.log(utils::LogLevel::FILE, text.length(), " characters loaded from ", fname, "\n");
  utils::trim(text);

  return parse();
}

std::shared_ptr<XMLElement> XML::load_data(std::istream & input) {
  text.assign((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());
  logger.log(utils::LogLevel::FILE, text.length(), " characters loaded an input stream\n");
  utils::trim(text);

  return parse();
}

std::string XML::extract_doctype() {

  size_t start = 0;
  std::string doctype;
  start = text.find("<!DOCTYPE");
  if (start != std::string::npos) {
    size_t stop = text.find(">", start);
    doctype = text.substr(start,stop);
    text.erase(start, stop - start + 1);
  }

  return doctype;
}

std::string XML::extract_declaration() {

  size_t start = 0;
  std::string declaration;
  start = text.find("<?xml");
  if (start != std::string::npos) {
    size_t stop = text.find("?>", start);
    declaration = text.substr(start,stop);
    text.erase(start, stop - start + 2);
  }

  return declaration;
}

void XML::remove_comments() {

  size_t start = 0;
  bool flag = false;
  do {
    flag = false;
    start = text.find("<!--");
    if (start != std::string::npos) {
      flag = true;
      size_t stop = text.find("-->", start);
      text.erase(start, stop - start + 3);
    }
  } while (flag);
}

std::shared_ptr<XMLElement> XML::parse() {

  remove_comments();
  extract_declaration();
  extract_doctype();

  std::shared_ptr<XMLElement> root = nullptr;
  std::size_t tag_begins = text.find('<');
  std::size_t tag_ends = 0;
  std::string ttag;
  int node_id = -1;
  std::stack<std::shared_ptr<XMLElement>> roots;
  std::size_t prev_tag_ends = 0;
  while (tag_begins != std::string::npos) {   // --- Repeat until the end of the string
    prev_tag_ends = tag_ends;                 // --- store the position where the most recently closed tag ended
    tag_ends = text.find('>', tag_begins);    // --- Find the position where the open tag ends
    std::string tag = text.substr(tag_begins, tag_ends - tag_begins + 1); // --- Extract the tag substring
    bool separated = (text[tag_ends - 1] == '/') ? true : false;  // --- is this a self-closing tag, e.g. <sth />
    bool closing = (text[tag_begins + 1] == '/') ? true : false;  // --- is this a closing tag, e.g. </sth>

    if (closing) {                       // --- If this is a 'closing tag', close the open tag from the top of the stack
      std::smatch base_match;
      if (std::regex_search(tag, base_match, extract_tag_name_close)) {
        if (base_match.size() >= 2) {
          ttag = base_match[1].str();
          if(roots.size() == 0) {
            return root;
          }
          roots.top()->text(text.substr(prev_tag_ends + 1, tag_begins - prev_tag_ends - 1));
          if (roots.top()->name().compare(ttag))
            throw std::runtime_error(
                "The most recently opened tag: " + roots.top()->name() + " now is closed by " + ttag
                    + " which is not allowed!");
          roots.pop();
        } else {
          logger.log(utils::LogLevel::SEVERE, "can't extract element's name from: ", tag, "\n");
          throw std::runtime_error("can't extract element's name from: " + tag);
        }
      } else {
        logger.log(utils::LogLevel::SEVERE, "can't extract element's name from: ", tag, "\n");
        throw std::runtime_error("can't find element's name in: " + tag);
      }
    } else {
      std::smatch base_match;
      if (std::regex_search(tag, base_match, extract_tag_name_open)) {
        if (base_match.size() >= 2) {
          ttag = base_match[1].str();
        } else {
          logger.log(utils::LogLevel::SEVERE, "can't extract element's name from: ", tag, "\n");
          throw std::runtime_error("can't extract element's name from: " + tag);
        }
        logger.log(utils::LogLevel::FINE, "New element found: ", ttag, "\n");
        std::shared_ptr<XMLElement> n = std::make_shared<XMLElement>((++node_id), ttag);
        if (root == nullptr) root = n;
        nodes.push_back(n);
        if (!roots.empty()) roots.top()->add_branch(n);
        if (!separated) roots.push(n);

        // --- Here we process the string stored inside the tag looking for key=value pairs
        const core::index2 len = tag_ends - tag_begins - ttag.size();
        std::string element_tokens = text.substr(tag_begins + 1 + ttag.size(), len);
        std::smatch match;
        std::sregex_iterator tokens_start(element_tokens.cbegin(), element_tokens.cend(), extract_key_value);
        std::sregex_iterator tokens_end;
        for (auto i = tokens_start; i != tokens_end; ++i) {
          std::string key{(*i)[1].str()};
          std::string value{(*i)[2].str()};
          logger.log(utils::LogLevel::FINE, "key=value pair found: ", key, "=", value, "\n");
          if(n->element.content.find(key)==n->element.content.end())
            n->element.content.insert(std::make_pair(key,value));
          else {
            n->element.at((*i)[1].str()) = (*i)[2].str();
          }
        }
      } else {
        logger.log(utils::LogLevel::SEVERE, "can't find element's name in: ", tag, "\n");
        throw std::runtime_error("can't find element's name in: " + tag);
      }
    }
    tag_begins = text.find('<', tag_ends);
  }
  doc_root = root;

  return root;
}

}
}
}
