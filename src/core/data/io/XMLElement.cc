#include <core/data/io/XMLElement.hh>

namespace core {
namespace data {
namespace io {

const std::string XMLElement::EMPTY;

static inline void indent(const core::index2 n, std::ostream & out) {
  for (int i = 0; i < n; i++)
    out << "  ";
}

std::ostream& operator<<(std::ostream &out, const std::shared_ptr<XMLElement> e) {

  out << *e;
  return out;
}

std::ostream& operator<<(std::ostream &out, const XMLElement & e) {

  static unsigned char level = 0;
  std::string keys_values = "";
  if (e.element.size() > 0) {
    for (const auto i : e.element.content) {
      keys_values += " " + i.first + "=\"" + i.second + "\"";
    }
  }
  if (e.is_simple_element()) {
    indent(level, out);
    out << utils::string_format("<%s%s />\n", e.name().c_str(), keys_values.c_str());
  } else {
    if (e.count_branches() == 0) {
      indent(level, out);
      out
          << utils::string_format("<%s%s>%s</%s>\n", e.name().c_str(), keys_values.c_str(), e.text_.c_str(),
              e.name().c_str());
      return out;
    }
    indent(level, out);
    out << utils::string_format("<%s%s>\n", e.name().c_str(), keys_values.c_str());
    level++;
    for (const std::shared_ptr<TreeNode<XMLElementData>> sp : e.branches) {
      std::shared_ptr<XMLElement> elem = std::dynamic_pointer_cast<XMLElement> (sp);
      out << elem ;
    }
    
    level--;
    indent(level, out);
    out << utils::string_format("</%s>\n", e.name().c_str());
  }
  return out;
}

}
}
}

