/** \file alignment_io.hh
 * @brief Reads and writes alignments in Edinburgh format.
 *
 * \include ex_alignment_io.cc
 */
#ifndef CORE_DATA_IO_alignment_io_H
#define CORE_DATA_IO_alignment_io_H

#include <string>
#include <iostream>

#include <core/index.hh>
#include <core/alignment/AlignmentAnnotation.hh>
#include <core/alignment/PairwiseAlignment.hh>
#include <core/alignment/PairwiseSequenceAlignment.hh>
#include <core/data/sequence/Sequence.hh>
#include <core/data/sequence/SecondaryStructure.hh>

namespace core {
namespace data {
namespace io {

using namespace core::data::sequence;

/** @brief Holds sequence alignment as text in a way appropriate for alignment-writing methods such as  write_edinburgh.
 *
 */
struct AlignmentOutputLines {
public:

  /// Maximum length of a sequence name. Any longer will be trimmed off
  static const core::index1 NAME_MAX_LENGTH = 10;

  int query_start; ///< where the alignment starts in the query sequence
  int query_end; ///< where the alignment ends in the query sequence
  int tmplt_start; ///< where the alignment starts in the template sequence
  int tmplt_end;///< where the alignment ends in the template sequence
  std::string query_name; ///< name of the query sequence
  std::string tmplt_name; ///< name of the template sequence
  std::string sequence_marks; ///< String with decorations for matches and similarities
  std::string query_sequence;  ///< the respective fragment of the query sequence
  std::string query_secondary; ///< the respective fragment of the query secondary structure
  std::string tmplt_sequence;  ///< the respective fragment of the template sequence
  std::string tmplt_secondary; ///< the respective fragment of the template secondary structure

  static std::vector<AlignmentOutputLines> create_blocks(const Sequence & query_sequence,
      const core::alignment::PairwiseAlignment & alignment, const Sequence & templt_sequence, const index2 width) {

    std::vector<AlignmentOutputLines> sink;
    return create_blocks(query_sequence, alignment, templt_sequence, width, sink);
  }

  static std::vector<AlignmentOutputLines> create_blocks(const SecondaryStructure & query_sequence,
      const core::alignment::PairwiseAlignment & alignment, const SecondaryStructure & templt_sequence,
      const index2 width) {

    std::vector<AlignmentOutputLines> sink;
    return create_blocks(query_sequence, alignment, templt_sequence, width, sink);
  }

  static std::vector<AlignmentOutputLines> & create_blocks(const Sequence & query_sequence, const core::alignment::PairwiseAlignment & alignment,
    const Sequence & templt_sequence, const index2 width, std::vector<AlignmentOutputLines> & storage);

  static std::vector<AlignmentOutputLines> & create_blocks(const SecondaryStructure & query_sequence, const core::alignment::PairwiseAlignment & alignment,
    const SecondaryStructure & templt_sequence, const index2 width, std::vector<AlignmentOutputLines> & storage);

  /** @brief Returns an empty string of the very same length as the query/template sequence name with padding.
   *
   * The method provides a spacer that is used to format output alignment correctly.
   */
  static const std::string & empty_string() {

    static const std::string empty(NAME_MAX_LENGTH + 5, ' ');
    return empty;
  }

private:
  static const std::string create_sequence_marks(const std::string & seq1,const std::string & seq2);

  static std::string format_name(const std::string & sequence_name);

  enum SequenceFields { QuerySeq, QuerySS, TmpltSeq, TmpltSS };

  template <SequenceFields S>
  void update_sequence(const std::string & sequence_block, const int start) {
    const core::index2 width = sequence_block.size();
    switch(S) {
      case QuerySeq :
        query_sequence = sequence_block;
        query_start = start;
        query_end = start + width - std::count_if(sequence_block.begin(), sequence_block.end(), [](char c) {return c=='-';}) - 1;
        break;
      case TmpltSeq :
        tmplt_sequence = sequence_block;
        tmplt_start = start;
        tmplt_end = start + width - std::count_if(sequence_block.begin(), sequence_block.end(), [](char c) {return c=='-';}) - 1;
        break;
      case QuerySS :
        query_secondary = sequence_block;
        break;
      case TmpltSS :
        tmplt_secondary = sequence_block;
        break;
    }
  }
};


/**  \brief Writes a pairwise sequence alignment in the Edinburgh format.
 *
 * Edinburgh-SS format is similar to Edinburgh but includes also secondary structure information.
 * Formatted alignment should look like:
 * <pre>
               * *  **  ** *** *** **** * *      ******  * *         **** **  *   *   * *  *
 Q7B1Y7_SAL KGWLFLVIAIVGEVIATSALKSSEGFTKLAPSAVVIIGYGIAFYFLSL-VLKSIPVGVAYAVWSGLGVVIITAIAWLLHG
 MMR_MYCTU  MIYLYLLCAIFAEVVATSLLKSTEGFTRLWPTVGCLVGYGIAFALLALSISHGMQTDVAYALWSAIGTAAIVLVAVLFLG

                    ** ***
 Q7B1Y7_SAL QKLDAWGFVGMGLI
 MMR_MYCTU  SPISVMKVVGVGLI
 </pre>
 *
 * @param query_sequence - object providing a query sequence and secondary structure
 * @param alignment - a query to template alignment
 * @param templt_sequence - object providing a template sequence and secondary structure
 * @param out - a stream where the output will be written
 * @param width - lines longer than that will be broken
 */
void write_edinburgh(const Sequence & query_sequence, const core::alignment::PairwiseAlignment & alignment,
    const Sequence & templt_sequence, std::ostream & out, const index2 width);

/**  \brief Writes a pairwise sequence alignment in the Edinburgh-SS format.
 *
 * Edinburgh-SS format is similar to Edinburgh but includes also secondary structure information.
 * Formatted alignment should look like:
 * <pre>                         *        *                          ** *   *
        query    4 EATIESNDA_MQYDLKEMVVDKSCKQFTVHLKHVGKMAKSAMGHNWVLTKEADKEGVATD   63
                   EEEEEECCC_CCCCCCEEEEECCCCEEEEEEEECCCCCCCCCCCEEEEEECCCHHHHHHH
                   EEEECCCCCCCEEECCEEEE_CCCCEEEEE__ECC____CC_CCEEEEC_CCC_C____C
 2pcyA.1e5_5.    2 DVLLGADDGSLAFVPSEFSI_SPGEKIVFK__NNA____GF_PHNIVFD_EDS_I____P   61

 *                      **   *      *     *   ** * *  * *
 query          63 GMNAGLAQDYVKAGDTRVIAHTKVIGGGESDSVTFDVSKLTPGEAYAYFCSFPGH_WAMM  122
                   HHHCCCCCCCCCCCCCCEEEEEEECCCCCEEEEEEECCCCCCCCCEEEECCCCCC_CCCE
                   __C_CCCCC_CCCCCCH____HHHCCCCCEEEEE_____CCCCEEEEEECC_C_CCCCCC
 2pcyA.1e5_5.   61 __S_GVDAS_KISMSEE____DLLNAKGETFEVA_____LSNKGEYSFYCS_P_HQGAGM  120

 *
        query  122 KGTLKLS  128
                   EEEEEEC
                   EEEEEEC
 2pcyA.1e5_5.  120 VGKVTVN  126
 </pre>
 *
 * @param query_ss - object providing a query sequence and secondary structure
 * @param alignment - a query to template alignment
 * @param templt_ss - object providing a template sequence and secondary structure
 * @param out - a stream where the output will be written
 * @param width - lines longer than that will be broken
 */
void write_edinburgh(const SecondaryStructure & query_ss, const core::alignment::PairwiseAlignment & alignment,
    const SecondaryStructure & templt_ss, std::ostream & out, const index2 width);

/** @brief Writes a pairwise sequence alignment in the Edinburgh format.
 *
 * @param seq_alignment - alignment to be written
 * @param out - output stream
 * @param width - lines longer than that will be broken
 */
inline void write_edinburgh(const core::alignment::PairwiseSequenceAlignment & seq_alignment, std::ostream & out,
    const index2 width) {

  if (seq_alignment.query_sequence->has_ss()) {
    SecondaryStructure_SP qss = std::static_pointer_cast<SecondaryStructure>(seq_alignment.query_sequence);
    SecondaryStructure_SP tss = std::static_pointer_cast<SecondaryStructure>(seq_alignment.template_sequence);
    write_edinburgh(*qss, *seq_alignment.alignment, *tss, out, width);
  } else write_edinburgh(*seq_alignment.query_sequence, *seq_alignment.alignment, *seq_alignment.template_sequence, out,
      width);
}

/** @brief Reads a pairwise sequence alignment in the Edinburgh format.
 *
 * @param buffer - buffer providing the input data
 * @return sequence alignment
 */
core::alignment::PairwiseSequenceAlignment_SP read_edinburgh(std::istream & buffer);

/** @brief Reads a pairwise sequence alignment in the Edinburgh format.
 *
 * @param file_name - input data file
 * @return sequence alignment
 */
core::alignment::PairwiseSequenceAlignment_SP read_edinburgh(const std::string & file_name);

/** @brief Reads all pairwise sequence alignments from a file in the Edinburgh format.
 *
 * @param file_name - input data file
 * @param container - a vector where the alignments will  be stored; the vector will not be cleared by this call,
 *  alignments extracted from a given file will be appended at the end of this vector
 * @return reference to the given container
 */
std::vector<core::alignment::PairwiseSequenceAlignment_SP> & read_edinburgh(const std::string & file_name,
    std::vector<core::alignment::PairwiseSequenceAlignment_SP> & container);

/** @brief Reads all pairwise sequence alignments from a file in the Edinburgh format.
 *
 * @param buffer - input data buffer
 * @param container - a vector where the alignments will  be stored; the vector will not be cleared by this call,
 *  alignments extracted from a given file will be appended at the end of this vector
 * @return reference to the given container
 */
std::vector<core::alignment::PairwiseSequenceAlignment_SP> & read_edinburgh(std::istream & buffer,
    std::vector<core::alignment::PairwiseSequenceAlignment_SP> & container);

/** @brief Reads a pairwise sequence alignment produced by HHPred program
 *
 * @param buffer - buffer providing the input data
 * @param info - map used to store alignment metadata such as e-value, template PDB code etc.
 * @return sequence alignment
 */
core::alignment::PairwiseSequenceAlignment_SP read_hhpred(std::istream & buffer);

/** @brief Reads a pairwise sequence alignment produced by HHPred program
 *
 * @param file_name - input data file
 * @param info - map used to store alignment metadata such as e-value, template PDB code etc.
 * @return sequence alignment
 */
std::vector<core::alignment::PairwiseSequenceAlignment_SP> read_hhpred(const std::string & file_name);

/** @brief Reads all alignments from a given file produced by HHPred program
 *
 * @param file_name - input data file
 * @param container - a vector where the alignments will  be stored; the vector will not be cleared by this call,
 *  alignments extracted from a given file will be appended at the end of this vector
 * @return reference to the given container
 */
std::vector<core::alignment::PairwiseSequenceAlignment_SP> & read_hhpred(const std::string & file_name,
          std::vector<core::alignment::PairwiseSequenceAlignment_SP> & container);

/** @brief Reads all alignments from a given file produced by HHPred program
 *
 * @param buffer - input data buffer
 * @param container - a vector where the alignments will  be stored; the vector will not be cleared by this call,
 *  alignments extracted from a given file will be appended at the end of this vector
 * @return reference to the given container
 */
std::vector<core::alignment::PairwiseSequenceAlignment_SP> & read_hhpred(std::istream & buffer,
        std::vector<core::alignment::PairwiseSequenceAlignment_SP> & container);

/// Dummy class; its only purpose is to tigger bindings for read_hhpred() method
class HHPredResults { std::vector<core::alignment::PairwiseSequenceAlignment_SP> alignments_; };

}
}
}

#endif
/**
 * \example ex_alignment_io.cc
 */
