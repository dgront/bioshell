/**@file json_io.hh Provides I/O methods for JSON contained data
 *
 */
#ifndef CORE_DATA_IO_JsonValue_H
#define CORE_DATA_IO_JsonValue_H

#include <string>
#include <vector>
#include <iostream>
#include <memory>

#include <core/algorithms/trees/TreeNode.hh>
#include <utils/Logger.hh>

namespace core {
namespace data {
namespace io {

/** @brief Represents a key:value pair stored in a JSON data structure
 */
class JsonValue {
public:

  /** @brief Creates a JSON node from given key and value strings.
   * @param key - a key string used to store a value; will be used to name a JavaScript variable
   * @param value - a value associated with the given key; may be empty - then the node corresponds to a JavaScript object
   */
  JsonValue(const std::string &key, const std::string &value = "") : need_quotes_(true), key_(key), value_(value) {}

  virtual ~JsonValue() = default;

  /// Returns a JSON value
  virtual const std::string string_value() const { return value_; }

  /// Returns a JSON key
  const std::string &key() const { return key_; }

  /** @brief returns true if the value converted to string has to be quoted.
   */
  virtual bool need_quotes() const { return need_quotes_; }

  /** @brief Set quotes for printing the stored value
   */
  void need_quotes(bool state) { need_quotes_ = state; }

protected:
  bool need_quotes_; ///< If true, the stored value will be printed in quotes
  std::string key_; ///< Key for this value
  std::string value_; ///< The value stored
};

/** @brief Represents a key:array pair stored in a JSON data structure
 */
class JsonArray : public JsonValue {
public:

  /** @brief Creates a JSON node from given key and value strings.
 * @param key - a key string used to store a value; will be used to name a JavaScript variable
 */
  JsonArray(const std::string &key) : JsonValue(key,"") {}

  /** @brief Creates a JSON node from given key and value strings.
   * @param key - a key string used to store a value; will be used to name a JavaScript variable
   * @param quote_bits - a string of zeros and ones defining which fields of this JSON array must be quoted
   * @param args - elements of this JSON array
   */
  template<typename... Args>
  JsonArray(const std::string &key, const std::string &quote_bits, Args... args) : JsonValue(key,"") {
    values(quote_bits,args...);
  }

  virtual ~JsonArray() = default;

  /// Always returns false since an array does not need quotes
  virtual bool need_quotes() const { return false; }

  /// Returns a JSON value
  virtual const std::string string_value() const;

  template<typename T>
  void values(const std::string &quote_bits, const T &v) {

    core::index1 which_token = quote_bits.size() - 1;

    if (quote_bits[which_token] == '1') values_.push_back("\"" + utils::to_string(v) + "\"");
    else values_.push_back(utils::to_string(v));
  }

  template<typename T, typename... Args>
  void values(const std::string &quote_bits, T first, Args... args) {

    core::index1 which_token = quote_bits.size() - sizeof...(args) - 1;
    if (quote_bits[which_token] == '1') values_.push_back("\"" + utils::to_string(first) + "\"");
    else values_.push_back(utils::to_string(first));

    values(quote_bits, args...);
  }

  template<typename T>
  void push_back(const bool need_quotes,const T & val,const std::string & format) {
    if(need_quotes) values_.push_back("\"" + utils::to_string(val) + "\"");
    else values_.push_back(utils::to_string(val));
  }

  void push_back(const bool need_quotes, const std::string &val);

private:
  std::vector<std::string> values_;
  std::stringstream out;
};

typedef std::shared_ptr<JsonValue> JsonValue_SP; ///< Shared pointer to JsonValue
typedef std::shared_ptr<JsonArray> JsonArray_SP; ///< Shared pointer to JsonArray
/// JsonNode is a TreeNode instance that holds a pointer to JsonValue as its data
typedef core::algorithms::trees::TreeNode<JsonValue_SP> JsonNode;
typedef std::shared_ptr<JsonNode> JsonNode_SP; ///< Shared pointer to JsonNode

/** @brief Create a new JSON node and insert a given array to it.
 *
 * The new JsonArray object will be created based on the given <code>keyvalue</code> and <code>values</code>
 *
 * @return  JSON tree node holding the given value; other values (leaves) may be inserted with
 * <code>add_branch()</code> method of TreeNode class
 */
template <typename ... Args>
JsonArray_SP create_json_array(const std::string & key,  const std::string &quote_bits, Args... args) {

  std::shared_ptr<JsonArray> array = std::make_shared<JsonArray>(key);
  array->values(quote_bits, args...);

  return array;
}

/** @name Create JSON nodes
 *  Methods of this group may be used to build JSON tree. Once a tree is created, it can be printed by sending
 *  its root to ostream using output operator. See the following example:
 *  \example ex_JsonNode.cc
 */
///@{
/** @brief Create an empty JSON node.
 *
 * One can use the node as a root of JSON tree and insert leaves to it using
 * <code>add_branch()</code> method of TreeNode class
 *
 * @return empty JSON tree node
 */
JsonNode_SP create_json_node();

/** @brief Create a new JSON node and insert a given value to it.
 *
 * @return  JSON tree node holding the given value; other values (leaves) may be inserted with
 * <code>add_branch()</code> method of TreeNode class
 */
JsonNode_SP create_json_node(const JsonValue_SP & value);

/** @brief Create a new JSON node and insert a given value to it.
 *
 * The new JsonValue object will be created based on the given <code>key:value</code> pair
 *
 * @return  JSON tree node holding the given value; other values (leaves) may be inserted with
 * <code>add_branch()</code> method of TreeNode class
 */
template <typename E>
JsonNode_SP create_json_node(const std::string & key, const E & value) {

  JsonValue_SP json_value = std::make_shared<JsonValue>(key,utils::to_string(value));
  json_value->need_quotes(false);
  return std::make_shared<JsonNode>(json_value,0);
}

/** @brief Create a new JSON node and insert a given array to it.
 *
 * The new JsonArray object will be created based on the given <code>key</code> and <code>values</code>
 *
 * @return  JSON tree node holding the given value; other values (leaves) may be inserted with
 * <code>add_branch()</code> method of TreeNode class
 */
template <typename ... Args>
JsonNode_SP create_json_node(const std::string & key,  const std::string &quote_bits, Args... args) {

  return std::make_shared<JsonNode>(create_json_array(key,quote_bits,args...),0);
}
///@}

///< output operator for a JsonValue instance
std::ostream &operator<<(std::ostream &out, const JsonValue &value);

///< output operator for a JsonValue pointer
std::ostream &operator<<(std::ostream &out, const JsonValue_SP &value);

///< output operator for a JsonArray instance
std::ostream &operator<<(std::ostream &out, const JsonArray &value);

///< output operator for a JsonNode shared pointer
std::ostream &operator<<(std::ostream &out, const JsonNode_SP node);

/** @brief Parses JSON data from a string.
 * @param json - data as a single string
 * @returns a shared pointer to the root of a JSON data structure
 */
JsonNode_SP read_json(const std::string &json);

}
}
}
#endif
