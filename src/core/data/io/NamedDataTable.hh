#ifndef CORE_DATA_IO_NamedDataTable_H
#define CORE_DATA_IO_NamedDataTable_H

#include <string>
#include <map>

#include <core/index.hh>
#include <core/data/io/DataTable.hh>

namespace core {
namespace data {
namespace io {

/** @brief Extends DataTable class by ability to name data columns
 *
 */
class NamedDataTable : public DataTable {
public:

  /// Create an empty data container
  NamedDataTable() : logger("NamedDataTable"), name_for_id_(255,"") {}

  /** @brief Define the name of a column
   *
   * @param which_column - index (from 0) of a column to be named
   * @param name - name given to that column
   */
  void column_name(const index1 which_column, const std::string & name) { 
    id_for_name_.insert({name, which_column}); 
    name_for_id_[which_column] = name; 
    logger << utils::LogLevel::INFO << "name '"<<name<<"' assigned to column at index " << int(id_for_name_[name]) << "\n";
  }

  /** @brief Returns the name of a column identified by its index
   *
   * @param which_column - column index
   * @return column name
   */
  const std::string & column_name(const index1 which_column) { return name_for_id_[which_column]; }

  /** @brief Returns the index of a column identified by its name
   *
   * @param which_column - column name
   * @return column index
   */
  index1 column_index(const std::string & column_name) { return id_for_name_.at(column_name); }

  /** @brief Returns true if this map contains a given column (identified by its name)
   *
   * @param which_column - column name
   * @return true if this map contains <code>which_column</column>
   */
  bool has_column(const std::string & column_name) { return id_for_name_.count(column_name)==1; }

private:
  utils::Logger logger;
  std::map<std::string, index1> id_for_name_;
  std::vector<std::string> name_for_id_;
};

}
}
}

#endif
