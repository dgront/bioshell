#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <stdexcept>
#include <utility>	// for std::make_pair()

#include <core/data/io/clustalw_io.hh>
#include <core/data/sequence/Sequence.hh>
#include <utils/Logger.hh>
#include <utils/io_utils.hh>

namespace core {
namespace data {
namespace io {

using core::data::sequence::Sequence;
using core::data::sequence::Sequence_SP;

utils::Logger clustalw_io_logs("clustalw_io");

std::vector<std::shared_ptr<Sequence>> & read_clustalw_file(const std::string & file_name,
    std::vector<std::shared_ptr<Sequence>> & sink, bool fix_aa) {

  if(!utils::if_file_exists(file_name)) {
    std::string msg = "Can't find MSA (Clustal-W format) file: " + file_name + "!\n";
    clustalw_io_logs<<utils::LogLevel::SEVERE<<msg;
    throw std::runtime_error(msg);
  }

  clustalw_io_logs << utils::LogLevel::FILE << "Reading CLUSTALW from " << file_name << " ...\n";
  std::map<std::string, std::string> tmp_seq;
  std::ifstream infile(file_name);
  if (!infile) {
    clustalw_io_logs << utils::LogLevel::WARNING << "Cannot read from the input CLUSTALW file: " << file_name << " !\n";
    return sink;
  }
  std::string line;
  char seq[4096], id[1024];
  std::getline(infile, line);
  if (!(line.compare(0, 7, "CLUSTAL") == 0)) {
    clustalw_io_logs << utils::LogLevel::SEVERE << "is " << file_name << " really in CLUSTALW format?\n";
    return sink;
  }
  while (std::getline(infile, line)) {
    if (line.length() < 2) continue;
    if (line[0] == '#') continue;
    const int nl = sscanf(line.c_str(), "%s %s", id, seq);
    if (nl < 2) {
      clustalw_io_logs << utils::LogLevel::WARNING << "skipping a line: " << line << "\n";
      continue;
    }
 
    if((seq[0] == '.') || (seq[0] == ':') || (seq[0] == '*')) continue;	// Remove the line with conservation info
    
    std::string current_sequence(seq);
    std::string current_id(id);

    std::map<std::string, std::string>::iterator it = tmp_seq.find(current_id);
    if (it != tmp_seq.end()) it->second += current_sequence;
    else tmp_seq[current_id] = current_sequence;
  }

  for (auto p = tmp_seq.begin(); p != tmp_seq.end(); ++p) {
    if(fix_aa) core::data::sequence::fix_code1(p->second);
    sink.push_back(std::shared_ptr<Sequence>(new core::data::sequence::Sequence(p->first, p->second, 1)));
  }

  clustalw_io_logs << utils::LogLevel::FILE << "found " << tmp_seq.size() << " sequences\n";

  return sink;
}

std::vector<std::shared_ptr<Sequence>> & read_clustalo_file(const std::string & file_name,
    std::vector<std::shared_ptr<Sequence>> & sink, bool fix_aa) {

  if(!utils::if_file_exists(file_name)) {
    std::string msg = "Can't find MSA (Clustal-O format) file: " + file_name + "!\n";
    clustalw_io_logs<<utils::LogLevel::SEVERE<<msg;
    throw std::runtime_error(msg);
  }

  clustalw_io_logs << utils::LogLevel::FILE << "Reading CLUSTALOmega from " << file_name << " ...\n";
  std::ifstream infile(file_name);
  if (!infile) {
    clustalw_io_logs << utils::LogLevel::WARNING << "Cannot read from the input CLUSTALOmega file: " << file_name << " !\n";
    return sink;
  }
  std::string line;
  char seq[4096], id[4096];
  while (std::getline(infile, line)) {
    if (line.length() < 2) continue;
    if (line[0] == '#') continue;
    const int nl = sscanf(line.c_str(), "%s %s", id, seq);
    if (nl < 2) {
      clustalw_io_logs << utils::LogLevel::WARNING << "skipping a line: " << line << ", expected exactly two tokens\n";
      continue;
    }

    if((seq[0] == '.') || (seq[0] == ':') || (seq[0] == '*')) continue;	// Remove the line with conservation info

    std::string current_sequence(seq);
    std::string current_id(id);
    if(fix_aa) core::data::sequence::fix_code1(current_sequence);
    sink.push_back(std::shared_ptr<Sequence>(new core::data::sequence::Sequence(current_id, current_sequence, 1)));
  }

  clustalw_io_logs << utils::LogLevel::FILE << "found " << sink.size() << " sequences\n";

  return sink;
}

void write_clustalo_file(const std::vector<std::shared_ptr<Sequence>> & data, const std::string & out_fname,
                         core::index2 max_header_length) {
  std::shared_ptr<std::ostream> out = utils::out_stream(out_fname);
  write_clustalo_file(data, *out, max_header_length);
  out->flush();
}

void write_clustalo_file(const std::vector<std::shared_ptr<Sequence>> & data, std::ostream & out,
                         core::index2 max_header_length) {
  out << "CLUSTAL O(1.2.0) multiple sequence alignment\n\n\n";
  size_t len = 0;
  for (const Sequence_SP &s:data)
    len = std::max(len, s->header().length());
  len = std::min(len, size_t(max_header_length));
  std::string format = "%" + utils::to_string(len) + "s %s\n";
  for (const Sequence_SP &s:data)
    out << utils::string_format(format, s->header().c_str(), s->sequence.c_str());
}

}
}
}

