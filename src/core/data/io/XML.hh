/** @file XML.hh
 *  @brief Provides a class that loads and parses a XML file from disk
 */
#ifndef CORE_DATA_IO_XML_H
#define CORE_DATA_IO_XML_H

#include <string>
#include <vector>
#include <memory>

#include <utils/Logger.hh>
#include <utils/io_utils.hh>

namespace core {
namespace data {
namespace io {

class XMLElement;    // Forward declaration of an XML element

/** @brief Loads and parses a XML file.
 *
 * Once the XML document is parsed, the contents is returned in the form of a trie-like data structure.
 * @include ex_XML.cc
 */
class XML {
public:

  /// Create a new parser object.
  XML() : logger("XML") {}

  /** @brief Create a parser object and use it to load data from a file.
   * @param fname - the name of file to be parsed
   */
  XML(const std::string & fname) : logger("XML") { load_data(fname); }

  /** @brief Create a parser object and use it to load data from a stream.
   * @param input - the input stream
   */
  XML(std::istream & input) : logger("XML") { load_data(input); }

  /** @brief Load data from a file.
   * @param fname - the name of file to be parsed
   */
  std::shared_ptr<XMLElement> load_data(const std::string & fname);

  /** @brief Load data from a stream.
   * @param input - the input stream
   */
  std::shared_ptr<XMLElement> load_data(std::istream & input);

  /// Returns root of thee XML trie data structure
  inline std::shared_ptr<XMLElement> document_root() { return doc_root; }

private:
  std::string text;
  utils::Logger logger;
  std::shared_ptr<XMLElement> doc_root;
  std::vector<std::shared_ptr<XMLElement>> nodes;
  std::shared_ptr<XMLElement> parse();
  void remove_comments();
  std::string extract_declaration();
  std::string extract_doctype();
};

}
}
}

#endif
/**
 * @example  ex_XML.cc
 */
