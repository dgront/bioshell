/** \file pir_io.hh
 * @brief  I/O methods for PIR file format
 */
#ifndef CORE_DATA_IO_pir_io_H
#define CORE_DATA_IO_pir_io_H

#include <string>
#include <vector>
#include <memory>

#include <core/data/sequence/Sequence.hh>
#include <core/data/sequence/PirEntry.hh>
#include <core/alignment/PairwiseAlignment.hh>
#include <core/alignment/PairwiseSequenceAlignment.hh>

#include <utils/string_utils.hh>

namespace core {
namespace data {
namespace io {

using namespace core::data::sequence;

/** @brief Reads a PIR-formatted file with sequences.
 *
 * The following example reads a PIR file and writes all the sequences found therein in FASTA format;
 * \include ap_pir_to_fasta.cc
 *
 * Another one reads PDB and writes PIR:
 * \include ap_pdb_to_pir.cc
 *
 * @param file_name - the name of an input file
 * @param sink - where the sequences will be stored
 */
std::vector<std::shared_ptr<Sequence>> & read_pir_file(const std::string file_name, std::vector<std::shared_ptr<Sequence>> & sink);

/// Makes a PIR-formatted multiline string from a given sequence
std::string create_pir_string(const Sequence & seq, const core::index2 line_width = 65535);

/// Makes a PIR-formatted multiline string from a given PirEntry instance
std::string create_pir_string(const core::data::sequence::PirEntry & pir_seq, const core::index2 line_width);

/** @brief Converts a given sequence alignment into a PIR-formatted string.
 *
 * @param ali - alignment object to be printed
 * @param query_sequence - query sequence object (unaligned, i.e. without gaps)
 * @param tmplt_sequence - template sequence object (unaligned, i.e. without gaps)
 * @param line_width - the length of each sequence line (the string will be broken to match the width)
 * @return strings in PIR format
 */
std::string create_pir_string(const core::alignment::PairwiseAlignment &ali, const Sequence &query_sequence,
                                const Sequence &tmplt_sequence, const core::index2 line_width = 65535);

/** @brief Converts a given sequence alignment into a PIR-formatted string(s).
 *
 * @param ali - alignment object to be printed
 * @param line_width - the length of each sequence line (the string will be broken to match the width)
 * @return strings in PIR format
 */
std::string create_pir_string(const core::alignment::PairwiseSequenceAlignment &ali,

                                const core::index2 line_width = 65535);
}
}
}

#endif

/**
 * \example ap_pir_to_fasta.cc
 * \example ap_pdb_to_pir.cc
 */
