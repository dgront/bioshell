/** \file Hsp.hh
 * @brief provides a class holding single blast hit (aka HSP) and respective I/O methods
 */
#ifndef CORE_DATA_IO_Hsp_H
#define CORE_DATA_IO_Hsp_H

#include <string>
#include <regex>
#include <algorithm>

#include <core/index.hh>
#include <utils/string_utils.hh>

#include <utils/Logger.hh>

namespace core {
namespace data {
namespace io {


/** @brief High-scoring Segment Pair (aka HSP) found by BLAST program.
 *
 */
class Hsp {
public:

  static const std::string output_header; ///< A header string that names columns of the ostream operator that is used to print a HSP object

  /** @brief No-argument constructor
   */
  Hsp();

  /** @brief Copying constructor
   *
   * @param h - a reference to the source Hsp object
   */
  Hsp(const Hsp &h);

  /** @brief Constructor that fills fields of this class with the given values.
   *
   * @param hit_length - length of the whole subject (the hit) sequence
   * @param hit_description - description of the hit (subject) sequence
   * @param hit_accession -accession of the hit (subject) sequence
   * @param score - sequence alignment score
   * @param expect - sequence alignment e-value
   * @param identities - number of identical positions in the aligned fragment
   * @param positives - number of similar positions in the aligned fragment
   * @param gaps - number of gaps in the aligned fragment
   * @param query_starts - first residue of the query sequence included in the HSP
   * @param query_ends - last residue of the query sequence included in the HSP
   * @param sbjct_starts - first residue of the subject sequence included in the HSP
   * @param sbjct_ends - last residue of the subject sequence included in the HSP
   * @param query - the fragment of the query sequence aligned with a subject (template)
   * @param sbjct - the fragment of the subject sequence aligned with the query
   */
  Hsp(index2 hit_length, const std::string &hit_description, const std::string &hit_accession,
      index2 score, double expect, index2 identities,
      index2 positives, index2 gaps, index2 query_start, index2 query_end, index2 sbjct_start,
      index2 sbjct_end, const std::string &query, const std::string &sbjct);

  /// Returns the length of the ful hit sequence
  index2 length() const { return length_; }

  /// Returns the length of this HSP, including gaps
  index2 hsp_length() const { return query_.size(); }

  /// Returns the number of amino acid positions aligned within this HSP to another amino acid
  index2 n_aligned() const;

  /// Returns subject (template) sequence accession ID
  const std::string &hit_accession() const { return hit_accession_; }

  /// Returns subject (template) sequence description
  const std::string &description() const { return description_; }

  /// Returns the alignment score, computed for this HSP
  index2 score() const { return score_; }

  /// Returns the alignment e-value, computed for this HSP
  double expect() const { return expect_; }

  /// Returns the number of identical positions in this HSP alignment
  index2 n_identical() const { return identities_; }

  /// Returns the number of similar (defined by a positive BLOSUM value) positions in this HSP alignment
  index2 n_positive() const { return positives_; }

  /// Returns the number of gaps in this HSP alignment
  index2 gaps() const { return gaps_; }

  /// Says which residue of the query sequence starts this HSP (counting from 1)
  index2 query_start() const { return query_start_; }

  /// Says which residue of the query sequence ends this HSP (counting from 1)
  index2 query_end() const { return query_end_; }

  /// Says which residue of the subject sequence starts this HSP (counting from 1)
  index2 sbjct_start() const { return sbjct_start_; }

  /// Says which residue of the subject sequence ends this HSP (counting from 1)
  index2 sbjct_end() const { return sbjct_end_; }

  /// Returns the aligned fragment of the query sequence
  const std::string &query() const { return query_; }

  /// Returns the aligned fragment of the template sequence
  const std::string &sbjct() const { return sbjct_; }

private:
  index2 length_;
  std::string description_;
  std::string hit_accession_;

  index2 score_;
  double expect_;
  index2 identities_;
  index2 positives_;
  index2 gaps_;
  index2 query_start_;
  index2 query_end_;
  index2 sbjct_start_;
  index2 sbjct_end_;
  std::string query_;
  std::string sbjct_;

};

/** @brief Prints a HSP in a brief tabular manner.
 * The output includes:
 *   - hit sequence ID
 *   - hit sequence length
 *   - alignment score
 *   - alignment length
 *   - n_gaps
 *   - gaps percentage - calculated as the number of gaps divided by the full length of a match (gaps included)
 *   - n_identical
 *   - percentage of identical positions (matches) computed in the same as by blast software (gap-included)
 *   - the aligned subject sequence
 */
std::ostream &operator<<(std::ostream &out, const Hsp &hsp);

}
}
}

#endif
