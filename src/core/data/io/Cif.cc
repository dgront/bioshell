#include <core/data/io/Cif.hh>

namespace core {
namespace data {
namespace io {

Cif::Cif(const std::string & filename) : ifs_(filename), logger("Cif") {

  if (!ifs_.is_open()) throw CifParseError("Cannot open file " + filename);

  read_root();
  ifs_.close();
}

void Cif::read_root() {

  std::shared_ptr<CifBlock> current_block = nullptr;
  std::string token;
  while (ifs_ >> token) {
    if (token.rfind("data_", 0) == 0) {
      current_block = std::make_shared<CifBlock>(token);
      blocks_[token] = current_block;
      logger << utils::LogLevel::FINE << "new block found: " << token << "\n";
    }
    if (token[0] == ';') handle_semicolon(token);
    if (token == "#") {
      // ---------- Peek the next token
      int len = ifs_.tellg(); // --- Get current position
      std::string next_token;
      if (!(ifs_ >> next_token)) {
        logger << utils::LogLevel::FINE << "Can't find next token after #, assuming EOF\n";
        return;
      }
      if (next_token == "#") continue; // --- Two '#' one after another
      if (next_token.rfind("data_", 0) == 0) {// --- next data_ block is coming
        ifs_.seekg(len ,std::ios_base::beg);
        continue;
      }
      ifs_.seekg(len ,std::ios_base::beg);
      while (read_section(current_block));
    }
  }
  logger << utils::LogLevel::INFO << blocks_.size() << " block(s) found in the input\n";
}

void Cif::handle_semicolon(std::string & token) {
  if (token == ";") return;
  bool done = false;
  std::string t;
  while (ifs_ >> t) {
    if (t == ";") {
      done = true;
      break;
    }
    token += t;
  }
  if (!done) throw CifParseError("Semicolon error.");
}

bool Cif::read_section(std::shared_ptr<CifBlock> data_block) {

  std::string token;

  do {
    if (!(ifs_ >> token)) return false; // --- return false on eof
  } while(token=="#"); // --- most likely two '#' one after another

  if (token == "loop_") {
    read_loop(data_block);
  } else { // Read normal key value section
    std::string key, value;

    auto section = std::make_shared<CifSection>();

    //first key is already read
    key = token;
    if (key[0] == ';') handle_semicolon(key);
    if (!(ifs_ >> value))
      throw CifParseError("Unexpected eof: no value for key: " + key);
    finish_value(value);
    logger << utils::LogLevel::FINER << "Inserting key:value :" << key << " : " << value << " into a CIF section of "
           << data_block->name << " block\n";
    section->add_key_value(key, value);

    //read rest
    bool endOk = false;
    while (ifs_ >> key) {
      if (key[0] == '#') {
        endOk = true;
        break;
      }

      if (key[0] == ';') {
        handle_semicolon(key);
        continue;
      }

      if (!(ifs_ >> value)) throw CifParseError("Unexpected eof: no value for key: " + key);

      finish_value(value);
      logger << utils::LogLevel::FINER << "Inserting key:value :" << key << " : " << value << " into a CIF section of "
             << data_block->name << " block\n";
      section->add_key_value(key, value);
    }

    data_block->push_back(section);

    if (!endOk) return false;
  }
  return true;
}

bool Cif::read_loop(std::shared_ptr<CifBlock> block) {

  std::string token;
  std::vector<std::string> labels;
  std::vector<std::string> entry;

  bool endOk = false;
  while (ifs_ >> token) {
    if (token[0] == '_') {
      labels.push_back(token);
    } else {
      endOk = true;
      break;
    }
  }
  if (!endOk) throw CifParseError("Unexpected eof during loop. (1)");

  int n = labels.size();
  auto section = std::make_shared<CifSection>(labels);

  if (token[0] == ';') handle_semicolon(token);

  finish_value(token);
  entry.push_back(token);
  for (core::index2 i = 1; i < n; ++i) {
    if (!(ifs_ >> token)) throw CifParseError("Unexpected eof during loop. (2)");

    if (token[0] == ';')
      handle_semicolon(token);
    finish_value(token);
    entry.push_back(token);
  }
  section->add_loop_row(entry);

  while (ifs_ >> token) {
    if (token[0] == '#')
      break;

    if (token[0] == ';') {
      handle_semicolon(token);
      continue;
    }

    finish_value(token);
    entry.clear();
    entry.push_back(token);
    for (int i = 1; i < n; ++i) {
      if (!(ifs_ >> token)) throw CifParseError("Unexpected eof during loop. (3)");
      finish_value(token);
      entry.push_back(token);
    }

    section->add_loop_row(entry);
  }

  block->push_back(section);

  return true;
}

void Cif::finish_value(std::string &value) {

  if ((value[0] != '\'')&&(value[0] != '\"')) return;
  if ((value.back() == '\'') || (value.back() == '\"')) return;
  std::string token;
  while (ifs_ >> token) {
    value += " " + token;
    if ((value.back() == '\'') || (value.back() == '\"')) return;
  }

  throw CifParseError("Unexpected eof during multi word value.");
}


CifSection::CifSection() : is_loop_(false) {}

CifSection::CifSection(std::vector<std::string> loopLabels) :

    is_loop_(true), loop_labels_(loopLabels) {}

const std::map<std::string, std::string> &CifSection::values() const {

  if (is_loop_) throw CifSectionTypeException();

  return values_;
}

const std::map<std::string, std::vector<std::string>> &CifSection::loop() const {

  if (!is_loop_) throw CifSectionTypeException();

  return loopData_;
}

bool CifSection::add_key_value(std::string key, std::string value) {

  if (is_loop_) throw CifSectionTypeException();

  return values_.insert(std::make_pair(key, value)).second;
}

bool CifSection::add_loop_row(const std::vector<std::string> & values) {

  if (!is_loop_) throw CifSectionTypeException();

  if (values.size() != loop_labels_.size())
    return false;

  for (std::size_t i = 0; i < loop_labels_.size(); ++i)
    loopData_[loop_labels_[i]].push_back(values[i]);

  return true;
}

const std::string & CifBlock::at(const std::string & key) const {

  for (const auto &b : sections_)
    if (b->has_key(key)) return b->at(key);

  throw std::out_of_range("Key "+key+" not found!");
}

bool CifBlock::has_key(const std::string &key) const {

  for (const auto &b : sections_)
    if (b->has_key(key)) return true;

  return false;
}

std::ostream & operator<<(std::ostream & out, const CifSection & sec) {

  if(sec.is_loop()) {
    out << "loop_\n";
    for(const std::string & label : sec.loop_labels()) out << label << "\n";

    core::index4 n_data = sec.loop().cbegin()->second.size();
    for (core::index4 idata = 0; idata < n_data; ++idata) {
      for (const std::string &label : sec.loop_labels()) out << sec.loop().at(label)[idata] << " ";
      out << "\n";
    }
  } else {
    for (const auto &key_val : sec.values())
      out << key_val.first << " " << key_val.second << "\n";
  }

  return out;
}


std::ostream & operator<<(std::ostream & out, const CifBlock & data_block) {

  for(const auto & sec_sp : data_block) {
    out << *sec_sp << "#\n";
  }

  return out;
}

std::ostream & operator<<(std::ostream & out, const Cif & cif) {

  for (const auto bi : cif) out << bi.first << "\n#\n" << (*bi.second);

  return out;
}

}
}
}
