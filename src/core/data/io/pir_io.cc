#include <fstream>
#include <iostream>

#include <core/data/io/pir_io.hh>

namespace core {
namespace data {
namespace io {

using namespace core::data::sequence;

static utils::Logger pir_io_logs("pir_io");

std::vector<std::shared_ptr<core::data::sequence::Sequence>> &read_pir_file(const std::string file_name,
      std::vector<std::shared_ptr<core::data::sequence::Sequence>> &sink) {

  pir_io_logs << utils::LogLevel::FILE << "Reading PIR from " << file_name << " ...\n";

  std::ifstream infile(file_name);
  if (!infile) {
    pir_io_logs << utils::LogLevel::WARNING << "Cannot read from the input PIR file: " << file_name << " !\n";
    return sink;
  }
  std::string line, current_sequence = "", current_id = "P1;", current_header = "empty";
  while (std::getline(infile, line)) {
    if (line.length() < 3) continue;
    if (line[0] == '#') continue;
    if (line.find("C;") == 0) continue;

    if (line.find(">P1;") == 0) {
      current_header = line;
      std::getline(infile, line);
      current_header += "\n"+line;
    }
    current_sequence = "";
    do {
      std::getline(infile, line);
      current_sequence += line;
    } while (line.find("*") == std::string::npos);
    current_sequence.pop_back(); // --- remove trailing '*'
    sink.push_back(std::make_shared<PirEntry>(current_header, current_sequence));
  }

  pir_io_logs << utils::LogLevel::FILE << "found " << sink.size() << " sequences\n";

  return sink;
}

std::string create_pir_string(const Sequence & seq, const core::index2 line_width) {

  std::string pir_entry = ">P1;" + seq.header() + "\n";
  core::index2 len = seq.sequence.length();
  core::index2 start = 0;
  do {
    core::index2 n = std::min(line_width, (core::index2) (len - start));
    pir_entry += seq.sequence.substr(start, n) + "\n";
    start += n;
  } while (start < len);
  if (pir_entry.back() != '*') pir_entry[pir_entry.size() - 1] = '*';

  return pir_entry;
}

std::string create_pir_string(const core::data::sequence::PirEntry & pir_seq, const core::index2 line_width) {

  std::string pir_entry = pir_seq.header() + "\n";
  core::index2 len = pir_seq.sequence.length();
  core::index2 start = 0;
  do {
    core::index2 n = std::min(line_width, (core::index2) (len - start));
    pir_entry += pir_seq.sequence.substr(start, n) + "\n";
    start += n;
  } while (start < len);
  if (pir_entry.back() != '*') pir_entry[pir_entry.size() - 1] = '*';

  return pir_entry;
}

std::string create_pir_string(const core::alignment::PairwiseAlignment &ali, const Sequence &query_sequence,
                              const Sequence &tmplt_sequence, const core::index2 line_width) {

  PirEntry q("", ali.get_aligned_query(query_sequence.sequence, '-'));
  q.type(PirEntryType::SEQUENCE);
  PirEntry t("", ali.get_aligned_template(tmplt_sequence.sequence, '-'));
  t.type(PirEntryType::STRUCTURE_X);

  std::string out = create_pir_string(q, line_width);
  out += "\n" + create_pir_string(t, line_width);

  return out;
}


std::string create_pir_string(const core::alignment::PairwiseSequenceAlignment &ali, const core::index2 line_width) {

  PirEntry q("", ali.get_aligned_query());
  q.type(PirEntryType::SEQUENCE);
  PirEntry t("", ali.get_aligned_template());
  t.type(PirEntryType::STRUCTURE_X);
  t.code(ali.alignment_data().at(core::alignment::AlignmentAnnotation::PDB_ID));
  char chain_id = ali.alignment_data().at(alignment::AlignmentAnnotation::CHAIN_ID)[0];
  t.first_chain_id(chain_id);
  t.last_chain_id(chain_id);

  std::string out = create_pir_string(q, line_width);
  out += "\n" + create_pir_string(t, line_width);

  return out;
}

}
}
}
