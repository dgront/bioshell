/** \file Pdb.hh
 * @brief provides classes and methods for reading and parsing PDB files
 */
#ifndef CORE_DATA_IO_PDB_H
#define CORE_DATA_IO_PDB_H

#include <string>
#include <vector>
#include <memory>
#include <map>
#include <functional>
#include <iostream>

#include <core/data/basic/Vec3.hh>
#include <core/data/basic/Vec3I.fwd.hh>
#include <core/data/basic/Vec3Cubic.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/Residue.fwd.hh>
#include <core/data/sequence/Sequence.hh>
#include <core/data/io/PdbField.hh>
#include <core/data/basic/Vec3Cubic.hh>

#include <utils/Logger.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>

namespace core {
namespace data {
namespace io {

/** @brief A data type for predicated that filter PDB lines (given as strings) while a PDB file is loaded in.
 *
 * This will remove unwanted atoms up-front prior parsing PDB data making this process faster
 */
typedef std::function<bool(const std::string&)> PdbLineFilter;

/** @name Predicates for filtering PDB lines.
 *
 * A few handy predicates were declared as a static const pointer to a function (aka  PdbLineFilter instances)
 * Each of these predicates takes a string as an argument and returns true if the string is a PDB line of a particular type.
 * The purpose for the predicates is to filter PDB data prior parsing and hence speed up the PDB reading process.
 *
 * <strong>Note:</strong> any predicate does not check if a given line is actually in the PDB format; this is taken for granted.
 *
 * Given an example string:
 * @code
std::string line = "ATOM    181  C   LYS A  24     -17.473  23.415  -7.068  1.00 14.12           C";
 * @endcode
 *
 * the following statement:
 * @code
 * core::data::io::is_bb(line);
 * @endcode
 * returns true. Moreover, also the following predicated:
 *     - core::data::io::is_standard_atom
 *     - core::data::io::is_not_hydrogen
 *     - core::data::io::is_not_water
 *     - core::data::io::is_not_alternative
 *     - core::data::io::keep_all
 *
 * will return <code>true</code>. But core::data::io::is_hetero_atom and core::data::io::is_ca will return <code>false</code>.
 *
 * The following simple example removes or water molecules and alternate atom locations while reading a PDB file:
 * \include ex_PdbLineFilter.cc
 */
///@{

/** @brief A method that creates a chain filter to read-in a particular chain
 *
 * @tparam C - the template code (single character)
 */
template<char C>
const PdbLineFilter create_chain_filter() {
  auto f = [&](const std::string line) {
    if(line[21]!=C) return false;
    return true;
  };
  return f;
}

/** @brief Predicate that negates another filter
 */
const PdbLineFilter invert_filter(const PdbLineFilter f1);

/** @brief Predicate that returns true if both <code>f1</code> and <code>f2</code> returned true.
 */
const PdbLineFilter all_true(const PdbLineFilter f1, const PdbLineFilter f2);

/** @brief Predicate that returns true if both <code>f1</code> and <code>f2</code> returned true.
 */
const PdbLineFilter one_true(const PdbLineFilter f1, const PdbLineFilter f2);

/** @brief Predicate that returns true if all the predicates returned true.
 */
const PdbLineFilter all_true(const PdbLineFilter f1, const PdbLineFilter f2, const PdbLineFilter f3);

/** @brief Predicate that returns true if all the predicates returned true.
 */
const PdbLineFilter all_true(const PdbLineFilter f1, const PdbLineFilter f2, const PdbLineFilter f3, const PdbLineFilter f4) ;

/** @brief Predicate that returns true if all the predicates returned true.
 */
const PdbLineFilter all_true(const PdbLineFilter f1, const PdbLineFilter f2, const PdbLineFilter f3, const PdbLineFilter f4, const PdbLineFilter f5);

/** @brief Predicate that returns true if all the predicates returned true.
 */
const PdbLineFilter all_true(const std::vector<PdbLineFilter> & filters);

/** @brief Predicate that returns true if a given PDB line holds a standard atom.
 */
static const PdbLineFilter is_standard_atom = [](const std::string line) {
  if(line[0]!='A') return false;
  if(line[1]!='T') return false;
  return true;
};


/** @brief Predicate that returns true if a given PDB line holds a hetero-atom
 */
static const PdbLineFilter is_hetero_atom = [](const std::string line) {
  if(line[0]!='H') return false;
  if(line[3]!='A') return false;
  return true;
};

/** @brief Predicate that returns true if a given PDB line holds a non-hydrogen atom
 */
static const PdbLineFilter is_not_hydrogen = [](const std::string line) {

  if(line[13]=='H') return false;
  if(line[12]=='H') return false;
  return true;
};

/** @brief Predicate that returns true if a given PDB line holds a hydrogen atom
 */
static const PdbLineFilter is_hydrogen = [](const std::string line) {

    if(line[13]=='H') return true;
    if(line[12]=='H') return true;
    return false;
};

/** @brief Predicate that returns true if a given PDB line holds anything but a water molecule
 */
static const PdbLineFilter is_not_water = [](const std::string line) {
    if ((line[17] == 'H') && (line[18] == 'O') && (line[19] == 'H')) return false;
    return true;
};

/** @brief Predicate that returns true if a given PDB line holds a water molecule
 */
static const PdbLineFilter is_water = [](const std::string line) {
    if ((line[17] == 'H') && (line[18] == 'O') && (line[19] == 'H')) return true;
    return false;
};

/** @brief Predicate that returns true if a given PDB line holds an atom that has no alternatives OR is the first of the set of alternative atom locations
 */
static const PdbLineFilter is_not_alternative = [](const std::string line) {
    if ((line[16] == 'A') || (line[16] == ' ')) return true;
    return false;
};

/** @brief Predicate that returns true if a given PDB line holds C\f$\alpha\f$ atom; false otherwise
 */
static const PdbLineFilter is_ca = [](const std::string line) {
    if (line[13] != 'C') return false;
    if (line[14] != 'A') return false;
    if (line[15] != ' ') return false;
    return true;
};

/** @brief Predicate that returns true if a given PDB line holds C\f$\beta\f$ atom; false otherwise
 */
static const PdbLineFilter is_cb = [](const std::string line) {
    if (line[13] != 'C') return false;
    if (line[14] != 'B') return false;
    return true;
};

/** @brief Predicate that returns true if a given PDB line holds one of the backbone heavy atoms; false otherwise
 */
static const PdbLineFilter is_bb = [](const std::string line) {
    if ((line[17] == 'H') && (line[18] == 'O')) return false; // --- Skip water molecules
    if ((line[13] == 'N') && (line[14] == ' ')) return true;
    if ((line[13] == 'C') && (line[14] == 'A')) return true;
    if ((line[13] == 'C') && (line[14] == ' ')) return true;
    if ((line[13] == 'O') && (line[14] == ' ')) return true;
    return false;
};

/** @brief Header line filter: only secondary structure definition is allowed from a PDB header
 */
static const PdbLineFilter only_ss_from_header = [](const std::string line) {
    if (line.rfind("SHE", 0) == 0) return true;
    if (line.rfind("HEL", 0) == 0) return true;
    return false;
};


/** @brief Predicate that always returns true.
 *
 * When this predicate is used, all PDB lines are loaded
 */
static const PdbLineFilter keep_all = [](const std::string line) {
  return true;
};
///@}

/** @brief Holds data extracted from a single <code>ATOM</code> line of a PDB file.
 *
 */
class Atom : public PdbField {
public:
  int serial;
  int residue_id;
  std::string name;
  char alt_loc;
  char i_code;
  std::string res_name;
  double x, y, z;
  double occupancy, temp_factor;
  char chain;
  bool is_heteroatom;
  std::string element;
  std::string charge;

  Atom(const std::string &pdb_line);
  std::string to_pdb_line() const;
  const static std::string atom_format;
  const static std::string atom_format_uncharged;
  const static std::string hetatm_format;
  const static std::string hetatm_format_uncharged;
};

/** @brief Represents <code>TER</code> field from a PDB file.
 */
class Ter : public PdbField {
public:
  int serial; ///< Serial number of this TER field
  std::string res_name; ///< 3-letter code of the terminal residue
  std::string chain; ///< ID of the terminated chain
  int residue_id; ///< ID of the terminal residue
  char i_code; ///< Insertion code of the terminal residue
  bool is_complete; ///< If false, the fields of this object might be undefined; In such a case bioshell just assumes the very last residue of a chain to be TER

  /** @brief Parses a TER line of PDB file
   * @param pdb_line - TER line - in PDB format
   */
  Ter(const std::string &pdb_line);

  /// Returns a TER line created from this data
  virtual std::string to_pdb_line() const;
};

/** @brief Holds data extracted from a single <code>HETATM</code> line of a PDB file.
 */
class Hetatm : public Atom {
public:
  Hetatm(const std::string &pdb_line) : Atom(pdb_line) {}
  std::string to_pdb_line() const;
};

/** @brief Holds data extracted from the <code>HEADER</code> line of a PDB file.
 */
class Header : public PdbField {
public:
  std::string classification; ///< Classifies the molecule(s)
  std::string date; ///< Deposition date
  std::string pdb_id; ///< identifier of this PDB deposit

  /** @brief Creates a PdbField storing HEADER data
   * @param pdb_line - a HEADER pdb line
   */
  Header(const std::string & pdb_line);

  /// Returns a HEADER line created from this data
  virtual std::string to_pdb_line() const;
};

/** @brief Holds data extracted from a single <code>MODRES</code> line of a PDB file.
 */
class Modres : public PdbField {
public:
  int residue_id;
  char chain;
  char i_code;
  std::string pdb_id;
  std::string res_name;
  std::string std_res_name;
  std::string comment;

  /// Creates the object by extracting data from a PDB line
  Modres(const std::string &pdb_line);

  /// Returns a MODRES line created from this data
  std::string to_pdb_line() const;
};

class Hetnam : public PdbField {
public:
  const std::string code3; ///< Three-letter code of a ligand residue
  const std::string name;  ///< Chemical name of this ligand molecule

  /// Creates the object by extracting data from a PDB line
  Hetnam(const std::string &pdb_line) : code3(pdb_line.substr(11,3)), name(pdb_line.substr(15)) {}

  /// Returns a HETNAM line created from this data
  std::string to_pdb_line() const;
};

/** @brief Holds data extracted from a single <code>FORMUL</code> line of a PDB file.
 */
class Formula : public PdbField {
public:
  const std::string code3; ///< Three-letter code of a ligand residue
  const std::string formula;  ///< Chemical formula of this ligand molecule

  /// Creates the object by extracting data from a PDB line
  Formula(const std::string &pdb_line);

  /// Returns a FORMUL line created from this data
  std::string to_pdb_line() const;
};

/** @brief Holds data extracted from a single <code>CONNECT</code> line of a PDB file.
 */
class Conect : public PdbField {
public:

  /// Creates the object by extracting data from a PDB line
  Conect(const std::string &pdb_line);

  /** @brief Creates a CONNECT line that stores a bond between two atoms
   * @param this_atom_id - index of the first atom of a bond
   * @param bonded_atom_id  - index of the second atom of a bond
   */
  Conect(const core::index4 this_atom_id, const core::index4 bonded_atom_id);

  /** @brief Creates a CONNECT line that defines two bonds between three atoms
   * @param this_atom_id - index of the first atom of a bond
   * @param bonded_atom_id  - index of the second atom of a bond
   * @param bonded_atom_id_2  - index of the second atom of the second bond
   */
  Conect(const core::index4 this_atom_id, const core::index4 bonded_atom_id, const core::index4 bonded_atom_id_2);

  /** @brief Creates a CONNECT line that defines three bonds between four atoms
   * @param this_atom_id - index of the first atom of a bond
   * @param bonded_atom_id  - index of the second atom of a bond
   * @param bonded_atom_id_2  - index of the second atom of the second bond
   * @param bonded_atom_id_3  - index of the second atom of the third bond
   */
  Conect(const core::index4 this_atom_id, const core::index4 bonded_atom_id, const core::index4 bonded_atom_id_2,
         const core::index4 bonded_atom_id_3);

  /** @brief Creates a CONNECT line that defines four bonds between five atoms
   * @param this_atom_id - index of the first atom of a bond
   * @param bonded_atom_id  - index of the second atom of a bond
   * @param bonded_atom_id_2  - index of the second atom of the second bond
   * @param bonded_atom_id_3  - index of the second atom of the third bond
   * @param bonded_atom_id_4  - index of the second atom of the fourth bond
   */
  Conect(const core::index4 this_atom_id, const core::index4 bonded_atom_id, const core::index4 bonded_atom_id_2,
         const core::index4 bonded_atom_id_3, const core::index4 bonded_atom_id_4);

  /** @brief Creates a CONNECT line that defines five bonds between six atoms
   * @param this_atom_id - index of the first atom of a bond
   * @param bonded_atom_id  - index of the second atom of a bond
   * @param bonded_atom_id_2  - index of the second atom of the second bond
   * @param bonded_atom_id_3  - index of the second atom of the third bond
   * @param bonded_atom_id_4  - index of the second atom of the fourth bond
   * @param bonded_atom_id_5  - index of the second atom of the fifth bond
   */
  Conect(const core::index4 this_atom_id, const core::index4 bonded_atom_id, const core::index4 bonded_atom_id_2,
         const core::index4 bonded_atom_id_3, const core::index4 bonded_atom_id_4,
         const core::index4 bonded_atom_id_5);

  /// Returns a PDB-formatted CONECT line representing this object
  std::string to_pdb_line() const;

  /// Read only access to the list of bonded atoms
  const std::vector<index4> & atom_ids() const { return atom_ids_; }

private:
  std::vector<index4> atom_ids_;

  const static std::string conect_format_2;
  const static std::string conect_format_3;
  const static std::string conect_format_4;
  const static std::string conect_format_5;
  const static std::string conect_format_6;
};

/** @brief Holds data extracted from all <code>KEYWRDS</code> lines
 */
class Keywords : public PdbField {
public:

  /// Default constructor creates an empty list of PDB keywords
  Keywords() {}

  /// Parses keywords from a given  <code>KEYWRDS</code> line and appends them to the list
  void add_line(const std::string & pdb_line);

  /// Appends another keyword to this list
  void add_keyword(const std::string & keyword) { keywords_.push_back(keyword); }

  /// Returns a read-only vector of keywords
  const std::vector<std::string> & keywords() const { return keywords_; }

  std::string to_pdb_line() const;

private:
  std::vector<std::string> keywords_;
};

/** @brief Holds translation vector data from PDB header (TVECT field)
 */
class TVect : public PdbField {
public:
  index2 serial;
  double x; ///< X coordinate of the translation vector
  double y; ///< Y coordinate of the translation vector
  double z; ///< Z coordinate of the translation vector

  /** @brief Creates an object reading data from a relevant PDB line
   * @param pdb_line - a TVECT pdb line
   */
  TVect(const std::string &pdb_line);

  /** @brief Create an object from provided data
   *
   * @param serial - serianl number of this translation vector
   * @param x - X coordinate of the translation vector
   * @param y - Y coordinate of the translation vector
   * @param z - Z coordinate of the translation vector
   */
  TVect(const index2 serial, const double x, const double y, const double z) : serial(serial), x(x), y(y), z(z) {}

  /** @brief Prints a PDB formatted string holding this TVECT data
   *
   * @return a PDB-formatted line
   */
  std::string to_pdb_line() const;
};

/** @brief Holds data extracted from all <code>TITLE</code> lines
 */
class Title : public PdbField {
public:
  /// Default constructor creates an empty object
  Title() {}

  /// Adds a new <code>TITLE</code> line from a PDB header into this object
  void add_line(const std::string & pdb_line);

  /// Return the string that contains all the data for the TITLE field, combined into a single line
  std::string title() const;

  /** @brief Prints a PDB formatted string holding this TITLE data
   *
   * @return a PDB-formatted lines
   */
  std::string to_pdb_line() const;

private:
  std::vector<std::string> compound_data_;
};

/** @brief Holds data extracted from all <code>COMPND</code> lines
 *
 */
class Compnd : public PdbField {
public:
  /// Default constructor creates an empty object
  Compnd() {}

  /// Adds a new <code>COMPND</code> line from a PDB header into this object
  void add_line(const std::string & pdb_line);

  /// Return the string that contains all the data for the COMPND field, combined into a single line
  std::string compound_data() const;

  /** @brief Prints a PDB formatted string holding this COMPND data
   *
   * @return a PDB-formatted lines
   */
  std::string to_pdb_line() const;

private:
  std::vector<std::string> compound_data_;
};

/** @brief Holds a single REMARK465 line i.e. information a missing residue.
 */
class Remark465 : public PdbField {
public:
  char chain_id;
  char i_code;
  int residue_id;
  index2 model_id;
  core::chemical::Monomer residue_type;

  /** @brief Creates an object reading data from a relevant PDB line
   * @param pdb_line - a REMARK 465 PDB line
   */
  Remark465(const std::string &pdb_line);

  /** @brief Prints a PDB formatted string holding this TVECT data
   *
   * @return a PDB-formatted line
   */
  std::string to_pdb_line() const;
};

/** @brief Holds data extracted from a single <code>DBREF</code> line of a PDB file.
 *
 */
class DBRef : public PdbField {
public:
/** @name DBRef entry data
 * @see https://www.wwpdb.org/documentation/file-format-content/format33/sect3.html
 */
 //@{
  std::string pdb_code;
  char chain;
  int residue_from;
  char insert_from;
  int residue_to;
  char insert_to;
  std::string db_name;
  std::string db_accession;
  std::string db_code;
  int db_residue_from;
  char db_insert_from;
  int db_residue_to;
  char db_insert_to;
  //@}

  /** @brief Parses a single DBREF line
   *
   * @param pdb_line
   */
  DBRef(const std::string &pdb_line);

  /// Writes this entry as a PDB-formatted string
  std::string to_pdb_line() const;
};

/** @brief Represents all <code>EXPDATA</code> lines of a PDB file header.
 *
 */
class Expdata : public PdbField {
public:

  /// Adds a new <code>REMARK 290</code> line from a PDB header into this container
  void add_line(const std::string & pdb_line);

  /// Returns a PDB-style string listing all the experiment types used to determine this deposit
  virtual std::string to_pdb_line() const;

  /** @brief Returns a vector of experimental methods used to determine this deposit.
   *
   * Permitted values include (according to PDB specification):
   *   X-RAY  DIFFRACTION
   *   FIBER  DIFFRACTION,
   *   NEUTRON  DIFFRACTION,
   *   ELECTRON  CRYSTALLOGRAPHY,
   *   ELECTRON  MICROSCOPY,
   *   SOLID-STATE  NMR,
   *   SOLUTION  NMR,
   *   SOLUTION  SCATTERING
   *
   * @return a list of experimental method names
   */
  const std::vector<std::string> & methods() const { return methods_; }

  /// Returns true if X-ray crystallography was used to establish this deposit
  bool is_xray() const;

  /// Returns true if NMR spectroscopy was used to establish this deposit
  bool is_nmr() const;

  /// Returns true if electron microscopy was used to establish this deposit
  bool is_em() const;

private:
  std::vector<std::string> methods_;
};

/** @brief Represents all <code>REMARK 2</code> entries of a PDB file header and stores resolution.
 *
 */
class Remark2 : public PdbField {
public:

  /** @brief Resolution of this deposit.
   *
   * Negative value means the resolution is not available, e.g for NMR deposits
   */
  double resolution = 0.0;

  /// Adds a new <code>REMARK 2</code> line from a PDB header
  void add_line(const std::string & pdb_line);

  /// Returns a PDB-style string denoting the resolution of this deposit
  virtual std::string to_pdb_line() const;
};

/** @brief Represents  <code>REMARK 3</code> entries of a PDB file header
 *
 */
class Remark3 : public PdbField {
public:

  /** @brief  R-value of this deposit.
   *
   * Zero or negative value means the R-value is not available, e.g for NMR deposits
   */
  double r_value = 0.0;

  /** @brief  R-free value of this deposit.
   *
   * Zero or negative value means the R-free  is not available, e.g for NMR deposits
   */
  double r_free = 0.0;

  /// Adds a new <code>REMARK 2</code> line from a PDB header
  void add_line(const std::string & pdb_line);

  /// Returns a PDB-style string denoting the resolution of this deposit
  virtual std::string to_pdb_line() const;

private:
  static const std::regex r_value_1;
  static const std::regex r_value_2;
  static const std::regex r_value_3;
  static const std::regex r_value_4;
  static const std::regex free_r_value_1;
  static const std::regex free_r_value_2;
};

/** @brief Represents all <code>REMARK 290</code> entries of a PDB file header.
 *
 */
class Remark290 : public PdbField {
  typedef core::calc::structural::transformations::Rototranslation Rototranslation;
  typedef std::vector<std::string> record;

public:

  /// Adds a new <code>REMARK 290</code> line from a PDB header
  void add_line(const std::string & pdb_line);

  /// Counts symmetry operators stored in this container
  core::index2 count_operators() { return rt_.size(); }

  /// Returns a PDB-style string representing all symmetry operators stored in this container
  virtual std::string to_pdb_line() const;

  /// Returns an iterator that points to the very first symmetry operator
  std::vector<Rototranslation>::const_iterator cbegin() const { return rt_.cbegin(); }

  /// Returns an interator that points behind the last symmetry operator
  std::vector<Rototranslation>::const_iterator cend() const { return rt_.cend(); }

  /// Returns an iterator that points to the very first symmetry operator
  std::vector<Rototranslation>::iterator begin() { return rt_.begin(); }

  /// Returns an interator that points behind the last  symmetry operator
  std::vector<Rototranslation>::iterator end() { return rt_.end(); }

private:
  std::vector<record> records;
  std::vector<Rototranslation> rt_;
};

class Remark350 : public PdbField {
    typedef core::calc::structural::transformations::Rototranslation Rototranslation;
    typedef std::vector<std::string> record;

public:
    /// Returns a PDB-style string representing all symmetry operators stored in this container
    std::string to_pdb_line() const;

    /// Adds a new <code>REMARK 350</code> line from a PDB header
    void add_line(const std::string & pdb_line);

    /// Counts symmetry operators stored in this container
    core::index2 count_biomolecules() { return rt_map.size(); }

    /// Returns an iterator that points to the very first symmetry operator
    std::map<std::vector<std::string>,std::vector<Rototranslation>>::const_iterator cbegin() const { return rt_map.cbegin(); }

    /// Returns an interator that points behind the last symmetry operator
    std::map<std::vector<std::string>,std::vector<Rototranslation>>::const_iterator cend() const { return rt_map.cend(); }

    /// Returns an iterator that points to the very first symmetry operator
    std::map<std::vector<std::string>,std::vector<Rototranslation>>::iterator begin() { return rt_map.begin(); }

    /// Returns an interator that points behind the last  symmetry operator
    std::map<std::vector<std::string>,std::vector<Rototranslation>>::iterator end() { return rt_map.end(); }
private:
    core::index2 n_op=1;
    std::vector<record> records;
    std::vector<std::string> chains_codes;
    std::vector<Rototranslation> rt_;
    std::map<std::vector<std::string>,std::vector<Rototranslation>> rt_map;

};


/** @brief Represents a single <code>HELIX</code> entry of a PDB file header.
 *
 */
class HelixField : public PdbField {
public:
  core::index2 serial;
  std::string helix_id;  ///< ID of this helix

  /** @name First residue for this helix
   *  The fields provide chain_d as well as id, name and icode of the residue
   */
  //@{
  std::string chain_from; ///< chain ID
  int residue_id_from; ///< residue ID
  std::string residue_name_from; ///< residue name
  char insert_from;///< insertion code for the first residue
  //@}
  /** @name Last residue for this helix
   *  The fields provide chain_d as well as id, name and icode of the residue
   */
  //@{
  std::string chain_to; ///< chain ID
  int residue_id_to; ///< residue ID
  std::string residue_name_to; ///< residue name
  char insert_to;///< insertion code for the last residue
  //@}
  char helix_class; ///< Class of this helix
  std::string comment; ///< A comment string
  core::index2 length; ///< The number of residues that belong to this helix

  /// Creates the data structure based on a HELIX line from a PDB file
  HelixField(const std::string &pdb_line);

  virtual std::string to_pdb_line() const;

  /// Returns true if a given residue belongs to this helix
  bool is_my_residue(const core::data::structural::Residue & r);
};

/** @brief Represents a single <code>SHEET</code> entry of a PDB file header.
 *
 */
class SheetField : public PdbField {
public:
  core::index2 strand_id;  ///< ID of this strand
  std::string sheet_id;  ///< ID of the sheet this strand belong to
  core::index2 n_strands;   ///< the number of strands in this sheet

  /** @name first residue for this strand
   *  The fields provide chain_d as well as id, name and icode of the first residue of this strand
   */
  //@{
  std::string chain_from;                   ///< chain ID
  int residue_id_from;               ///< residue ID
  std::string residue_name_from;     ///< residue name
  char insert_from;                  ///< insertion code for the last residue
  //@}

  /** @name Last residue for this strand
   *  The fields provide chain_d as well as id, name and icode of the last residue of this strand
   */
  //@{
  std::string chain_to;                 ///< chain ID
  int residue_id_to;             ///< residue ID
  std::string residue_name_to;   ///< residue name
  char insert_to;                ///< insertion code for the last residue
  //@}

  int sense;

  std::string register_my_atom;
  char register_my_insert;
  std::string register_my_chain;
  std::string register_my_residue_name;
  int register_my_residue_id;
  std::string register_previous_atom;
  char register_previous_insert;
  std::string register_previous_chain;
  std::string register_previous_residue_name;
  int register_previous_residue_id;

  /// Creates the data structure based on a SHEET line from a PDB file
  SheetField(const std::string &pdb_line);
  virtual std::string to_pdb_line() const;

  /// Returns true if a given residue belongs to this strand
  bool is_my_residue(const core::data::structural::Residue & r);
};


/** @brief Represents <code>SEQRES</code> field from a PDB file header.
 *
 * \include ex_Seqres.cc
 */
class Seqres : public PdbField {
public:

  /// Map that holds a list of residues for each chain, identified by a single character
  std::map<char,std::vector<core::chemical::Monomer>> sequences;

  /// Bare constructor
  Seqres() : logger("Seqres") {}

  /// Bare virtual destructor
  inline virtual ~Seqres() {};

  /// Accumulates next <code>SEQRES</code> line from a file header
  void add_line(const std::string & seqres_line);

  /// Writes this <code>SEQRES</code> field in the PDB format
  virtual std::string to_pdb_line() const;

  /// Returns the number of chains found a PDB header
  core::index2 count_chains() { return sequences.size(); }

  /** @brief Creates a Sequence object for a chain.
   *
   * Note, that a corresponding Chain object will not be created.
   * @param chain_id - ID of a requested chain
   * @param sequence_header - a string used to describe this sequence
   */
  core::data::sequence::Sequence_SP create_sequence(const char chain_id, const std::string & sequence_header) {
    return std::make_shared<core::data::sequence::Sequence>(sequence_header.c_str(), sequences[chain_id]);
  }

private:
  utils::Logger logger;
};

/** \brief PDB class reads and writes biomolecular structures in the PDB file format.
 *
 * This parser recognises the following fields of a PDB-formatted file:
 *  - ATOM
 *  - HETATM
 *  - MODRES
 *  - FORMULA
 *  - SEQES
 *  - COMPND
 *  - DBREF
 *  - CONNECT
 *  - EXPDATA
 *  - TVECT
 *  - HEADER
 *  - HELIX
 *  - SHEET
 *  - KEYWRDS
 *  - TER
 *  - TVECT
 *  - REMARK: 2 (resolution), 3 (R-value and R-free), 290 (crystallogaphic symmetry) and 465 (missing residues)
 *
 * The following example reads a PDB file (backbone atoms only) and prints some basic statistics:
 * \include ex_Pdb.cc
 */
class Pdb {
public:

  /** @brief Defines string constants corresponding to pre-defined PdbLineFilter objects.
   *
   * These constants may be passed to a PDB reader constructor. This results in the same behavior as passing
   * PdbLineFilter variables themselves.
   * The constant values are typically the same as the names of filter variables, i.e.:
   * "is_ca", "is_bb", "is_cb", "is_hydrogen", "is_hetero_atom", "is_standard_atom",
   * "is_not_hydrogen", "is_not_alternative", "is_not_water", "is_water"
   */
  static const std::vector<std::string> pdb_filter_names;

  /** @brief Contains objects created by parsing PDB file header.
   *
   * Because there might be several entries of the same type in a header (e.g. "SHEET"), the header data container
   * must be implemented as <code>std::multimap</code> rather than <code>std::map</code>. Here it is how one can
   * iterate over one type of fields:
   * <code><pre>
  std::pair<Pdb::HeaderIterator, Pdb::HeaderIterator> range = reader.header.equal_range("HELIX");
  for (Pdb::HeaderIterator it = range.first; it != range.second; ++it)
    std::cerr << (*it).second->to_pdb_line() << "\n";
</pre></code>
   */
  std::multimap<std::string, std::shared_ptr<PdbField>> header;

  /// For convenience, lets define the iterator type for the header data container.
  typedef typename std::multimap<std::string,std::shared_ptr<PdbField>>::iterator HeaderIterator;

  std::vector<std::shared_ptr<std::vector<Atom>>> atoms;  ///< All the atoms found in the PDB file; not yet assigned to any residue
  std::vector<std::shared_ptr<std::vector<Ter>>> termini; ///< TER markers for every chain in each model

  /** \brief Constructor reads and parses PDB file but does not create any core::data::structural::Structure object
   *
   * For example, to read only protein backbone and parse the header of a PDB file, use:
   * @code
   * Pdb reader("infile.pdb",is_bb,true); // 'true' says that the header should be parsed
   * @endcode
   * Now we can ask the reader to create a structure (i.e. an object that represents a protein) from the first (indexed by 0) model stored in the PDB file.
   * @code
   * core::data::structural::Structure_SP backbone = reader.create_structure(0);
   * @endcode
   *
   * @param fname_or_data - input file name or PDB data itself - as a multiline string
   * @param atom_predicate - object that says whether an atom line (either HETATM or ATOM) should be parsed or not;
   *    by default all lines are parsed
   * @param header_predicate - object that says whether a header line should be parsed or not;
   *    by default all lines are parsed if @code if_parse_header @endcode flag is set to true.
   * @param if_parse_header - header is not parsed by default; say <code>true</code> to parse it
   * @param first_model_only - load only first model from a file and stop reading
   */
  Pdb(const std::string & fname_or_data, const PdbLineFilter & atom_predicate = keep_all,
      const PdbLineFilter & header_predicate = keep_all, const bool if_parse_header = false,
      const bool first_model_only = false);

  /** @brief  Constructor reads and parses PDB file but does not create any core::data::structural::Structure object
   * @param fname_or_data  - input file name or PDB data itself - as a multiline string
   * @param pdb_filter_names - a single string containing zero (empty string), one or more PDB line filter names.
   * Use a space character to separate filter names, logical AND operator will be applied in such  cases
   * @param if_parse_header - header is not parsed by default; say <code>true</code> to parse it
   * @param first_model_only - load only first model from a file and stop reading
   *
   * For example, use the following to read only backbone atoms:
   * @code
   * Pdb reader("infile.pdb","is_bb",true); // 'true' says that the header should be parsed
   * @endcode
   */
  Pdb(const std::string & fname_or_data, const std::string & pdb_filter_names,
      const bool if_parse_header = false, const bool first_model_only = false);

  /** \brief Constructor reads and parses PDB data from a stream; it does not create any core::data::structural::Structure object
   *
   * @param infile - input stream with PDB-formatted data
   * @param atom_predicate - object that says whether an atom line (either HETATM or ATOM) should be parsed or not;
   *    by default all lines are parsed
   * @param header_predicate - object that says whether a header line should be parsed or not;
   *    by default all lines are parsed if @code if_parse_header @endcode flag is set to true.
   * @param if_parse_header - header is not parsed by default; say <code>true</code> to parse it
   * @param first_model_only - load only first model from a file and stop reading
   * @see Pdb(const std::string, const PdbLineFilter &, const bool)
   */
  Pdb(std::istream & infile, const PdbLineFilter & atom_predicate = keep_all,
      const PdbLineFilter & header_predicate = keep_all,
      const bool if_parse_header = false, const bool first_model_only = false) {
    read_pdb(infile, atom_predicate, header_predicate, if_parse_header, first_model_only);
  }

  /** @brief Returns the PDB code of a deposit : four-character string.
   *
   * @returns four-character string, e.g. 2AZA, 2GB1, 1HLB etc
   */
  std::string pdb_code() const;

  /// Returns the number of models in the PDB deposit that has been loaded
  core::index2 count_models() const { return atoms.size(); }

  /**
   * @brief Returns the number of atoms loaded for a given model
   * @param which_model - index of a model
   * @return the number of atoms loaded in a requested model. Atom lines that do not satisfy PdbAtomFilter
   * used at PDB loading are not counted
   */
  core::index2 count_atoms(core::index4 which_model) const { return atoms[which_model]->size(); }

  /** @brief Creates a Structure object from a model stored in the PDB data structure
   *
   * @param which_model - model index; starts from 0
   * @returns pointer to the newly created Structure object
   */
  core::data::structural::Structure_SP create_structure(const core::index2 which_model);

  /** @brief Creates all Structure objects that are available in this PDB stream
   *
   * @param structures - vector to hold created structures
   */
  void create_structures(std::vector<core::data::structural::Structure_SP> & structures);

  /** @brief Read coordinates of a given model and store them in a given structure.
   *
   * Original coordinates are lots; this method changes coordinates, assuming that atom types and residue assignment
   * in the given model is the same as in the structure
   *
   * @param structure - a structure to store coordinates
   */
  void fill_structure(const core::index2 which_model, core::data::structural::Structure & structure);

  /** @brief Read coordinates of a given model and store them in a given vector of points.
   *
   * The purpose of this method is to load quickly coordinates from a PDB file when chain/residue/atom hierarchy
   * is not important.
   * Original coordinates stored in the given <code>xyz</code> vector are lots; this method overwrites them,
   * assuming that the given vector provides enough space
   *
   * @param which_model - index of a model to be loaded (from 0)
   * @param xyz - a vector to store coordinates
   */
  void fill_structure(const core::index2 which_model, std::vector<core::data::basic::Vec3> & xyz);

  /** @brief Read coordinates of a given model and store them in a given vector of points in periodic box.
   *
   * The purpose of this method is to load quickly coordinates from a PDB file when chain/residue/atom hierarchy
   * is not important. Coordinates are loaded into vectors living in periodic boundary conditions, thus this
   * method can be used to load MD/MC trajectories.
   * Original coordinates stored in the given <code>xyz</code> vector are lots; this method overwrites them,
   * assuming that the given vector provides enough space
   *
   * @param which_model - index of a model to be loaded (from 0)
   * @param xyz - a vector to store coordinates
   */
   void fill_structure(const core::index2 which_model, std::vector<core::data::basic::Vec3I> & xyz);

  /// Returns all symmetry operators stored in this PDB reader
  std::shared_ptr<Remark290> symmetry_operators() { return remark290_; }

    /// Returns all symmetry operators stored in this PDB reader
    std::shared_ptr<Remark350> biomolecule_symmetry() { return remark350_; }

  /** @brief Creates Vec3 object for each valid <code>ATOM</code> line in the input PDB data.
   *
   * Resulting Vec3 objects will be inserted into a given std::vector container.
   *
   * @param infile - stream providing input data
   * @param destination - vector where the data should go; should be large nough to fit the coordinates,
   *	otherwise say if_push_back = true
   * @param if_push_back - when false (which is the default), this method assumes the vector of coordinates is 
   *	long enough to accomodate all atoms, If <code>true</code>, then the given destination vector is emptied
   *	and all the atoms are pushed back to it
   * @param predicate - coordinates will be parsed if and only if a given PDB line satisfies a predicate. By default PdbLineFilter::is_ca predicate is used
   * so only C\f$\alpha\f$ atom are parsed and inserted into the destination vector.
   * @returns the number of atoms created
   */
  static core::index4 read_coordinates(std::istream & infile, std::vector<core::data::basic::Vec3> & destination,
    const bool if_push_back = false, const PdbLineFilter & predicate = is_ca);

  /** @brief Creates Vec3 object for each valid <code>ATOM</code> line in the input PDB data.
   *
   * Resulting Vec3 objects will be inserted into a given std::vector container.
   *
   * @param fname - name of the input PDB file
   * @param destination - vector where the data should go
   * @param if_push_back - when false (which is the default), this method assumes the vector of coordinates is 
   *	long enough to accomodate all atoms, If <code>true</code>, then the given destination vector is emptied
   *	and all the atoms are pushed back to it
   * @param predicate - coordinates will be parsed if and only if a given PDB line satisfies a predicate. By default PdbLineFilter::is_ca predicate is used
   * so only C\f$\alpha\f$ atom are parsed and inserted into the destination vector.
   * @returns the number of atoms created
   */
  static core::index4 read_coordinates(const std::string & fname, std::vector<core::data::basic::Vec3> & destination,
    const bool if_push_back = false, const PdbLineFilter & predicate = is_ca);

private:
  static utils::Logger logger;
  std::string fname_;
  std::shared_ptr<Remark290> remark290_;
  std::shared_ptr<Remark350> remark350_;

  void read_pdb(std::istream & infile, const PdbLineFilter & atom_predicate = keep_all,
      const PdbLineFilter & header_predicate = keep_all, const bool if_parse_header = true,
      const bool first_model_only = false);

  /** @brief Creates a PdbLineFilter instance based on a string initializer.
   *
   * This method creates a filter that rules ATOM / HETATM lines.  To filter header lines,
   * use @code header_line_filters_factory() @endcode
   * Currently implemented filter names are:
   * "is_ca", "is_bb", "is_cb", "is_hydrogen", "is_hetero_atom", "is_standard_atom", "is_not_hydrogen",
   * "is_not_alternative", "is_not_water", "is_water"
   *
   * @param pdb_filter_names_string - a string containing one or more pdb_filter_names, separated with a space.
   * @param filters_map - map  filters names to procedures
   * @return an object that may be used to filter PDB lines
   */
  static PdbLineFilter pdb_line_filters_factory(const std::string & pdb_filter_names_string,
      std::map<std::string const, PdbLineFilter> & filters_map);
};

/** \brief Tries to find a PDB file in a directory
 *
 * <p>Looks in the specified directory for a file with a given PDB data, identified by
 * a given PDB code. For a given 4-character ID (digit + 3 letters) the method checks
 * the following possibilities:
 *     - given_path/1abc
 *     - given_path/1ABC
 *     - given_path/1abc.pdb
 *     - given_path/1ABC.pdb
 *     - given_path/1ABC.PDB
 *     - given_path/pdb1abc
 *     - given_path/PDB1ABC
 *     - given_path/pdb1abc.ent
 *     - given_path/PDB1ABC.ent
 *     - given_path/pdb1abc.ent.gz
 *     - given_path/PDB1ABC.ent.gz
 *     - given_path/ab/pdb1abc.ent
 *     - given_path/ab/pdb1abc.ent.gz
 * where 1abc and 1ABC denote a lower-case and an upper-case PDB ID, respectively.</p>
 *
 * @param pdbCode four-character PDB ID
 * @param pdbPath directory to look into
 *
 * @return a name of PDB file that was found or an empty string
 */
std::string find_pdb_file_name(const std::string &pdbCode, const std::string &pdbPath);

/** @brief Finds a PDB file on a local file system and returns a PDB reader object which loaded that file
 *
 * @param pdb_code - pdb code or a file name
 * @path path where the file should be located
 * @param if_parse_header - use false to skip header processing
 * @param if_first_model_only - when true, this method will load only the first model (set to <code>false</code> by default)
 */
std::shared_ptr<Pdb> find_pdb(const std::string &pdb_code, const std::string &pdb_path,
    const bool if_parse_header = true, const bool if_first_model_only = false);

/** @brief Writes a given structure in the PDB format.
 *
 * This method simply calls <code>PdbAtom::to_pdb_line()</code> for each atom found in the given structure.
 * @param structure - points to the structure to be written
 * @param out - output stream
 */
void write_pdb(const core::data::structural::Structure_SP structure, std::ostream & out);

/** @brief Writes a given structure in the PDB format.
 *
 * This method simply calls <code>PdbAtom::to_pdb_line()</code> for each atom found in the given structure.
 * @param structure - points to the structure to be written
 * @param file_name - output file name
 * @param model_id - if higher that 0, the structure will be appended to the file as a model
 */
void write_pdb(const core::data::structural::Structure_SP structure, const std::string & file_name, const index4 model_id = 0);

/// Returns true if the given string is a valid  PDB code
bool is_pdb_code(const std::string & code);

/** @brief Attempts to extract a PDB code from a file name.
 * This method only strips off commonly used prefixes such as "pdb" and extensions (e.g., .pdb, .pdb.gz, .ent and so on)
 * Actually, it uses the same set of prefixes and suffixes as <code>find_pdb()</code> method
 * It is not guaranteed this method returns a string that would be accepted by <code>is_pdb_code()</code> method
 */
std::string pdb_code_from_file_name(const std::string & code);

void rototranslation_to_pdb(
        std::vector<calc::structural::transformations::Rototranslation, std::allocator<calc::structural::transformations::Rototranslation>> input, std::vector<std::string> & out_lines);

} // ~ io
} // ~ data
} // ~ core

#endif

/**
 *  \example ex_Pdb.cc
 *  \example ex_Seqres.cc
 *  \example ap_filter_pdb.cc
 */
