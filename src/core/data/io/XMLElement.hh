#ifndef CORE_DATA_IO_XMLElement_H
#define CORE_DATA_IO_XMLElement_H

#include <string>
#include <vector>
#include <memory>
#include <map>

#include <utils/Logger.hh>
#include <utils/io_utils.hh>
#include <core/algorithms/trees/TreeNode.hh>

namespace core {
namespace data {
namespace io {

using namespace core::algorithms::trees;

class XMLElement;

 /** @brief Represents a single XML element data
  *
  * This object stores name of the respective element, its attributes and their values. For a given example XML element:
<code><pre>

<options type="color">
  <item shade="bright" value="Purple" />
  <item>Green</item>
  <item>Orange</item>
</options>

</pre></code>   
   * this object will hold <code>name</code> equals to <code>options</code>; the <code>content</code> std::map will contain
   * only one attribute ("type") associated with the value ("color")
   */
class XMLElementData {
public:

  /** @brief <code>key="property value"</code> attributes and their data
   *
   * This map stores attributes and their values that are assigned to this XML element. For a  XML example given above
   * this object  will hold only one attribute ("type") associated with the value ("color")
   */
  std::map<std::string, std::string> content; 

  /** @brief Returns a value assigned to a given attribute of this XML element.
   *
   * E.g. for the key <code>"type"</code> returns <code>"color"</code> when the XML element was <code><options type="color" /></code>
   * @returns attribute value as a string const-reference
   */
  const std::string & at(const std::string & key) const { return content.at(key); }

  /** @brief Returns a value assigned to a given attribute of this XML element.
   *
   * E.g. for the key <code>"type"</code> returns <code>"color"</code> when the XML element was <code><options type="color" /></code>
   * @returns attribute value as a string
   */
  std::string & at(const std::string & key) { return content.at(key); }

  /// Returns the name of this XML element
  const std::string & name() const { return name_; }
  
  /// Returns the number of attributes describing this element
  size_t size() const { return content.size(); }

  /// Returns an iterator referring to the first attribute of this element
  std::map<std::string, std::string>::iterator begin() { return content.begin(); }

  /// Returns an iterator referring to the past-the-end attribute of this element
  std::map<std::string, std::string>::iterator end() { return content.end(); }

  /// Returns a const iterator referring to the first attribute of this element
  std::map<std::string, std::string>::const_iterator cbegin() const { return content.cbegin(); }

  /// Returns a const_iterator referring to the past-the-end attribute of this element
  std::map<std::string, std::string>::const_iterator cend() const { return content.cend(); }
  
private:
  friend XMLElement;
  std::string name_;   ///< Name of this element
};

/** @brief XMLElement is just a tree node of XMLElementData.
 *
 */
class XMLElement : public TreeNode<XMLElementData> {
public:

  friend std::ostream& operator<<(std::ostream &out, const std::shared_ptr<XMLElement> e);
  friend std::ostream& operator<<(std::ostream &out, const XMLElement & e);


  /** @brief Creates a new node for a given tag name.
   * This constructor creates a node which will be written as
   * <code>
   * <element_tag></element_tag>
   * <code>
   */
  XMLElement(const int id, const std::string & element_name) :
      TreeNode<XMLElementData>(id), logger("XMLElement") {

    element.name_ = element_name;
  }

  virtual ~XMLElement()= default;

  /** @brief Returns true if this element has no children neither any content; only properties are allowed in a simple node.
   */
  bool is_simple_element() const { return count_branches() == 0 && text_.size() == 0; }

  /** \brief Sets the text stored at this element.
   *
   * @param new_text - the text that will be stored at this element node
   */
  inline void text(const std::string & new_text) { text_ = new_text; }

  /** \brief Returns the text stored at this element.
   *
   * This method access the content of this element that is not an XML element by itself
   * @returns the text that is stored at this element node
   */
  inline const std::string & text() const { return text_; }

  /** \brief Sets the name of this element.
   *
   * @param new_name - the new name of this element; will be stored in XMLElementData instance in this node
   */
  inline void name(const std::string & new_name) { element.name_ = new_name; }

  /** \brief Returns the name of this XML element.
   */
  inline const std::string & name() const { return element.name(); }

  /** \brief Returns the value assigned to a given attribute.
   *
   * @param attribute_name - name of the requested attribute, e.g. "cmd" for:
   * <external cmd="blastpgp">
   * This method should return the "blastpgp" string for this example
   */
  const std::string & attribute(const std::string attribute_name) { return element.at(attribute_name); }

  /// Returns true if the given attribute has been defined for this element
  bool has_attribute(const std::string attribute_name) { return element.content.find(attribute_name)!=element.content.end(); }

  /** \brief Returns the value for a given key.
   *
   * This method first looks for an attribute named <code>key</code> and when found, returns the value associated with it.
   * Otherwise it checks the children elements and looks for the first one named <code>key</code>.
   * When this is found, returns the text stored in the child node
   * Otherwise, returns an empty string.
   *
   * @param key - name of an attribute or child element
   * @return a string associated with the given key, or an empty string
   */
  const std::string & find_value(const std::string key) {

    if (element.content.find(key) != element.content.end()) return element.at(key);
    for(core::index2 i=0;i<count_branches();++i) {
      std::shared_ptr<XMLElement> bi = std::static_pointer_cast<XMLElement>(get_branch(i));
      if(bi->name().compare(key) == 0) return bi->text_;
    }

    return EMPTY;
  }

  /** \brief Returns the value for a given key.
   *
   * This method first looks for an attribute named <code>key</code> and when found, returns the value associated with it.
   * Otherwise it checks the children elements and looks for the first one named <code>key</code>.
   * When this is found, returns the text stored in the child node
   * Otherwise, returns the provided <code>empty_value</code>
   *
   * @param key - name of an attribute or child element
   * @param empty_value - value returned when the given key can't be found
   * @return a value stored with a given key
   */
  template<typename T>
  T find_value(const std::string key, T empty_value) {

    if (element.content.find(key) != element.content.end()) return utils::from_string<T>(element.at(key));
    for(core::index2 i=0;i<count_branches();++i) {
      std::shared_ptr<XMLElement> bi = std::static_pointer_cast<XMLElement>(get_branch(i));
      if(bi->name().compare(key) == 0) return utils::from_string<T>(bi->text_);
    }

    return empty_value;
  }

  /// Returns an iterator referring to the first attribute of this element
  std::map<std::string, std::string>::iterator attributes_begin() { return element.content.begin(); }

  /// Returns an iterator referring to the past-the-end attribute of this element
  std::map<std::string, std::string>::iterator attributes_end() { return element.content.end(); }

  /// Returns a const iterator referring to the first attribute of this element
  std::map<std::string, std::string>::const_iterator attributes_cbegin() const { return element.content.cbegin(); }

  /// Returns a const_iterator referring to the past-the-end attribute of this element
  std::map<std::string, std::string>::const_iterator attributes_cend() const { return element.content.cend(); }

private:
  std::string text_;
  utils::Logger logger;
  static const std::string EMPTY;
};

std::ostream& operator<<(std::ostream &out, const std::shared_ptr<XMLElement> e);

}
}
}

#endif
