#ifndef CORE_DATA_BASIC_Array3D_H
#define CORE_DATA_BASIC_Array3D_H

#include <utility>
#include <tuple>
#include <vector>
#include <stdexcept>
#include <algorithm>		// for std::min_element() and std::make_tuple()
#include <utils/string_utils.hh>

namespace core {
namespace data {
namespace basic {

/** @brief 3D matrix implemented with data stored as std::vector<T>
 */
template<typename T>
class Array3D {
public:
  /** @brief Creates a new matrix.
   *
   * @param n_rows - the number of rows
   * @param n_columns  - the number of columns
   * @param n_layers - the number of layers
   */
  Array3D(const index2 n_rows, const index2 n_columns, const index2 n_layers) : n_rows(n_rows), n_columns(n_columns),
                                                                                n_layers(n_layers) {
    if ((n_columns == 0) || (n_rows == 0) || (n_layers == 0)) throw std::invalid_argument("Invalid array size");
    the_data.resize(n_rows * n_columns * n_layers);
    size = n_rows * n_columns * n_layers;
  }

  /// Returns the number of rows of this matrix
  size_t count_rows() const { return n_rows; }

  /// Returns the number of columns of this matrix
  size_t count_columns() const { return n_columns; }

  /// Returns the number of layers of this matrix
  size_t count_layers() const { return n_layers; }

  /// Returns 1D style index of a particular (row,column,layer) entry
  size_t to1D(const size_t row, const size_t column, const size_t layer) const {
    return layer * n_layers * n_rows + row * n_columns + column;
  }

  /// Returns 3D style (row,column,layer) index of a particular entry in the internal storage
  std::vector<size_t> to3D(const size_t index) const {
    std::vector<size_t> result;
    size_t temp = index;
    result.push_back(temp / (n_layers * n_rows));
    temp -= temp / (n_layers * n_rows);
    result.push_back(temp / n_columns);
    result.push_back(temp % n_columns);
    return result;
  }

  /// Returns a const-value stored in the 1D internal vector at a given index
  T const &operator[](const size_t index) const { return the_data[index]; }

  /// Returns a value stored in the 1D internal vector at a given index
  T &operator[](const size_t index) { return the_data[index]; }

  /// Returns a const-value stored in the certain element of the matrix
  T const &operator()(const size_t row, const size_t column, size_t layer) const {
    return the_data[to1D(row, column, layer)];
  }

  /// Returns a value stored in the certain element of the matrix
  T &operator()(const size_t row, const size_t column, const size_t layer) {
    return the_data[to1D(row, column, layer)];
  }

  /// Returns a value stored in the certain element of the matrix
  T &get(const size_t row, const size_t column, const size_t layer) { return the_data[to1D(row, column, layer)]; }

  /// Returns a const-value stored in the certain element of the matrix
  const T &get(const size_t row, const size_t column, const size_t layer) const { return the_data[to1D(row, column, layer)]; }

  /// Sets a value for the certain element of the matrix
  void set(const size_t row, const size_t column, const size_t layer, T t) { the_data[to1D(row, column, layer)] = t; }

  /** @brief Copies the content from the given data container into this matrix
   *
   * @param data - input data
   * @tparam D - a 1D data container type, e.g. <code>std::vector<double> </code>
   */
  template<typename D>
  void set(const D &data) {
    index4 i = 0;
    for (auto it = data.begin(); it != data.end(); ++it) {
      the_data[i] = data[i];
      ++i;
    }
  }

  /** Adds a given value to an element of this array.
 *
 * This method is provided for efficient accumulation of properties related to a 2D index
 *
 * @param row - row index (index in the first dimension)
 * @param column - column index (index in the second dimension)
 * @param layer - layer index (index in the third dimension)
 * @param inc_val - the value to be added
 */
  void add(const size_t row, const size_t column, const size_t layer, T inc_val) { the_data[to1D(row, column, layer)] += inc_val; }

  /** @brief Clears the matrix with the given zero-value.
   *
   * @param data - value used to fill the whole matrix
   */
  void clear(const T data) { for (index4 i = 0; i < size; ++i) the_data[i] = data; }

  /// Exposes the internal vector of data
  const std::vector<T> &expose_vector() const { return the_data; }

///  returns aperiodic cubic neighbors for a given 1D point
   std::vector<T> get_cubic_neighbors(const T point) const {

    std::vector<T> neighbors;
    if ((point + 1) % n_columns != 0) neighbors.push_back(point + 1);
    if (point % n_columns != 0) neighbors.push_back(point - 1);
    if (point % (n_columns * n_layers) + n_rows < n_rows * n_columns) neighbors.push_back(point + n_rows);
    if (point % (n_columns * n_layers) >= n_rows) neighbors.push_back(point - n_rows);
    if (point + (n_rows * n_layers) < (n_rows * n_layers * n_columns)) neighbors.push_back(point + n_rows * n_layers);
    if (point - (n_rows * n_layers) >= 0) neighbors.push_back(point - n_rows * n_layers);
    return neighbors;
  }

protected:
  std::vector<T> the_data; ///< the 3D data is actually stored in 1D std::vector
  const index2 n_rows; ///< the number of rows (<code>i</code> index of an element)
  const index2 n_columns; ///< the number of columns (<code>j</code> index of an element)
  const index2 n_layers; ///< the number of layers (<code>k</code> index of an element)
  index2 size; ///< size of this container i.e. its capacity
};

}
}
}

#endif




