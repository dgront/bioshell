#ifndef CORE_DATA_BASIC_Array2DStatic_H
#define CORE_DATA_BASIC_Array2DStatic_H

#include <utility>

namespace core {
namespace data {
namespace basic {

template<typename T, size_t N_rows, size_t N_columns>
class Array2DStatic {
public:
	size_t n_rows() const { return N_rows; }

	size_t n_columns() const { return N_columns; }

	size_t to1D(size_t row, size_t column) const { return row * N_columns + column; }

	std::pair<size_t, size_t> to2D(size_t index) const { return std::make_pair(index / N_columns, index % N_columns); }

	T const &operator[](size_t index) const { return the_data[index]; }

	T &operator[](size_t index) { return the_data[index]; }

	T const &operator()(size_t row, size_t column) const { return the_data[to1D(row, column)]; }

	T &operator()(size_t row, size_t column) { return the_data[to1D(row, column)]; }

	T & get(size_t row, size_t column) { return the_data[to1D(row, column)]; }

	void set(size_t row, size_t column, T t) { the_data[to1D(row, column)] = t; }

	T const * expose() const { return the_data; }

	T * expose() { return the_data; }

	T const *begin() const { return the_data; }

	T *begin() { return the_data; }

	T const *end() const { return the_data + N_rows * N_columns; }

	T *end() { return the_data + N_rows * N_columns; }

private:
	T the_data[N_rows * N_columns];
};

}
}
}

#endif
