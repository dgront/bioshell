#ifndef CORE_DATA_BASIC_Array2DSymmetric_H
#define CORE_DATA_BASIC_Array2DSymmetric_H

#include <core/data/basic/Array2D.hh>

#include <utils/string_utils.hh>

namespace core {
namespace data {
namespace basic {

/** @brief 2D matrix implemented with data stored as std::vector<T>
 *
 * @input ex_basic_algebra.cc
 * @tparam T - type of the data stored in the matrix
 *
 * @example ex_Array2DSymmetric.cc
 */
template<typename T>
class Array2DSymmetric : public Array2D<T> {
public:
  /** @brief Creates a new matrix.
   *
   * @param matrix_size - the number of rows and columns (must be equal since the matrix is symmetric)
   */
  Array2DSymmetric(const index2 matrix_size) : Array2D<T>( matrix_size, matrix_size) {}

  /** @brief Creates a new matrix.
   *
   * @param n_rows - the number of rows
   * @param n_columns  - the number of columns
   * @param data - data used to fill the matrix
   */
  Array2DSymmetric(const index2 matrix_size, const std::vector<T> & data)
    : Array2D<T>(matrix_size, (matrix_size + 1) / 2, data) {}

  /** @brief Reads Array2D data from a file.
   *
   * The file must have a flat table format. All data entries from the file will be converted to the type T
   *
   * @param file_name - name of the input file
   * @return  array containing the data
   */
  static Array2DSymmetric from_file(const std::string & file_name) {

    core::data::io::DataTable dt;
    dt.load(file_name);
    core::index2 nrow = dt.size();
    core::index2 ncol = dt[0].size();

    Array2DSymmetric a(nrow, ncol);
    for (core::index2 i = 0; i < nrow; ++i) {
      const core::data::io::TableRow &tr = dt[i];
      for (core::index2 j = 0; j < ncol; ++j) a(i, j) = tr.get<T>(j);
    }

    return a;
  }

  /// Returns the number of rows of this matrix
  size_t count_rows() const { return Array2D<T>::n_rows; }

  /// Returns the number of columns of this matrix
  size_t count_columns() const { return Array2D<T>::n_rows; }

  /// Returns a value stored in the certain element of the matrix
  T const &operator()(const size_t row, const size_t column) const { return Array2D<T>::the_data[to1D(row, column)]; }

  /// Returns a value stored in the certain element of the matrix
  T &operator()(const size_t row, const size_t column) { return Array2D<T>::the_data[to1D(row, column)]; }

  /// Returns a value stored in the certain element of the matrix
  T &get(const size_t row, const size_t column) { return Array2D<T>::the_data[to1D(row, column)]; }

  /// Returns a const-value stored in the certain element of the matrix
  const T & get(const size_t row,const size_t column) const { return Array2D<T>::the_data[to1D(row, column)]; }

  /// Sets a value for the certain element of the matrix
  void set(const size_t row,const  size_t column, T t) { Array2D<T>::the_data[to1D(row, column)] = t; }

  /// Returns 1D style index of a particular (row,column) entry
  inline size_t to1D(const size_t row, const size_t column) const {
    return (row < column) ? row * Array2D<T>::n_columns + column : column * Array2D<T>::n_rows + row;
  }

  /// Returns 2D style (row,column) index of a particular  entry in the internal storage
  std::pair<size_t, size_t> to2D(const size_t index) const { return std::make_pair(index / Array2D<T>::n_columns, index % Array2D<T>::n_columns); }

  /** @brief prints the matrix nicely
 *
 * @param format - format used to print a single matrix value
 * @param out_stream - stream to send the data
 */
  void print(const std::string &format, std::ostream &out_stream) {

    for (index2 i = 0; i < Array2D<T>::n_rows; i++) {
      for (index2 j = 0; j < Array2D<T>::n_columns; j++) out_stream << utils::string_format(format, get(i,j));
      out_stream << std::endl;
    }
  }
};

}
}
}
/**
 * @include  ex_Array2DSymmetric.cc
 */
#endif
