#ifndef CORE_DATA_BASIC_ThreadSafeMap_H
#define CORE_DATA_BASIC_ThreadSafeMap_H

#include <map>
#include <mutex>
#include <iterator>

namespace core {
namespace data {
namespace basic {

/** @brief Extension of <code>std::map<Key, T> </code> which is thread safe
 *
 * @tparam Key - type used as a key for mapping
 * @tparam T  - type of data stored in this map
 */
template<typename Key, typename T>
class ThreadSafeMap : private std::map<Key, T> {
public:

  /** @brief access an element of this map
   *
   * @param key - key related to a requested value
   * @return stored value
   */
  const T &at(const Key &key) const;

  /** @brief access an element of this map
   *
   * @param key - key related to a requested value
   * @return stored value
   */
  const T &operator[](const Key &key) const;

  /** @brief returns true if this map contains no elements
   *
   * @return true if the map is empty
   */
  bool empty() const { return std::map<Key, T>::empty(); }

  /** @brief returns the number of elements stored in this map
   *
   * @return the number of elements stored in this map
   */
  size_t size() const noexcept { return std::map<Key, T>::size(); }


  /** @brief Finds an element in this map
   *
   * @return true if this map contains a given key
   */
  bool contains(const Key &key) const;

  /** @brief Insert or update an element
   *
   * @param key - the key
   * @param value - the value assigned to the key
   * @return true when the insertion actually happened
   */
  bool insert_or_assign(const Key& key, T & value);

private:
  mutable std::mutex lock;
};

template<typename Key, typename T>
const T &ThreadSafeMap<Key, T>::at(const Key &key) const {

  lock.lock();
  const T &v = std::map<Key, T>::at(key);
  lock.unlock();
  return v;
};

template<typename Key, typename T>
const T &ThreadSafeMap<Key, T>::operator[](const Key &key) const {

  lock.lock();
  const T &v = std::map<Key, T>::at(key);
  lock.unlock();
  return v;
};

template<typename Key, typename T>
bool ThreadSafeMap<Key, T>::contains(const Key &key) const {

  bool flag = false;
  lock.lock();
  flag = (std::map<Key,T>::find(key) != std::map<Key,T>::end());
  lock.unlock();

  return flag;
};

template<typename Key, typename T>
bool ThreadSafeMap<Key, T>::insert_or_assign(const Key &key, T &value) {

  lock.lock();
  auto it = std::map<Key, T>::find(key);
  if (it == std::map<Key, T>::end()) {
    std::map<Key, T>::insert(std::pair<Key, T>(key, value));
  }
  else {
    std::map<Key, T>::at(key) = value;
  }
  lock.unlock();

  return true;
};


}
}
}

#endif