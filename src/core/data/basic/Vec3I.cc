#include <iostream>

#include <core/data/basic/Vec3I.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace core {
namespace data {
namespace basic {

double Vec3I::box_length_ = (1 << 12);
double Vec3I::int_to_real_ = box_length_ / std::numeric_limits<COORDINATE_VALUE>::max();
double Vec3I::int_to_real_2_ = int_to_real_ * int_to_real_;

utils::Logger logs("Vec3I");

void Vec3I::set_box_len(const double new_box_len) {
  box_length_ = new_box_len / 2.0;
  int_to_real_ = box_length_ / std::numeric_limits<COORDINATE_VALUE>::max();
  int_to_real_2_ = int_to_real_ * int_to_real_;
  logs << utils::LogLevel::INFO << "cubic box length for PBC set to " << box_length_ * 2.0 <<", each coordinate in the range: ["
      << -box_length_<<", "<<box_length_
       << "], coordinate accuracy is " << int_to_real_ << "\n";
}

COORDINATE_VALUE Vec3I::wrap(double n) {
  double tmp = n / int_to_real_;
  if (tmp > std::numeric_limits<COORDINATE_VALUE>::max()) {
    tmp -= std::numeric_limits<COORDINATE_VALUE>::max();
    return -(std::numeric_limits<COORDINATE_VALUE>::max() - tmp);
  } else if (tmp < -std::numeric_limits<COORDINATE_VALUE>::max()) {
    tmp += std::numeric_limits<COORDINATE_VALUE>::max();
    return std::numeric_limits<COORDINATE_VALUE>::max() + tmp;
  } else
    return COORDINATE_VALUE(n / int_to_real_);
}

double Vec3I::length() const {

  double l = x();
  double l2 = l * l;
  l = y();
  l2 += l * l;
  l = z();
  l2 += l * l;
  return sqrt(l2);
}

void Vec3I::norm(const double new_length) {
  operator*=(new_length / length());
}

std::ostream& operator<<(std::ostream &out, const Vec3I &v) {

  out << utils::string_format("%8.3f %8.3f %8.3f", v.x(), v.y(), v.z());
	return out;
}

utils::Logger &operator<<(utils::Logger &logger, const Vec3I &coordinates) {

  logger << utils::string_format("%8.3f %8.3f %8.3f", coordinates.x(), coordinates.y(), coordinates.z());

  return logger;
}

} // ~ basic
} // ~ data
} // ~ core
