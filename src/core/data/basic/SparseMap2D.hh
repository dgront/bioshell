#ifndef CORE_DATA_BASIC_SparseMatrix2D_H
#define CORE_DATA_BASIC_SparseMatrix2D_H

#include <utility>
#include <tuple>
#include <map>
#include <stdexcept>
#include <algorithm>		// for std::min_element() and std::make_tuple()

#include <utils/string_utils.hh>

namespace core {
namespace data {
namespace basic {

/** @brief Sparse matrix is implemented as a std::map of std::maps
 *
 * @tparam K - a key type used to index the matrix; usually an integer type
 * @tparam D -  data type stored in this matrix
 */
template<typename K, typename D>
class SparseMap2D {
public:

  class const_iterator {
  public:

      typedef typename std::map<K, std::map<K, D>>::const_iterator outer_iterator;
      typedef typename std::map<K, D>::const_iterator inner_iterator;

      typedef std::forward_iterator_tag iterator_category;
      typedef typename inner_iterator::value_type value_type;
      typedef typename inner_iterator::difference_type difference_type;
      typedef typename inner_iterator::pointer pointer;
      typedef typename inner_iterator::reference reference;

      const_iterator() { }

      const_iterator(outer_iterator it) : outer_it_(it), outer_end_(it) { }

      const_iterator(outer_iterator it, outer_iterator end) : outer_it_(it), outer_end_(end) {

        if (outer_it_ == outer_end_) return;
        inner_it_ = (*outer_it_).second.cbegin();
        advance_past_empty_inner_containers();
      }

      reference operator*()  const { return *inner_it_;  }
      pointer operator->() const { return &*inner_it_; }

      const_iterator& operator++ () {
        ++inner_it_;
        if (inner_it_ == (*outer_it_).second.cend()) advance_past_empty_inner_containers();

        return *this;
      }

      const_iterator operator++ (int) {
        iterator it(*this);
        ++*this;

        return it;
      }

      friend bool operator==(const const_iterator &a, const const_iterator &b) {
        if (a.outer_it_ != b.outer_it_) return false;

        if (a.outer_it_ != a.outer_end_ && b.outer_it_ != b.outer_end_ && a.inner_it_ != b.inner_it_)
          return false;

        return true;
      }

      friend bool operator!=(const const_iterator &a,const const_iterator &b) { return !(a == b); }

  private:

      void advance_past_empty_inner_containers() {
        while (outer_it_ != outer_end_ && inner_it_ == (*outer_it_).second.cend()) {
          ++outer_it_;
          if (outer_it_ != outer_end_) inner_it_ = (*outer_it_).second.cbegin();
        }
      }

      outer_iterator outer_it_;
      outer_iterator outer_end_;
      inner_iterator inner_it_;
    };  // ~ iterator

  class iterator {
  public:

      typedef typename std::map<K, std::map<K, D>>::iterator outer_iterator;
      typedef typename std::map<K, D>::iterator inner_iterator;

      typedef std::forward_iterator_tag iterator_category;
      typedef typename inner_iterator::value_type value_type;
      typedef typename inner_iterator::difference_type difference_type;
      typedef typename inner_iterator::pointer pointer;
      typedef typename inner_iterator::reference reference;

      iterator() { }

      iterator(outer_iterator it) : outer_it_(it), outer_end_(it) { }

      iterator(outer_iterator it, outer_iterator end) : outer_it_(it), outer_end_(end) {

        if (outer_it_ == outer_end_) return;
        inner_it_ = (*outer_it_).second.begin();
        advance_past_empty_inner_containers();
      }

      reference operator*()  const { return *inner_it_;  }
      pointer operator->() const { return &*inner_it_; }

      iterator& operator++ () {
        ++inner_it_;
        if (inner_it_ == (*outer_it_).second.end()) advance_past_empty_inner_containers();

        return *this;
      }

      iterator operator++ (int) {
        iterator it(*this);
        ++*this;

        return it;
      }

      friend bool operator==(const iterator &a, const iterator &b) {
        if (a.outer_it_ != b.outer_it_) return false;

        if (a.outer_it_ != a.outer_end_ && b.outer_it_ != b.outer_end_ && a.inner_it_ != b.inner_it_)
          return false;

        return true;
      }

      friend bool operator!=(const iterator &a,const iterator &b) { return !(a == b); }

  private:

      void advance_past_empty_inner_containers() {
        while (outer_it_ != outer_end_ && inner_it_ == (*outer_it_).second.end()) {
          ++outer_it_;
          if (outer_it_ != outer_end_) inner_it_ = (*outer_it_).second.begin();
        }
      }

      outer_iterator outer_it_;
      outer_iterator outer_end_;
      inner_iterator inner_it_;
    };  // ~ iterator

  /// begin() const-iterator for objects stored in this matrix
  inline SparseMap2D::const_iterator  cbegin() const { return const_iterator(data.cbegin(),data.cend()); };

  /// end() const-iterator for objects stored in this matrix
  inline SparseMap2D::const_iterator  cend() const { return const_iterator(data.cend()); };

  /// begin() iterator for objects stored in this matrix
  inline SparseMap2D::iterator  begin() { return iterator(data.begin(),data.end()); };

  /// end() iterator for objects stored in this matrix
  inline SparseMap2D::iterator  end() { return iterator(data.end()); };

  /// begin() const-iterator for objects stored in a given row of this matrix
  inline typename std::map<K,D>::const_iterator cbegin(const K row) const { return data.at(row).cbegin(); };

  /// end() const-iterator for objects stored in a given row of this matrix
  inline typename std::map<K,D>::const_iterator cend(const K row) const { return data.at(row).cend(); };

  /// begin() iterator for objects stored in a given row of this matrix
  inline typename std::map<K,D>::iterator begin(const K row) { return data.at(row).begin(); };

  /// end() iterator for objects stored in a given row of this matrix
  inline typename std::map<K,D>::iterator end(const K row) { return data.at(row).end(); };

  /// Returns maximum row index of this matrix
  size_t max_row_index() const { return std::max_element(data.cbegin(),data.cend(), data.value_comp())->first; }

  /// Counts elements of this matrix
  size_t size() const {

    size_t cnt = 0;
    std::for_each(cbegin(),cend(),[&cnt](const std::pair<K, D> & t){++cnt;});

    return cnt;
  }

  /// Counts elements of this matrix
  size_t row_size(K row) const { return data.at(row).size(); }

  /// Returns true if this matrix has a row identified by a particular key
  bool has_row(const K row) const { return (data.find(row) != data.end()); }

  /// Returns true if this matrix has an element stored at particular (row, column) combination
  bool has_element(const K row,const K column) const {

    auto r = data.find(row);
    if (r != data.end()) {
      const auto &m = (*r).second;
      return (m.find(column) != m.end());
    }
    return false;
  }

  /** @brief Collects indexes of items inserted into this map.
   *
   * This method goes through this sparse map and collects these 2D indexes (i,j) (represented as std::pair<K,K>)
   * where <code>(*this)[i][j]</code> is a valied element of this map. Note, that the collected indexes may be obsolete
   * after inserting to this map.
   *
   * The collected vector of indexes may be useful to iterate over elements when the map does not change too often.
   *
   * @param output - vector used to store the data indexes
   */
  void nonempty_indexes(std::vector<std::pair<K,K>> & output) {

    output.clear();
    for(const auto & m : data) {
      for(const auto & r:m.second) {
        output.push_back(std::make_pair(m.first,r.first));
      }
    }
  }

  /// Insert a new value into this matrix
  void insert(const K row, const K column, D t) { data[row][column] = t; }

  /// Removes an element from this matrix, if it exists. If not, nothing happens
  void erase(const K &row, const K &column) {

    if (data.find(row) != data.end()) {
      auto it = data[row].find(column);
      if (it != data[row].end()) data[row].erase(it);
    }
  }

  /// Returns an element of this map; if not found, std::out_of_range exception will be thrown
  D & at(const K row, const K column) { return data.at(row).at(column); }

  /// Returns a const - element of this map; if not found, the given <code>empty</code> element will be returned
  D & at(const K row, const K column, D & empty) {

    if (data.find(row) != data.end()) {
      auto it = data[row].find(column);
      if (it != data[row].end()) return data[row][column];
      else return empty;
    }

    return empty;
  }

  /// Removes all the data from this array
  void clear() { data.clear(); }

  /// Returns a const - element of this map; if not found, std::out_of_range exception will be thrown
  const D & at(const K row, const K column) const { return data.at(row).at(column); }

  /// Returns an element of this map; if not found, the given <code>empty</code> element will be returned
  const D & at(const K row, const K column, const D & empty) const {

    if (data.find(row) != data.end()) {
      auto it = data.at(row).find(column);
      if (it != data.at(row).end()) return it->second;
      else return empty;
    }

    return empty;
  }

  /** @brief  Returns a maximum element of a given row
   *
   * @param row_index - which row?
   * @param empty - if the row is empty, the <code>empty</code> element will be returned
   * @return maximum element of a given row
   */
  const D & max_element_in_row(const K row_index, const D & empty) const {

    if (data.find(row_index) != data.cend()) {
      K max_el_id = data.at(row_index).begin()->first;
      for(const auto & m : data.at(row_index)) {
        if(m.second > data.at(row_index).at(max_el_id))
          max_el_id = m.second;
      }
      return data.at(row_index).at(max_el_id);
    }

    return empty;
  }

  /** @brief  Returns a minimum element of a given row
   *
   * @param row_index - which row?
   * @param empty - if the row is empty, the <code>empty</code> element will be returned
   * @return minimum element of a given row
   */
  const D & min_element_in_row(const K row_index, const D & empty) const {

    if (data.find(row_index) != data.cend()) {
      K max_el_id = data.at(row_index).begin()->first;
      for(const auto & m : data.at(row_index)) {
        if(m.second < data.at(row_index).at(max_el_id))
          max_el_id = m.second;
      }
      return data.at(row_index).at(max_el_id);
    }

    return empty;
  }

  /** @brief  Returns a sum of elements of a given row
   *
   * @param row_index - which row?
   * @param empty - if the row is empty, the <code>empty</code> element will be returned
   * @return sum of elements of a given row
   */
  D sum_in_row(const K row_index,  D  empty) const {

    if (data.find(row_index) != data.cend())
      for(const auto & m : data.at(row_index)) {
        empty += m.second;
      }

    return empty;
  }

private:
  std::map<K, std::map<K, D>> data;
};

}
}
}

#endif

