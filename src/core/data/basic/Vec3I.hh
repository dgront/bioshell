/** @file Vec3.hh Provides Vec3 data type - a vector in 3D
 */
#ifndef CORE_DATA_BASIC_VEC3I_H
#define CORE_DATA_BASIC_VEC3I_H

#include <cmath>
#include <limits>
#include <functional>

#include <core/data/structural/PdbAtom.hh>

#undef __SSE__ // PyBioShell on mac doesn't compile when SSE is set

#ifdef __SSE__
#include <xmmintrin.h>
#endif

#include <iostream>
#include <vector>
#include <memory>

#include <core/data/basic/Vec3I.fwd.hh>
#include <core/calc/numeric/basic_math.hh>
#include <core/index.hh>
#include <utils/Logger.hh>

namespace core {
namespace data {
namespace basic {


typedef int COORDINATE_VALUE;

/** \brief Represents a vector in the 3D space.
 */
class Vec3I {
public:
  friend struct std::hash<core::data::basic::Vec3I>;

  /** @name X, Y, Z coordinates of the vector
   */
  ///@{
  COORDINATE_VALUE x_, y_, z_;			// four-bytes integer for each coordinate
  ///@}

public:
  core::index2 chain_id = 0;      	///< Index of a chain this atom belongs to
  core::index1 residue_type = 0;  	///< A place to keep information about residue type
  core::index1 atom_type = 0;     	///< A place to keep information about atom type

  /// Constructor creates a new vector placed at (0,0,0)
  Vec3I() : x_(0), y_(0), z_(0) { }

  /// Constructor creates a new vector placed at (nx,ny,nz)
  Vec3I(double nx, double ny, double nz) { x(nx); y(ny); z(nz); }

  /// Constructor creates a new vector placed at (v[0],v[1],v[2])
  explicit Vec3I(const std::vector<double> & v) {x(v[0]); y(v[1]); z(v[2]); }

  /// Constructor creates a new vector placed at (v,v,v)
  explicit Vec3I(double v) { x(v); y(v); z(v); }

  /// Constructor creates a new vector placed at a location given by a 3D Vec3 vector
  explicit Vec3I(const Vec3 & v) : Vec3I(v.x, v.y, v.z) {}

  double x() const { return x_ * int_to_real_; }

  void x(double nx) { x_ = wrap(nx); }

  double y() const { return y_ * int_to_real_; }

  void y(double ny) { y_ = wrap(ny); }

  double z() const { return z_ * int_to_real_; }

  void z(double nz) { z_ = wrap(nz); }

  COORDINATE_VALUE ix() const { return x_; }

  COORDINATE_VALUE iy() const { return y_; }

  COORDINATE_VALUE iz() const { return z_; }

  /** @brief Sets the new value to all the three coordinates of this vector.
   *
   * @param v - a new value for all the three coordinates of this vector
   */
  void set(const double v) { x(v); y(v); z(v); }

  /** @brief Sets the new value to all the three coordinates of this vector.
   *
   * @param ax - a new value for X coordinate
   * @param ay - a new value for Y coordinate
   * @param az - a new value for Z coordinate
   */
  void set(const double ax, const double ay, const double az) { x(ax); y(ay); z(az); }

  /// Copy coordinates of a given Vec3I object to this vector
  void set(const Vec3I& v) {
    x_ = v.x_;
    y_ = v.y_;
    z_ = v.z_;
  }

  /// Copy coordinates of a given Vec3 object to this vector
  inline void set(const Vec3& v) { set(v.x, v.y, v.z); }

  /// Copy coordinates of a given PdbAtom object to this vector
  void set(const core::data::structural::PdbAtom &a) { set(a.x, a.y, a.z); }

  /// Copy the first three coordinates of a given std::vector to this vector
  void set(const std::vector<double>& v) {
    x(v[0]);
    y(v[1]);
    z(v[2]);
  }

  /** @brief Indexing operator provides X, Y or Z coordinate based on the index.
   * @param axis - index of the coordinate: 0, 1 or 2 to get X, Y or Z, respectively
   */
  double operator[](const index1 axis) const {

    if(axis==0) return x();
    if(axis==1) return y();
    return z();
  }

  static double int_to_real_factor() { return int_to_real_; }

  /// Add a constant to the coordinates of this vector
  inline Vec3I& operator+=(const Vec3I& r);

  /// Copy operator
  inline Vec3I& operator=(const Vec3I& r);

  /// Set a new value same to all coordinates, e.g. 0.0
  inline Vec3I& operator=(const double v);

  /// Subtract a constant from the coordinates of this vector
  inline Vec3I& operator-=(const Vec3I& r);

  /// Multiply this vector by a constant
  inline Vec3I& operator*=(const double a);

  /// Divide this vector by a constant
  inline Vec3I& operator/=(const double a);

  /// Add a double constant to this vector
  inline Vec3I& operator+=(const double a);

  /// Subtract a double constant from this vector
  inline Vec3I& operator-=(const double a);

  /// Adds dx, dy and dz to this vector
  inline Vec3I& add(const double dx, const double dy, const double dz);

  inline double dot_product(const Vec3I& r) const { return x() * r.x() + y() * r.y() + z() * r.z(); }

  /// Return the length of this vector
  double length() const;

  /** @brief Scale this vector to a new length.
   * By default this vector will be normalised i.e. set to length 1.0
   * @param new_length - the new length of this vector
   */
  void norm(const double new_length = 1.0);

  /** @brief returns true if this vector is equal to the vector <code>v</code>
   *
   * @param v - vector for comparison
   * @return true if the two vectors are equal
   */
  inline bool operator==(const Vec3I& v) const;

  /** @brief returns true if this vector is equal to the vector <code>v</code>
   *
   * @param v - vector for comparison
   * @return true if the two vectors are equal
   */
  inline bool operator<(const Vec3I& v) const;

  /** @brief Returns the size of a simulation box.
   *
   */
  static inline double get_box_len() { return box_length_ * 2.0; }

  /** @brief Sets the new value of the size of a simulation box.
   *
   */
  static void set_box_len(const double new_box_len);

  /** @brief Calculates the square distance between a given point and this point if it's closer than <code>sqrt(cutoff2)</code>
   *
   * This method first calculates the squared distance of X coordinates. If this value is already higher
   * than the given cutoff value, this method returns the cutoff without further distance evaluation.
   * @param v - the square distance will be measured between <code>v</code> and this point
   * @param cutoff2 - cutoff for the squared distance value.
   * @return the square distance or <code>cutoff2</code>, whichever is smaller
   */
  inline double distance_square_to(const Vec3I& v, const double cutoff2) const {

    double cutoff2_i = cutoff2 / int_to_real_2_;
    double r = v.x_ - x_;
    double r2 = r * r;
    if (r2 >= cutoff2_i) return cutoff2;
    r = v.y_ - y_;
    r2 += r * r;
    if (r2 >= cutoff2_i) return cutoff2;
    r = v.z_ - z_;
    r2 += r * r;
    if (r2 >= cutoff2_i) return cutoff2;

    return r2 * int_to_real_2_;
  }

  /** @brief Calculates the square distance between a given point and this point
   * Periodic boundary conditions will be applied.
   * @param v - the square distance will be measured between <code>v</code> and this point
   * @return the square distance
   */
  inline double distance_square_to(const Vec3I& v) const {
    double r = v.x_ - x_;
    double r2 = r * r;
    r = v.y_ - y_;
    r2 += r * r;
    r = v.z_ - z_;
    r2 += r * r;

    return r2 * int_to_real_2_;
  }

  /** @brief Calculates the closest possible (according to PBC)  square distance between a given point and this point if it's closer than <code>sqrt(cutoff2)</code>
   *
   * This method first calculates the squared distance of X coordinates. If this value is already higher
   * than the given cutoff value, this method returns the cutoff without further distance evaluation.
   * @param v - the square distance will be measured between <code>v</code> and this point
   * @param cutoff2 - cutoff for the squared distance value.
   * @return the square distance or <code>cutoff2</code>, whichever is smaller
   */
   // @todo Remove all methods with "closest", the distance_to() already includes PBC
  inline double closest_distance_square_to(const Vec3I & v, const double cutoff) const { return distance_square_to(v, cutoff); }

  /** @brief Calculates the closest possible (according to PBC) square distance between a given point and this point.
   *
   * This method is identical to <code>distance_square_to()</code> as Vec3I class always applies periodic boundary conditions.
   * @param v - the square distance will be measured between <code>v</code> and this point
   * @return the square distance
   */
  inline double closest_distance_square_to(const Vec3I & v) const { return distance_square_to(v); }

  /** @brief Calculates the distance between a given point and this point.
   *
   * Periodic boundary conditions will be applied.
   * @param v - the distance will be measured between <code>v</code> and this point
   * @return the distance
   */
  inline double distance_to(const Vec3I& v) const { return sqrt(distance_square_to(v)); }

  /** Returns the closest value of <code>this.x - v.x</code>.
   * Note, that the result might be negative!
   */
  inline double closest_delta_x(const Vec3I &v) const { return (x_ - v.x_) * int_to_real_; }

  /** Returns the closest value of <code>this.y - v.y</code>.
   * Note, that the result might be negative!
   */
  inline double closest_delta_y(const Vec3I &v) const { return (y_ - v.y_) * int_to_real_; }

  /** Returns the closest value of <code>this.z - v.z</code>.
   * Note, that the result might be negative!
   */
  inline double closest_delta_z(const Vec3I &v) const { return (z_ - v.z_) * int_to_real_; }

  /** @brief Finds a Vec3 coordinates of an image of this vector which is the closest to the reference
   * @param reference - a reference atom
   * @param dst - the image found by this method
   */
  void get_closest(const Vec3I &reference, Vec3 &dst) const {
    dst.x = reference.x() + (x_ - reference.x_) * int_to_real_;
    dst.y = reference.y() + (y_ - reference.y_) * int_to_real_;
    dst.z = reference.z() + (z_ - reference.z_) * int_to_real_;
  }

private:
  static double int_to_real_;
  static double int_to_real_2_;
  static double box_length_;      // --- maximum coordinate value, e.g. X is in the range [-box_length_, box_length_]
  friend std::ostream &operator<<(std::ostream &out, const Vec3I &v);
  friend utils::Logger &operator<<(utils::Logger &logger, const Vec3I &coordinates);
  static COORDINATE_VALUE wrap(double n);
};


inline Vec3I& Vec3I::operator+=(const Vec3I& r) {

  x_ += r.x_;
  y_ += r.y_;
  z_ += r.z_;
  return *this;
}

inline Vec3I& Vec3I::operator+=(const double r) {

  double rr = COORDINATE_VALUE(r/int_to_real_);
  x_ += rr;
  y_ += rr;
  z_ += rr;
  return *this;
}

inline Vec3I& Vec3I::operator=(const Vec3I& r) {

  x_ = r.x_;
  y_ = r.y_;
  z_ = r.z_;
  return *this;
}

inline Vec3I& Vec3I::operator=(const double v) {

  x(v);
  y(v);
  z(v);
  return *this;
}

inline bool Vec3I::operator==(const Vec3I& v) const {

  return ((x_ == v.x_) && (y_ == v.y_) && (z_ == v.z_));
}

/** @brief Less-than comparison between Vec3 objects is implemented as comparing their X, Y and Z coordinates
 *  @param aj -  the second vector being compared with this one
 */
inline bool Vec3I::operator<(const Vec3I & aj) const {

  if (x_ != aj.x_) return x_ < aj.x_;
  if (y_ != aj.y_) return y_ < aj.y_;
  if (z_ != aj.z_) return z_ < aj.z_;
  return false;
}

inline Vec3I& Vec3I::operator-=(const Vec3I& r) {

  x_ -= r.x_;
  y_ -= r.y_;
  z_ -= r.z_;
  return *this;
}

inline Vec3I& Vec3I::operator*=(const double a) {

  x_ *= a;
  y_ *= a;
  z_ *= a;
  return *this;
}

inline Vec3I& Vec3I::operator/=(const double a) {

  x_ /= a;
  y_ /= a;
  z_ /= a;
  return *this;
}

inline Vec3I& Vec3I::operator-=(const double a) {

  double rr = COORDINATE_VALUE(a/int_to_real_);
  x_ -= rr;
  y_ -= rr;
  z_ -= rr;
  return *this;
}

inline Vec3I& Vec3I::add(const double dx, const double dy, const double dz) {

  x_ += (COORDINATE_VALUE(dx / int_to_real_));
  y_ += (COORDINATE_VALUE(dy / int_to_real_));
  z_ += (COORDINATE_VALUE(dz / int_to_real_));
  return *this;
}

inline Vec3I operator*(double r, Vec3I v) {
  v *= r;
  return v;
}

inline Vec3I operator*(Vec3I v, double r) {
  v *= r;
  return v;
}

inline Vec3I operator+(Vec3I v, const Vec3I& r) {
  v += r;
  return v;
}

inline Vec3I operator-(Vec3I v, const Vec3I& r) {
  v -= r;
  return v;
}

inline double operator*(const Vec3I& l, const Vec3I& r) {
  double dot_product = l.x() * r.x() + l.y() * r.y() + l.z() * r.z();
  return dot_product;    
}

    inline double dot_product(const Vec3I& l, const Vec3I& r) {
        double dot_product = l.x() * r.x() + l.y() * r.y() + l.z() * r.z();
        return dot_product;
    }


static inline void cross_product(const Vec3I& l, const Vec3I& r, Vec3I &result) {

  double x = l.y() * r.z() - l.z() * r.y();
  double y = l.z() * r.x() - l.x() * r.z();
  double z = l.x() * r.y() - l.y() * r.x();
  result.set(x, y, z);
}

/** @brief Calculates determinant of a matrix defined by its row vectors.
 *
 * @param v1 - the first row of the input matrix
 * @param v2 - the second row of the input matrix
 * @param v3 - the third row of the input matrix
 * @return determinant of a 3x3 matrix
 */
static inline double det(const Vec3I& v1, const Vec3I& v2, const Vec3I &v3) {

  return v1.x() * ((v2.y() * v3.z()) - (v3.y() * v2.z())) - v1.y() * (v2.x() * v3.z() - v3.x() * v2.z()) +
         v1.z() * (v2.x() * v3.y() - v3.x() * v2.y());
}

/** @brief Calculates signed distance between the first and fourth point.
 *
 * The signed distance is defined as:
 * \f[
 * r_{14}^{*} =  \textrm{sign}(
 * \det \left( \begin{array}{c}
 * p_1 - p_0 \\
 * p_2 - p_1 \\
 * p_3 - p_2 \end{array} \right)
 * ) \times \|p_3-p_0\|
 * \f]
 * @param p0 - the first atom (point)
 * @param p1 - the second atom (point)
 * @param p2 - the third atom
 * @param p3 - the fourth atom (point)
 * @return determinant of a 3x3 matrix
 */
static inline double r14x(const Vec3I& p0,const Vec3I& p1, const Vec3I& p2, const Vec3I &p3) {

  Vec3I v1(p1);
  v1-=p0;
  Vec3I v2(p2);
  v2-=p1;
  Vec3I v3(p3);
  v3-=p2;
  double mul = core::calc::numeric::sgn(det(v1, v2, v3));
  if (mul == 0) mul = 1.0;
  return mul * p3.distance_to(p0);
}

#undef __SSE__
#ifdef __SSE__
static inline __m128 cross_product_sse(__m128 & a, __m128 & b) {

  __m128 result = _mm_sub_ps(_mm_mul_ps(b, _mm_shuffle_ps(a, a, _MM_SHUFFLE(3, 0, 2, 1))),
      _mm_mul_ps(a, _mm_shuffle_ps(b, b, _MM_SHUFFLE(3, 0, 2, 1))));
  return _mm_shuffle_ps(result, result, _MM_SHUFFLE(3, 0, 2, 1));
}

static inline void cross_product_sse(const Vec3& l, const Vec3& r, Vec3 &result) {

  __m128 a = _mm_set_ps(0.0, l.x, l.y, l.z);
  __m128 b = _mm_set_ps(0.0, r.x, r.y, r.z);
  __m128 o = cross_product_sse(a, b);

  float res[4] = { 0, 0, 0, 0 };
  _mm_storer_ps(res, o);
  result.x = res[1];
  result.y = res[2];
  result.z = res[3];
}
#endif


} // ~ basic
} // ~ data
} // ~ core

namespace std {

/** @brief Calculates a hash of a Vec3 object from its coordinates
 */
template<>
struct hash<core::data::basic::Vec3I> {
  std::size_t operator()(const core::data::basic::Vec3I &k) const {
    return (int(k.x_ * 10) + (int(k.y_ * 10) << 10) + (int(k.x_ * 10) << 20));
  }
};
}

#endif
