#ifndef CORE_DATA_BASIC_VEC3CUBIC_H
#define CORE_DATA_BASIC_VEC3CUBIC_H

#include <cmath>
#include <cstdlib>

#include <core/data/basic/Vec3.hh>

namespace core {
namespace data {
namespace basic {

/** @brief A vector in 3D Cartesian space with cubic Periodic Boundary Conditions (PBC) built in
 *
 * This class behaves mostly as its base (which is Vec3). All the vector's coordinates may take any real value
 * (so they are not restrained to the simulation box) The only difference is in
 * <code>closest_distance_square_to</code> method, which takes into account  PBC while evaluating the distance.
 * The  extra features of this class are <code>wrap()</code> methods which calculate the in-the-box coordinate
 * methods used to.
 */
class Vec3Cubic: public Vec3 {
public:

  /// Return the with of the periodic box
  static inline float get_box_len() { return box_len; }

  /// Sets the new the with of the periodic box
  static inline void set_box_len(const float new_box_len) {
    box_len = new_box_len;
    box_len_half = box_len / 2.0; 
  }

  Vec3Cubic() : Vec3() {}

  Vec3Cubic(float x, float y, float z) : Vec3(x,y,z) {}

  explicit Vec3Cubic(float v)  : Vec3(v) {}

  /** @brief Wraps coordinates of this vector to the cubic box and stores the resulting coordinates in the given vector.
   *
   * Coordinates of this vector are not changed.
   *
   * @include apply_pbc.cc
   */
  inline void wrap(Vec3Cubic & out) const {

    out.x = x;
    out.y = y;
    out.z = z;
    if (x > box_len) out.x = x - box_len;
    else if (x < 0) out.x = x + box_len;

    if (y > box_len) out.y = y - box_len; 
    else if (y < 0) out.y = y + box_len;

    if (z > box_len) out.z = z - box_len;
    else if (z < 0) out.z = z + box_len;
  }

  /** Returns the closest value of <code>this.x - v.x</code>.
   * Note, that the result might be negative!
   */
  inline float closest_delta_x(const Vec3Cubic &v) const {

    const float dx = x - v.x;
    if (dx > 0)
      return (dx > box_len_half) ? -(box_len - dx) : dx;
    else
      return (dx < -box_len_half) ? box_len + dx : dx;
  }

  /** Returns the closest value of <code>this.x - v.x</code>.
   * Note, that the result might be negative!
   */
  inline float closest_delta_y(const Vec3Cubic &v) const {

    const float dy = y - v.y;
    if (dy > 0)
      return (dy > box_len_half) ? -(box_len - dy) : dy;
    else
      return (dy < -box_len_half) ? box_len + dy : dy;
  }

  /** Returns the closest value of <code>this.x - v.x</code>.
   * Note, that the result might be negative!
   */
  inline float closest_delta_z(const Vec3Cubic &v) const {

    const float dz = z - v.z;
    if (dz > 0)
      return (dz > box_len_half) ? -(box_len - dz) : dz;
    else
      return (dz < -box_len_half) ? box_len + dz : dz;
  }

  /** @brief Calculates the closest possible (according to PBC) square distance between a given point and this point.
   *
   * @param v - the square distance will be measured between these images of <code>v</code> and this point
   * that are located within the original simulation box
   * @return the square distance
   */
  inline float closest_distance_square_to(const Vec3Cubic & v) const {
    float r = closest_delta_x(v);
    float r2 = r * r;
    r = closest_delta_y(v);
    r2 += r * r;
    r = closest_delta_z(v);
    r2 += r * r;

    return r2;
  }

  /** @brief Calculates the closest possible (according to PBC) square distance between a given point and this point.
   *
   * This method first calculates the squared distance of X coordinates. If this value is already higher
   * than the given cutoff value, this method returns the cutoff without further distance evaluation.
   * @param v - the square distance will be measured between these images of <code>v</code> and this point
   * that are located within the original simulation box
   * @param cutoff2 - cutoff for the squared distance value.
   * @return the square distance or <code>cutoff2</code>, whichever is smaller
   */
  inline float closest_distance_square_to(const Vec3Cubic & v, const float cutoff2) const {

    float r = closest_delta_x(v);
    float r2 = r * r;
    if (r2 >= cutoff2) return cutoff2;
    r = closest_delta_y(v);
    r2 += r * r;
    if (r2 >= cutoff2) return cutoff2;
    r = closest_delta_z(v);
    r2 += r * r;

    return r2;
  }

  /**
   * @brief Creates a <code> std::vector<Vec3Cubic> </code> object of a requested size.
   *
   * The method has been added mainly to expose <code> std::vector<Vec3Cubic> </code> type in PyBioShell
   * @param n - size of the created std::vector of Vec3Cubic
   * @return a vector of Vec3Cubic objects
   */
  static std::vector<Vec3Cubic> create_coordinates(const core::index4 n) { return std::vector<Vec3Cubic>(n); }

private:
  static float box_len;
  static float box_len_half;
};

} // ~ basic
} // ~ data
} // ~ core

#endif
/**
 */
