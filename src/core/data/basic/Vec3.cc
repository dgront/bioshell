#include <iostream>

#include <core/data/basic/Vec3.hh>
#include <core/data/basic/Vec3I.hh>
#include <utils/string_utils.hh>
#include <utils/Logger.hh>

namespace core {
namespace data {
namespace basic {

/// Constructor creates a new vector based on Vec3I vector
Vec3::Vec3(const Vec3I & v) {
	x = v.x();
	y = v.y();
	z = v.z();
}


/// Copy coordinates of a given Vec3 object to this vector
void Vec3::set(const Vec3I& v) {
	x = v.x();
	y = v.y();
	z = v.z();
}

std::ostream& operator<<(std::ostream &out, const Vec3 &v) {

  out << utils::string_format("%8.3f %8.3f %8.3f", v.x, v.y, v.z);
	return out;
}

utils::Logger &operator <<(utils::Logger &logger, const Vec3 & coordinates) {

	logger << utils::string_format("%8.3f %8.3f %8.3f",coordinates.x,coordinates.y,coordinates.z);

	return logger;
}
} // ~ basic
} // ~ data
} // ~ core
