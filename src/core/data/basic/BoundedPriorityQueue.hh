#ifndef CORE_DATA_BASIC_BoundedPriorityQueue_H
#define CORE_DATA_BASIC_BoundedPriorityQueue_H

#include <algorithm>
#include <vector>

#include <core/index.hh>

#include <utils/Logger.hh>

namespace core {
namespace data {
namespace basic {

/** @brief A priority queue that has a fixed bounded size.
 *
 * BoundedPriorityQueue is a container of a fixed, pre-defined size which keeps its elements ordered (greatest first).
 * When a new element is inserted and the capacity is exceeded, the worst (lowest priority) element is removed from
 * the queue.
 *
 * @tparam T - type of data stored in this container
 * @tparam StrictWeakOrdering - operation used to compare objects in a queue; defines sorting order
 * @tparam AreEqual - operation to decide whether two object are equal or not
 */
template<class T, class StrictWeakOrdering, class AreEqual>
class BoundedPriorityQueue {
public:

  /** @brief Creates a new queue container.
   *
   * @param cmp - object used to sort elements i.e. it defines the priority. Highest priority elements goes to the front of the queue
   * @param eq - object used to define two elements equal - necessary to keep only unique elements in  the queue.
   * @param sorted_capacity - how many objest to store
   * @param max_capacity - actually the container keeps more elements than the capacity limits; when the
   *    <code>max_capacity</code> is reached, the queue is sorted and exceeding elements are removed so
   *    best <code>sorted_capacity</code> elements are left
   * @param worst_element - if an inserted element is worse than this one, it will be rejected upfront.
   */
  BoundedPriorityQueue(StrictWeakOrdering cmp, AreEqual eq,core::index4 sorted_capacity, core::index4 max_capacity,T worst_element) :
      comp(cmp), eql(eq), l("BoundedPriorityQueue") {
    max_capacity_ = max_capacity;
    sorted_capacity_ = sorted_capacity;
    data_.resize( max_capacity );
    sorted = false;
    last_ = 0;
    n_sorts = 0;
    n_denied = 0;
    n_inserts = 0;
    worst_ = worst_element;
  }

  /// Destructor just logs sone statistics about queuing efficiency
  ~BoundedPriorityQueue() { l<<utils::LogLevel::FINE << "inserts, sorts, denied: " << int(n_inserts)<<" "<<int(n_sorts) << " " << int(n_denied) << "\n"; }

  /// @brief Returns a reference to the best element in the queue.
  /// @detailed This element is greater than any other element in the queue.
  inline const T& front() {
    refresh();
    return data_[0];
  }

  /** @brief Returns the last element in the queue.
   *
   * @return lowest-priority element still in the queue
   */
  inline const T& tail() {
    refresh();
    return data_[last_];
  }

  /** @brief Returns element at a given index in the queue.
   * @return an element from the queue
   */
  inline const T& at(index4 i) {
    refresh();
    return data_.at(i);
  }

  /** @brief Returns element at a given index in the queue.
   * @return an element from the queue
   */
  inline const T& operator[](index4 i) {
    refresh();
    return data_[i];
  }

  inline const std::vector<T>& expose_data() {
    refresh();
    return data_;
  }

  /** @brief Returns const-begin iterator to this container.
   *
   * @return a const-access iterator pointing to the first element of this queue
   */
  inline typename std::vector<T>::const_iterator cbegin() const { return data_.begin(); }

  /** @brief Returns const-end iterator to this container.
   *
   * @return a const-access iterator pointing behind the last element of this queue
   */
  inline typename std::vector<T>::const_iterator cend() { refresh(); return data_.begin() + last_; }

  /** @brief Inserts a new element to this queue.
   *
   * The method will return false if an element is rejected, e.g. when its priority is too low
   * @param x - an element to be inserted
   * @return  true if the element was actually inserted, false otherwise
   */
  inline bool push(const T& x) {

    if  (comp(worst_, x)) {
      n_denied++;
      return false;
    }
    if(last_ == max_capacity_-2) {
      data_[max_capacity_-1] = x;
      refresh();
      worst_ = data_[sorted_capacity_];
      n_sorts++;
      n_inserts++;
      return true;
    }
    n_inserts++;
    data_[last_] = x;
    last_++;
    sorted=false;

    return true;
  }

  /// @brief Returns true if there is any chance the new element will be accepted by this queue
  inline bool push_might_work(const T& x) { return  (!comp(worst_, x)); }

  /// @brief sets new capacity for the container
  void resize(const index4 sorted_capacity,const  index4 max_capacity) {
    max_capacity_ = max_capacity;
    sorted_capacity_ = sorted_capacity;
    data_.resize(max_capacity);
  }

  /// Returns the size of this container, which is never greater than <code>sorted_capacity</code>
  inline core::index4 size() { return std::min(last_, (core::index4) data_.size()); }

  /// Removes all the elements from this container.
  inline void clear() { data_.clear(); }

private:
  bool sorted;
  core::index4 max_capacity_;
  core::index4 sorted_capacity_;
  core::index4 last_;
  T worst_;
  std::vector<T> data_;
  StrictWeakOrdering comp;
  AreEqual eql;

  // debug info
  utils::Logger l;
  index4 n_sorts;
  index4 n_denied;
  index4 n_inserts;

  inline void refresh() {
    if (!sorted) {
      std::sort(data_.begin(), data_.begin() + last_, comp);
      auto end = std::unique(data_.begin(),data_.end(),eql);
      last_ = std::min(sorted_capacity_ + 1,index4(std::distance(data_.begin(),end)));
      sorted = true;
    }
  }
};

} // ~basic
} // ~data
} // ~core

#endif
