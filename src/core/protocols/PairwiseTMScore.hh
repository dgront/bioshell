#ifndef CORE_PROTOCOLS_PairwiseTMScore_H
#define CORE_PROTOCOLS_PairwiseTMScore_H

#include <memory>

#include <core/data/basic/Vec3.hh>
#include <core/data/io/load_decoys.hh>
#include <core/data/basic/Array2D.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/transformations/TMScore.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/protocols/PairwiseStructureComparison.hh>

#include <utils/Logger.hh>
#include <utils/string_utils.hh>

using core::data::basic::Vec3;

namespace core {
namespace protocols {

using namespace core::data::structural;

/** @brief Few handy methods to calculate tmscore between structures (i.e. sets of points).
 *
 * This class uses <code>TMScore</code> class to calculate all tmscore values between the given set(s) of structures.
 */
class PairwiseTMScore : public PairwiseStructureComparison {
public:

  /** @brief Constructor defines the set of structures for tmscore calculations.
   *
   * Prepares this  calculator to compute tmscore values for all-vs-all structure pairs. All the decoys must have the same number of atoms.
   * All the atoms are used in calculations.
   * @param decoys - a vector of structures (sets of atoms) to be used in calculations
   * @param decoy_ids - strings to identify each of the structures (used for output). If this vector is empty (which is the default case),
   * order numbers will be used for this purpose
   * @param reference_length - the reference number of residues (used to normalize tm-score);
   * the default value = 0 uses the number of template atoms for this purpose
   */
  PairwiseTMScore(std::vector<Structure_SP> &decoys, const std::vector<std::string> &decoy_ids =
  std::vector<std::string>(), const core::index2 reference_length = 0) : PairwiseStructureComparison(decoys, nullptr,
    decoy_ids), logger("PairwiseTMScore"), ref_length(reference_length) {}

  /// Default virtual destructor
  virtual ~PairwiseTMScore() = default;

  /** @brief Calculates tmscore for every pair of structures and writes the results to a stream.
   *
   * The output stream for the tmscore results may be defined by calling <code>set_outstream()</code> method
   */
  virtual void calculate();

  /** @brief Calculates tmscore between the given reference and every structure stored in this object.
   *
   * This method also performs structural superimpositions and writes the transformed structures into <code>pdb_output</code> stream
   * (if it is not null). The output stream for the tmscore results may be defined by calling <code>set_outstream()</code> method
   * @param reference - a pointer to the reference coordinates
   * @param pdb_output - not used, but present to make match the signature of a virtual function; may be nullptr
   */
  virtual void calculate(const Structure_SP reference,std::shared_ptr<std::ostream> pdb_output = nullptr);

private:
  utils::Logger logger;
  core::data::basic::Coordinates the_query;
  core::data::basic::Coordinates the_tmplt;
  core::index2 ref_length;
  static const std::shared_ptr<core::data::structural::selectors::IsCA> ca_only;
};

}
}

#endif
