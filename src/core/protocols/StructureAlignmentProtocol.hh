#ifndef CORE_PROTOCOLS_StructureAlignmentProtocol_H
#define CORE_PROTOCOLS_StructureAlignmentProtocol_H

#include <string>
#include <utility> // for std::tuple

#include <core/index.hh>

#include <core/alignment/CoordinatesSet.hh>
#include <core/alignment/PairwiseStructureAlignment.hh>
#include <core/alignment/TMAlign.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

#include <core/protocols/StructureAlignmentProtocol.fwd.hh>

#include <utils/io_utils.hh>

namespace core {
namespace protocols {

/** \brief Protocol for computing pairwise alignment of protein structures
 */
class StructureAlignmentProtocol {
public:

  StructureAlignmentProtocol(core::index2 n_threads = 1) : log("StructureAlignmentProtocol"), n_threads(n_threads) {}

  core::index2 add_input_structure(const core::data::structural::Structure_SP structure, const std::string &name) {
    coordinates.emplace_back(structure, name);
    return coordinates.size() - 1;
  }

  /** @brief Calculates an alignment between two structures.
   *
   * The structures must had been previously inserted by calling <code>add_input_structure()</code> method
   *
   * @param i - index of the first structure
   * @param j  - index of the second structure
   * @return tuple of three : i, j and pairwise structure alignment object. Such a structure of returned data
   * is necessary to run alignments in separate threads
   */
  alignment::PairwiseStructureAlignment_SP align(core::index2 i, core::index2 j);

  /** @brief Calculates a structure alignment between any two structures inserted to this object.
   *
   */
  void align();

  /** @brief Calculates a structure alignment between the given i-th structure and any other structure inserted to this object.
   */
  void align(index2 i);

  alignment::PairwiseStructureAlignment_SP operator()(const core::index2 i, const core::index2 j);

  void write_aligned_structures(const core::index2 i, const core::index2 j, const std::string &query_fname,
                                const std::string &tmplt_fname);

  void write_aligned_structures();

  void print_alignment_report(core::index2 i, core::index2 j, std::ostream & out_stream);

  void print_alignment_report(std::ostream & out_stream);

  void print_alignment_report_tabular(core::index2 i, core::index2 j, std::ostream & out_stream);

  void print_alignment_report_tabular(std::ostream & out_stream);

  void print_alignment_fasta(core::index2 i, core::index2 j, std::ostream & out_stream);

  void print_alignment_fasta(std::ostream & out_stream);

private:
  std::vector<alignment::CoordinatesSet> coordinates;
  std::map<core::index4, core::alignment::PairwiseStructureAlignment_SP> alignments_;
  utils::Logger log;
  core::index2 n_threads;

  core::index4 key(core::index2 i, core::index2 j) { return (i < j) ? (i << 16) + j : (j << 16) + i; }
};

}
}

#endif

