#ifndef CORE_PROTOCOLS_AbstractSequenceProtocol_H
#define CORE_PROTOCOLS_AbstractSequenceProtocol_H

#include <string>
#include <utility>
#include <unordered_map>

#include <core/index.hh>
#include <core/algorithms/IterateIJ.hh>
#include <core/alignment/aligner_factory.hh>

namespace core {
namespace protocols {

/** \brief Abstract class for all protocols involving pairwise sequence alignment calculations
 *
 * @see SequenceWeightingProtocol - derived protocol evaluates sequence weights
 * @see PairwiseSequenceIdentityProtocol - derived protocol evaluates pairwise sequence identity
 */
class AbstractSequenceProtocol {
public:

  /// Virtual destructor that is required for a virtual class
  virtual ~AbstractSequenceProtocol() {}

  /** @name Provide input sequences for the protocol.
   * Functions that provides input sequences to be used in evaluations, alignments, etc.
   */
  ///@{

  /** @brief Adds a new input sequence.
   *
   * The sequence will be used in sequence alignments,  p-value evaluations and other operations performed by protocols
   * derived from this base class
   *
   * @param seq - one of the sequences to be used by this protocol
   * @return index of the sequence i.e. its position in the vector of internal sequences
   */
  core::index4 add_input_sequence(const core::data::sequence::Sequence_SP seq);

  /** @brief Adds a new input sequence.
   *
   * The sequence will be used in sequence alignments,  p-value evaluations and other operations performed by protocols
   * derived from this base class
   *
   * @param sequence - one of the sequences to be used by this protocol
   * @return index of the sequence i.e. its position in the vector of internal sequences
   */
  core::index4 add_input_sequence(const std::string & sequence);

  /** @brief Adds a new input sequence.
   *
   * The sequence will be used in sequence alignments,  p-value evaluations and other operations performed by protocols
   * derived from this base class
   *
   * @param header - sequence name (fasta header)
   * @param sequence - one of the sequences to be used by this protocol
   * @return index of the sequence i.e. its position in the vector of internal sequences
   */
  core::index4 add_input_sequence(const std::string &header, const std::string &sequence);

  /** @brief Adds new input sequences.
   *
   * The sequences will be used in sequence alignments,  p-value evaluations and other operations performed by protocols
   * derived from this base class
   *
   * @param sequences - a vector of sequences to be used by this protocol
   * @return index of the last sequence inserted i.e. its position in the vector of internal sequences
   */
  core::index4 add_input_sequences(const std::vector<core::data::sequence::Sequence_SP> & sequences);

  /** @brief Adds new input sequence.
   *
   * The sequence will be used in sequence alignments,  p-value evaluations and other operations performed by protocols
   * derived from this base class
   *
   * @param sequence - a vector of sequences to be used by this protocol
   * @return index of the sequence i.e. its position in the vector of internal sequences
   */
  core::index4 add_input_sequences(const std::vector<std::string> & sequences);

  ///@}

  /** @brief Returns the number of sequences inserted into this object
   *
   * @return  the number of sequences inserted into this object
   */
  core::index4 count_sequences() const { return sequences_.size(); }

  /** @name Protocol parameters.
   *
   * Functions that sets parameters of the protocol; the parameters remains the same for every alignment
   */
  ///@{

  /** @brief Select the query sequence used in calculations
   *
   * @param which_query - index of a sequence that will be aligned with any other sequence available for this protocol
   *  If not provided, the protocol makes all pairwise
   *  evaluations i.e. \f$ N \times N / 2\f$ alignments. When a query sequence is selected this protocol calculates
   *   \f$ N -1 \f$ alignments for each query
   * @return reference to <code>this</code> object to facilitate chaining
   */
  virtual AbstractSequenceProtocol & select_query(const core::index4 which_query) {

    which_query_.clear();
    which_query_.push_back(which_query);
    return *this;
  }

  /** @brief Adds another query sequence used in calculations
   *
   * @param which_query - index of a sequence that will be aligned with any other sequence available for this protocol
   *  If not provided, the protocol makes all pairwise
   *  evaluations i.e. \f$ N \times N / 2\f$ alignments. When a query sequence is selected this protocol calculates
   *   \f$ N -1 \f$ alignments for each query
   * @return reference to <code>this</code> object to facilitate chaining
   */
  virtual AbstractSequenceProtocol & add_query(const core::index4 which_query);

  /** @brief Adds a pair of sequences for which the protocol will be executed
   *
   * @param i - index of a sequence that will be aligned with the sequence j
   * @param j - index of a sequence that will be aligned with the sequence i
   * @return reference to <code>this</code> object to facilitate chaining
   */
  virtual AbstractSequenceProtocol & add_pair(const core::index4 i,const core::index4 j);

  /** @brief Adds a pair of sequences for which the protocol will be executed
   *
   * @param i_name - name of a sequence that will be aligned with the sequence j_name
   * @param j_name - index of a sequence that will be aligned with the sequence i_name
   * @return reference to <code>this</code> object to facilitate chaining
   */
  virtual AbstractSequenceProtocol & add_pair(const std::string & i_name, const std::string & j_name);

  /** @brief Defines the algorithm used for alignment calculations.
   *
   * @param atype - enumeration defining the alignment method
   * @return reference to <code>this</code> object to facilitate chaining
   */
  AbstractSequenceProtocol & alignment_method(core::alignment::AlignmentType atype) { atype_ = atype; return *this; }

  /** @brief Defines the substitution matrix used to align sequences
   *
   * @param matrix_name - name of the matrix, e.g. "BLOSUM62"
   * @return reference to <code>this</code> object to facilitate chaining
   */
  AbstractSequenceProtocol & substitution_matrix(const std::string &matrix_name) { substitution_matrix_ = matrix_name; return *this; }

  /** @brief Defines gap opening penalty
   *
   * @param open_penalty - gap opening penalty value; -10 is the default
   * @return reference to <code>this</code> object to facilitate chaining
   */
  AbstractSequenceProtocol & gap_open(const short int open_penalty) { gap_open_ = open_penalty; return *this; }

  /** @brief Defines gap extending penalty
   *
   * @param gap_extend - gap extending penalty value; -10 is the default
   * @return reference to <code>this</code> object to facilitate chaining
   */
  AbstractSequenceProtocol & gap_extend(const short int extend_penlty) { gap_extend_ = extend_penlty; return *this; }

  /** @brief Defines the number of CPU threads that run the protocol congruently
   *
   * @param cnt_threads - how many CPU threads may be run in parallel
   * @return reference to <code>this</code> object to facilitate chaining
   */
  AbstractSequenceProtocol & n_threads(const core::index4 cnt_threads) { n_threads_ = cnt_threads; return *this; }

  /** @brief Keep calculated sequence alignments
   *
   * @param if_store_alignments - say false to save memory
   * @return reference to <code>this</code> object to facilitate chaining
   */
  AbstractSequenceProtocol & keep_alignments(const bool if_store_alignments) { if_store_alignments_ = if_store_alignments; return *this; }

  /** @brief Jobs will be split into batches of that size.
   *
   * All batch jobs are sent to a thread pool. One all jobs in a pools have been calculated, the results are sent
   * to output file and next batch of jobs is sent to the pool.
   *
   * @param cnt_threads - how many CPU threads may be run in parallel
   * @return reference to <code>this</code> object to facilitate chaining
   */
  AbstractSequenceProtocol & batch_size(const core::index4 jobs_per_batch) { batch_size_ = jobs_per_batch; return *this; }


  /** @brief Make printed sequence names at most that long.
   *
   * Sequence names printed in a protocol's output will be trimmed to that size. Also, the first
   * <code>n_chars</code> characters is used to internally map the sequences (for quick by-name access).
   * <strong>Note</strong> that therefore the first <code>n_chars</code> characters of a sequence header must be unique!
   * @param n_chars - number of characters used to print a sequence name
   * @return reference to <code>this</code> object to facilitate chaining
   */
  AbstractSequenceProtocol & printed_seqname_length(index1 n_chars) { n_chars_ = n_chars; return *this; }

  ///@}

  /// This is the method to run the protocol
  virtual void run() = 0;

protected:
  core::index2 n_threads_;    ///< The number of threads to be used by this protocol in parallel evaluation
  core::index2 longest_len_;  ///< The length of the longest sequence (automatically updated by add_input_sequence() methods)
  core::index4 batch_size_;   ///< Jobs will be split into batches of that size; results of each batch will be sent to a file
  core::index1 n_chars_;      ///< maximum number of characters spent to print a sequence name in this protocol's output
  /// Keeps a map that connects each sequence index to its key - the first <code>n_chars_</code> characters of the sequences's header
  std::unordered_map<std::string, core::index4> seq_by_key;

  /** @brief Holds index of query sequences.
   *
   * If this vector is empty, the protocol will compute all-vs-all comparisons. Otherwise, this protocol
   * will be run for each query versus all other sequences
   */
  std::vector<core::index4> which_query_;
  std::vector<std::pair<core::index4,core::index4>> which_pairs_;
  short int gap_open_; ///< gap opening penalty
  short int gap_extend_; ///< gap continuation penalty
  core::alignment::AlignmentType atype_; ///< alignment type (e.g. local, global, etc)
  bool if_store_alignments_; ///< flag to keep alignment objects in memory; set to false if the number of sequences is really large
  std::string substitution_matrix_; ///< substitution matrix to be used
  std::vector<core::data::sequence::Sequence_SP> sequences_; ///< Input sequences for the protocol

  /** @brief Creates an iterator that iterates over the pairs of sequences for which calculations should be executed
   * @param enforce_all_pairs - if true, seq_i will be compared with seq_j and seq_j will be compared with seq_i
   *    otherwise every pair will be compared exactly once (the default behavior)
   * @return  IterateIJ iterator object created according to settings imposed by a user
   */
  core::algorithms::IterateIJ prepare_iteration(bool enforce_all_pairs = false) const;

private:
  core::index4 map_sequence(core::data::sequence::Sequence_SP s);
};

}
}

#endif

