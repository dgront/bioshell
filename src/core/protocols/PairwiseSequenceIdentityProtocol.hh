#ifndef CORE_PROTOCOLS_PairwiseSequenceIdentityProtocol_H
#define CORE_PROTOCOLS_PairwiseSequenceIdentityProtocol_H

#include <string>
#include <ostream>

#include <core/index.hh>
#include <core/alignment/PairwiseAlignment.hh>
#include <core/alignment/aligner_factory.hh>
#include <core/data/basic/SparseMap2D.hh>
#include <core/protocols/AbstractSequenceProtocol.hh>

namespace core {
namespace protocols {

/** @brief Stores results describing a pairwise sequence alignment.
 */
struct PairwiseScores {

  index2 n_identical;                     ///< the number of identical residue pairs aligned
  index2 alignment_length;                ///< the length of this alignment
  index2 alignment_length_no_tail_gaps;   ///< the length of this alignment without from and end gap (semiglobal alignment length)
};

/** \brief Protocol for computing sequence identity for pairs of protein sequences.
 *
 * For every pair of protein sequences registered into this object,  the protocol calculates sequence identity
 * based on their pairwise sequence alignment.
 *
 * \include ap_PairwiseSequenceIdentityProtocol.cc
 */
class PairwiseSequenceIdentityProtocol : public AbstractSequenceProtocol {
public:

  /** @brief Sets up the protocol with default parameters.
   *
   * Note, that this protocol inherits from <code>AbstractSequenceProtocol</code> and most of its setting
   * are actually set by that base class.
   */
  PairwiseSequenceIdentityProtocol();

  /// Default virtual destructor
  virtual ~PairwiseSequenceIdentityProtocol() = default;

  /** @brief Defines the lowest seq_identity for which results are still recorded by this protocol
   *
   * @param seq_identity - sequence identity cutoff
   * @return reference to <code>this</code> object to facilitate chaining
   */
  PairwiseSequenceIdentityProtocol & seq_identity_cutoff(const double seq_identity) { seq_identity_cutoff_ = seq_identity; return *this; }

  /** @brief Tells this protocol to use FastaMatchProtocol to filter dissimilar pairs
   *
   * @param state - if true, FastaMatchProtocol will be used
   * @return reference to <code>this</code> object to facilitate chaining
   */
  PairwiseSequenceIdentityProtocol & if_use_fasta_filter(const bool state) { if_use_fasta_filter_ = state; return *this; }

  virtual void run();

  /** Retrieves alignment that has been already computed.
   *
   * The alignment is available only if <code>keep_alignments()</code> was set to <code>true</code> before running the protocol
   *
   * @param i - index of the first sequence
   * @param j - index of the second sequence
   * @return alignment or <code>nullptr</code>
   */
  alignment::PairwiseAlignment_SP get_alignment(const core::index4 i, const core::index4 j);

  /** @brief Returns the number of identical residues after aligning a given pair of sequences.
   *
   * @param i - index of the first sequence
   * @param j - index of the second sequence
   * @return number of identical residues
   */
  core::index2 count_identical(const core::index4 i, const core::index4 j);

  /** @brief Returns the sequence identity computed by aligning a given pair of sequences.
   *
   * The sequence identity ratio is defined as the number of identical residues divided either by the length of the shorter sequence
   * or by the length of semiglobal alignment, whichever is greater.
   *
   * @param i - index of the first sequence
   * @param j - index of the second sequence
   * @return sequence identity
   */
  double get_sequence_identity(const core::index4 i, const core::index4 j);

  /** @brief Returns read-only reference to the results of this protocol.
   *
   * The returned container holds all sequence identity values calculated by this protocol; they might be accessed
   * by <code>(i,j)</code> operator. If for a certain <code>i,j</code> pair the identity ratio was below cutoff, 0.0
   * is returned
   * @return
   */
  const core::data::basic::SparseMap2D<core::index4, PairwiseScores> &expose_sequence_identity() const { return scores_; }

  /// Prints the meaningful protein pairs and sequence identity between them
  void print_sequence_identity(std::ostream &out_stream);

  /// Prints the header string for the protocol's  results' table
  void print_header(std::ostream &out_stream);

  /** @brief Returns the number of pairwise sequence alignment computations done by the most recent <code>run()</code> call
   *
   * Every <code>run()</code> call zeroes the counter, result is not cumulative
   * @return number of alignments computed in the most recent <code>run()</code> call
   */
  core::index4 n_jobs_completed() const { return n_jobs_completed_; }

protected:
  bool if_use_fasta_filter_;
  double seq_identity_cutoff_;
  /** @brief Stores alignment results: the number of identical positions and the length of an alignment (as a std::pair).
   * Results are stored only if above a cutoff
   */
  core::data::basic::SparseMap2D<core::index4, PairwiseScores> scores_;
  core::data::basic::SparseMap2D<core::index4, core::alignment::PairwiseAlignment_SP> alignments_;

private:
  utils::Logger logs_;
  core::index4 n_jobs_completed_;
};

}
}

#endif

