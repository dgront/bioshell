#ifndef CORE_PROTOCOLS_SequenceWeightingProtocol_H
#define CORE_PROTOCOLS_SequenceWeightingProtocol_H

#include <core/index.hh>
#include <core/protocols/AbstractSequenceProtocol.hh>

namespace core {
namespace protocols {

/** \brief Base class for protocols that compute sequence weights
 *
 */
class AbstractSequenceWeightingProtocol : public AbstractSequenceProtocol {
public:

  /** @brief Returns the weight value for a given sequence
   *
   * @param i - index of a sequence
   * @return sequence weight for the requested sequence
   */
  double get_weight(const core::index2 i) const;

  /// Prints the weights for all sequences
  void print_weights(std::ostream &out_stream) const;

protected:
  std::vector<double> weights_;
  std::vector<double> sum_distances_;
};

/** \brief Protocol for computing sequence weights based on inverse sequence identity
 *
 * Weight \f$ w_i \f$ for i-th sequence in a given set of \f$ N \f$ sequences is defined as:
 * \f[
 * w_i = \frac{1}{1+\sum_j a_{ij}}
 * \f]
 * where \f$ a_{ij} \f$ is sequence identity value measured between sequence <code>i</code> and <code>j</code>
 */
class InverseIdentitySequenceWeights : public AbstractSequenceWeightingProtocol {
public:

  /// Sets up the protocol with default parameters.
  InverseIdentitySequenceWeights() : logs("InverseIdentitySequenceWeights") {}

  /// Default virtual destructor
  virtual ~InverseIdentitySequenceWeights() = default;

  virtual void run();

private:
  utils::Logger logs;
};

/** \brief Protocol for computing sequence weights based on the Vingron & Argos method
 * The VA method (Vingron & Argos 1989) calculates the Hamming \f$ H(i,j) \f$ distance
 * between any two \f$ s_i, s_j \f$ sequences in the alignment. The weight \f$ w_i \f$ assigned to \f$ s_i \f$ sequence
 * is the sum of the distances of all other sequences in the alignment to that
 * sequence, divided by the sum of all pairwise distances, i.e.:
 *
 * \f[
 * w_i = \frac{\sum_j H(i,j)}{\sum_{i,j} H(i,j)}
 * \f]
 * @see Vingron,M. and Argos,P. (1989) Comput. Appl. Biosci., 5, 115–121.
 */
class VingronArgosSequenceWeights : public AbstractSequenceWeightingProtocol {
public:

  /// Sets up the protocol with default parameters.
  VingronArgosSequenceWeights()  : logs("VingronArgosSequenceWeights") {}

  /// Default virtual destructor
  virtual ~VingronArgosSequenceWeights() = default;

  virtual void run();

private:
  utils::Logger logs;
};

/** \brief Protocol for computing sequence weights based on the Henikoff's method
 *
 * @see Henikoff,S. and Henikoff,J.G. (1994) J. Mol. Biol., 243, 574–578.
 */
class HenikoffSequenceWeights : public AbstractSequenceWeightingProtocol {
public:

  /// Sets up the protocol with default parameters.
  HenikoffSequenceWeights()  : logs("HenikoffSequenceWeights") {}

  /// Default virtual destructor
  virtual ~HenikoffSequenceWeights() = default;

  virtual void run();

private:
  utils::Logger logs;
};

}
}

#endif

