#ifndef CORE_PROTOCOLS_PairwiseLigandCrmsd_H
#define CORE_PROTOCOLS_PairwiseLigandCrmsd_H

#include <memory>

#include <core/data/basic/Vec3.hh>
#include <core/data/structural/Structure.hh>
#include <core/protocols/PairwiseCrmsd.hh>

#include <utils/Logger.hh>
#include <utils/string_utils.hh>

using core::data::basic::Vec3;

namespace core {
namespace protocols {

using namespace core::data::structural;

/** @brief Calculates crmsd distances between ligand molecules after superimposing receptor conformations.
 *
 * This class assumes <strong>the receptor is rigid or semi-rigid</strong> i.e. its conformation doesn't change much
 * from a pose to pose. Based on this assumption, calculations may be greatly sped up. Rototranslation transformation
 * may be determined by superimposing all structures on the first one. \f$ N \times N \f$ evaluations of all-vs-all crsmd
 * can be thus performed in nearly linear time.
 *
 * @include ap_PairwiseCrmsd.cc
 */
class PairwiseLigandCrmsd : public PairwiseCrmsd {
public:

  /** @brief Creates an empty protocol instance.
   *
   * @param select_ligand_atoms - selector that defines the atoms to be used in crmsd calculations, after optimal superposition of receptor atoms
   * @param select_receptor_atoms - defines atoms that will be  used to define transformation; in null, all the atoms will be used for this purpose, including a ligand
   * Structures to be processed should be added with <code>add_input_coordinates()</code> method
   */
  PairwiseLigandCrmsd(const selectors::AtomSelector_SP select_ligand_atoms,
                      const selectors::AtomSelector_SP select_receptor_atoms = nullptr) :
    PairwiseCrmsd(select_receptor_atoms,select_ligand_atoms), logger("PairwiseLigandCrmsd") {}

  /// Default virtual destructor
  virtual ~PairwiseLigandCrmsd() = default;

  /** @brief Calculates crmsd for every pair of structures and writes the results to a stream.
   *
   * The output stream for the crmsd results may be defined by calling <code>set_outstream()</code> method
   */
  virtual void calculate();

  /** @brief Calculates crmsd between the given reference and every structure stored in this object.
   *
   * This method also performs structural superimpositions and writes the transformed structures into <code>pdb_output</code> stream
   * (if it is not null). The output stream for the crmsd results may be defined by calling <code>set_outstream()</code> method
   * @param reference - a pointer to the reference coordinates
   * @param pdb_output - stream where transformed (rotated & translated) atoms will be written); it may be null - nothing
   *    is written in that case
   */
  virtual void calculate(const Structure_SP reference,std::shared_ptr<std::ostream> pdb_output = nullptr);

private:
  utils::Logger logger;
  core::data::basic::Coordinates_SP tmp;
};

/**
 * \example ap_PairwiseCrmsd.cc
 */

}
}

#endif
