#include <core/protocols/structure_calculation_protocols.hh>

#include <core/calc/structural/local_backbone_geometry.hh>
#include <core/calc/structural/protein_angles.hh>
#include <core/chemical/ChiAnglesDefinition.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/interactions/ContactMap.hh>
#include <core/data/structural/ResidueSegmentProvider.hh>
#include <core/data/structural/selectors/ResidueSegmentSelector.hh>

namespace core {
namespace protocols {

void calculate_backbone_dihedrals(const core::data::structural::Structure_SP s, core::data::io::DataTable &out) {

  using namespace core::calc::structural;

  core::data::structural::ResidueSegmentProvider rsp(s, 3);
  core::data::structural::selectors::HasProperlyConnectedCA is_connected;
  Phi phi(1);
  Psi psi(1);
  Omega omega(1);
  std::string format{"%7.2f"};
  while(rsp.has_next()) {
    const core::data::structural::ResidueSegment_SP  seg = rsp.next();

    if (is_connected(*seg)) {
      core::data::io::TableRow tr;
      const auto & res = *(*seg)[1];
      // --- get info about the residue for which the angles are evaluated and insert it to a spreadsheet's row
      tr.push_back(res.id()).push_back(res.residue_type().code3).push_back(res.owner()->id());
      tr.push_back(utils::string_format(format, phi(*seg)));
      tr.push_back(utils::string_format(format, psi(*seg)));
      tr.push_back(utils::string_format(format, omega(*seg)));
      out.push_back(tr);
    }
  }
}


void calculate_sidechain_dihedrals(const core::data::structural::Chain &chain, core::data::io::DataTable &out) {

  core::data::structural::selectors::ResidueHasAllHeavyAtoms has_full_sc;

  for (core::index2 ipos = 0; ipos < chain.size(); ++ipos) {
    const core::data::structural::Residue & r = *chain[ipos];
    const core::chemical::Monomer &m = r.residue_type();
    if (!has_full_sc(r)) continue;
    if ((m.id == core::chemical::Monomer::ALA.id) || (m.id == core::chemical::Monomer::GLY.id)) continue;

    core::data::io::TableRow tr;
    // --- describe the residue, chain etc.
    tr.push_back(r.id()).push_back(m.code3).push_back(chain.id());
    tr.push_back(r.ss());    // --- secondary structure of the residue

    tr.push_back(core::calc::structural::define_rotamer(r));
    for (core::index2 i = 1; i <= core::chemical::ChiAnglesDefinition::count_chi_angles(m); ++i)
      tr.push_back(utils::string_format("%8.3f", core::calc::structural::evaluate_chi(r, i) * 180.0 / 3.14159));
    out.push_back(tr);
  }
}

void calculate_sidechain_dihedrals(const core::data::structural::Structure &structure, core::data::io::DataTable &out) {

  for (const auto &chain_sp : structure) calculate_sidechain_dihedrals(*chain_sp, out);
}


}
}

