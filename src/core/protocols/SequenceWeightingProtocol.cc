#include <core/index.hh>

#include <core/alignment/on_alignment_computations.hh>
#include <core/data/sequence/SequenceProfile.hh>
#include <core/protocols/SequenceWeightingProtocol.hh>

#include <utils/string_utils.hh>

namespace core {
namespace protocols {


double AbstractSequenceWeightingProtocol::get_weight(const core::index2 i) const { return weights_[i]; }

/// Prints the weights for all sequences
void AbstractSequenceWeightingProtocol::print_weights(std::ostream &out_stream) const {

  for (core::index2 i = 0; i < sequences_.size(); ++i)
    out_stream << utils::string_format("%5d %7.4f %s\n", i, weights_[i],
                                       sequences_[i]->header().substr(0, std::min(size_t(15), sequences_[i]->header().size())).c_str());
}


void InverseIdentitySequenceWeights::run() {

  if (weights_.size() != sequences_.size()) weights_.resize(sequences_.size());
  if (sum_distances_.size() != sequences_.size()) sum_distances_.resize(sequences_.size());

    for (core::index2 i = 0; i < weights_.size(); ++i) sum_distances_[i] = 0.0;
    for (core::index2 i = 1; i < weights_.size(); ++i) {
      for (core::index2 j = 0; j < i; ++j) {
        double aligned_seq_identity = core::alignment::semiglobal_identity_ratio(sequences_[i]->sequence, sequences_[j]->sequence);
        sum_distances_[i] += aligned_seq_identity;
        sum_distances_[j] += aligned_seq_identity;
      }
    }
    double total = 0;
    for (core::index2 i = 0; i < weights_.size(); ++i) {
      weights_[i] = 1.0 / (1.0 + sum_distances_[i]);
      total += weights_[i];
    }
    for (core::index2 i = 0; i < weights_.size(); ++i) weights_[i] /= total;

    logs << utils::LogLevel::INFO << weights_.size() * weights_.size() << " identities computed\n";
}


void VingronArgosSequenceWeights::run() {

  if (weights_.size() != sequences_.size()) weights_.resize(sequences_.size());
  if (sum_distances_.size() != sequences_.size()) sum_distances_.resize(sequences_.size());

  for (core::index2 i = 0; i < weights_.size(); ++i) sum_distances_[i] = 0.0;
  for (core::index2 i = 1; i < weights_.size(); ++i) {
    for (core::index2 j = 0; j < i; ++j) {
      double aligned_seq_identity = core::alignment::hamming_distance(sequences_[i]->sequence, sequences_[j]->sequence);
      sum_distances_[i] += aligned_seq_identity;
      sum_distances_[j] += aligned_seq_identity;
    }
  }
  double total = 0;
  for (core::index2 i = 0; i < weights_.size(); ++i) total += sum_distances_[i];

  for (core::index2 i = 0; i < weights_.size(); ++i) weights_[i] = sum_distances_[i] / total;

  logs << utils::LogLevel::INFO << weights_.size() * weights_.size() << " identities computed\n";
}

double aa_contribution(index2 i_aa, index2 seq_pos, const core::data::sequence::SequenceProfile & profile) {

  return profile.get_probability(seq_pos, i_aa) * profile.observed_residue_types(seq_pos);

}

void HenikoffSequenceWeights::run() {

  using core::data::sequence::SequenceProfile;

  if (weights_.size() != sequences_.size()) weights_.resize(sequences_.size());

  SequenceProfile sp(*sequences_[0],SequenceProfile::aaOrderByNCBIGapped(), sequences_);

  for (core::index2 i = 0; i < weights_.size(); ++i) {
    weights_[i] = 0.0;
    const std::string & seq = sequences_[i]->sequence;
    for (core::index2 seq_pos = 0; seq_pos < seq.size(); ++seq_pos) {
      if(seq[seq_pos] != '-' && seq[seq_pos] != '_') {
        double d =  sp.get_probability(seq_pos, seq[seq_pos]) * sp.observed_residue_types(seq_pos);
        d *= sequences_.size();
        weights_[i] += 1.0 / d;
      }
    }
  }

  double total = 0;
  for (core::index2 i = 0; i < weights_.size(); ++i) total += weights_[i];

  for (core::index2 i = 0; i < weights_.size(); ++i) weights_[i] = weights_[i] / total;

  logs << utils::LogLevel::INFO << weights_.size() * weights_.size() << " identities computed\n";
}

}
}

