#ifndef CORE_PROTOCOLS_FastaMatchProtocol_H
#define CORE_PROTOCOLS_FastaMatchProtocol_H

#include <string>
#include <ostream>

#include <core/index.hh>
#include <core/protocols/AbstractSequenceProtocol.hh>
#include <core/alignment/AlignmentBlock.hh>
#include <core/data/basic/SparseMap2D.hh>

namespace core {
namespace protocols {

typedef std::vector<core::alignment::AlignmentBlock> DiagonalsVec;
typedef std::shared_ptr<DiagonalsVec> DiagonalsVec_SP;

/** \brief Finds high similar fragments shared between two amino acid sequences
 *
 * For every pair of protein sequences registered into this object,  the protocol finds high similar
 * fragments (i.e. subsequences) shared between two amino acid sequences.
 *
 * The <code>AbstractSequenceProtocol::keep_alignments()</code> method does not influence this protocol:
 * FastaMatchProtocol always stores diagonal matches (if found for a given pair of sequences) as they are the only result of the protocol
 * This protocol however does not compute actual alignments
 *
 * \include ap_FastaMatchProtocol.cc
 */
class FastaMatchProtocol : public AbstractSequenceProtocol {
public:

  /** @brief Sets up the protocol with default parameters.
   *
   * Note, that this protocol inherits from <code>AbstractSequenceProtocol</code> and most of its setting
   * are actually set by that base class.
   */
  FastaMatchProtocol();

  /// Default virtual destructor
  virtual ~FastaMatchProtocol() = default;

  /** @brief Defines how long a diagonal match must be to get recorded by this protocol.
   *
   * @param match_length - length of the shortest diagonal matches this protocol will be recording
   * @return reference to <code>this</code> object to facilitate chaining
   */
  FastaMatchProtocol &shortest_match_recorded(const core::index1 match_length) {
    shortest_diagonal_recorded_ = match_length;
    return *this;
  }

  /** @brief Defines the longest gap when two matches located on the same diagonal may be combined into a single match
   *
   * <code>longest_gap</code> defines the maximum separation between two exactly matching blocks.
   * Two blocks of exact matches between a query and a template sequence may be separated by no more than
   * <code>longest_gap</code> residues and still be recorded as a diagonal match.
   * @return reference to <code>this</code> object to facilitate chaining
   */
  FastaMatchProtocol &longest_gap(const core::index1 longest_gap) {

    longest_gap_ = longest_gap + 3;
    return *this;
  }

  /** @brief Defines the minimum diagonal coverage criteria
   *
   * <code>minimum_diagonal_coverage</code> is used to decide whether a given pair of sequences
   * are significantly similar or not. It is defined as the total length of all diagonal matches
   * divided by the length of a shorter sequence
   * @return reference to <code>this</code> object to facilitate chaining
   */
  FastaMatchProtocol &minimum_diagonal_coverage(const double m) {

    minimum_diagonal_coverage_ = m;
    return *this;
  }

  /** @brief Defines the minimum sequence identity parameter
   *
   * <code>minimum_identity</code> is used to decide whether a given pair of sequences are significantly similar
   * or not. If the sequence identity of the longest match is lower than the given cutoff value, that match
   * will not be recorded.
   * @return reference to <code>this</code> object to facilitate chaining
   */
  FastaMatchProtocol &minimum_identity(const double m) {

    minimum_identity_ = m;
    return *this;
  }

  /** @brief Returns <code>shortest_diagonal_recorded</code> parameter value.
   *
   * @return <code>shortest_diagonal_recorded</code> value
   */
  core::index1 shortest_diagonal_recorded() { return shortest_diagonal_recorded_; }

  /** @brief Returns <code>longest_gap</code> parameter value.
   *
   * @return <code>longest_gap</code> value
   */
  core::index1 longest_gap() { return longest_gap_; }

  virtual void run();

  /** @brief provides a vector of sequences that match to a given sequence
   *
   * @param i - index of the query sequence
   * @param matches - vector where indexes of matching sequences will be inserted;
   * the vector will be emptied before matches are inserted
   * @return the number of matches
   */
  core::index4 matches(const core::index4 i, std::vector<index4> & matches) const;

  /// begin() const-iterator for matches found for a given sequence
  std::map<core::index4, DiagonalsVec_SP>::const_iterator cbegin(const index4 row) const;

  /// end() const-iterator for matches found for a given sequence
  std::map<core::index4, DiagonalsVec_SP>::const_iterator cend(const index4 row) const;

  /** @brief Checks whether there are any matches found between two given sequences.
   *
   * @param i - index of the first sequence
   * @param j - index of the second sequence
   * @return true if there is a match between the two sequences that satisfies criteria set up for this protocol
   */
  bool has_match(index4 i, index4 j) const;

  /** @brief Retrieves diagonal matches that has been already computed for a given pair of sequences.
   *
   * @param i - index of the first sequence
   * @param j - index of the second sequence
   * @return a list of diagonal matches found between the two sequences
   */
  const DiagonalsVec_SP get_diagonals(const core::index4 i, const core::index4 j);

  /// Prints the meaningful protein pairs and sequence identity between them
  void print_diagonals(std::ostream &out_stream);

  /// Prints the header string for the protocol's  results' table
  void print_header(std::ostream &out_stream) { out_stream << "#i_seq  j_seq :  n_identical seq_id i_fragm j_fragm | \n"; }

  /** @brief Returns the number of pairwise sequence alignment computations done by the most recent <code>run()</code> call
   *
   * Every <code>run()</code> call zeroes the counter, result is not cumulative
   * @return number of alignments computed in the most recent <code>run()</code> call
   */
  core::index4 n_jobs_completed() const { return n_jobs_completed_; }

protected:
  core::index1 shortest_diagonal_recorded_;
  core::index1 longest_gap_;
  double minimum_diagonal_coverage_;
  double minimum_identity_;
  std::vector<core::index2> sequence_length_;

  /** @brief Protocol results are stored there
   * This 2D array holds a vector of diagonal matches (@code core::alignment::AlignmentBlock @endcode instances)
   * a given  pair of aligned sequences. Note, than this array is sparse and it may be empty if there is no meaningful
   * diagonal for a given pair of sequences
   */
  core::data::basic::SparseMap2D<core::index4, DiagonalsVec_SP> diagonals;
  std::set<std::pair<index4, index4>> matches_;

private:
  utils::Logger logs;
  core::index4 n_jobs_completed_;

  /// For every input sequence, holds a list of aa-tuples which can be found in that sequence; these vectors are sorted
  std::vector<std::vector<index2>> tuples_4_sequence;
  /// For every sequence - holds a map that provides location of every tuple in that sequence
  std::vector<std::map<index2, std::vector<core::index2>>> tuples_map_4_sequence;


  /// Create tuples for every sequence in the input vector
  void process_sequences();

  /// Creates an integer hash unique for a given three-peptide
  index2 hash_for_tuple(const std::string & peptide) const;
};

}
}

#endif

