/** @file structure_calculation_protocols.hh
 *  @brief Provides functions that calculate various structural properties of a biomacromolecule.
 *
 * The common feature of the protocol gathered in this file is that they all store their output in a DataTable object.
 * They are therefore high-level functions intended to deliver print-ready output
 */
#ifndef CORE_PROTOCOLS_structure_calculation_protocols_HH
#define CORE_PROTOCOLS_structure_calculation_protocols_HH

#include <core/data/structural/Structure.hh>
#include <core/data/io/DataTable.hh>
#include <core/calc/structural/interactions/ContactMap.hh>

namespace core {
namespace protocols {

/** @brief Calculate protein backbone dihedral angles: \f$ \Phi, \Psi\f$ and \f$ \omega \f$ for each residue in a structure
 *
 * Results are converted to std::strings and stored in DataTable object, each angle value as a separate entry
 * @param structure - input structure
 * @param out - object where the results are stored; a Two dimensional table with a single row for each residue
*/
void calculate_backbone_dihedrals(const core::data::structural::Structure_SP structure, core::data::io::DataTable & out);

/** @brief Calculate protein side chain dihedral angles: \f$ \chi_1 - \chi_4\f$ for each residue in a chain
 *
 * Results are converted to std::strings and stored in DataTable object, each angle value as a separate entry.
 * @param chain - input chain
 * @param out - object where the results are stored; a Two dimensional table with a single row for each residue
 */
void calculate_sidechain_dihedrals(const core::data::structural::Chain &chain, core::data::io::DataTable & out);

/** @brief Calculate protein side chain dihedral angles: \f$ \chi_1 - \chi_4\f$ for each residue in a structure
 *
 * Results are converted to std::strings and stored in DataTable object, each angle value as a separate entry
 * @param structure - input structure
 * @param out - object where the results are stored; a Two dimensional table with a single row for each residue
 */
void calculate_sidechain_dihedrals(const core::data::structural::Structure &structure, core::data::io::DataTable &out);

}
}

#endif

