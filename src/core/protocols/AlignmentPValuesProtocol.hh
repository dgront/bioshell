#ifndef CORE_PROTOCOLS_AlignmentPValuesProtocol_H
#define CORE_PROTOCOLS_AlignmentPValuesProtocol_H

#include <string>
#include <utility> // for std::tuple

#include <core/index.hh>

#include <core/alignment/PairwiseAlignment.fwd.hh>
#include <core/alignment/aligner_factory.hh>
#include <core/data/basic/SparseMap2D.hh>
#include <core/protocols/AbstractSequenceProtocol.hh>

namespace core {
namespace protocols {

/** \brief Protocol for computing pairwise alignment of protein sequences.
 *
 * For every pair of protein sequences known inserted into this object,  the protocol calculates p-value
 * of their pairwise sequence alignment. The value is stored if and only if the p-value is low enough
 * i.e. its below the predefined cutoff.
 */
class AlignmentPValuesProtocol : public AbstractSequenceProtocol {
public:

  /** @brief Sets up the protocol with default parameters
   *
   */
  AlignmentPValuesProtocol();

  /// Methods that set up alignment parameters; the parameters remains the same for every alignment

  /** @name Protocol parameters
   */
  ///@{

  /** @brief Defines the number of sequence shufflings
   *
   * @param n_shuffles - how many times one of the sequences will be shuffled to estimate p-value
   * @return reference to <code>this</code> object to facilitate chaining
   */
  AlignmentPValuesProtocol &  n_shuffles(const core::index2 n_shuffles) { n_shuffles_ = n_shuffles; return *this; }

  /** @brief Defines the highest p-value for which results are still recorded by this protocol
   *
   * @param p_value_cut - p-value cutoff (0.001 is the default)
   * @return reference to <code>this</code> object to facilitate chaining
   */
  AlignmentPValuesProtocol &  p_value_cutoff(const double p_value_cut) { p_value_cut_ = p_value_cut; return *this; }
  ///@}

  /// Default virtual destructor
  virtual ~AlignmentPValuesProtocol() = default;

  virtual void run();

  alignment::PairwiseAlignment_SP get_alignment(const core::index2 i, const core::index2 j);

  /** @brief Returns p-value for aligning a given pair of sequences.
   *
   * Might return -1 if the p-value was above cutoff
   *
   * @param i - index of the first sequence
   * @param j - index of the second sequence
   * @return p-value for the respective alignment when it was below the thershold, -1 otherwise
   */
  double get_p_value(const core::index2 i, const core::index2 j);

  /// Prints the meaningful protein pairs and their p-values
  void print_p_values(std::ostream & out_stream);

private:
  core::index2 n_shuffles_;
  double p_value_cut_;
  core::data::basic::SparseMap2D<core::index2, double> scores_;
  core::data::basic::SparseMap2D<core::index2, core::alignment::PairwiseAlignment_SP> alignments_;
  utils::Logger logs;
};

}
}

#endif

