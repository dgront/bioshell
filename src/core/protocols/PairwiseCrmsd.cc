#include <memory>
#include <iostream>
#include <iomanip>

#include <core/data/basic/Vec3.hh>
#include <core/data/io/load_decoys.hh>
#include <core/data/basic/Array2D.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/transformations/Crmsd.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/protocols/PairwiseCrmsd.hh>

#include <utils/Logger.hh>
#include <utils/string_utils.hh>

namespace core {
namespace protocols {

using namespace core::data::structural;
using core::data::basic::Vec3;

PairwiseCrmsd::PairwiseCrmsd(const std::vector<Structure_SP> & decoys,
    const std::shared_ptr<selectors::AtomSelector> select_superimposed_atoms,
    const std::shared_ptr<selectors::AtomSelector> select_rotated_atoms,
    const std::vector<std::string> &decoy_ids) : PairwiseStructureComparison(decoys,
  select_superimposed_atoms, decoy_ids), select_rotated_(select_rotated_atoms), logger("PairwiseCrmsd") {

  // ---------- Extract coordinates that will be rotated
  if ((select_rotated_ != nullptr) && (select_rotated_ != select_superimposed_)) {
    for (const Structure_SP &xyz : decoys) {
      core::data::basic::Coordinates_SP xyzi = std::make_shared<core::data::basic::Coordinates>(0);
      if (n_atoms_rotate != 0) xyzi->resize(n_atoms_rotate);
      structure_to_coordinates(xyz, *xyzi, select_rotated_);
      n_atoms_rotate = xyzi->size();
      xyz_posttransform.push_back(xyzi);
    logger << utils::LogLevel::WARNING << "Added separated set of " << int(n_atoms_rotate) << " atoms for crmsd evaluation at index " 
      << xyz_posttransform.size() - 1 <<"\n";
    }
  }
}

core::index2 PairwiseCrmsd::add_input_structure(const Structure_SP strctr, const std::string &decoy_id) {

  // --- add the structure and apply stage-1-selector (superposition set)
  PairwiseStructureComparison::add_input_structure(strctr,decoy_id);

  // ---  apply stage-2-selector (crmsd set)
  if ((select_rotated_ != nullptr) && (select_rotated_ != select_superimposed_)) {
    core::data::basic::Coordinates_SP xyzi = std::make_shared<core::data::basic::Coordinates>(0);
    if (n_atoms_rotate != 0) xyzi->resize(n_atoms_rotate);
    structure_to_coordinates(strctr, *xyzi, select_rotated_);
    n_atoms_rotate = xyzi->size();
    xyz_posttransform.push_back(xyzi);

    logger << utils::LogLevel::WARNING << "Added separated set of " << int(n_atoms_rotate)
           << " atoms for crmsd evaluation at index "
           << xyz_posttransform.size() - 1 << "\n";
  }

  return xyz_matching.size() - 1;
}

void PairwiseCrmsd::calculate() {

  bool if_step_two = (xyz_posttransform.size() != 0);
  if (xyz_matching.size() < 2)
    logger << utils::LogLevel::WARNING << "At least two models must be given for pairwise crmsd\n";

  std::ostream & out = (out_stream != nullptr) ? *out_stream : std::cout;

  core::calc::structural::transformations::Crmsd<core::data::basic::Coordinates,core::data::basic::Coordinates> rms;
  for (core::index4 i = 1; i < xyz_matching.size(); ++i) {
    for (core::index4 j = 0; j < i; ++j) {
      double val = rms.crmsd(*xyz_matching[i], *xyz_matching[j], n_atoms_matching, if_step_two);
      if (!if_step_two) {
        if (val > max_crmsd_) continue;

        if (out_matrix_ != nullptr) {
          out_matrix_->insert(i, j, val);
          out_matrix_->insert(j, i, val);
        } else out << tags_[i] << " " << tags_[j] << " " << val << "\n";
      } else {
        double val2 = rms.calculate_crmsd_value(*xyz_posttransform[i], *xyz_posttransform[j], n_atoms_rotate);
        if (val2 > max_crmsd_) continue;

        if (out_matrix_ != nullptr) {
          out_matrix_->insert(i, j, val2);
          out_matrix_->insert(j, i, val2);
        } else out << tags_[i] << " " << tags_[j] << " " << val << " " << val2 << "\n";
      }
    }
  }
}

void PairwiseCrmsd::calculate(Structure_SP reference, std::shared_ptr<std::ostream> pdb_output) {

  // ---------- Prepare crmsd calculator
  core::calc::structural::transformations::Crmsd<core::data::basic::Coordinates,core::data::basic::Coordinates> rms;

  // ---------- Prepare the vector of native coordinates (for the superposition stage)
  core::data::basic::Coordinates_SP xyzn = std::make_shared<core::data::basic::Coordinates>(n_atoms_matching);
  structure_to_coordinates(reference, *xyzn, select_superimposed_);
  logger << utils::LogLevel::INFO << "Crmsd calculation to the native based on superposition of " << (int) n_atoms_matching << " atoms\n";

  // ---------- Prepare the vector of native coordinates that will be actually rotated (if needed)
  bool if_pdb_output = (pdb_output != nullptr);
  bool if_posttranform = (xyz_posttransform.size() != 0);
  core::data::basic::Coordinates_SP xyzn2 = nullptr;
  if(if_posttranform) {
    xyzn2 = std::make_shared<core::data::basic::Coordinates>(n_atoms_rotate);
    structure_to_coordinates(reference, *xyzn2, select_rotated_);
    logger << utils::LogLevel::INFO << "Final crmsd value will be computed on " << (int) n_atoms_rotate << " atoms after superposition of " 
      << (int) n_atoms_matching << " atoms\n";
  }
  std::ostream & out = (out_stream != nullptr) ? *out_stream : std::cout;
  selectors::AtomSelector_SP select_printed = (select_output_atoms_==nullptr) ?
      std::make_shared<core::data::structural::selectors::SelectEverything>() : select_output_atoms_;
  Vec3 tmp;
  if (if_posttranform) out << "#  name   crmsd  len  crmsd  len\n";
  else out << "#  name   crmsd  len\n";
  for (size_t i_model = 0; i_model < xyz_matching.size(); ++i_model) {
    double val = rms.crmsd(*xyz_matching[i_model], *xyzn, n_atoms_matching, if_pdb_output || if_posttranform);
    core::index2 n1 = xyzn->size();
    if (if_posttranform) {
      core::index2 n2 = xyzn2->size();
      // ---------- Rotate the second set of atoms according to the transformation from the first step and compute crmsd value
      out << utils::string_format("%8s %6.3f %4d %6.3f %4d\n", tags_[i_model].c_str(), val, n1,
        rms.calculate_crmsd_value_rotate(*xyz_posttransform[i_model], *xyzn2, n_atoms_rotate), n2);
    } else
      out << utils::string_format("%8s %6.3f %4d\n", tags_[i_model].c_str(), val, n1);
    if (if_pdb_output) {
      *pdb_output << "MODEL   " << std::setw(6) << (i_model + 1) << "\n";
      for (auto it = structures[i_model]->first_atom(); it != structures[i_model]->last_atom(); ++it) {
        if ((*select_printed)(**it)) {
          tmp.set(**it);
          rms.apply(**it);
          *pdb_output << (*it)->to_pdb_line() << "\n";
          (**it).set(tmp);
        }
      }
      *pdb_output << "ENDMDL\n";
    }
  }// ~ end of loop over models fot superposition
}

}
}

