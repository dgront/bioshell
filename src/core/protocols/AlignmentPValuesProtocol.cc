#include <string>

#include <core/index.hh>
#include <core/alignment/PairwiseSequenceAlignment.hh>
#include <core/alignment/aligner_factory.hh>
#include <core/alignment/scoring/NcbiSimilarityMatrixFactory.hh>
#include <core/alignment/scoring/SimilarityMatrixScore.hh>
#include <core/calc/statistics/OnlineStatistics.hh>
#include <core/calc/statistics/NormalDistribution.hh>
#include <core/protocols/AlignmentPValuesProtocol.hh>
#include <core/calc/statistics/Random.hh>
#include <core/alignment/on_alignment_computations.hh>
#include <utils/ThreadPool.hh>

namespace core {
namespace protocols {

typedef typename std::shared_ptr<core::alignment::AbstractAligner<short, core::alignment::scoring::SimilarityMatrixScore<short>>> AlignerType_SP;

AlignmentPValuesProtocol::AlignmentPValuesProtocol() : logs("AlignmentPValuesProtocol") {

  n_threads_ = 1;
  longest_len_ = 0;
  n_shuffles_ = 100;
  substitution_matrix_ = "BLOSUM62";
  gap_extend_ = -1;
  gap_open_ = -10;
  p_value_cut_ = 0.001;
  batch_size_ = 1000;
}

struct AlignmentPValuesProtocolOutput {

  index2 i;
  index2 j;
  double score;
  double avg;
  double sdev;
};

struct AlignmentPValuesProtocolJob {

  AlignmentPValuesProtocolJob(const std::vector<data::sequence::Sequence_SP> &sequences,
                              const core::alignment::scoring::NcbiSimilarityMatrix_SP substitution_matrix,
                              const core::alignment::AlignmentType atype,
                              core::index2 longest) : longest_len_(longest), sequences_(sequences),
                                                      sim_m(substitution_matrix), atype_(atype) {
  }

  AlignmentPValuesProtocolOutput operator()(const core::index2 i, const core::index2 j) {

    using namespace core::alignment;
    using namespace core::alignment::scoring;

    AlignerType_SP aligner = create_aligner<short, SimilarityMatrixScore<short>>(atype_, longest_len_);
    AlignmentPValuesProtocolOutput out;
    if (sequences_[i]->length() > sequences_[j]->length()) {
      out.i = i;
      out.j = j;
    } else {
      out.j = i;
      out.i = j;
    }
    std::string q = sequences_[out.i]->sequence;
    std::string t = sequences_[out.j]->sequence;

    {
      SimilarityMatrixScore<short> score(q, t, *sim_m);
      out.score = aligner->align_for_score(gap_open_, gap_extend_, score);
    }

    core::calc::statistics::Random &r = core::calc::statistics::Random::get();
    core::calc::statistics::OnlineStatistics stats;

    for (size_t k = 0; k < n_shuffles_ / 2; ++k) {
      shuffle(t.begin(), t.end(), r);
      SimilarityMatrixScore<short> score(q, t, *sim_m);
      stats(aligner->align_for_score(gap_open_, gap_extend_, score));
      shuffle(q.begin(), q.end(), r);
      SimilarityMatrixScore<short> score2(q, t, *sim_m);
      stats(aligner->align_for_score(gap_open_, gap_extend_, score2));
    }

    out.avg = stats.avg();
    out.sdev = sqrt(stats.var());

    return out;
  }

  short int gap_open_;
  short int gap_extend_;
  core::index2 n_shuffles_;
  core::index2 longest_len_;
  const std::vector<core::data::sequence::Sequence_SP> &sequences_;
  const core::alignment::scoring::NcbiSimilarityMatrix_SP sim_m;
  const core::alignment::AlignmentType atype_;
};

void AlignmentPValuesProtocol::run() {

  using namespace core::alignment;
  using namespace core::alignment::scoring;

  NcbiSimilarityMatrix_SP sim_m = NcbiSimilarityMatrixFactory::get().get_matrix(substitution_matrix_);
  std::shared_ptr<AbstractAligner<short, SimilarityMatrixScore<short>>> aligner =
    create_aligner<short, SimilarityMatrixScore<short>>(atype_, longest_len_);

  utils::ThreadPool pool(n_threads_); // --- Create a pool of  threads
  AlignmentPValuesProtocolJob job(sequences_, sim_m, atype_, longest_len_);
  job.gap_open_ = gap_open_;
  job.gap_extend_ = gap_extend_;
  job.n_shuffles_ = n_shuffles_;

  std::vector<std::future<AlignmentPValuesProtocolOutput>> futures;
  // --- Prepare iterator that goes through all pairs to be computed
  core::algorithms::IterateIJ iterator = prepare_iteration();
  auto it = iterator.begin();
  auto stop = iterator.end();
  while (it != stop) {

    // ---------- Clear remainings from jobs of the previous iteration
    futures.clear();

    // --- Submit tasks in batches
    for (core::index4 i = 0; i < batch_size_; ++i) {
      auto pair = *it;
      futures.push_back(pool.enqueue(job, pair.first, pair.second));
      if ((++it) == stop) break;
    }

    // ---------- Collect results
    for (core::index2 k = 0; k < futures.size(); ++k) {
      AlignmentPValuesProtocolOutput out = futures[k].get();
      double pval = 1 - core::calc::statistics::NormalDistribution::cdf(out.score, out.avg, out.sdev);
      if (pval > p_value_cut_) continue;

      index2 i = out.i;
      index2 j = out.j;
      scores_.insert(i, j, pval);
      if (if_store_alignments_) {
        SimilarityMatrixScore<short> score2(sequences_[i]->sequence, sequences_[j]->sequence, *sim_m);
        aligner->align(gap_open_, gap_extend_, score2);
        auto alignment = aligner->backtrace();
        alignments_.insert(i, j, alignment);
      }
    }
  }
}

alignment::PairwiseAlignment_SP AlignmentPValuesProtocol::get_alignment(const core::index2 i, const core::index2 j) {

  return alignments_.at(i, j, nullptr);
}

double AlignmentPValuesProtocol::get_p_value(const core::index2 i, const core::index2 j) {

  return scores_.at(i, j, -1.0);
}

void AlignmentPValuesProtocol::print_p_values(std::ostream &out_stream) {

  using namespace core::alignment;

  if (if_store_alignments_) out_stream << "#  i    j :  p-value   log() seq_id  n_gaps  len_i len_j\n";
  else out_stream << "#  i    j :  p-value   log()  len_i len_j\n";

  for (core::index2 i = 0; i < sequences_.size(); ++i) {
    if (scores_.has_row(i)) {
      for (auto iter = scores_.begin(i); iter != scores_.end(i); ++iter) {
        core::index2 j = (*iter).first;
        double p_value = (*iter).second;

        if (if_store_alignments_) {
          auto alignment = get_alignment(i, j);
          double aligned_seq_identity = sum_identical(*alignment, sequences_[i]->sequence, sequences_[j]->sequence);
          aligned_seq_identity /= double(std::min(sequences_[i]->sequence.length(), sequences_[j]->sequence.length()));
          core::index2 n_gaps = count_gaps(*alignment, sequences_[i]->sequence, sequences_[j]->sequence,
            (atype_ == AlignmentType::GLOBAL_ALIGNMENT));

          out_stream << utils::string_format("%4d %4d :  %7.4f %7.2f  %7.4f %5d  %5d %5d\n",
            i, j, p_value, log10(p_value), aligned_seq_identity, n_gaps, sequences_[i]->sequence.length(),
            sequences_[j]->sequence.length());
        } else
          out_stream << utils::string_format("%4d %4d :  %7.4f %7.2f  %5d %5d\n",
            i, j, p_value, log10(p_value), sequences_[i]->sequence.length(),
            sequences_[j]->sequence.length());
      }
    }
  }
}

}
}
