#include <memory>
#include <stdexcept>
#include <iostream>
#include <iomanip>

#include <core/data/basic/Vec3.hh>
#include <core/data/io/load_decoys.hh>
#include <core/data/basic/Array2D.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/transformations/Crmsd.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/protocols/PairwiseStructureComparison.hh>

#include <utils/Logger.hh>
#include <utils/string_utils.hh>

using core::data::basic::Vec3;

namespace core {
namespace protocols {

using namespace core::data::structural;

PairwiseStructureComparison::PairwiseStructureComparison(const std::vector<Structure_SP> & decoys,
        const selectors::AtomSelector_SP select_superimposed_atoms, const std::vector<std::string> & decoy_ids) :
        n_atoms_matching(0), out_stream(new std::ostream(std::cout.rdbuf())),
        max_crmsd_(1000.0), select_superimposed_(select_superimposed_atoms), logger("PairwiseStructureComparison") {

  // ---------- Check the input
  if (decoys.size() == 0) throw std::runtime_error("the vector of input structures is empty!");

  // ---------- Check if the size of the two input vector is the same (each structure needs exactly one name
  if ((decoy_ids.size() != 0) && (decoy_ids.size() != decoys.size()))
    logger << utils::LogLevel::SEVERE << "The number of structure tags differs from the number of structures!";

  // ---------- Extract coordinates from structures
  if (decoy_ids.size() == 0)
    for (const Structure_SP &xyz : decoys) add_input_structure(xyz,"");
  else
    for (index4 i = 0; i < decoys.size(); ++i) add_input_structure(decoys[i], decoy_ids[i]);

  logger << utils::LogLevel::INFO << "Registered " << (int) xyz_matching.size() << " decoys for structure similarity calculations, "
      << (int) n_atoms_matching << " atom each\n";
}

core::index2 PairwiseStructureComparison::add_input_structure(const Structure_SP strctr, const std::string &decoy_id) {

  structures.push_back(strctr);
  tags_.push_back((decoy_id.size() == 0) ? utils::to_string(xyz_matching.size()) : decoy_id);

  core::data::basic::Coordinates_SP xyzi = std::make_shared<core::data::basic::Coordinates>(0);
  if (n_atoms_matching != 0) xyzi->resize(n_atoms_matching);
  if(select_superimposed_!= nullptr) structure_to_coordinates(strctr, *xyzi, select_superimposed_);
  else  structure_to_coordinates(strctr, *xyzi);
  n_atoms_matching = xyzi->size();
  xyz_matching.push_back(xyzi);

  logger << utils::LogLevel::INFO << "Using " << (int) n_atoms_matching << " atoms for superimposition\n";

  return xyz_matching.size() - 1;
}

}
}

