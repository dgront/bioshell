#include <string>
#include <future>
#include <utility> // for std::tuple
#include <iomanip>

#include <core/index.hh>

#include <core/alignment/PairwiseAlignment.hh>
#include <core/alignment/scoring/NcbiSimilarityMatrixFactory.hh>
#include <core/alignment/scoring/SimilarityMatrixScore.hh>
#include <core/protocols/PairwiseSequenceIdentityProtocol.hh>
#include <core/protocols/FastaMatchProtocol.hh>

#include <core/alignment/on_alignment_computations.hh>
#include <utils/ThreadPool.hh>
#include <core/alignment/scoring/k_tuples.hh>
#include <core/algorithms/basic_algorithms.hh>

namespace core {
namespace protocols {

typedef typename std::shared_ptr<core::alignment::AbstractAligner<short, core::alignment::scoring::SimilarityMatrixScore<short>>> AlignerType_SP;

PairwiseSequenceIdentityProtocol::PairwiseSequenceIdentityProtocol() : logs_("PairwiseSequenceIdentityProtocol") {

  n_threads_ = 1;
  longest_len_ = 0;
  substitution_matrix_ = "BLOSUM62";
  gap_extend_ = -1;
  gap_open_ = -10;
  seq_identity_cutoff_ = 0.25;
  if_use_fasta_filter_ = false;
  n_chars_ = 6;
  batch_size_ = 1000;
}

struct SeqIdentityProtocolOutput {

  index4 i;
  index4 j;
  index2 n_identical;
  index2 alignment_length;
  index2 alignment_semi_length;
};

struct SeqIdentityProtocolJob {

  SeqIdentityProtocolJob(const std::vector<data::sequence::Sequence_SP> &sequences,
                         const core::alignment::scoring::NcbiSimilarityMatrix_SP substitution_matrix,
                         const core::alignment::AlignmentType atype, core::index2 longest) :
    longest_len_(longest), sequences_(sequences), sim_m(substitution_matrix), atype_(atype) {
  }

  SeqIdentityProtocolOutput operator()(const core::index4 i, const core::index4 j) {

    using namespace core::alignment;
    using namespace core::alignment::scoring;

    AlignerType_SP aligner = create_aligner<short, SimilarityMatrixScore<short>>(atype_, longest_len_);
    SeqIdentityProtocolOutput out;
    out.i = i;
    out.j = j;
    std::string q = sequences_[out.i]->sequence;
    std::string t = sequences_[out.j]->sequence;

    SimilarityMatrixScore<short> score(sequences_[i]->sequence, sequences_[j]->sequence, *sim_m);
    aligner->align(gap_open_, gap_extend_, score);
    auto alignment = aligner->backtrace();
    out.n_identical = sum_identical(*alignment, sequences_[i]->sequence, sequences_[j]->sequence);
    out.alignment_length = alignment->length();
    out.alignment_semi_length = out.alignment_length - alignment->n_front_gaps() - alignment->n_end_gaps();

    return out;
  }

  short int gap_open_;
  short int gap_extend_;
  core::index2 longest_len_;
  const std::vector<core::data::sequence::Sequence_SP> &sequences_;
  const core::alignment::scoring::NcbiSimilarityMatrix_SP sim_m;
  const core::alignment::AlignmentType atype_;
};



void PairwiseSequenceIdentityProtocol::print_header(std::ostream &out_stream) {
  out_stream << "#" << std::setw(n_chars_) << "i_seq" << std::setw(n_chars_) << "j_seq";
  out_stream << " : seq_id n_identical len_ali_no_tails len_ali_full i_len j_len\n";
}

void PairwiseSequenceIdentityProtocol::run() {

  using namespace core::alignment;
  using namespace core::alignment::scoring;

  n_jobs_completed_ = 0;

  NcbiSimilarityMatrix_SP sim_m = NcbiSimilarityMatrixFactory::get().get_matrix(substitution_matrix_);
  std::shared_ptr<AbstractAligner<short, SimilarityMatrixScore<short>>> aligner =
    create_aligner<short, SimilarityMatrixScore<short>>(atype_, longest_len_);

  SeqIdentityProtocolJob job(sequences_, sim_m, atype_, longest_len_);
  job.gap_open_ = gap_open_;
  job.gap_extend_ = gap_extend_;

  size_t n_jobs = 0; // --- the total number of pairwise calculations
  std::vector<std::future<SeqIdentityProtocolOutput>> futures;

  // --- Stuff to compute CUPS (cell updates per second)
  long long int n_cells = 0;
  std::chrono::time_point<std::chrono::high_resolution_clock> start = std::chrono::high_resolution_clock::now();

  // --- Prepare iterator that goes through all pairs to be computed
  core::algorithms::IterateIJ iterator = prepare_iteration();
  logs_ << utils::LogLevel::INFO << "Staring a protocol with " << iterator.count() << " calculations\n";

  // ---------- Here we create a FASTA filter
  FastaMatchProtocol fasta_filter;
  fasta_filter.n_threads(n_threads_).batch_size(100000).keep_alignments(false).printed_seqname_length(this->n_chars_);
  fasta_filter.minimum_diagonal_coverage(seq_identity_cutoff_).shortest_match_recorded(20).minimum_identity(
      seq_identity_cutoff_).longest_gap(8);

  index4 n_passed = 0;
  if(if_use_fasta_filter_) {  // ---  if it was requested by a user, populate the filter with the sequences
    logs_ << utils::LogLevel::INFO << "Using FASTA filer to limit the number of compared pairs\n";
    for(const Sequence_SP seq_sp : sequences_) fasta_filter.add_input_sequence(seq_sp);
    fasta_filter.run();       // --- run the filter to find matching pairs
  } else {
    logs_ << utils::LogLevel::INFO << "FASTA filer is NOT used\n";
  }

  utils::ThreadPool pool(n_threads_); // --- Create a pool of  threads
  pool.report_every_k(batch_size_);

  auto it = iterator.begin();
  auto stop = iterator.end();
  while (it != stop) {

    // ---------- Clear remainings from jobs of the previous iteration
    futures.clear();

    // --- Submit tasks in batches
    core::index4 i = 0;
    while(i < batch_size_) {
      auto pair = *it;
      // ---------- Apply FASTA filter if it was requested by a user
      if(if_use_fasta_filter_) {
        if (fasta_filter.has_match(pair.first, pair.second))
          futures.push_back(pool.enqueue(job, pair.first, pair.second));
      } else
        futures.push_back(pool.enqueue(job, pair.first, pair.second));
      if ((++it) == stop) break;
      ++i;
    }

    // ---------- Collect results
    for (core::index4 k = 0; k < futures.size(); ++k) {
      SeqIdentityProtocolOutput out = futures[k].get();
      index4 i = out.i;
      index4 j = out.j;
      ++n_jobs_completed_;
      ++n_jobs;
      n_cells += sequences_[i]->sequence.size() * sequences_[j]->sequence.size();
      double seqid = out.n_identical  / double(std::min(sequences_[i]->sequence.length(), sequences_[j]->sequence.length()));
      if (seqid < seq_identity_cutoff_) continue;
      PairwiseScores sc{out.n_identical,out.alignment_length,out.alignment_semi_length};
      scores_.insert(i, j, sc);
      if (if_store_alignments_) {
        SimilarityMatrixScore<short> score2(sequences_[i]->sequence, sequences_[j]->sequence, *sim_m);
        aligner->align(gap_open_, gap_extend_, score2);
        auto alignment = aligner->backtrace();
        alignments_.insert(i, j, alignment);
      }
    }
  }

  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(end - start);
  logs_ << utils::LogLevel::INFO << "GCUPS: " << double(n_cells) / time_span.count() / 1000000.0 << "\n";
  if (if_use_fasta_filter_)
    logs_ << utils::LogLevel::INFO << double(n_passed) / double(n_jobs_completed_) << " pairs passed the FASTA filte\n";
}

alignment::PairwiseAlignment_SP PairwiseSequenceIdentityProtocol::get_alignment(const core::index4 i, const core::index4 j) {

  return alignments_.at(i, j, nullptr);
}

core::index2 PairwiseSequenceIdentityProtocol::count_identical(const core::index4 i, const core::index4 j) {

  if (scores_.has_element(i, j))
    return scores_.at(i, j).n_identical;
  else return 0;
}

double PairwiseSequenceIdentityProtocol::get_sequence_identity(const core::index4 i, const core::index4 j) {

  if (scores_.has_element(i, j)) {
    core::index2 i_len = sequences_[i]->sequence.length();
    core::index2 j_len = sequences_[j]->sequence.length();
    return scores_.at(i, j).n_identical/ double(std::max(scores_.at(i, j).alignment_length_no_tail_gaps, std::min(j_len, i_len)));
  }
  else return 0;
}

/// Prints the meaningful protein pairs and their sequence identity values
void PairwiseSequenceIdentityProtocol::print_sequence_identity(std::ostream &out_stream) {

  for (core::index4 i = 0; i < sequences_.size(); ++i) {
    std::string si = sequences_[i]->header();
    si = utils::trim(si);
    std::replace(si.begin(), si.end(), ' ', '_');
    std::replace(si.begin(), si.end(), '\t', '_');
    core::index2 i_len = sequences_[i]->sequence.length();
    if (scores_.has_row(i)) {
      for (auto iter = scores_.begin(i); iter != scores_.end(i); ++iter) {
        core::index4 j = (*iter).first;
        core::index2 j_len = sequences_[j]->sequence.length();
        const PairwiseScores &pair = (*iter).second;
        double idperc =
            pair.n_identical / double(std::max(scores_.at(i, j).alignment_length_no_tail_gaps, std::min(j_len, i_len)));
        std::string sj = sequences_[j]->header();
        sj = utils::trim(sj);
        std::replace(sj.begin(), sj.end(), ' ', '_');
        std::replace(sj.begin(), sj.end(), '\t', '_');
        out_stream << utils::string_format("%s %s :  %7.4f     %5d %5d %5d  %5d %5d\n",
                                           si.substr(0, n_chars_).c_str(), sj.substr(0, n_chars_).c_str(),
                                           idperc, pair.n_identical, pair.alignment_length_no_tail_gaps,
                                           pair.alignment_length, i_len, j_len);
      }
    }
  }
}

}
}

