#include <core/index.hh>
#include <core/protocols/StructureAlignmentProtocol.hh>

#include <utils/ThreadPool.hh>

namespace core {
namespace protocols {

using namespace core::alignment;

core::alignment::PairwiseStructureAlignment_SP StructureAlignmentProtocol::operator()(const core::index2 i,const core::index2 j) {

  core::index4 k = key(i,j);
  if(alignments_.find(k)==alignments_.end()) alignments_[k] = align(i,j);

  return alignments_[k];
}

void StructureAlignmentProtocol::print_alignment_report_tabular(std::ostream & out_stream) {

  out_stream << "# query (Qlen) template (Tlen) : crmsd Nali | tmscore d0 by T | tmscore d0 by Q |\n";
  for(const auto & o : alignments_) {
    index4 k = o.first;
    index2 i = (k & 0x0000ffff);
    index2 j = (k >> 16);
    print_alignment_report_tabular(i, j, out_stream);
  }
}

void StructureAlignmentProtocol::print_alignment_report(std::ostream & out_stream) {

  for(const auto & o : alignments_) {
    index4 k = o.first;
    index2 i = (k & 0x0000ffff);
    index2 j = (k >> 16);
    print_alignment_report(i, j, out_stream);
  }
}


void StructureAlignmentProtocol::print_alignment_fasta(std::ostream & out_stream) {

  for(const auto & o : alignments_) {
    index4 k = o.first;
    index2 i = (k & 0x0000ffff);
    index2 j = (k >> 16);
    print_alignment_fasta(i, j, out_stream);
  }
}


void StructureAlignmentProtocol::write_aligned_structures() {

  for(const auto & o : alignments_) {
    index4 k = o.first;
    index2 i = (k & 0x0000ffff);
    index2 j = (k >> 16);

    std::string qn = utils::basename(coordinates[i].name);
    utils::trim_extensions(qn);
    std::string tn = utils::basename(coordinates[j].name);
    utils::trim_extensions(tn);

    write_aligned_structures(i, j, qn + "-on-" + tn + ".pdb", tn + "-on-" + qn + ".pdb");
  }
}

alignment::PairwiseStructureAlignment_SP StructureAlignmentProtocol::align(index2 i, index2 j) {

  log.log(utils::LogLevel::INFO,"Running ", coordinates[i].name, " vs ", coordinates[j].name, "\n");

  // ---------- Create the aligner and do the job
  std::shared_ptr<core::alignment::TMAlign> tma_sp =
    std::make_shared<core::alignment::TMAlign>(coordinates[i].coordinates(),coordinates[j].coordinates());
  tma_sp->align();

  auto out = std::make_shared<alignment::PairwiseStructureAlignment>(tma_sp->recent_alignment(),coordinates[i],coordinates[j]);
  out->job_id = key(i,j);

  return out;
}

void StructureAlignmentProtocol::align(index2 i) {

  log << utils::LogLevel::INFO << "called all-vs-native structure alignments\n";

  if (n_threads == 1) {
    for (core::index2 j = 0; j < i; ++j) {
      auto a = align(i, j);
      alignments_[key(i, j)] = a;
    }
    for (core::index2 j = i + 1; j < coordinates.size(); ++j) {
      auto a = align(i, j);
      alignments_[key(i, j)] = a;
    }

  } else {
    utils::ThreadPool pool(n_threads);
    std::vector<std::future<alignment::PairwiseStructureAlignment_SP>> futures;

    for (core::index2 j = 0; j < i; ++j) futures.push_back(pool.enqueue(*this, i, j));

    for (core::index2 j = i + 1; j < coordinates.size(); ++j) futures.push_back(pool.enqueue(*this, i, j));

    for (auto &f : futures) {
      auto a = f.get();
      alignments_[a->job_id] = a;
    }
  }
}

void StructureAlignmentProtocol::align() {

  log << utils::LogLevel::INFO << "called all-vs-all structure alignments\n";
  if (n_threads == 1) {
    for (core::index2 i = 1; i < coordinates.size(); ++i) {
      for (core::index2 j = 0; j < i; ++j) {
        auto a = align(i, j);
        alignments_[key(i,j)] = a;
      }
    }
  } else {
    utils::ThreadPool pool(n_threads);
    std::vector<std::future<alignment::PairwiseStructureAlignment_SP>> futures;
    for (core::index2 i = 1; i < coordinates.size(); ++i) {
      for (core::index2 j = 0; j < i; ++j) futures.push_back(pool.enqueue(*this, i, j));
    }

    for (auto &f : futures) {
      auto a = f.get();
      alignments_[a->job_id] = a;
    }
  }
}

void StructureAlignmentProtocol::print_alignment_fasta(core::index2 i, core::index2 j, std::ostream & out_stream) {

  // --- Check if the alignment between i and j has been already calculated
  core::index4 k = key(i,j);
  if(alignments_.find(k)==alignments_.end()) alignments_[k] = operator()(i,j); // --- if not, do it now
  const PairwiseStructureAlignment &strc_alignment = *alignments_[k]; // --- create a reference to the alignment object

  out_stream << core::data::io::create_fasta_string(*strc_alignment.alignment,*strc_alignment.query_sequence,*strc_alignment.template_sequence);
}

void StructureAlignmentProtocol::print_alignment_report(core::index2 i, core::index2 j, std::ostream & out_stream) {

  // --- Check if the alignment between i and j has been already calculated
  core::index4 k = key(i,j);
  if(alignments_.find(k)==alignments_.end()) alignments_[k] = operator()(i,j); // --- if not, do it now
  const PairwiseStructureAlignment &strc_alignment = *alignments_[k]; // --- create a reference to the alignment object

  const CoordinatesSet & query = coordinates[i]; // --- coordinates of all the selected atoms in query, not just the aligned ones
  const CoordinatesSet & tmplt = coordinates[j]; // --- atoms that are not selected, are not included; typically only CA are here

  core::index2 tmplt_size = tmplt.coordinates().size(); // --- size of the two vectors of coordinates
  core::index2 query_size = query.coordinates().size();

  out_stream << utils::string_format("query structure: %s (%d residues)\n", query.name.c_str(), query_size);
  out_stream << utils::string_format("reference structure: %s (%d residues)\n", tmplt.name.c_str(), tmplt_size);

  // --- Retrieve coordinates of aligned atoms (gaps not included) and evaluate tm-score, crmsd, etc.
  core::data::basic::Coordinates aligned_query_ca, aligned_tmplt_ca;
  strc_alignment.alignment->get_aligned_query_template(query.coordinates(), tmplt.coordinates(), aligned_query_ca, aligned_tmplt_ca);
  core::calc::structural::transformations::TMScore tms(aligned_query_ca, aligned_tmplt_ca);
  tms.tmscore(tmplt_size);
  double rms_value = tms.calculate_crmsd_value(aligned_query_ca,aligned_tmplt_ca,aligned_tmplt_ca.size());

  out_stream
    << utils::string_format("TM-score=%6.4f (if normalized by length of the template, i.e., LN=%d, d0=%.2f)\n",
      tms.recent_tmscore(tmplt_size), tmplt_size, core::calc::structural::transformations::TMScore::d0(tmplt_size));
  out_stream
    << utils::string_format("TM-score=%6.4f (if normalized by length of the query   , i.e., LN=%d, d0=%.2f)\n",
      tms.recent_tmscore(query_size), query_size, core::calc::structural::transformations::TMScore::d0(query_size));
  out_stream<<utils::string_format("Aligned length=%d, RMSD=%6.3f, Seq_ID=n_identical/n_aligned=0.136\n",
    strc_alignment.alignment->n_aligned(),rms_value);

  out_stream << "Transformation that superimposes the query on the reference:\n" << core::calc::structural::transformations::Rototranslation(tms) << "\n";
}


void StructureAlignmentProtocol::print_alignment_report_tabular(index2 i, index2 j, std::ostream &out_stream) {

  // --- Check if the alignment between i and j has been already calculated
  core::index4 k = key(i,j);
  if(alignments_.find(k)==alignments_.end()) alignments_[k] =  operator()(i,j); // --- if not, do it now
  const PairwiseStructureAlignment &strc_alignment = *alignments_[k]; // --- create a reference to the alignment object

  const CoordinatesSet & query = coordinates[i]; // --- coordinates of all the selected atoms in query, not just the aligned ones
  const CoordinatesSet & tmplt = coordinates[j]; // --- atoms that are not selected, are not included; typically only CA are here

  core::index2 tmplt_size = tmplt.coordinates().size(); // --- size of the two vectors of coordinates
  core::index2 query_size = query.coordinates().size();

  // --- Retrieve coordinates of aligned atoms (gaps not included) and evaluate tm-score, crmsd, etc.
  core::data::basic::Coordinates aligned_query_ca, aligned_tmplt_ca;
  strc_alignment.alignment->get_aligned_query_template(query.coordinates(), tmplt.coordinates(), aligned_query_ca, aligned_tmplt_ca);
  core::calc::structural::transformations::TMScore tms(aligned_query_ca, aligned_tmplt_ca);
  tms.tmscore(tmplt_size);
  double rms_value = tms.calculate_crmsd_value(aligned_query_ca, aligned_tmplt_ca, aligned_tmplt_ca.size());

  out_stream << utils::string_format("Q%04d: %s ( %3d aa) T%04d: %s ( %3d aa) : %6.3f %3d | %6.4f %4.2f | %6.4f %4.2f |\n",
    i, query.name.c_str(), query_size, j, tmplt.name.c_str(), tmplt_size,
    rms_value, strc_alignment.alignment->n_aligned(),
    tms.recent_tmscore(query_size), query_size, core::calc::structural::transformations::TMScore::d0(query_size),
    tms.recent_tmscore(tmplt_size), tmplt_size, core::calc::structural::transformations::TMScore::d0(tmplt_size));
}

void StructureAlignmentProtocol::write_aligned_structures(const core::index2 i,const core::index2 j,
    const std::string & query_fname,const std::string & tmplt_fname) {

  // --- Check if the alignment between i and j has been already calculated
  core::index4 k = key(i,j);
  if(alignments_.find(k)==alignments_.end()) alignments_[k] =  operator()(i,j); // --- if not, do it now
  const PairwiseStructureAlignment &strc_alignment = *alignments_[k]; // --- create a reference to the alignment object

  auto query = coordinates[i];
  auto tmplt = coordinates[j];
  std::vector<core::data::structural::Residue_SP> aligned_q;
  std::vector<core::data::structural::Residue_SP> aligned_t;

  // --- Retrieve aligned residues, skip gapped positions; will be used in PDB output
  strc_alignment.alignment->get_aligned_query_template(coordinates[i].residues(), coordinates[j].residues(),
    aligned_q, aligned_t);

  // --- Retrieve coordinates of aligned atoms (gaps not included) and evaluate tm-score, crmsd, etc.
  core::data::basic::Coordinates aligned_query_ca, aligned_tmplt_ca;
  strc_alignment.alignment->get_aligned_query_template(query.coordinates(), tmplt.coordinates(), aligned_query_ca, aligned_tmplt_ca);
  core::calc::structural::transformations::TMScore tms(aligned_query_ca, aligned_tmplt_ca);
  tms.tmscore(tmplt.coordinates().size());

  // --- Mark all atoms with occupancy 0.0 and then the aligned atoms with occupancy 1.0
  for (auto qi = query.structure->first_atom(); qi != query.structure->last_atom(); ++qi) (*qi)->occupancy(0.0);
  for (auto qr : aligned_q)
    for (auto qa : *qr) qa->occupancy(1.0);
  for (auto ti = tmplt.structure->first_atom(); ti != tmplt.structure->last_atom(); ++ti) (*ti)->occupancy(0.0);
  for (auto tr : aligned_t)
    for (auto ta : *tr) ta->occupancy(1.0);

  // --- Write aligned structures to PDB
  std::ofstream q_out(query_fname);
  for (auto qi = query.structure->first_atom(); qi != query.structure->last_atom(); ++qi) {
    tms.apply(**qi);
    q_out << (*qi)->to_pdb_line() << "\n";
  }
  std::ofstream t_out(tmplt_fname);
  for (auto ti = tmplt.structure->first_atom(); ti != tmplt.structure->last_atom(); ++ti)
    t_out << (*ti)->to_pdb_line() << "\n";
}

}
}

