#include <core/protocols/LigandsOnGridProtocol.hh>
#include <core/calc/statistics/OnlineStatistics.hh>

namespace core {
namespace protocols {

void LigandsOnGridProtocol::calculate() {

  if (xyz_matching.size() < 2)
    logger << utils::LogLevel::WARNING << "At least two models must be given for calculations\n";

  double cx = 0, cy = 0, cz = 0.0;
  for (core::index4 j = 0; j < xyz_matching[0]->size(); ++j) {
    const Vec3 & it = (*xyz_matching[0])[j];
    cx += (it).x;
    cy += (it).y;
    cz += (it).z;
  }
  cx /= double(xyz_matching[0]->size());
  cy /= double(xyz_matching[0]->size());
  cz /= double(xyz_matching[0]->size());

  grid_sp_ = std::make_shared<core::calc::structural::NeighborGrid3D>(cx, cy, cz, width_);

  std::ostream & out = (out_stream != nullptr) ? *out_stream : std::cout;
  core::calc::statistics::OnlineStatistics avg;
  // --- O(N) step : superimpose all the models on the first one
  core::calc::structural::transformations::Crmsd<core::data::basic::Coordinates, core::data::basic::Coordinates> rms;
  for (core::index4 i = 1; i < xyz_matching.size(); ++i) {
    avg(rms.crmsd(*xyz_matching[i], *xyz_matching[0], n_atoms_matching, true));
    core::data::structural::PdbAtom_SP ligand_cm = std::make_shared<core::data::structural::PdbAtom>();
    for (core::data::basic::Vec3 v:*(xyz_posttransform[i])) {
      (*ligand_cm)+=v;
      rms.apply(v);
    }
    (*ligand_cm) /= double(xyz_posttransform[i]->size());
    ligand_cm->id(i);
    grid_sp_->insert(ligand_cm);
  }

  out << "# Statistics for crmsd computed on receptor only (superimposition stage):\n" <<
      "# min   avg   var   max\n" <<
      utils::string_format("%6.3f %6.3f %6.3f %6.3f\n", avg.min(),avg.avg(),sqrt(avg.var()),avg.max());
}

}
}