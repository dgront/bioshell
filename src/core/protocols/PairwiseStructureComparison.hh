#ifndef CORE_PROTOCOLS_PairwiseStructureComparison_H
#define CORE_PROTOCOLS_PairwiseStructureComparison_H

#include <memory>

#include <core/data/basic/Vec3.hh>
#include <core/data/io/load_decoys.hh>
#include <core/data/basic/Array2D.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>
#include <core/calc/structural/transformations/Crmsd.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>

#include <utils/Logger.hh>
#include <utils/string_utils.hh>
#include <core/data/basic/SparseMap2D.hh>

using core::data::basic::Vec3;

namespace core {
namespace protocols {

using namespace core::data::structural;

/** @brief Data type used to store resulting values
 */
class ResultsMatrix : public core::data::basic::SparseMap2D<core::index2,float> {};

/// Shared pointer to ResultsMatrix
typedef std::shared_ptr<ResultsMatrix> ResultsMatrix_SP;

/** @brief Base class for pairwise structural comparison.
 *
 * User should provide input structures using <code>add_input_structure()</code> method.
 * Once all the structures are uploaded, call <code>calculate()</code> method.
 */
class PairwiseStructureComparison {
public:

  /** @brief Creates an empty protocol instance.
   *
   * Structures to be processed should be added with <code>add_input_coordinates()</code> method, which makes
   * a deep copy of the atoms selected by <code>select_superimposed_atoms</code>
   * @param select_superimposed_atoms - selector that defines the atoms to be used in the calculations
   *    use <code>nullptr</code> or a pointer to a <code>SelectAllAtoms</code> instance to select all the atoms of each input structure
   */
  PairwiseStructureComparison(const selectors::AtomSelector_SP select_superimposed_atoms = nullptr) :
    max_crmsd_(1000.0), select_superimposed_(select_superimposed_atoms), logger("PairwiseStructureComparison") {}

  /** @brief Constructor defines the set of structures for  calculations.
   *
   * Prepares this  calculator to compute similarity (or distance) values for all-vs-all structure pairs. All the structures must have the same number of atoms.
   * All the atoms are used in calculations.
   * @param decoys - a vector of structures (sets of atoms) to be used in calculations
   * @param select_superimposed_atoms - selector that defines the atoms to be used in the calculations;
   *    use <code>nullptr</code> or a pointer to a <code>SelectAllAtoms</code> instance to select all the atoms of each input structure
   * @param decoy_ids - strings to identify each of the structures (used for output). If this vector is empty (which is the default case),
   * order numbers will be used for this purpose
   */
  PairwiseStructureComparison(const std::vector<Structure_SP> &decoys,
                              const selectors::AtomSelector_SP select_superimposed_atoms = nullptr,
                              const std::vector<std::string> &decoy_ids = std::vector<std::string>());

  /// Empty virtual destructor to satisfy the compiler
  virtual ~PairwiseStructureComparison() = default;

  /** @brief Creates a deep copy of coordinates and stores them inside this object for future crmsd calculations
   *
   * @param strctr - input structure; all its atoms are copied
   * @param decoy_id - a string to identify the structure; if empty, the insertion index will be used
   * @return the index at which the given set of coordinates is stored
   */
  virtual core::index2 add_input_structure(const Structure_SP strctr, const std::string &decoy_id = "");

  /** @brief Defines the largest crmsd value for which results are still recorded by this protocol.
   *
   * @param max_crmsd - largest crmsd value for which are printed / recorded by this protocol
   * @return reference to <code>this</code> object to facilitate chaining
   */
  PairwiseStructureComparison & crmsd_cutoff(const double max_crmsd) { max_crmsd_ = max_crmsd; return *this; }

  /** @brief Defines an output stream where the resulting crmsd values will be written.
   *
   * @return reference to <code>this</code> object to facilitate chaining
   */
  PairwiseStructureComparison & output_stream(std::shared_ptr<std::ostream> output) { out_stream = output; return *this; }

  /** @brief Provide a selector  to choose which part of an input structures will be written in PDB output.
   *
   * The selector does not affect calculations; it is used only if user asks for PDB output.
   * @param select_output_atoms - output PDB files will contain only the atoms which satisfy this selector.
   *    By default all input atoms are printed to PDB output
   * @return reference to <code>this</code> object to facilitate chaining
   */
  PairwiseStructureComparison & output_selector(selectors::AtomSelector_SP select_output_atoms) {
    select_output_atoms_ = select_output_atoms;
    return *this;
  }

  /** @brief This virtual method that must be implemented by a derived class is responsible for the actual calculations.
   *
   * The output stream for the results may be defined by calling <code>set_outstream()</code> method
   */
  virtual void calculate() = 0;

  /** @brief Runs the calculations between the given reference and every structure stored in this object.
   *
   * @param reference - a pointer to the reference coordinates
   * @param pdb_output - stream where transformed (rotated & translated) atoms will be written); it may be null
   */
  virtual void calculate(const Structure_SP reference, std::shared_ptr<std::ostream> pdb_output = nullptr) = 0;

  /** @brief Provide an object where results of this protocol will be stored.
   *
   * Resuts are stored in ResultsMatrix, derived from core::data::basic::SparseMap2D<core::index2,float>
   * @param matrix - shared pointer to the matrix object
   */
  void set_out_matrix(ResultsMatrix_SP matrix) { out_matrix_ = matrix; }

  /** @brief Asks this object to create a new matrix to store results
   */
  void set_out_matrix() { out_matrix_ = std::make_shared<ResultsMatrix>(); }

  /** @brief Provide a read only access to results of this protocol
   * @return a shared pointer to ResultsMatrix object
   */
  const ResultsMatrix_SP out_matrix() const { return out_matrix_; }

  /** @brief Provide a read only access to structure names
   * @return a vector of strings, one name for each input structure
   */
  const std::vector<std::string> & tags() const { return tags_; }

protected:
  core::index4 n_atoms_matching = 0; ///< The number of atoms in a structure (the same for all of them)
  std::shared_ptr<std::ostream> out_stream = nullptr;
  selectors::AtomSelector_SP select_output_atoms_ = nullptr;
  double max_crmsd_; ///< Cut-off for storing/printing results (to limit the output)
  ResultsMatrix_SP out_matrix_ = nullptr;
  std::vector<std::string> tags_; ///< Human-readable names for input structures
  std::vector<core::data::structural::Structure_SP> structures; ///< The set of input structures - all atoms, also these not used in actual calculations
  std::vector<core::data::basic::Coordinates_SP> xyz_matching; ///< The set of input coordinates - only these which are used in structural comparison

  /** @brief Selector used to select atoms for structure comparison.
   *
   * The pairwise structure similarity will be computed based on atoms for which this selector returns true. If the selector
   * is nullptr - all the input atoms are selected
   */
  selectors::AtomSelector_SP select_superimposed_ = nullptr;

private:
  utils::Logger logger;
};

}
}

#endif
