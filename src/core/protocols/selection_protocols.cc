
#include <core/protocols/selection_protocols.hh>
#include <core/algorithms/predicates.hh>

namespace core {
namespace protocols {

struct ResidueHasSizeZero {
  bool operator()(core::data::structural::Residue_SP r) { return r->size() == 0;}
};

struct ChainHasSizeZero {
  bool operator()(core::data::structural::Chain_SP c) { return c->size() == 0;}
};

void keep_selected_atoms(const core::data::structural::selectors::AtomSelector &selector,
                         core::data::structural::Structure &strctr) {

  for (auto chain_sp : strctr) {
    for (auto residue_sp : *chain_sp) {
      residue_sp->erase(std::remove_if(residue_sp->begin(), residue_sp->end(),
        core::algorithms::Not<data::structural::selectors::AtomSelector>(selector)), residue_sp->end());
    }
    chain_sp->erase(std::remove_if(chain_sp->begin(), chain_sp->end(), ResidueHasSizeZero()), chain_sp->end());
  }
  strctr.erase(std::remove_if(strctr.begin(), strctr.end(), ChainHasSizeZero()), strctr.end());
}


void keep_selected_residues(const core::data::structural::selectors::ResidueSelector &selector,
    core::data::structural::Structure &strctr) {

  for (auto chain_sp : strctr) {
    chain_sp->erase(std::remove_if(chain_sp->begin(), chain_sp->end(),
          core::algorithms::Not<data::structural::selectors::ResidueSelector>(selector)), chain_sp->end());
  }

  strctr.erase(std::remove_if(strctr.begin(), strctr.end(), ChainHasSizeZero()), strctr.end());
}

void keep_selected_chains(const core::data::structural::selectors::ChainSelector &selector,
                          core::data::structural::Structure &strctr) {

  strctr.erase(std::remove_if(strctr.begin(), strctr.end(),
    core::algorithms::Not<data::structural::selectors::ChainSelector>(selector)), strctr.end());
}

core::index4 copy_selected_coordinates(data::structural::Structure &strctr,
    const data::structural::selectors::AtomSelector &selector, std::vector<Vec3> &output) {

  index4 out_cnt = 0;
  for(auto at_it = strctr.first_const_atom();at_it!=strctr.last_const_atom();++at_it) {
    if(selector(**at_it)) output.push_back(**at_it);
    ++out_cnt;
  }

  return out_cnt;
}

std::vector<core::data::basic::Vec3> copy_selected_coordinates(data::structural::Structure &strctr,
      const data::structural::selectors::AtomSelector &selector) {
  std::vector<core::data::basic::Vec3> output;
  copy_selected_coordinates(strctr, selector, output);
  return output;
}


std::vector<data::structural::PdbAtom_SP> copy_selected_atoms(data::structural::Structure &strctr, const data::structural::selectors::AtomSelector &selector) {

  std::vector<data::structural::PdbAtom_SP> output;
  copy_selected_atoms(strctr, selector, output);
  return output;
}

core::index4 copy_selected_atoms(data::structural::Structure &strctr, const data::structural::selectors::AtomSelector &selector,
                                       std::vector<data::structural::PdbAtom_SP> &output) {

  index4 out_cnt = 0;
  for (auto at_it = strctr.first_const_atom(); at_it != strctr.last_const_atom(); ++at_it) {
    if (selector(*at_it)) output.push_back(*at_it);
    ++out_cnt;
  }

  return out_cnt;
}

}
}

