#include <exception>

#include <core/protocols/AbstractSequenceProtocol.hh>

namespace core {
namespace protocols {

utils::Logger logs("AbstractSequenceProtocol");

AbstractSequenceProtocol & AbstractSequenceProtocol::add_query(const core::index4 which_query) {

  which_query_.push_back(which_query);
  return *this;
}

AbstractSequenceProtocol & AbstractSequenceProtocol::add_pair(const core::index4 i,const core::index4 j) {

  which_pairs_.push_back({i,j});

  return *this;
}

AbstractSequenceProtocol &AbstractSequenceProtocol::add_pair(const std::string &i_name, const std::string &j_name) {

  core::index4 is = std::numeric_limits<core::index4>::max(), js = std::numeric_limits<core::index4>::max();

  const auto ipair = seq_by_key.find(i_name);
  if(ipair==seq_by_key.cend())
    throw std::invalid_argument("Incorrect sequence name: " + i_name);
  else is = ipair->second;
  const auto jpair = seq_by_key.find(j_name);
  if(jpair==seq_by_key.cend())
    throw std::invalid_argument("Incorrect sequence name: " + j_name);
  else js = jpair->second;

  logs << utils::LogLevel::FINER << "pair " << i_name << " " << j_name << " inserted as " << is << "," << js << "\n";
  add_pair(is, js);

  return *this;
}

core::index4 AbstractSequenceProtocol::map_sequence(core::data::sequence::Sequence_SP seq) {

  std::string key = seq->header().substr(0,n_chars_);
  if(seq_by_key.find(key)!=seq_by_key.end())
    throw std::invalid_argument( "This sequence pool already contains a sequence under the key " +
                                     key + "\nThe registered sequence is: \n>" + sequences_[seq_by_key[key]]->header() +
                                     "\n" + sequences_[seq_by_key[key]]->sequence);
  logs << utils::LogLevel::FINER << "sequence key registered: " << key << "\n";
  sequences_.push_back(seq);
  seq_by_key[key] = sequences_.size() - 1;
  if (longest_len_ < seq->length()) longest_len_ = seq->length();

  return sequences_.size() - 1;
}

core::index4 AbstractSequenceProtocol::add_input_sequence(const core::data::sequence::Sequence_SP seq) {

  return map_sequence(seq);
}

core::index4 AbstractSequenceProtocol::add_input_sequence(const std::string &sequence) {

  return map_sequence(std::make_shared<core::data::sequence::Sequence>(
      utils::string_format("seq-%d", sequences_.size()), sequence, 1));
}

core::index4 AbstractSequenceProtocol::add_input_sequence(const std::string &header, const std::string &sequence) {

  return map_sequence(std::make_shared<core::data::sequence::Sequence>(header, sequence, 1));
}

core::index4
AbstractSequenceProtocol::add_input_sequences(const std::vector<core::data::sequence::Sequence_SP> &sequences) {

  for (core::data::sequence::Sequence_SP seq : sequences) {
    map_sequence(seq);
    if (longest_len_ < seq->length()) longest_len_ = seq->length();
  }

  return sequences_.size() - 1;
}

core::index4 AbstractSequenceProtocol::add_input_sequences(const std::vector<std::string> &sequences) {

  for (const std::string &sequence:sequences) {
    map_sequence(std::make_shared<core::data::sequence::Sequence>(
        utils::string_format("seq-%d", sequences_.size()), sequence, 1));
    if (longest_len_ < sequence.size()) longest_len_ = sequence.size();
  }
  return sequences_.size() - 1;
}

core::algorithms::IterateIJ AbstractSequenceProtocol::prepare_iteration(bool enforce_all_pairs) const {

  core::algorithms::IterateIJ it(sequences_.size(), !enforce_all_pairs);
  for (const core::index4 r : which_query_) it.add_selected_row(r);
  for (const auto &p:which_pairs_) it.add_selected_pair(p.first, p.second);

  return it;
}

}
}
