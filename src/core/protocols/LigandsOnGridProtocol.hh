#ifndef CORE_PROTOCOLS_LigandOnGridProtocol_H
#define CORE_PROTOCOLS_LigandOnGridProtocol_H

#include <core/protocols/PairwiseLigandCrmsd.hh>
#include <core/calc/structural/NeighborGrid3D.hh>

namespace core {
namespace protocols {
/** @brief Quickly groups ligand conformations in space by projecting them on a grid.
 *
 * The protocol takes a pool of 3D structures where a ligand has been docked to a receptor.
 * Receptor atoms are superimposed on a reference structure (typically the first one in the set)
 * Then the transformed (rotated and translated) conformation of a ligand is projected on a grid.
 *
 * It is assumed that receptor (e.g. protein) structures are quite similar; the protocol is therefore suitable
 * for fixed or semi-fixed docking.
 */
class LigandsOnGridProtocol : public PairwiseLigandCrmsd {
public:
  /** @brief Creates an empty protocol instance.
   *
   * @param select_ligand_atoms - selector that defines the atoms to be used in crmsd calculations,
   *    after optimal superposition of receptor atoms
   * @param select_receptor_atoms - defines atoms that will be  used to define transformation;
   *    in null, all the atoms will be used for this purpose, including a ligand.
   *    A good choice is to use alpha-carbons (IsCA selector)
   * Structures to be processed should be added with <code>add_input_structure()</code> method - the first one (ith index 0, is the representative one)
  */
  LigandsOnGridProtocol(const selectors::AtomSelector_SP select_ligand_atoms,
                       const selectors::AtomSelector_SP select_receptor_atoms = nullptr) :
      PairwiseLigandCrmsd(select_ligand_atoms,select_receptor_atoms), logger("LigandOnGridProtocol") {}

  /// Default virtual destructor
  virtual ~LigandsOnGridProtocol() = default;

  LigandsOnGridProtocol &box_grid_size(const double width) {
    width_ = width;
    return *this;
  }

  /** @brief Runs the protocol.
   *
   * The NeighborGrid3D object, produced by this protocol may be accessed by <code>grid()</code> method
   */
  void calculate();

  /** @brief Returns a grid 3D computed by this protocol
   * @return a pointer to object NeighborGrid3D_SP
   */
  const core::calc::structural::NeighborGrid3D_SP grid() { return grid_sp_; }

protected:
  double width_;    ///< Grid spacing
private:
  utils::Logger logger;
  core::calc::structural::NeighborGrid3D_SP grid_sp_;
};

}
}

#endif //CORE_PROTOCOLS_LigandOnGridProtocol_H
