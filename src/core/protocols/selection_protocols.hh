/** @file selection_protocols.hh
 *  @brief Provides functions that apply sub-structure selectors to a given structure
 */
#ifndef CORE_PROTOCOLS_selection_protocols_H
#define CORE_PROTOCOLS_selection_protocols_H

#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>

namespace core {
namespace protocols {

/** @brief Removes all these atoms from a given structure which do not satisfy a selector.
 * Once unselected atoms are removed, all empty residues and chains are removed as well.
 *
 * @param selector - an atom selector object
 * @param strctr - structure to be modified
 */
void keep_selected_atoms(const core::data::structural::selectors::AtomSelector &selector, core::data::structural::Structure &strctr);

/** @brief Removes all these residues from a given structure which do not satisfy a selector.
 * Once unselected residues are removed, all empty chains and chains are removed as well.
 *
 * @param selector - a residue selector object
 * @param strctr - structure to be modified
 */
void keep_selected_residues(const core::data::structural::selectors::ResidueSelector &selector, core::data::structural::Structure &strctr);

/** @brief Removes all the chains from a given structure which do not satisfy a selector.
 *
 * @param selector - a chain selector object
 * @param strctr - structure to be modified
 */
void keep_selected_chains(const core::data::structural::selectors::ChainSelector &selector, core::data::structural::Structure &strctr);

/** @brief Copies all the atoms of a given structure which do satisfy a selector.
 *
 * @param selector - an atom selector object
 * @param strctr - structure to be modified
 * @param output - vector where the coordinates are copied
 * @return the number of copied atoms
 */
core::index4 copy_selected_coordinates(data::structural::Structure &strctr,
      const data::structural::selectors::AtomSelector &selector, std::vector<core::data::basic::Vec3> &output);

/** @brief Copies all the atoms of a given structure which do satisfy a selector.
 *
 * @param selector - an atom selector object
 * @param strctr - structure to be modified
 * @return a newly allocated vector where the atoms are copied
 */
std::vector<core::data::basic::Vec3> copy_selected_coordinates(data::structural::Structure &strctr,
    const data::structural::selectors::AtomSelector &selector);

/** @brief Copies all the atoms of a given structure which do satisfy a selector.
 *
 * @param selector - an atom selector object
 * @param strctr - structure to be modified
 * @param output - vector where the atoms are copied
 * @return the number of copied atoms
 */
core::index4 copy_selected_atoms(data::structural::Structure &strctr, const data::structural::selectors::AtomSelector &selector,
                                 std::vector<data::structural::PdbAtom_SP> &output);

/** @brief Copies all the atoms of a given structure which do satisfy a selector.
 *
 * @param selector - an atom selector object
 * @param strctr - structure to be modified
 * @return a newly allocated vector where the atoms are copied
 */
std::vector<core::data::structural::PdbAtom_SP> copy_selected_atoms(data::structural::Structure &strctr,
    const data::structural::selectors::AtomSelector &selector);
}
}

#endif

