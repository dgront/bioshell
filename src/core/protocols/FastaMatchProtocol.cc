#include <string>
#include <future>
#include <utility> // for std::tuple

#include <core/index.hh>

#include <core/protocols/FastaMatchProtocol.hh>
#include <core/alignment/on_alignment_computations.hh>
#include <utils/ThreadPool.hh>
#include <core/algorithms/basic_algorithms.hh>
#include <core/alignment/scoring/k_tuples.hh>
#include <core/alignment/AlignmentBlock.hh>

namespace core {
namespace protocols {

using namespace core::alignment::scoring; // --- for k-tuples
using namespace core::data::sequence; // --- for core::data::sequence::Sequence

/** @brief Calculate the total length of all diagonal matches divided by the length of the shorter sequence
 * @param diagonals - a vector of all matching regions between two sequences
 * @param shorter_length - the length of the shorter of the two sequences
 * @return diagonal match coverage parameter
 */
double diagonal_match_coverage(const DiagonalsVec & diagonals, core::index2 shorter_length) {
  double total_len = 0;
  for(const auto & d: diagonals) total_len += d.size();
  return total_len / (double) shorter_length;
}

FastaMatchProtocol::FastaMatchProtocol() : logs("FastaMatchProtocol") {

  // --- settings from the base class
  n_threads_ = 1;
  longest_len_ = 0;
  n_chars_ = 6;
  batch_size_ = 1000;
  // --- settings for this particular protocol
  shortest_diagonal_recorded_ = 5;
  longest_gap_ = 7 + 3;
  minimum_diagonal_coverage_ = 0.3;
  minimum_identity_ = 0.5;
}

struct FastaMatchProtocolOutput {

  index4 i = 0;
  index4 j = 0;
  index2 longest_diagonal_match = 0;
  DiagonalsVec_SP diagonals = nullptr;
};


struct FastaMatchProtocolJob {

  FastaMatchProtocolJob(const core::index1 shortest_diagonal_recorded, core::index1 longest_gap,
                        double minimum_diagonal_coverage, const std::vector<core::index2> & sequence_length,
                        const std::vector<std::vector<index2>> & tuples_4_sequence,
                        std::vector<std::map<index2, std::vector<core::index2>>> & tuples_map_4_sequence) :
      shortest_diagonal_recorded_(shortest_diagonal_recorded), longest_gap_(longest_gap),
      minimum_diagonal_coverage_(minimum_diagonal_coverage),
      sequence_length_(sequence_length), tuples_4_sequence_(tuples_4_sequence),
      tuples_map_4_sequence_(tuples_map_4_sequence) {
  }

  FastaMatchProtocolOutput operator()(const core::index4 i, const core::index4 j) {

    using namespace core::alignment;
    using namespace core::alignment::scoring;

    FastaMatchProtocolOutput out;
    out.i = i;
    out.j = j;

    const auto &qtuples = tuples_4_sequence_[i];
    const auto &qmap = tuples_map_4_sequence_[i];
    const auto &ttuples = tuples_4_sequence_[j]; // --- k-character strings from template sequence
    const auto &tmap = tuples_map_4_sequence_[j]; // --- k-tuples gathered by position in the template sequence
    matching_tuples_.clear();
    hotspots_by_offset_.clear();
    diagonals_tmp_.clear();

    // ---------- Finds these k-tuples (k-character strings) which appears in both sequences
    core::algorithms::intersect_sorted(qtuples.begin(), qtuples.end(), ttuples.begin(), ttuples.end(), matching_tuples_);

    // ---------- For every tuple that can be found in both sequences, i.e. for every tuple match
    for (const index2 match : matching_tuples_) {
      for (core::index2 q_index: qmap.at(match)) { // --- that tuple may be found in more that one place in a query
        for (core::index2 t_index: tmap.at(match)) { // --- that tuple may be found in more that one place in a template
          int offset = q_index - t_index;
          if (hotspots_by_offset_.find(offset) == hotspots_by_offset_.end())
            hotspots_by_offset_[offset] = std::vector<std::pair<core::index2, core::index2>>{};
          hotspots_by_offset_[offset].push_back(std::make_pair(q_index, t_index));
        }
      }
    }

    // ---------- Here we compile a list of diagonals i.e. of blocks of merged hot-spots
    core::alignment::AlignmentBlock b{0, 0, 0, 0};
    for (auto p : hotspots_by_offset_) {
      std::vector<std::pair<core::index2, core::index2>> &hsp = p.second;
      // ---------- sort hotspots by index in query
      std::sort(hsp.begin(), hsp.end(),
                [](std::pair<core::index2, core::index2> p1, std::pair<core::index2, core::index2> p2) {
                    return p1.first < p2.first;
                });

      // ---------- Push the very first diagonal to a temporary vector
      diagonals_tmp_.emplace_back(hsp[0].first, hsp[0].first + 3 - 1, hsp[0].second, hsp[0].second + 3 - 1);
      for (size_t ii = 1; ii < hsp.size(); ++ii) {
        // ---------- Create a temporary block from i-th hot-spot
        b.first_query_pos = hsp[ii].first;
        b.last_query_pos = core::index2(hsp[ii].first + 3 - 1);
        b.first_tmplt_pos = hsp[ii].second;
        b.last_tmplt_pos = core::index2(hsp[ii].second + 3 - 1);
        // ---------- If the most recent block cannot be extended with block b, push it as the last block
        if (!(diagonals_tmp_.back().combine_with(b, longest_gap_))) {
          // ---------- if the previously accepted block is too short, replace it with a new one
          if (diagonals_tmp_.back().size() < shortest_diagonal_recorded_)
            diagonals_tmp_[diagonals_tmp_.size() - 1] = b;
          else // ---------- otherwise push the new one back to the vector. This will remove too short blocks from results
            diagonals_tmp_.push_back(b);
        }
      }
    }

    // ---------- Calculate the total length of all diagonal matches; here we don't care they may not be on the same diagonal line but on parallel ones
    double total_len = 0;
    for (auto iter = diagonals_tmp_.begin(); iter != diagonals_tmp_.end(); ++iter) total_len += (iter)->size();
    total_len /= std::min(sequence_length_[i], sequence_length_[j]);
    if (total_len < minimum_diagonal_coverage_) {
      out.longest_diagonal_match = 0;
      return out;
    }
    // ---------- Sort diagonals by length
    std::sort(diagonals_tmp_.begin(), diagonals_tmp_.end(),
              [](const AlignmentBlock &bi, const AlignmentBlock &bj) { return bi.size() > bj.size(); });

    out.longest_diagonal_match = diagonals_tmp_[0].size();
    out.diagonals = std::make_shared<std::vector<core::alignment::AlignmentBlock>>();
    for (auto iter = diagonals_tmp_.begin(); iter != diagonals_tmp_.end(); ++iter)
      out.diagonals->push_back(*iter);

    return out;
  }

private:
  /// Protocol's parameters from FastaMatchProtocol
  const core::index1 shortest_diagonal_recorded_;
  const core::index1 longest_gap_;
  const double minimum_diagonal_coverage_;
  const std::vector<core::index2> sequence_length_;

  ///< Data from FastaMatchProtocol - tuples precomputed for every sequence
  const std::vector<std::vector<index2>> & tuples_4_sequence_;
  const std::vector<std::map<index2, std::vector<core::index2>>> & tuples_map_4_sequence_;

  /// Data for the job
  std::vector<index2> matching_tuples_;
  std::map<int, std::vector<std::pair<core::index2, core::index2>>> hotspots_by_offset_;
  DiagonalsVec diagonals_tmp_;
};

/// Create tuples for every sequence in the input vector
void FastaMatchProtocol::process_sequences() {

  using namespace core::alignment::scoring;
  auto start = std::chrono::high_resolution_clock::now(); // --- timer starts!

  // ---------- Temporary storage of tuples as std::string before they are converted to core::index2
  std::map<std::string, std::vector<core::index2>> qmap; // --- stores query tuples and their location

  sequence_length_.reserve(sequences_.size());
  for (const Sequence_SP &sequence : sequences_) {
    sequence_length_.push_back(sequence->length());
    qmap.clear();
    create_k_tuples<3>(sequence->sequence, qmap);
    std::map<index2, std::vector<core::index2>> i_qmap; // --- stores query tuples and their location
    std::vector<index2> i_qtuples;// --- stores tuple strings -  keys to qmap (sorted  )
    for (const auto &p : qmap) {
      index2 ip = hash_for_tuple(p.first);
      i_qtuples.push_back(ip);
      i_qmap[ip] = p.second;
    }

    std::sort(i_qtuples.begin(), i_qtuples.end());
    tuples_map_4_sequence.push_back(i_qmap);
    tuples_4_sequence.push_back(i_qtuples);
  }
  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
  logs << utils::LogLevel::INFO << (size_t ) sequences_.size()
       << " sequences preprocessed within " << time_span.count() << " [s]\n";
}

bool FastaMatchProtocol::has_match(index4 i, index4 j) const {

  if (matches_.find(std::pair<index4, index4>{i, j}) != matches_.cend()) return true;
  if (matches_.find(std::pair<index4, index4>{j, i}) != matches_.cend()) return true;
  return false;
}

index4 FastaMatchProtocol::matches(const core::index4 i, std::vector<index4> & matches_id) const {

  matches_id.clear();
  for (const std::pair<index4, index4> & m:matches_) {
    if (m.first == i) matches_id.push_back(m.second);
    else if (m.second == i) matches_id.push_back(m.first);
  }

  return matches_id.size();
}

index2 FastaMatchProtocol::hash_for_tuple(const std::string & peptide) const {

  static const std::string order = "ACDEFGHIKLMNPQRSTVWXY";
  index2 t = 0;
  int f = 1;
  for (int i = 0; i < peptide.size(); ++i) {
    size_t k = order.find(peptide[i]);
    k = (k == std::string::npos) ? 20 : k;
    t += k * f;
    f *= 21;
  }

  return t;
}

void FastaMatchProtocol::run() {

  // --- Prepare k-tuples
  process_sequences();

  utils::ThreadPool pool(n_threads_); // --- Create a pool of  threads
  pool.report_every_k(batch_size_);
  FastaMatchProtocolJob job(shortest_diagonal_recorded_, longest_gap_, minimum_diagonal_coverage_,
              sequence_length_, tuples_4_sequence, tuples_map_4_sequence);

  n_jobs_completed_ = 0;

  size_t n_jobs = 0; // --- the total number of pairwise calculations
  std::vector<std::future<FastaMatchProtocolOutput>> futures;

  auto start = std::chrono::high_resolution_clock::now();

  // --- Prepare iterator that goes through all pairs to be computed
  core::algorithms::IterateIJ iterator = prepare_iteration();
  logs << utils::LogLevel::INFO << "Starting a protocol with " << iterator.count() << " calculations\n";
  auto it = iterator.begin();
  auto stop = iterator.end();
  while (it != stop) {

    // ---------- Clear remainings from jobs of the previous iteration
    futures.clear();

    // --- Submit tasks in batches
    for (core::index4 i = 0; i < batch_size_; ++i) {
      auto pair = *it;
      futures.push_back(pool.enqueue(job, pair.first, pair.second));
      if ((++it) == stop) break;
    }

    // ---------- Collect results
    for (core::index4 k = 0; k < futures.size(); ++k) {
      FastaMatchProtocolOutput out = futures[k].get();
      ++n_jobs_completed_;
      ++n_jobs;
      if (out.longest_diagonal_match < shortest_diagonal_recorded_) continue;
      unsigned long i = out.i;
      unsigned long j = out.j;
      std::remove_if(out.diagonals->begin(), out.diagonals->end(),
                     [this, i, j](core::alignment::AlignmentBlock &b) {
                         return b.fraction_identical(sequences_[i]->sequence, sequences_[j]->sequence) <
                                minimum_identity_;
                     });
      if (out.diagonals->empty()) continue;
      matches_.insert({i,j}); // --- insert a match into the set!
      if(if_store_alignments_) diagonals.insert(i, j, out.diagonals);
    }
  }

  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
  logs << utils::LogLevel::INFO << (size_t ) n_jobs
      << " FASTA searches done within " << time_span.count() << " [s], " << diagonals.size() << " hits found\n";
}

const DiagonalsVec_SP  FastaMatchProtocol::get_diagonals(const core::index4 i, const core::index4 j) {
  return diagonals.at(i,j);
}

std::map<core::index4, DiagonalsVec_SP>::const_iterator FastaMatchProtocol::cbegin(const index4 row) const {

  return diagonals.cbegin(row);
}

std::map<core::index4, DiagonalsVec_SP>::const_iterator FastaMatchProtocol::cend(const index4 row) const{

  return diagonals.cend(row);
}

void FastaMatchProtocol::print_diagonals(std::ostream &out_stream) {

  for (index4 ir = 0; ir <= diagonals.max_row_index(); ++ir) {
    if (!diagonals.has_row(ir)) continue;
    std::string row_seq_name = sequences_[ir]->header();
    row_seq_name = utils::trim(row_seq_name).substr(0, n_chars_);
    for (auto it = diagonals.cbegin(ir); it != diagonals.cend(ir); ++it) {
      if(!it->second) continue;
      index4 ic = it->first;
      std::string col_seq_name = sequences_[ic]->header();
      col_seq_name = utils::trim(col_seq_name).substr(0, n_chars_);
      int idiag = 0;
      std::stringstream out;
      out << utils::string_format("%s %s : %d %d : ", row_seq_name.c_str(), col_seq_name.c_str(),
                                  sequences_[ir]->sequence.size(), sequences_[ic]->sequence.size());

      const DiagonalsVec &cuurent_diags = *(it->second);
      out << diagonal_match_coverage(cuurent_diags, std::min(sequences_[ir]->length(), sequences_[ic]->length())) <<" : ";
      double n_cov = 0;
      for (const auto &d:cuurent_diags) {
          std::string q_seq = d.query_sequence(sequences_[ir]->sequence);
          std::string t_seq = d.template_sequence(sequences_[ic]->sequence);
          double f_id = core::alignment::sum_identical(q_seq, t_seq) / double(q_seq.size());
          if (f_id < minimum_identity_) continue;
          out << utils::string_format("%4d %5.3f %s %s | ", q_seq.size(), f_id, q_seq.c_str(),
                                      t_seq.c_str());
          n_cov += q_seq.size();
          ++idiag;
      }
      if ((idiag > 0) &&
          (n_cov / std::min(sequences_[ir]->length(), sequences_[ic]->length()) > minimum_diagonal_coverage_))
        out_stream << out.str() << "\n";
    }
  }
}

}
}

