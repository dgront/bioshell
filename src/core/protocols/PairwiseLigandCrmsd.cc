#include <memory>
#include <iostream>
#include <iomanip>

#include <core/index.hh>
#include <core/data/basic/Vec3.hh>
#include <core/data/io/load_decoys.hh>
#include <core/data/basic/Array2D.hh>
#include <core/calc/statistics/OnlineStatistics.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/transformations/Crmsd.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/protocols/PairwiseLigandCrmsd.hh>

#include <utils/Logger.hh>
#include <utils/string_utils.hh>

namespace core {
namespace protocols {

using namespace core::data::structural;
using core::data::basic::Vec3;

void PairwiseLigandCrmsd::calculate() {

  if (xyz_matching.size() < 2)
    logger << utils::LogLevel::WARNING << "At least two models must be given for pairwise crmsd\n";

  std::ostream & out = (out_stream != nullptr) ? *out_stream : std::cout;
  core::calc::statistics::OnlineStatistics avg;
  // --- O(N) step : superimpose all the models on the first one
  core::calc::structural::transformations::Crmsd<core::data::basic::Coordinates, core::data::basic::Coordinates> rms;
  for (core::index4 i = 1; i < xyz_matching.size(); ++i) {
    avg(rms.crmsd(*xyz_matching[i], *xyz_matching[0], n_atoms_matching, true));
    for (core::data::basic::Vec3 v:*(xyz_posttransform[i])) rms.apply(v);
  }

  out << "# Statistics for crmsd computed on receptor only (superimposition stage):\n" <<
      "# min   avg   var   max\n" <<
      utils::string_format("%6.3f %6.3f %6.3f %6.3f\n", avg.min(),avg.avg(),sqrt(avg.var()),avg.max());
  for (core::index4 i = 1; i < xyz_posttransform.size(); ++i) {
    const core::data::basic::Coordinates & xyz_i = *(xyz_posttransform[i]);
    for (core::index4 j = 0; j < i; ++j) {
      const core::data::basic::Coordinates & xyz_j = *(xyz_posttransform[j]);
      double s = 0;
      for(core::index2 k=0;k<n_atoms_rotate;++k) {
        double t = xyz_i[k].x - xyz_j[k].x;
        s += t * t;
        t = xyz_i[k].y - xyz_j[k].y;
        s += t * t;
        t = xyz_i[k].z - xyz_j[k].z;
        s += t * t;
      }
      s = sqrt(s / (double) n_atoms_rotate);
      if (s > max_crmsd_) continue;
      if (out_matrix_ != nullptr) {
        out_matrix_->insert(i, j, s);
        out_matrix_->insert(j, i, s);
      } else
        out << tags_[i] << " " << tags_[j] << " " << s << "\n";
    }
  }
}

void PairwiseLigandCrmsd::calculate(const Structure_SP reference, std::shared_ptr<std::ostream> pdb_output) {

  // ---------- Prepare crmsd calculator
  core::calc::structural::transformations::Crmsd<core::data::basic::Coordinates,core::data::basic::Coordinates> rms;

  // ---------- Prepare the vector of native receptor coordinates (for the superposition stage)
  core::data::basic::Coordinates_SP xyzn = std::make_shared<core::data::basic::Coordinates>(n_atoms_matching);
  structure_to_coordinates(reference, *xyzn, select_superimposed_);

  // ---------- Prepare the vector of native ligand coordinates (for the crmsd calculations)
  core::data::basic::Coordinates_SP lign = std::make_shared<core::data::basic::Coordinates>(n_atoms_rotate);
  structure_to_coordinates(reference, *lign, select_rotated_);

  Vec3 tmp;
  *out_stream << "#  name   crmsd  len  crmsd  len\n";

  for (size_t i_model = 0; i_model < xyz_matching.size(); ++i_model) {
    double val = rms.crmsd(*xyz_matching[i_model], *xyzn, n_atoms_matching, true);

    // ---------- Rotate the second set of atoms according to the transformation from the first step and compute crmsd value
    *out_stream << utils::string_format("%8s %6.3f %4d %6.3f %4d\n", tags_[i_model].c_str(), val, n_atoms_matching,
      rms.calculate_crmsd_value_rotate(*xyz_posttransform[i_model], *lign, n_atoms_rotate), n_atoms_rotate);

    if (pdb_output != nullptr) {
      *pdb_output << "MODEL   " << std::setw(6) << (i_model + 1) << "\n";
      for (auto it = structures[i_model]->first_atom(); it != structures[i_model]->last_atom(); ++it) {
        if ((!select_output_atoms_) || (*select_output_atoms_)(**it)) {
          tmp.set(**it);
          rms.apply(**it);
          *pdb_output << (*it)->to_pdb_line() << "\n";
          (**it).set(tmp);
        }
      }
      *pdb_output << "ENDMDL\n";
    }
  }// ~ end of loop over models fot superposition
}

}
}

