#ifndef CORE_PROTOCOLS_PairwiseCrmsd_H
#define CORE_PROTOCOLS_PairwiseCrmsd_H

#include <memory>

#include <core/data/basic/Vec3.hh>
#include <core/data/basic/Array2D.hh>
#include <core/data/structural/Structure.hh>
#include <core/protocols/PairwiseStructureComparison.hh>

#include <utils/Logger.hh>
#include <utils/string_utils.hh>

using core::data::basic::Vec3;

namespace core {
namespace protocols {

using namespace core::data::structural;

/** @brief A protocol which evalues crmsd on a pair of structures or their fragments
 *
 * This class uses <code>Crmsd</code> class to calculate all crmsd distances between the given set(s) of structures.
 * It is possible to evaluate crmsd on a subset of atoms. Also the set of transformed atoms may be specified separately.
 *
 * @include ap_PairwiseCrmsd.cc
 */
class PairwiseCrmsd : public PairwiseStructureComparison {
public:

  /** @brief Creates an empty protocol instance.
   *
   * Structures to be processed should be added with <code>add_input_coordinates()</code> method, which makes
   * a deep copy of the atoms selected by <code>select_atoms</code>
   * @param select_superimposed_atoms - selector that defines the atoms to be used for superimposition 
   * @param select_rotated_atoms - selector that defines the atoms to be used for crmsd calculations 
   */
  PairwiseCrmsd(const selectors::AtomSelector_SP select_superimposed_atoms, const selectors::AtomSelector_SP select_rotated_atoms) :
      PairwiseStructureComparison(select_superimposed_atoms), n_atoms_rotate(0),  select_rotated_(select_rotated_atoms), logger("PairwiseCrmsd") {}
  
  /** @brief Creates an empty protocol instance.
   *
   * Structures to be processed should be added with <code>add_input_coordinates()</code> method, which makes
   * a deep copy of the atoms selected by <code>select_atoms</code>
   * @param select_superimposed_atoms - selector that defines the atoms to be used both for superimposition and to calculate crmsd;
   *    use <code>nullptr</code> or a pointer to a <code>SelectAllAtoms</code> instance to select all the atoms of each input structure
   */
  PairwiseCrmsd(const selectors::AtomSelector_SP select_superimposed_atoms = nullptr) :
      PairwiseStructureComparison(select_superimposed_atoms), n_atoms_rotate(0), logger("PairwiseCrmsd") {}

  /** @brief Prepares crmsd calculations for a set of structures
   *
   * crmsd will be computed for all decoys in the given set, either all-vs-all or all-vs-reference,
   * depending on which <code>calculate()</code> method variant is used. All the decoys must have the same number of atoms.
   * All the atoms are used in calculations.
   * @param decoys - a vector of structures (sets of atoms) to be used both for superimposition and to calculate crmsd
   * @param select_atoms - selector that defines the atoms to be used in the crmsd calculations;
   *    use <code>nullptr</code> or a pointer to a <code>SelectAllAtoms</code> instance to select all the atoms of each input structure
   * @param decoy_ids - strings to identify each of the structures (used for output). If this vector is empty (which is the default case),
   * order numbers will be used for this purpose
   */
  PairwiseCrmsd(std::vector<Structure_SP> & decoys, const selectors::AtomSelector_SP select_atoms = nullptr,
                const std::vector<std::string> & decoy_ids = std::vector<std::string>()) :
    PairwiseStructureComparison(decoys,select_atoms,decoy_ids), n_atoms_rotate(0), logger("PairwiseCrmsd") {}

  /** @brief Prepares crmsd calculations for a vector of structures (<code>Structure_SP</code> objects)
   *
   * crmsd will be computed for all structures in the given set, either all-vs-all or all-vs-reference,
   * depending on which <code>calculate()</code> method variant is used. The crmsd wil be evaluated only on the atoms that match a given selector,
   * e.g. use core::data::structural::IsBB to evaluate crmsd on all backbone atoms or core::data::structural::IsCA to calculate C-alpha only crmsd
   * All the decoys must have the same number of selected atoms.
   * Only the selected atoms are used in calculations and the rototranslation transformation will be established based on these atoms.
   * Only the atoms selected by <code>select_rotated_atoms</code> will be transformed (rotated and translated)
   * and written to the output stream
   *
   * <strong>Note</strong>, that the two selectors: the one for crmsd calculations and the other for transformation are
   * totally unrelated. The transformation is established solely on the first set of atoms and only the second set of atoms
   * is actually transformed. The two atom sets may be disjoint.
   *
   * @param decoys - a vector of structures to be used in calculations
   * @param select_superimpose_atoms - selector that defines the atoms to be used in crmsd calculations
   * @param select_rotated_atoms - defines atoms that will be transformed (rotated and translated) other atoms will not show up in output
   * @param decoy_ids - strings to identify each of the structures (used for output). If this vector is empty (which is the default case),
   * order numbers will be used for this purpose
   */
  PairwiseCrmsd(const std::vector<Structure_SP> & decoys, const selectors::AtomSelector_SP select_superimpose_atoms = nullptr,
      const selectors::AtomSelector_SP select_rotated_atoms = nullptr, const std::vector<std::string> & decoy_ids =
          std::vector<std::string>());

  /// Default virtual destructor
  virtual ~PairwiseCrmsd() = default;

  /** @brief Creates a deep copy of coordinates and stores them inside this object for future crmsd calculations
   *
   * Both <code>select_superimpose_atoms</code> and <code>select_rotated_atoms</code> selectors are applied,
   * if defined.
   * @param strctr - input structure; all its atoms are copied
   * @param decoy_id - a string to identify the structure; if empty, the insertion index will be used
   * @return the index at which the given set of coordinates is stored
   */
  virtual core::index2 add_input_structure(const Structure_SP strctr, const std::string &decoy_id = "");

  /** @brief Calculates crmsd for every pair of structures and writes the results to a stream.
   *
   * The output stream for the crmsd results may be defined by calling <code>set_outstream()</code> method
   */
  virtual void calculate();

  /** @brief Calculates crmsd between the given reference and every structure stored in this object.
   *
   * This method also performs structural superimpositions and writes the transformed structures into <code>pdb_output</code> stream
   * (if it is not null). The output stream for the crmsd results may be defined by calling <code>set_outstream()</code> method
   * @param reference - a pointer to the reference coordinates
   * @param pdb_output - stream where transformed (rotated & translated) atoms will be written); it may be null - nothing
   *    is written in that case
   */
  virtual void calculate(const Structure_SP reference, std::shared_ptr<std::ostream> pdb_output = nullptr);

protected:
  core::index4 n_atoms_rotate;
  selectors::AtomSelector_SP select_rotated_ = nullptr;
  std::vector<core::data::basic::Coordinates_SP> xyz_posttransform;

private:
  utils::Logger logger;
};

/**
 * \example ap_PairwiseCrmsd.cc
 */

}
}

#endif
