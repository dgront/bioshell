#include <memory>
#include <iostream>
#include <iomanip>

#include <core/data/basic/Vec3.hh>
#include <core/data/io/load_decoys.hh>
#include <core/data/basic/Array2D.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Residue.hh>
#include <core/data/structural/Chain.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/calc/structural/transformations/TMScore.hh>
#include <core/calc/structural/transformations/Rototranslation.hh>
#include <core/protocols/PairwiseTMScore.hh>

#include <utils/Logger.hh>
#include <utils/string_utils.hh>

using core::data::basic::Vec3;

namespace core {
namespace protocols {

using namespace core::data::structural;

const std::shared_ptr<core::data::structural::selectors::IsCA> PairwiseTMScore::ca_only =
    std::make_shared<core::data::structural::selectors::IsCA>();

void PairwiseTMScore::calculate() {

  if (xyz_matching.size() < 2)
    logger << utils::LogLevel::WARNING << "At least two models must be given for pairwise tm-score\n";

  *out_stream << "# query tmplt tm-score crmsd n_res\n";
  // ---------- Prepare tm-score calculator
  the_query.resize(n_atoms_matching);
  the_tmplt.resize(n_atoms_matching);
  core::calc::structural::transformations::TMScore tms(the_query,the_tmplt);
  for (core::index4 i = 1; i < xyz_matching.size(); ++i) {
    for (core::index2 k = 0; k < n_atoms_matching; ++k)
      the_query[k].set((*(xyz_matching[i]))[k]);
    for (core::index4 j = 0; j < i; ++j) {
      for (core::index2 k = 0; k < n_atoms_matching; ++k)
        the_tmplt[k].set((*(xyz_matching[j]))[k]);
      double val = (ref_length==0) ? tms.tmscore() : tms.tmscore(ref_length);
      if (out_matrix_ != nullptr) {
        out_matrix_->insert(i, j, val);
        out_matrix_->insert(j, i, val);
      } else *out_stream << tags_[i] << " " << tags_[j] << " " << val << " " << tms.recent_crmsd() << " "
                    << tms.recent_matched_length() << "\n";
    }
  }
}

/**
 * \todo_code Implement I/O for this method, as it has been done for PairwiseCrmsd
 */
void PairwiseTMScore::calculate(const Structure_SP reference, std::shared_ptr<std::ostream> pdb_output) {

  // ---------- Prepare tm-score calculator
  the_query.resize(n_atoms_matching);
  the_tmplt.resize(n_atoms_matching);
  core::calc::structural::transformations::TMScore tms(the_query,the_tmplt);

  structure_to_coordinates(reference, the_tmplt, ca_only);

  *out_stream << "# query tm-score crmsd n_res\n";

  for (core::index4 i = 0; i < xyz_matching.size(); ++i) {
    for (core::index2 k = 0; k < n_atoms_matching; ++k)
      the_query[k].set((*(xyz_matching[i]))[k]);
    double val = (ref_length==0) ? tms.tmscore() : tms.tmscore(ref_length);
    *out_stream << tags_[i] << " " << val << " " << tms.recent_crmsd() << " "<<tms.recent_matched_length()<<"\n";
  }
}

}
}

