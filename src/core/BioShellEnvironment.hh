#ifndef CORE_BioShellEnvironment_HH
#define CORE_BioShellEnvironment_HH

#include <string>
#include <fstream>

#include <utils/Logger.hh>

namespace core {

/** @brief Returns a shell variable value
 *
 * @param key - the variable name, e.g. BIOSHELL_DATA_DIR
 * @return the variable's value
 */
const std::string get_env_var(const std::string & key);

/** @brief defines global constants and environment for BioShell's programs
 */
class BioShellEnvironment {
public:

  /**@brief Returns the directory where the BioShell keeps his stuff
   *
   * The path should be defined by the -in:database command line option
   * @return the path to BioShell's parameters
   */
  static const std::string & bioshell_db_path();

  /**@brief Sets the new value of the directory name where the BioShell keeps his stuff
   *@param new_path - the new location
   */
  static void bioshell_db_path(const std::string & new_path);

  static void set_db_path_from_environment();
    
  /**@brief Tries to locate a file from BioShell database; local directory is also tested.
   *
   * @param fname - name of the file to be opened
   * @return file name with a properly completed path
   */
  static const std::string from_file_or_db(const std::string & fname);

  /** @brief Looks for a file in BioShell database; local directory is also tested.
   *
   * @param fname - name of the file to be opened
   * @param db_location - relative path within the BioShell database where the file should be located
   * @return file name with a properly completed path
   */
  static const std::string from_file_or_db(const std::string & fname, const std::string & db_location);

private:
  static utils::Logger logger;
  /// path pointing to the main directory with BioShell database
  static std::string bioshell_db_path_;
  static bool db_path_env_tested_;
};

}

#endif
