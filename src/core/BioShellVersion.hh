#ifndef CORE_BioShellVersion_HH
#define CORE_BioShellVersion_HH

#include <string>

#include <utils/Logger.hh>

namespace core {

/** @brief Holds information about the version of this code.
 *
 * The values are assigned automatically by <code>cmake<code>
 */
struct BioShellVersion {
  static const std::string GIT_HASH; ///< Hash of this commit
  static const std::string GIT_TIMESTAMP; ///<Time stamp of this commit
  static const std::string GIT_BRANCH; ///< name of this branch
  static const bool IS_DEBUG_BUILD; ///< True if this is debug build

  std::string to_string();
};

/** @brief Prints the version onfo nicely
 *
 * @param ostream - output stream
 * @param ver - a version information
 * @return reference to the output stream
 */
std::ostream & operator<<(std::ostream & ostream, const BioShellVersion & ver);

}

#endif
