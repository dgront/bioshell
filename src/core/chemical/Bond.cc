#include <iostream>
#include <core/chemical/Bond.hh>

namespace core {
namespace chemical {

std::ostream & operator<<(std::ostream & out, const PdbBond & b) {

  out << *b.first_atom() << " :" << bond_names[b.type()] << ": " << *b.second_atom();

  return out;
}

std::ostream & operator<<(std::ostream & out, const PdbBond_SP b) {

  out << *b->first_atom() << " :" << bond_names[b->type()] << ": " << *b->second_atom();

  return out;
}

}
}
