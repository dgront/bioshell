#include <core/chemical/PKaValues.hh>

namespace core {
namespace chemical {

const PKaValues PKaValues::EMBOSS({8.6, 3.6, 8.5, 3.9, 4.1, 6.5, 10.8, 12.5, 10.1});

const PKaValues PKaValues::SOLOMON({9.6, 2.4, 8.3, 3.9, 4.3, 6.0, 10.5, 12.5, 10.1});

const PKaValues PKaValues::SILLERO({8.2, 3.2, 9.0, 4.0, 4.5, 6.4, 10.4, 12.0, 10.0});

double PKaValues::pKa(const core::chemical::Monomer & m) const {
  switch (m.id) {
    case 4:
      return pKa_CYS();
    case 18:
      return pKa_TYR();
    case 1:
      return pKa_ARG();
    case 11:
      return pKa_LYS();
    case 8:
      return pKa_HIS();
    case 6:
      return pKa_GLU();
    case 3:
      return pKa_ASP();
    default: return 0;
  };
}

}
}
