#ifndef CORE_CHEMICAL_Molecule_HH
#define CORE_CHEMICAL_Molecule_HH

#include <core/chemical/BondType.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/algorithms/SimpleGraph.hh>
#include <core/algorithms/GraphWithData.hh>

namespace core {
namespace chemical {

using namespace core::algorithms;

/** \brief A molecule is a graph of atoms connected with bonds.
 *
 * <h3>Builds a toluene molecule (with no hydrogens, just to make this example short) or loads a PDB file and makes
 * a molecule from a Structure object:</h3>
 * \include ex_Molecule.cc
 * This is an example of PDB molecule, i.e. a molecule where PdbAtom objects are used as atoms of a molecule.
 *
 * <h3>Builds a toluene molecule using Vec3 instances as atoms of the molecule:</h3>
 * \include ex_Molecule_Vec3.cc
 *
 * @see SimpleGraph< V, E >
 * @see createMolecule() and the example therein
 * @see molecule_utils.hh
 */
template <typename A>
class Molecule : protected GraphWithData<SimpleGraph, A, BondType> {
public:

  /** @brief The name of this molecule (empty by default)
   *
   * Molecule name is required by MOL2 file format from TRIPOS
   */
  std::string molecule_name;

  /// Default constructor creates an empty molecule
  Molecule() = default;

  /** \brief Returns the number of atoms in this molecule
   * @return the number of atoms
   */
  inline core::index4 count_atoms() const { return GraphWithData<SimpleGraph, A, BondType>::count_vertices(); }

  /** \brief Adds a new atom to this molecule.
   *
   * @param a - points to the atom to be inserted
   */
  inline core::index4 add_atom(A & a) { return GraphWithData<SimpleGraph, A, BondType>::add_vertex(a); }

  /** \brief Returns true if a given atom belongs to this molecule.
   * @param a - an atom
   * @return true if the given atom belongs to this molecule
   */
  inline bool contains_atom(const A & a) const { return GraphWithData<SimpleGraph, A, BondType>::has_vertex(a); }

  /** \brief Binds two atoms with a bond.
   *
   * The method will add a bond between the two given atoms.
   * If any of the two atoms hasn't belong to this molecule yet, it is inserted before
   * creating a bond.
   *
   * @param first_atom - the first of the two bonded atoms
   * @param second_atom - the second of the two bonded atoms
   * @param bond_type - a bond type
   * @see BondType for allowed bond types
   */
  inline void bind_atoms(A & first_atom, A & second_atom, BondType bond_type) {

    GraphWithData<SimpleGraph, A, BondType>::add_edge(first_atom, second_atom, bond_type);
  }

  /** \brief Binds two atoms with a bond.
   *
   * The method will add a bond between the two given atoms.
   *
   * @param first_atom_index - the index of the first of two bonded atoms
   * @param second_atom_index - the index of the second of the two bonded atoms
   * @param bond_type - a bond type
   * @see BondType for allowed bond types
   */
  inline void bind_atoms(const core::index4 first_atom_index, const core::index4 second_atom_index, BondType bond_type) {

    GraphWithData<SimpleGraph, A, BondType>::add_edge(first_atom_index,second_atom_index,bond_type);
  }

  /** @brief Removes a bond between two given atoms
   *
   * @param firstAtom - first of the two presumably bonded atoms
   * @param secondAtom - second of the two presumably bonded atoms
   * @return true if the bond was actually removed, false in the other case (which is most likely the atoms were not bonded)
   */
  bool remove_bond(const A & firstAtom,const A & secondAtom) { return GraphWithData<SimpleGraph, A, BondType>::remove_edge(firstAtom,secondAtom); }

  /** \brief Returns an iterator pointing to the first atom in this molecule
   * @return an iterator to the first atom
   */
  typename std::vector<A>::iterator begin_atom() { return GraphWithData<SimpleGraph, A, BondType>::begin(); }

  /** \brief Returns an iterator referring to the past-the-end atom of this molecule
   * @return an iterator pointing behind the last atom
   */
  typename std::vector<A>::iterator end_atom() { return GraphWithData<SimpleGraph, A,BondType>::end(); }

  /** \brief Returns a const-iterator pointing to the first atom in this molecule
 * @return an iterator to the first atom
 */
  typename std::vector<A>::const_iterator cbegin_atom() const { return GraphWithData<SimpleGraph, A, BondType>::cbegin(); }

  /** \brief Returns a const-iterator referring to the past-the-end atom of this molecule
   * @return an iterator pointing behind the last atom
   */
  typename std::vector<A>::const_iterator cend_atom() const { return GraphWithData<SimpleGraph, A, BondType>::cend(); }

  /** \brief Returns an iterator pointing to the first neighbor of a given atom in this molecule
   * @return an iterator to the first neighbor atom
   */
  core::algorithms::GivenOrderIterator<A,SimpleGraph::iterator> begin_atom(const A & a) { return GraphWithData<SimpleGraph, A, BondType >::begin(a); }

  /** \brief Returns an iterator referring to the past-the-end neighbor of the given atom
   * @return an iterator pointing behind the last neighbor (bonded atom) of a given atom
   */
  core::algorithms::GivenOrderIterator<A,SimpleGraph::iterator> end_atom(const A & a) { return GraphWithData<SimpleGraph, A, BondType>::end(a); }

  /** \brief Returns a const-iterator pointing to the first atom in this molecule
   * @return an iterator to the first atom
   */
  core::algorithms::GivenOrderConstIterator<A,SimpleGraph::const_iterator> cbegin_atom(const A & a) const { return GraphWithData<SimpleGraph, A, BondType>::cbegin(a); }

  /** \brief Returns a const-iterator referring to the past-the-end atom of this molecule
   * @return an iterator pointing behind the last atom
   */
  core::algorithms::GivenOrderConstIterator<A,SimpleGraph::const_iterator> cend_atom(const A & a) const { return GraphWithData<SimpleGraph, A, BondType>::cend(a); }

  /** \brief Returns the number of vertices of this graph.
  * @return  the number of vertices of this graph.
  */
  index4 count_vertices() const { return GraphWithData<SimpleGraph, A, BondType>::count_vertices(); }

  /** \brief Returns true if the two atoms are connected with a bond
   *
   * @param a1 - the first atom
   * @param a2 - the second atom
   * @return true if the two atoms are connected with a bond, false otherwise
   */
  bool are_bonded(const A & a1, const A & a2) const {
    return GraphWithData<SimpleGraph, A, BondType >::are_connected(a1,a2);
  }

  /** \brief Returns true if the two atoms are connected with a bond
   *
   * @param i - index of the first atom
   * @param j - index of the second atom
   * @return true if the two atoms are connected with a bond, false otherwise
   */
  bool are_bonded(const core::index4 i,const core::index4 j) const {
    return SimpleGraph::are_connected(i,j);
  }

  /** \brief Returns true if the two atoms are connected with a bond
   *
   * Alias to are_bonded(const core::index4,const core::index4) to comply with SimpleGraph interface
   * @param i - index of the first atom
   * @param j - index of the second atom
   * @return true if the two atoms are connected with a bond, false otherwise
   */
  bool are_connected(const core::index4 vi, const core::index4 vj) const { return are_bonded(vi, vj); }

  /** \brief Returns the number of bonds in this molecule
   * @return the number of bonds
   */
  inline core::index4 count_bonds() const { return GraphWithData<SimpleGraph, A, BondType>::count_edges(); }

  /** \brief Returns the number of bonds connected to the given atom in this molecule
   * @return the number of bonds
   */
  inline core::index4 count_bonds(const A & a) const {
    int vi = GraphWithData<SimpleGraph, A, BondType>::index(a);
    if (vi < 0) return 0;
    return GraphWithData < SimpleGraph, A, BondType>::count_edges(a);
  }

  /** \brief Returns the number of edges attached to a given vertex (alias to count_bonds(A&) )
   * @return  the number of edges attached to a given vertex of this graph.
   */
  inline index4 count_edges(const A & a) const { return count_bonds(a); }

  /** \brief Returns the total number of edges of this graph  (alias to count_bonds() ).
   * @return  the total number of edges of this graph.
   */
  inline index4 count_edges() const { return count_bonds(); }

  /** \brief Returns a bond between two atoms.
   *
   * <p>If the two atoms are not covalently bonded in this molecule, an exception is thrown</p>
   *
   * @param a1 - the first atom that is supposed to be bonded to the second one
   * @param a2 - the second atom that is supposed to be bonded to the first one
   * @return a bond between two given atoms or throws an exception
   */
  BondType get_bond(const A & a1, const A & a2) {

    return (GraphWithData<SimpleGraph, A, BondType>::edge(a1, a2));
  }

  /// Provides begin const-iterator for bonds of this molecule
  typename std::map<std::pair<index4, index4>,BondType>::const_iterator cbegin_bonds() const {
    return GraphWithData<SimpleGraph, A, BondType>::cbegin_edges();
  }

  /// Provides end const-iterator for bonds of this molecule
  typename std::map<std::pair<index4, index4>, BondType>::const_iterator cend_bonds() const {
    return GraphWithData<SimpleGraph, A, BondType>::cend_edges();
  }

  /** @brief Returns an atom referred by an index.
   *
   * The indexing starts from 0 and must be consistent with the indexes returned by <code>add_atom()</code> method
   */
  const A & get_atom(const core::index4 i) const { return GraphWithData<SimpleGraph, A, BondType >::vertex(i); }

  /** @brief Returns an atom referred by an index.
   *
   * The indexing starts from 0 and must be consistent with the indexes returned by <code>add_atom()</code> method
   */
  A & get_atom(const core::index4 i) { return GraphWithData<SimpleGraph, A, BondType >::vertex(i); }

  /** @brief Return a vector of atoms connected to a given atoms
   * @param vector - a given atoms
   * @return list of connected atoms
   */
  const std::vector<index4> & get_adjacent(const index4 atoms) const { return GraphWithData<SimpleGraph, A, BondType >::get_adjacent(atoms); }

  /** @brief Return a flag assigned to a vertex identified by its internal index.
   * By default all flags are 0, but new values may be defined by @code vertex_flag(const index4, const index1)@endcode method
   * @param vertex - index of a vertex
   * @return flag assigned to a vertex
   */
  index1 vertex_flag(const core::index4 vertex) const { return GraphWithData<SimpleGraph, A, BondType >::vertex_flag(vertex); }

  /** @brief Return a flag assigned to a vertex identified by its internal index.
   * By default all flags are 0, but new values may be defined by @code vertex_flag(const index4, const index1)@endcode method
   * @param vertex - index of a vertex
   * @param flag - a the new flag to be assigned to a vertex
   */
  void vertex_flag(const core::index4 vertex, const core::index1 flag) { GraphWithData<SimpleGraph, A, BondType >::vertex_flag(vertex, flag); }

  /** \brief Alias to begin_atom().
   * Redefined to comply with the protected base class interface
   * @return an iterator to the first atom
   */
  typename std::vector<A>::iterator begin() { return begin_atom(); }

  /** \brief Alias to end_atom().
   * Redefined to comply with the protected base class interface
   * @return an iterator to the first atom
   */
  typename std::vector<A>::iterator end() { return end_atom(); }

  /** \brief Alias to begin_atom(A &a).
   * @return an iterator to the first neighbor atom
   */
  core::algorithms::GivenOrderIterator<A,SimpleGraph::iterator> begin(const A & a) { return begin_atom(a); }

  /** \brief Alias to end_atom(A &a).
   * @return an iterator pointing behind the last neighbor (bonded atom) of a given atom
   */
  core::algorithms::GivenOrderIterator<A,SimpleGraph::iterator> end(const A & a) { return end_atom(a); }

  /** \brief Alias to add_atom()
   *
   * @param a - points to the atom to be inserted
   */
  inline core::index4 add_vertex(A & a) { return add_atom(a); }

  /** \brief Alias to get_bond()
   *
   * @param a1 - the first atom that is supposed to be bonded to the second one
   * @param a2 - the second atom that is supposed to be bonded to the first one
   * @return a bond between two given atoms or throws an exception
   */
  inline BondType edge(const A & a1, const A & a2) { return get_bond(a1, a2); }

  /** \brief Alias to bind_atoms()
   *
   * The method will add a bond between the two given atoms.
   *
   * @param first_atom - the index of the first of two bonded atoms
   * @param second_atom - the index of the second of the two bonded atoms
   * @param bond_type - a bond type
   * @see BondType for allowed bond types
   */
  inline void add_edge(A & first_atom, A & second_atom, BondType bond_type) {

    bind_atoms(first_atom,second_atom,bond_type);
  }

  /** \brief Alias to bind_atoms()
   *
   * The method will add a bond between the two given atoms.
   *
   * @param first_atom_index - the index of the first of two bonded atoms
   * @param second_atom_index - the index of the second of the two bonded atoms
   * @param bond_type - a bond type
   * @see BondType for allowed bond types
   */
  inline void add_edge(const core::index4 first_atom_index, const core::index4 second_atom_index, BondType bond_type) {

    bind_atoms(first_atom_index,second_atom_index,bond_type);
  }
};

}
}

#endif
/**
 * \example ex_Molecule.cc
 * \example ex_Molecule_Vec3.cc
 */
