#include <core/algorithms/graph_algorithms.hh>
#include <core/chemical/Molecule.hh>
#include <core/chemical/Molecule.hh>
#include <core/chemical/molecule_utils.hh>

namespace core {
namespace chemical {

using namespace core::algorithms;
using namespace core::data::structural;

static core::index2 hash(const core::index1 & ai,const  core::index1 & aj) { return (ai < aj) ? ai * 16 + aj : aj * 16 + ai; }

std::map<int, double> bonds = {{hash(1,1), 0.74}, // H-H
                                   {hash(1, 6),   1.08}, // C-H
                                   {hash(1, 7),   0.98}, // N-H
                                   {hash(1, 8),   0.94}, // O-H
                                   {hash(1, 15),   1.44}, // H-P
                                   {hash(1, 16),  1.34}, // S-H
                                   {hash(6, 6),   1.56}, // C-C
                                   {hash(6, 7),   1.47}, // C-N
                                   {hash(6, 8),   1.43}, // C-O
                                   {hash(6, 15),   1.47}, // C-P
                                   {hash(6, 16),  1.81}, // C-S
                                   {hash(7, 7),   1.40}, // N-N
                                   {hash(7, 8),   1.36}, // O-N
                                   {hash(7, 16),  1.71}, // S-N
                                   {hash(8, 8),   1.32}, // O-O
                                   {hash(8, 15),   1.63}, // O-P
                                   {hash(8, 16),  1.70}, // O-S
                                   {hash(15, 15),   2.21}, // P-P
                                   {hash(15, 16),   1.86}, // P=S
                                   {hash(16, 16), 2.08}}; // S-S

double bond_length(const core::index1 & ai,const  core::index1 & aj) {

  core::index2 h = hash(ai, aj);
  if (bonds.find(h) == bonds.end()) return 2.3;
  else return bonds[h];
}

double bond_length(const AtomicElement & ai, const AtomicElement & aj) {

  return bond_length(ai.z,aj.z);
}

core::chemical::PdbMolecule_SP structure_to_molecule(const Structure & s, const double tolerance ) {
   
core::chemical::PdbMolecule_SP m = std::static_pointer_cast<PdbMolecule>(create_molecule(s.first_const_atom(),s.last_const_atom(),tolerance));

  return m;
}

std::vector<std::vector<index4>> find_rings(core::chemical::PdbMolecule &mol) {

  return find_cycles<core::chemical::PdbMolecule,PdbAtom_SP,BondType>(mol);
}

std::vector<std::vector<PdbAtom_SP>> find_ring_atoms(core::chemical::PdbMolecule &mol) {

  using namespace core::chemical;

  std::vector<std::vector<index4>> cycles = core::algorithms::find_cycles<PdbMolecule,PdbAtom_SP,BondType>(mol);
  std::vector<std::vector<PdbAtom_SP>> atom_cycles;

  for (const std::vector<index4> &c:cycles) {
    std::vector<PdbAtom_SP> atom_cycle;
    for (index4 ai:c)
      atom_cycle.push_back(mol.get_atom(ai));
    atom_cycles.push_back(atom_cycle);
  }

  return atom_cycles;
}

std::vector<std::pair<index4,index4>> maximal_common_submolecule(const core::chemical::PdbMolecule & mol1,const core::chemical::PdbMolecule & mol2) {

  typedef GraphWithData<SimpleGraph,std::pair<index4,index4>,index1> GraphType;

  // ---------- Here we create a modular product of the two molecular graphs
  GraphType pg;
  // ---------- First we create vertices
  for(index4 i=0;i<mol1.count_vertices();++i) {
    PdbAtom_SP a1 = mol1.get_atom(i);
    for (index4 j = 0; j < mol2.count_vertices(); ++j) {
      PdbAtom_SP a2 = mol2.get_atom(j);
      if (a1->element_index() != a2->element_index()) continue;
      if (mol1.count_bonds(a1) != mol2.count_bonds(a2)) continue;
      pg.add_vertex(std::make_pair(i,j));
    }
  }

  for (index4 i = 1; i < pg.count_vertices(); ++i) {
    std::pair<index4, index4> v_i = pg.vertex(i);
    for (index4 j = 0; j < i; ++j) {
      std::pair<index4, index4> v_j = pg.vertex(j);
      if ((v_i.first == v_j.first) || (v_i.second == v_j.second)) continue;
      if(mol1.are_connected(v_i.first,v_j.first) && mol2.are_connected(v_i.second,v_j.second))
        pg.add_edge(i,j,1);
      if((!mol1.are_connected(v_i.first,v_j.first)) && (!mol2.are_connected(v_i.second,v_j.second)))
        pg.add_edge(i,j,2);
//      if(pg.SimpleGraph::are_connected(i,j))
//        std::cerr << "MU: ("<<i<<","<<j<<") " << v_i.first << " " << v_j.first << " : " << v_i.second << " " << v_j.second << " "
//                  << int(pg.edge(i, j)) << "\n";

    }
  }

  std::vector<index4> max_clique;
  for(core::index4 i=0;i<pg.count_vertices();++i) {
    std::vector<index4> clique = core::algorithms::find_clique<GraphType, std::pair<index4, index4>, index1>(3, pg, i);
    if(clique.size()>max_clique.size()) clique.swap(max_clique);
  }
  std::vector<std::pair<index4,index4>> pairs;
  for (index4 i:max_clique) {
    std::pair<index4, index4> &p = pg.vertex(i);
//    std::cerr << p.first<<" "<<p.second<<"\n";
    pairs.push_back(p);
  }

  return pairs;
}

std::vector<core::chemical::PdbMolecule_SP> split_molecules(core::chemical::PdbMolecule &mol,
                                                            const core::index2 min_size) {
//  typedef GraphWithData<SimpleGraph,PdbAtom_SP,BondType> GraphType;
  auto mols = core::algorithms::connected_components<PdbMolecule,PdbAtom_SP,BondType>(mol,min_size);

  return mols;
}

}
}

