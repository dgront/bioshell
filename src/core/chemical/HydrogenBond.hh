/** @file HydrogenBond.hh
 * @brief Provides HydrogenBond and PdbHydrogenBond types
 */
#ifndef CORE_CHEMICAL_HydrogenBond_HH
#define CORE_CHEMICAL_HydrogenBond_HH

#include <core/chemical/Bond.hh>
#include <core/data/structural/PdbAtom.hh>

namespace core {
namespace chemical {

/** \brief Represents a hydrogen bond between two atoms
 *
 * @param first_atom - the first atom object
 * @param second_atom - the second atom object
 * @param type - the type of this bond
 */
class HydrogenBond : public PdbBond {
public:
  /// Returns a pointer to the hydrogen donor atom, e.g amide hydrogen
  const core::data::structural::PdbAtom_SP first_atom() const { return first_atom_; }
  /// Returns a pointer to the hydrogen acceptor atom, e.g. carbonyl oxygen
  const core::data::structural::PdbAtom_SP second_atom() const { return second_atom_; }
  /// Returns a pointer to the hydrogen donor atom, e.g amide hydrogen
  const core::data::structural::PdbAtom_SP donor_atom() const { return first_atom_; }
  /// Returns a pointer to the hydrogen acceptor atom, e.g. carbonyl oxygen
  const core::data::structural::PdbAtom_SP acceptor_atom() const { return second_atom_; }
  core::chemical::BondType type() const { return type_; }

  /** @brief Creates a hydrogen bond object.
   *
   * Note, that this constructor requires the two heavy atoms: donor and acceptor,
   * but it actually  doesn't need the hydrogen atom
   * @param donor_atom - hydrogen donor atom, such as an amide N or ahydroxyl O
   * @param acceptor_atom - acceptor atom, such as a carbonyl O
   */
  HydrogenBond(core::data::structural::PdbAtom_SP donor_atom, core::data::structural::PdbAtom_SP acceptor_atom ) :
    PdbBond(donor_atom, acceptor_atom, BondType::HYDROGEN) {}
};

}
}

#endif
