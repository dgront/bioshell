/** \file ChiAnglesDefinition.hh
 * @brief Defines atoms involved in each Chi angle in the standard 20 amino acids
 */
#ifndef CORE_CHEMICAL_ChiAnglesDefinition_HH
#define CORE_CHEMICAL_ChiAnglesDefinition_HH

#include <string>
#include <map>
#include <vector>

#include <core/chemical/Monomer.hh>

namespace core {
namespace chemical {

/** @brief Provides a list of atoms involved in each of the \f$\chi\f$ angles for every amino acid type.
 *
 * The following example shows, how to look up definitions of Chi angles in TRP and ARG:
 * \include ex_ChiAnglesDefinition.cc
 */
struct ChiAnglesDefinition {

  /** @brief Map holding the atom names relevant for each \f$\chi\f$ dihedral angle.
   */
  static const std::map<std::string, std::vector<std::vector<std::string>>> chi_angles;

  /** @brief Returns names of the atoms involved in a given \f$\chi\f$ dihedral angle.
   * @param aa_name - name of the amino acid where the \f$\chi\f$  is located
   * @param chi_index - index of the  \f$\chi\f$ dihedral <strong>starting from 1</strong>, e.g to get atoms constituting \f$\chi_2\f$ in TRP, call:
   * @code
   * chi_angle_atoms("TRP",2);
   * @endcode
   * @return a vector of atom names
   */
  static const std::vector<std::string> & chi_angle_atoms(const std::string & aa_name, const core::index2 chi_index) { return chi_angles.at(aa_name)[core::index2(chi_index-1)]; }

  /** @brief Returns names of the atoms involved in a given \f$\chi\f$ dihedral angle.
   * @param aa_type - object defining the amino acid where the \f$\chi\f$  is located
   * @param chi_index - index of the  \f$\chi\f$ dihedral <strong>starting from 1</strong>, e.g to get atoms constituting \f$\chi_2\f$ in TRP, call:
   * @code
   * chi_angle_atoms(Monomer::TRP,2);
   * @endcode
   * @return a vector of atom names
   */
  static const std::vector<std::string> & chi_angle_atoms(const Monomer & aa_type, const core::index2 chi_index) { return chi_angles.at(aa_type.code3)[core::index2(chi_index-1)]; }

  /** @brief Returns the number of  \f$\chi\f$ dihedral angles possible for a given amino acid type.
   * @param aa_type - object defining the amino acid where the \f$\chi\f$  is located
   * @return the number of \f$\chi\f$ angles
   */
  static size_t count_chi_angles(const std::string & aa_name) { return chi_angles.at(aa_name).size(); }

  /** @brief Returns the number of  \f$\chi\f$ dihedral angles possible for a given amino acid type.
   * @param aa_type - object defining the amino acid where the \f$\chi\f$  is located
   * @return the number of \f$\chi\f$ angles
   */
  static size_t count_chi_angles(const Monomer & aa_type) {return (chi_angles.find(aa_type.code3)!=chi_angles.end()) ? chi_angles.at(aa_type.code3).size() : 0;}
};

}
}

#endif
/**
 *\example ex_ChiAnglesDefinition.cc
 */
