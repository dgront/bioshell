#ifndef CORE_ALIGNMENT_SCORING_MonomerStructureFactory_H
#define CORE_ALIGNMENT_SCORING_MonomerStructureFactory_H

#include <memory>
#include <iostream>
#include <string>
#include <map>
#include <utils/Logger.hh>

#include <core/chemical/MonomerStructure.hh>

namespace core {
namespace chemical {

/** @brief Loads MonomerStructure objects on request, keeps objects that has been already loaded.
 *
 * This class is a singleton that provides MonomerStructure objects i.e. structural description of each
 * building block (i.e.  a monomer) that can be found in a PDB file.
 * @see MonomerStructure class
 */
class MonomerStructureFactory {
public:

  /** @brief Static method to access the factory singleton.
   * @return a singleton instance of a factory that provides MonomerStructureFactory objects
   */
  static MonomerStructureFactory & get_instance() {
    static MonomerStructureFactory manager;
    return manager;
  }

  /** @brief Returns a pointer to a MonomerStructure object
   *
   * If the name does not refer to any monomer loaded so far, the factory will attempt to load it from a CIF file
   * that is stored in <code>./data</code> directory of a BioShell distribution
   * @param code3 - three-letter code of a monomer, e.g. <code>HEM</code> of <code>TRP</code>
   */
  const MonomerStructure_SP get(const std::string & code3);

  /** @brief Returns true if a monomer has been already loaded for a given three-letter code
   * @param code3 - monomer code
   * @return true if it has already been loaded
   */
  bool is_known_monomer(const std::string & code3) const { return monomers_.find(code3)!= monomers_.end(); }

  /** @brief Loads a new monomer from a CIF file
   * @param filename - a file name (.cif format)
   * @return total number of monomers stored in this factory
   */
  core::index4 register_monomer(const std::string & filename);

  /** @brief const-iterator pointing at the very first monomer in this container
   * @return const-iterator to monomers' map
   */
  std::map<std::string, MonomerStructure_SP>::const_iterator cbegin() const { return monomers_.cbegin(); }

  /** @brief const-iterator pointing behind the very first monomer in this container
   * @return const-iterator to monomers' map
   */
  std::map<std::string, MonomerStructure_SP>::const_iterator cend() const { return monomers_.cend(); }

private:
  utils::Logger logs;
  std::map<std::string, MonomerStructure_SP> monomers_;

  MonomerStructureFactory();
};

}
}

#endif

