#ifndef CORE_CHEMICAL_MonomerStructure_HH
#define CORE_CHEMICAL_MonomerStructure_HH

#include <string>
#include <vector>
#include <iostream>
#include <memory>
#include <stdexcept>

#include <core/data/io/Cif.hh>
#include <core/chemical/Monomer.hh>
#include <core/chemical/PdbMolecule.hh>
#include <core/index.hh>
#include <utils/Logger.hh>

namespace core {
namespace chemical {

using namespace data::structural;
using namespace data::io;

class MonomerStructure;
/// MonomerStructure_SP is a shared pointer to a MonomerStructure instance
typedef std::shared_ptr<MonomerStructure> MonomerStructure_SP;

/** @brief Holds structural data for a monomer.
 *
 * This class extends Monomer by adding structural data: atom types, atomic coordinates and bonds information.
 */
class MonomerStructure: public PdbMolecule, public Monomer {
public:
  /** @brief Smallest dihedral angle measured within a ring that still can be classified as an aromatic.
   * The value expressed in radians, always positive.
   * Ideal aromatic ring should be flat, i.e. the MIN_AROMATIC_RING_DIHEDRAL  should = pi
   */
  static constexpr const double MIN_AROMATIC_RING_DIHEDRAL = 160 * M_PI / 180.0;

  MonomerStructure(const Monomer & m, PdbMolecule & mol);

  /** @brief Constructor initializes this object from a CIF file
   *
   * @param cif_filename name of file with structure data
   */
  MonomerStructure(const CifSection_SP atoms_section,const CifSection_SP bonds_section,const char code1,const char* code3,
    const unsigned char n_atoms,const unsigned char n_heavy_atoms,const bool ambig_flag,const double charge);

  /** @brief Reads a monomer structure from a CIF file
   * @param file_name - name of the input CIF file
   * @return a pointer to a newly created MonomerStructure object
   */
  static MonomerStructure_SP from_cif(const std::string& file_name);

  /** @brief Reads a monomer structure from a PDB file.
   *
   * The PDB file must define bonds with CONNECT lines. Use two identical CONNECT entries to define a double bond,
   * three for a TRIPLE bond
   * @param file_name - name of the input PDB file
   * @return a pointer to a newly created MonomerStructure object
   * @see PdbMolecule::from_pdb() method is used to read a molecule data from a PDB file
   */
  static MonomerStructure_SP from_pdb(const std::string& file_name);

  /// Read-only vector of polar hydrogen atoms of this Monomer
  const std::vector<PdbAtom_SP> & polar_hydrogens() const { return polar_hydrogens_; }
  /// Read-only vector of non-polar hydrogen atoms of this Monomer
  const std::vector<PdbAtom_SP> & nonpolar_hydrogens() const { return nonpolar_hydrogens_; }
  /// Read-only vector of non-polar heavy atoms of this Monomer
  const std::vector<PdbAtom_SP> & nonpolar_heavy() const { return nonpolar_heavy_; }
  /// Read-only vector of hydrogen donors of this Monomer
  const std::vector<PdbAtom_SP> & hydrogen_donors() const { return hydrogen_donors_; }
  /// Read-only vector of hydrogen acceptors of this Monomer
  const std::vector<PdbAtom_SP> & hydrogen_acceptors() const { return hydrogen_acceptors_; }
  /// Read-only vector of rings of this Monomer
  const std::vector<std::vector<PdbAtom_SP>> rings() const { return rings_; }
  /// Read-only vector of aromatic atoms of this Monomer
  const std::vector<PdbAtom_SP> aromatic_atoms() const { return aromatic_atoms_; }

private:
  utils::Logger logs;
  std::vector<PdbAtom_SP> polar_hydrogens_;
  std::vector<PdbAtom_SP> nonpolar_hydrogens_;
  std::vector<PdbAtom_SP> nonpolar_heavy_;
  std::vector<PdbAtom_SP> hydrogen_donors_;
  std::vector<PdbAtom_SP> hydrogen_acceptors_;
  std::vector<PdbAtom_SP> aromatic_atoms_;
  std::vector<std::vector<PdbAtom_SP>> rings_;

  MonomerStructure(const char code1, const char *code3, const unsigned char n_atoms, const unsigned char n_heavy_atoms,
                   const bool ambig_flag, const double charge);
  void assign_atoms();
};

}
}

#endif

/**
 */
