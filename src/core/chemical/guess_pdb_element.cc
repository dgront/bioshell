#include <vector>

#include <core/chemical/guess_pdb_element.hh>
#include <regex>

namespace core {
namespace chemical {

std::vector<std::string> pattern_strings = {" B.{2}", "BR.{2}", "BE.{2}", "BA.{2}", "AG.{2}", "AL.{2}", "AR.{2}", "AU.{2}",
                                 "AS.{2}", "( C  )|( C\\w )|( C\\w\\d)", "CA.{2}", "CD.{2}", "CE.{2}", "CL.{2}",
                                 "CO.{2}", "CR.{2}", "CS.{2}", "CU.{2}", "( D|DD).{2}", "EU.{2}", " F.{2}", "FE.{2}",
                                 "GA.{2}", "GD.{2}", "(( |\\d)(H|H[^FGO]).{2})|(H[DEG]\\d{2})", "HF.{2}", "HG.{2}",
                                 "HO.{2}", " I.{2}", "IN.{2}", "IR.{2}", " K.{2}", "KR.{2}", "LA.{2}", "LI.{2}",
                                 "LU.{2}", "MG.{2}", "MN.{2}", "MO.{2}", " N.{2}", "NA.{2}", "NI.{2}", "( O|OE).{2}",
                                 "OS.{2}", " P.{2}", "PB.{2}", "PD.{2}", "PR.{2}", "PT.{2}", "RB.{2}", "RE.{2}",
                                 "RH.{2}", "RU.{2}", " S.{2}", "SB.{2}", "SE.{2}", "SI.{2}", "SM.{2}", "SR.{2}",
                                 "TA.{2}", "TB.{2}", "TE.{2}", "TL.{2}", " U.{2}", " V.{2}", " W.{2}", " Y.{2}",
                                 "XE.{2}", "(UNK.{1}| UNK)", "YB.{2}", "ZN.{2}", "ZR.{2}", "HE.{2}", "NE.{2}",
                                 "KR.{2}"};

std::vector<AtomicElement> elements = {AtomicElement::BORON, AtomicElement::BROMINE, AtomicElement::BERYLLIUM,
                            AtomicElement::BARIUM, AtomicElement::SILVER, AtomicElement::ALUMINIUM,
                            AtomicElement::ARGON, AtomicElement::GOLD, AtomicElement::ARSENIC, AtomicElement::CARBON,
                            AtomicElement::CALCIUM, AtomicElement::CADMIUM, AtomicElement::CERIUM,
                            AtomicElement::CHLORINE, AtomicElement::COBALT, AtomicElement::CHROMIUM,
                            AtomicElement::CAESIUM, AtomicElement::COPPER, AtomicElement::DEUTERIUM,
                            AtomicElement::EUROPIUM, AtomicElement::FLUORINE, AtomicElement::IRON,
                            AtomicElement::GALLIUM, AtomicElement::GADOLINIUM, AtomicElement::HYDROGEN,
                            AtomicElement::HAFNIUM, AtomicElement::MERCURY, AtomicElement::HOLMIUM,
                            AtomicElement::IODINE, AtomicElement::INDIUM, AtomicElement::IRIDIUM,
                            AtomicElement::POTASSIUM, AtomicElement::KRYPTON, AtomicElement::LANTHANUM,
                            AtomicElement::LITHIUM, AtomicElement::LUTETIUM, AtomicElement::MAGNESIUM,
                            AtomicElement::MANGANESE, AtomicElement::MOLYBDENUM, AtomicElement::NITROGEN,
                            AtomicElement::SODIUM, AtomicElement::NICKEL, AtomicElement::OXYGEN, AtomicElement::OSMIUM,
                            AtomicElement::PHOSPHORUS, AtomicElement::LEAD, AtomicElement::PALLADIUM,
                            AtomicElement::PRASEODYMIUM, AtomicElement::PLATINUM, AtomicElement::RUBIDIUM,
                            AtomicElement::RHENIUM, AtomicElement::RHODIUM, AtomicElement::RUTHENIUM,
                            AtomicElement::SULFUR, AtomicElement::ANTIMONY, AtomicElement::SELENIUM,
                            AtomicElement::SILICON, AtomicElement::SAMARIUM, AtomicElement::STRONTIUM,
                            AtomicElement::TANTALUM, AtomicElement::TERBIUM, AtomicElement::TECHNETIUM,
                            AtomicElement::THALLIUM, AtomicElement::URANIUM, AtomicElement::VANADIUM,
                            AtomicElement::TUNGSTEN, AtomicElement::YTTRIUM, AtomicElement::XENON, AtomicElement::DUMMY,
                            AtomicElement::YTTERBIUM, AtomicElement::ZINC, AtomicElement::ZIRCONIUM,
                            AtomicElement::HELIUM, AtomicElement::NEODYMIUM, AtomicElement::KRYPTON};


AtomicElement guess_pdb_element(const std::string &pdb_atom_name) {

  std::smatch a_match;
  for (core::index1 i = 0; i < pattern_strings.size(); ++i) {
    const std::regex re(pattern_strings[i]);
    if (std::regex_match(pdb_atom_name, a_match, re))
      return elements[i];
  }
  return AtomicElement::DUMMY;

}

bool is_hydrogen(const core::data::structural::PdbAtom &atom) {

  if (atom.element_index() == 1)
    return true;
  if (guess_pdb_element(atom.atom_name()).z == 1)
    return true;
  return false;
}


}
}

