#ifndef CORE_CHEMICAL_PdbMolecule_FWD_HH
#define CORE_CHEMICAL_PdbMolecule_FWD_HH

#include <core/chemical/Molecule.hh>
#include <core/data/structural/PdbAtom.hh>

namespace core {
namespace chemical {

/// Molecule that uses PdbAtom_SP type to represent atoms
class PdbMolecule;

/// A shared pointer type to PdbMolecule
typedef std::shared_ptr<PdbMolecule> PdbMolecule_SP;

}
}

#endif
