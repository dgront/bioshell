/** @file AAHydrophobicity.hh
 * @brief provides hydrophobicity data for amino acid residues
 */
#ifndef CORE_CHEMICAL_AAHydrophobicity_HH
#define CORE_CHEMICAL_AAHydrophobicity_HH

#include <vector>
#include <initializer_list>

#include <core/chemical/Monomer.hh>

namespace core {
namespace chemical {

/** \brief Provides a few  amino acid hydrophobicity scales.
 */
class AAHydrophobicity {
public:
  static const AAHydrophobicity KyteDoolittle;///< Kyte-Doolittle scale


  double hydrophobicity(const core::chemical::Monomer &res_type) const {
    return (res_type.parent_id < 21) ? hydrophobicity_values[res_type.parent_id] : 0.0;
  }

private:
  std::vector<double> hydrophobicity_values;

  /** @brief Initialize the object with given values.
   *
   */
  AAHydrophobicity(const std::initializer_list<double> values) : hydrophobicity_values(values) {}
};

}
}

#endif
