#include <core/chemical/MonomerStructureFactory.hh>
#include <core/BioShellEnvironment.hh>
#include <utils/io_utils.hh>

namespace core {
namespace chemical {

const MonomerStructure_SP MonomerStructureFactory::get(const std::string &code3) {

  if (monomers_.find(code3) == monomers_.end()) {
    logs << utils::LogLevel::FILE << "loading " << code3 << ".cif from database\n";
    register_monomer(core::BioShellEnvironment::from_file_or_db("monomers/" + code3 + ".cif"));
  }

  return monomers_[code3];
}


MonomerStructureFactory::MonomerStructureFactory() : logs("MonomerStructureFactory") {

  std::string monomers_path = BioShellEnvironment::bioshell_db_path() + "/monomers/";
  std::vector<std::string> monomers_names = utils::read_listfile(monomers_path + "monomers_list");
  for (auto name: monomers_names) {
    if (name[0] != '#')
      register_monomer(monomers_path + name + ".cif");
  }
}

core::index4 MonomerStructureFactory::register_monomer(const std::string & filename) {

  MonomerStructure_SP m = nullptr;
  std::string ext = utils::root_extension(filename).second;
  if (ext == "cif")
    m = MonomerStructure::from_cif(filename);
  if (ext == "pdb")
    m = MonomerStructure::from_pdb(filename);

  logs << utils::LogLevel::INFO << "Attempting to register the " << m->code3 << " monomer from the " << filename
       << " file\n";
  if (is_known_monomer(m->code3)) {
    logs << utils::LogLevel::WARNING << "Monomer " << m->code3
         << " is already known! Cannot load another monomer with the same code3!\n";
  } else
    monomers_[m->code3] = m;
  return monomers_.size();
}

}
}
