#include <string>
#include <iostream>

#include <core/chemical/BondType.hh>

namespace core {
namespace chemical {

std::string mol2_bond_code_from_type(const BondType type) {

  switch (type) {
  case BondType::DUMMY:
    return "du";
  case BondType::SINGLE:
    return "1";
  case BondType::DOUBLE:
    return "2";
  case BondType::TRIPLE:
    return "3";
  case BondType::AMIDE:
    return "1";
  case BondType::AROMATIC:
    return "ar";
  case BondType::DISULFIDE:
    return "1";
  case BondType::HYDROGEN:
    return "hy";
  case BondType::UNKNOWN:
  default:
    return "un";
  }
}

std::map<BondType, std::string> bond_names = {{BondType::DUMMY,     "DUMMY"},
                                              {BondType::SINGLE,    "SINGLE"},
                                              {BondType::DOUBLE,    "DOUBLE"},
                                              {BondType::TRIPLE,    "TRIPLE"},
                                              {BondType::AMIDE,     "AMIDE"},
                                              {BondType::AROMATIC,  "AROMATIC"},
                                              {BondType::DISULFIDE, "DISULFIDE"},
                                              {BondType::HYDROGEN,  "HYDROGEN"},
                                              {BondType::UNKNOWN,   "UNKNOWN"}};

std::map<std::string,BondType> bond_by_code = {{"d", BondType::DUMMY},
                                               {"du", BondType::DUMMY},
                                               {"1", BondType::SINGLE},
                                               {"2", BondType::DOUBLE},
                                               {"3", BondType::TRIPLE},
                                               {"a", BondType::AROMATIC},
                                               {"ar", BondType::AROMATIC},
                                               {"am", BondType::AMIDE},
                                               {"h", BondType::HYDROGEN},
                                               {"hy", BondType::HYDROGEN},
                                               {"SING", BondType::SINGLE},
                                               {"DOUB", BondType::DOUBLE},
                                               {"TRIP", BondType::TRIPLE},
                                               {"AROM", BondType::AROMATIC}
};

BondType bond_type_from_code(const std::string &code) {

  if (bond_by_code.find(code) != bond_by_code.end()){

    return bond_by_code[code];}
  else {

    return BondType::UNKNOWN;
  }
}

}
}
