/** \file guess_pdb_element.hh
 * @brief Defines a function that can guess an atomic element based on a PDB atom name
 */
#ifndef CORE_CHEMICAL_guess_pdb_element_HH
#define CORE_CHEMICAL_guess_pdb_element_HH

#include <string>
#include <map>

#include <core/chemical/AtomicElement.hh>
#include <core/data/structural/PdbAtom.hh>

namespace core {
namespace chemical {

AtomicElement guess_pdb_element(const std::string & pdb_atom_name);

bool is_hydrogen(const core::data::structural::PdbAtom &atom);

}
}
#endif

