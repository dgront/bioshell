/** \file molecule_utils.hh
 * @brief Utility methods that operates on Molecule objects
 */
#ifndef CORE_CHEMICAL_molecule_utils_HH
#define CORE_CHEMICAL_molecule_utils_HH

#include <memory>
#include <map>
#include <functional>

#include <core/chemical/Molecule.hh>
#include <core/chemical/PdbMolecule.hh>
#include <core/data/structural/PdbAtom.hh>

namespace core {
namespace chemical {

using namespace core::algorithms;
using namespace core::data::structural;

/** @brief Comparator used to sort planar angle definitions.
 *
 * The angles are sorted according to indexes of atoms involved in a given planar, i.e. the first
 * angle will be between atoms 1-2-3, followed by 1-2-5 and so on.
 */
struct ComparePlanarAngles {

  bool operator()(const std::tuple<PdbAtom_SP, PdbAtom_SP, PdbAtom_SP> &p1,
                  const std::tuple<PdbAtom_SP, PdbAtom_SP, PdbAtom_SP> &p2) {
    if (std::get<0>(p1)->id() > std::get<0>(p2)->id()) return false;
    if (std::get<0>(p1)->id() < std::get<0>(p2)->id()) return true;
    if (std::get<1>(p1)->id() > std::get<1>(p2)->id()) return false;
    if (std::get<1>(p1)->id() < std::get<1>(p2)->id()) return true;
    if (std::get<2>(p1)->id() > std::get<2>(p2)->id()) return false;
    return true;
  }
};

/** @brief Comparator used to sort dihedral angle definitions.
 *
 * The angles are sorted according to indexes of atoms involved in a given dihedral, i.e. the first
 * dihedral angle will be between atoms 1-2-3-4, followed by 1-2-3-5 and so on.
 */
struct CompareDihedralAngles {

  /// Compares two dihedral angle definitions
  bool operator()(const std::tuple<PdbAtom_SP, PdbAtom_SP, PdbAtom_SP, PdbAtom_SP> &p1,
                  const std::tuple<PdbAtom_SP, PdbAtom_SP, PdbAtom_SP, PdbAtom_SP> &p2) {
    if (std::get<0>(p1)->id() > std::get<0>(p2)->id()) return false;
    if (std::get<0>(p1)->id() < std::get<0>(p2)->id()) return true;
    if (std::get<1>(p1)->id() > std::get<1>(p2)->id()) return false;
    if (std::get<1>(p1)->id() < std::get<1>(p2)->id()) return true;
    if (std::get<2>(p1)->id() > std::get<2>(p2)->id()) return false;
    if (std::get<2>(p1)->id() < std::get<2>(p2)->id()) return true;
    if (std::get<3>(p1)->id() > std::get<3>(p2)->id()) return false;
    return true;
  }
};

/** \brief Returns all possible planar angles in the molecule.
 *
 * @param mol - a molecule graph object
 * @param output - a vector where the planar angles are stored
 * @tparam A - type used to represent an atom, e.g. PdbAtom_SP
 * In the following example, all planar angles found in a molecule are evaluated and printed:
 * \include ex_Molecule.cc
 * @return an array of all possible planar angles
 */
template <typename A>
core::index2 find_planar_angles(const Molecule <A> &mol, std::vector<std::tuple<A, A, A>> &output) {

  std::map<unsigned long long, std::tuple<A, A, A>> angles;
  for (auto a_it = mol.cbegin_atom(); a_it != mol.cend_atom(); ++a_it) {
    const A atom = *a_it;
    if (mol.count_bonds(atom) < 2) continue;

    unsigned long long hash_middle = (std::hash<A>{}(atom)) << 20;
    for (auto it_i = mol.cbegin_atom(atom); it_i != mol.cend_atom(atom); ++it_i) {
      auto it_j = it_i;
      ++it_j;
      for (; it_j != mol.cend_atom(*a_it); ++it_j) {
        unsigned long long hi = (std::hash<A>{}((*it_i)));
        unsigned long long hj = (std::hash<A>{}((*it_j)));
        unsigned long long hash =  (hi < hj) ? (hi << 40) + hash_middle + hj : (hj << 40) + hash_middle + hi;
        if (angles.find(hash) == angles.end()) {
          angles[hash] = std::tuple<A, A, A>(*it_i, *a_it, *it_j);
        }
      }
    }
  }
  std::for_each(angles.begin(), angles.end(),
                [&output](const typename std::map<unsigned long, std::tuple<A, A, A>>::value_type &p) {
                    output.push_back(p.second);
                });

  return output.size();
}

/** \brief Returns all possible torsion angles in a molecule.
 *
 * @param mol - a molecule graph object
 * @param output - a vector where the dihedral angles are stored
 * @tparam A - type used to represent an atom, e.g. PdbAtom_SP
 * In the following example, all planar and dihedral angles found in a molecule are evaluated and printed:
 * \include ex_Molecule.cc
 * @return an array of all possible dihedral angles
 */
template <typename A>
core::index2 find_torsion_angles(const  Molecule <A> &mol, std::vector<std::tuple<A, A, A, A>> &output) {

  std::map<unsigned long long, std::tuple<A, A, A, A>> angles;
  std::vector<core::index4> ids(4, 0);
 // for (auto a_it = mol.cbegin_atom(); a_it != mol.cend_atom(); ++a_it) {
   for (auto a_it = 0;a_it <mol.count_atoms(); ++a_it) {
   // if (mol.count_bonds(*a_it) < 2) continue;

    for (auto it_i = mol.get_adjacent(a_it).begin(); it_i != mol.get_adjacent(a_it).end(); ++it_i) {
      core::index4 ai = *it_i;
  //    auto it_j = it_i;
  //    ++it_j;
      for (auto it_j = mol.get_adjacent(ai).begin(); it_j != mol.get_adjacent(ai).end(); ++it_j) {
        core::index4 aj = *it_j;
        if (a_it==aj) continue;

        for (auto it_k = mol.get_adjacent(aj).begin(); it_k != mol.get_adjacent(aj).end(); ++it_k) {
          core::index4 ak = *it_k;
            if ((ai == ak) || (ak==a_it)) continue;
          ids[0] = std::hash<A>{}(mol.get_atom(a_it));
          ids[1] = std::hash<A>{}(mol.get_atom(ai));
          ids[2] = std::hash<A>{}(mol.get_atom(aj));
          ids[3] = std::hash<A>{}(mol.get_atom(ak));

          std::sort(ids.begin(), ids.end());
          unsigned long long hash = ((long long) (ids[0] & 0xffff) << 48) + ((long long) (ids[1] & 0xffff) << 32)
                                    + ((ids[2] & 0xffff) << 16) + (ids[3] & 0xffff);
          if (angles.find(hash) == angles.end()) {
            angles.emplace(
                std::pair<unsigned long, std::tuple<A, A, A, A>>(hash, std::tuple<A, A, A, A>(mol.get_atom(a_it), mol.get_atom(ai), mol.get_atom(aj), mol.get_atom(ak))));
          }
        }
      }
    }
  }
  std::for_each(angles.begin(), angles.end(),
                [&output](const typename std::map<unsigned long, std::tuple<A, A, A, A>>::value_type &p) {
                    output.push_back(p.second);
                });

  return output.size();
}

/** @brief Returns all atoms of a molecular fragment that is bonded to a given atom.
 *
 * @param preceding_atom - atom that precedes the molecular fragment. This atom and all other atoms bonded to it will not be reported by this method
 * @param the_atom - the first atom of a molecular fragment to be found
 * @param mol - tme molecule graph
 * @param attached_atoms - a vector where the resulting atoms will be placed
 * @return a reference to a vector of results
 *
 * @include ex_find_side_chains.cc
 */
template <typename A>
std::vector<A> &  find_side_group(const A preceding_atom,const A the_atom, const Molecule<A> & mol, std::vector<A> & attached_atoms) {

  for(auto atom_it=mol.cbegin_atom(the_atom);atom_it!=mol.cend_atom(the_atom);++atom_it) {
    if(*atom_it==preceding_atom) continue; // the case when we go back to the bulk of the molecule
      if(std::find(attached_atoms.begin(),attached_atoms.end(),*atom_it)==attached_atoms.end()) {
      attached_atoms.push_back(*atom_it);
      find_side_group(preceding_atom,*atom_it,mol,attached_atoms);
    }
  }
  return attached_atoms;
}

/** @brief Provide a crude estimation of a bond length between two given atom types.
 * This should be understood as the maximum length where a bond may be observed, e.g. for two
 * carbon atoms this method will return \f$1.56\mathrm{\AA}\f$ which is the length of a single bond.
 * All other (i.e. double and triple) are always shorter
 * @param ai - index denoting the type of the first bonded atom, e.g. 6 for carbon
 * @param aj - index denoting the type of the second bonded atom, e.g. 8 for oxygen
 */
double bond_length(const core::index1 & ai,const  core::index1 & aj);

/** @brief Provide a crude estimation of a bond length between two given atom types.
 * This should be understood as the maximum length where a bond may be observed, e.g. for two
 * carbon atoms this method will return \f$1.56\mathrm{\AA}\f$ which is the length of a single bond.
 * All other (i.e. double and triple) are always shorter
 * @param ai - the type of the first bonded atom
 * @param aj - the type of the second bonded atom
 */
double bond_length(const AtomicElement & ai, const AtomicElement & aj);

/** \brief Creates a molecule from atoms.
 *
 * All the atoms within the given iteration will be used to create a molecule. Bonds will be detected based on
 * interatomic distances.
 * @param atoms_begin - iterator pointing to the very first atom to be used in a molecule
 * @param atoms_end - past-iterator defining the end of atoms
 * @param tolerance - parameter controlling when a bond is created between two atoms
 * @tparam It - iterator type
 * @return a newly created molecule object
 *
 * \todo_code implement detection of bond order
 */
template<typename It>
std::shared_ptr<Molecule<typename It::value_type>> create_molecule(It atoms_begin,It atoms_end, const double tolerance = 0.1) {

  typedef typename It::value_type AtomType;

  std::shared_ptr<Molecule<AtomType>> m = std::make_shared<Molecule<AtomType>>();

  for (It a_it=atoms_begin; a_it != atoms_end; ++a_it) m->add_atom(*a_it);
  for (auto a_it = m->begin_atom(); a_it != m->end_atom(); ++a_it) {
    PdbAtom_SP ai = *a_it;
    for (auto b_it = m->begin_atom(); b_it != m->end_atom(); ++b_it) {
      PdbAtom_SP aj = *b_it;
      if(ai==aj) break;
      double c = bond_length(ai->element_index(),aj->element_index())+ tolerance;
      c *= c;
      double d = ai->distance_square_to(*aj, c);
      if (d < c) m->bind_atoms(ai, aj, BondType::SINGLE);
    }
  }

  return m;
}

/** \brief Creates a molecule from a given Structure.
 *
 * All the atoms of the given Structure will be used to create a molecule. Bonds will be detected based on
 * interatomic distances.
 * @param s - a structure object
 * @param tolerance - parameter controlling when a bond is created between two atoms
 * @return a newly created molecule object
 *
 * \todo_code implement detection of bond order
 */
core::chemical::PdbMolecule_SP structure_to_molecule(const core::data::structural::Structure & s, const double tolerance = 0.1);

/** @brief Find bonds between atoms.
 *
 * The method creates a list of bonds stored as tuples of two integer indexes, pointing to atoms
 *
 * @tparam It - iterator type ti provide atoms
 * @param atoms_begin - iterator that points to the first atom
 * @param atoms_end - iterator that points to behind the last atom
 * @param bonds - vector to sore results
 * @param tolerance - tolerance for bond lengths
 */
template<typename It>
void detect_bond_indexes(It atoms_begin, It atoms_end,
                         std::vector<std::pair<core::index4, core::index4>> & bonds, const double tolerance = 0.1) {

  bonds.clear();
  core::index4 i = 0;
  for (It a_it = atoms_begin; a_it != atoms_end; ++a_it) {
    PdbAtom_SP ai = *a_it;
    core::index4 j = 0;
    for (auto b_it = atoms_begin; b_it != atoms_end; ++b_it) {
      PdbAtom_SP aj = *b_it;
      if (ai == aj) break;
      double c = bond_length(ai->element_index(), aj->element_index()) + tolerance;
      c *= c;
      double d = ai->distance_square_to(*aj, c);
      if (d < c) bonds.emplace_back(i, j);
      ++j;
    }
    ++i;
  }
}

std::vector<std::pair<index4, index4>>
maximal_common_submolecule(const core::chemical::PdbMolecule &mol1, const core::chemical::PdbMolecule &mol2);

/** @brief Find indexes of atoms that belong to a ring.
 *
 * The method uses find_cycles() graph utility to detect rings
 *
 * @param mol - an input molecule
 * @return a vector of rings, every ring is represented as a vector of atom indexes
 */
std::vector<std::vector<index4>> find_rings(core::chemical::PdbMolecule &mol);

/** @brief Find atoms that belong to a ring.
 *
 * The method uses find_cycles() graph utility to detect rings
 *
 * @param mol - an input molecule
 * @return a vector of rings, every ring is represented as a vector of its atoms
 */
std::vector<std::vector<PdbAtom_SP>> find_ring_atoms(core::chemical::PdbMolecule &mol);

/** @brief Splits a molecule that actually comprises separate chemical moieties into separate molecules.
 *
 * This function creates new molecules based on the source molecule. Any two molecules will be separated
 * if there is no bond (chemical connection) defined between them.
 * @param mol - source molecule
 * @param min_size minimum number of atoms a molecule must have to be created
 * @return a vector of molecules
 */
std::vector<core::chemical::PdbMolecule_SP> split_molecules(core::chemical::PdbMolecule &mol,
    const core::index2 min_size);

}
}

#endif

/**
 * @example ex_find_side_group.cc
 */