#include <core/chemical/BondType.hh>
#include <core/chemical/MonomerStructure.hh>
#include <core/chemical/molecule_utils.hh>
#include <core/calc/structural/angles.hh>

#include <utils/string_utils.hh>

namespace core {
namespace chemical {

using namespace core::data::io;
using namespace data::structural;

const std::string monomer_section_column = std::string("_chem_comp.id");
const std::string monomer_code1_column = std::string("_chem_comp.one_letter_code");
const std::string monomer_code3_column = std::string("_chem_comp.three_letter_code");
const std::string monomer_charge_column = std::string("_chem_comp.pdbx_formal_charge");
const std::string monomer_ambig_flag_column = std::string("_chem_comp.pdbx_ambiguous_flag");
const std::string atom_section_column = std::string("_chem_comp_atom.comp_id");
const std::string is_aromatic_column = std::string("_chem_comp_atom.pdbx_aromatic_flag");
const std::string atom_id_column = std::string("_chem_comp_atom.pdbx_ordinal");
const std::string atom_name_column = std::string("_chem_comp_atom.atom_id");
const std::string x_column = std::string("_chem_comp_atom.pdbx_model_Cartn_x_ideal");
const std::string y_column = std::string("_chem_comp_atom.pdbx_model_Cartn_y_ideal");
const std::string z_column = std::string("_chem_comp_atom.pdbx_model_Cartn_z_ideal");
const std::string symbol_column = std::string("_chem_comp_atom.type_symbol");
const std::string bond_section_column = std::string("_chem_comp_bond.comp_id");
const std::string bond_atom1_column = std::string("_chem_comp_bond.atom_id_1");
const std::string bond_atom2_column = std::string("_chem_comp_bond.atom_id_2");
const std::string bond_type_column = std::string("_chem_comp_bond.value_order");

MonomerStructure_SP MonomerStructure::from_cif(const std::string &cif_filename) {

  Cif reader(cif_filename);
  CifSection_SP atoms_section;
  CifSection_SP bonds_section;
  CifSection_SP monomer_section;
  for (auto block: reader) {
    for (auto section: *(block.second)) {
      if (section->is_loop())
        for (auto lab: section->loop_labels()) {
          if (lab == atom_section_column)
            atoms_section = section;
          if (lab == bond_section_column)
            bonds_section = section;
          if (lab == monomer_section_column)
            monomer_section = section;
        }
      else if (section->has_key(monomer_section_column))
        monomer_section = section;
    }
  }

  unsigned char n_atoms = 0;
  unsigned char n_heavy_atoms = 0;
  for (auto i = 0; i < atoms_section->loop().at(atom_section_column).size(); i++) {
    n_atoms += 1;
    if (atoms_section->loop().at(symbol_column)[i] != "H")
      n_heavy_atoms += 1;
  }

  char code1 = utils::from_string<char>(monomer_section->at(monomer_code1_column));
  const char *code3 = monomer_section->at(monomer_code3_column).c_str();

  bool ambig_flag;
  if (monomer_section->at(monomer_ambig_flag_column) == "N")
    ambig_flag = false;
  else ambig_flag = true;

  double charge = utils::from_string<double>(monomer_section->at(monomer_charge_column));

  return std::make_shared<MonomerStructure>(atoms_section, bonds_section, code1, code3,
                                            n_atoms, n_heavy_atoms, ambig_flag, charge);
}

MonomerStructure_SP MonomerStructure::from_pdb(const std::string& file_name) {

  PdbMolecule_SP mol = PdbMolecule::from_pdb(file_name);
  const Monomer & m = mol->get_atom(0)->owner()->residue_type();

  MonomerStructure_SP ms = std::make_shared<MonomerStructure>(m, *mol);

  return ms;
}


MonomerStructure::MonomerStructure(const Monomer & m, PdbMolecule & mol) : PdbMolecule(mol), Monomer(m),
    logs("MonomerStructure") {

  assign_atoms();
  for (const std::vector<PdbAtom_SP> &a_ring: rings_) {
    if (a_ring.size() < 5) continue;                    // --- 5 atoms minimum to have an aromatic ring
    double min_dih = M_PI;                              // --- by default the ring is flat, but we will check...
    for (int i = 0; i < a_ring.size() - 3; ++i) {       // --- find the smallest dihedral angle of this ring
      double a = fabs(core::calc::structural::evaluate_dihedral_angle(*a_ring[i], // --- make it always positive
                          *a_ring[i + 1], *a_ring[i + 2], *a_ring[i + 3]));
      a = std::max(a, M_PI - a);                        // --- turn 0 deg into 180 deg
      min_dih = std::min(min_dih, fabs(a));             // --- find the smallest
    }
    if(min_dih>MIN_AROMATIC_RING_DIHEDRAL) {            // --- it's flat enough to be aromatic
      for(PdbAtom_SP ai: a_ring) {
        aromatic_atoms_.push_back(ai);
        logs << utils::LogLevel::FINE << "atom " << ai->atom_name() << " of " << m.code3
             << " residue marked as aromatic\n";
      }
    }
  }
}


MonomerStructure::MonomerStructure(const CifSection_SP atoms_section, const CifSection_SP bonds_section,
                                   const char code1, const char *code3,
                                   const unsigned char n_atoms, const unsigned char n_heavy_atoms,
                                   const bool ambig_flag, const double charge) : PdbMolecule(),
    Monomer(0, code1, code3, 'P', n_atoms, n_heavy_atoms, ambig_flag, charge, 0), logs("MonomerStructure") {

  for (auto i = 0; i < atoms_section->loop().at(atom_section_column).size(); i++) {

    core::index4 id = utils::from_string<core::index4>(atoms_section->loop().at(atom_id_column)[i]);
    std::string atom_name = atoms_section->loop().at(atom_name_column)[i];
    double x = utils::from_string<double>(atoms_section->loop().at(x_column)[i]);
    double y = utils::from_string<double>(atoms_section->loop().at(y_column)[i]);
    double z = utils::from_string<double>(atoms_section->loop().at(z_column)[i]);
    std::string symbol = atoms_section->loop().at(symbol_column)[i];
    PdbAtom_SP atom = std::make_shared<PdbAtom>(id, core::data::structural::format_pdb_atom_name(atom_name), x, y, z, 1.0,
                                                99.99, core::chemical::AtomicElement::by_symbol(symbol).z);
    if (atoms_section->loop().at(is_aromatic_column)[i] == "Y")
      aromatic_atoms_.push_back(atom);
    add_atom(atom);
  }

  for (auto i = 0; i < bonds_section->loop().at(bond_section_column).size(); i++) {
    std::string atom1 = bonds_section->loop().at(bond_atom1_column)[i];
    std::string atom2 = bonds_section->loop().at(bond_atom2_column)[i];
    std::string type = bonds_section->loop().at(bond_type_column)[i];
    PdbAtom_SP atm1;
    PdbAtom_SP atm2;

    for (auto i = cbegin_atom(); i != cend_atom(); i++) {
      if ((*i)->atom_name() == core::data::structural::format_pdb_atom_name(atom1))
        atm1 = *i;
      if ((*i)->atom_name() == core::data::structural::format_pdb_atom_name(atom2))
        atm2 = *i;
    }
    bind_atoms(atm1, atm2, bond_type_from_code(type));
  }
  assign_atoms();
}

void MonomerStructure::assign_atoms() {

  for (auto i = cbegin_atom(); i != cend_atom(); i++) {
    // ---------- Every N and O is an acceptor for a hydrogen atom, Cl and F also
    if ((*i)->element_index() == 7 || (*i)->element_index() == 8 || (*i)->element_index() == 17 ||
        (*i)->element_index() == 9) {
      hydrogen_acceptors_.push_back(*i);
      // ---------- and is a donor if it is connected to at least one H
      for (auto atm:get_adjacent((*i)->id() - 1))
        if (get_atom(atm)->element_index() == 1) {
          hydrogen_donors_.push_back(*i);
          break;
        }
    }
	  //  --- nonpolar_heavy is S and C (but not carbonyl one)
    else if ((*i)->element_index() == 16)
      nonpolar_heavy_.push_back(*i);
    else if ((*i)->element_index() == 6) {
      bool carbonyl_flag = false;
      for (auto atm2_index:get_adjacent((*i)->id() - 1))
        //if the second atom is O and it is bouble bond then it is a carbonyl
        if (get_atom(atm2_index)->element_index() == 8 &&
            get_bond(*i, get_atom(atm2_index)) == core::chemical::BondType::DOUBLE) {
          carbonyl_flag = true;
          break;
        }
      if (!carbonyl_flag) nonpolar_heavy_.push_back(*i);
    }
  }

  // --- assign polar_hydrogens
  for (auto donor:hydrogen_donors_)
    for (auto atm:get_adjacent((*donor).id() - 1))
      if (get_atom(atm)->element_index() == 1) {
        polar_hydrogens_.push_back(get_atom(atm));
      }

  // --- assign nonpolar_hydrogens
  for (auto atm: nonpolar_heavy_)
    for (auto hydro:get_adjacent(atm->id() - 1))
      if (get_atom(hydro)->element_index() == 1)
        nonpolar_hydrogens_.push_back(get_atom(hydro));

  rings_ = find_ring_atoms(*this);
}

}
}
