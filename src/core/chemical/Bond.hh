/** \file Bond.hh
 * @brief Class representing a chemical bond 
 */
#ifndef CORE_CHEMICAL_Bond_HH
#define CORE_CHEMICAL_Bond_HH

#include <core/chemical/BondType.hh>
#include <core/data/structural/PdbAtom.hh>

namespace core {
namespace chemical {

/** \brief Represents a chemical bond between two objects, e.g. atoms
 *
 * @param first_atom - the first atom object
 * @param second_atom - the second atom object
 * @param type - the type of this bond
 * @tparam A - type of the bonded objects, e.g. PdbAtom
 */
template<class A>
class Bond {
public:
  /// The first of the two bonded atoms
  const A first_atom() const { return first_atom_; }
  /// The second of the two bonded atoms
  const A second_atom() const { return second_atom_; }
  /// Bond type
  core::chemical::BondType type() const { return type_; }

  /** \brief Constructor creates a new bond for a given pair of atoms
   *
   * @param first_atom - the first atom object
   * @param second_atom - the second atom object
   * @param type - the type of this bond
   * @tparam A - type of the bonded objects, e.g. PdbAtom
   */
  Bond(A first_atom, A second_atom, const BondType type ) : first_atom_(first_atom), second_atom_(second_atom), type_(type) {}

protected:
  A first_atom_; ///< the first atom object (deep copy)
  A second_atom_;///< the second atom object (deep copy)
  const core::chemical::BondType type_; ///< bond type
};

/// PdbBond is a Bond speciated for PdbAtom_SP type
typedef Bond<core::data::structural::PdbAtom_SP> PdbBond;
/// Declaration of the shared pointer to a PdbBond type
typedef std::shared_ptr<PdbBond> PdbBond_SP;

/// ostream operator writes a PdbBond object into a stream (e.g. for debug purposes)
std::ostream & operator<<(std::ostream & out, const PdbBond & b);

/// ostream operator writes a PdbBond object into a stream (e.g. for debug purposes)
std::ostream & operator<<(std::ostream & out, const PdbBond_SP b);

}
}

#endif
