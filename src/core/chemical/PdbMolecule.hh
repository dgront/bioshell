#ifndef CORE_CHEMICAL_PdbMolecule_HH
#define CORE_CHEMICAL_PdbMolecule_HH

#include <core/chemical/Molecule.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/selectors/structure_selectors.hh>
#include <core/chemical/PdbMolecule.fwd.hh>

namespace core {
namespace chemical {

/// A helper data structure to help providing bond definitions for PdbMolecule constructor
struct BondDefinition {
  core::index4 first_atom_id; ///< Id of the first bonded atom; IDs start from 1, as in thr PDB format
  core::index4 second_atom_id; ///< Id of the second bonded atom; IDs start from 1, as in thr PDB format
  std::string bond_code; ///< A string defining the type of this bond
};

/** @brief Molecule that uses PdbAtom_SP type to represent atoms; bonds are represented by Bond<PdbAtom_SP> objects
 */
class PdbMolecule : public Molecule<core::data::structural::PdbAtom_SP> {
  using ResidueSelector = core::data::structural::selectors::ResidueSelector;
  using SelectEverything = core::data::structural::selectors::SelectEverything;
public:
  /// Default constructor creates an empty molecule
  PdbMolecule() = default;

  /// Copy constructor
  PdbMolecule(const PdbMolecule & mol);

  /** @brief Creates a molecule from a list of atoms and a list of bonds
   */
  PdbMolecule(const std::vector<std::string> & atom_names, const std::vector<BondDefinition> & bonds);

  /** @brief Creates a PdbMolecule object from a PDB file
   *
   * @param file_name - input PDB file
   * @param select_residues - a selector that allows creating a molecule only from a selected part of the PDB object
   * @return a newly created molecule
   */
  static PdbMolecule_SP from_pdb(const std::string & file_name, const ResidueSelector & select_residues = SelectEverything());

  /** @brief Creates a PdbMolecule object from a PDB file
   *
   * @param input_data - input PDB data stream
   * @param select_residues - a selector that allows creating a molecule only from a selected part of the PDB object
   * @return a newly created molecule
   */
  static PdbMolecule_SP from_pdb(std::istream & input_data, const ResidueSelector & select_residues = SelectEverything());

private:
  /// Keep a pointer to the structure who owns the residue of this molecule, otherwise it may expire
  core::data::structural::Structure_SP my_structure_;
};

}
}

#endif
