#include <core/chemical/ChiAnglesDefinition.hh>

namespace core {
namespace chemical {

const std::map<std::string, std::vector<std::vector<std::string>>>ChiAnglesDefinition::chi_angles = { //
  { "VAL", { {" N  "," CA "," CB "," CG1"}}},
  { "VAL", { {" N  "," CA "," CB "," CG1"}}},
  { "CYS", { {" N  "," CA "," CB "," SG "}}},
  { "ASP", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," OD1"}}},
  { "SER", { {" N  "," CA "," CB "," OG "}}},
  { "GLN", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," CD "}, {" CB "," CG "," CD "," OE1"}}},
  { "LYS", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," CD "}, {" CB "," CG "," CD "," CE "}, {" CG "," CD "," CE "," NZ "}}},
  { "ILE", { {" N  "," CA "," CB "," CG1"}, {" CA "," CB "," CG1"," CD1"}}},
  { "PRO", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," CD "}, {" CB "," CG "," CD "," N  "}}},
  { "THR", { {" N  "," CA "," CB "," OG1"}}},
  { "PHE", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," CD1"}}},
  { "GLU", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," CD "}, {" CB "," CG "," CD "," OE1"}}},
  { "MET", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," SD "}, {" CB "," CG "," SD "," CE "}}},
  { "HIS", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," ND1"}}},
  { "LEU", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," CD1"}}},
  { "ARG", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," CD "}, {" CB "," CG "," CD "," NE "}, {" CG "," CD "," NE "," CZ "}}},
  { "TRP", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," CD1"}}},
  { "MSE", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG ","SE  "}, {" CB "," CG ","SE  "," CE "}}},
  { "VAL", { {" N  "," CA "," CB "," CG1"}}},
  { "ASN", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," OD1"}}},
  { "TYR", { {" N  "," CA "," CB "," CG "}, {" CA "," CB "," CG "," CD1"}}}};

}
}
