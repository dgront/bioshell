#include <core/chemical/PdbMolecule.hh>
#include <core/data/structural/PdbAtom.hh>
#include <core/data/structural/Structure.hh>
#include <core/data/io/Pdb.hh>

namespace core {
namespace chemical {

using namespace core::data::structural;

PdbMolecule::PdbMolecule(const PdbMolecule & mol) {

  for(auto it=mol.cbegin_atom(); it!=mol.cend_atom();++it) {
    PdbAtom_SP copy = *it;
    add_atom(copy);
  }
  for(auto it=mol.cbegin_bonds(); it!=mol.cend_bonds();++it) {
    const std::pair<std::pair<index4, index4>,BondType> p = *it;
    bind_atoms(p.first.first, p.first.second, p.second);
  }
}

PdbMolecule::PdbMolecule(const std::vector<std::string> &atom_names, const std::vector<BondDefinition> &bonds) {

  using core::data::structural::PdbAtom;
  using core::data::structural::PdbAtom_SP;

  index2 i = 1;
  for (const std::string & name:atom_names) {
    PdbAtom_SP a = std::make_shared<PdbAtom>(i, name);
    add_atom(a);
    ++i;
  }

  for (const BondDefinition &bi:bonds)
    bind_atoms(bi.first_atom_id - 1, bi.second_atom_id - 1, core::chemical::bond_type_from_code(bi.bond_code));
}

PdbMolecule_SP PdbMolecule::from_pdb(std::istream &input_data, const ResidueSelector &select_residues) {

  using namespace core::data::io;
  typedef GraphWithData<SimpleGraph,PdbAtom_SP,BondType> base;

  utils::Logger logs("PdbMolecule::from_pdb");

  std::map<core::index4, PdbAtom_SP> atom_by_id;
  PdbMolecule_SP mol = std::make_shared<PdbMolecule>();

  Pdb reader(input_data, keep_all, keep_all, true, true);
  Structure_SP s = reader.create_structure(0);
  for (auto a_it = s->first_atom(); a_it != s->last_atom(); ++a_it) {
    if(select_residues(**a_it)) {
      mol->add_atom(*a_it);
      atom_by_id[(**a_it).id()] = *a_it;
    }
  }

  auto from_to = reader.header.equal_range("CONECT");
  for(auto it=from_to.first;it!=from_to.second;++it) {
    const std::vector<index4> & atom_ids = std::static_pointer_cast<Conect>(it->second)->atom_ids();
    PdbAtom_SP a0 = atom_by_id[atom_ids[0]];
    if (a0 == nullptr) {
      logs << utils::LogLevel::WARNING << "can't find atom of ID: " << atom_ids[0]
           << ". It will not be connected to anything. The incorrect CONECT record was:\n"<<(*it->second).to_pdb_line()<<"\n";
      continue;
    }
    if(atom_by_id.find(atom_ids[0])==atom_by_id.cend()) continue; // --- no such atom in this PDB, .e.g. some atoms were removed
    for (int i = 1; i < atom_ids.size(); ++i) {                      // --- IDs of all atoms connected to a0
      PdbAtom_SP ai = atom_by_id[atom_ids[i]];
      if (ai == nullptr) {
        logs << utils::LogLevel::WARNING << "can't find atom of ID: " << atom_ids[i]
             << ". It will not be connected to anything\n";
        continue;
      }
      if (atom_by_id.find(atom_ids[i]) != atom_by_id.cend()) {
        BondType type = BondType::SINGLE;
        if (mol->are_bonded(a0, ai)) {
          if (mol->get_bond(a0, ai) == BondType::SINGLE) type = BondType::DOUBLE;
          if (mol->get_bond(a0, ai) == BondType::DOUBLE) type = BondType::TRIPLE;
        }
        mol->base::add_edge(a0, ai, type);
      }
    }
  }

  mol->my_structure_ = s; /// --- store a pointer to the structure loaded to prevent pointers from expiration

  return mol;
}

PdbMolecule_SP PdbMolecule::from_pdb(const std::string & file_name, const ResidueSelector & select_residues) {

  std::ifstream in(file_name);
  return from_pdb(in, select_residues);
}

}
}