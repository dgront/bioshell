/** @file PKaValues.hh
 * @brief provides pKa data for amino acid residues
 */
#ifndef CORE_CHEMICAL_PKaValues_HH
#define CORE_CHEMICAL_PKaValues_HH

#include <vector>
#include <initializer_list>

#include <core/chemical/Monomer.hh>

namespace core {
namespace chemical {

/** \brief Provides a few sets of pKa values that may be used to calculate pI for a protein sequence.
 */
class PKaValues {
public:
  static const PKaValues EMBOSS;///< Set used by EMBOSS
  static const PKaValues SOLOMON;///< Extracted from: Solomon's Organic Chemistry, fifth edition
  static const PKaValues SILLERO;///< Extracted from: Ribeiro, JM and Sillero, A,  Comput. Biol. Med. 1990, 20, 235-242.

  /** @brief Initialize the object with given values.
   *
   * The initializer list must provide the pKa values for: N-terminus, C-terminus, CYS, ASP, GLU, HIS, LYS, ARG and TYR
   */
  PKaValues(const std::initializer_list<double> values) : parameters(values) {}

  /// Returns pKa for the N-terminus of a protein
  double pKa_N_term() const { return parameters[0]; }

   /// Returns pKa for the C-terminus of a protein
   double pKa_C_term() const { return parameters[1]; }

   /// Returns pKa for a CYS residue
   double pKa_CYS() const { return parameters[2]; }

   /// Returns pKa for a ASP residue
   double pKa_ASP() const { return parameters[3]; }

   /// Returns pKa for a GLU residue
   double pKa_GLU() const { return parameters[4]; }

   /// Returns pKa for a HIS residue
   double pKa_HIS() const { return parameters[5]; }

   /// Returns pKa for a LYS residue
   double pKa_LYS() const { return parameters[6]; }

   /// Returns pKa for a ARG residue
   double pKa_ARG() const { return parameters[7]; }

   /// Returns pKa for a TYR residue
   double pKa_TYR() const { return parameters[8]; }

  /// Returns pKa for a given residue type
  double pKa(const core::chemical::Monomer & m) const;

private:
  std::vector<double> parameters;
};

}
}

#endif
