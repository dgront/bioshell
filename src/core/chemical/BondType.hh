/** \file BondType.hh
 * @brief Defines chemical bond types.
 * This file provides also methods to convert between a string code denoting a bond type and the respective enum value
 */
#ifndef CORE_CHEMICAL_BondType_HH
#define CORE_CHEMICAL_BondType_HH

#include <string>
#include <map>

namespace core {
namespace chemical {

/** \brief Defines the types of chemical bonds.
 * This enum defines a few bond types, that can be found in CIF, MOL2 and SDF files. The enum vales are used to define
 * bond types in a small molecule (e.g. Molecule instances)
 */
enum class BondType {

	DUMMY = 0, ///< a dummy bond, most likely it is not a chemical bond at all, but for some technical reason the respective atoms should stay connected
	SINGLE = 1, ///< a single bond
	DOUBLE = 2, ///< a double bond
	TRIPLE = 3, ///< a triple bond
	AROMATIC = 4, ///< an aromatic bond
	DISULFIDE = 5, ///< a disulfide bond
	AMIDE = 6, ///< an amide bond
	HYDROGEN = 7, ///< a hydrogen bond
	UNKNOWN = 8 ///< any other chemical bond
};

/** \brief Returns a string denoting a given bond type, as in MOL2 format.
 *
 * Known bond codes are: '1', '2', '3', 'ar', 'hy' and 'du'
 *
 * @param type - a bond type
 * @return a string, e.g. "2" for a double bond or "ar" for an aromatic bond
 */
std::string mol2_bond_code_from_type(const BondType type);

/** \brief Returns a bond type for a given string describing a bond.
 *
 * @param code - a bond type code, e.g. "ar" (aromatic bond code in MOL2 format) or "DOUB"
 * (denotes a double bond in CIF format)
 * @return a respective BondType enum value
 */
BondType bond_type_from_code(const std::string &code);

/// Map provides string names for each BondType enum, e.g. to print bonds on a screen
extern std::map<BondType, std::string> bond_names;

}
}
#endif

